//--------------------------------------------------------------------------------------
// File: TestScript.cpp
//
// c++ script created from a dll
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Stranded.h"
#include "TestScript.h"


// Setup goes here
bool TestScript::Init()
{
	auto entity = _engine->CreateEntity( "Lee" );
	Model* model = Model::CreateFromFile( "Resources\\Models\\cube.mesh", _engine );
	model->SetScale( 10 );
	entity->AddComponent( model );
	_engine->AddEntity( entity );
	std::cout << "Hi there, I'm a script\n";

	return true;
}

// Per-frame logic
void TestScript::Update( float dt )
{

}

// Cleanup goes here
void TestScript::Release()
{

}

// Called when a component is added to the parent entity
void TestScript::OnComponentAdded( Component* component )
{

}

// Called when a component is removed from the parent entity
void TestScript::OnComponentRemoved( Component* component )
{

}

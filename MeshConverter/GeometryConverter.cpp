//--------------------------------------------------------------------------------------
// File: GeometryConverter.cpp
//
// Handles conversion of 3rd party model formats into Maoli Binary
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "GeometryConverter.h"
#include "Quake3BSP.h"

#include "Engine\Engine.h"
#include "Animation/AnimationManager.h"

// ASSIMP
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

namespace Maoli
{
	// Ctor
	GeomtryConverter::GeomtryConverter( Engine* engine ) : Geometry( engine )
	{

	}

	// Convert a 3d model into Maoli Binary format
	// Materials are sent to the output directory
	bool GeomtryConverter::ConvertModel( const char* inputfile, const char* outputFile )
	{
		if ( LoadUnsupportedFormat( inputfile ) )
		{
			Save( outputFile );
			Release();
			return true;
		}

		return false;
	}

	// Import from assimp
	bool GeomtryConverter::LoadAssimp( const char* file )
	{
		// Import the model
		Assimp::Importer importer;
		importer.SetPropertyInteger( "AI_CONFIG_PP_SBP_REMOVE", aiPrimitiveType_LINE | aiPrimitiveType_POINT );
		const aiScene* scene = importer.ReadFile( file,
			aiProcess_GenSmoothNormals |
			//aiProcess_FindDegenerates          |
			aiProcess_PreTransformVertices |
			aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_MakeLeftHanded |
			aiProcess_ImproveCacheLocality |
			aiProcess_RemoveRedundantMaterials |
			aiProcess_GenUVCoords |
			aiProcess_TransformUVCoords |
			aiProcess_OptimizeMeshes |
			aiProcess_OptimizeGraph |
			aiProcess_SortByPType |
			aiProcess_FlipUVs |
			aiProcess_FlipWindingOrder );

		// If the import failed, report it
		if ( !scene || !scene->HasMeshes() )
		{
			std::cout << "ASSIMP Import Failed" << std::endl;
			std::cout << importer.GetErrorString() << std::endl;
			return false;
		}


		// Find out how many submeshes there are, and get the total vertex/index count
		_chunks.Allocate( scene->mNumMeshes );
		uint32 totalVerts = 0, totalFaces = 0;
		for ( uint32 i = 0; i < scene->mNumMeshes; i++ )
		{
			totalVerts += scene->mMeshes[i]->mNumVertices;
			totalFaces += scene->mMeshes[i]->mNumFaces;
		}

		// Make sure there is a default material if none exist in the file
		Array<Material*> materials;
		if ( scene->HasMaterials() )
		{
			materials.Allocate( scene->mNumMaterials );
			aiColor3D mColor;
			aiString szTex;
			String szDir = String( file ).GetDirectory();
			String materialName = String( file ).GetFile().RemoveFileExtension();
			Array<FixedString<char, 4>> ext;
			ext.Add( ".bmp" );
			ext.Add( ".jpg" );
			ext.Add( ".tga" );
			ext.Add( ".tif" );
			ext.Add( ".png" );
			ext.Add( ".dds" );
			std::unordered_map<aiTextureType, TEX_TYPE> texMapping;
			texMapping[aiTextureType_DIFFUSE] = TEX_DIFFUSE;
			texMapping[aiTextureType_NORMALS] = TEX_NORMAL;
			texMapping[aiTextureType_SPECULAR] = TEX_SPECULAR;
			texMapping[aiTextureType_OPACITY] = TEX_ALPHA;
			for ( uint32 i = 0; i < scene->mNumMaterials; i++ )
			{
				Material* pMat = Material::Create( materialName + i, _parent );
				materials[i] = pMat;

				// Properties
				scene->mMaterials[i]->Get( AI_MATKEY_COLOR_DIFFUSE, mColor );
				pMat->SetBaseColor( Vector3( mColor.r, mColor.g, mColor.b ) );
				scene->mMaterials[i]->Get( AI_MATKEY_COLOR_EMISSIVE, mColor );
				pMat->SetEmissive( Vector4( 0, 0, 0 ) );
				//scene->mMaterials[i]->Get(AI_MATKEY_OPACITY, fValue);
				//pMat->SetTransparency(1.0f-fValue);

				// Textures
				for ( auto iter = texMapping.begin(); iter != texMapping.end(); ++iter )
				{
					scene->mMaterials[i]->Get( AI_MATKEY_TEXTURE( iter->first, 0 ), szTex );
					if ( szTex.length > 0 )
					{
						// Sometimes files are wrong, so try many extensions
						String baseName = String( szTex.data ).RemoveFileExtension();
						for ( uint32 k = 0; k < ext.Size(); ++k )
						{
							if ( FileExists( szDir + baseName + ext[k] ) )
							{
								baseName += ext[k];
								break;
							}
						}

						if ( !pMat->SetTextureName( szDir + baseName, iter->second ) )
						if ( !pMat->SetTextureName( szDir.GetPreviousDirectory( ) + baseName, iter->second ) )
							pMat->SetTextureName( String( "..\\" ) + baseName, iter->second );
					}
				}
			}
		}

		// Allocate the mesh data, and fill it
		_positions.Allocate( totalVerts );
		_texCoords.Allocate( totalVerts );
		_normals.Allocate( totalVerts );
		_indices.Allocate( totalFaces * 3 );
		uint32 iCount = 0, vCount = 0;
		for ( uint32 i = 0; i < scene->mNumMeshes; i++ )
		{
			// Submesh info
			_chunks[i].indexCount = scene->mMeshes[i]->mNumFaces * 3;
			_chunks[i].startIndex = iCount;
			_chunks[i].material = materials[scene->mMeshes[i]->mMaterialIndex];
			//scene->mMaterials[i]->Get(AI_MATKEY_TWOSIDED, iValue);
			//if(iValue==1)
			//    _chunks[i].renderOptions.Set( RO_TWOSIDED );
			_chunks[i].name = String( "SubMesh" ) + ((int32)i + 1);

			// Indices
			for ( uint32 j = 0; j < scene->mMeshes[i]->mNumFaces; j++ )
			{
				if ( scene->mMeshes[i]->mFaces[j].mNumIndices == 3 )
				{
					_indices[iCount] = vCount + scene->mMeshes[i]->mFaces[j].mIndices[0];
					_indices[iCount + 1] = vCount + scene->mMeshes[i]->mFaces[j].mIndices[1];
					_indices[iCount + 2] = vCount + scene->mMeshes[i]->mFaces[j].mIndices[2];
					iCount += 3;
				}
			}

			// Vertices
			for ( uint32 j = 0; j < scene->mMeshes[i]->mNumVertices; j++, vCount++ )
			{
				// Positions
				_positions[vCount] = Vector3( scene->mMeshes[i]->mVertices[j].x,
					scene->mMeshes[i]->mVertices[j].y, scene->mMeshes[i]->mVertices[j].z );

				// Texture coords
				if ( scene->mMeshes[i]->HasTextureCoords( 0 ) )
				{
					_texCoords[vCount].x = scene->mMeshes[i]->mTextureCoords[0][j].x;
					_texCoords[vCount].y = scene->mMeshes[i]->mTextureCoords[0][j].y;
				}

				// Normals
				if ( scene->mMeshes[i]->HasNormals() )
					_normals[vCount] = Vector3( scene->mMeshes[i]->mNormals[j].x,
					scene->mMeshes[i]->mNormals[j].y, scene->mMeshes[i]->mNormals[j].z );
			}
		}

		return true;
	}


	// Import from maya
	bool GeomtryConverter::LoadMaya( const char* file )
	{
		// Open the file
		std::ifstream fin( file, std::ios::binary );
		if ( fin.is_open() )
		{
			// Number of verts
			uint32 numVerts;
			fin.read( reinterpret_cast<char*>(&numVerts), sizeof(uint32) );

			// Read in each vertex
			_positions.Allocate( numVerts );
			_normals.Allocate( numVerts );
			_texCoords.Allocate( numVerts );
			for ( uint32 i = 0; i < numVerts; ++i )
			{
				// Position
				fin.read( reinterpret_cast<char*>(&_positions[i]), 3 * sizeof(float) );

				// Normal
				fin.read( reinterpret_cast<char*>(&_normals[i]), 3 * sizeof(float) );

				// Tex coords
				fin.read( reinterpret_cast<char*>(&_texCoords[i]), 2 * sizeof(float) );
			}


			// Triangles
			uint32 numFaces;
			fin.read( reinterpret_cast<char*>(&numFaces), sizeof(uint32) );

			// Triangle data
			_indices.Allocate( numFaces * 3 );
			fin.read( reinterpret_cast<char*>(_indices.GetRawData()), sizeof(uint32)*numFaces * 3 );

			// Setup a single submesh
			_chunks.Allocate( 1 );
			_chunks[0].indexCount = _indices.Size();
			_chunks[0].startIndex = 0;
			_chunks[0].material = Material::Create( String( file ).GetFile().RemoveFileExtension(), _parent );
			_chunks[0].name = "Maya";

			fin.close();
			return true;
		}
		return false;
	}

	// Import from Q3 BSP
	bool GeomtryConverter::LoadBSP( const char* file )
	{
		// Open the file
		std::ifstream fin( file, std::ifstream::binary );
		if ( fin.is_open() )
		{
			// Read in the header
			Quake3::Header header;
			fin.read( (char*)&header, sizeof(Quake3::Header) );

			// Texture names
			Quake3::DirEntry& texEntry = header.direntries[(int32)Quake3::LumpType::Textures];
			int32 numTextures = texEntry.length / sizeof(Quake3::Texture);
			Quake3::Texture* textures = new Quake3::Texture[numTextures];
			fin.seekg( texEntry.offset, std::ifstream::beg );
			fin.read( (char*)textures, texEntry.length );

			// Vertices
			Quake3::DirEntry& vertEntry = header.direntries[(int32)Quake3::LumpType::Vertexes];
			int32 numVerts = vertEntry.length / sizeof(Quake3::Vertex);
			Quake3::Vertex* verts = new Quake3::Vertex[numVerts];
			fin.seekg( vertEntry.offset, std::ifstream::beg );
			fin.read( (char*)verts, vertEntry.length );

			// Mesh triangulation
			Quake3::DirEntry& meshEntry = header.direntries[(int32)Quake3::LumpType::Meshverts];
			int32 numMeshVerts = meshEntry.length / sizeof(int32);
			int32* meshVerts = new int32[numMeshVerts];
			fin.seekg( meshEntry.offset, std::ifstream::beg );
			fin.read( (char*)meshVerts, meshEntry.length );

			// Faces
			Quake3::DirEntry& faceEntry = header.direntries[(int32)Quake3::LumpType::Faces];
			int32 numFaces = faceEntry.length / sizeof(Quake3::Face);
			Quake3::Face* faces = new Quake3::Face[numFaces];
			fin.seekg( faceEntry.offset, std::ifstream::beg );
			fin.read( (char*)faces, faceEntry.length );

			// Done with the file
			fin.close();

			// Process verts
			_positions.Allocate( numVerts );
			_texCoords.Allocate( numVerts );
			_normals.Allocate( numVerts );
			const float scale = 0.01f;
			for ( int32 i = 0; i < numVerts; ++i )
			{
				_positions[i].x = verts[i].position[0] * scale;
				_positions[i].y = verts[i].position[2] * scale;
				_positions[i].z = verts[i].position[1] * scale;

				_normals[i].x = verts[i].normal[0];
				_normals[i].y = verts[i].normal[2];
				_normals[i].z = verts[i].normal[1];

				_texCoords[i].x = verts[i].texcoord[0][0];
				_texCoords[i].y = verts[i].texcoord[0][1];
			}

			// Sort faces by texture
			std::sort( &faces[0], &faces[numFaces], []( const Quake3::Face& a, const Quake3::Face& b ){
				return a.texture < b.texture;
			} );

			// Get the texture directory for the map
			String textureDirectory = String( file ).GetDirectory().GetPreviousDirectory() + String( "\\" );

			// Process each face
			GeometryChunk* currentChunk = nullptr;
			int32 startIndex = 0;
			int32 currentTexture = -1;
			for ( int32 i = 0; i < numFaces; ++i )
			{
				// Only handle polygon and mesh types for now
				if ( faces[i].type == 1 || faces[i].type == 3 )
				{
					// Check if a new submesh needs to be created
					if ( currentTexture != faces[i].texture )
					{
						if ( currentChunk )
							startIndex += currentChunk->indexCount;

						// Load the texture
						currentTexture = faces[i].texture;
						Material* mat = Material::Create( String( textures[currentTexture].name ).GetFile().RemoveFileExtension(), _parent );
						String texFileTGA = (textureDirectory + textures[currentTexture].name + ".tga").FixFilePath();
						String texFileJPG = (textureDirectory + textures[currentTexture].name + ".jpg").FixFilePath();
						if ( Maoli::FileExists( texFileTGA ) )
							mat->SetTextureName( texFileTGA, TEX_DIFFUSE );
						else if ( Maoli::FileExists( texFileJPG ) )
							mat->SetTextureName( texFileJPG, TEX_DIFFUSE );
						else
							mat->SetTextureName( "Resources\\Textures\\Game Pack\\Textures\\tex_055.jpg", TEX_DIFFUSE );

						// Check for alpha testing
						if ( textures[currentTexture].flags == 81952 )
							mat->SetTextureName( mat->GetTextureFileNames()[TEX_DIFFUSE], TEX_ALPHA );

						currentChunk = &_chunks.Add();
						currentChunk->name += _chunks.Size();
						currentChunk->startIndex = startIndex;
						currentChunk->material = mat;
						currentChunk->indexCount = 0;
					}

					// Read the polygon data
					for ( int32 j = 0; j < faces[i].n_meshverts; ++j, ++currentChunk->indexCount )
					{
						_indices.Add( meshVerts[faces[i].meshvert + j] + faces[i].vertex );
					}
				}
			}

			// For now, patches get their own submeshes
			currentChunk = nullptr;
			currentTexture = -1;
			int32 startVertex = _positions.Size();
			for ( int32 i = 0; i < numFaces; ++i )
			{
				if ( faces[i].type == 2 )
				{
					// Check if a new submesh needs to be created
					if ( currentTexture != faces[i].texture )
					{
						if ( currentChunk )
							startIndex += currentChunk->indexCount;

						// Load the texture
						currentTexture = faces[i].texture;
						Material* mat = Material::Create( textures[currentTexture].name, _parent );
						String texFileTGA = (textureDirectory + textures[currentTexture].name + ".tga").FixFilePath();
						String texFileJPG = (textureDirectory + textures[currentTexture].name + ".jpg").FixFilePath();
						if ( Maoli::FileExists( texFileTGA ) )
							mat->SetTextureName( texFileTGA, TEX_DIFFUSE );
						else if ( Maoli::FileExists( texFileJPG ) )
							mat->SetTextureName( texFileJPG, TEX_DIFFUSE );

						currentChunk = &_chunks.Add();
						currentChunk->name += _chunks.Size();
						currentChunk->startIndex = startIndex;
						currentChunk->material = mat;
						currentChunk->indexCount = 0;
					}

					// Setup the group
					Quake3::PatchGroup p;
					p.textureIndex = faces[i].texture;
					p.lightmapIndex = faces[i].lm_index;
					p.width = faces[i].size[0];
					p.height = faces[i].size[1];

					// Create space to hold quadratic patches
					int32 numPatchesWide = (p.width - 1) / 2;
					int32 numPatchesHigh = (p.height - 1) / 2;
					p.numQuadraticPatches = numPatchesWide*numPatchesHigh;
					p.quadraticPatches = new Quake3::Patch[p.numQuadraticPatches];

					// Fill in the quadratic patches
					for ( int32 y = 0; y < numPatchesHigh; ++y )
					{
						for ( int32 x = 0; x < numPatchesWide; ++x )
						{
							for ( int32 row = 0; row < 3; ++row )
							{
								for ( int32 point = 0; point < 3; ++point )
								{
									p.quadraticPatches[y*numPatchesWide + x].
										controlPoints[row * 3 + point] = verts[faces[i].vertex +
										(y * 2 * p.width + x * 2) +
										row*p.width + point];
								}
							}

							//tesselate the patch
							p.quadraticPatches[y*numPatchesWide + x].Tesselate( 8 );
						}
					}

					// Process patch geometry
					Vector3 v;
					for ( int32 i = 0; i < p.numQuadraticPatches; ++i )
					{
						// Add the verts back into the vertex array
						startVertex = _positions.Size();
						for ( int32 j = 0; j < p.quadraticPatches[i].numVerts; ++j )
						{
							v.x = p.quadraticPatches[i].vertices[j].position[0] * scale;
							v.y = p.quadraticPatches[i].vertices[j].position[2] * scale;
							v.z = p.quadraticPatches[i].vertices[j].position[1] * scale;
							_positions.Add( v );

							v.x = p.quadraticPatches[i].vertices[j].normal[0];
							v.y = p.quadraticPatches[i].vertices[j].normal[2];
							v.z = p.quadraticPatches[i].vertices[j].normal[1];
							_normals.Add( v );

							v.x = p.quadraticPatches[i].vertices[j].texcoord[0][0];
							v.y = p.quadraticPatches[i].vertices[j].texcoord[0][1];
							_texCoords.Add( Vector2( v.x, v.y ) );
						}

						// Indices, must convert to tri list from tri strip
						int32 i0 = -1, i1 = p.quadraticPatches[i].indices[0] + startVertex, i2 = i1 = p.quadraticPatches[i].indices[1] + startVertex;
						bool flip = false;
						for ( int32 j = 2; j < p.quadraticPatches[i].numIndices; ++j )
						{
							i0 = i1;
							i1 = i2;
							i2 = p.quadraticPatches[i].indices[j] + startVertex;
							if ( flip )
							{
								_indices.Add( i0 );
								_indices.Add( i1 );
								_indices.Add( i2 );
							}
							else
							{
								_indices.Add( i0 );
								_indices.Add( i2 );
								_indices.Add( i1 );
							}
							flip = !flip;
							currentChunk->indexCount += 3;
						}
					}

					// Cleanup
					delete[] p.quadraticPatches;
				}
			}

			// Finalize the mesh
			_positions.Finalize();
			_texCoords.Finalize();
			_normals.Finalize();
			_chunks.Finalize();
			_indices.Finalize();

			// Cleanup
			delete[] textures;
			delete[] verts;
			delete[] meshVerts;
			delete[] faces;

			return true;
		}

		return false;
	}


	// Import from Havok Animation
	bool GeomtryConverter::LoadHavok( const char* file )
	{
		// Load in the file
		auto havokLoader = new hkLoader( );
		hkRootLevelContainer* container = havokLoader->load( file );
		HK_ASSERT2( 0x27343437, container != HK_NULL, "Could not load asset" );
		hkxScene* scene = reinterpret_cast<hkxScene*>(container->findObjectByType( hkxSceneClass.getName() ));
		HK_ASSERT2( 0x27343435, scene, "No scene loaded" );
		hkaAnimationContainer* ac = reinterpret_cast<hkaAnimationContainer*>(container->findObjectByType( hkaAnimationContainerClass.getName() ));
		HK_ASSERT2( 0x27343435, ac && (ac->m_skins.getSize() > 0), "No skins loaded" );
		HK_ASSERT2( 0x27343435, ac && (ac->m_skeletons.getSize() > 0), "No skins loaded" );

		// Get the skeleton and skins
		Array<hkaMeshBinding*> skinBindings;
		skinBindings.Allocate( ac->m_skins.getSize() );
		_numBones = 0;
		for ( int32 i = 0; i < ac->m_skins.getSize(); ++i )
		{
			skinBindings[i] = ac->m_skins[i];
			_numBones += skinBindings[i]->m_boneFromSkinMeshTransforms.getSize();
		}

		// Compute the number of submeshes
		uint32 numMeshes = 1;
		//for ( uint32 i = 0; i < skinBindings.Size(); ++i )
		//for ( int32 j = 0; j < skinBindings[i]->m_mesh->m_sections.getSize(); ++j, ++numMeshes );

		// hkxMeshes are split into sections, which Maoli treats as submeshes
		_chunks.Allocate( numMeshes );

		// Get the total number of vertices and indices
		uint32 numVerts = 0, indexCount = 0;
		numMeshes = 0;
		for ( uint32 i = 0; i < skinBindings.Size(); i++ )
		{
			for ( int32 j = 0; j < 1; ++j, ++numMeshes )
			//for ( int32 j = 0; j < skinBindings[i]->m_mesh->m_sections.getSize(); ++j, ++numMeshes )
			{
				// Setup the submesh
				_chunks[numMeshes].indexCount = skinBindings[i]->m_mesh->m_sections[j]->getNumTriangles() * 3;
				_chunks[numMeshes].startIndex = indexCount;

				numVerts += skinBindings[i]->m_mesh->m_sections[j]->m_vertexBuffer->getNumVertices();
				indexCount += _chunks[numMeshes].indexCount;
			}
		}

		// Read in each mesh section
		_positions.Allocate( numVerts );
		_texCoords.Allocate( numVerts );
		_normals.Allocate( numVerts );
		_boneWeights.Allocate( numVerts );
		_indices.Allocate( indexCount );
		numVerts = indexCount = 0;
		for ( uint32 e = 0; e < skinBindings.Size(); e++ )
		{
			for ( int32 i = 0; i < 1; ++i )
			//for ( int32 i = 0; i < skinBindings[e]->m_mesh->m_sections.getSize(); ++i )
			{
				// Get the vertex buffer
				hkxVertexBuffer* hk_vertex_buffer = skinBindings[e]->m_mesh->m_sections[i]->m_vertexBuffer;
				const hkxVertexDescription& hk_vert_desc = hk_vertex_buffer->getVertexDesc();

				int32 pos_stride=0, tc_stride=0, norm_stride=0;
				char* pos_base = nullptr, *tc_base = nullptr, *norm_base = nullptr;

				// Positions
				const hkxVertexDescription::ElementDecl* pos = hk_vert_desc.getElementDecl( hkxVertexDescription::HKX_DU_POSITION, 0 );
				pos_stride = pos->m_byteStride;
				pos_base = static_cast<char*>(hk_vertex_buffer->getVertexDataPtr( *pos ));

				// Tex coords
				const hkxVertexDescription::ElementDecl* tex_coord = hk_vert_desc.getElementDecl( hkxVertexDescription::HKX_DU_TEXCOORD, 0 );
				if ( tex_coord )
				{
					tc_stride = tex_coord->m_byteStride;
					tc_base = static_cast<char*>(hk_vertex_buffer->getVertexDataPtr( *tex_coord ));
				}

				// Normals
				const hkxVertexDescription::ElementDecl* norm = hk_vert_desc.getElementDecl( hkxVertexDescription::HKX_DU_NORMAL, 0 );
				norm_stride = norm->m_byteStride;
				norm_base = static_cast<char*>(hk_vertex_buffer->getVertexDataPtr( *norm ));

				// Blend weights
				const hkxVertexDescription::ElementDecl* weight = hk_vert_desc.getElementDecl( hkxVertexDescription::HKX_DU_BLENDWEIGHTS, 0 );
				int32 weight_stride = weight->m_byteStride;
				byte* weight_base = static_cast<byte*>(hk_vertex_buffer->getVertexDataPtr( *weight ));

				// Blend indices
				const hkxVertexDescription::ElementDecl* index = hk_vert_desc.getElementDecl( hkxVertexDescription::HKX_DU_BLENDINDICES, 0 );
				int32 index_stride = index->m_byteStride;
				byte* index_base = static_cast<byte*>(hk_vertex_buffer->getVertexDataPtr( *index ));


				// Mapping:
				// Bone indices can either map directly to bones (m_mappings is empty)
				// or a single mapping can exist for all mesh sections (m_mappings.getSize() == 1)
				// or each mesh section might contain it's own mapping.
				// This mapping is just an index array, where the vertex bone index is used to
				// look up the true bone index within the skeleton.
				int32 numMappings = skinBindings[e]->m_mappings.getSize();
				assert( numMappings < 2 && "Multiple mappings not supported" );
				hkaMeshBinding::Mapping* map = numMappings == 1 ? &skinBindings[e]->m_mappings[0] : nullptr;

				// Copy the index data
				hkUint32 a, b, c;
				for ( uint32 j = 0; j < skinBindings[e]->m_mesh->m_sections[i]->getNumTriangles(); j++, indexCount += 3 )
				{
					skinBindings[e]->m_mesh->m_sections[i]->getTriangleIndices( j, a, b, c );
					_indices[indexCount] = a + numVerts;
					_indices[indexCount + 1] = b + numVerts;
					_indices[indexCount + 2] = c + numVerts;
				}


				// Copy the vertex data
				for ( int32 j = 0; j < hk_vertex_buffer->getNumVertices(); j++, numVerts++ )
				{
					// Positions
					memcpy( &_positions[numVerts], pos_base, sizeof(Vector3) );

					// Tex coords
					if ( tex_coord )
					{
						memcpy( &_texCoords[numVerts], tc_base, sizeof(Vector2) );
						_texCoords[numVerts].y = 1.0f - _texCoords[numVerts].y;
					}

					// Normals
					Vector3& n = *reinterpret_cast<Vector3*>(norm_base);
					_normals[numVerts].x = -n.x;
					_normals[numVerts].y = -n.y;
					_normals[numVerts].z = -n.z;

					// Bone weights
					float sum = 0;
					for ( int32 k = 0; k < 4; k++ )
					{
						if ( map )
							_boneWeights[j].bones[k] = static_cast<uint32>(map->m_mapping[index_base[k]]);
						else
							_boneWeights[j].bones[k] = static_cast<uint32>(index_base[k]);

						sum += _boneWeights[j].weights[k] = static_cast<float>(weight_base[k]) / 255.0f;
					}

					// Increment the data pointers
					pos_base += pos_stride;
					tc_base += tc_stride;
					norm_base += norm_stride;
					index_base += index_stride;
					weight_base += weight_stride;
				}
			}
		}

		// Cleanup
		delete havokLoader;

		return true;
	}


	// Load a model
	bool GeomtryConverter::LoadUnsupportedFormat( const char* inputfile )
	{
		String ext = String( inputfile ).GetFileExtension().ToLower();
		if ( ext == "bsp" )
		{
			if ( !LoadBSP( inputfile ) )
			{
				std::cout << "Failed to load model!\n";
				Release();
				return false;
			}
		}
		else if ( ext == "mesh" )
		{
			if ( !LoadMaya( inputfile ) )
			{
				std::cout << "Failed to load model!\n";
				Release();
				return false;
			}
		}
		else if ( ext == "hkx" )
		{
			if ( !LoadHavok( inputfile ) )
			{
				std::cout << "Failed to load model!\n";
				Release();
				return false;
			}
		}
		else
		{
			if ( !LoadAssimp( inputfile ) )
			{
				std::cout << "Failed to load model!\n";
				Release();
				return false;
			}
		}

		// Finalize the mesh
		Optimize();
		//RemoveDuplicateVerts();
		if ( !HasBoneWeights() )
			CenterMesh();
		ComputeBounds();

		return true;
	}

	

}

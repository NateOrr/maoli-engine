//--------------------------------------------------------------------------------------
// File: GeometryConverter.h
//
// Handles conversion of 3rd party model formats into Maoli Binary
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Geometry.h"

namespace Maoli
{
	class GeomtryConverter : public Geometry
	{
	public:

		// Ctor
		GeomtryConverter( Engine* engine );

		// Convert a 3d model into Maoli Binary format
		// Materials are sent to the output directory
		bool ConvertModel( const char* inputfile, const char* outputFile );

		// Load a model
		bool LoadUnsupportedFormat( const char* file );

	private:

		// Internal load helpers
		bool LoadAssimp( const char* file );
		bool LoadMaya( const char* file );
		bool LoadBSP( const char* file );
		bool LoadHavok( const char* file );

	};
}

//--------------------------------------------------------------------------------------
// File: main.cpp
//
// Mesh Converter ulility
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "GeometryConverter.h"

#include <iostream>
using namespace Maoli;

int32 main( int32 argc, const char* argv[] )
{
	// Convert each mesh file
	Engine engine;
	engine.Init( 640, 480, MAOLI_ANIMATION);
	for ( int32 i = 1; i < argc; ++i )
	{
		Geometry* gc = Geometry::Create( "temp", &engine );
		String sourceFile = argv[i];
		String destFile = sourceFile.RemoveFileExtension() + ".mesh";
		((GeomtryConverter*)gc)->ConvertModel( sourceFile, destFile );
		Geometry::Delete( gc );
		std::cout << argv[i] << std::endl;
	}
	return 0;
}
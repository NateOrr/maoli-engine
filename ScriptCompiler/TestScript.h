//--------------------------------------------------------------------------------------
// File: TestScript.h
//
// c++ script created from a dll
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

using namespace Maoli;

class TestScript : public ScriptObject
{
	// Setup goes here
	virtual bool Init();

	// Per-frame logic
	virtual void Update( float dt );

	// Cleanup goes here
	virtual void Release();

	// Called when a component is added to the parent entity
	virtual void OnComponentAdded( Component* component );

	// Called when a component is removed from the parent entity
	virtual void OnComponentRemoved( Component* component );
};



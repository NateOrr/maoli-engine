//--------------------------------------------------------------------------------------
// File: Lee.cpp
//
// c++ script created from a dll
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Stranded.h"
#include "Lee.h"

// Export this script to the DLL
EXPORT_SCRIPT( Lee );

// Setup goes here
bool Lee::Init()
{
	auto entity = _engine->CreateEntity( "Lee" );
	Model* model = Model::CreateFromFile( "Resources\\Models\\cube.mesh", _engine );
	model->SetScale( 10 );
	entity->AddComponent( model );
	_engine->AddEntity( entity );
	std::cout << "Hi there, I'm a script\n";

	return true;
}

// Per-frame logic
void Lee::Update( float dt )
{

}

// Cleanup goes here
void Lee::Release()
{

}

// Called when a component is added to the parent entity
void Lee::OnComponentAdded( Component* component )
{

}

// Called when a component is removed from the parent entity
void Lee::OnComponentRemoved( Component* component )
{

}

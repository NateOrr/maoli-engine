//--------------------------------------------------------------------------------------
// File: main.cpp
//
// Script DLL Compiler Utility
//
// This program uses a template visual c++ project to build a script DLL via command line
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Maoli.h"
#include <string>
using namespace Maoli;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;

// Win32
#define NOMINMAX
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

typedef FixedString<char, 512> LongString;

int32 main( int32 argc, const char* argv[] )
{
	// Display program info
	cout << "Maoli Game Engine" << endl;
	cout << "Script Compiler Utility" << endl;
	cout << "Programmed by Nate Orr 2014" << endl << endl;

	// Validation
	if ( argc < 2 )
	{
		cout << "Usage: \"SourceFile.cpp\"" << endl;
		return -1;
	}

	// Grab the file names from the command line args
	Array<LongString> codeFiles( argc - 1 );
	Array<LongString> headerFiles( argc - 1 );
	String dir( "Solution\\" );
	for ( int32 i = 0; i < argc-1; ++i )
	{
		codeFiles[i] = argv[i+1];
		headerFiles[i] = codeFiles[i].RemoveFileExtension() + ".h";

		// Copy the script to the project directory
		if ( !CopyFileA( codeFiles[i], dir + codeFiles[i], false ) )
		{
			cout << "There was an error moving the script cpp" << codeFiles[i].c_str() << endl;
			return -1;
		}
		if ( !CopyFileA( headerFiles[i], dir + headerFiles[i], false ) )
		{
			cout << "There was an error moving the script header " << headerFiles[i].c_str() << endl;
			return -1;
		}
	}

	// Project files
	const LongString tempFile = "Solution\\temp.vcxproj";
	const LongString projectFile = "Solution\\Script.vcxproj";

	// Backup the project file
	if ( !CopyFileA( projectFile, tempFile, false ) )
	{
		cout << "Could not find visual studio project" << endl;
		return false;
	}

	// Open the vcxproj file
	ifstream fin( tempFile );
	ofstream fout( projectFile );
	if ( !fin.is_open() )
	{
		cout << "Could not open visual studio project" << endl;
		return -1;
	}

	// Modify the project file to use the new cpps
	std::string line;
	LongString newLine;
	uint32 currentLine = 0;
	while ( !fin.eof() )
	{
		++currentLine;
		getline( fin, line );
		newLine = line.c_str();
		fout << newLine.c_str() << endl;

		// Insert header files into the project
		if ( currentLine == 13 )
		{
			for ( uint32 i = 0; i < headerFiles.Size(); ++i )
			{
				fout << "    <ClInclude Include=\"" << headerFiles[i] << "\" />" << endl;
			}
		}

		// Insert cpp files into the project
		if ( currentLine == 15 )
		{
			for ( uint32 i = 0; i < codeFiles.Size(); ++i )
			{
				fout << "    <ClCompile Include=\"" << codeFiles[i] << "\" />" << endl;
			}
		}
	}
	fin.close();
	fout.close();

	// Build the dll
	system( "build.bat" );

	// Move the DLL to the dest location
	if(!CopyFileA( "Solution\\Debug\\Script.dll", "Script.dll", false ))
		cout << "There was a problem moving the script dll" << endl;

	// Cleanup
	for ( uint32 i = 0; i < codeFiles.Size(); ++i )
	{
		DeleteFileA( dir + headerFiles[i] );
		DeleteFileA( dir + codeFiles[i] );
	}
	CopyFileA( tempFile, projectFile, false );
	DeleteFileA( tempFile );

	system("pause");
}
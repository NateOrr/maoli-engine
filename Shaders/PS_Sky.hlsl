//--------------------------------------------------------------------------------------
// File: PS_Sky.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Atmosphere.hlsli>


// Skydome pixel shader
void PS_Sky(PS_INPUT_SKY input, out float4 oColor : SV_Target0)
{
	//oColor = g_txMaterial[TEX_DIFFUSE1].Sample( g_samLinear, input.Tex0 );

	float fCos = dot( g_SunDir, input.Tex1 ) / length( input.Tex1 );
	float fCos2 = fCos * fCos;
	
	float3 v3RayleighSamples = g_txScattering[SCATTER_RAYLEIGH].Sample( g_samBilinear, input.Tex0.xy ).rgb;
	float3 v3MieSamples = g_txScattering[SCATTER_MIE].Sample( g_samBilinear, input.Tex0.xy ).rgb;
	//float4 v3RayleighSamples = 0;
	//float4 v3MieSamples = 0;
	
	// Compute scattering (test)
	//ComputeScattering(input.Tex0, v3RayleighSamples, v3MieSamples);

	float3 Color = getRayleighPhase(fCos2) * v3RayleighSamples.rgb + getMiePhase(fCos, fCos2) * v3MieSamples.rgb;
	oColor =  float4( ToneMap(Color.rgb), 1 );
}

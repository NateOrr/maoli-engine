//--------------------------------------------------------------------------------------
// File: VS_River.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Water.hlsli>

// Vertex shader for water
PS_INPUT_WATER VS_River( VS_INPUT input )
{
    PS_INPUT_WATER output;
	
	// Project position
    output.Pos = mul( input.Pos, g_mWorld );
    output.WPos = output.Pos.xyz;
    output.Pos = mul( output.Pos, g_mViewProjection );

	// Scale in xy, offset in zw
    output.Tex = input.Tex;

	// Get world space normal
	output.Normal = mul( input.Norm, (float3x3)g_mWorld );

    return output;
}

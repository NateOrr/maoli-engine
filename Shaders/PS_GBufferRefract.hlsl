//--------------------------------------------------------------------------------------
// File: PS_GBufferCube.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <MaterialHelper.hlsli>
#include <Deferred.hlsli>
#include <Shading.hlsli>


// Pixel shader for GBuffer pass with cube mapping
DL_Output PS_GBufferRefract( PS_INPUT_GBUFFER input )
{
	// Init the output struct
	DL_Output oBuf;

	// Shading normal and polygon normal
	float3 normal = GetNormal( input.Tex, normalize( input.Norm ), input.WPos );
	float3 flatNormal = normalize( cross( ddx_fine( input.WPos ), ddy_fine( input.WPos ) ) );
	oBuf.Normal = EncodeNormal( normal );
	oBuf.FlatNormal = EncodeNormal( flatNormal );

	// Base color
	float3 v = normalize( input.WPos - g_CameraPos );
	float4 baseColor = GetMaterialColor( input.Tex );
	float3 r = refract( v, normal, 0.9f );
	float4 refraction = g_txCubeProbe.Sample( g_samAnisotropic, r );
	oBuf.Color = refraction;

	// Material properties
	oBuf.Properties[MAT_ROUGHNESS] = g_MaterialRoughness;
	oBuf.Properties[MAT_METALIC] = g_MaterialMetalic;
	oBuf.ObjectID = g_ObjectID;
	oBuf.Emissive = g_MaterialEmissive;

	return oBuf;
}

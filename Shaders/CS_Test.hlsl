//--------------------------------------------------------------------------------------
// File: CS_LightCUll.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Deferred.hlsli>


// Framebuffer UAV
RWTexture2D<float4> g_Framebuffer : register(u3);


// Pack and unpack two <=16-bit coordinates into a single uint
uint PackCoords(uint2 coords)
{
    return coords.y << 16 | coords.x;
}

uint2 PackRGBA16(float4 c)
{
    return f32tof16(c.rg) | (f32tof16(c.ba) << 16);
}

#define BLOCK_SIZE 16
#define GROUP_SIZE (BLOCK_SIZE*BLOCK_SIZE)

// Compute the min and max depth to construct the tile frusta

[numthreads( BLOCK_SIZE, BLOCK_SIZE, 1 )]
void CS_Test(
	uint3 groupId : SV_GroupID,
	uint3 groupThreadId : SV_GroupThreadID,
	//uint groupIndex : SV_GroupIndex,
	uint3 threadId : SV_DispatchThreadID)
{
	// NOTE: This is currently necessary rather than just using SV_GroupIndex to work
    // around a compiler bug on Fermi.
    uint groupIndex = groupThreadId.y * BLOCK_SIZE + groupThreadId.x;

	float x = (float)threadId.x / g_ScreenSize.x;
	float y = (float)threadId.y / g_ScreenSize.y;
	g_Framebuffer[threadId.xy] = float4(0, 1, 0, 0);
}

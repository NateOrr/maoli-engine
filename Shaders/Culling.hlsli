//--------------------------------------------------------------------------------------
// File: Culling.hlsli
//
// Furstum culling
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _CULLING_HLSLI_
#define _CULLING_HLSLI_

RWStructuredBuffer<LightIndexData> g_IndexDataRW;
RWStructuredBuffer<uint> g_ClusterIndicesRW;
RWStructuredBuffer<uint> g_IndexOffset;

// Compute the AABB of a sphere in screen space
void GetSphereScreenSpaceAABB( float3 viewPos, float radius, out uint2 aabbMin, out uint2 aabbMax )
{
	// Get view space bounds
	float3 top = viewPos + float3(0, radius, 0);
	float3 bottom = viewPos + float3(0, -radius, 0);
	float3 left = viewPos + float3(-radius, 0, 0);
	float3 right = viewPos + float3(radius, 0, 0);

	// Figure out which diagonal to use
	left.z = left.x < 0.0f ? left.z - radius : left.z + radius;
	right.z = right.x < 0.0f ? right.z + radius : right.z - radius;
	top.z = top.y < 0.0f ? top.z + radius : top.z - radius;
	bottom.z = bottom.y < 0.0f ? bottom.z - radius : bottom.z + radius;

	// Clamp z to the clip planes
	left.z = clamp( left.z, g_NearZ, g_FarZ );
	right.z = clamp( right.z, g_NearZ, g_FarZ );
	top.z = clamp( top.z, g_NearZ, g_FarZ );
	bottom.z = clamp( bottom.z, g_NearZ, g_FarZ );

	// Apply perspective transform
	float rLeft = left.x * g_mProj[0][0] / left.z;
	float rRight = right.x * g_mProj[0][0] / right.z;
	float rTop = top.y * g_mProj[1][1] / top.z;
	float rBottom = bottom.y * g_mProj[1][1] / bottom.z;

	// Clamp to viewport
	rLeft = clamp( rLeft, -1.0f, 1.0f );
	rRight = clamp( rRight, -1.0f, 1.0f );
	rTop = clamp( rTop, -1.0f, 1.0f );
	rBottom = clamp( rBottom, -1.0f, 1.0f );

	// Convert to screen coords
	rLeft = rLeft * 0.5f + 0.5f;
	rRight = rRight * 0.5f + 0.5f;
	rTop = rTop * 0.5f + 0.5f;
	rBottom = rBottom * 0.5f + 0.5f;
	rTop = 1.0f - rTop;
	rBottom = 1.0f - rBottom;

	// Output
	aabbMin.x = uint(rLeft * g_ScreenSize.x);
	aabbMin.y = uint(rTop * g_ScreenSize.y);
	aabbMax.x = uint(rRight * g_ScreenSize.x);
	aabbMax.y = uint(rBottom * g_ScreenSize.y);
}

// Get the extents of a light across the clusters
bool GetLightClusterRange(Light light, out int3 min, out int3 max)
{
	// Move the light into view space and get camera distance
	const float3 viewPos = mul( float4(light.position, 1), g_mView ).xyz;
	float viewDist = length( viewPos );
	bool camInLight = viewDist < light.range;
	bool inCam = true;
	if ( viewPos.z < 0 && !camInLight )
	{
		inCam = false;
		min = max = 0;
	}
	else
	{
		// Get the depth range
		float minDepth = viewDist - light.range;
		float maxDepth = viewDist + light.range;
		minDepth = saturate( NormalizeDepth( minDepth ) );
		maxDepth = saturate( NormalizeDepth( maxDepth ) );
		min.z = int( minDepth * g_NumSlices );
		max.z = int( maxDepth * g_NumSlices );

		// Get screen space xy bounds to find the tiles it belongs to
		[branch]
		if ( !camInLight )
		{
			// Get the screen space bounds of the light
			uint2 lightMin, lightMax;
			GetSphereScreenSpaceAABB( viewPos, light.range, lightMin, lightMax );

			// Get the tile range
			min.x = lightMin.x / g_TileSize;
			min.y = lightMin.y / g_TileSize;
			max.x = lightMax.x / g_TileSize;
			max.y = lightMax.y / g_TileSize;
		}
		else
		{
			// Inside the light volume, all tiles are always affected
			min.x = min.y = 0;
			max.x = g_TileWidth - 1;
			max.y = g_TileHeight - 1;
		}
	}

	return inCam;
}

#endif
//--------------------------------------------------------------------------------------
// File: VS_Default.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Std projection with tex coords
PS_INPUT_DEFAULT VS_Default( VS_INPUT input )
{
    PS_INPUT_DEFAULT output;
    output.Pos = mul( input.Pos, g_mWorld );
    output.WPos = output.Pos.xyz;
    output.Pos = mul( output.Pos, g_mViewProjection ); 
    output.Tex = input.Tex; 
    return output;
}

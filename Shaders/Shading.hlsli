//--------------------------------------------------------------------------------------
// File: Shading.fxh
//
// Support for different lighting models
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _SHADING_HLSLI_
#define _SHADING_HLSLI_

#include <ShadowMap.hlsli>
#include <Atmosphere.hlsli>
#include <ImageBasedLighting.hlsli>
#include <Partition.hlsli>

#define EPSILON 0.0001f

// Light properties
struct Light
{
	float4	color;
	float3	position;
	float	range;
};

// Light properties
struct SpotLight : Light
{
	float3	direction;
	float	innerRadius;
	float	outerRadius;	
};

// Light
cbuffer cbLight : register(b2)
{
	float3 g_SunDirection;
	float3 g_SunColor;
	uint g_NumSpotLights;
	uint g_NumPointLights;
	uint g_NumShadowSpotLights;
	uint g_NumShadowPointLights;
};

// Cluster index data
struct LightIndexData
{
	uint start;
	uint numShadowSpotLights;
	uint numShadowPointLights;
	uint numSpotLights;
	uint numPointLights;
};

// Global light list
StructuredBuffer<SpotLight>	g_SpotLights;
StructuredBuffer<Light> g_PointLights;
StructuredBuffer<LightIndexData> g_ClusterIndexData;
StructuredBuffer<uint> g_ClusterIndices;


// Compute the Schlick approximation of the Fresnel term
float3 Fresnel(float3 specularColor, float3 l, float3 n)
{
	const float NoL = saturate( dot(l, n) );
	//const float p = pow(1-NoL, 5);
	const float p = pow(2, (-5.55473f*NoL-6.98316f)*NoL);
	return specularColor + (1-specularColor)*p;
}

// Compute the specular BRDF
// General Cook-Torrence microfacet model
float3 SpecularBRDF(float3 specularColor, float3 l, float3 v, float3 n, float3 h, float a, float k)
{
	// Dots
	const float NoH = saturate( dot(n, h) );
	const float HoL = saturate( dot(h, l) );
	const float HoV = saturate( dot(h, v) );
	const float NoL = saturate( dot(n, l) );
	const float NoV = saturate( dot(n, v) );

	if(dot(n, l) < 0.0f)
		return 0.0f;

	// D
	const float a2 = a*a;
	const float denomD = ((NoH*NoH)*(a2-1)+1);
	const float D = a2 / (denomD*denomD);

	// G
	const float GL = HoL / (HoL*(1-k) + k);
	const float GV = HoV / (HoV*(1-k) + k);
	const float G = GL*GV;

	// F
	const float p = pow(2, (-5.55473f*NoL-6.98316f)*NoL);
	const float3 F = specularColor + (1-specularColor)*p;

	return F*D*G;
}

// Helper for computing the GGX visibility term
float GGX_V1( in float m2, in float nDotX )
{
	return 1.0f / (nDotX + sqrt( m2 + (1 - m2) * nDotX * nDotX ));
}

// Computes the specular term using a GGX microfacet distribution. m is roughness, n is the surface normal, h is the 
// half vector, and l is the direction to the light source
float GGX_Specular( in float m, in float3 n, in float3 h, in float3 v, in float3 l )
{
	float nDotH = saturate( dot( n, h ) );
	float nDotL = saturate( dot( n, l ) );
	float nDotV = saturate( dot( n, v ) );
	float nDotH2 = nDotH * nDotH;
	float m2 = m * m;
	// Calculate the distribution term
	const float Pi = 3.1415926f;
	float d = m2 / (Pi * pow( nDotH * nDotH * (m2 - 1) + 1, 2.0f ));
	// Calculate the matching visibility term
	float v1i = GGX_V1( m2, nDotL );
	float v1o = GGX_V1( m2, nDotV );
	float vis = v1i * v1o;
	// Multiply this result with the Fresnel term
	return d * vis;
}


// Blinn-phong
float3 BlinnPhong(float3 specularColor, float3 n, float3 h, float power)
{
	float NoH = saturate( dot(n, h) );
	return specularColor * pow(NoH, power);
}

// Compute the light vector, returning the distance to the light
float GetLightVector( float3 lightPos, float3 p, out float3 lightVector )
{
	lightVector = lightPos - p;
	float d = length( lightVector );
	lightVector *= rcp( d );
	return d;
}

// Get the attenuation
float ApplyAttenuation( float range, float d )
{
	return saturate( (range - d) / d );
}

// Spot light factor
float ApplySpotFactor( SpotLight light, float3 l )
{
	return smoothstep( light.outerRadius, light.innerRadius, dot( -light.direction, l ));
}

// Apply shading
float3 ApplyLighting( float3 n, float3 l, float3 p, float3 lightColor, float3 baseColor, float roughness, float metalic )
{
	// Compute basic diffuse
	float nDotL = saturate( dot( n, l ) );
	if ( nDotL < EPSILON )
		return 0.0f;
	float3 diffuse = nDotL * lightColor * baseColor;
	if ( nDotL < EPSILON )
		return 0.0f;

	// Compute the half vector
	float3 v = normalize( g_CameraPos - p );
		float3 h = normalize( l + v );

		//// Compute specular component
		//float nDotH = saturate( dot( n, h ) );
		//float nDotV = max( dot( n, v ), 0.0001f );
		//float nDotH2 = nDotH * nDotH;
		//float m2 = roughness*roughness;

		//// Calculate the distribution term
		//float d = m2 / (pi * pow( nDotH * nDotH * (m2 - 1) + 1, 2.0f ));

		//// Calculate the matching visibility term
		//float v1i = GGX_V1( m2, nDotL );
		//float v1o = GGX_V1( m2, nDotV );
		//float vis = v1i * v1o;

		//// Calculate the fresnel term
		//float3 f = Fresnel( lightColor, h, l );

		//// Put it all together
		//float3 specular =  d * f * vis;
		float3 specular = BlinnPhong( lightColor, n, h, 16.0f );

	//float3 specular = lightColor * GGX_Specular( roughness, n, h, v, l );

	// Final color is a linear blend between metalic and dielectric
	// Metalic has no diffuse, and a tinted specular
	float3 metal = specular*baseColor;
	float3 dielectric = diffuse;
	return lerp( dielectric, metal, metalic );
}


// Compute the full lighting equation for point lights, with shadowing
float3 ComputePointLightingWithShadow( Light light, float3 baseColor, float3 n, float3 p, float roughness, float metalic, uint shadowIndex )
{
	// Compute the light vector and attenuation
	float3 l = light.position - p;
	float d = length( l );
	float attenuation = ApplyAttenuation( light.range, d );
	if ( attenuation < EPSILON )
		return 0;

	// Shadow mapping
	float zNear = 0.05f;
	float zFar = light.range;
	float dz = rcp(zFar - zNear);
	float clipRatio1 = zFar * dz;
	float clipRatio2 = -zNear*zFar * dz;
	float z = max( max( abs( l.x ), abs( l.y ) ), abs( l.z ) );
	float depth = saturate( (clipRatio1 * z + clipRatio2) / z );
	attenuation *= ShadowContributionPoint( -l, depth, shadowIndex );
	if ( attenuation < EPSILON )
		return 0;

	// Final lighting
	l *= rcp( d );
	return ApplyLighting( n, l, p, light.color.rgb, baseColor, roughness, metalic ) * attenuation;
}

// Compute the full lighting equation for point lights
float3 ComputePointLighting( Light light, float3 baseColor, float3 n, float3 p, float roughness, float metalic )
{
	// Compute the light vector and attenuation
	float3 l;
	float d = GetLightVector( light.position, p, l );
	float attenuation = ApplyAttenuation( light.range, d );

	// Final lighting
	return ApplyLighting( n, l, p, light.color.rgb, baseColor, roughness, metalic ) * attenuation;
}

// Compute the full lighting equation
float3 ComputeSpotLightingWithShadow(SpotLight light, float3 baseColor, float3 n, float3 p, float roughness, float metalic, uint shadowIndex)
{
	// Compute the light vector and attenuation
	float3 l;
	float d = GetLightVector( light.position, p, l );
	float attenuation = ApplyAttenuation( light.range, d );
	
	// Spot factor
	attenuation *= ApplySpotFactor( light, l );

	// Shadow mapping
	float4 positionLight = mul( float4(p, 1.0f), g_ShadowMatrices[shadowIndex] );
	float2 texCoord = (positionLight.xy / positionLight.w) * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
	//float depth = GetLinearDepth( positionLight.z / positionLight.w, g_ShadowProjRatio );
	float depth = positionLight.z / positionLight.w;
	attenuation *= ShadowContributionSpot( texCoord, depth, shadowIndex );
	if ( attenuation < EPSILON )
		return 0;

	// Final lighting
	return ApplyLighting( n, l, p, light.color.rgb, baseColor, roughness, metalic ) * attenuation;
}


// Compute the full lighting equation
float3 ComputeSpotLighting( SpotLight light, float3 baseColor, float3 n, float3 p, float roughness, float metalic )
{
	// Compute the light vector and attenuation
	float3 l;
	float d = GetLightVector( light.position, p, l );
	float attenuation = ApplyAttenuation( light.range, d );

	// Spot factor
	attenuation *= ApplySpotFactor( light, l );

	// Final lighting
	return ApplyLighting( n, l, p, light.color.rgb, baseColor, roughness, metalic ) * attenuation;
}

// Compute the full lighting equation with SDSM shadows
float3 ComputeDirectionalLightingWithShadow( float3 lightDir, float3 lightColor, float3 baseColor, float3 n, float3 p, float roughness, float metalic, float3 viewPos,
	Partition partition, uint partitionIndex )
{
	// Shadow mapping
	float4 positionLight = mul( float4(p, 1.0f), g_mShadow );
	float3 texCoord = (positionLight.xyz / positionLight.w) * float3(0.5f, -0.5f, 1.0f) + float3(0.5f, 0.5f, 0.0f);
	texCoord = texCoord * partition.scale.xyz + partition.bias.xyz;
	float depth = clamp( texCoord.z, 0.0f, 1.0f );
	float shadow = ShadowContributionSDSM( texCoord.xy, depth, partitionIndex, partition );
	if ( shadow < EPSILON )
		return 0;

	// Final lighting
	return ApplyLighting( n, -lightDir, p, lightColor, baseColor, roughness, metalic ) * shadow;
}

// Compute the full lighting equation without shadows
float3 ComputeDirectionalLighting( float3 lightDir, float3 lightColor, float3 baseColor, float3 n, float3 p, float roughness, float metalic )
{
	return ApplyLighting( n, -lightDir, p, lightColor, baseColor, roughness, metalic );
}

#endif

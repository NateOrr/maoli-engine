//--------------------------------------------------------------------------------------
// File: PS_ComputeScattering.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Atmosphere.hlsli>


// Precompute an atmospheric scattering texture
void PS_ComputeScattering( PS_INPUT_TEX input, out float4 oRayleigh : SV_Target0, out float4 oMie : SV_Target1 )
{
	float2 Tex0 = input.Pos.xy * InvRayleighMieN.xy;
	 
	const float3 v3PointPv = float3( 0, InnerRadius, 0 );
	//const float dDistribution = (1.0 - exp(-0.5 * 10.0 * input.Tex.x ));
	//const float AngleY = 100.0 * dDistribution * PI / 180.0 ;
	const float AngleY = 100.0 * Tex0.x * PI / 180.0;
	const float AngleXZ = PI * Tex0.x;
	
	float3 v3Dir;
	v3Dir.x = sin( AngleY ) * cos( AngleXZ  );
	v3Dir.y = cos( AngleY );
	v3Dir.z = sin( AngleY ) * sin( AngleXZ  );
	v3Dir = normalize( v3Dir );

	// Pv -> Pa 
	float fFarPvPa = HitOuterSphere( v3PointPv , v3Dir );
	float3 v3Ray = v3Dir;

	// In Scattering
	float3 v3PointP = v3PointPv;
	float fSampleLength = fFarPvPa / iNumSamples;
	float fScaledLength = fSampleLength * fScale;
	float3 v3SampleRay = v3Ray * fSampleLength;
	v3PointP += v3SampleRay * 0.5f;
				
	float3 v3RayleighSum = 0;
	float3 v3MieSum = 0;
	
	[unroll(20)]
	for( int k = 0; k < iNumSamples; k++ )
	{
		float PointPHeight = length( v3PointP );

		// Density Ratio at Point P
		float2 DensityRatio = GetDensityRatio( PointPHeight );
		DensityRatio *= fScaledLength;

		// P->Viewer OpticalDepth
		float2 ViewerOpticalDepth = t( v3PointP, v3PointPv );
						
		// P->Sun OpticalDepth
		float dFarPPc = HitOuterSphere( v3PointP, g_SunDir );
		float2 SunOpticalDepth = t( v3PointP, v3PointP + g_SunDir * dFarPPc );

		// Calculate the attenuation factor for the sample ray
		float2 OpticalDepthP = SunOpticalDepth.xy + ViewerOpticalDepth.xy;
		float3 v3Attenuation = exp( - Kr4PI * InvWavelength4 * OpticalDepthP.x - Km4PI * OpticalDepthP.y );

		v3RayleighSum += DensityRatio.x * v3Attenuation;
		v3MieSum += DensityRatio.y * v3Attenuation;

		// Move the position to the center of the next sample ray
		v3PointP += v3SampleRay;
	}

	float3 Rayleigh = v3RayleighSum * KrESun;
	float3 Mie = v3MieSum * KmESun;
	Rayleigh *= InvWavelength4;
	Mie *= WavelengthMie;
	
	oRayleigh = float4( Rayleigh, 1 );
	oMie = float4( Mie, 1 );
}

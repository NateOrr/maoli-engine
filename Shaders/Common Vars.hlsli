//--------------------------------------------------------------------------------------
// File: Common Vars.fxh
//
// Contains all global variables and definitions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _COMMONVARS_HLSLI_
#define _COMMONVARS_HLSLI_

/////////////////////////////////
// Global constants
#define g_Epsilon 0.0003f
static const float pi = 3.14159265359f;

// Light types
#define LIGHT_DIRECTIONAL 0
#define LIGHT_SPOT 1
#define LIGHT_POINT 2

// Texture types
#define TEX_DIFFUSE 0
#define TEX_NORMAL 1
#define TEX_SPECULAR 2
#define TEX_HEIGHT 3
#define TEX_ALPHA 4
#define TEX_EMISSIVE 5
#define TEX_SIZE 6


// Per-object vars
cbuffer cbPerObject : register(b4)
{
	// Transforms
	matrix g_mWorld;
	uint g_ObjectID;
};

// Updated once per frame FrameMove
cbuffer cbPerFrame : register(b0)
{
	matrix g_mViewProjection;
	matrix g_mOldViewProjection;
	matrix g_mView;
	matrix g_mInvViewProjection;

	// Basis for position reconstruction world space
	float3 g_ScreenToWorldX;
	float3 g_ScreenToWorldY;
	float3 g_ScreenToWorldZ;

	// Elapsed time since last frame
	float g_ElapsedTime;
	float g_Timer;

	// Field of view
	float g_FoV;

	// Reflection matrix
	matrix g_mMirror;
};

// Camera
cbuffer cbCamera
{
	float3 g_CameraPos;
	float3 g_CameraDir;
};


// Material
cbuffer cbMaterial : register(b1)
{
	// Material Properties
	float4 g_MaterialColor;
	float4 g_MaterialEmissive;
	float  g_MaterialRoughness;
	float  g_MaterialMetalic;
	int    g_MaterialTextureFlag;	// Bitfield
};


// Shadow map pass
cbuffer cbShadowMap : register(b6)
{
	matrix g_mShadow;
	float4 g_TextureSize;
	//float2 g_ShadowRange;
	//float2 g_ShadowProjRatio;
};


// Variables only modified on resize
cbuffer cbChangesOnResize : register(b3)
{
	// Screen size
	float4 g_ScreenSize;  //(xy is size, zw is 1/size)
	float g_FarZ;
	float g_NearZ;
	float2 g_ProjRatio; // float2 ( zfar /( zfar - znear ), znear /( znear - zfar ));
	matrix g_mProj;

	// Scene ambient color
	float4 g_Ambient;

	// Virtual texture
	int g_VTPageSize;
	int g_VTPageTableSize;
	int g_VTMaxPage;
	int g_VTTexSize;
	int g_VTPhysTexSize;
	float g_VTMaxMipLevel;

	// Clustered shading
	uint g_TileSize;
	uint g_TileWidth;
	uint g_TileHeight;
	uint g_NumSlices;
	uint g_MaxLightsPerCluster;
	uint g_NumClusters;
};



// Textures

// Material textures
Texture2D g_txMaterial[TEX_SIZE];

// Sky cubemap
TextureCube g_txSkyCube;

// Random noise texture
Texture2D g_txRandom;

// Environment probe cubemap
TextureCube g_txCubeProbe;

// Light accum. buffer
Texture2D g_txLightBuffer[2];

// Framebuffer texture
Texture2D g_txFrameBuffer;

// Animation
StructuredBuffer<float4x4> g_BonePalette;




// Virtual Texture vars
#define VT_DIFFUSE 0
#define VT_NORMAL 1
#define VT_SPECULAR 2
#define VT_SLOT_SIZE 3

// Page table slot data, used by the shader
struct PageTableSlot
{
	int2 offset;
	bool exists;
};

// Each entry has 3 slots currently
struct PageTableEntry
{
	PageTableSlot slot[VT_SLOT_SIZE];
	int mip;
	bool forceMip;
};
StructuredBuffer<PageTableEntry> g_PageTable;

Texture2D g_txVirtual;	// The physical texture constructed from the virtual texture pages		
StructuredBuffer<int> g_VTMipOffsets;
RWStructuredBuffer<int> g_PageLoadList : register(u6);


// Samplers

// Point filter
SamplerState g_samPoint;
SamplerState g_samPointWrap;

// Linear filter
SamplerState g_samLinear;
SamplerComparisonState g_samPCF;

// Linear filter
SamplerState g_samLinearVT;

// Basic bilinear sampling
SamplerState g_samBilinear;

// Anisotropic filter
SamplerState g_samAnisotropic;

SamplerState g_samAnisotropicClamp;


#endif

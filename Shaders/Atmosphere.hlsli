//--------------------------------------------------------------------------------------
// File: Sky.fxh
//
// Support for skydome
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _SKY_HLSLI_
#define _SKY_HLSLI_

cbuffer cbSky : register(b7)
{
	float PI = 3.141592653;
	float InnerRadius = 6356752.3142;
	float OuterRadius = 6356752.3142 * 1.015;
	float fScale = 1.0 / (6452.103598913 - 6356.7523142);
	float KrESun = 0.0025 * 30.0;
	float KmESun = 0.0010 * 30.0;
	float Kr4PI = 0.0025 * 4.0 * 3.141592653;
	float Km4PI = 0.0010 * 4.0 * 3.141592653;
	int tNumSamples = 50;
	int iNumSamples = 20;
	float2 v2dRayleighMieScaleHeight= float2( 0.25, 0.1 );
	float3 WavelengthMie = float3( pow( 0.650, -0.84 ), pow( 0.570, -0.84 ), pow( 0.475, -0.84 ) );
	float InvOpticalDepthN = 1.0 / 128.0;
	float3 v3HG = float3( 1.5f * ( (1.0f - (-0.995*-0.995)) / (2.0f + (-0.995*-0.995)) ), 1.0f + (-0.995*-0.995), 2.0f * -0.995 );
	float InvOpticalDepthNLessOne = 1.0 / ( 128.0 - 1.0 );
	float2 InvRayleighMieN = float2( 1.0 / 128.0, 1.0 / 64.0 );
	float2 InvRayleighMieNLessOne = float2( 1.0 / (128.0 - 1.0), 1.0 / (64.0 - 1.0) );
	float HalfTexelOpticalDepthN = float( 0.5 / 128.0 ); 
	float3 InvWavelength4 = float3( 1.0 / pow( 0.650, 4 ), 1.0 / pow( 0.570, 4 ), 1.0 / pow( 0.475, 4 ) );
}

cbuffer cbSun : register(b12)
{
	float3 g_SunDir;
}

// Rayleigh / Mie scattering textures
#define SCATTER_RAYLEIGH 0
#define SCATTER_MIE 1
Texture2D g_txScattering[2];


// Atmospheric Scattering
float3 _HDR( float3 LDR)
{
	return 1.0f - exp( -2.0f * LDR );
}

float3 ToneMap( float3 HDR)
{
	return (HDR / (HDR + 1.0f));
}

float getMiePhase(float fCos, float fCos2)
{
	return v3HG.x * (1.0 + fCos2) / pow(max(0,v3HG.y - v3HG.z * fCos), 1.5);
}

float getRayleighPhase(float fCos2)
{
	return 0.75 * (1.0 + fCos2);
}

float2 GetDensityRatio( float fHeight )
{
	const float fAltitude = (fHeight - InnerRadius) * fScale;
	return exp( -fAltitude / v2dRayleighMieScaleHeight.xy );
}

float HitOuterSphere( float3 O, float3 Dir ) 
{
	float3 L = -O;

	float B = dot( L, Dir );
	float C = dot( L, L );
	float D = C - B * B; 
	float q = sqrt( OuterRadius * OuterRadius - D );
	return B+q;
}


float RayleighPhase(float cosTheta)
{
	return 1.0f + 0.5f*cosTheta*cosTheta;
}

// Computes the scattering intensity by solving the optical depth integral
// float GetScatteredIntensity(float lambda, float3 viewPos, float3 viewDir, float3 scatteringPoint)
// {
// 	const float atmosphereTurbidity = 2.0f;
// 	const float rayleighAngularCoefficient = RayleighPhase(0);
// 	
// 	inScattering = atmosphereTurbidity * (rayleighAngularCoefficient * moleculeDensity + mieAngularCoefficient * aerosolDensity);
// 	
// 	return 1;
// }


float2 t( float3 P, float3 Px )
{
	// Out Scattering Integral / OpticalDepth
	float2 OpticalDepth = 0;

	float3 v3Vector =  Px - P;
	float fFar = length( v3Vector );
	float3 v3Dir = v3Vector / fFar;
			
	float fSampleLength = fFar / tNumSamples;
	float fScaledLength = fSampleLength * fScale;
	float3 v3SampleRay = v3Dir * fSampleLength;
	P += v3SampleRay * 0.5f;
			
	for(int i = 0; i < tNumSamples; i++)
	{
		float fHeight = length( P );
		OpticalDepth += GetDensityRatio( fHeight );
		P += v3SampleRay;
	}		

	OpticalDepth *= fScaledLength;
	return OpticalDepth;
}

struct PS_INPUT_SKY
{
    float4 Pos : SV_POSITION;
    float2 Tex0 : TEXCOORD0;
	float3 Tex1 : TEXCOORD1;
	float3 View : TEXCOORD2;
};

#endif

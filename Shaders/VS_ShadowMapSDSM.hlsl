//--------------------------------------------------------------------------------------
// File: VS_ShadowMapSDSM.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <Common Inputs.hlsli>
#include <Common Vars.hlsli>
#include <ShadowMap.hlsli>
#include <Partition.hlsli>

// Vertex shader for SDSM shadow map rendering
void VS_ShadowMapSDSM( VS_INPUT_POS input, out float4 oPosition : SV_Position )
{
	// todo: optimize to one matrix mult
	oPosition = mul(input.Pos, g_mShadow);
    
    // Scale/bias NDC space
    Partition partition = g_PartitionsReadOnly[g_PartitionID];
    oPosition.xy *= partition.scale.xy;
    oPosition.x += (2.0f * partition.bias.x + partition.scale.x - 1.0f);
    oPosition.y -= (2.0f * partition.bias.y + partition.scale.y - 1.0f);
    
    // Clamp to [0, 1] happens the viewport transform
    oPosition.z = oPosition.z * partition.scale.z + partition.bias.z;
}
    
    

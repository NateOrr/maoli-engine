//--------------------------------------------------------------------------------------
// File: HDR.fxh
//
// HDRI Processing
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _HDR_HLSLI_
#define _HDR_HLSLI_

#define DOWNSAMPLE_SCALE 0.0625		// Squared scale factor for the downsampled image (eg 1/4 size : 0.25*0.25=0.0625)

Texture2D g_txHDR;							// HDR scene texture
Texture2D<float2> g_txLuminance;			// Luminance texture
Texture2D<float2> g_txAdaptedLuminance;		// Final luminance texture

//const float3 g_LuminanceVector = float3(0.2125f, 0.7154f, 0.0721f);
static const float3 g_LuminanceVector = float3(0.3333f, 0.3333f, 0.3333f);
static const float g_MaxLuminance = 1.00f;
static const float g_LumMinP=0.01f;
static const float g_LumAvgP=0.6f;
static const float g_LumMaxP=0.99f;
static const float g_MiddleGray = 0.38f;

// HDR downscale input
struct PS_INPUT_DOWNSCALE
{
	float4	 Pos		: SV_POSITION;
	float4x2 TexSamples : TEXCOORD0;
	float2   Tex		: TEXCOORD2;
};

// Tonemapping shader input
struct PS_INPUT_TONEMAP
{
	float4	 Pos		: SV_POSITION;
	float4x2 TexSamples : TEXCOORD0;
	float2   Tex		: TEXCOORD2;
	float2   Lum		: TEXCOORD3;
};


// Map the pixe; luminance based on the scene's
float MapLuminance(float Lw, float2 lumSample)
{
	float Ld = 0.0f;
	if(Lw>0.0f)
	{
		float L = Lw;
		L *= g_MiddleGray / (lumSample.r+0.001f);
		Ld = L * (1.0f + L / (lumSample.g*lumSample.g)) / (1.0f + L);
	}
	return clamp(Ld/Lw, 0.7f, 1.3f);	
}


#endif

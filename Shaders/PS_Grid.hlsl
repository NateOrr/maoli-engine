//--------------------------------------------------------------------------------------
// File: PS_Grid.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Deferred.hlsli>


// Pixel Shader for rendering the terrain widget
float4 PS_Grid( PS_INPUT_VIEW input ) : SV_Target
{
	float depth = DL_GetDepth( input.Pos.xy );
	if ( depth > g_FarZ*0.9 )
		discard;

	// Reconstruct the world space position using the depth buffer value
	float3 wPos = ReconstructPosition( input.Pos.xy, depth );
	
	// Grid distance factor
	uint scale = 1U;
	float thickness = 0.25f;
	float4 color = float4(1, 0, 0, 0.8f);
	float dist = distance(wPos, g_CameraPos);
	if(dist<100)
	{
		scale = 10U;
		color = float4(1, 0, 1, 0.8f);
		thickness = 0.1f;
	}
	else
	{
		scale = 100U;
		color = float4(0, 0, 1, 0.8f);
		thickness = 0.4f;
	}
	
	// Grid lines to show scale
	float2 grid = wPos.xz-floor(wPos.xz);
	if( grid.x<=thickness || grid.y<=thickness )
	{
		int wx = (int)abs(floor(wPos.x));
		int wz = (int)abs(floor(wPos.z));
		if( (wx%scale == 0 || wz%scale == 0) )
			return color;
	}
	
	discard;
	return 0;
}

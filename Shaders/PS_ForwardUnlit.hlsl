//--------------------------------------------------------------------------------------
// File: PS_ForwardUnlit.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <MaterialHelper.hlsli>


// Pixel shader for forward rendering with no lighting
float4 PS_ForwardUnlit( PS_INPUT_TEX input ) : SV_Target
{
	// Perform alpha testing
	AlphaTest(input.Tex);
		
	// Simple texturing
	return GetMaterialColor(input.Tex);
}

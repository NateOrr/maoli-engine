//--------------------------------------------------------------------------------------
// File: VS_GBuffer.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shader for GBuffer pass
PS_INPUT_GBUFFER VS_GBuffer( VS_INPUT input )
{
    PS_INPUT_GBUFFER output;

    output.Pos = mul( input.Pos, g_mWorld );
    output.WPos = output.Pos.xyz;
    output.Pos = mul( output.Pos, g_mViewProjection );
    output.Tex = input.Tex;
	output.Norm = mul( input.Norm, (float3x3)g_mWorld );

	return output;
}

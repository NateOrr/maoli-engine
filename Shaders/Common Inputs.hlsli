#ifndef _COMMONINPUTS_HLSLI_
#define _COMMONINPUTS_HLSLI_

// Shader I/O Structures

// Standard vertex
struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD;
    float3 Norm: NORMAL;
};

// Animated vertex
struct VS_INPUT_SKIN
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD;
    float3 Norm: NORMAL;
	float4 Weights: WEIGHTS;
	uint4 Bones : BONES;
};

// Animated vertex
struct VS_INPUT_SKIN_POS
{
    float4 Pos : POSITION;
	float4 Weights: WEIGHTS;
	uint4 Bones : BONES;
};

// Standard vertex with instancing
struct VS_INPUT_INSTANCED
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD;
    float3 Norm: NORMAL;

	matrix WorldMatrix : WorldMatrix;
};

// Position vertex with instancing
struct VS_INPUT_POS_INSTANCED
{
	float4 Pos : POSITION;

	matrix WorldMatrix : WorldMatrix;
};

// Position / texture vertex with instancing
struct VS_INPUT_TEX_INSTANCED
{
	float4 Pos : POSITION;
	float2 Tex : TEXCOORD;

	matrix WorldMatrix : WorldMatrix;
};

// vertex with position
struct VS_INPUT_POS
{
    float4 Pos : POSITION;
};

// vertex with pos and uv coords
struct VS_INPUT_POS_TEX
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD;
};


struct PS_INPUT
{
	float4 Pos : SV_POSITION;
};


struct PS_INPUT_TEX
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
};

struct PS_INPUT_DEFAULT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 WPos: TEXCOORD1;
};

struct PS_INPUT_VIEW
{
	float4 Pos  : SV_POSITION;
	float3 vPos : TEXCOORD0;
};

struct PS_INPUT_POSTFX
{
	float4 Pos		: SV_POSITION;
	float3 ViewDir  : TEXCOORD0;
};

struct PS_INPUT_VIEW_TEX
{
	float4 Pos  : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 View : TEXCOORD1;
};

// Basic instancing
struct PS_INPUT_INSTANCE
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
    float4 Color : TEXCOORD1;
};

// GBuffer ps input
struct PS_INPUT_GBUFFER
{
    float4 Pos  : SV_POSITION;
    float2 Tex  : TEXCOORD0;
    float3 WPos : TEXCOORD1;
    float3 Norm : TEXCOORD2;
};

// GBuffer ps input
struct PS_INPUT_GBUFFER_VIEW
{
	float4 Pos  : SV_POSITION;
	float2 Tex  : TEXCOORD0;
	float3 WPos : TEXCOORD1;
	float3 Norm : TEXCOORD2;
	float3 View : TEXCOORD3;
};

struct PS_INPUT_SHADE
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
    float3 Norm: TEXCOORD1;
    float3 WPos: TEXCOORD2;
	float3 ViewPos : TEXCOORD3;
};


struct PS_INPUT_SHADE_FULL
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
    float3 Norm: TEXCOORD1;
    float3 WPos: TEXCOORD2;
	float3 Eye: TEXCOORD3;
};

#endif

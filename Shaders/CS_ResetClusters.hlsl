//--------------------------------------------------------------------------------------
// File: CS_ResetClusters.hlsl
//
// Reset cluster data before light culling
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Shading.hlsli>

RWStructuredBuffer<LightIndexData> g_IndexDataRW;
RWStructuredBuffer<uint> g_ClusterIndicesRW;
RWStructuredBuffer<uint> g_IndexOffset;

#define DISPATCH_SIZE 64

[numthreads( DISPATCH_SIZE, 1, 1 )]
void CS_ResetClusters( uint3 threadId : SV_DispatchThreadID )
{
	if ( g_NumClusters > threadId.x )
	{
		// This is the starting index for each cluster inside the global index array
		// It is used to track the offset location when building the light lists
		uint start = g_MaxLightsPerCluster * threadId.x;
		g_IndexOffset[threadId.x] = start;
	
		g_IndexDataRW[threadId.x].start = start;
		g_IndexDataRW[threadId.x].numShadowSpotLights = 0;
		g_IndexDataRW[threadId.x].numShadowPointLights = 0;
		g_IndexDataRW[threadId.x].numSpotLights = 0;
		g_IndexDataRW[threadId.x].numPointLights = 0;
	}
};
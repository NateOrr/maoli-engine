//--------------------------------------------------------------------------------------
// File: VS_ForwardSkinnedLit.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shader for lit forward rendering
PS_INPUT_SHADE VS_ForwardSkinnedLit( VS_INPUT_SKIN input)
{
	PS_INPUT_SHADE output;

	// Perform skinning
	SkinVertexNormal(input, output.Pos, output.Norm);
	
	// Output position and texcoord
	output.Pos = mul( output.Pos, g_mWorld );
	output.WPos = output.Pos.xyz;
	output.ViewPos = mul( output.Pos, g_mView ).xyz;
	output.Pos = mul( output.Pos, g_mViewProjection );
	output.Tex = input.Tex;
	output.Norm = mul( output.Norm, (float3x3)g_mWorld );
	return output;
}

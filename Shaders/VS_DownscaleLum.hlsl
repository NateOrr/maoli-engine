//--------------------------------------------------------------------------------------
// File: VS_DownscaleLum.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <HDR.hlsli>


// Luminance downscale vertex shader
PS_INPUT_DOWNSCALE VS_DownscaleLum( VS_INPUT input )
{
    PS_INPUT_DOWNSCALE output;
    output.Pos = float4(input.Pos.xyz, 1);   
    
    // Compute the pixel sizes
	int2 dim;
	g_txLuminance.GetDimensions(dim.x, dim.y);
	float2 texSize = 1.0f / (float2)dim;
    
    // Texture coords 
    output.TexSamples[0] = input.Tex + float2( texSize.x, 0);
    output.TexSamples[1] = input.Tex + float2(-texSize.x, 0);
    output.TexSamples[2] = input.Tex + float2(0,  texSize.y);
    output.TexSamples[3] = input.Tex + float2(0, -texSize.y);
    output.Tex = input.Tex;
    
    return output;
}

//--------------------------------------------------------------------------------------
// File: PS_ClipmapUpdate.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Terrain.hlsli>

// Pixel shader for clipmap updating
// The coarse level height is stored the G channel
void PS_ClipmapUpdate( PS_INPUT_TEX input, out float oHeight : SV_Target0 )
{
	// Get the scaled texture coords for this vertex
	float2 texScaled;
	texScaled = (input.Tex-0.5f)*g_ClipmapSize*g_ClipmapScale + g_ClipmapOffset - g_ClipmapScale;
	texScaled /= g_HeightmapSize;
	texScaled += 0.5f;
	oHeight = g_txHeightmap.SampleLevel(g_samBilinear, texScaled, 0).r;
}

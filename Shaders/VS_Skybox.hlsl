//--------------------------------------------------------------------------------------
// File: VS_Skybox.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Atmosphere.hlsli>


// Skydome vertex shader
PS_INPUT_POSTFX VS_Skybox( VS_INPUT input)
{
    // Output the position
	PS_INPUT_POSTFX output;
	output.Pos = mul(input.Pos,g_mWorld);
	output.ViewDir = output.Pos.xyz-g_CameraPos;
    output.Pos = mul(output.Pos,g_mViewProjection);
    output.Pos.z = output.Pos.w;
    output.Pos *= 0.8f;
    return output;
}

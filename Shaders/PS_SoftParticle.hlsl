//--------------------------------------------------------------------------------------
// File: PS_SoftParticle.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Particle.hlsli>
#include <Deferred.hlsli>

// Pixel shader for GBuffer pass
float4 PS_SoftParticle( PS_INPUT_PARTICLE input ) : SV_Target0
{
	// Soft blend with the environment
	const float scale = 0.2f;
	float depth = DL_GetDepth( input.Pos.xy );
	depth = RescaleDepth( depth );
	float softness = saturate( (depth - input.Pos.z) * scale );

	return input.Color * g_txParticle.Sample( g_samLinear, input.Tex ) * softness;
}

//--------------------------------------------------------------------------------------
// File: PS_BrightPass.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <HDR.hlsli>
#include <Deferred.hlsli>

// Bright pass sends the brightest pixels to be bloomed
void PS_BrightPass( PS_INPUT_TEX input, out float4 oBloom : SV_Target)
{
	// Compute the luminance for this pixel
	float4 sample = g_txHDR.Sample(g_samPoint, input.Tex);
	float lum = dot( sample.rgb, g_LuminanceVector );
	
	// Sample the scene luminance
	float2 lumScene = g_txAdaptedLuminance.Load(int3(0,0,0));
	
	// If the pixel is very bright, send to the bloom target
	if(lum > g_LumMaxP*lumScene.g)
		oBloom = sample;
	else
		oBloom = 0;
}

//--------------------------------------------------------------------------------------
// File: PS_DarkenScene.hlsl
//
// Perform full scene darkening
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Stranded.hlsli>

float4 PS_DarkenScene(PS_INPUT_TEX input) : SV_Target
{
	//return float4(0, 0, 0, g_DarkenAmount);
	return g_txFrameBuffer.Sample(g_samBilinear, input.Tex) * g_DarkenAmount;
}

//--------------------------------------------------------------------------------------
// File: VS_Forward.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex shader for forward rendering
PS_INPUT_TEX VS_Forward( VS_INPUT input)
{
	// Output position and texcoord
	PS_INPUT_TEX output;
	output.Pos = mul(input.Pos,g_mWorld);
	output.Pos = mul(output.Pos,g_mViewProjection);
	output.Tex = input.Tex;
	return output;
}

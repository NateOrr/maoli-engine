// File: Sprite.hlsli
//
// Alpha blended sprite rendering
// Specific to engine internals, quad with 0,0 -> 1,0 positions
// 
// Nate Orr
//-------------------------------------------------------------------------------------
#ifndef _SPRITE_HLSLI_
#define _SPRITE_HLSLI_

// Sprite ps format
struct PS_INPUT_SPRITE
{
	float4 Pos   : SV_POSITION;
	float2 Tex   : TEXCOORD0;
	float4 Color : TEXCOORD1;
};

// Sprite texture
Texture2D g_txSprite;

#endif

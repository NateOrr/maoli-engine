//--------------------------------------------------------------------------------------
// File: VS_ShadowMapSkinnedSDSM.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <Common Inputs.hlsli>
#include <Common Vars.hlsli>
#include <ShadowMap.hlsli>
#include <Partition.hlsli>
#include <Common Functions.hlsli>

// Vertex shader for SDSM shadow map rendering
void VS_ShadowMapSkinnedSDSM( VS_INPUT_SKIN_POS input, out float4 oPosition : SV_Position )
{
	// Animation
	SkinVertex(input, oPosition);

	// todo: optimize to one matrix mult
	oPosition = mul(oPosition, g_mShadow);
    
    // Scale/bias NDC space
    Partition partition = g_PartitionsReadOnly[g_PartitionID];
    oPosition.xy *= partition.scale.xy;
    oPosition.x += (2.0f * partition.bias.x + partition.scale.x - 1.0f);
    oPosition.y -= (2.0f * partition.bias.y + partition.scale.y - 1.0f);
    
    // Clamp to [0, 1] happens the viewport transform
    // NOTE: Depth clipping is disabled this pass (directional light)
    oPosition.z = oPosition.z * partition.scale.z + partition.bias.z;
}
    
    

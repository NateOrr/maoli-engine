//--------------------------------------------------------------------------------------
// File: VS_PostFXDepth.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shaders that prep for postfx passes that need reconstructed position

// Ortho2D projection
PS_INPUT_POSTFX VS_PostFXDepth( VS_INPUT input )
{
    PS_INPUT_POSTFX output;
    output.ViewDir = input.Norm;
    output.Pos = float4(input.Pos.xyz, 1);    
    return output;
}

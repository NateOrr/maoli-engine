//--------------------------------------------------------------------------------------
// File: VS_ForwardSkinnedUnlit.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shader for unlit forward rendering
PS_INPUT_TEX VS_ForwardSkinnedUnlit( VS_INPUT_SKIN input )
{
	PS_INPUT_TEX output;

	// Perform skinning
	SkinVertex( input, output.Pos);

	// Output position and texcoord
	output.Pos = mul( output.Pos, g_mWorld );
	output.Pos = mul( output.Pos, g_mViewProjection );
	output.Tex = input.Tex;

	return output;
}

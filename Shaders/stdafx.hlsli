//--------------------------------------------------------------------------------------
// File: stdafx.hlsli
//
// Common include files
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _STDAFX_HLSLI_
#define _STDAFX_HLSLI_

// Debug flags
// --------------------------------------------------------------------------------------
#define PHASE_EDITOR

// Virual texture
#define DEBUG_VT_MIPS 0


#include <Common Vars.hlsli>
#include <Common Inputs.hlsli>
#include <Common Functions.hlsli>


#endif

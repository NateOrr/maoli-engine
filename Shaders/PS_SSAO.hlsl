//--------------------------------------------------------------------------------------
// File: PS_SSAO.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Deferred.hlsli>

// Params
static const int rMax = 5;
static const float dMax = 0.6f;

// Project a sphere radius into screen space
int ProjectRadius( float z )
{
	int sr = (g_ScreenSize.x/2 * dMax) / (2 * z * tan( g_FoV*0.5f ));
	return min( sr, rMax );
}

// Falloff for ao
float Falloff( float d )
{
	float r = d / dMax;
	return 1.0f - min( 1.0f, r*r );
}

// Compute the ao value
float ComputeAO( float3 p, float3 pi, float3 n )
{
	float3 ri = pi - p;
	float di = length( ri );
	ri *= rcp( di );
	float NoR = saturate( dot( n, ri ) - 0.15 );	// Bias ~30 deg (position reconstruction precision)
	return NoR * Falloff( di );
}

// Pixel shader for full scene ambient occlusion pass
void PS_SSAO( PS_INPUT_VIEW input, out float4 oColor : SV_Target )
{
	// Get the kernel size for this pixel
	float z = DL_GetDepth( input.Pos.xy );
	float3 p = ReconstructPosition( input.Pos.xy * 2, z );
	float3 n = DecodeNormal( g_txFlatNormal[input.Pos.xy] );
	int r = ProjectRadius( RescaleDepth( z ) );

	// Clamp sample range to the screen
	int minx = max( 0, input.Pos.x - r );
	int miny = max( 0, input.Pos.y - r );
	int maxx = min( (int)g_ScreenSize.x/2U - 1, input.Pos.x + r );
	int maxy = min( (int)g_ScreenSize.y/2U - 1, input.Pos.y + r );
	float N = (maxx - minx + 1) * (maxy - miny + 1);

	// Compute ao
	float ao = 0;
	int2 samplePos;
	for ( samplePos.y = miny; samplePos.y <= maxy; ++samplePos.y )
	{
		for ( samplePos.x = minx; samplePos.x <= maxx; ++samplePos.x )
		{
			// Get the sample point
			float3 pi = ReconstructPosition( samplePos * 2, DL_GetDepth( samplePos ) );

			// Compute ao
			ao += ComputeAO( p, pi, n );
		}
	}
	oColor = 1.0f - ao * rcp( N );
}


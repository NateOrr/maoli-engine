//--------------------------------------------------------------------------------------
// File: CS_ClearPartitions.hlsl
//
// Compute min/max forthe shadow map partitions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>

// Clear out all the intervals just case
[numthreads(SDSM_PARTITIONS, 1, 1)]
void CS_ClearPartitions(uint groupIndex : SV_GroupIndex)
{
    g_PartitionsUint[groupIndex].intervalBegin = 0x7F7FFFFF;       // Float max
    g_PartitionsUint[groupIndex].intervalEnd = 0;
}

//--------------------------------------------------------------------------------------
// File: PS_GBuffer.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <MaterialHelper.hlsli>
#include <Deferred.hlsli>

cbuffer cbDissolve
{
	float3 g_DissolveColor;
	float g_DissolveFactor;
};

texture2D<float> g_txDissolve;

// Pixel shader for GBuffer pass
DL_Output PS_GBufferDissolveAlpha( PS_INPUT_GBUFFER input )
{
	AlphaTest(input.Tex);

	// Dissolve effect
	float dissolve = (1.0f-g_txDissolve.Sample( g_samLinear, input.Tex )) * g_DissolveFactor;
	if(dissolve > 1.0f)
		discard;

	// Init the output struct
	DL_Output oBuf;

	// Base color
	oBuf.Color = GetMaterialColor( input.Tex );
	oBuf.Color.rgb *= g_DissolveColor;

	// Shading normal and polygon normal
	float3 normal = GetNormal( input.Tex, normalize( input.Norm ), input.WPos );
	float3 flatNormal = normalize( cross( ddx_fine( input.WPos ), ddy_fine( input.WPos ) ) );
	oBuf.Normal = EncodeNormal( normal );
	oBuf.FlatNormal = EncodeNormal( flatNormal );

	// Material properties
	oBuf.Properties[MAT_ROUGHNESS] = g_MaterialRoughness;
	oBuf.Properties[MAT_METALIC] = g_MaterialMetalic;
	oBuf.ObjectID = g_ObjectID;

	// Emissive color
	oBuf.Emissive = g_MaterialEmissive * g_txMaterial[TEX_EMISSIVE].Sample( g_samLinear, input.Tex );

	return oBuf;
}

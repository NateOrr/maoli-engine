//--------------------------------------------------------------------------------------
// File: PS_ForwardLit.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Shading.hlsli>
#include <MaterialHelper.hlsli>

static const float4 g_PartitionColors[] = {
	float4(0, 1, 1, 1),
	float4(1, 0, 1, 1),
	float4(1, 1, 0, 1),
	float4(0, 1, 0, 1),
};

// Forward rendering pixel shader
void PS_ForwardLit( PS_INPUT_SHADE input, out float4 oColor : SV_Target)
{
	AlphaTest(input.Tex);

	// Get material properties
	float3 normal = GetNormal(input.Tex, normalize(input.Norm), input.WPos);
	float roughness = g_MaterialRoughness;
	float metalic = g_MaterialMetalic;
	float3 baseColor = GetMaterialColor(input.Tex).rgb;
	float viewDist = length( input.ViewPos );
	float depth = input.ViewPos.z;

	// Find the cluster for this pixel
	const uint sliceID = NormalizeDepth( viewDist ) * g_NumSlices;
	const uint tileX = input.Pos.x / g_TileSize;
	const uint tileY = input.Pos.y / g_TileSize;
	const uint clusterID = tileY*g_TileWidth*g_NumSlices + tileX*g_NumSlices + sliceID;

	// Grab the index data
	LightIndexData indexData = g_ClusterIndexData[clusterID];
	uint i = 0, offset = indexData.start;

	// Ambient
	oColor.rgb = baseColor * g_Ambient.rgb;
	oColor.a = 1.0f - g_MaterialColor.a;

	// Cascade shadow map directional light
	for ( i = 0; i < SDSM_PARTITIONS; ++i )
	{
		Partition partition = g_PartitionsReadOnly[i];
		if ( depth >= partition.intervalBegin && depth < partition.intervalEnd )
		{
			oColor += float4(ComputeDirectionalLightingWithShadow( g_SunDirection, g_SunColor, baseColor.rgb, normal.xyz, input.WPos, roughness, metalic, input.ViewPos, partition, i ), 0);
			break;
		}
	}

	// Shadow casting spot lights
	for ( i = 0; i < indexData.numShadowSpotLights; ++i, ++offset )
		oColor += float4(ComputeSpotLightingWithShadow( g_SpotLights[g_ClusterIndices[offset]], baseColor, normal, input.WPos, roughness, metalic, i ), 0);

	// Shadow casting point lights
	for ( i = 0; i < indexData.numShadowPointLights; ++i, ++offset )
		oColor += float4(ComputePointLightingWithShadow( g_PointLights[g_ClusterIndices[offset]], baseColor, normal, input.WPos, roughness, metalic, i ), 0);

	// Spot lights
	for ( i = 0; i < indexData.numSpotLights; ++i, ++offset )
		oColor += float4(ComputeSpotLighting( g_SpotLights[g_ClusterIndices[offset]], baseColor, normal, input.WPos, roughness, metalic ), 0);

	// Point lights
	for ( i = 0; i < indexData.numPointLights; ++i, ++offset )
		oColor += float4(ComputePointLighting( g_PointLights[g_ClusterIndices[offset]], baseColor, normal, input.WPos, roughness, metalic ), 0);
}

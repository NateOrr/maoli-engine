//--------------------------------------------------------------------------------------
// File: PostFX.fxh
//
// Post processing
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _POSTFX_HLSLI_
#define _POSTFX_HLSLI_

// General textures to use for post processing
Texture2D g_txPostFX;
Texture2D g_txPostFX2;

// Contains weights for Gaussian blurring
#define MAX_GAUSS_SAMPLES 9
cbuffer cbPostFX : register(b11)
{
	float g_GWeightsH[MAX_GAUSS_SAMPLES];
	float g_GWeightsV[MAX_GAUSS_SAMPLES];
	float g_GOffsetsH[MAX_GAUSS_SAMPLES];
	float g_GOffsetsV[MAX_GAUSS_SAMPLES];
	int g_FilterKernel;
}


#endif

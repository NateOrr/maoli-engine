//--------------------------------------------------------------------------------------
// File: PS_DownScaleLuminance.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <HDR.hlsli>


// Pixel shader for downscaling the luminance texture of min/max/avg values
void PS_DownScaleLuminance( PS_INPUT_DOWNSCALE input, out float2 oColor : SV_Target)
{
	// Sample the luminance from surrounding pixels
	float2 sample1 = g_txLuminance.Sample(g_samPoint, input.Tex );
	float2 sample2 = g_txLuminance.Sample(g_samPoint, input.TexSamples[0] );
	float2 sample3 = g_txLuminance.Sample(g_samPoint, input.TexSamples[1] );
	float2 sample4 = g_txLuminance.Sample(g_samPoint, input.TexSamples[2] );
	float2 sample5 = g_txLuminance.Sample(g_samPoint, input.TexSamples[3] );
	
	// Avg
	oColor.r = sample1.r+sample2.r+sample3.r+sample4.r+sample5.r;
	oColor.r *= 0.2f;
	
	// Max
	oColor.g = max(sample1.g, max(sample2.g, max(sample3.g, min(sample5.g, sample4.g) ) ) );
}

//--------------------------------------------------------------------------------------
// File: PS_Particle.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Particle.hlsli>
#include <Deferred.hlsli>

// Pixel shader for GBuffer pass
float4 PS_Particle( PS_INPUT_PARTICLE input ) : SV_Target0
{
	return input.Color * g_txParticle.Sample( g_samLinear, input.Tex );
}

//--------------------------------------------------------------------------------------
// File: VS_GBufferInstanced.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shader for GBuffer pass with instancing
PS_INPUT_GBUFFER VS_GBufferInstanced( VS_INPUT_INSTANCED input )
{
	PS_INPUT_GBUFFER output;

	output.Pos = mul( input.Pos, input.WorldMatrix );
	output.WPos = output.Pos.xyz;
	output.Pos = mul( output.Pos, g_mViewProjection );
	output.Tex = input.Tex;
	output.Norm = mul( input.Norm, (float3x3)input.WorldMatrix );

	return output;
}

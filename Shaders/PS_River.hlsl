//--------------------------------------------------------------------------------------
// File: PS_River.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Water.hlsli>
#include <PostFX.hlsli>
#include <Deferred.hlsli>
#include <Shading.hlsli>
#include <MaterialHelper.hlsli>

// Pixel shader for water
void PS_River( PS_INPUT_WATER input, out float4 oColor : SV_Target0 )
{
	// Reconstruct the world space position using the depth buffer value
	float depth = DL_GetDepth( input.Pos.xy );
	float3 wPos = ReconstructPosition( input.Pos.xy, depth );
	float2 Tex = input.Pos.xy * g_ScreenSize.zw;

	// Scale in xy, offset in zw
    input.Tex = input.Tex * g_WaterUV.xy + frac(g_WaterUV.zw);
	
	// Water surface point
	float3 waterPos = input.WPos;
	
	// Underwater extinction
	float viewDepth = length(waterPos-wPos);
	float4 ext_coef = exp(-(g_WaterExtinctionColor) * viewDepth);
	float4 backScaterCoef = g_WaterBackscatterColor*0.5f;

	// Get the material properties
	float4 color = GetMaterialColor(input.Tex);
	float3 normal =  GetNormal(input.Tex, input.Normal, input.WPos);

	   // Sample the sky cube map if needed
    //if( (reflection.r==0) && (reflection.g==0) && (reflection.b==0))
    //{
		float3 v = normalize( input.WPos - g_CameraPos );
		float3 r = reflect( v, normal );
		float4 reflection = g_txCubeProbe.Sample(g_samAnisotropic, r);
    //}
	
	
	// Refraction screen space
	float dist = length(g_CameraPos - waterPos);
	float scale = 0.25 / (dist+1);
	float2 refractUV = saturate( Tex + normal.xy*scale );
	float2 reflectUV = Tex - normal.xy*scale*4;
	
	// Refracted underwater surface color
	float4 floorColor = lerp(g_txFrameBuffer.Sample(g_samBilinear, refractUV), color, 0.35f) * ext_coef + backScaterCoef;
	
	// Reflection
	//float4 reflection = g_txFrameBuffer.Sample(g_samBilinear, reflectUV);

	// Lighting on water surface
	float NoL = saturate( dot( -g_SunDirection, normal ) );
	float3 refVector = normalize( 2 * NoL * normal + g_SunDirection );
    float spec = pow(saturate(dot(refVector, v)), 128);
    
    // Fresnel term (precompute later on)
    const float nSnell = 1.34;
    float reflectivity;
	float3 nI = v;
	float3 nN = normal;
	float costhetai = abs( dot(nI,nN) );
	float thetai = acos(costhetai);
	float sinthetat = sin(thetai)/nSnell;
	float thetat = asin(sinthetat);
	if(thetai == 0.0)
	{
		reflectivity = (nSnell - 1)/(nSnell + 1);
		reflectivity = reflectivity * reflectivity;
	}
	else
	{
		float fs = sin(thetat - thetai) / sin(thetat + thetai);
		float ts = tan(thetat - thetai) / tan(thetat + thetai);
		reflectivity = ( fs*fs + ts*ts );
	}
	
	//oColor = floorColor;
	oColor = lerp(floorColor, reflection, reflectivity);
}

//--------------------------------------------------------------------------------------
// File: PS_MergeHeightInNormal.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <MaterialEditor.hlsli>

// Compute a normal map from a bump map
float4 PS_MergeHeightInNormal(PS_INPUT_TEX input) : SV_Target
{
	float3 normal = g_txBlendBase.SampleLevel(g_samPoint, input.Tex, 0).rgb;
	float height = g_txBlendLayer.SampleLevel(g_samPoint, input.Tex, 0).r;
	return float4(normal, height);
}

//--------------------------------------------------------------------------------------
// File: VS_BoxBlurSDSM.hlsl
//
// Special box blur for sdsm partitions (modified from SDSM sample)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>
#include <PostFX.hlsli>

BoxBlurVSOut VS_BoxBlurSDSM(VS_INPUT input)
{
    BoxBlurVSOut output;
    output.Position = float4(input.Pos.xyz, 1);
    
    // Scale filter size into this partition
    float2 filterSize = g_BlurFilterSize * g_PartitionsReadOnly[g_PartitionID].scale.xy;
        
    // Rescale blur kernel into texel space
	float2 filterSizeTexels = filterSize * g_TextureSize.xy;

    // Compute the blur data for the ps
    float filterSizeDim = (g_BlurDirection == 0 ? filterSizeTexels.x : filterSizeTexels.y);
    filterSizeDim = max(filterSizeDim, 1.0f);
    output.blurData = ComputeBlurData(filterSizeDim);
    
    return output;
}

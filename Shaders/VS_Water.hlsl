//--------------------------------------------------------------------------------------
// File: VS_Water.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Water.hlsli>

// Vertex shader for water
PS_INPUT_WATER VS_Water( VS_INPUT input )
{
    PS_INPUT_WATER output;
    output.Pos = mul( input.Pos, g_mWorld );
    
    float scale = 1.0f;
        
    // Get height from the water grid
    int3 index = int3(input.Tex*GRID_SIZE, 0);
    float3 displacement = g_txWater.Load(index%GRID_SIZE);
    float height = displacement.y;
    output.Pos.xz *= scale;
    
    // Compute the normal   
    float3 hPos[4];
    int htexel = 1.0;
    float hh = height;
    hPos[0] = float3( scale, g_txWater.Load( (index + int3(htexel, 0,0))%GRID_SIZE ).y - hh,  0.0f);
 	hPos[1] = float3( 0.0f,  g_txWater.Load( (index + int3(0, htexel,0))%GRID_SIZE ).y - hh,  scale);
 	hPos[2] = float3(-scale, g_txWater.Load( (index + int3(-htexel,0,0))%GRID_SIZE ).y - hh,  0.0f);
 	hPos[3] = float3( 0.0f,  g_txWater.Load( (index + int3(0,-htexel,0))%GRID_SIZE ).y - hh, -scale);
 	output.Normal = cross(hPos[0], hPos[1]) + cross(hPos[1], hPos[2]) + cross(hPos[2], hPos[3]) + cross(hPos[3], hPos[0]);
	
    // Apply displacement
    output.Pos.xyz += displacement;  
    output.WPos = output.Pos.xyz;    
    
    // Get the real normal
    float3x3 mTan = { float3(1,0,0), float3(0,0,-1), float3(0,1,0) };
    output.Normal = lerp( mul(g_txWaterNormals.Load(index%GRID_SIZE).xyz*2.0f-1.0f, mTan), output.Normal, 0.8);
    
    output.Pos = mul( output.Pos, g_mViewProjection ); 
    output.Tex = input.Tex; 
    return output;
}

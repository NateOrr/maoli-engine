//--------------------------------------------------------------------------------------
// File: MaterialEditor.hlsli
//
// Material editor shaders
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _MATERIALEDITOR_HLSLI_
#define _MATERIALEDITOR_HLSLI_

Texture2D g_txBlendBase;
Texture2D g_txBlendLayer;
Texture2D g_txBlendMask;

#endif

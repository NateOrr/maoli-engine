//--------------------------------------------------------------------------------------
// File: CS_BuildPartitions.hlsl
//
// Compute min/max forthe shadow map partitions (modified from SDSM sample)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>

float3 g_LightSpaceBorder;
float3 g_MaxScale;
float g_DilationFactor;

// Utility function that uses partitionsBounds to generate zoom regions (scale/bias)
void ComputePartitionDataFromBounds(PartitionBounds bounds, out float3 scale, out float3 bias)
{
    float3 minTexCoord = bounds.minCoord;
    float3 maxTexCoord = bounds.maxCoord;
        
    // Ensure our partitions have enough of a border for edge softening (blurring)
    // Also ensure our partition depth ranges stay far enough away from the geometry
    minTexCoord -= g_LightSpaceBorder.xyz;
    maxTexCoord += g_LightSpaceBorder.xyz;
        
    scale = 1.0f / (maxTexCoord - minTexCoord);
    bias = -minTexCoord * scale;

    // Dilate
    // TODO: This isn't necessarily required... we could instead just clamp g_LightSpaceBorder
    // to something fairly small to cover anisotropic kernels, etc. addition to
    // the blur. Leave it here for now though as the %-based dilation is convenient.
    float oneMinusTwoFactor = 1.0f - 2.0f * g_DilationFactor;
    scale *= oneMinusTwoFactor;
    bias = g_DilationFactor + oneMinusTwoFactor * bias;
    
    // Clamp scale (but remain centered)
    float3 center = float3(0.5f, 0.5f, 0.5f);
    float3 clampedScale = min(scale, g_MaxScale.xyz);
    bias = (clampedScale / scale) * (bias - center) + center;
    scale = clampedScale;
}


[numthreads(SDSM_PARTITIONS, 1, 1)]
void CS_BuildPartitions(uint groupIndex : SV_GroupIndex)
{
    // Compute scale/bias and update bounds partition UAV
    float3 scale, bias;
    ComputePartitionDataFromBounds(g_PartitionBoundsReadOnly[groupIndex], scale, bias);
    
    g_Partitions[groupIndex].scale = scale;
    g_Partitions[groupIndex].bias = bias;
}

//--------------------------------------------------------------------------------------
// File: VS_Sprite.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <sprite.hlsli>

// Sprite buffer
struct Sprite
{
	float2 scale;		// Scaling factor
	float2 position;	// Screen space position
	float4 color;		// Material color
	float4 region;		// Region inside the texture
};
StructuredBuffer<Sprite> g_SpriteInstances;

// Vertex shader for sprite rendering
PS_INPUT_SPRITE VS_Sprite( VS_INPUT_POS input, uint id : SV_InstanceID)
{
	PS_INPUT_SPRITE output;

	// Get the instance data
	Sprite instance = g_SpriteInstances[id];

	// Project the position
	output.Pos = input.Pos;
	output.Pos.xy *= instance.scale;
	output.Pos.xy += instance.position;
	output.Pos = mul(output.Pos, g_mWorld);
	output.Pos.xy -= float2(1.0f, -1.0f);
	
	// Set the color
	output.Color = instance.color;

	// Transform the texture coords to match the render region
	float2 texSize;
	g_txSprite.GetDimensions(texSize.x, texSize.y);
	output.Tex = input.Pos.xy;
	output.Tex *= instance.region.zw / texSize;
	output.Tex += instance.region.xy / texSize;
	output.Tex = clamp(output.Tex, 0.0f, 1.0f);
    
	return output;  
}

//--------------------------------------------------------------------------------------
// File: CS_Partitions.hlsl
//
// Compute the range for each partition (modified from SDSM sample)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>


// Logarithmic partition distribution (from SDSM paper)
float PartitionFunction(uint partition, float ratio, float minDepth)
{
    float power = float(partition) * rcp(float(SDSM_PARTITIONS));
    return minDepth * pow(ratio, power);
}

[numthreads(SDSM_PARTITIONS, 1, 1)]
void CS_Partitions(uint groupIndex : SV_GroupIndex)
{
    // Get the depth ratio from the partitions
	float minDepth = g_Partitions[0].intervalBegin;
	float depthRangeRatio = abs(g_Partitions[SDSM_PARTITIONS - 1].intervalEnd / minDepth);

    // Compute the partition ranges using a logarithmic distribution
    g_Partitions[groupIndex].intervalBegin =
        groupIndex == 0 ? g_NearZ : PartitionFunction(groupIndex, depthRangeRatio, minDepth);
    g_Partitions[groupIndex].intervalEnd =
        groupIndex == (SDSM_PARTITIONS - 1) ? g_FarZ : PartitionFunction(groupIndex + 1, depthRangeRatio, minDepth);
}

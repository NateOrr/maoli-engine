//--------------------------------------------------------------------------------------
// File: PS_Water.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Water.hlsli>
#include <PostFX.hlsli>
#include <Deferred.hlsli>
#include <Shading.hlsli>

// Pixel shader for water
void PS_Water( PS_INPUT_WATER input, out float4 oColor : SV_Target0 )
{
	// Reconstruct the world space position using the depth buffer value
	float3 vPos = normalize(input.WPos - g_CameraPos);
	float depth = DL_GetDepth( input.Pos.xy );
	float3 wPos = ReconstructPosition( input.Pos.xy, depth );
	float2 Tex = input.Pos.xy * g_ScreenSize.zw;
	
		
	// Water surface point
	float3 waterPos = input.WPos;
	
	// View direction
	float3 viewDir = normalize(g_CameraPos - waterPos);
	
	// Underwater extinction
	//const float4 ext_color = float4(0.0467f, 0.114f, 0.102f, 0);
	//const float4 back_scat_color = float4(0, 0.027f, 0.022, 0);
	float viewDepth = length(waterPos-wPos);
	float4 ext_coef = exp(-(g_WaterExtinctionColor) * viewDepth);
	float4 backScaterCoef = g_WaterBackscatterColor*0.5f;
	
	// Sample normal map
	float4 normalUV = g_WaterUV + waterPos.xzxz*0.15;
	float3x3 mTan = ComputeTangentFrame( input.Normal, waterPos, normalUV.xy);
	float3 trueWaterNormal =  normalize( mul(g_txMaterial[TEX_NORMAL].Sample( g_samAnisotropic, normalUV.xy ).xyz*2.0f-1.0f, mTan)+
										 mul(g_txMaterial[TEX_SPECULAR].Sample( g_samAnisotropic, normalUV.zw ).xyz*2.0f-1.0f, mTan) );
	
	// Refraction screen space
	float dist = length(g_CameraPos - waterPos);
	float scale = 0.3 / (dist+1);
	float2 refractUV = Tex + trueWaterNormal.xy*scale;
	float2 reflectUV = Tex - trueWaterNormal.xy*scale*4;
	
	// Refracted underwater surface color
	float4 floorColor = g_txFrameBuffer.Sample(g_samPoint, refractUV) * ext_coef + backScaterCoef;
	
	// Reflection
	float4 reflection = g_txPostFX.Sample(g_samPoint, reflectUV);
	
	// Lighting on water surface
	float3 refVector = normalize( 2 * dot( -g_SunDirection, trueWaterNormal ) * trueWaterNormal + g_SunDirection );
    float4 spec = pow(saturate(dot(refVector, viewDir)), 128);
    
    // Sample the sky cube map if needed
    if( (reflection.r==0) && (reflection.g==0) && (reflection.b==0))
    {
		float3 viewReflect = normalize(reflect(-viewDir, trueWaterNormal));
		reflection = g_txSkyCube.Sample(g_samAnisotropic, viewReflect)*1.5;
    }
    
    // Fresnel term (precompute later on)
    const float nSnell = 1.34;
    float reflectivity;
	float3 nI = viewDir;
	float3 nN = trueWaterNormal;
	float costhetai = abs( dot(nI,nN) );
	float thetai = acos(costhetai);
	float sinthetat = sin(thetai)/nSnell;
	float thetat = asin(sinthetat);
	if(thetai == 0.0)
	{
		reflectivity = (nSnell - 1)/(nSnell + 1);
		reflectivity = reflectivity * reflectivity;
	}
	else
	{
		float fs = sin(thetat - thetai) / sin(thetat + thetai);
		float ts = tan(thetat - thetai) / tan(thetat + thetai);
		reflectivity = ( fs*fs + ts*ts );
	}
	
	oColor = reflectivity*reflection + (1-reflectivity)*floorColor + spec;
}

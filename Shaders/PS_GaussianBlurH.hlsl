//--------------------------------------------------------------------------------------
// File: PS_GaussianBlurH.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>

// Horizontal gaussian blur
void PS_GaussianBlurH( PS_INPUT_TEX input, out float4 oColor : SV_Target )
{
	oColor = 0;    
	[unroll]
	for(int i=0; i<MAX_GAUSS_SAMPLES; i++)
        oColor += g_txPostFX.Sample(g_samPoint, input.Tex + float2(g_GOffsetsH[i], 0.0f)) * g_GWeightsH[i];
}

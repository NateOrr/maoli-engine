//--------------------------------------------------------------------------------------
// File: VS_ShadowMapTerrainSDSM.hlsl
//
// SDSM Shadow rendering for terrain
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Terrain.hlsli>
#include <ShadowMap.hlsli>
#include <Partition.hlsli>

// Vertex Shader for GBuffer pass for terrain using vertex texturing
void VS_ShadowMapTerrainSDSM( VS_INPUT_POS input, out float4 oPosition : SV_Position )
{
	float3 worldPos;

	// Position based on the patch
	worldPos.xz = input.Pos.xz*g_ClipmapScale + g_ClipmapOffset;

	// Compute the texture coords based on the position
	float isize = 1.0f / (g_ClipmapScale*g_ClipmapSize);
	float2 CTex = (worldPos.xz + g_ClipmapScale) * isize + 0.5f;

	// Adjust the position to fit inside the next coarser level
	worldPos.xz += g_ClipmapFix;

	// Sample the height and normal at this location
	worldPos.y = g_txClipmap.SampleLevel( g_samPoint, CTex, 0 ).r;

	// Compute the height for the next coarser level, and fix the T-Gaps
	float zc;
	int2 pos = int2(CTex*g_ClipmapSize);
		uint2 mod2 = pos % 2U;
		const float texel = 1.0f / g_ClipmapSize;
	if ( mod2.x == 0 && mod2.y == 1 )
	{
		float2 coordOffset = float2(0, texel);
			zc = (g_txClipmap.SampleLevel( g_samPoint, CTex + coordOffset, 0 ).r +
			g_txClipmap.SampleLevel( g_samPoint, CTex - coordOffset, 0 ).r) * 0.5f;
		if ( pos.x == 0 || pos.x == g_ClipmapSize - 1 )
			worldPos.y = zc;
	}
	else if ( mod2.x == 1 && mod2.y == 0 )
	{
		float2 coordOffset = float2(texel, 0);
			zc = (g_txClipmap.SampleLevel( g_samPoint, CTex + coordOffset, 0 ).r +
			g_txClipmap.SampleLevel( g_samPoint, CTex - coordOffset, 0 ).r) * 0.5f;
		if ( pos.y == 0 || pos.y == g_ClipmapSize - 1 )
			worldPos.y = zc;
	}
	else if ( mod2.x == 1 && mod2.y == 1 )
	{
		float2 coordOffsetX = float2(texel, 0);
			float2 coordOffsetY = float2(0, texel);
			zc = (g_txClipmap.SampleLevel( g_samPoint, CTex + coordOffsetX + coordOffsetY, 0 ).r +
			g_txClipmap.SampleLevel( g_samPoint, CTex - coordOffsetX + coordOffsetY, 0 ).r +
			g_txClipmap.SampleLevel( g_samPoint, CTex + coordOffsetX - coordOffsetY, 0 ).r +
			g_txClipmap.SampleLevel( g_samPoint, CTex - coordOffsetX - coordOffsetY, 0 ).r) * 0.25f;
	}
	else
	{
		zc = worldPos.y;
	}

	// Compute the transition blending factors
	float wFactor = (g_ClipmapSize*g_ClipmapScale) / 10.0f;
	float ax = clamp( (abs( worldPos.x - g_CameraPos.x ) - ((g_ClipmapSize*g_ClipmapScale - 1) / 2 - wFactor - 1)) / wFactor, 0, 1 );
	float ay = clamp( (abs( worldPos.z - g_CameraPos.z ) - ((g_ClipmapSize*g_ClipmapScale - 1) / 2 - wFactor - 1)) / wFactor, 0, 1 );

	// Blend the transition heights
	worldPos.y = lerp( worldPos.y, zc, max( ax, ay ) );

	// Scale the height
	worldPos.y *= g_HeightScale;

	// Transform
	oPosition = mul( float4(worldPos, 1), g_mShadow );

	// Scale/bias NDC space
	Partition partition = g_PartitionsReadOnly[g_PartitionID];
	oPosition.xy *= partition.scale.xy;
	oPosition.x += (2.0f * partition.bias.x + partition.scale.x - 1.0f);
	oPosition.y -= (2.0f * partition.bias.y + partition.scale.y - 1.0f);

	// Clamp to [0, 1] happens the viewport transform
	oPosition.z = oPosition.z * partition.scale.z + partition.bias.z;
}

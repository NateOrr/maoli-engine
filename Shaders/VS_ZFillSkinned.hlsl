//--------------------------------------------------------------------------------------
// File: VS_ZFillSkinned.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex shader for forward rendering
float4 VS_ZFillSkinned( VS_INPUT_SKIN_POS input ) : SV_Position
{
	float4 pos;
	SkinVertex( input, pos );
	return mul( mul( pos, g_mWorld ), g_mViewProjection );
}

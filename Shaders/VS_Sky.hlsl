//--------------------------------------------------------------------------------------
// File: VS_Sky.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Atmosphere.hlsli>


// Skydome vertex shader
PS_INPUT_SKY VS_Sky( VS_INPUT input)
{
    // Output the position
	PS_INPUT_SKY output;
	output.Pos = mul(input.Pos,g_mWorld);
	output.View = output.Pos.xyz-g_CameraPos;
    output.Pos = mul(output.Pos,g_mViewProjection);
    output.Pos.z = output.Pos.w;
    output.Pos *= 0.8f;
    output.Tex0 = input.Tex;
    output.Tex1 = -input.Pos.xyz;
    return output;
}

//--------------------------------------------------------------------------------------
// File: CS_ReducePartitionBounds.hlsl
//
// Compute min/max forthe shadow map partitions (modified from SDSM sample)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>

// TODO: This actually needs to be modified to ensure that it fits in local memory for any number of partitions!
// Needs to be based on max local memory size and max partition count.
// Currently set to 128 threads which works with up to 10 partitions (given 32KB local memory)
#define REDUCE_BOUNDS_BLOCK_X 16
#define REDUCE_BOUNDS_BLOCK_Y 8
#define REDUCE_BOUNDS_BLOCK_SIZE (REDUCE_BOUNDS_BLOCK_X*REDUCE_BOUNDS_BLOCK_Y)

// Store these as raw float arrays for reduction efficiency:
// Grouped by SDSM_PARTITIONS
// Then grouped by SV_GroupIndex
#define REDUCE_BOUNDS_SHARED_MEMORY_ARRAY_SIZE (SDSM_PARTITIONS * REDUCE_BOUNDS_BLOCK_SIZE)
groupshared float3 sBoundsMin[REDUCE_BOUNDS_SHARED_MEMORY_ARRAY_SIZE];
groupshared float3 sBoundsMax[REDUCE_BOUNDS_SHARED_MEMORY_ARRAY_SIZE];


[numthreads( REDUCE_BOUNDS_BLOCK_X, REDUCE_BOUNDS_BLOCK_Y, 1 )]
void CS_ReducePartitionBounds( 
	int3 groupId       : SV_GroupID,
	int3 groupThreadId : SV_GroupThreadID,
	int  groupIndex : SV_GroupIndex )
{
	// Initialize stack copy of partition data for this thread
	PartitionBounds boundsReduce[SDSM_PARTITIONS];
	{
		[unroll] for ( int partition = 0; partition < SDSM_PARTITIONS; ++partition )
		{
			boundsReduce[partition] = ResetPartitionBoundsFloat( );
		}
	}

	// Loop over tile and reduce into local memory
	float nearZ = g_PartitionsReadOnly[0].intervalBegin;
	float farZ = g_PartitionsReadOnly[SDSM_PARTITIONS - 1].intervalEnd;
	{
		int2 tileStart = groupId.xy * SDSM_TILE_SIZE.xx + groupThreadId.xy;
		for ( int tileY = 0; tileY < SDSM_TILE_SIZE; tileY += REDUCE_BOUNDS_BLOCK_Y )
		{
			for ( int tileX = 0; tileX < SDSM_TILE_SIZE; tileX += REDUCE_BOUNDS_BLOCK_X )
			{
				int2 screenPos = tileStart + int2(tileX, tileY);
				if ( screenPos.x < g_ScreenSize.x && screenPos.y < g_ScreenSize.y )
				{
					float depth = DL_GetDepth( screenPos );
					float3 worldPos = ReconstructPosition( screenPos, depth );
					depth = RescaleDepth( depth );

					// Drop samples that fall outside the view frustum (clear color, etc)
					if ( depth >= nearZ && depth < farZ )
					{
						int partition = 0;
						[unroll] for ( int i = 0; i < (SDSM_PARTITIONS - 1); ++i )
						{
							[flatten] if ( depth >= g_PartitionsReadOnly[i].intervalEnd )
							{
								++partition;
							}
						}

						// Update relevant partition data for this thread
						// This avoids the need for atomics since we're the only thread accessing this data
						float4 positionLight = mul( float4(worldPos, 1.0f), g_mShadow );
						float3 lightTexCoord = (positionLight.xyz / positionLight.w) * float3(0.5f, -0.5f, 1.0f) + float3(0.5f, 0.5f, 0.0f);
						boundsReduce[partition].minCoord = min( boundsReduce[partition].minCoord, lightTexCoord );
						boundsReduce[partition].maxCoord = max( boundsReduce[partition].maxCoord, lightTexCoord );
					}
				}
			}
		}
	}


	// Copy result to shared memory for reduction
	{
		[unroll] for ( int partition = 0; partition < SDSM_PARTITIONS; ++partition )
		{
			int index = (groupIndex * SDSM_PARTITIONS + partition);
			sBoundsMin[index] = boundsReduce[partition].minCoord;
			sBoundsMax[index] = boundsReduce[partition].maxCoord;
		}
	}

	GroupMemoryBarrierWithGroupSync( );

	// Now reduce our local memory data set to one element
	for ( int offset = (REDUCE_BOUNDS_SHARED_MEMORY_ARRAY_SIZE >> 1); offset >= SDSM_PARTITIONS; offset >>= 1 )
	{
		for ( int i = groupIndex; i < offset; i += REDUCE_BOUNDS_BLOCK_SIZE )
		{
			sBoundsMin[i] = min( sBoundsMin[i], sBoundsMin[offset + i] );
			sBoundsMax[i] = max( sBoundsMax[i], sBoundsMax[offset + i] );
		}
		GroupMemoryBarrierWithGroupSync( );
	}

	// Now write out the result from this pass
	if ( groupIndex < SDSM_PARTITIONS )
	{
		InterlockedMin( g_PartitionBoundsUint[groupIndex].minCoord.x, asuint( sBoundsMin[groupIndex].x ) );
		InterlockedMin( g_PartitionBoundsUint[groupIndex].minCoord.y, asuint( sBoundsMin[groupIndex].y ) );
		InterlockedMin( g_PartitionBoundsUint[groupIndex].minCoord.z, asuint( sBoundsMin[groupIndex].z ) );
		InterlockedMax( g_PartitionBoundsUint[groupIndex].maxCoord.x, asuint( sBoundsMax[groupIndex].x ) );
		InterlockedMax( g_PartitionBoundsUint[groupIndex].maxCoord.y, asuint( sBoundsMax[groupIndex].y ) );
		InterlockedMax( g_PartitionBoundsUint[groupIndex].maxCoord.z, asuint( sBoundsMax[groupIndex].z ) );
	}
}

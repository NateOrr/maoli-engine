//--------------------------------------------------------------------------------------
// File: VS_DeferredOrtho.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shaders that prep for deferred passed that need reconstructed position

// Ortho2D projection
PS_INPUT_VIEW VS_DeferredOrtho( VS_INPUT input )
{
    PS_INPUT_VIEW output;
    output.vPos = input.Norm;
    output.Pos = float4(input.Pos.xyz, 1);   
    return output;
}

//--------------------------------------------------------------------------------------
// File: CS_PartitionBounds.hlsl
//
// Compute min/max forthe shadow map partitions (modified from SDSM sample)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>


#define REDUCE_ZBOUNDS_BLOCK_DIM 16
#define REDUCE_ZBOUNDS_BLOCK_SIZE (REDUCE_ZBOUNDS_BLOCK_DIM*REDUCE_ZBOUNDS_BLOCK_DIM)

groupshared float sMinZ[REDUCE_ZBOUNDS_BLOCK_SIZE];
groupshared float sMaxZ[REDUCE_ZBOUNDS_BLOCK_SIZE];

// Compute a tight fitting z range to partition the scene with
[numthreads( REDUCE_ZBOUNDS_BLOCK_DIM, REDUCE_ZBOUNDS_BLOCK_DIM, 1 )]
void CS_PartitionBounds( int3 groupId       : SV_GroupID,
	int3 groupThreadId : SV_GroupThreadID,
	int  groupIndex : SV_GroupIndex )
{
	// Initialize stack copy of reduction data for this thread
	float minZ = g_FarZ;
	float maxZ = g_NearZ;

	// Loop over tile and reduce into local memory
	{
		int2 tileStart = groupId.xy * SDSM_TILE_SIZE.xx + groupThreadId.xy;
		for ( int tileY = 0; tileY < SDSM_TILE_SIZE; tileY += REDUCE_ZBOUNDS_BLOCK_DIM )
		{
			for ( int tileX = 0; tileX < SDSM_TILE_SIZE; tileX += REDUCE_ZBOUNDS_BLOCK_DIM )
			{
				int2 screenPos = tileStart + int2(tileX, tileY);
				if ( screenPos.x < g_ScreenSize.x && screenPos.y < g_ScreenSize.y )
				{
					float depth = RescaleDepth( DL_GetDepth( screenPos ) );
					if ( depth >= g_NearZ && depth < g_FarZ )
					{
						minZ = min( minZ, depth );
						maxZ = max( maxZ, depth );
					}
				}
			}
		}
	}

	// Copy result to shared memory for reduction
	sMinZ[groupIndex] = minZ;
	sMaxZ[groupIndex] = maxZ;

	GroupMemoryBarrierWithGroupSync();

	// Reduce our local memory data set to one element
	// TODO: Switch to local atomics for last few iterations
	for ( int offset = (REDUCE_ZBOUNDS_BLOCK_SIZE >> 1); offset > 0; offset >>= 1 )
	{
		if ( groupIndex < offset )
		{
			sMinZ[groupIndex] = min( sMinZ[groupIndex], sMinZ[offset + groupIndex] );
			sMaxZ[groupIndex] = max( sMaxZ[groupIndex], sMaxZ[offset + groupIndex] );
		}
		GroupMemoryBarrierWithGroupSync();
	}

	// Now write out the result from this pass to the partition data
	// We'll fill in the intermediate intervals in a subsequent pass
	if ( groupIndex == 0 )
	{
		// Just use scatter atomics for now... we can switch to a global reduction if necessary later
		// Note that choosing good tile dimensions to "just" fill the machine should keep this efficient
		InterlockedMin( g_PartitionsUint[0].intervalBegin, asuint( sMinZ[0] ) );
		InterlockedMax( g_PartitionsUint[SDSM_PARTITIONS - 1].intervalEnd, asuint( sMaxZ[0] ) );
	}
}

#ifndef _COMMONFUNCTIONS_HLSLI_
#define _COMMONFUNCTIONS_HLSLI_

// Reconstruct a matrix
float4x4 DecodeMatrix(float3x4 encodedMatrix)
{
	return float4x4(	float4(encodedMatrix[0].xyz,0),
						float4(encodedMatrix[1].xyz,0),
						float4(encodedMatrix[2].xyz,0),
						float4(encodedMatrix[0].w,encodedMatrix[1].w,encodedMatrix[2].w,1));
}

// Normalize a depth value
float NormalizeDepth( float depth )
{
	return (depth-g_NearZ) / (g_FarZ - g_NearZ);
}

// Get linear Z from device Z
float GetLinearDepth( float deviceDepth, float2 projRatio )
{
	return projRatio.y / (deviceDepth - projRatio.x);
}

// Get linear Z from device Z
float GetLinearDepth( float deviceDepth )
{
	return g_ProjRatio.y / (deviceDepth - g_ProjRatio.x);
}

// Rescale depth into the view range
float RescaleDepth( float linearDepth )
{
	return linearDepth * (g_FarZ - g_NearZ) + g_NearZ;
}

// Reconstruct world space position from linear depth
float3 ReconstructPosition( float2 screenPos, float linearDepth )
{
	return (g_ScreenToWorldZ + (g_ScreenToWorldX * screenPos.x) + (g_ScreenToWorldY * screenPos.y)) * linearDepth + g_CameraPos;
}



// Manually compute the mip level
// Input: Tex coords * Tex dimension
#define mip_bias -0.6f  // This could be set from app side the future
float GetMipLevel(float2 fTexCoordsPerSize)
{
	// Compute all derivatives
   float2 dxSize = ddx(fTexCoordsPerSize);
   float2 dySize = ddy(fTexCoordsPerSize);

   // Find min of change u and v across quad: compute du and dv magnitude across quad
   float2 dTexCoords = dxSize * dxSize + dySize * dySize;

   // Standard mipmapping uses max here
   float fMinTexCoordDelta = max( dTexCoords.x, dTexCoords.y );

   // Compute the current mip level
   float mip = 0.5f*log2( fMinTexCoordDelta ) + mip_bias;

   return max(0, min(mip, g_VTMaxMipLevel));
}


//-----------------------------------
// tangent frame functions
//-----------------------------------
float3x3 ComputeTangentFrame( float3 N, float3 p, float2 uv )
{
    // get edge vectors of the pixel triangle
    float3 dp1 = ddx( p );
    float3 dp2 = ddy( p );
    float2 duv1 = ddx( uv );
    float2 duv2 = ddy( uv );

    // solve the linear system
    float3x3 M = float3x3( dp1, dp2, cross( dp1, dp2 ) );
    float2x3 inversetransposeM = float2x3( cross( M[1], M[2] ), cross( M[2], M[0] ) );
    float3 T = mul( float2( duv1.x, duv2.x ), inversetransposeM );
    float3 B = mul( float2( duv1.y, duv2.y ), inversetransposeM );

    // construct tangent frame 
    return float3x3( normalize(T), normalize(B), N );
}

// Create a plane equation from 3 vertices
float4 Plane(float3 v1, float3 v2, float3 v3)
{
	float3 n = normalize( cross(v1-v3, v1-v2) );
	float  d = dot(n, v1);
	return float4(n, -d);
}

// Dot product of a plane with a 3d vector
float PlaneDotCoord(float4 plane, float3 p)
{
	return dot(plane, float4(p, 1.0f));
}

// Performs skinned animation on the vertex position using the matrix pallete
void SkinVertex( VS_INPUT_SKIN input, out float4 Pos)
{
	Pos=0;
	[unroll]
	for(int i=0; i<4; i++)
		Pos += input.Weights[i] * mul( g_BonePalette[input.Bones[i]], input.Pos );
}

// Performs skinned animation on the vertex position using the matrix pallete
void SkinVertex( VS_INPUT_SKIN_POS input, out float4 Pos)
{
	Pos=0;
	[unroll]
	for(int i=0; i<4; i++)
		Pos += input.Weights[i] * mul( g_BonePalette[input.Bones[i]], input.Pos );
}

// Performs skinned animation on the vertex position and normal using the matrix pallete
void SkinVertexNormal( VS_INPUT_SKIN input, inout float4 Pos, inout float3 Normal )
{
	Pos=0;
	Normal=0;

	[unroll]
	for(int i=0; i<4; i++)
	{
		Pos += input.Weights[i] * mul( g_BonePalette[input.Bones[i]], input.Pos );
		Normal += input.Weights[i] * mul( input.Norm, (float3x3)g_BonePalette[input.Bones[i]] );
	}
}


//-----------------------------------------------------------------------------
// Crytek Triangle Wave functions, a cheap alternative to sin waves
//-----------------------------------------------------------------------------
float4 SmoothCurve( float4 x ) 
{  
	return x * x *( 3.0 - 2.0 * x );  
}  
float4 TriangleWave( float4 x ) 
{  
	return abs( frac( x + 0.5 ) * 2.0 - 1.0 );  
}  
float4 SmoothTriangleWave( float4 x ) 
{  
	return SmoothCurve( TriangleWave( x ) );  
}  

//-----------------------------------------------------------------------------
// R8G8B8A8_UNORM <-> FLOAT4
//-----------------------------------------------------------------------------
uint D3DX_FLOAT_to_UINT(float _V, float _Scale)
{
    return (uint)floor(_V * _Scale + 0.5f);
}

float4 D3DX_R8G8B8A8_UNORM_to_FLOAT4(uint packedInput)
{
    precise float4 unpackedOutput;
    unpackedOutput.x = (float)  (packedInput      & 0x000000ff)  / 255;
    unpackedOutput.y = (float)(((packedInput>> 8) & 0x000000ff)) / 255;
    unpackedOutput.z = (float)(((packedInput>>16) & 0x000000ff)) / 255;
    unpackedOutput.w = (float)  (packedInput>>24)                / 255;
    return unpackedOutput;
}

uint D3DX_FLOAT4_to_R8G8B8A8_UNORM(precise float4 unpackedInput)
{
    uint packedOutput;
    packedOutput = ( (D3DX_FLOAT_to_UINT(saturate(unpackedInput.x), 255))     |
                     (D3DX_FLOAT_to_UINT(saturate(unpackedInput.y), 255)<< 8) |
                     (D3DX_FLOAT_to_UINT(saturate(unpackedInput.z), 255)<<16) |
                     (D3DX_FLOAT_to_UINT(saturate(unpackedInput.w), 255)<<24) );
    return packedOutput;
}

#endif

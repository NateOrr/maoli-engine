//--------------------------------------------------------------------------------------
// File: PS_DownscaleGBuffer.hlsl
//
// Downscale the gbuffers for ssao
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Deferred.hlsli>

struct Output
{
	float Depth: SV_Target0;
	float2 Normal : SV_Target1;
	float4 Emissive : SV_Target2;
};

Output PS_DownscaleGBuffer( PS_INPUT_TEX input )
{
	Output o = (Output)0;
	input.Pos.xy *= 2;
	o.Depth = g_txDepth[input.Pos.xy];
	o.Normal = g_txFlatNormal[input.Pos.xy];
	o.Emissive = DL_GetEmissiveColor( input.Pos.xy * g_ScreenSize.zw );
	return o;
}
//--------------------------------------------------------------------------------------
// File: CS_MouseIntersect.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Honua.hlsli>
#include <Deferred.hlsli>

// Very simple compute shader, gets the mouse intersection with the scene using the gbuffer
[numthreads( 1, 1, 1 )]
void CS_MouseIntersect()
{
	g_MouseSceneIntersectRW[0] = g_CameraPos + DL_GetDepth( g_TWTexCoord*g_ScreenSize.xy )*g_TWRay;
}

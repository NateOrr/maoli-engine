//--------------------------------------------------------------------------------------
// File: VS_Particle.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Particle.hlsli>

// Vertex Shader for GBuffer pass
PS_INPUT_PARTICLE VS_Particle( VS_INPUT_PARTICLE input )
{
	PS_INPUT_PARTICLE output;
	output.Pos = mul( input.Pos*float4(input.ParticleSize, 1), g_mWorld ) + float4(input.ParticlePos.xyz, 0);
	output.Pos = mul( output.Pos, g_mViewProjection );
	output.Tex = input.Tex;
	output.Color = input.ParticleColor;
	return output;
}

//--------------------------------------------------------------------------------------
// File: PS_VelocityMap.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Deferred.hlsli>

// Pixel shader for building a velocity map for motion blur
void PS_VelocityMap( PS_INPUT_POSTFX input, out float2 oColor : SV_Target )
{
	// Reconstruct the world space position using the depth buffer value
	float depth = DL_GetDepth( input.Pos.xy );

	// Only process pixels that have geometry
	if ( depth > g_NearZ )
	{
		float3 worldPos = ReconstructPosition( input.Pos.xy, depth );

			// Project using the previous frame's matrix
			float4 screenPos = mul( float4(worldPos, 1.0f), g_mViewProjection );
			float4 screenPosPrev = mul( float4(worldPos, 1.0f), g_mOldViewProjection );
			screenPos.xy /= screenPos.w;
		screenPosPrev.xy /= screenPosPrev.w;
		oColor = (screenPos.xy - screenPosPrev.xy) * 0.5f;
	}
	else
	{
		oColor = 0;
	}
}

//--------------------------------------------------------------------------------------
// File: PS_Sprite.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <sprite.hlsli>

// Empty pixel shader (occlusion culling etc)
void PS_Sprite(PS_INPUT_SPRITE input, out float4 oColor : SV_Target0)
{
	oColor = g_txSprite.Sample(g_samPoint, input.Tex) * input.Color;
}

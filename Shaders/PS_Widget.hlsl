//--------------------------------------------------------------------------------------
// File: PS_Widget.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Honua.hlsli>
#include <Deferred.hlsli>


// Pixel Shader for rendering the paint/sculpt widget
float4 PS_Widget( PS_INPUT_VIEW input ) : SV_Target
{
	float depth = DL_GetDepth( input.Pos.xy );
	if ( depth > g_FarZ*0.9 )
		discard;
	
	// Construct the mouse intersection point
	float3 sPos = g_MouseSceneIntersect;

	// Reconstruct the world space position using the depth buffer value
	float3 wPos = ReconstructPosition( input.Pos.xy, depth );
	
	float thickness = (distance(sPos.xz, g_CameraPos.xz)+5.0f) / 320.0f;

	float dist = distance(sPos.xz, wPos.xz);
		
	if(dist>=g_TWHardness && dist<=g_TWHardness+thickness)
		return float4(1,1,0,0.8f);
	else if(dist>=g_TWRadius && dist<=g_TWRadius+thickness)
		return float4(1,0,0,0.8f);
	
	// Get the mask factor
	if(dist<=g_TWRadius && g_UsePaintMask)
	{
		float2 projUV;
		return float4(ComputePaintMask(wPos.xz, sPos.xz, g_TWRadius, projUV).rgb, 0.5f);
	}
	discard;
	return 0;
}

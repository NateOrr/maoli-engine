//--------------------------------------------------------------------------------------
// File: PS_CubeIBL.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Shading.hlsli>
#include <Deferred.hlsli>

// Pixel Shader for Shading pass
void PS_CubeIBL( PS_INPUT_VIEW input, out float4 oColor : SV_Target0 )
{
	// Compute lighting
	//float4 baseColor = DL_GetColorAndMetalic( input.Pos.xy );
	//float4 n = DL_GetNormalAndRoughness( input.Pos.xy );
	//float depth = DL_GetDepth( input.Pos.xy );
	//float3 v = normalize( g_CameraPos - ReconstructPosition( input.Pos.xy, depth ) );

		oColor = 0;// float4(SpecularIBL( baseColor.rgb, n.w, n.xyz, v ), 1);
	//oColor = Fresnel(oColor, -v, n);
}

//--------------------------------------------------------------------------------------
// File: Honua.fxh
//
// Editor and debug rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _HONUA_HLSLI_
#define _HONUA_HLSLI_

#include <MaterialHelper.hlsli>

// Painting types
#define PAINT_TILE 0
#define PAINT_PROJECT 1

// vars
cbuffer cbEditor : register(b10)
{
	int2		g_TWTexCoord;	// Texture coords for the widget depth lookup
	float		g_TWRadius;		// Widget radius
	float		g_TWHardness;	// Inner radius

	float3		g_TWRay;		// The view ray for reconstructing the world space widget position
	float		g_TWStrength;	// Strength
	
	uint2		g_PaintLayerSize;
	uint2		g_PaintPage;
	
	bool		g_UsePaintMask;
	bool		g_LayerMaskInvert;
	bool		g_LayerMaskInvertColor;
	bool		g_LayerMaskAbsolute;
	
	bool		g_LayerMaskModulate;
	bool		g_LayerMaskColor;
	uint		g_PaintMode;
	uint		g_PaintMaskMode;
	
	float3		g_MouseSceneIntersect;
	float		g_LayerID;

	float4		g_PaintRegion;
};

Texture2D					g_txPaintMask;							// Texture to mask the painting region
RWTexture2D<uint>			g_PhysicalTextureUAV : register(u6);	// UAV into the physical texture so we can do painting
RWStructuredBuffer<float3>  g_MouseSceneIntersectRW : register(u0);	// Single element buffer, holds mouse/scene intersection point



#include <BlendPaint.hlsli>

// Converts world space position into texels based on a circle, and samples the mask
float4 ComputePaintMask(float2 wPos, float2 sPos, float radius, out float2 projUV)
{
	float4 mask = 1.0f;
	projUV = 0;
	if(g_UsePaintMask)
	{
		// Get the uv coords
		float2 maskUV; 
		float2 cornerPos = sPos - radius.xx;
		float maskDist = distance(cornerPos, wPos) / (radius*2);
		projUV = normalize(wPos-cornerPos)*maskDist;
		if(g_PaintMaskMode == PAINT_PROJECT)
		{
			maskUV = projUV;
		}
		else if(g_PaintMaskMode == PAINT_TILE)
		{
			float2 cornerPos = sPos - radius.xx;
			maskDist = distance(cornerPos, wPos);
			maskUV = normalize(wPos-cornerPos)*maskDist;
		}
		
		// Convert to texels and load the mask
		int w, h, n;
		g_txPaintMask.GetDimensions(0, w, h, n);
		maskUV *= uint2(w, h);
		uint3 maskCoord = int3(maskUV, 0);
		maskCoord.xy = maskCoord.xy % uint2(w, h);
		mask = g_txPaintMask.Load(maskCoord);
		mask.a = 0.299*mask.r + 0.587*mask.g + 0.114*mask.b;
				
		// Modifications
		if(g_LayerMaskAbsolute)
		{
			if(mask.r<0.1f) mask.r = 0.0f; else mask.r = 1.0f;
			if(mask.g<0.1f) mask.g = 0.0f; else mask.g = 1.0f;
			if(mask.b<0.1f) mask.b = 0.0f; else mask.b = 1.0f;
		}
		if(g_LayerMaskInvert)
			mask.a = 1.0f - mask.a;
		if(g_LayerMaskInvertColor)
			mask.rgb = (float3)1.0f - mask.rgb;
	}
	if(g_LayerMaskColor)
		return mask;
	else
		return mask.aaaa;
}


// Paints a texel
void PaintTexel(uint2 page, uint2 localCoords, float4 mask, float2 projUV)
{
	// Figure out which slots we need
	uint2 vtDiffuse=0, vtNormal=0, vtSpecular=0;
	bool bDiffuse = true;//(CheckMaterialTexture(TEX_DIFFUSE) || CheckMaterialTexture(TEX_ALPHA));
	bool bNormal = (CheckMaterialTexture(TEX_NORMAL) || CheckMaterialTexture(TEX_HEIGHT));
	bool bSpecular = CheckMaterialTexture(TEX_SPECULAR);
		
	// Get the coords, and do the blend
	// Each component needs to be separately blended
	uint3 layerIndex;
	uint2 effectivePageSize = g_VTPageSize - 4;
	uint2 layerTex = (page*effectivePageSize) + localCoords - uint2(2, 2);
	if(g_PaintMode==PAINT_TILE)
		layerIndex = int3(layerTex%g_PaintLayerSize, 0);
	else if(g_PaintMode==PAINT_PROJECT)
		layerIndex = int3((projUV*g_PaintLayerSize) / (g_TWRadius*2) + g_PaintLayerSize/2, 0);
	
	// Delta blend facor and pageID
	float delta = 0.2f*g_TWStrength*mask.a;
	uint pageID = g_PaintPage.y*g_VTPageTableSize+g_PaintPage.x;
	float4 pageTexel;

	// Diffuse/alpha
	if(bDiffuse)
	{
		// Get the texel from the UAV
		vtDiffuse = localCoords + g_PageTable[pageID].slot[VT_DIFFUSE].offset;
		pageTexel = D3DX_R8G8B8A8_UNORM_to_FLOAT4( g_PhysicalTextureUAV[vtDiffuse] );

		// Blend diffuse if needed
		float3 diff;
		if(CheckMaterialTexture(TEX_DIFFUSE))
			diff = g_txMaterial[TEX_DIFFUSE].Load(layerIndex).rgb;
		else
			diff = pageTexel.rgb;
		diff *= g_MaterialColor.rgb;
		if(g_LayerMaskModulate)
			diff *= mask.rgb;
		pageTexel.rgb = lerp(pageTexel.rgb, diff, delta);
		
		// Blend alpha as the total page contribution
		// This allows a fallback material to be used
		// where pages dont exist
		pageTexel.a = saturate(lerp(pageTexel.a, 1.0f, delta));
		
		// Write back to the UAV
		g_PhysicalTextureUAV[vtDiffuse] = D3DX_FLOAT4_to_R8G8B8A8_UNORM(pageTexel);
	}

	// Normal/height
	if(bNormal)
	{
		// Get the texel from the UAV
		vtNormal = localCoords + g_PageTable[pageID].slot[VT_NORMAL].offset;
		pageTexel = D3DX_R8G8B8A8_UNORM_to_FLOAT4( g_PhysicalTextureUAV[vtNormal] );

		// Blend normal if needed
		if(CheckMaterialTexture(TEX_NORMAL))
			pageTexel.rgb = lerp(pageTexel.rgb, g_txMaterial[TEX_NORMAL].Load(layerIndex).rgb, delta);
		
		// Blend height if needed
		if(CheckMaterialTexture(TEX_HEIGHT))
			pageTexel.a = lerp(pageTexel.a, g_txMaterial[TEX_HEIGHT].Load(layerIndex).a, delta);
		
		// Write back to the UAV
		g_PhysicalTextureUAV[vtNormal] = D3DX_FLOAT4_to_R8G8B8A8_UNORM(pageTexel);
	}

	// Specular/thickness
// 	if(bSpecular)
// 	{
// 		// Get the texel from the UAV
// 		vtSpecular = localCoords + g_PageTable[pageID].slot[VT_SPECULAR].offset;
// 		pageTexel = D3DX_R8G8B8A8_UNORM_to_FLOAT4( g_PhysicalTextureUAV[vtSpecular] );
// 
// 		// Blend specular if needed
// 		float3 spec = g_MaterialSpecular.rgb;
// 		if(CheckMaterialTexture(TEX_SPECULAR))
// 			spec *= g_txMaterial[TEX_SPECULAR].Load(layerIndex).rgb;
// 		pageTexel.rgb = lerp(pageTexel.rgb, spec, delta);
// 		
// 		// Write back to the UAV
// 		g_PhysicalTextureUAV[vtSpecular] = D3DX_FLOAT4_to_R8G8B8A8_UNORM(pageTexel);
// 	}
	


	// Check if this is a border texel
	uint2 borderOffset = localCoords;
	uint2 borderPage = page;
	bool border = false;
	uint padSize = 2;
	
	// Left border
	if(localCoords.x < 2*padSize)
	{
		borderPage.x--;
		borderOffset.x = (g_VTPageSize-padSize) + (localCoords.x-padSize);
		border = true;
	}
	// Right border
	else if(localCoords.x > g_VTPageSize-2*padSize)
	{
		borderPage.x++;
		borderOffset.x = (g_VTPageSize-padSize) - localCoords.x;
		border = true;
	}

	// Write to x-border if needed
	uint borderPageID;
	uint2 borderCoords;
	if(border)
	{
		borderPageID = borderPage.y*g_VTPageTableSize + borderPage.x;
		
		// Diffuse
		if(bDiffuse)
		{
			borderCoords = g_PageTable[borderPageID].slot[VT_DIFFUSE].offset + borderOffset;
			g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtDiffuse];
		}

		// Normal
		if(bNormal)
		{
			borderCoords = g_PageTable[borderPageID].slot[VT_NORMAL].offset + borderOffset;
			g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtNormal];
		}

		// Specular
		if(bSpecular)
		{
			borderCoords = g_PageTable[borderPageID].slot[VT_SPECULAR].offset + borderOffset;
			g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtSpecular];
		}
	}
	borderOffset = localCoords;
	borderPage = page;
	border = false;


	// Top border
	if(localCoords.y < 2*padSize)
	{
		borderPage.y--;
		borderOffset.y = (g_VTPageSize-padSize) + (localCoords.y-padSize);
		border = true;
	}
	// Bottom border
	else if(localCoords.y > g_VTPageSize-2*padSize)
	{
		borderPage.y++;
		borderOffset.y = (g_VTPageSize-padSize) - localCoords.y;
		border = true;
	}
	
	// Write to y-border page if needed
	if(border)
	{
		borderPageID = borderPage.y*g_VTPageTableSize + borderPage.x;
		
		// Diffuse
		if(bDiffuse)
		{
			borderCoords = g_PageTable[borderPageID].slot[VT_DIFFUSE].offset + borderOffset;
			g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtDiffuse];
		}

		// Normal
		if(bNormal)
		{
			borderCoords = g_PageTable[borderPageID].slot[VT_NORMAL].offset + borderOffset;
			g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtNormal];
		}

		// Specular
		if(bSpecular)
		{
			borderCoords = g_PageTable[borderPageID].slot[VT_SPECULAR].offset + borderOffset;
			g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtSpecular];
		}
	}
}

#endif

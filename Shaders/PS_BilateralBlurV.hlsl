//--------------------------------------------------------------------------------------
// File: PS_BilateralBlurV.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>
#include <Deferred.hlsli>

// Box-filtered blur
float4 PS_BilateralBlurV( PS_INPUT_TEX input ) : SV_Target
{
	// Preshader
	const float2 sampeDir = int2(0, 1);
	float2 SampleOffset = g_TextureSize.zw * float2(1.0f, 0.0f);
	float2 Offset = 0.5f * float( g_FilterKernel - 1 ) * SampleOffset;
	const float weight = 1.0f / (float)g_FilterKernel;
	float2 BaseTexCoord = input.Tex - Offset;
	int2 baseTexel = BaseTexCoord * g_ScreenSize.xy;
	float d = DL_GetDepth( baseTexel );
	float3 n = DL_GetNormal( baseTexel );

	float sampleCount = 0;
	float4 Sum = 0;
	for ( int i = 0; i < g_FilterKernel; ++i)
	{
		int2 texel = baseTexel + i*sampeDir;
		float di = DL_GetDepth( texel );
		float3 ni = DL_GetNormal( texel );
		if ( abs( di - d ) > 0.02 )
			break;
		Sum += g_txPostFX.SampleLevel( g_samBilinear, BaseTexCoord + i * SampleOffset, 0 );
		++sampleCount;
	}

	return Sum * rcp(sampleCount);
}

//--------------------------------------------------------------------------------------
// File: PS_TextureBlend.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <MaterialEditor.hlsli>



// Box-filtered blur
float4 PS_TextureBlend(PS_INPUT_TEX input) : SV_Target
{
    float4 t1 = g_txBlendBase.SampleLevel(g_samPoint, input.Tex, 0);
    float4 t2 = g_txBlendLayer.SampleLevel(g_samPoint, input.Tex, 0);
    float4 mask = g_txBlendMask.SampleLevel(g_samPoint, input.Tex, 0);
	return lerp(t1, t2, mask);
}

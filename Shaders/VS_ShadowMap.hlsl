//--------------------------------------------------------------------------------------
// File: VS_ShadowMapPCF.hlsl
//
// PCF shadow mapping
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <Common Inputs.hlsli>
#include <Common Vars.hlsli>
#include <ShadowMap.hlsli>

// Vertex shader for shadow map rendering
float4 VS_ShadowMap( VS_INPUT_POS input ) : SV_Position
{
	return mul( input.Pos, g_mShadow );
}

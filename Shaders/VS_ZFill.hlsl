//--------------------------------------------------------------------------------------
// File: VS_ZFill.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>

// Vertex shader for ZFill pass
float4 VS_ZFill( VS_INPUT_POS input ) : SV_Position
{
	return mul( mul( input.Pos, g_mWorld ), g_mViewProjection );
}

//--------------------------------------------------------------------------------------
// File: VS_Basic.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Standard projection
PS_INPUT VS_Basic( VS_INPUT input )
{
    PS_INPUT output;
    output.Pos = mul( input.Pos, g_mWorld );
    output.Pos = mul( output.Pos, g_mViewProjection );
    return output;
}

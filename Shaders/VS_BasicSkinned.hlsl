//--------------------------------------------------------------------------------------
// File: VS_BasicSkinned.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Standard projection
PS_INPUT VS_BasicSkinned( VS_INPUT_SKIN input )
{
    PS_INPUT output;
	SkinVertex(input, output.Pos);

    output.Pos = mul( output.Pos, g_mWorld );
    output.Pos = mul( output.Pos, g_mViewProjection );
    return output;
}

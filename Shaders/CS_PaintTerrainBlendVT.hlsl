//--------------------------------------------------------------------------------------
// File: CS_PaintTerrainBlendVT.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Terrain.hlsli>
#include <Honua.hlsli>

// Compute shader for painting to the terrain virtual texture
// numthreads is hard coded for 252 effective page size!!!
[numthreads( 28, 28, 1 )]
void CS_PaintTerrainBlendVT(uint3 threadID : SV_DispatchThreadID)
{
	// Convert the texel location to world space terrain position
	uint effectivePageSize = g_VTPageSize-4;
	float2 texel = g_PaintPage*effectivePageSize + threadID.xy;
	texel /= g_VTTexSize;
	texel /= g_TerrainVTExtents;
	texel -= 0.5.xx;
	float2 terrainPos = texel * g_HeightmapSize;

	// Ignore areas outside the widget
	float dist = distance(terrainPos, g_MouseSceneIntersect.xz);		
	if(dist>g_TWRadius)
		return;

	// Mask factor and projected UV
	float2 projUV;
	float4 mask = ComputePaintMask(terrainPos, g_MouseSceneIntersect.xz, g_TWRadius, projUV);
	
	// Do the painting
	uint2 localCoords = threadID.xy + uint2(2, 2);
	PaintBlendMapTexelVT(g_PaintPage, localCoords, mask);
}

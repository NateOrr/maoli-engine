//--------------------------------------------------------------------------------------
// File: PS_Basic.hlsl
//
// Just outputs material color
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>

float4 PS_Basic() : SV_Target
{
	return g_MaterialColor;
}

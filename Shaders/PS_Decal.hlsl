//--------------------------------------------------------------------------------------
// File: PS_Decal.hlsl
//
// Deferred decals
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Shading.hlsli>
#include <Deferred.hlsli>

// Decal texture
texture2D g_txDecal;

cbuffer cbDecal
{
	matrix g_DecalMatrix;
	float4 g_DecalColor;
};

void PS_Decal( PS_INPUT_TEX input, out float4 oColor : SV_Target0 )
{
	// Reconstruct the world space position using the depth buffer value
	float depth = DL_GetDepth( input.Pos.xy );
	float3 worldPos = ReconstructPosition( input.Pos.xy, depth );

	// Check object id against decal id
	if ( g_ObjectID != DL_GetID(input.Pos.xy) )
		discard;

	// Compute the decal texture coord by projection into decal space
	float4 positionDecal = mul( float4(worldPos, 1.0f), g_DecalMatrix );
	float2 texCoord = (positionDecal.xy / positionDecal.w) * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
	if ( any( texCoord<0 ) || any( texCoord>1 ) )
		discard;

	// Sample the decal
	oColor = g_txDecal.Sample( g_samBilinear, texCoord ) * g_DecalColor;
}

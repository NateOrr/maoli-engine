//--------------------------------------------------------------------------------------
// File: VS_ForwardReflect.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex shader for forward rendering
PS_INPUT_TEX VS_ForwardReflect( VS_INPUT input)
{
	// Output position and texcoord
	PS_INPUT_TEX output;
	output.Pos = mul(input.Pos,g_mWorld);
	output.Pos = mul(output.Pos, g_mMirror);
	output.Pos = mul(output.Pos,g_mViewProjection);
	output.Tex = input.Tex;
	return output;
}

//--------------------------------------------------------------------------------------
// File: VS_Deferred.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shaders that prep for deferred passed that need reconstructed position


// Standard projection
PS_INPUT_VIEW VS_Deferred( VS_INPUT input )
{
    PS_INPUT_VIEW output;
    output.Pos = mul( input.Pos, g_mWorld );
    output.vPos = output.Pos.xyz-g_CameraPos;
	output.Pos = mul( output.Pos, g_mViewProjection );
    return output;
}

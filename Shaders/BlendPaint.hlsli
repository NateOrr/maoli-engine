//--------------------------------------------------------------------------------------
// File: BlendPaint.hlsli
//
// blend map painting the editor
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _BLENDPAINT_HLSLI_
#define _BLENDPAINT_HLSLI_

// Paints a texel
// r=blend, g=layerID, b=blend, a=layerID
// 2 materials stored
void PaintBlendMapTexelVT(uint2 page, uint2 localCoords, float4 mask)
{
	// Figure out which slots we need
	uint2 vtCoords;
		
	// Get the coords, and do the blend
	// Each component needs to be separately blended
	uint2 effectivePageSize = g_VTPageSize - 4;
	
	// Delta blend factor and pageID
	float delta = 0.2f*g_TWStrength*mask.a;
	uint pageID = g_PaintPage.y*g_VTPageTableSize+g_PaintPage.x;
	
	// Get the texel from the UAV
	vtCoords = localCoords + g_PageTable[pageID].slot[VT_DIFFUSE].offset;
	float4 blend = D3DX_R8G8B8A8_UNORM_to_FLOAT4( g_PhysicalTextureUAV[vtCoords] );

	// Updated blend amount
	[unroll]
	for(int i=0; i<4; i++)
	{
		if(g_LayerID == i)
			blend[i] = clamp(lerp(blend[i], 1.0f, delta), 0.0f, 1.0f);
		else
			blend[i] = clamp(lerp(blend[i], 0.0f, delta), 0.0f, 1.0f);
	}
		
	// Write back to the UAV to the proper channels
	g_PhysicalTextureUAV[vtCoords] = D3DX_FLOAT4_to_R8G8B8A8_UNORM( blend );
	

	// Check if this is a border texel
	uint2 borderOffset = localCoords;
	uint2 borderPage = page;
	bool border = false;
	uint padSize = 2;
	
	// Left border
	if(localCoords.x < 2*padSize)
	{
		borderPage.x--;
		borderOffset.x = (g_VTPageSize-padSize) + (localCoords.x-padSize);
		border = true;
	}
	// Right border
	else if(localCoords.x > g_VTPageSize-2*padSize)
	{
		borderPage.x++;
		borderOffset.x = (g_VTPageSize-padSize) - localCoords.x;
		border = true;
	}

	// Write to x-border if needed
	uint borderPageID;
	uint2 borderCoords;
	if(border)
	{
		borderPageID = borderPage.y*g_VTPageTableSize + borderPage.x;
		borderCoords = g_PageTable[borderPageID].slot[VT_DIFFUSE].offset + borderOffset;
		g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtCoords];
	}
	borderOffset = localCoords;
	borderPage = page;
	border = false;


	// Top border
	if(localCoords.y < 2*padSize)
	{
		borderPage.y--;
		borderOffset.y = (g_VTPageSize-padSize) + (localCoords.y-padSize);
		border = true;
	}
	// Bottom border
	else if(localCoords.y > g_VTPageSize-2*padSize)
	{
		borderPage.y++;
		borderOffset.y = (g_VTPageSize-padSize) - localCoords.y;
		border = true;
	}
	
	// Write to y-border page if needed
	if(border)
	{
		borderPageID = borderPage.y*g_VTPageTableSize + borderPage.x;
		borderCoords = g_PageTable[borderPageID].slot[VT_DIFFUSE].offset + borderOffset;
		g_PhysicalTextureUAV[borderCoords] = g_PhysicalTextureUAV[vtCoords];
	}
}

#endif

//--------------------------------------------------------------------------------------
// File: PS_FXAA.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>
#include <FXAA.hlsli>

// Pixel shader for FXAA
void PS_FXAA( PS_INPUT_TEX input, out float4 oColor : SV_Target )
{
	FxaaTex tex = { g_samAnisotropicClamp, g_txFrameBuffer };
	oColor = float4( FxaaPixelShader(input.Tex, tex, g_ScreenSize.zw), 1.0f );
}

//--------------------------------------------------------------------------------------
// File: VS_OrthoTex.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Ortho2D projection with tex coords
PS_INPUT_TEX VS_OrthoTex( VS_INPUT input )
{
    PS_INPUT_TEX output;
    output.Pos = float4(input.Pos.xyz, 1);   
    output.Tex = input.Tex; 
    return output;
}

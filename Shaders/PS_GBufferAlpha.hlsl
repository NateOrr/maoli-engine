//--------------------------------------------------------------------------------------
// File: PS_GBuffer.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <MaterialHelper.hlsli>
#include <Deferred.hlsli>


// Pixel shader for GBuffer pass
DL_Output PS_GBufferAlpha( PS_INPUT_GBUFFER input )
{
	AlphaTest(input.Tex);
	
	// Init the output struct
	DL_Output oBuf;

	// Base color
	oBuf.Color = GetMaterialColor( input.Tex );

	// Shading normal and polygon normal
	float3 normal = GetNormal( input.Tex, normalize( input.Norm ), input.WPos );
	float3 flatNormal = normalize( cross( ddx_fine( input.WPos ), ddy_fine( input.WPos ) ) );
	oBuf.Normal = EncodeNormal( normal );
	oBuf.FlatNormal = EncodeNormal( flatNormal );

	// Material properties
	oBuf.Properties[MAT_ROUGHNESS] = g_MaterialRoughness;
	oBuf.Properties[MAT_METALIC] = g_MaterialMetalic;
	oBuf.ObjectID = g_ObjectID;
	
	// Emissive color
	oBuf.Emissive = g_MaterialEmissive * g_txMaterial[TEX_EMISSIVE].Sample( g_samLinear, input.Tex );

	return oBuf;
}

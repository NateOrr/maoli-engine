//--------------------------------------------------------------------------------------
// File: VS_GBufferVegetation.hlsl
//
// Vertex Shader for GBuffer pass with procedural
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>

struct VegetationAnimationProperties
{
	float	speed;
	float	detailFreq;
	float	edgeAtten;
	float	detailAmp;
	float	branchAtten;
	float	branchAmp;
	float	bendScale;
	float	pad;				// To keep 16 byte aligned

	float3  wind;
	float   pad2;
};

cbuffer cbVegetation
{
	VegetationAnimationProperties g_Vegetation;
};

// Vertex Shader for GBuffer pass with procedural
PS_INPUT_GBUFFER VS_GBufferVegetation( VS_INPUT input )
{
	PS_INPUT_GBUFFER output;

	// Transform to world space
	output.Pos = mul( input.Pos, g_mWorld );
	float3 worldPos = output.Pos.xyz;

	// Phases (object, vertex, branch)  
	float fDetailPhase = 0.5f;			// From texture
	float fBranchPhase = 0.75f;			// From texture

	float fObjPhase = dot( output.Pos.xyz, 1 );
	fBranchPhase += fObjPhase;
	float fVtxPhase = dot( input.Pos.xyz, fDetailPhase + fBranchPhase );

	// x is used for edges; y is used for branches  
	float2 vWavesIn = g_Timer + float2(fVtxPhase, fBranchPhase);

	// 1.975, 0.793, 0.375, 0.193 are good frequencies  
	float4 vWaves = (frac( vWavesIn.xxyy *
	float4(1.975, 0.793, 0.375, 0.193) ) *
	2.0 - 1.0) * g_Vegetation.speed * g_Vegetation.detailFreq;
	vWaves = SmoothTriangleWave( vWaves );
	float2 vWavesSum = vWaves.xz + vWaves.yw;

	// Edge (xy) and branch bending (z)  
	output.Pos.xyz += vWavesSum.xxy * float3(g_Vegetation.edgeAtten * g_Vegetation.detailAmp *
	input.Norm.xy, g_Vegetation.branchAtten * g_Vegetation.branchAmp);

	// Bend factor - Wind variation is done on the CPU.  
	//float fBF = output.Pos.z * g_Vegetation.bendScale;  

	//// Smooth bending factor and increase its nearby height limit.  
	//fBF += 1.0;  
	//fBF *= fBF;  
	//fBF = fBF * fBF - fBF;  

	//// Displace position  
	//float3 vNewPos = output.Pos.xyz;  
	//vNewPos += g_Vegetation.wind * fBF;  

	//// Rescale with respect to distance of vertex to the model center
	//float fLength = length(worldPos);
	//output.Pos.xyz = normalize(vNewPos.xyz)* fLength;  

	// Compute world space normal
	output.Norm = mul( input.Norm, (float3x3)g_mWorld );

	// Apply wind
	float windFactor = dot( output.Norm, g_Vegetation.wind );
	output.Pos.xyz += windFactor * output.Norm;

	// Save the world space position
	output.WPos = output.Pos.xyz;

	// Project
	output.Pos = mul( output.Pos, g_mViewProjection );
	output.Tex = input.Tex;

	return output;
}

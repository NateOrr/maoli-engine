//--------------------------------------------------------------------------------------
// File: PS_BoxBlurV.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>

// Box-filtered blur
float4 PS_BoxBlurV(PS_INPUT_TEX input) : SV_Target
{
    // Preshader
    float2 SampleOffset = g_TextureSize.zw * float2(0.0f, 1.0f);
	float2 Offset = 0.5f * float( g_FilterKernel - 1 ) * SampleOffset;
	const float weight = 1.0f / (float)g_FilterKernel;
	float2 BaseTexCoord = input.Tex - Offset;

		// NOTE: This loop can potentially be optimized to use bilinear filtering to take
		// two samples at a time, rather than handle even/odd filters nicely. However the
		// resulting special-casing required for different filter sizes will probably
		// negate any benefit of fewer samples being taken. Besides, this method is already
		// supidly-fast even for gigantic kernels.
		float4 Sum = 0;
	for ( int i = 0; i < g_FilterKernel; i++ )
	{
		Sum += weight * g_txPostFX.SampleLevel( g_samBilinear, BaseTexCoord + i * SampleOffset, 0 );
	}

	return Sum;
}

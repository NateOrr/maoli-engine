//--------------------------------------------------------------------------------------
// File: VS_ToneMap.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <HDR.hlsli>


// Pixel shader for tonemapping
PS_INPUT_TONEMAP VS_ToneMap( VS_INPUT input )
{
    PS_INPUT_TONEMAP output;
    output.Pos = float4(input.Pos.xyz, 1);   
    
    // Texture coords 
    output.TexSamples[0] = input.Tex + float2( g_ScreenSize.z, g_ScreenSize.w);
    output.TexSamples[1] = input.Tex + float2( -g_ScreenSize.z, g_ScreenSize.w);
    output.TexSamples[2] = input.Tex + float2( g_ScreenSize.z, -g_ScreenSize.w);
    output.TexSamples[3] = input.Tex + float2( -g_ScreenSize.z, -g_ScreenSize.w);
    output.Tex = input.Tex;
    
    // Luminance
    output.Lum = g_txAdaptedLuminance.Load(int3(0,0,0));
    
    return output;
}

//--------------------------------------------------------------------------------------
// File: PS_ShadowMapAlphaSDSM.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <Common Inputs.hlsli>
#include <Common Vars.hlsli>
#include <ShadowMap.hlsli>
#include <Common Functions.hlsli>
#include <MaterialHelper.hlsli>


void PS_ShadowMapAlphaSDSM( float4 iPosition  : SV_Position, sample float2 iTexCoord : TEXCOORD )
{
	AlphaTest(iTexCoord);
}

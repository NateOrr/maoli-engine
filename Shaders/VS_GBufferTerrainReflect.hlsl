//--------------------------------------------------------------------------------------
// File: VS_GBufferTerrainReflect.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Terrain.hlsli>

// Vertex Shader for GBuffer pass for terrain using vertex texturing
PS_INPUT_TERRAIN VS_GBufferTerrainReflect( VS_INPUT_POS input)
{
    PS_INPUT_TERRAIN output;
    
    // Position based on the patch
    output.WPos.xz = input.Pos.xz*g_ClipmapScale + g_ClipmapOffset;
    
    // Compute the texture coords based on the position
    float isize = 1.0f / (g_ClipmapScale*g_ClipmapSize);
    output.CTex = (output.WPos.xz + g_ClipmapScale) * isize + 0.5f;
    
    // Adjust the position to fit inside the next coarser level
    output.WPos.xz += g_ClipmapFix;
    
    // Sample the height and normal at this location
    output.WPos.y = g_txClipmap.SampleLevel(g_samPoint, output.CTex, 0).r;

	// Compute the height for the next coarser level, and fix the T-Gaps
	float zc;
	int2 pos = int2(output.CTex*g_ClipmapSize);
    uint2 mod2 = pos%2U;
    const float texel = 1.0f / g_ClipmapSize;
	if(mod2.x==0&&mod2.y==1)
	{
		float2 coordOffset = float2(0, texel);
		zc = (g_txClipmap.SampleLevel(g_samPoint, output.CTex + coordOffset, 0).r+ 
			  g_txClipmap.SampleLevel(g_samPoint, output.CTex - coordOffset, 0).r) * 0.5f;
		if(pos.x==0||pos.x==g_ClipmapSize-1)
			output.WPos.y = zc;
	}
	else if(mod2.x==1&&mod2.y==0)
	{
		float2 coordOffset = float2(texel, 0);
		zc = (g_txClipmap.SampleLevel(g_samPoint, output.CTex + coordOffset, 0).r+ 
			  g_txClipmap.SampleLevel(g_samPoint, output.CTex - coordOffset, 0).r) * 0.5f;
		if(pos.y==0||pos.y==g_ClipmapSize-1)
			output.WPos.y = zc;
	}
	else if(mod2.x==1&&mod2.y==1)
	{
		float2 coordOffsetX = float2(texel, 0);
		float2 coordOffsetY = float2(0, texel);
		zc = (g_txClipmap.SampleLevel(g_samPoint, output.CTex + coordOffsetX + coordOffsetY, 0).r+ 
			  g_txClipmap.SampleLevel(g_samPoint, output.CTex - coordOffsetX + coordOffsetY, 0).r+
			  g_txClipmap.SampleLevel(g_samPoint, output.CTex + coordOffsetX - coordOffsetY, 0).r+ 
			  g_txClipmap.SampleLevel(g_samPoint, output.CTex - coordOffsetX - coordOffsetY, 0).r) * 0.25f;
	}
	else
	{
		zc = output.WPos.y;
	}
		
	// Compute the transition blending factors
	float wFactor = (g_ClipmapSize*g_ClipmapScale) / 10.0f;
	float ax = clamp( (abs(output.WPos.x-g_CameraPos.x) - ((g_ClipmapSize*g_ClipmapScale-1)/2 - wFactor - 1)) / wFactor, 0, 1);
	float ay = clamp( (abs(output.WPos.z-g_CameraPos.z) - ((g_ClipmapSize*g_ClipmapScale-1)/2 - wFactor - 1)) / wFactor, 0, 1);
		
	// Blend the transition heights
	output.WPos.y = lerp(output.WPos.y, zc, max(ax, ay));
	
	// Scale the height
	output.WPos.y *= g_HeightScale;
	
	// Store the full tex coords
	float iheightsize = 1.0f / g_HeightmapSize;
	output.Tex = output.WPos.xz*iheightsize + 0.5;	
	   
    // Transform for reflect
	//output.Pos = mul( float4(output.WPos.x, -output.WPos.y+2*g_WaterLevel, output.WPos.z, 1), g_mViewProjection );
	output.Pos = mul( float4(output.WPos, 1), g_mMirror );
	output.Pos = mul( output.Pos, g_mViewProjection );

	output.Pos = mul( float4(output.WPos, 1), g_mViewProjection );
    return output;
}

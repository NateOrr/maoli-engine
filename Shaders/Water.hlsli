//--------------------------------------------------------------------------------------
// File: Water.fxh
//
// Water rendering as a post process
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _WATER_HLSLI_
#define _WATER_HLSLI_

cbuffer cbWater : register(b8)
{
	float4 g_WaterUV;
	float4 g_WaterExtinctionColor;
	float4 g_WaterBackscatterColor;
	float  g_WaterLevel;
}
Texture2D<float3> g_txWater;
Texture2D g_txWaterNormals;

#define GRID_SIZE 128U

struct PS_INPUT_WATER
{
	float4 Pos    : SV_POSITION;
	float2 Tex    : TEXCOORD0;
	float3 WPos   : TEXCOORD1;
	float3 Normal : TEXCOORD2;
};



#endif

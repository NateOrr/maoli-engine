//--------------------------------------------------------------------------------------
// File: VS_Ortho.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Ortho2D projection
PS_INPUT VS_Ortho( VS_INPUT input )
{
    PS_INPUT output;
    output.Pos = float4(input.Pos.xyz, 1);   
    return output;
}

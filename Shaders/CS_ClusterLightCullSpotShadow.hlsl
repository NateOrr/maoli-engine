//--------------------------------------------------------------------------------------
// File: CS_ClusterLightCullSpotShadow.hlsl
//
// Assign shadow casting spot lights to the clusters they affect
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Shading.hlsli>
#include <Culling.hlsli>

#define DISPATCH_SIZE 64

[numthreads( DISPATCH_SIZE, 1, 1 )]
void CS_ClusterLightCullSpotShadow( uint3 threadId : SV_DispatchThreadID )
{
	uint lightIndex = threadId.x;
	if ( lightIndex < g_NumShadowSpotLights )
	{
		// Get the range of clusters that the light belongs to
		int3 min, max;
		if ( !GetLightClusterRange( (Light)g_SpotLights[lightIndex], min, max ) )
			return;

		// Add the light to the clusters
		uint index;
		for ( int ty = min.y; ty <= max.y; ++ty )
		{
			for ( int tx = min.x; tx <= max.x; ++tx )
			{
				int clusterID = ty*g_NumSlices*g_TileWidth + tx*g_NumSlices;
				for ( int i = min.z; i <= max.z; ++i )
				{
					// Add to the index list
					InterlockedAdd( g_IndexOffset[i + clusterID], 1, index );
					g_ClusterIndicesRW[index] = lightIndex;

					// Update the counter
					InterlockedAdd( g_IndexDataRW[i + clusterID].numShadowSpotLights, 1 );
				}
			}
		}
	}
}
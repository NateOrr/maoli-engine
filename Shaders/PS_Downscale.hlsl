//--------------------------------------------------------------------------------------
// File: PS_Downscale.hlsl
//
// Downscale the texture
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>

float4 PS_Downscale( PS_INPUT_TEX input ) : SV_Target
{
	return g_txPostFX.SampleLevel( g_samBilinear, input.Tex, 0 );
}
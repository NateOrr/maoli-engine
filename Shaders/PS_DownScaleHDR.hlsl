//--------------------------------------------------------------------------------------
// File: PS_DownScaleHDR.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <HDR.hlsli>


// Pixel shader for computing luminance from the HDRI
void PS_DownScaleHDR( PS_INPUT_DOWNSCALE input, out float2 oColor : SV_Target)
{
	// Sample the luminance from surrounding pixels
	float sample1 = dot(g_txHDR.Sample(g_samPoint, input.Tex ).rgb, g_LuminanceVector);
	float sample2 = dot(g_txHDR.Sample(g_samPoint, input.TexSamples[0] ).rgb, g_LuminanceVector);
	float sample3 = dot(g_txHDR.Sample(g_samPoint, input.TexSamples[1] ).rgb, g_LuminanceVector);
	float sample4 = dot(g_txHDR.Sample(g_samPoint, input.TexSamples[2] ).rgb, g_LuminanceVector);
	float sample5 = dot(g_txHDR.Sample(g_samPoint, input.TexSamples[3] ).rgb, g_LuminanceVector);
	
	// Max
	oColor.g = max(sample1, max(sample2, max(sample3, min(sample5, sample4) ) ) );
	
	// Use the log of luminance for better result of averaging
	sample1 = log(sample1 + 0.0001f);
	sample2 = log(sample2 + 0.0001f);
	sample3 = log(sample3 + 0.0001f);
	sample4 = log(sample4 + 0.0001f);
	sample5 = log(sample5 + 0.0001f);
	
	// Avg
	oColor.r = (sample1+sample2+sample3+sample4+sample5) * 0.2f;
}

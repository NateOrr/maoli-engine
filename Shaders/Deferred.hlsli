//--------------------------------------------------------------------------------------
// File: Deferred.hlsli
//
// Deferred rendering functions for encapsulation
//
// Deferred shading gbuffers are organized as follows:
//
//		GBuffer[0].r = BaseColor.r
//		GBuffer[0].g = BaseColor.g
//		GBuffer[0].b = BaseColor.b
//		GBuffer[0].a = Metallic
//
//		GBuffer[1].r = Normal.x
//		GBuffer[1].g = Normal.y
//		GBuffer[1].b = Normal.z
//		GBuffer[1].a = Roughness
//
//
// Nate Orr
//-------------------------------------------------------------------------------------
#ifndef _DEFERRED_HLSLI_
#define _DEFERRED_HLSLI_

#include <Common Functions.hlsli>

// Bilinear weights
static const float g_BilinearWeights[4] = { 9.0 / 16.0, 3.0 / 16.0, 3.0 / 16.0, 1.0 / 16.0 };
static const float2 g_BilinearOffsets[4] = { float2(0, 0), float2(1, 0), float2(0, 1), float2(1, 1) };

//--------------------------------------------------------------------------------------
// GBuffer textures
Texture2D g_txColor;
Texture2D<float2> g_txNormal;
Texture2D<float2> g_txFlatNormal;
Texture2D<float2> g_txProperties;
Texture2D<float> g_txDepth;
Texture2D g_txEmissive;
Texture2D<uint> g_txObjectID;

// Property indices
#define MAT_ROUGHNESS 0
#define MAT_METALIC 1

// GBuffer output
struct DL_Output
{
	float4 Color		 : SV_Target0;	// Base Color
	float2 Normal		 : SV_Target1;  // Normal
	float2 FlatNormal	 : SV_Target2;  // Polygon Normal
	float2 Properties    : SV_Target3;  // Material properties
	float4 Emissive		 : SV_Target4;  // Emissive color
	uint   ObjectID		 : SV_Target5;  // Emissive color
};

// Compress a normal into 2 channels
#define kPI 3.1415926536f
float2 EncodeNormal( float3 n )
{
	return (float2(atan2( n.y, n.x ) / kPI, n.z) + 1.0f)*0.5f;
}

// Decompress a normal
float3 DecodeNormal( float2 enc )
{
	float2 ang = enc * 2 - 1;
		float2 scth;
	sincos( ang.x * kPI, scth.x, scth.y );
	float2 scphi = float2(sqrt( 1.0 - ang.y*ang.y ), ang.y);
		return float3(scth.y*scphi.x, scth.x*scphi.x, scphi.y);
}

// Diffuse color
float4 DL_GetColor( uint2 screenPos )
{
	return g_txColor[screenPos];
}

// Normal / position derivitives
float3 DL_GetNormal( uint2 screenPos )
{
	return DecodeNormal( g_txNormal[screenPos] );
}

// Material properties
float2 DL_GetProperties( uint2 screenPos )
{
	return g_txProperties[screenPos];
}

// Linear depth
float DL_GetDepth( uint2 screenPos )
{
	return GetLinearDepth( g_txDepth[screenPos] );
}

// Emissive color
float4 DL_GetEmissiveColor( float2 texCoord )
{
	return g_txEmissive.SampleLevel( g_samBilinear, texCoord, 0 );
}

// Object ID
uint DL_GetID( uint2 screenPos )
{
	return g_txObjectID[screenPos];
}


/*float3 ReconstructPosition( float2 screenPos )
{
// Get the depth value for this pixel
float z = g_txDepth[screenPos];

// Get x/w and y/w from the viewport position
float2 vTexCoord = screenPos * g_ScreenSize.zw;
float x = vTexCoord.x * 2 - 1;
float y = (1 - vTexCoord.y) * 2 - 1;
float4 vProjectedPos = float4(x, y, z, 1.0f);

// Transform by the inverse projection matrix
float4 vPositionWS = mul( vProjectedPos, g_mInvViewProjection );

// Divide by w to get the view-space position
return vPositionWS.xyz / vPositionWS.w;
}*/

// Depth weighting for bilateral filter
float GetBilateralDepthWeight( float d, float di )
{
	//return pow( rcp( 1.0 + abs( di - d ) ), 16 );
	return saturate( rcp( 0.0001 + abs( di - d ) ) );
}

// Normal weighting for bilateral filter
float GetBilateralNormalWeight( float3 n, float3 ni )
{
	//return pow( (dot( n, ni ) + 1.0) / 2.0, 8 );
	return saturate( pow( saturate(dot( n, ni )), 32 ) );
}

// Bilateral filtering read
float4 Bilateral4( Texture2D tex, int2 texCoord, float d, float3 n )
{
	float4 blend = 0;
		uint3 coord = 0;

		[unroll]
	for ( int i = 0; i < 4; ++i )
	{
		coord.xy = texCoord + g_BilinearOffsets[i];
		float di = DL_GetDepth( coord.xy * 2 );
		float3 ni = DL_GetNormal( coord.xy * 2 );
			float wn = GetBilateralNormalWeight( n, ni );
		float wd = GetBilateralDepthWeight( d, di );
		blend += g_BilinearWeights[i] * wn * wd * tex.Load( coord );
	}

	return blend;
}

// Bilateral filtering read
float Bilateral1( Texture2D tex, int2 texCoord, float d, float3 n )
{
	float blend = 0;
	uint3 coord = 0;

		[unroll]
	for ( int i = 0; i < 4; ++i )
	{
		coord.xy = texCoord + g_BilinearOffsets[i];
		float di = DL_GetDepth( coord.xy * 2 );
		float3 ni = DL_GetNormal( coord.xy * 2 );
			float wn = GetBilateralNormalWeight( n, ni );
		float wd = GetBilateralDepthWeight( d, di );
		float w = g_BilinearWeights[i] * wn * wd;
		blend += w * tex.Load( coord ).r;
	}

	return blend;
}

#endif

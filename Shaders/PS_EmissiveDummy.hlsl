#include <Common Vars.hlsli>

float4 PS_EmissiveDummy( ) : SV_TARGET
{
	return g_txMaterial[TEX_EMISSIVE].Sample( g_samPoint, float2(0, 0) );
}
//--------------------------------------------------------------------------------------
// File: PS_LightenObject.hlsl
//
// Re-light an object
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Stranded.hlsli>

float4 PS_LightenObject( PS_INPUT input ) : SV_Target
{
	float2 Tex = input.Pos.xy * g_ScreenSize.zw;
	return g_txFrameBuffer.Sample( g_samBilinear, Tex );
}

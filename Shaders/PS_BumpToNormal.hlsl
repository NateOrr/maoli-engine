//--------------------------------------------------------------------------------------
// File: PS_BumpToNormal.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <MaterialEditor.hlsli>

// Compute a normal map from a bump map
float4 PS_BumpToNormal(PS_INPUT_TEX input) : SV_Target
{
	// Get the texel size
	float2 pixelSize;
	g_txBlendBase.GetDimensions(pixelSize.x, pixelSize.y);
	pixelSize = rcp(pixelSize);

	const float scale = 10.0f; // make input
	const float2 size = float2(2.0,0.0);
	const float3 off = float3(-1,0,1);

    float s11 = g_txBlendBase.SampleLevel(g_samLinear, input.Tex, 0).x * scale;
    float s01 = g_txBlendBase.SampleLevel(g_samLinear, input.Tex + off.xy*pixelSize, 0).x * scale;
    float s21 = g_txBlendBase.SampleLevel(g_samLinear, input.Tex + off.zy*pixelSize, 0).x * scale;
    float s10 = g_txBlendBase.SampleLevel(g_samLinear, input.Tex + off.yx*pixelSize, 0).x * scale;
    float s12 = g_txBlendBase.SampleLevel(g_samLinear, input.Tex + off.yz*pixelSize, 0).x * scale;
    float3 va = normalize(float3(size.xy,s21-s01));
    float3 vb = normalize(float3(size.yx,s12-s10));
    float3 bump = cross(va,vb);

	// Store height alpha
	return float4( (bump+1.0f)/2.0f, s11);
}

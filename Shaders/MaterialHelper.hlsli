//--------------------------------------------------------------------------------------
// File: MaterialHelper.fxh
//
// Helper functions that use materials
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _MATERIALHELPER_HLSLI_
#define _MATERIALHELPER_HLSLI_

#include <ConeStepMapping.hlsli>


//-----------------------------------
// Check if a texture is active
// -----------------------------------
bool CheckMaterialTexture(uint i)
{
	return (g_MaterialTextureFlag & (1U<<i));
}


//-----------------------------------
// Get the material texture color
//-----------------------------------
float4 GetMaterialColor( float2 Tex )
{
	float4 color = g_MaterialColor;
	if(CheckMaterialTexture(TEX_DIFFUSE))
		color *= g_txMaterial[TEX_DIFFUSE].Sample( g_samAnisotropic, Tex );
	return color;
}

//-----------------------------------
// Get emissive
//-----------------------------------
float4 GetMaterialEmissive(float2 Tex)
{
	if(CheckMaterialTexture(TEX_EMISSIVE))
		return g_txMaterial[TEX_EMISSIVE].Sample( g_samLinear, Tex ) * g_MaterialEmissive; 
	return g_MaterialEmissive;
}

//-----------------------------------
// Get the normal based on the bump mapping mode
//-----------------------------------
float3 GetNormal(inout float2 Tex, float3 Normal, float3 Position)
{
	// Standard normal mapping
	float3 n = Normal;
	if(CheckMaterialTexture(TEX_HEIGHT))
	{
		n = CSM_GetNormal(Position, Normal, Tex, ddx(Tex), ddy(Tex));
	}
	else if(CheckMaterialTexture(TEX_NORMAL))
	{
		float3x3 mTan = ComputeTangentFrame( Normal, Position, Tex);
		n = normalize( mul(g_txMaterial[TEX_NORMAL].Sample( g_samLinear, Tex ).xyz*2.0f-1.0f, mTan) );
	}
	return n;
}

// Alpha testing: automatically discards pixels that fail
void AlphaTest(float2 Tex)
{
	if ( CheckMaterialTexture( TEX_DIFFUSE ) && g_txMaterial[TEX_DIFFUSE].Sample( g_samAnisotropic, Tex ).a < 0.05f )
		discard;
}

#endif

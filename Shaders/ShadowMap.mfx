//--------------------------------------------------------------------------------------
// Shadow map rendering
//--------------------------------------------------------------------------------------

Effect ShadowMap
{
    VertexShader = VS_ShadowMap;

	RasterizerState = ShadowMapRS;
	BlendState = NoColorWritesBS;
};

Effect ShadowMapSkinned
{
    VertexShader = VS_ShadowMapSkinned;

	RasterizerState = ShadowMapRS;
	BlendState = NoColorWritesBS;
};

Effect ShadowMapSDSM
{
    VertexShader = VS_ShadowMapSDSM;
        
    RasterizerState = SDSMShadowMapRS;
	BlendState = NoColorWritesBS;
};

Effect ShadowMapTerrainSDSM
{
    VertexShader = VS_ShadowMapTerrainSDSM;
        
    RasterizerState = SDSMShadowMapRS;
	BlendState = NoColorWritesBS;
};

Effect ShadowMapInstancedSDSM
{
    VertexShader = VS_ShadowMapInstancedSDSM;
        
    RasterizerState = SDSMShadowMapRS;
	BlendState = NoColorWritesBS;
};

Effect ShadowMapAlphaInstancedSDSM
{
    VertexShader = VS_ShadowMapAlphaInstancedSDSM;
    PixelShader = PS_ShadowMapAlphaSDSM;
        
    RasterizerState = SDSMShadowMapAlphaRS;
	BlendState = NoColorWritesBS;
};

Effect ShadowMapAlphaSDSM
{
    VertexShader = VS_ShadowMapAlphaSDSM;
    PixelShader = PS_ShadowMapAlphaSDSM;
        
    RasterizerState = SDSMShadowMapAlphaRS;
	BlendState = NoColorWritesBS;
};

Effect ShadowMapSkinnedSDSM
{
    VertexShader = VS_ShadowMapSkinnedSDSM;
        
    RasterizerState = SDSMShadowMapRS;
	BlendState = NoColorWritesBS;
};

Effect MSAAtoEVSM
{
    VertexShader = VS_DeferredOrtho;
    PixelShader = PS_MSAAtoEVSM;
        
    DepthStencilState = DisableDepthDS;
};

Effect ResolveEVSM
{
    VertexShader = VS_DeferredOrtho;
    PixelShader = PS_ResolveEVSM;
        
    DepthStencilState = DisableDepthDS;
};


 Effect ClearPartitions
 {
	ComputeShader = CS_ClearPartitions;
 };


 Effect PartitionBounds
 {
	ComputeShader = CS_PartitionBounds;
 };


 Effect Partitions
 {
	ComputeShader = CS_Partitions;
 };

 Effect BuildPartitions
 {
	ComputeShader = CS_BuildPartitions;
 };
 	
 Effect ReducePartitionBounds
 {
	ComputeShader = CS_ReducePartitionBounds;
 };

 Effect ClearPartitionBounds
 {
	ComputeShader = CS_ClearPartitionBounds;
 };

// Partition box blur
Effect BoxBlurSDSM
{
    VertexShader = VS_BoxBlurSDSM;
    PixelShader = PS_BoxBlurSDSM;
        
    DepthStencilState = DisableDepthDS;
};

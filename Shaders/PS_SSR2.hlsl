//--------------------------------------------------------------------------------------
// File: PS_SSR.hlsl
//
// Screen space reflection
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>
#include <Deferred.hlsli>

float4 PS_SSR2( PS_INPUT_TEX input ) : SV_Target
{
	// Make sure we are dealing with a metalic surface
	float2 props = DL_GetProperties( input.Pos.xy );
	if ( props[MAT_METALIC] < 0 )
		discard;

	// Get the initial depth and reflection ray
	float depth = DL_GetDepth( input.Pos.xy );
	float3 p = ReconstructPosition( input.Pos.xy, depth );
	float3 n = DL_GetNormal( input.Pos.xy );
	float3 v = normalize( p - g_CameraPos );
	float3 r = reflect( v, n );
	float3 rayPos = mul( float4(p, 1), g_mView ).xyz;
	float3 p1 = mul( float4(p + r, 1), g_mView ).xyz;
	float3 rayDir = normalize( p1 - rayPos );

	if ( rayDir.z < 0 )
		discard;

	// March the ray until a colision is reached
	float4 tex;
	float3 hitPoint;
	for ( int i = 0; i<8; ++i )
	{
		hitPoint = rayPos + rayDir;

		tex = mul( float4(hitPoint, 1), g_mProj );
		tex.xy = (tex.xy / tex.w) * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
		if ( any( tex.xy < 0 ) || any( tex.xy >= 1.0 ) )
			discard;

		depth = RescaleDepth( DL_GetDepth( tex.xy * g_ScreenSize.xy ) );

		// Fine binary search
		if ( depth < hitPoint.z )
		{
			for ( int j = 0; j < 8; ++j )
			{
				float3 testPoint = (rayPos + hitPoint) * 0.5f;

				tex = mul( float4(testPoint, 1), g_mProj );
				tex.xy = (tex.xy / tex.w) * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);

				depth = RescaleDepth( DL_GetDepth( tex.xy * g_ScreenSize.xy ) );

				if ( depth < testPoint.z )
					hitPoint = testPoint;
				else
					rayPos = testPoint;
			}
			return g_txPostFX.SampleLevel( g_samBilinear, tex.xy, 0 );
		}

		rayPos = hitPoint;
	}

	discard;
	return 0;
}
//--------------------------------------------------------------------------------------
// File: CS_ClearPartitionBounds.hlsl
//
// Compute min/max forthe shadow map partitions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>

[numthreads(SDSM_PARTITIONS, 1, 1)]
void CS_ClearPartitionBounds(uint  groupIndex : SV_GroupIndex)
{
    g_PartitionBoundsUint[groupIndex] = EmptyPartitionBoundsUint();
}

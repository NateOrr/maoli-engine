//--------------------------------------------------------------------------------------
// File: Partition.hlsli
//
// Shadow map partitioning for directional lights
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _PARTITION_HLSLI_
#define _PARTITION_HLSLI_

#include <Deferred.hlsli>

// Constants
#define SDSM_PARTITIONS (4)
#define SDSM_TILE_SIZE (128)

// Frustum partition data
struct Partition
{
    float intervalBegin;
    float intervalEnd;
    
    // These are given texture coordinate [0, 1] space
    float3 scale;
    float3 bias;
};

// Uint version for the partion data
struct PartitionUint
{
    uint intervalBegin;
    uint intervalEnd;
    
    // These are given texture coordinate [0, 1] space
    uint3 scale;
    uint3 bias;
};

// Parition bounds data
struct PartitionBounds
{
    float3 minCoord;
    float3 maxCoord;
};

// Uint version for the partion bounds data
struct PartitionBoundsUint
{
    uint3 minCoord;
    uint3 maxCoord;
};

// Reset bounds to [0, maxFloat]
PartitionBoundsUint EmptyPartitionBoundsUint()
{
    PartitionBoundsUint b;
    b.minCoord = uint(0x7F7FFFFF).xxx;
    b.maxCoord = uint(0).xxx;
    return b;
}

// Reset bounds to [0, maxFloat]
PartitionBounds ResetPartitionBoundsFloat()
{
    PartitionBounds b;
    b.minCoord = asfloat( uint(0x7F7FFFFF).xxx );
    b.maxCoord = asfloat( uint(0).xxx );
    return b;
}

// Constant buffers
cbuffer cbPartition
{
	int		g_PartitionID;		// Current partition to process
    float2	g_BlurFilterSize;   // Filter size light view space [0,1] (unaffected by partitioning)
    uint	g_BlurDirection;    // Which dimension to blur (0 = horiz, 1 = vert)
}

// Buffers
RWStructuredBuffer<Partition>			g_Partitions;				// Float partition uav
RWStructuredBuffer<PartitionUint>		g_PartitionsUint;			// Uint partition uav
StructuredBuffer<Partition>				g_PartitionsReadOnly;		// Partition srv
RWStructuredBuffer<PartitionBoundsUint> g_PartitionBoundsUint;		// Uint bounds uav
StructuredBuffer<PartitionBounds>		g_PartitionBoundsReadOnly;	// Bounds srv

// Utility to work out blur metadata
struct BlurData
{
    uint interiorSamplesLoopCount : interiorSamplesLoopCount;
    float interiorSampleWeight : interiorSampleWeight;
    float edgeSampleWeight : edgeSampleWeight;
};


BlurData ComputeBlurData(float filterSizeTexels)
{
    BlurData output;
    
    // Just draw the diagram to understand... :)
    float sideSamples = 0.5f * filterSizeTexels - 0.5f;
    output.edgeSampleWeight = modf(sideSamples, output.interiorSamplesLoopCount);

    // Normalize weights
    output.interiorSampleWeight = 1.0f / filterSizeTexels;
    output.edgeSampleWeight *= output.interiorSampleWeight;
    
    return output;
}


// Box blur shader input
struct BoxBlurVSOut
{
    float4 Position : SV_Position;
   
    nointerpolation BlurData blurData;	// Const per poly
};


#endif

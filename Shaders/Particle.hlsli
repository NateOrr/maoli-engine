//--------------------------------------------------------------------------------------
// File: Particle.hlsli
//
// Instanced particle rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _PARTICLE_HLSLI_
#define _PARTICLE_HLSLI_

// Particle ps input
struct VS_INPUT_PARTICLE
{
	float4 Pos				: POSITION;
	float2 Tex				: TEXCOORD0;
	float3 Norm				: NORMAL;

	float4 ParticlePos		: ParticlePos;
	float4 ParticleColor	: ParticleColor;
	float3 ParticleSize     : ParticleSize;
	uint   SpriteFrame		: SpriteFrame;
};

struct PS_INPUT_PARTICLE
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float4 Color : COLOR;
};

texture2D g_txParticle;

#endif

//--------------------------------------------------------------------------------------
// File: VS_Instanced.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shader for pass with instancing
PS_INPUT_SHADE VS_Instanced( VS_INPUT_INSTANCED input )
{
	PS_INPUT_SHADE output;

	output.Pos = mul( input.Pos, input.WorldMatrix );
	output.WPos = output.Pos.xyz;
	output.ViewPos = mul( output.Pos, g_mView ).xyz;
	output.Pos = mul( output.Pos, g_mViewProjection );
	output.Tex = input.Tex;
	output.Norm = mul( input.Norm, (float3x3)input.WorldMatrix );

	return output;
}

//--------------------------------------------------------------------------------------
// File: PS_MSAAtoEVSMforSDSM.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <Common Inputs.hlsli>
#include <Common Vars.hlsli>
#include <ShadowMap.hlsli>

// 4 samples seem to work well enough
#define NUM_SAMPLES 8
Texture2DMS<float, NUM_SAMPLES> g_txDepthMS;

// Builds a variance shadow map
float2 PS_MSAAtoEVSM( PS_INPUT_VIEW input ) : SV_Target
{
	float2 moments = 0;
	const float weight = 1.0f / float( NUM_SAMPLES );
	float exponent = GetExponentESM( g_PartitionsReadOnly[g_PartitionID] );

	[unroll]
	for ( int i = 0; i < NUM_SAMPLES; ++i )
	{
		float depth = g_txDepthMS.Load( input.Pos.xy, i );
		float warpedDepth = WarpDepth( depth, exponent );
		moments += weight * ComputeMoments( warpedDepth );
	}

	return moments;
}

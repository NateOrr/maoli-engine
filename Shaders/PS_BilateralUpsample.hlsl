//--------------------------------------------------------------------------------------
// File: PS_BilateralUpsample.hlsl
//
// Normal/depth sensitive bilinear upsampling
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>
#include <Deferred.hlsli>

float4 PS_BilateralUpsample( PS_INPUT_TEX input ) : SV_Target
{
	uint2 fineTexel = input.Pos.xy;
	uint2 coarseTexel = fineTexel / 2;

	float d = DL_GetDepth( fineTexel );
	float3 n = DL_GetNormal( fineTexel );

	// Bilateral upsample
	return Bilateral4( g_txPostFX, coarseTexel, d, n );
}
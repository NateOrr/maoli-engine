//--------------------------------------------------------------------------------------
// File: PS_MotionBlur.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>
#include <Deferred.hlsli>

// Pixel shader for computing luminance from the HDRI
void PS_MotionBlur( PS_INPUT_TEX input, out float4 oColor : SV_Target )
{
	// Todo: parameters
	const int NumberOfPostProcessSamples = 8;
	const float PixelBlurConst = 0.25f;
	float depth = DL_GetDepth(input.Pos.xy);
	
	float2 pixelVelocity;
    
    // Get this pixel's current velocity and this pixel's last frame velocity
    // The velocity is stored .r & .g channels
    float4 curFramePixelVelocity = g_txPostFX.Sample(g_samPoint, input.Tex);
    float4 lastFramePixelVelocity = g_txPostFX2.Sample(g_samPoint, input.Tex);

    // If this pixel's current velocity is zero, then use its last frame velocity
    // otherwise use its current velocity.  We don't want to add them because then 
    // you would get double the current velocity the center.  
    // If you just use the current velocity, then it won't blur where the object 
    // was last frame because the current velocity at that point would be 0.  Instead 
    // you could do a filter to find if any neighbors are non-zero, but that requires a lot 
    // of texture lookups which are limited and also may not work if the object moved too 
    // far, but could be done multi-pass.
    float curVelocitySqMag = curFramePixelVelocity.r * curFramePixelVelocity.r +
                             curFramePixelVelocity.g * curFramePixelVelocity.g;
    float lastVelocitySqMag = lastFramePixelVelocity.r * lastFramePixelVelocity.r +
                              lastFramePixelVelocity.g * lastFramePixelVelocity.g;
                                   
    if( lastVelocitySqMag > curVelocitySqMag )
    {
        pixelVelocity.x =  lastFramePixelVelocity.r * PixelBlurConst;   
        pixelVelocity.y = -lastFramePixelVelocity.g * PixelBlurConst;
    }
    else
    {
        pixelVelocity.x =  curFramePixelVelocity.r * PixelBlurConst;   
        pixelVelocity.y = -curFramePixelVelocity.g * PixelBlurConst;    
    }
    
    // For each sample, sum up each sample's color "Blurred" and then divide
    // to average the color after all the samples are added.
    float3 Blurred = 0;  
    for(float i = 0; i < NumberOfPostProcessSamples; i++)
    {   
        // Sample texture a new spot based on pixelVelocity vector 
        // and average it with the other samples        
        float2 lookup = pixelVelocity * i / NumberOfPostProcessSamples + input.Tex;
        
		// Lookup the color at this new spot
		float4 Current = g_txFrameBuffer.SampleLevel(g_samPoint, lookup, 0);
    
		// Add it with the other samples
		Blurred += Current.rgb;
    }
    
    // Return the average color of all the samples
    oColor =  float4(Blurred / NumberOfPostProcessSamples, 1.0f); 
}

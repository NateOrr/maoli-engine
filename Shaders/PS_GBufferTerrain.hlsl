//--------------------------------------------------------------------------------------
// File: PS_GBufferTerrain.hlsl
//
// Using a blend map to combine materials
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Terrain.hlsli>
#include <Deferred.hlsli>

// Pixel shader for GBuffer pass for terrain rendering
DL_Output PS_GBufferTerrain( PS_INPUT_TERRAIN input )
{
	// Skip if the tex coords are outside the bounds
	if( any(input.Tex < 0.0f) || any(input.Tex > 1.0f) )
		discard;
 	
	// Init the output struct
	DL_Output oBuf;
	
	// Setup properties
	float4 diff = 0;
	float3 normal;
    float roughness = 0;
	float4 blend =  g_txBlend.Sample(g_samAnisotropic, input.Tex);
	BlendTerrainMaterials(blend, input.WPos, input.Tex, normal, diff, roughness);

	// Color
	oBuf.Color = diff;

	// Shading normal and polygon normal
	float3 flatNormal = normalize( cross( ddx_fine( input.WPos ), ddy_fine( input.WPos ) ) );
	oBuf.Normal = EncodeNormal( normal );
	oBuf.FlatNormal = EncodeNormal( flatNormal );

	// Material properties
	oBuf.Properties[MAT_ROUGHNESS] = roughness;
	oBuf.Properties[MAT_METALIC] = 0;
	oBuf.ObjectID = 0;

	// Emissive color
	oBuf.Emissive = 0;
	
	return oBuf;
}

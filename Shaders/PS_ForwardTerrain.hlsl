//--------------------------------------------------------------------------------------
// File: PS_ForwardTerrain.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Terrain.hlsli>

// Pixel shader for forward rendering pass for terrain
float4 PS_ForwardTerrain( PS_INPUT_TERRAIN input ) : SV_Target
{
	
 	// Skip if the tex coords are outside the bounds
 	if(input.Tex.x<0.0||input.Tex.y<0.0||input.Tex.x>1.0||input.Tex.y>1.0)
 		discard; 	
   
	// Diffuse blend from 4 detail textures
	float4 oColor = 1.0f;
	
	
	return oColor;
}

//--------------------------------------------------------------------------------------
// File: VS_GBufferSkinned.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shader for GBuffer pass
PS_INPUT_GBUFFER VS_GBufferSkinned( VS_INPUT_SKIN input )
{
    PS_INPUT_GBUFFER output;

	// Perform skinning
	SkinVertexNormal(input, output.Pos, output.Norm);

    output.Pos = mul( output.Pos, g_mWorld );
    output.WPos = output.Pos.xyz;
    output.Pos = mul( output.Pos, g_mViewProjection );
    output.Tex = input.Tex;
    output.Norm = mul(input.Norm, (float3x3)g_mWorld);

    return output;
}

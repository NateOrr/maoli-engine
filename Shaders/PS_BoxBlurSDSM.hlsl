//--------------------------------------------------------------------------------------
// File: PS_BoxBlurSDSM.hlsl
//
// Special box blur for sdsm partitions (modified from SDSM sample)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Partition.hlsli>
#include <PostFX.hlsli>

float2 PS_BoxBlurSDSM(BoxBlurVSOut input) : SV_Target
{
    uint2 sampleOffsetMask = (uint2(0, 1) == g_BlurDirection.xx ? uint2(1, 1) : uint2(0, 0));

    // Center sample
    uint2 coords = int2(input.Position.xy);
    float2 average = input.blurData.interiorSampleWeight * g_txPostFX.Load(uint3(coords, 0)).xy;
    
    // Other interior samples
    for (uint i = 1; i <= input.blurData.interiorSamplesLoopCount; ++i) 
	{
        uint2 offset = i * sampleOffsetMask;
        average += input.blurData.interiorSampleWeight * g_txPostFX.Load(uint3(coords - offset, 0)).xy;
        average += input.blurData.interiorSampleWeight * g_txPostFX.Load(uint3(coords + offset, 0)).xy;
    }
    
    // Edge samples
	uint2 offset = (input.blurData.interiorSamplesLoopCount + 1) * sampleOffsetMask;
	average += input.blurData.edgeSampleWeight * g_txPostFX.Load(uint3(coords - offset, 0)).xy;
	average += input.blurData.edgeSampleWeight * g_txPostFX.Load(uint3(coords + offset, 0)).xy;
    
    return average;
}

//--------------------------------------------------------------------------------------
// File: PS_PaintTerrain.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Terrain.hlsli>
#include <Honua.hlsli>


// Compute shader for terrain painting
[numthreads( 28, 28, 1 )]
void CS_PaintTerrain(uint3 threadID : SV_DispatchThreadID)
{
	const float range = 252.0f;
	float2 threadNorm = (float2)threadID.xy / range.xx;
	
	// Get the terrain position from the tex coord
 	float2 scaledTex = g_PaintRegion.xy + g_PaintRegion.zw*threadNorm;
 	float2 terrainPos = (scaledTex-0.5f.xx)*g_HeightmapSize;
 
 	// Ignore areas outside the widget
 	float dist = distance(terrainPos, g_MouseSceneIntersect.xz);		
 	if(dist>g_TWRadius)
 		return;
 
	// Mask factor and projected UV
	float2 projUV;
	float4 mask = ComputePaintMask(terrainPos, g_MouseSceneIntersect.xz, g_TWRadius, projUV);

	// Get the dimensions of the blend map
	int2 texSize;
	g_PhysicalTextureUAV.GetDimensions(texSize.x, texSize.y);

	// Get the absolute coords the blend map
	int2 blendCoords = int2(texSize * scaledTex);

	// Get the blend values from the uav
	float4 blend = D3DX_R8G8B8A8_UNORM_to_FLOAT4( g_PhysicalTextureUAV[blendCoords] );
 
 	// Updated blend amount
	float delta = 0.2f*g_TWStrength*mask.a;
	float falloff = g_TWRadius - dist;
	if ( falloff < 0.2f )
		delta *= lerp( 0, 1, dist / g_TWRadius );
 	[unroll]
 	for(int i=0; i<4; i++)
 	{
 		if(g_LayerID == i)
 			blend[i] = clamp(lerp(blend[i], 1.0f, delta), 0.0f, 1.0f);
 		else
 			blend[i] = clamp(lerp(blend[i], 0.0f, delta), 0.0f, 1.0f);
 	}
		
	// Write back to the UAV to the proper channels
	g_PhysicalTextureUAV[blendCoords] = D3DX_FLOAT4_to_R8G8B8A8_UNORM( blend );
}

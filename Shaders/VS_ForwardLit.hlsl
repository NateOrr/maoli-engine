//--------------------------------------------------------------------------------------
// File: VS_ForwardLit.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Vertex Shader for lit forward rendering
PS_INPUT_SHADE VS_ForwardLit( VS_INPUT input)
{
	PS_INPUT_SHADE output;
	
	// Output position and texcoord
	output.Pos = mul(input.Pos,g_mWorld);
	output.WPos = output.Pos.xyz;
	output.ViewPos = mul( output.Pos, g_mView ).xyz;
	output.Pos = mul(output.Pos,g_mViewProjection);
	output.Tex = input.Tex;
	output.Norm = mul(input.Norm, (float3x3)g_mWorld);
	return output;
}

//--------------------------------------------------------------------------------------
// File: PS_AdaptLuminance.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <HDR.hlsli>


// Pixel shader for the adapting luminance over time
void PS_AdaptLuminance( PS_INPUT_DOWNSCALE input, out float2 oColor : SV_Target )
{
	// Sample the luminance from surrounding pixels
	float2 sample1 = g_txLuminance.Sample(g_samPoint, input.Tex );
	float2 sample2 = g_txLuminance.Sample(g_samPoint, input.TexSamples[0] );
	float2 sample3 = g_txLuminance.Sample(g_samPoint, input.TexSamples[1] );
	float2 sample4 = g_txLuminance.Sample(g_samPoint, input.TexSamples[2] );
	float2 sample5 = g_txLuminance.Sample(g_samPoint, input.TexSamples[3] );
	
	// Avg
	oColor.r = sample1.r+sample2.r+sample3.r+sample4.r+sample5.r;
	oColor.r *= 0.2f;
	
	// Max
	oColor.g = max(sample1.g, max(sample2.g, max(sample3.g, min(sample5.g, sample4.g) ) ) );
	
	// Reverse the log
	oColor.r = exp(oColor.r);
	
	// Clamp to the correct ranges
	oColor.r = clamp(oColor.r, g_LumMinP*g_MaxLuminance, g_LumMaxP*g_MaxLuminance);
	oColor.g = clamp(oColor.g, g_LumMinP*g_MaxLuminance, g_LumMaxP*g_MaxLuminance);
	
	// Adapt from the previous frame's values based on a time factor
	oColor = lerp( g_txAdaptedLuminance.Load(int3(0, 0, 0)), oColor, 1.0 - pow(0.992, 40.0*0.02) );
}

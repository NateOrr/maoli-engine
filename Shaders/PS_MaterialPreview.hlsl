//--------------------------------------------------------------------------------------
// File: PS_MaterialPreview.hlsl
//
// Rendering material preview images
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Shading.hlsli>
#include <MaterialHelper.hlsli>

// Forward rendering pixel shader
void PS_MaterialPreview( PS_INPUT_SHADE input, out float4 oColor : SV_Target )
{
	// Get material properties
	float3 normal = GetNormal( input.Tex, normalize( input.Norm ), input.WPos );
	float roughness = g_MaterialRoughness;
	float metalic = g_MaterialMetalic;
	float3 baseColor = GetMaterialColor( input.Tex ).rgb;

	// Light positions
	const float3 positions[] = {
		float3(4.5f, 7.0f, -3),
		float3(4.5f, -7.0f, -3),
	};

	// Ambient
	oColor.rgb = baseColor * g_Ambient.rgb;
	oColor.a = g_MaterialColor.a;

	// Shading from 2 lights
	Light light;
	light.color = float4(1, 1, 1, 0);
	light.range = 50.0f;
	[unroll]
	for ( int i = 0; i < 2; ++i )
	{
		light.position = positions[i];
		oColor += float4(ComputePointLighting( light, baseColor, normal, input.WPos, roughness, metalic ), 0);
	}
}

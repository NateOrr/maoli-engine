//--------------------------------------------------------------------------------------
// File: Terrain.hlsli
//
// GPU Geometry Clipmaps
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _TERRAIN_HLSLI_
#define _TERRAIN_HLSLI_

#include <MaterialHelper.hlsli>

// Terrain clipmap info
cbuffer cbTerrain : register(b9)
{
	float  g_ClipmapScale;			// Scale of the currect clipmap level
	float  g_ClipmapSize;			// Size of a clipmap
	float  g_HeightmapSize;			// Size of the heightmap
	float  g_HeightScale;			// Heightmap scale
	float2 g_ClipmapOffset;			// Offset into the main heightmap
	float2 g_ClipmapFix;			// Trim fix offset
	float  g_TerrainVTExtents;		// Scale factor to convert to virtual texture coords
};
Texture2D g_txHeightmap;		// Terrain heightmap
Texture2D g_txBlend;			// Terrain blend map
Texture2D g_txClipmap;			// Clipmap for the current level


struct PS_INPUT_TERRAIN
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
    float2 CTex: TEXCOORD1;
    float3 WPos: TEXCOORD2;
};

// Terrain Materials
// Currently terrains are rendered with a blend map that is stored as a virtual texture.
// This is a single blend texture, with each element storing the blend factor
// for the corresponding terrain layer.  A terrain layer is basically a material.
// The first layer is considered the primary layer, and this layer will pass
// the data into the gbuffer that must be looked up again for shading.
// CSM, emissive material, and surface params are only used from the primary layer.
// All other layers only contribue diffuse, normal and specular mapping.
#define NUM_TERRAIN_LAYERS 4

struct TerrainLayer
{
	float4	color;		// Diffuse color for each layer
	float2	texScale;	// Texture scale for each layer
	float	roughness;	// Specular color for each layer
	uint	texFlag;	// Bitfield indicates which textures are loaded
};

cbuffer cbTerrainMaterial : register(b13)
{
	bool	g_TerrainUsesCSM;							// True if csm is active
	int		g_TerrainCSMSamples;						// Max number of samples to use during cone step mapping
	float	g_TerrainCSMScale;							// Scaling factor for cone step mapping displacement appearence
	int		g_TerrainPad;
	
	TerrainLayer g_TerrainLayers[NUM_TERRAIN_LAYERS];	// Layer data
};

Texture2D g_txTerrainDiffuse[NUM_TERRAIN_LAYERS];			 // Diffuse texture for each layer
Texture2D g_txTerrainNormal[NUM_TERRAIN_LAYERS];			 // Normal map for each layer
Texture2D<float> g_txTerrainRoughness[NUM_TERRAIN_LAYERS];	 // Roughness map for each layer


// --------------------------------------------------------------------------------------
// Check if a terrain texture exists
// --------------------------------------------------------------------------------------
bool CheckTerrainTexture(uint i, uint layer)
{
	return (g_TerrainLayers[layer].texFlag & (1U<<i));
}


// --------------------------------------------------------------------------------------
// Compute the normal from the heightmap data
// --------------------------------------------------------------------------------------
float3 ComputeTerrainNormal(float2 texCoord)
{
	float3 hPos[4];
	float htexel = 1.0 / g_HeightmapSize;
	float hh = g_txHeightmap.SampleLevel( g_samLinear, texCoord,  0).r*g_HeightScale;
	hPos[0] = float3( 1.0f, g_txHeightmap.SampleLevel( g_samLinear, texCoord + float2(htexel, 0),  0).r*g_HeightScale - hh,  0.0f);
	hPos[1] = float3( 0.0f, g_txHeightmap.SampleLevel( g_samLinear, texCoord + float2(0, htexel),  0).r*g_HeightScale - hh,  1.0f);
	hPos[2] = float3(-1.0f, g_txHeightmap.SampleLevel( g_samLinear, texCoord + float2(-htexel, 0), 0).r*g_HeightScale - hh,  0.0f);
	hPos[3] = float3( 0.0f, g_txHeightmap.SampleLevel( g_samLinear, texCoord + float2(0, -htexel), 0).r*g_HeightScale - hh, -1.0f);
	float3 normal = cross(hPos[0], hPos[1]) + cross(hPos[1], hPos[2]) + cross(hPos[2], hPos[3]) + cross(hPos[3], hPos[0]);
	normal = -normalize(normal);  
	return normal;
}

// --------------------------------------------------------------------------------------
// Blend terrain materials
// --------------------------------------------------------------------------------------
void BlendTerrainMaterials(float4 blend, float3 pos, float2 tex, out float3 normal, out float4 diff, out float roughness)
{
	// Setup properties
	diff = 0;
	normal = ComputeTerrainNormal(tex);
    roughness = 0;
	float2 texCoord = tex*g_TerrainLayers[0].texScale;
	float2 dx = ddx(texCoord);
	float2 dy = ddy(texCoord);

	// Get the tangent matrix
    float3x3 mTan = ComputeTangentFrame( normal, pos, texCoord);

	// Apply cone step mapping if it is used by the primary layer
	[branch]
	if(g_TerrainUsesCSM && blend[0]>0)
		normal = CSM_GetNormal(pos, normal, texCoord, dx, dy, mTan, g_txTerrainNormal[0], g_TerrainCSMScale, g_TerrainCSMSamples);
   	
	// Blend each material together
	[unroll]
    for(uint i=0; i<NUM_TERRAIN_LAYERS; i++)
	{
		[branch]
		if(blend[i]>0)
		{
			// Diffuse
			diff += blend[i] * g_txTerrainDiffuse[i].SampleGrad(g_samAnisotropic, texCoord, dx, dy) * g_TerrainLayers[i].color;

			// Normal
            if(CheckTerrainTexture(TEX_NORMAL, i))
		            normal += 0.25f*blend[i] * normalize( mul(g_txTerrainNormal[i].SampleGrad( g_samLinear, texCoord, dx, dy ).xyz*2.0f-1.0f, mTan) );

            // Roughness
            if(CheckTerrainTexture(TEX_SPECULAR, i))
			    roughness += blend[i] * g_txTerrainRoughness[i].SampleGrad(g_samAnisotropic, texCoord, dx, dy) * g_TerrainLayers[i].roughness;
			else
			    roughness += blend[i] * g_TerrainLayers[i].roughness;
        }
	}

	normal = normalize(normal);
}

#endif

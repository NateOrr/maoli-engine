//--------------------------------------------------------------------------------------
// File: VS_AnimatedParticle.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Particle.hlsli>

// Particle frame data
cbuffer cbParticle
{
	uint2  g_SpriteDimensions;
	float2 g_AnimationFrameSize;
	float2 g_TextureDimensions;
	float2 g_SpriteUVScale;		// Ratio of frame size to texture size
};


// Vertex Shader for GBuffer pass
PS_INPUT_PARTICLE VS_AnimatedParticle( VS_INPUT_PARTICLE input )
{
	PS_INPUT_PARTICLE output;
	output.Pos = mul( input.Pos*float4(input.ParticleSize, 1), g_mWorld ) + float4(input.ParticlePos.xyz, 0);
	output.Pos = mul( output.Pos, g_mViewProjection );
	output.Tex = input.Tex;
	output.Color = input.ParticleColor;

	// Scale the tex coords to match the frame size
	output.Tex *= g_SpriteUVScale;

	// Offset to the current frame
	uint cellx = input.SpriteFrame % g_SpriteDimensions.x;
	uint celly = input.SpriteFrame / g_SpriteDimensions.y;
	float2 frameOffset;
	frameOffset.x = (float)(cellx) * g_AnimationFrameSize.x;
	frameOffset.y = (float)(celly) * g_AnimationFrameSize.y;
	frameOffset /= g_TextureDimensions;
	output.Tex += frameOffset;

	return output;
}

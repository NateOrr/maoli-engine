//--------------------------------------------------------------------------------------
// File: VS_BasicTex.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Ortho2D projection with tex coords
PS_INPUT_TEX VS_BasicTex( VS_INPUT input )
{
    PS_INPUT_TEX output;
    output.Pos = mul( input.Pos, g_mWorld );
    output.Pos = mul( output.Pos, g_mViewProjection ); 
    output.Tex = input.Tex; 
    return output;
}

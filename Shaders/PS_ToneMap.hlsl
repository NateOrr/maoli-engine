//--------------------------------------------------------------------------------------
// File: PS_ToneMap.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <HDR.hlsli>


// Pixel shader for tonemapping
void PS_ToneMap( PS_INPUT_TONEMAP input, out float4 oColor : SV_Target)
{
	// Makeshift AA using an edge filter
	oColor = g_txHDR.Sample(g_samPoint, input.Tex);

	// Tonemapping
	//oColor.rgb *= MapLuminance( dot(g_LuminanceVector, oColor.rgb), input.Lum );
	
	// Bloom
	//oColor.rgb += 0.2f * g_txHDRBloom.Sample(g_samLinear, input.Tex).rgb;
}

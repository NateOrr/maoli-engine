//--------------------------------------------------------------------------------------
// File: ShadowMap.fxh
//
// Support for shadow mapping
//
// Nate Orr
//--------------------------------------------------------------------------------------
#ifndef _SHADOWMAP_HLSLI_
#define _SHADOWMAP_HLSLI_

#include <Partition.hlsli>

// Shadow maps
Texture2DArray<float2> g_txCascade;
Texture2DArray<float2> g_txSpotShadowMaps;
TextureCubeArray<float2> g_txCubeShadowMaps;
StructuredBuffer<matrix> g_ShadowMatrices;
//StructuredBuffer<float2> g_ShadowRanges;

// Constants
static const float g_Exponent = 44.0f;
static const float g_DepthBias = 0.0001f;

// Get the exponent for a partition
float GetExponentESM( Partition partition )
{
	return min( g_Exponent / partition.scale.z, 42.0f );
}

// ESM warp
float WarpDepth( float depth )
{
	depth = 2.0f * depth - 1.0f;
	return exp( g_Exponent * depth );
}

// ESM warp
float WarpDepth( float depth, float exponent )
{
	depth = 2.0f * depth - 1.0f;
	return exp( exponent * depth );
}

// Compute the Moments for VSM
float2 ComputeMoments( float Dist )
{
	//float dx = ddx( Dist );
	//float dy = ddy( Dist );
	//return float2(Dist, Dist*Dist + 0.25f*(dx*dx + dy*dy));
	return float2(Dist, Dist*Dist);
}

// Chebyshev inequality for variance shadow mapping
float ChebyshevUpperBound( float2 moments, float mean, float minVariance )
{
	// Compute variance
	float variance = moments.y - (moments.x * moments.x);
	variance = max( variance, minVariance );

	// Compute probabilistic upper bound
	float d = mean - moments.x;
	float pMax = variance / (variance + (d * d));

	// One-tailed Chebyshev
	return (mean <= moments.x ? 1.0f : pMax);
}

float errf( float x )
{
	const float a = 0.140012f;
	const float fourOverPi = 4.0f / 3.141592f;
	const float x2 = x*x;
	return sign( x ) * sqrt( 1 - exp( -x2*((fourOverPi + a*x2) / (1 + a*x2)) ) );
}

float Gaussian( float2 moments, float mean, float minVariance )
{
	const float b = 0.5f;
	const float denom = (1.0f + errf( b / sqrt( 2 ) ));
	float variance = moments.y - (moments.x * moments.x);
	variance = max( variance, minVariance );
	float dev = sqrt( variance );
	float num = 1.0f - errf( (mean - moments.x - b*dev) / (dev * sqrt( 2.0f )) );
	return num * rcp( denom );
}

// Light bleeding reduction for vsm
float LightBleedingReduction( float p )
{
	return smoothstep( 0.25f, 1.0f, p );
}


// Shadow factor for this partition
float ShadowContributionSDSM( float2 texCoord, float depth, uint index, Partition partition )
{
	float2 occluder = g_txCascade.SampleLevel( g_samAnisotropicClamp, float3(texCoord, index), 0);
	float exponent = GetExponentESM( partition );
	float warpedDepth = WarpDepth( depth, exponent );
	float depthScale = 0.0001f * exponent * warpedDepth;
	float minVariance = depthScale * depthScale;
	return LightBleedingReduction( ChebyshevUpperBound( occluder, warpedDepth, 0.0001f ) );
}

// Shadow factor for a spot light
float ShadowContributionSpot( float2 texCoord, float depth, uint index )
{
	float2 occluder = g_txSpotShadowMaps.SampleLevel( g_samAnisotropicClamp, float3(texCoord, index), 0 );
	float warpedDepth = WarpDepth( depth );
	float depthScale = 0.0001f * g_Exponent * warpedDepth;
	float minVariance = depthScale * depthScale;
	return LightBleedingReduction( ChebyshevUpperBound( occluder, warpedDepth, minVariance ) );
}

// Shadow factor for a point light
float ShadowContributionPoint( float3 texCoord, float depth, uint index )
{
	float2 occluder = g_txCubeShadowMaps.SampleLevel( g_samAnisotropicClamp, float4(texCoord, index), 0 );
	float warpedDepth = WarpDepth( depth );
	float depthScale = 0.0001f * g_Exponent * warpedDepth;
	float minVariance = depthScale * depthScale;
	return LightBleedingReduction( ChebyshevUpperBound( occluder, warpedDepth, minVariance ) );
}

// Shadow map pass input
struct PS_INPUT_SM
{
	float4 Pos     : SV_POSITION;
	float3 ViewPos : TEXCOORD;
};

#endif

//--------------------------------------------------------------------------------------
// File: VS_ShadowMapPCF.hlsl
//
// PCF shadow mapping
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <Common Inputs.hlsli>
#include <Common Vars.hlsli>
#include <ShadowMap.hlsli>

// Vertex shader for shadow map rendering
float4 VS_ShadowMapSkinned( VS_INPUT_SKIN_POS input ) : SV_Position
{
	float4 outPos;
	SkinVertex( input, outPos );
	return mul( outPos, g_mShadow );
}

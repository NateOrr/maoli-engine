//--------------------------------------------------------------------------------------
// File: PS_LocalReflections.hlsl
//
// Cube map reflections
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <PostFX.hlsli>
#include <Deferred.hlsli>

struct LightProbe
{
	float3 position;
	float3 min;
	float3 max;
};

static const float3 g_LightProbePosition = float3(-4, -4.5, 0);
static const float3 g_LightProbeMin = float3(-12, -12.5, -8);
static const float3 g_LightProbeMax = float3( 4, 3.5, 8);

float4 PS_LocalReflections( PS_INPUT_TEX input ) : SV_Target
{
	// Make sure we are dealing with a metalic surface
	const float2 screenPos = input.Pos.xy;
	float2 props = DL_GetProperties( screenPos );
	float4 ssr = g_txPostFX.Sample(g_samPoint, input.Tex);
	//if ( props[MAT_METALIC] < 0 || ssr.a == 1.0f )
	//	discard;

	// Get the reflection ray
	float depth = DL_GetDepth( screenPos );
	float3 p = ReconstructPosition( screenPos, depth );
	float3 n = DL_GetNormal( screenPos );
	float3 v = normalize( p - g_CameraPos );
	float3 r = reflect( v, n );

	// Correct for probe location by ray-tracing to the probe AABB
	float3 nrdir = normalize(r);
	float3 rbmax = (g_LightProbeMax - p)/nrdir;
	float3 rbmin = (g_LightProbeMin - p)/nrdir;

	float3 rbminmax = (nrdir>0.0f)?rbmax:rbmin;
	float fa = min(min(rbminmax.x, rbminmax.y), rbminmax.z);

	float3 posonbox = p + nrdir*fa;
	r = normalize(posonbox - g_LightProbePosition);

	float3 localRef = g_txCubeProbe.Sample(g_samAnisotropic, r).rgb;
	return float4(localRef, 0);
	//return float4( lerp(localRef, ssr.rgb, ssr.a), 0 );
}
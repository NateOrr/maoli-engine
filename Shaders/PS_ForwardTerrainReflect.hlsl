//--------------------------------------------------------------------------------------
// File: PS_ForwardTerrainReflect.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Water.hlsli>
#include <Terrain.hlsli>
#include <Shading.hlsli>

// Pixel shader for forward rendering pass for terrain
float4 PS_ForwardTerrainReflect( PS_INPUT_TERRAIN input ) : SV_Target
{
 	if(input.WPos.y <= g_WaterLevel)
 	discard;
 	
 	// Skip if the tex coords are outside the bounds
 	if(input.Tex.x<0.0||input.Tex.y<0.0||input.Tex.x>1.0||input.Tex.y>1.0)
 		discard; 	
   
	// Diffuse blend from 4 detail textures
	float4 oColor = 1.0f;
	
	// Perform sun diffuse lighting if we are doing a reflection
		
	float diff = saturate( dot(ComputeTerrainNormal(input.Tex), -g_SunDirection) );
	oColor *= g_Ambient+diff;
	
	return oColor;
}

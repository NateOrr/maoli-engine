//--------------------------------------------------------------------------------------
// File: PS_ShadeScene.hlsl
//
// Primary shading pass
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>
#include <Shading.hlsli>
#include <Deferred.hlsli>
#include <PostFX.hlsli>

static const float4 g_PartitionColors[] = {
	float4(0, 1, 1, 1),
	float4(1, 0, 1, 1),
	float4(1, 1, 0, 1),
	float4(0, 1, 0, 1),
};

// Spot light deferred shading pass
void PS_ShadeScene( PS_INPUT_VIEW input, out float4 oColor : SV_Target0 )
{
	// Reconstruct the world space position using the depth buffer value
	float depth = DL_GetDepth( input.Pos.xy );
	float3 pos = ReconstructPosition( input.Pos.xy, depth );
	float3 viewPos = mul( float4(pos, 1), g_mView ).xyz;
	float viewDist = length( viewPos );
	depth = RescaleDepth( depth );

	// Sample the gbuffer
	float2 texCoord = input.Pos.xy * g_ScreenSize.zw;
	float3 normal = DL_GetNormal( input.Pos.xy );
	float3 baseColor = DL_GetColor( input.Pos.xy ).rgb;
	float2 prop = DL_GetProperties( input.Pos.xy );
	float4 emissive = DL_GetEmissiveColor( texCoord );

	// Find the cluster for this pixel
	const uint sliceID = NormalizeDepth(viewDist) * g_NumSlices;
	const uint tileX = input.Pos.x / g_TileSize;
	const uint tileY = input.Pos.y / g_TileSize;
	const uint clusterID = tileY*g_TileWidth*g_NumSlices + tileX*g_NumSlices + sliceID;

	// Grab the index data
	LightIndexData indexData = g_ClusterIndexData[clusterID];
	uint i = 0, offset = indexData.start;

	// Ambient
	float ao = max( Bilateral1( g_txPostFX, input.Pos.xy / 2, depth, normal ), g_txPostFX.SampleLevel( g_samPoint, texCoord, 0 ).r);
	oColor.rgb = baseColor * g_Ambient.rgb * ao;

	// Emissive
	oColor.rgb += emissive.rgb;

	// Cascade shadow map directional light
	for ( i = 0; i < SDSM_PARTITIONS; ++i )
	{
		Partition partition = g_PartitionsReadOnly[i];
		if ( depth >= partition.intervalBegin && depth < partition.intervalEnd )
		{
			oColor += float4(ComputeDirectionalLightingWithShadow( g_SunDirection, g_SunColor, baseColor, normal, pos, prop[MAT_ROUGHNESS], prop[MAT_METALIC], viewPos, partition, i ), 0);
			//oColor += g_PartitionColors[i] * 0.15f;
			break;
		}
	}

	// Shadow casting spot lights
	for ( i = 0; i < indexData.numShadowSpotLights; ++i, ++offset )
		oColor += float4(ComputeSpotLightingWithShadow( g_SpotLights[g_ClusterIndices[offset]], baseColor, normal, pos, prop[MAT_ROUGHNESS], prop[MAT_METALIC], i ), 1);

	// Shadow casting point lights
	for ( i = 0; i < indexData.numShadowPointLights; ++i, ++offset )
		oColor += float4(ComputePointLightingWithShadow( g_PointLights[g_ClusterIndices[offset]], baseColor, normal, pos, prop[MAT_ROUGHNESS], prop[MAT_METALIC], i ), 1);

	// Spot lights
	for ( i = 0; i < indexData.numSpotLights; ++i, ++offset )
		oColor += float4(ComputeSpotLighting( g_SpotLights[g_ClusterIndices[offset]], baseColor, normal, pos, prop[MAT_ROUGHNESS], prop[MAT_METALIC] ), 1);

	// Point lights
	for ( i = 0; i < indexData.numPointLights; ++i, ++offset )
		oColor += float4(ComputePointLighting( g_PointLights[g_ClusterIndices[offset]], baseColor, normal, pos, prop[MAT_ROUGHNESS], prop[MAT_METALIC] ), 1);
}

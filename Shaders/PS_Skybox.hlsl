//--------------------------------------------------------------------------------------
// File: PS_Skybox.hlsl
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include <stdafx.hlsli>


// Pixel shader for forward rendering with no lighting
float4 PS_Skybox( PS_INPUT_POSTFX input ) : SV_Target
{
	return g_txCubeProbe.Sample( g_samLinear, normalize( input.ViewDir ) );
}

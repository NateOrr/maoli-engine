//--------------------------------------------------------------------------------------
// File: ExporterIncludes.cpp
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "ExporterIncludes.h"
#include <cmath>

// Equality operator
bool tVertex::operator==( const tVertex& v )
{
	return ( fabs(fX-v.fX)<0.001f && fabs(fY-v.fY)<0.001f && fabs(fZ-v.fZ)<0.001f &&
			 fabs(fNX-v.fNX)<0.001f && fabs(fNY-v.fNY)<0.001f && fabs(fNZ-v.fNZ)<0.001f &&
			 fabs(fU-v.fU)<0.001f && fabs(fV-v.fV)<0.001f );
}

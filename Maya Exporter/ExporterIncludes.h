//--------------------------------------------------------------------------------------
// File: ExporterIncludes.h
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once
#include <vector>
#include <string>

// Vertex
struct tVertex
{
	float fX, fY, fZ;		// Coordinates
	float fNX, fNY, fNZ;	// Normal
	float fU, fV;			// Texture Coordinates

	// Equality operator
	bool operator==(const tVertex& v);
};

// Triangle
struct tTriangle
{
	unsigned int uIndices[3]; // these create a triangle from 3 vertices in the unique vertex list.
};


// Mesh
class CMesh
{
public:
	std::string 				m_strName; 		// This is the name of the mesh.
	std::vector<std::string> 	m_vTextureNames;	// These are the textures that are used in this mesh.
	std::vector<tVertex>  		m_vUniqueVerts;	// These are all the unique vertices in this mesh.
	std::vector<tTriangle> 		m_vTriangles;		// These are the triangles that make up the mesh.    
};

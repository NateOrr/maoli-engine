//--------------------------------------------------------------------------------------
// File: FirstPersonCamera.cpp
//
// User controlled first person camera
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Stranded.h"
#include "FirstPersonCamera.h"

namespace Maoli
{

	// Ctor
	FirstPersonCamera::FirstPersonCamera( Engine* engine ) : Camera( engine )
	{
		_mode = CameraMode::FirstPerson;
		_focusDistance = 5.0f;
		_engine->RegisterEventCallback( EngineEvent::MouseWheelMoved, this, &FirstPersonCamera::OnMouseWheelMoved );
	}

	// Dtor
	FirstPersonCamera::~FirstPersonCamera()
	{
		_engine->UnregisterEventCallback( EngineEvent::MouseWheelMoved, this );
	}

	// Per-frame logic
	void FirstPersonCamera::Update( float dt )
	{
		if ( !_engine->GetPlatform()->KeyDown( Keys::LeftControl ) )
		{
			if ( _mode == CameraMode::FirstPerson )
			{
				// Update the camera speed
				float speed = 50.0f * dt;
				Vector3 deltaSpeed( 0, 0, 0 );
				if ( _engine->GetPlatform()->KeyDown( Keys::Shift ) )
					speed *= 8.0f;
				if ( _engine->GetPlatform()->KeyDown( Keys::W ) )
					AddToVelocity( speed * GetForwardVector() );
				if ( _engine->GetPlatform()->KeyDown( Keys::S ) )
					AddToVelocity( -speed * GetForwardVector() );
				if ( _engine->GetPlatform()->KeyDown( Keys::A ) )
					AddToVelocity( -speed * GetRightVector() );
				if ( _engine->GetPlatform()->KeyDown( Keys::D ) )
					AddToVelocity( speed * GetRightVector() );
				if ( _engine->GetPlatform()->KeyDown( Keys::Q ) )
					AddToVelocity( -speed * GetUpVector() );
				if ( _engine->GetPlatform()->KeyDown( Keys::E ) )
					AddToVelocity( speed * GetUpVector() );

				// Update the view angles (FPS camera)
				float lookSpeed = 0.1f;
				if ( _engine->GetPlatform()->MouseDown( MouseButton::Right ) )
					AddToAngularVelocity( _engine->GetPlatform()->MouseDeltaY() * lookSpeed, _engine->GetPlatform()->MouseDeltaX() * lookSpeed, 0 );
			}
			else
			{
				// Update the view angles
				float lookSpeed = 0.1f;
				if ( _engine->GetPlatform()->MouseDown( MouseButton::Right ) )
					AddToAngularVelocity( _engine->GetPlatform()->MouseDeltaY() * lookSpeed, _engine->GetPlatform()->MouseDeltaX() * lookSpeed, 0 );
			}
		}

		// Update the camera
		Camera::Update( dt );
	}

	// Building the orientation matrix
	void FirstPersonCamera::UpdateOrientation()
	{
		if ( _mode == CameraMode::FirstPerson )
			Camera::UpdateOrientation();
		else
		{
			Vector3 camPos = Vector3( 0, 0, -_focusDistance );
			Matrix r = Matrix::CreateRotationYawPitchRoll( _theta.y, _theta.x, _theta.z );
			camPos = r.Transform( camPos );
			_matrix.LookAt( camPos, Vector3( 0, 0, 0 ) );
			_isDirty = false;
		}
	}

	// Mouse wheel event
	void FirstPersonCamera::OnMouseWheelMoved( void* userData )
	{
		int32 delta = *(int32*)userData;
		_focusDistance -= float( delta ) * 0.5f;
		_isDirty = true;
	}

	// Set the camera mode
	void FirstPersonCamera::SetMode( CameraMode mode )
	{
		_mode = mode;
		_isDirty = true;
	}

	// Set the focus distance
	void FirstPersonCamera::SetFocusDistance( float value )
	{
		_focusDistance = value;
		_isDirty = true;
	}


}

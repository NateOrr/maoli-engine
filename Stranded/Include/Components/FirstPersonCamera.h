//--------------------------------------------------------------------------------------
// File: FirstPersonCamera.h
//
// User controlled first person camera
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Camera modes
	enum class CameraMode
	{
		FirstPerson,
		ArcBall,
	};

	class FirstPersonCamera : public Camera
	{
	public:

		COMPONENT_DECL( FirstPersonCamera );

		// Ctor
		FirstPersonCamera( Engine* engine );

		// Dtor
		virtual ~FirstPersonCamera();

		// Per-frame logic
		virtual void Update( float dt );

		// Get the current mode
		inline CameraMode GetMode() const { return _mode; }

		// Set the camera mode
		void SetMode( CameraMode mode );

		// Get the focus distance
		inline float GetFocusDistance() const { return _focusDistance; }

		// Set the focus distance
		void SetFocusDistance( float value );
		

	private:

		// Mouse wheel event
		void OnMouseWheelMoved(void* userData);

		// Building the orientation matrix
		virtual void UpdateOrientation();

		CameraMode	_mode;				// Camera mode
		float		_focusDistance;		// Distance to the focus target for the arcball mode
	};
}

//--------------------------------------------------------------------------------------
// File: Game.cpp
//
// Game System
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Stranded.h"
#include "Game.h"

namespace Maoli
{
	// Ctor
	Game::Game() : System( nullptr ), _graphics( nullptr )
	{
		//_heapId = Maoli::MemoryManager::GetInstance()->CreateHeap(200*1024*1024);
		_isRunning = false;
	}

	// Dtor
	Game::~Game()
	{
		// The game needs to be unregistered before the engine is deleted
		_engine->UnregisterSystem( this );
		SafeDelete(_engine);

		_console.Close();
		//Maoli::MemoryManager::GetInstance()->DestroyHeap(_heapId);
	}

	// Create the game world
	bool Game::Create( int32 width, int32 height, bool windowed )
	{
		// Start the console
		_console.Open();

		// Create the engine
		_engine = new Engine;
		if ( !_engine->Init( width, height, windowed ) )
			return false;

		// Setup the systems
		WindowHandle renderWindow = _engine->GetPlatform()->GetRenderWindowHandle();
		_engine->RegisterSystem( SystemType::Animation );
		_engine->RegisterSystem( SystemType::Audio );
		_engine->RegisterSystem( SystemType::Graphics );
		_engine->RegisterSystem( SystemType::Physics );
		_engine->RegisterSystem( SystemType::Scripting );

		// Configure the graphics
		_graphics = _engine->GetGraphics();
		_graphics->EnableVSync( true );

		// Register the game with the engine
		_isRunning = true;
		if ( !_engine->RegisterSystem( SystemType::User, this ) )
			return false;

		return true;
	}

	// Create the game world
	bool Game::Create( WindowHandle mainWindow, WindowHandle renderWindow )
	{
		// Start the console
		_console.Open();

		// Create the engine
		_engine = new Engine;
		if ( !_engine->Init( mainWindow, renderWindow ) )
			return false;

		// Setup the systems
		_engine->RegisterSystem( SystemType::Animation );
		_engine->RegisterSystem( SystemType::Audio );
		_engine->RegisterSystem( SystemType::Graphics );
		_engine->RegisterSystem( SystemType::Physics );
		_engine->RegisterSystem( SystemType::Scripting );

		// Configure the graphics
		_graphics = _engine->GetGraphics();
		_graphics->EnableVSync( true );

		// Register the game with the engine
		_isRunning = true;
		if ( !_engine->RegisterSystem( SystemType::User, this ) )
			return false;

		return true;
	}

	// Create the game world
	bool Game::Create( WindowHandle mainWindow )
	{
		return Create( mainWindow, mainWindow );
	}
		

	// Setup
	bool Game::Init()
	{
		return true;
	}

	// Register a component to the system
	bool Game::RegisterComponent( Component* component )
	{
		// Ex:
		//		if(component->GetTypeID() == typeid(SomeComponent))
		//		{
		//			_components.Add(component);
		//			return true;
		//		}

		return false;
	}

	// Remove a component
	void Game::UnregisterComponent( Component* component )
	{
		// Ex:
		//		if(component->GetTypeID() == typeid(SomeComponent))
		//		{
		//			_components.Remove(component);
		//			return true;
		//		}

	}

	// Cleanup
	void Game::Release()
	{

	}

	// Run the game
	void Game::Run()
	{
		while ( _isRunning )
		{
			Step();
			Render();
		}
	}

	// Frame update. This gets called by Step() after updating components.
	void Game::Update( float dt )
	{

	}

	// Perform one frame of game logic
	void Game::Step()
	{
		_engine->Update();
	}

	// Render the game world
	void Game::Render()
	{
		_engine->Render();
	}

	// Register component types with the engine
	void Game::RegisterComponentTypes()
	{
		Component::RegisterType<FirstPersonCamera>();
	}

}

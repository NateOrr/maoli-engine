//--------------------------------------------------------------------------------------
// File: Game.h
//
// Game system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// The game is treated like any other system, and internally manages 
	// the components that it needs
	class Game : public System
	{
	public:

		// Ctor
		Game();

		// Dtor
		virtual ~Game();

		// Create the game world
		bool Create( int32 width, int32 height, bool windowed );
		bool Create( WindowHandle mainWindow );
		bool Create( WindowHandle mainWindow, WindowHandle renderWindow );

		// Perform one frame of game logic
		void Step();

		// Render the game world
		void Render();

		// Run the game
		void Run();

	protected:

		// Setup
		virtual bool Init();

		// Register a component to the system
		virtual bool RegisterComponent( Component* component );

		// Remove a component
		virtual void UnregisterComponent( Component* component );

		// Register component types with the engine
		virtual void RegisterComponentTypes();

		// Frame update
		virtual void Update( float dt );

		// Cleanup
		virtual void Release();

		// Exit the app
		inline void Shutdown() { _isRunning = false; }

		Renderer*	_graphics;		// Renderer
		Console		_console;		// Debug console
		uint32		_heapId;		// Memory heap
		bool		_isRunning;		// True while the game runs

	};
}

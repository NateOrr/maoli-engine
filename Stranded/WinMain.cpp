//---------------------------------------------------------------------------------------
// File: WinMain.cpp
//
// App entry point
//
// Nate Orr
//---------------------------------------------------------------------------------------
#include "Stranded.h"

// Lead detection
// #if defined(DEBUG) || defined(_DEBUG)
// #include <vld.h>
// #endif

//  #include <Windows.h>
//  #include <stdio.h>
//  #include <Dbghelp.h>
// 
// // Required for dump file
// #pragma comment(lib, "dbghelp.lib")
//  
//  LONG WINAPI errorFunc( _EXCEPTION_POINTERS *pExceptionInfo )
//  {
//  	HANDLE hFile = ::CreateFile( L"dumpfile.mdmp", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
//  
//  	if ( hFile != INVALID_HANDLE_VALUE )
//  	{
//  		_MINIDUMP_EXCEPTION_INFORMATION ExInfo;
//  
//  		ExInfo.ThreadId = ::GetCurrentThreadId();
//  		ExInfo.ExceptionPointers = pExceptionInfo;
//  		ExInfo.ClientPointers = NULL;
//  		MiniDumpWriteDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL );
//  		//MessageBox("Dump File Saved look x directory please email to developer at the following email adress crashdmp@gmail.com with the subject Gamename - Version ");
//  		::CloseHandle( hFile );
//  	}
//  
//  	return 0;
//  }

// Entry point to the program
int32 CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int32 nCmdShow)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) || defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

	// Set up the function to create the dump file
	//SetUnhandledExceptionFilter( errorFunc );

	// Create has an overload that takes in a HWND if you don't want to use
	// the built in window creation
	//Stranded game;
	Maoli::Game game;
	//if(game.Create(1024, 768, true))
	//	game.Run();

#ifdef _DEBUG
	if(game.Create(1440, 810, true))
		game.Run();
#else
	if(game.Create(1440, 810, false))
		game.Run();
#endif

	return 0;
}

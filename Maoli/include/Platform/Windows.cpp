//--------------------------------------------------------------------------------------
// File: Windows.cpp
//
// Windows platform implementation
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Platform/Windows.h"
#include <Direct.h>
#include "Win32.h"

namespace Maoli
{
	// Window proc
	static LRESULT CALLBACK WindowProc( HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam )
	{
		WindowsPlatform* platform = (WindowsPlatform*)GetWindowLongPtr( hWnd, GWLP_USERDATA );
		MSG msg = { hWnd, Msg, wParam, lParam };
		if ( !platform || !platform->ProcessMessage( msg ) )
			return DefWindowProc( hWnd, Msg, wParam, lParam );
		return 0;
	}

	// Ctor
	WindowsPlatform::WindowsPlatform( Engine* parent ) : Platform( parent )
	{
		FindWorkingDirectory();
		FindProcessorCount();
	}

	// Create a window
	bool WindowsPlatform::InitWindow( int32 width, int32 height )
	{
		// Get the HINSTANCE
		HINSTANCE hInstance = GetModuleHandle( nullptr );

		// Register the Window Class.
		WNDCLASSEX wcex = {};
		wcex.cbSize = sizeof( wcex );
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.hIcon = LoadIcon( nullptr, IDI_APPLICATION );
		wcex.hIconSm = LoadIcon( nullptr, IDI_APPLICATION );
		wcex.hCursor = LoadCursor( nullptr, IDC_ARROW );
		wcex.hInstance = hInstance;
		wcex.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wcex.lpfnWndProc = WindowProc;
		wcex.lpszMenuName = nullptr;
		wcex.lpszClassName = L"Direct3D Window";
		RegisterClassEx( &wcex );

		// Create the window
		_window = CreateWindow( wcex.lpszClassName,
			L"Maoli Game Engine",
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT,
			width, height,
			HWND_DESKTOP,
			nullptr,
			hInstance,
			nullptr );
		if ( !_window )
		{
			MessageBox( "ERROR: Failed to create the Main Window!", "Error", MB_OK | MB_ICONERROR );
			return false;
		}

		// Save the dimensions
		_width = width;
		_height = height;

		// Register this class instance with the window
		SetWindowLongPtr( _window, GWLP_USERDATA, (LONG_PTR)this );

		// Show the window
		ShowWindow( _window, true );
		UpdateWindow( _window );

		_renderWindow = _window;

		return true;
	}


	// Load a DLL
	Handle WindowsPlatform::LoadDLL( const char* file )
	{
		return LoadLibraryA( file );
	}


	// Display a generic message box
	int32 WindowsPlatform::MessageBox( const char* msg, const char* title, int32 flags /*= 0*/ )
	{
		return MessageBoxA( HWND_DESKTOP, msg, title, flags );
	}


	// Get the working directory
	void WindowsPlatform::FindWorkingDirectory()
	{
		char dir[MAX_PATH];
		_workingDirectory = _getcwd( dir, MAX_PATH );
		_workingDirectory += "\\";
	}


	// Get the processor count
	void WindowsPlatform::FindProcessorCount()
	{
		_processorCount = Win32::GetPhysicalProcessorCount();
	}
	static int32 frame = 0;


	// Per frame update
	bool WindowsPlatform::Update( bool processMessages )
	{
		// Get the mouse coords
		GetCursorPos( reinterpret_cast<POINT*>(&_mouseState.position) );
		ScreenToClient( _renderWindow, reinterpret_cast<POINT*>(&_mouseState.position) );

		// Reset wheel delta
		_mouseState.wheelDelta = 0;

		if ( IsActiveWindow() )
		{
			// Lock the mouse for game cameras
			if ( _lockMouse )
			{
				Point lockedPos( _width / 2, _height / 2 );
				_mouseState.prevPosition = lockedPos;
				ClientToScreen( _renderWindow, reinterpret_cast<POINT*>(&lockedPos) );
				SetCursorPos( lockedPos.x, lockedPos.y );
			}

			// Compute deltas
			_mouseState.delta.x = _mouseState.position.x - _mouseState.prevPosition.x;
			_mouseState.delta.y = _mouseState.position.y - _mouseState.prevPosition.y;

			// Properly set the previous location
			if ( !_lockMouse )
				_mouseState.prevPosition = _mouseState.position;

		}


		// Handle OS messages
		if ( processMessages )
			return ProcessMessages();

		return true;
	}


	// Process available OS messages
	bool WindowsPlatform::ProcessMessages()
	{
		// Process waiting messages
		MSG msg = { 0 };
		while ( PeekMessage( &msg, nullptr, 0, 0, PM_REMOVE ) )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}

		return true;
	}

	// Cleanup
	void WindowsPlatform::Release()
	{

	}

	// Create a file directory
	bool WindowsPlatform::CreateDirectory( const char* dir )
	{
		return CreateDirectoryA( dir, nullptr ) == TRUE;
	}

	// Copy a file
	bool WindowsPlatform::CopyFile( const char* source, const char* dest )
	{
		return CopyFileA( source, dest, false ) == TRUE;
	}

	// Process a single OS message, returns false if the app should exit
	bool WindowsPlatform::ProcessMessage( const OSMessage& msg )
	{
		switch ( msg.message )
		{
			// Key down
			case WM_KEYDOWN:
				_keyState[msg.wParam] |= ((msg.lParam & (1 << 30)) == 0) ? 0x80 | 0x01 : 0x80;
				break;

				// Key up
			case WM_KEYUP:
				_keyState[msg.wParam] = 0;
				break;

				// Left button down
			case WM_LBUTTONDOWN:
				_mouseState.buttons[(int32)MouseButton::Left] |= 0x80 | 0x01;
				break;

				// Middle button down
			case WM_MBUTTONDOWN:
				_mouseState.buttons[(int32)MouseButton::Middle] |= 0x80 | 0x01;
				break;

				// Right button down
			case WM_RBUTTONDOWN:
				_mouseState.buttons[(int32)MouseButton::Right] |= 0x80 | 0x01;
				break;

				// Left button Up
			case WM_LBUTTONUP:
				_mouseState.buttons[(int32)MouseButton::Left] = 0;
				break;

				// Middle button Up
			case WM_MBUTTONUP:
				_mouseState.buttons[(int32)MouseButton::Middle] = 0;
				break;

				// Right button Up
			case WM_RBUTTONUP:
				_mouseState.buttons[(int32)MouseButton::Right] = 0;
				break;

				// Mouse wheel
			case WM_MOUSEWHEEL:
				_mouseState.wheelDelta = GET_WHEEL_DELTA_WPARAM( msg.wParam ) / WHEEL_DELTA;
				_parent->BroadcastEvent( EngineEvent::MouseWheelMoved, &_mouseState.wheelDelta );
				break;

				// Resize the window
			case WM_SIZE:
				if ( _parent->GetGraphics() )
				{
					_parent->GetGraphics()->OnResizeWindow();
					_width = _parent->GetGraphics()->GetWidth();
					_height = _parent->GetGraphics()->GetHeight();
				}
				break;

				// Exit the app
			case WM_DESTROY:
			case WM_CLOSE:
				PostQuitMessage( 0 );
				break;

			default:
				return false;
		}

		return true;
	}

	// Get the names of all files in a directory
	void WindowsPlatform::GetFilesInDirectory( const char* dir, Array<String>& files )
	{
		WIN32_FIND_DATAA data;
		HANDLE fh = FindFirstFileA( dir, &data );
		if ( fh != INVALID_HANDLE_VALUE )
		{
			do
			{
				files.Add( data.cFileName );
			} while ( FindNextFileA( fh, &data ) != 0 );
		}
	}

	// Set the mouse position
	void WindowsPlatform::SetMouseLocation( int32 x, int32 y )
	{
		SetCursorPos( x, y );
	}

	// Hide the mouse cursor
	void WindowsPlatform::HideMouseCursor()
	{
		ShowCursor( false );
	}

	// Show the mouse cursor
	void WindowsPlatform::ShowMouseCursor()
	{
		ShowCursor( true );
	}

	// Check if this is the active window
	bool WindowsPlatform::IsActiveWindow() const
	{
		return (GetActiveWindow() == GetWindowHandle());
	}

	// Set the window handle
	void WindowsPlatform::SetWindow( WindowHandle mainWindow )
	{
		_window = _renderWindow = mainWindow;
		RECT rect;
		GetWindowRect( _renderWindow, &rect );
		_width = rect.right - rect.left;
		_height = rect.bottom - rect.top;
	}

	// Set the window handle
	void WindowsPlatform::SetWindow( WindowHandle mainWindow, WindowHandle renderWindow )
	{
		_window = mainWindow;
		_renderWindow = renderWindow;
		RECT rect;
		GetWindowRect( _renderWindow, &rect );
		_width = rect.right - rect.left;
		_height = rect.bottom - rect.top;
	}

}

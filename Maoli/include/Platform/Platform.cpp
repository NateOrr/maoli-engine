//--------------------------------------------------------------------------------------
// File: Platform.cpp
//
// Platform manager base class, operating system specific code
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"

#include "Platform/Platform.h"

namespace Maoli
{

	// Ctor
	Platform::Platform( Engine* parent )
	{
		_parent = parent;

		// Setup the input state memory
		ZeroMemory(&_mouseState, sizeof(MouseState));
		ZeroMemory(&_keyState, 256);
		_lockMouse = false;
	}

	// Dtor
	Platform::~Platform()
	{

	}

}

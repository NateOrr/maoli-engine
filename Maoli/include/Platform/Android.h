//--------------------------------------------------------------------------------------
// File: Android.h
//
// Android platform implementation
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Platform\Platform.h"

namespace Maoli
{
	class AndroidPlatform : public Platform
	{
	public:
		// Ctor
		AndroidPlatform( Engine* parent );

		// Create a window
		virtual bool InitWindow( int32 width, int32 height );

		// Load a DLL
		virtual Handle LoadDLL( const char* file );

		// Display a generic message box
		virtual int32 MessageBox( const char* msg, const char* title = "Maoli Engine", int32 flags = 0 );

		// Create a file directory
		virtual bool CreateDirectory( const char* dir );

		// Copy a file
		virtual bool CopyFile( const char* source, const char* dest );

		// Get the names of all files in a directory
		virtual void GetFilesInDirectory( const char* dir, Array<String>& files );

		// Per frame update
		virtual bool Update( bool processMessages );

		// Process a single OS message, returns true if the message was handled
		virtual bool ProcessMessage( const OSMessage& msg );

		// Set the mouse position
		virtual void SetMouseLocation( int32 x, int32 y );

		// Hide the mouse cursor
		virtual void HideMouseCursor();

		// Show the mouse cursor
		virtual void ShowMouseCursor();

		// Check if this is the active window
		virtual bool IsActiveWindow() const;


		// Set the window handle
		virtual void SetWindow( WindowHandle mainWindow );
		virtual void SetWindow( WindowHandle mainWindow, WindowHandle renderWindow );

		// Cleanup
		virtual void Release();


	private:

		// Process all current OS messages, returns false if the app should exit
		virtual bool ProcessMessages();

		// Get the working directory
		virtual void FindWorkingDirectory();

		// Get the processor count
		virtual void FindProcessorCount();

	};
}

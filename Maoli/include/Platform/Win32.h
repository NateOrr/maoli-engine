//--------------------------------------------------------------------------------------
// File: Util.h
//
// Win32 Helper functions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	namespace Win32
	{
		// Helper functions for querying information about the processors in the current
		// system.  ( Copied from the doc page for GetLogicalProcessorInformation() )
		uint32 CountBits(ULONG_PTR bitMask);
		int32 GetPhysicalProcessorCount();


		// Convert unicode string to an ascii string
		BOOL WINAPI UnicodeToAnsi(LPWSTR pszwUniString, LPSTR  pszAnsiBuff, uint32  dwAnsiBuffSize);

		// Convert ascii string to an unicode string
		BOOL WINAPI AnsiToUnicode(LPCSTR  pszAnsiString,	LPWSTR pszwUniBuff,	uint32 dwUniBuffSize);
	}

}

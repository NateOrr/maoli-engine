//--------------------------------------------------------------------------------------
// File: Win32.cpp
//
// Win32 Helper functions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include <math.h>

namespace Maoli
{
	namespace Win32
	{
		// Helper functions for querying information about the processors in the current
		// system.  ( Copied from the doc page for GetLogicalProcessorInformation() )

		//  Helper function to count bits in the processor mask
		uint32 CountBits(ULONG_PTR bitMask)
		{
			uint32 LSHIFT = sizeof(ULONG_PTR)*8 - 1;
			uint32 bitSetCount = 0;
			uint32 bitTest = 1 << LSHIFT;
			uint32 i;

			for( i = 0; i <= LSHIFT; ++i)
			{
				bitSetCount += ((bitMask & bitTest)?1:0);
				bitTest/=2;
			}

			return bitSetCount;
		}

		typedef BOOL (WINAPI *LPFN_GLPI)(
			PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, 
			PDWORD);

		int32 GetPhysicalProcessorCount()
		{
			uint32 procCoreCount = 0;    // Return 0 on any failure.  That'll show them.

			LPFN_GLPI Glpi;

			Glpi = (LPFN_GLPI) GetProcAddress(
				GetModuleHandle(TEXT("kernel32")),
				"GetLogicalProcessorInformation");
			if (nullptr == Glpi) 
			{
				// GetLogicalProcessorInformation is not supported
				return procCoreCount;
			}

			BOOL done = FALSE;
			PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = nullptr;
			DWORD returnLength = 0;

			while (!done) 
			{
				uint32 rc = Glpi(buffer, &returnLength);

				if (FALSE == rc) 
				{
					if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) 
					{
						if (buffer) 
							free(buffer);

						buffer=(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(
							returnLength);

						if (nullptr == buffer) 
						{
							// Allocation failure\n
							return procCoreCount;
						}
					} 
					else 
					{
						// Unanticipated error
						return procCoreCount;
					}
				} 
				else done = TRUE;
			}

			uint32 byteOffset = 0;
			PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = buffer;
			while (byteOffset < returnLength) 
			{
				if (ptr->Relationship == RelationProcessorCore) 
				{
					if(ptr->ProcessorCore.Flags)
					{
						//  Hyperthreading or SMT is enabled.
						//  Logical processors are on the same core.
						procCoreCount += 1;
					}
					else
					{
						//  Logical processors are on different cores.
						procCoreCount += CountBits(ptr->ProcessorMask);
					}
				}
				byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
				ptr++;
			}

			free (buffer);

			return procCoreCount;
		}


		// Convert unicode string to an ascii string
		BOOL WINAPI UnicodeToAnsi(LPWSTR pszwUniString, LPSTR  pszAnsiBuff, uint32  dwAnsiBuffSize)
		{
			int32 iRet = 0;
			iRet = WideCharToMultiByte(
				CP_ACP,
				0,
				pszwUniString,
				-1,
				pszAnsiBuff,
				dwAnsiBuffSize,
				nullptr,
				nullptr
				);
			return ( 0 != iRet );
		}


		// Convert ascii string to an unicode string
		BOOL WINAPI AnsiToUnicode(LPCSTR  pszAnsiString,	LPWSTR pszwUniBuff,	uint32 dwUniBuffSize)
		{
			int32 iRet = 0;
			iRet = MultiByteToWideChar(
				CP_ACP,
				0,
				pszAnsiString,
				-1,
				pszwUniBuff,
				dwUniBuffSize
				);

			return ( 0 != iRet );
		}
	}
}

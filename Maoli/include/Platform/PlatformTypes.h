//--------------------------------------------------------------------------------------
// File: PlatformTypes.h
//
// Platform specific types
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	// Mouse buttons
	enum class MouseButton
	{
		Left,
		Right,
		Middle,
	};

	// Windows Types
#ifdef MAOLI_WINDOWS

	// Exclude useless windows stuff
#define NOMINMAX
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN

	// Win32 headers
#include <windows.h>
#include <process.h>
#include <mmsystem.h>

	// Undefine annoying windows macro names
#undef SendMessage
#undef DrawText
#undef MessageBox
#undef CopyFile
#undef CreateDirectory
#undef LoadLibrary

	// Platform types
	typedef HWND		WindowHandle;
	typedef HANDLE		Handle;
	typedef MSG			OSMessage;
	typedef HINSTANCE	AppInstance;

	// Keys
	enum class Keys
	{
		// Letters
		A = 'A', B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

		// Numbers
		Key0 = '0', Key1, Key2, Key3, Key4, Key5, Key6, Key7, Key8, Key9,

		Escape = VK_ESCAPE,
		Minus = 189,					// - on main keyboard 
		Equals = 187,
		Backspace = VK_BACK,			// backspace 
		Tab = VK_TAB,
		LeftBracket = 219,
		RightBracket = 221,
		Enter = VK_RETURN,				// Enter on main keyboard 
		LeftControl = 17,
		Semicolon = 186,
		Apostrophe = 220,
		BackSlash = 220,
		Comma = 188,
		Period = 190,					// . on main keyboard 
		Slash = 191,					// / on main keyboard 
		Shift = VK_SHIFT,
		LeftAlt = VK_LMENU,				// left Alt 
		Space = VK_SPACE,
		CapsLock = VK_CAPITAL,
		F1 = VK_F1,
		F2 = VK_F2,
		F3 = VK_F3,
		F4 = VK_F4,
		F5 = VK_F5,
		F6 = VK_F6,
		F7 = VK_F7,
		F8 = VK_F8,
		F9 = VK_F9,
		F10 = VK_F10,
		NumLock = VK_NUMLOCK,
		Scroll = VK_SCROLL,			    // Scroll Lock 
		NumPad7 = VK_NUMPAD7,
		NumPad8 = VK_NUMPAD8,
		NumPad9 = VK_NUMPAD9,
		NumPad4 = VK_NUMPAD4,
		NumPad5 = VK_NUMPAD5,
		NumPad6 = VK_NUMPAD6,
		NumPad1 = VK_NUMPAD1,
		NumPad2 = VK_NUMPAD2,
		NumPad3 = VK_NUMPAD3,
		NumPad0 = VK_NUMPAD0,
		Subtract = VK_SUBTRACT,         // - on numeric keypad 
		Add = VK_ADD,					// + on numeric keypad 
		Decimal = VK_DECIMAL,			// . on numeric keypad 
		Multiply = VK_MULTIPLY,			// * on numeric keypad 
		F11 = VK_F11,
		F12 = VK_F12,
		RightControl = VK_RCONTROL,
		Divide = VK_DIVIDE,				// / on numeric keypad 
		RightAlt = VK_RMENU,			// right Alt 
		Pause = VK_PAUSE,				// Pause 
		Home = VK_HOME,					// Home on arrow keypad 
		Up = VK_UP,						// UpArrow on arrow keypad 
		Left = VK_LEFT,					// LeftArrow on arrow keypad 
		Right = VK_RIGHT,				// RightArrow on arrow keypad 
		End = VK_END,					// End on arrow keypad 
		Down = VK_DOWN,					// DownArrow on arrow keypad 
		Insert = VK_INSERT,				// Insert on arrow keypad 
		Delete = VK_DELETE,				// Delete on arrow keypad 
		LeftWindows = VK_LWIN,			// Left Windows key 
		RightWindows = VK_RWIN,			// Right Windows key 
		PageUp = VK_PRIOR,				// PgUp on arrow keypad 
		PageDown = VK_NEXT,				// PgDn on arrow keypad 
	};

	// Android
#elif MAOLI_ANDROID

	// Platform types
	typedef int32	WindowHandle;
	typedef int32	Handle;
	typedef int32	OSMessage;
	typedef int32	AppInstance;

	// Keycodes
	enum class Keys
	{

	};

#endif

}
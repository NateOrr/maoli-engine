//--------------------------------------------------------------------------------------
// File: Platform.h
//
// Platform manager base class, operating system specific code
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "PlatformTypes.h"

namespace Maoli
{
	// Forward decl
	class Engine;

	// OS Wrapper
	class Platform
	{
	public:

		// Ctor
		Platform( Engine* parent );

		// Dtor
		virtual ~Platform();

		// Create a window
		virtual bool InitWindow( int32 width, int32 height ) = 0;

		// Load a DLL
		virtual Handle LoadDLL( const char* file ) = 0;

		// Display a message box
		virtual int32 MessageBox( const char* msg, const char* title = "Maoli Engine", int32 flags = 0 ) = 0;

		// Create a file directory
		virtual bool CreateDirectory( const char* dir ) = 0;

		// Copy a file
		virtual bool CopyFile( const char* source, const char* dest ) = 0;

		// Get the names of all files in a directory
		virtual void GetFilesInDirectory( const char* dir, Array<String>& files ) = 0;

		// Per frame update
		virtual bool Update( bool processMessages ) = 0;

		// Process a single OS message, returns true if the message was handled
		virtual bool ProcessMessage( const OSMessage& msg ) = 0;

		// Set the mouse position
		virtual void SetMouseLocation( int32 x, int32 y ) = 0;

		// Hide the mouse cursor
		virtual void HideMouseCursor() = 0;

		// Show the mouse cursor
		virtual void ShowMouseCursor() = 0;

		// Check if this is the active window
		virtual bool IsActiveWindow() const = 0;

		// Cleanup
		virtual void Release() = 0;


		// Set the window handle
		virtual void SetWindow( WindowHandle mainWindow ) = 0;
		virtual void SetWindow( WindowHandle mainWindow, WindowHandle renderWindow ) = 0;

		// Get the handle to the window
		inline WindowHandle GetWindowHandle() const { return _window; }

		// Get the handle to the rendering window
		inline WindowHandle GetRenderWindowHandle() const { return _renderWindow; }

		// Get the cpu core count
		inline int32 GetProcessorCount() const { return _processorCount; }

		// Get the working directory
		inline const String& GetWorkingDirectory() const { return _workingDirectory; }


		///////////////////////////////////////////////////////
		// Input

		// Check if a key is up
		inline bool KeyUp( Keys key ) const { return _keyState[(uint32)key] == 0; }

		// Check if a key is down
		inline bool KeyDown( Keys key ) const { return (_keyState[(uint32)key] & 0x80) != 0; }

		// Check if a key has been pressed
		inline bool KeyPressed( Keys key ) { return (_keyState[(uint32)key] & 0x01) ? (_keyState[(uint32)key] &= ~0x01) != 0 : false; }

		// Mouse X movement
		inline int32 MouseDeltaX() const { return _mouseState.delta.x; }

		// Mouse Y movement
		inline int32 MouseDeltaY() const { return _mouseState.delta.y; }

		// Mouse wheel movement
		inline int32 MouseWheelDelta() const { return _mouseState.wheelDelta; }

		// Check for mouse down
		inline bool MouseDown( MouseButton button ) const { return (_mouseState.buttons[(int32)button] & 0x80) != 0; }

		// Check for mouse clicks
		inline bool MouseClicked( MouseButton button ) { return (_mouseState.buttons[(int32)button] & 0x01) ? (_mouseState.buttons[(int32)button] &= ~0x01) != 0 : false; }

		// Mouse position
		inline const Point& GetMouseLocation() const { return _mouseState.position; }

		// Lock the mouse cursor
		inline void LockMouse() { _lockMouse = true; }

		// Unlock the mouse cursor
		inline void UnlockMouse() { _lockMouse = false; }

		// Get the window width
		inline int32 GetWindowWidth() const { return _width; }

		// Get the window width
		inline int32 GetWindowHeight() const { return _height; }


	protected:

		// Process all current OS messages, returns false if the app should exit
		virtual bool ProcessMessages() = 0;

		///////////////////////////////////////////////////////////
		// These MUST be called in the constructor of 
		// derived classes

		// Get the working directory
		virtual void FindWorkingDirectory() = 0;

		// Get the processor count
		virtual void FindProcessorCount() = 0;

		///////////////////////////////////////////////////////////


		WindowHandle		_window;			// Handle to the main app window
		WindowHandle		_renderWindow;		// Handle to window that will receive rendering
		int32					_processorCount;	// CPU core count
		String				_workingDirectory;	// App startup directory
		Engine*				_parent;			// Creating app
		int32					_width;				// Window width
		int32					_height;			// Window height

		// Mouse state
		struct MouseState
		{
			byte	buttons[3];			// Button states
			Point	position;			// Mouse position for this frame
			Point	prevPosition;		// Mouse position from the previous frame
			Point	delta;				// Change in mouse position
			int32		wheelDelta;			// Change in mouse wheel position
		};

		byte				    _keyState[256];			// Stores the keyboard data
		MouseState			    _mouseState;			// Stores the mouse data
		bool					_lockMouse;				// True if the mouse should be locked to the screen center

	private:

		// Copying not allowed
		Platform( const Platform& );
		Platform& operator=(const Platform&);

	};
}

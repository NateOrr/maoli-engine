//--------------------------------------------------------------------------------------
// File: Android.h
//
// Android platform implementation
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Platform/Android.h"

namespace Maoli
{
	// Ctor
	AndroidPlatform::AndroidPlatform( Engine* parent ) : Platform( parent )
	{
		FindWorkingDirectory();
		FindProcessorCount();
	}

	// Create a window
	bool AndroidPlatform::InitWindow( int32 width, int32 height )
	{
		// Save the dimensions
		_width = width;
		_height = height;

		return true;
	}


	// Load a DLL
	Handle AndroidPlatform::LoadDLL( const char* file )
	{
		return 0;
	}


	// Display a generic message box
	int32 AndroidPlatform::MessageBox( const char* msg, const char* title, int32 flags /*= 0*/ )
	{
		return 0;
	}


	// Get the working directory
	void AndroidPlatform::FindWorkingDirectory()
	{
		_workingDirectory = "";
	}


	// Get the processor count
	void AndroidPlatform::FindProcessorCount()
	{
		_processorCount = 1;
	}

	// Per frame update
	bool AndroidPlatform::Update( bool processMessages )
	{
		return true;
	}


	// Process available OS messages
	bool AndroidPlatform::ProcessMessages()
	{
		return true;
	}

	// Cleanup
	void AndroidPlatform::Release()
	{

	}

	// Create a file directory
	bool AndroidPlatform::CreateDirectory( const char* dir )
	{
		return false;
	}

	// Copy a file
	bool AndroidPlatform::CopyFile( const char* source, const char* dest )
	{
		return false;
	}

	// Process a single OS message, returns false if the app should exit
	bool AndroidPlatform::ProcessMessage( const OSMessage& msg )
	{
		return false;
	}

	// Get the names of all files in a directory
	void AndroidPlatform::GetFilesInDirectory( const char* dir, Array<String>& files )
	{
		
	}

	// Set the mouse position
	void AndroidPlatform::SetMouseLocation( int32 x, int32 y )
	{
		
	}

	// Hide the mouse cursor
	void AndroidPlatform::HideMouseCursor()
	{
		
	}

	// Show the mouse cursor
	void AndroidPlatform::ShowMouseCursor()
	{
		
	}

	// Check if this is the active window
	bool AndroidPlatform::IsActiveWindow() const
	{
		return true;
	}

	// Set the window handle
	void AndroidPlatform::SetWindow( WindowHandle mainWindow )
	{
		_window = _renderWindow = mainWindow;
	}

	// Set the window handle
	void AndroidPlatform::SetWindow( WindowHandle mainWindow, WindowHandle renderWindow )
	{
		_window = mainWindow;
		_renderWindow = renderWindow;
	}

}

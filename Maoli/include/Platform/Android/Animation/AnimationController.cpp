//--------------------------------------------------------------------------------------
// File: HavokAnimationController.cpp
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "AnimationController.h"
#include "hkaMaoliControllerListener.h"
#include "Game/Entity.h"
#include "AnimationTrack.h"
#include "Animation.h"
#include "Base/Animation/BaseAnimationManager.h"

namespace Maoli
{
	// Ctor
	AnimationController::AnimationController( Engine* engine ) : BaseAnimationController( engine )
	{

	}

	// Cleanup
	AnimationController::~AnimationController()
	{

	}


	// Update the animation
	void AnimationController::Update( float dt )
	{
		if ( _playing )
		{
			// Step the animation and build the pose for this frame
			UpdatePose();

			// Update the tracks
			for ( uint32 i = 0; i < _tracks.Size(); ++i )
				_tracks[i]->Update( dt );
		}
	}


	// Fill the bone palette
	void AnimationController::FillBonePalette( float* palette )
	{

	}


	// Add an animation track
	void AnimationController::AddTrack( BaseAnimationTrack* track, uint32 index )
	{
		// Add the track
		_tracks.Insert( track, index );
		track->EnableLoop( true );
		_playing = true;
	}

	// Build the current pose
	void AnimationController::UpdatePose()
	{
		_entity->BroadcastMessage( ComponentEvent::AnimationUpdated, this, nullptr );
	}
}

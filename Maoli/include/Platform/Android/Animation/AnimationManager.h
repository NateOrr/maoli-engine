//--------------------------------------------------------------------------------------
// File: HavokAnimationManager.h
//
// Manages animations
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Engine\System.h"
#include "Animation\Base\BaseAnimationManager.h"

namespace Maoli
{
	class AnimationManager : public BaseAnimationManager
	{
	public:

		// Ctor
		AnimationManager(Engine* engine);
	};
}

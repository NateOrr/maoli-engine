//--------------------------------------------------------------------------------------
// File: HavokAnimationController.h
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Base/Animation/BaseAnimationController.h"

namespace Maoli
{
	// An instance of an animated mesh
	class AnimationController : public BaseAnimationController
	{
		friend class Animation;
	public:

		// Component setup
		COMPONENT_DECL( AnimationController );

		// Init
		AnimationController( Engine* engine );

		// Cleanup
		~AnimationController();


		//////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Fill the bone palette
		virtual void FillBonePalette( float* palette );

		// Build the current pose
		virtual void UpdatePose();

	private:

		// Add an animation track
		virtual void AddTrack( BaseAnimationTrack* track, uint32 index );

	};

}

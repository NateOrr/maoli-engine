//--------------------------------------------------------------------------------------
// File: HavokAnimation.h
//
// Animation interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Base/Animation/BaseAnimation.h"

namespace Maoli
{
	// Animation interface
	class Animation : public BaseAnimation
	{
		friend class AnimationController;
		friend class BaseAnimationManager;

	public:

		// Load a havok animation
		virtual bool LoadTrack( const char* file, uint32 index );

		// Delete an animation track, there must be more than 1 track for this to be valid
		virtual void DeleteTrack( uint32 trackIndex );

		// Create a new instance of this controller for use in the game engine
		virtual BaseAnimationController* CreateInstance(BaseAnimationController* instance = nullptr);

	private:

		// Ctor
		Animation( Engine* engine );

		// Dtor
		~Animation();

		// Cleanup
		virtual void Release();

		// Load a havok animation
		bool LoadAnimation( const char* file, uint32 index );

		// Load a havok skin
		bool LoadSkin( const char* file, uint32 index );

		uint32								_numBones;		// Total number of bones
	};

}

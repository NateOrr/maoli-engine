//--------------------------------------------------------------------------------------
// File: HavokAnimation.cpp
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"

#include "Animation.h"
#include "AnimationTrack.h"
#include "AnimationController.h"

namespace Maoli
{
	// Ctor
	Animation::Animation( Engine* engine ) : BaseAnimation( engine )
	{
		_numBones = 0;
	}


	// Dtor
	Animation::~Animation()
	{

	}

	// Cleanup
	void Animation::Release()
	{

	}

	// Load a havok animation
	bool Animation::LoadTrack( const char* file, uint32 index )
	{
		// Check if its already loaded
		for ( uint32 i = 0; i < _trackFiles.Size(); ++i )
		{
			if ( _trackFiles[i] == file )
				return true;
		}

		if ( _trackNames.Size() > 0 )
			return LoadAnimation( file, index );
		return LoadSkin( file, index );
	}

	// Havok setup
	bool Animation::LoadAnimation( const char* file, uint32 index )
	{
		return true;
	}


	// Load a havok skin
	bool Animation::LoadSkin( const char* file, uint32 index )
	{
		return true;
	}

	// Create a new instance of this controller for use in the game engine
	BaseAnimationController* Animation::CreateInstance( BaseAnimationController* instance )
	{
		String name = GetName() + _instances.Size();
		if ( !instance )
			instance = new AnimationController( _parent );
		AddInstance( instance );
		AnimationController* controller = (AnimationController*)instance;
		controller->_controller = BaseAnimation::pointer( this );
		return instance;
	}


	// Delete an animation track, there must be more than 1 track for this to be valid
	void Animation::DeleteTrack( uint32 trackIndex )
	{
		// Internal delete
		_trackNames.RemoveAt( trackIndex );
		_trackFiles.RemoveAt( trackIndex );

		// Update all the instances
		for ( uint32 i = 0; i < _instances.Size(); ++i )
		{
			AnimationController* controller = (AnimationController*)_instances[i];
			controller->_tracks[trackIndex]->Stop();
			delete controller->_tracks.RemoveAt( trackIndex );
			if ( controller->GetNumTracks() > 0 )
				controller->GetTrack( 0 )->Play();
		}
	}



}

//--------------------------------------------------------------------------------------
// File: AnimationTrack.h
//
// An animation track
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Base/Animation/BaseAnimationTrack.h"

namespace Maoli
{

	class AnimationTrack : public BaseAnimationTrack
	{
		friend class AnimationController;
		friend class Animation;
	public:

		// Ctor
		AnimationTrack( BaseAnimationController* controller );

		// Update timing and process events
		virtual void Update( float dt );

		// Set a single track to max weight, easing over a duration
		virtual void Play( float easeDuration = 0.35f );

		// Ease a track into play over a duration
		virtual void EaseIn( float duration );

		// Ease a track out of play over a duration
		virtual void EaseOut( float duration );

		// Stop playing the track
		virtual void Stop();

		// Set the playback speed of a track
		virtual void SetSpeed( float speed );

		// Set the blend weight for a track
		virtual void SetWeight( float weight );

		// Enable or disable track looping
		virtual void EnableLoop( bool loop );

		// Set the local time for the track
		virtual void SetTime( float time, bool updatePose );

	private:


	};
}
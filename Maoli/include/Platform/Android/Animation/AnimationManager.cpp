//--------------------------------------------------------------------------------------
// File: HavokAnimationManager.cpp
//
// Manages animations
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "AnimationManager.h"
#include "Animation.h"

namespace Maoli
{
	// Ctor
	AnimationManager::AnimationManager( Engine* engine ) : BaseAnimationManager( engine )
	{

	}

}

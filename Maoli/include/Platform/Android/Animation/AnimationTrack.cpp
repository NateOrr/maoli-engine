//--------------------------------------------------------------------------------------
// File: AnimationTrack.cpp
//
// An animation track
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "AnimationTrack.h"
#include "Animation\Base\BaseAnimationController.h"

namespace Maoli
{
	// Ctor
	AnimationTrack::AnimationTrack( BaseAnimationController* controller ) : BaseAnimationTrack( controller )
	{

	}

	// Set a single track to max weight, easing over a duration
	void AnimationTrack::Play( float easeDuration /*= 0.35f */ )
	{

	}

	// Ease a track into play over a duration
	void AnimationTrack::EaseIn( float easeDuration )
	{

	}

	// Ease a track into play over a duration
	void AnimationTrack::EaseOut( float easeDuration )
	{

	}

	// Stop playing the track
	void AnimationTrack::Stop()
	{
		_isPlaying = false;
	}

	// Set the playback speed of a track
	void AnimationTrack::SetSpeed( float speed )
	{
		_speed = speed;
	}

	// Set the blend weight for a track
	void AnimationTrack::SetWeight( float weight )
	{
		_weight = weight;
	}

	// Enable or disable track looping
	void AnimationTrack::EnableLoop( bool loop )
	{
		_loop = loop;
	}

	// Set the local time for the track
	void AnimationTrack::SetTime( float time, bool updatePose )
	{
		_time = time;
		if ( updatePose )
			_controller->UpdatePose();
	}

	// Update timing and process events
	void AnimationTrack::Update( float dt )
	{
		if ( IsPlaying() )
		{
			// Update the timing
			_prevTime = _time;
			_time += dt;

			// Check if any events should be triggered
			for ( uint32 j = 0; j < _events.Size(); ++j )
			{
				if ( _events[j].time >= _time && _events[j].time <= _prevTime )
				{
					_controller->GetOwner()->BroadcastMessage( _events[j].id, _controller, nullptr );
				}
			}
		}
	}

}
//--------------------------------------------------------------------------------------
// File: HavokAnimationManager.cpp
//
// Manages animations
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "AnimationManager.h"
#include "Animation.h"

namespace Maoli
{
	// Ctor
	AnimationManager::AnimationManager( Engine* engine ) : BaseAnimationManager( engine )
	{

	}

	// Init
	bool AnimationManager::Init()
	{
		BaseAnimationManager::Init();

		// Init havok
		return Havok::Init();
	}

	void AnimationManager::Update( float dt )
	{

	}

	// Cleanup
	void AnimationManager::Release()
	{
		BaseAnimationManager::Release();
	}


}

//--------------------------------------------------------------------------------------
// File: HavokAnimationManager.h
//
// Manages animations
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Engine/System.h"
#include "Base/Animation/BaseAnimationManager.h"

namespace Maoli
{
	class AnimationManager : public BaseAnimationManager
	{
	public:

		// Ctor
		AnimationManager(Engine* engine);

		////////////////////////////////
		// Required interface

		// Init
		virtual bool Init();

		// Frame update
		virtual void Update( float dt );

		// Cleanup
		virtual void Release();
		
	};
}

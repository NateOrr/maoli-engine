//--------------------------------------------------------------------------------------
// File: hkaMaoliControllerListener.cpp
//
// Animation controller callbacks
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "hkaMaoliControllerListener.h"
#include "Game/Entity.h"
#include "AnimationController.h"

namespace Maoli
{
	// Ctor
	hkaMaoliControllerListener::hkaMaoliControllerListener( AnimationController* owner ) : _owner(owner)
	{

	}

	// Called whenever the hkaDefaultAnimationControl overflows (hkaDefaultAnimationControl::update passes the end of the animation) 
	void hkaMaoliControllerListener::loopOverflowCallback( hkaDefaultAnimationControl *control, hkReal deltaTime, hkUint32 overflows )
	{
		control->easeOut( 0.25f );
		_owner->GetOwner()->BroadcastMessage( ComponentEvent::AnimationFinished, _owner, nullptr );
	}

}

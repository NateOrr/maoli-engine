//--------------------------------------------------------------------------------------
// File: AnimationTrack.cpp
//
// An animation track
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "AnimationTrack.h"
#include "Base\Animation\BaseAnimationController.h"
#include "hkaMaoliControllerListener.h"

namespace Maoli
{
	// Ctor
	AnimationTrack::AnimationTrack( BaseAnimationController* controller ) : BaseAnimationTrack( controller )
	{
		_havokController = nullptr;
		_skeleton = nullptr;
		_loopListener = nullptr;
	}

	// Set a single track to max weight, easing over a duration
	void AnimationTrack::Play( float easeDuration /*= 0.35f */ )
	{
		// Clear the other tracks
		_controller->ClearTracks( easeDuration );

		// Ease this track in
		EaseIn( easeDuration );
	}

	// Ease a track into play over a duration
	void AnimationTrack::EaseIn( float easeDuration )
	{
		// Make sure the track is in the skeleton
		if ( !_isPlaying )
		{
			_skeleton->addAnimationControl( _havokController );
			_isPlaying = true;
		}

		// Reset non-looping tracks
		if ( !_loop )
			SetTime( 0, false );

		// Blend in over time
		_havokController->easeIn( easeDuration );
	}

	// Ease a track into play over a duration
	void AnimationTrack::EaseOut( float easeDuration )
	{
		_havokController->easeOut( easeDuration );
	}

	// Stop playing the track
	void AnimationTrack::Stop()
	{
		_skeleton->removeAnimationControl( _havokController );
		_isPlaying = false;
	}

	// Set the playback speed of a track
	void AnimationTrack::SetSpeed( float speed )
	{
		_speed = speed;
		_havokController->setPlaybackSpeed( speed );
	}

	// Set the blend weight for a track
	void AnimationTrack::SetWeight( float weight )
	{
		_weight = weight;
		_havokController->setMasterWeight( weight );
	}

	// Enable or disable track looping
	void AnimationTrack::EnableLoop( bool loop )
	{
		_loop = loop;
		if ( loop && !_loop )
			_havokController->removeDefaultControlListener( _loopListener );
		else if ( !loop && _loop )
			_havokController->addDefaultControlListener( _loopListener );
	}

	// Set the local time for the track
	void AnimationTrack::SetTime( float time, bool updatePose )
	{
		_time = time;
		_havokController->setLocalTime( time );
		if ( updatePose )
			_controller->UpdatePose();
	}

	// Update timing and process events
	void AnimationTrack::Update( float dt )
	{
		if ( IsPlaying() )
		{
			// Update the timing
			_prevTime = _time;
			_time = _havokController->getLocalTime();

			// Check if any events should be triggered
			for ( uint32 j = 0; j < _events.Size(); ++j )
			{
				if ( _events[j].time >= _time && _events[j].time <= _prevTime )
				{
					_controller->GetOwner()->BroadcastMessage( _events[j].id, _controller, nullptr );
				}
			}
		}
	}

	// Called when a non-looping animation has finished easing out
	void AnimationTrack::easedOutCallback( hkaDefaultAnimationControl* control, hkReal deltaTime )
	{
		// Stop processing the animation
		_skeleton->removeAnimationControl( control );
		_isPlaying = false;
	}

}
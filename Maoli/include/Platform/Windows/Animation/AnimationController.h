//--------------------------------------------------------------------------------------
// File: HavokAnimationController.h
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

// Forward decl
class hkaDefaultAnimationControl;
class hkaAnimatedSkeleton;
class hkaMeshBinding;
class hkaPose;

#include "Base/Animation/BaseAnimationController.h"

namespace Maoli
{
	// Forward decl
	class hkaMaoliControllerListener;

	// An instance of an animated mesh
	class AnimationController : public BaseAnimationController
	{
		friend class Animation;
	public:

		// Component setup
		COMPONENT_DECL( AnimationController );

		// Init
		AnimationController( Engine* engine );

		// Cleanup
		~AnimationController();


		//////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update( float dt );

		// Fill the bone palette
		virtual void FillBonePalette( float* palette );

		// Build the current pose
		virtual void UpdatePose();

	private:

		// Add an animation track
		virtual void AddTrack( BaseAnimationTrack* track, uint32 index );

		hkaAnimatedSkeleton*						_skeleton;		// Instance of this animation
		hkaPose*									_pose;			// Current pose
		hkaMaoliControllerListener*					_loopListener;	// Listener for handling non-looping animations

		// For accessing animation tracks via their control (useful for havok callbacks)
		std::unordered_map<hkaDefaultAnimationControl*, BaseAnimationTrack*>	_trackMap;

	};

}

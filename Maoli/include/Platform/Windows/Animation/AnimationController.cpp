//--------------------------------------------------------------------------------------
// File: HavokAnimationController.cpp
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "AnimationController.h"
#include "hkaMaoliControllerListener.h"
#include "Game/Entity.h"
#include "AnimationTrack.h"
#include "Animation.h"
#include "Base/Animation/BaseAnimationManager.h"

namespace Maoli
{
	// Ctor
	AnimationController::AnimationController( Engine* engine ) : BaseAnimationController( engine )
	{
		_skeleton = nullptr;
		_pose = nullptr;
		_loopListener = new hkaMaoliControllerListener( this );
	}

	// Cleanup
	AnimationController::~AnimationController()
	{
		delete _skeleton;
		delete _pose;
		delete _loopListener;
	}


	// Update the animation
	void AnimationController::Update( float dt )
	{
		if ( _playing )
		{
			// Step the animation and build the pose for this frame
			_skeleton->stepDeltaTime( dt );
			UpdatePose();

			// Update the tracks
			for ( uint32 i = 0; i < _tracks.Size(); ++i )
				_tracks[i]->Update( dt );
		}
	}


	// Fill the bone palette
	void AnimationController::FillBonePalette( float* palette )
	{
		// Grab the pose in model space
		const hkArray<hkQsTransform>& poseModelSpace = _pose->getSyncedPoseModelSpace();

		// Build the final transforms that will be used for rendering
		hkTransform t;
		Animation* controller = (Animation*)*_controller;
		for ( uint32 i = 0; i < controller->GetBindings().Size(); ++i )
		{
			// Multiply through by the bind pose inverse world inverse matrices, according to the skel to mesh bone mapping
			for ( int32 p = 0; p < controller->GetBindings()[i]->m_boneFromSkinMeshTransforms.getSize(); p++ )
			{
				t.setMul( poseModelSpace[p], controller->GetBindings()[i]->m_boneFromSkinMeshTransforms[p] );
				t.get4x4ColumnMajor( &palette[p * 16] );
			}
		}
	}



	// Add an animation track
	void AnimationController::AddTrack( BaseAnimationTrack* track, uint32 index )
	{
		// Add the track
		_tracks.Insert( track, index );
		track->EnableLoop( true );
		_playing = true;

		// Setup the havok stuff
		AnimationTrack* havokTrack = (AnimationTrack*)track;
		_trackMap[havokTrack->_havokController] = track;
		havokTrack->_havokController->addDefaultControlListener( havokTrack );
		havokTrack->_skeleton = _skeleton;
		havokTrack->_loopListener = _loopListener;
	}

	// Build the current pose
	void AnimationController::UpdatePose()
	{
		_skeleton->sampleAndCombineAnimations( _pose->accessUnsyncedPoseLocalSpace().begin(), _pose->getFloatSlotValues().begin() );
		_entity->BroadcastMessage( ComponentEvent::AnimationUpdated, this, nullptr );
	}
}

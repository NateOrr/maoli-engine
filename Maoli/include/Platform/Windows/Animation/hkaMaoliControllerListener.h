//--------------------------------------------------------------------------------------
// File: hkaMaoliControllerListener.h
//
// Animation controller callbacks
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class AnimationController;

	// Animation controller callbacks
	class hkaMaoliControllerListener : public hkaDefaultAnimationControlListener
	{
	public:

		// Ctor
		hkaMaoliControllerListener(AnimationController* owner);

		// Called whenever the hkaDefaultAnimationControl overflows (hkaDefaultAnimationControl::update passes the end of the animation) 
		virtual void loopOverflowCallback (hkaDefaultAnimationControl *control, hkReal deltaTime, hkUint32 overflows);

	private:

		AnimationController*	_owner;

	};
}

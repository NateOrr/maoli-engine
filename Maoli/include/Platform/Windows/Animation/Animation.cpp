//--------------------------------------------------------------------------------------
// File: HavokAnimation.cpp
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"

#include "Animation.h"
#include "hkaMaoliControllerListener.h"
#include "Engine/Engine.h"
#include "AnimationTrack.h"
#include "AnimationController.h"

namespace Maoli
{

	// Ctor
	Animation::Animation( Engine* engine ) : BaseAnimation( engine )
	{
		_loader = new hkLoader();
		_skeleton = nullptr;
		_numBones = 0;
	}


	// Dtor
	Animation::~Animation()
	{
		delete _loader;
	}

	// Cleanup
	void Animation::Release()
	{

	}

	// Load a havok animation
	bool Animation::LoadTrack( const char* file, uint32 index )
	{
		// Check if its already loaded
		for ( uint32 i = 0; i < _trackFiles.Size(); ++i )
		{
			if ( _trackFiles[i] == file )
				return true;
		}

		if ( _trackNames.Size() > 0 )
			return LoadAnimation( file, index );
		return LoadSkin( file, index );
	}

	// Havok setup
	bool Animation::LoadAnimation( const char* file, uint32 index )
	{
		hkRootLevelContainer* container = _loader->load( file );
		HK_ASSERT2( 0x27343437, container != HK_NULL, "Could not load asset" );
		hkaAnimationContainer* ac = reinterpret_cast<hkaAnimationContainer*>(container->findObjectByType( hkaAnimationContainerClass.getName() ));

		HK_ASSERT2( 0x27343435, ac && (ac->m_animations.getSize() > 0), "No animation loaded" );
		HK_ASSERT2( 0x27343435, ac && (ac->m_bindings.getSize() > 0), "No binding loaded" );

		if ( ac->m_animations.getSize() > 1 )
			_parent->GetPlatform()->MessageBox( "Support for multi animation files is not yet available" );
		for ( int32 i = 0; i < ac->m_animations.getSize(); ++i )
			_animations.Insert( ac->m_animations[i], index );

		String trackName = String( file ).GetFile().RemoveFileExtension();
		_trackFiles.Insert( file, index );
		_trackNames.Insert( trackName, index );
		if ( ac->m_bindings.getSize() >1 )
			_parent->GetPlatform()->MessageBox( "Support for multi animation files is not yet available" );
		for ( int32 i = 0; i < ac->m_bindings.getSize(); ++i )
			_bindings.Insert( ac->m_bindings[i], index );

		// Update the instances
		for ( uint32 i = 0; i < _instances.Size(); ++i )
		{
			AnimationController* controller = (AnimationController*)_instances[i];
			for ( int32 k = 0; k < ac->m_bindings.getSize(); k++ )
			{
				AnimationTrack* track = new AnimationTrack( _instances[i] );
				track->_duration = _animations[k]->m_duration;
				track->_havokController = new hkaDefaultAnimationControl( ac->m_bindings[k] );
				controller->AddTrack( track, index );
			}
		}

		return true;
	}


	// Load a havok skin
	bool Animation::LoadSkin( const char* file, uint32 index )
	{
		// Load in the file
		hkRootLevelContainer* container = _loader->load( file );
		HK_ASSERT2( 0x27343437, container != HK_NULL, "Could not load asset" );
		hkxScene* scene = reinterpret_cast<hkxScene*>(container->findObjectByType( hkxSceneClass.getName() ));
		HK_ASSERT2( 0x27343435, scene, "No scene loaded" );
		hkaAnimationContainer* ac = reinterpret_cast<hkaAnimationContainer*>(container->findObjectByType( hkaAnimationContainerClass.getName() ));
		HK_ASSERT2( 0x27343435, ac && (ac->m_skins.getSize() > 0), "No skins loaded" );
		HK_ASSERT2( 0x27343435, ac && (ac->m_skeletons.getSize() > 0), "No skins loaded" );

		// Get the skeleton and skins
		_skinBindings.Allocate( ac->m_skins.getSize() );
		for ( int32 i = 0; i < ac->m_skins.getSize(); ++i )
			_skinBindings[i] = ac->m_skins[i];
		_skeleton = ac->m_skeletons[0];

		// Load in any animations in this file
		if ( ac->m_animations.getSize() > 1 )
			_parent->GetPlatform()->MessageBox( "Support for multi animation files is not yet available" );
		for ( int32 i = 0; i < ac->m_animations.getSize(); ++i )
			_animations.Insert( ac->m_animations[i], index );

		String trackName = String( file ).GetFile().RemoveFileExtension();
		_trackNames.Insert( trackName, index );
		_trackFiles.Insert( file, index );
		if ( ac->m_bindings.getSize() >1 )
			_parent->GetPlatform()->MessageBox( "Support for multi animation files is not yet available" );
		for ( int32 i = 0; i < ac->m_bindings.getSize(); ++i )
			_bindings.Insert( ac->m_bindings[i], index );

		// Get the total number of bones used
		_numBones = 0;
		for ( uint32 i = 0; i < _skinBindings.Size(); ++i )
			_numBones += _skinBindings[i]->m_boneFromSkinMeshTransforms.getSize();

		// Update the instances
		for ( uint32 i = 0; i < _instances.Size(); ++i )
		{
			AnimationController* controller = (AnimationController*)_instances[i];
			controller->_numBones = _numBones;
			controller->_skeleton = new hkaAnimatedSkeleton( _skeleton );
			controller->_pose = new hkaPose( controller->_skeleton->getSkeleton() );

			for ( int32 k = 0; k < ac->m_bindings.getSize(); k++ )
			{
				AnimationTrack* track = new AnimationTrack( _instances[i] );
				track->_duration = _animations[k]->m_duration;
				track->_havokController = new hkaDefaultAnimationControl( ac->m_bindings[k] );
				controller->AddTrack( track, index );
			}
			controller->_skeleton->setReferencePoseWeightThreshold( 0.5f );
		}
		return true;
	}

	// Create a new instance of this controller for use in the game engine
	BaseAnimationController* Animation::CreateInstance( BaseAnimationController* instance )
	{
		String name = GetName() + _instances.Size();
		if ( !instance )
			instance = new AnimationController( _parent );
		AddInstance( instance );
		AnimationController* controller = (AnimationController*)instance;
		controller->_controller = BaseAnimation::pointer( this );
		if ( _skeleton )
		{
			controller->_numBones = _numBones;
			controller->_skeleton = new hkaAnimatedSkeleton( _skeleton );
			controller->_pose = new hkaPose( controller->_skeleton->getSkeleton() );

			for ( uint32 k = 0; k < _bindings.Size(); k++ )
			{
				AnimationTrack* track = new AnimationTrack( controller );
				track->_duration = _animations[k]->m_duration;
				track->_havokController = new hkaDefaultAnimationControl( _bindings[k] );
				controller->AddTrack( track, k );
			}
			controller->_skeleton->setReferencePoseWeightThreshold( 0.5f );
		}
		return instance;
	}


	// Delete an animation track, there must be more than 1 track for this to be valid
	void Animation::DeleteTrack( uint32 trackIndex )
	{
		// Internal delete
		_animations.RemoveAt( trackIndex );
		_bindings.RemoveAt( trackIndex );
		_trackNames.RemoveAt( trackIndex );
		_trackFiles.RemoveAt( trackIndex );

		// Update all the instances
		for ( uint32 i = 0; i < _instances.Size(); ++i )
		{
			AnimationController* controller = (AnimationController*)_instances[i];
			controller->_tracks[trackIndex]->Stop();
			delete controller->_tracks.RemoveAt( trackIndex );
			if ( controller->GetNumTracks() > 0 )
				controller->GetTrack( 0 )->Play();
		}
	}
}

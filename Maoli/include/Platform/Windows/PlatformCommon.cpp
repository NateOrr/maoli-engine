//--------------------------------------------------------------------------------------
// File: Havok.cpp
//
// Havok Support
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "PlatformCommon.h"

namespace Maoli
{
	// Static members
	bool Havok::_isLoaded = false;

	// Havok error handling
	static void HavokErrorReport( const char* msg, void* userContext )
	{
		std::cout << msg << std::endl;
	}

	// Setup
	bool Havok::Init( int32 memoryPoolSize )
	{
		if ( !_isLoaded )
		{
			// Setup havok math
			_MM_SET_FLUSH_ZERO_MODE( _MM_FLUSH_ZERO_ON );

			// Need to have memory allocated for the solver. Allocate 1mb for it.
			hkMemoryRouter* memoryRouter = hkMemoryInitUtil::initDefault( hkMallocAllocator::m_defaultMallocAllocator, hkMemorySystem::FrameInfo( memoryPoolSize ) );
			_isLoaded = hkBaseSystem::init( memoryRouter, HavokErrorReport ).isSuccess();
		}
		return _isLoaded;
	}

	// Cleanup
	void Havok::Release()
	{
		if ( _isLoaded )
		{
			hkBaseSystem::quit();
			hkMemoryInitUtil::quit();
			_isLoaded = false;
		}
	}

}

// Required for havok
#include <Common/Base/KeyCode.cxx>

// Exclude things that are not needed
#undef HK_FEATURE_PRODUCT_AI
#undef HK_FEATURE_PRODUCT_CLOTH
#undef HK_FEATURE_PRODUCT_DESTRUCTION
#undef HK_FEATURE_PRODUCT_component
#undef HK_FEATURE_PRODUCT_MILSIM
#include <Common/Base/Config/hkProductFeatures.cxx>
//--------------------------------------------------------------------------------------
// File: Havok.h
//
// Havok Support
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

//--------------------------------------------------------------------------------------
// Windows
//--------------------------------------------------------------------------------------
#ifndef MAOLI_HAVOK
#define MAOLI_HAVOK

#define NOMINMAX
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

// Havok defines
#define HK_PLATFORM_SIM

// Havok base
#include <Common/Base/hkBase.h>
#include <Common/Base/System/hkBaseSystem.h>
#include <Common/Base/Memory/System/Util/hkMemoryInitUtil.h>
#include <Common/Base/Memory/Allocator/Malloc/hkMallocAllocator.h>

// Havok Animation
#include <Animation/Animation/hkaAnimation.h>
#include <Animation/Animation/Animation/hkaAnimationBinding.h>
#include <Animation/Animation/Deform/Skinning/hkaMeshBinding.h>
#include <Animation/Animation/Playback/hkaAnimatedSkeleton.h>
#include <Animation/Animation/hkaAnimationContainer.h>
#include <Animation/Animation/Playback/Control/Default/hkaDefaultAnimationControl.h>
#include <Animation/Animation/Playback/Control/Default/hkaDefaultAnimationControlListener.h> 
#include <Animation/Animation/Rig/hkaPose.h>
#include <Common/Serialize/Util/hkLoader.h>
#include <Common/Serialize/Util/hkRootLevelContainer.h>
#include <Common/SceneData/Scene/hkxScene.h>

// Havok Physics
#include <Physics2012/Collide/hkpCollide.h>
#include <Physics2012/Collide/Agent/ConvexAgent/SphereBox/hkpSphereBoxAgent.h>
#include <Physics2012/Collide/Shape/Convex/Box/hkpBoxShape.h>
#include <Physics2012/Collide/Shape/Convex/Sphere/hkpSphereShape.h>
#include <Physics2012/Collide/Dispatch/hkpAgentRegisterUtil.h>
#include <Common/Base/Types/Geometry/hkStridedVertices.h>
#include <Physics2012/Collide/Shape/Convex/ConvexVertices/hkpConvexVerticesShape.h>
#include <Physics2012/Dynamics/World/hkpWorld.h>
#include <Physics2012/Dynamics/Entity/hkpRigidBody.h>
#include <Physics2012/Utilities/Dynamics/Inertia/hkpInertiaTensorComputer.h>
#include <Common/Base/Types/Geometry/hkGeometry.h>
#include <Physics2012/Internal/Collide/BvCompressedMesh/hkpBvCompressedMeshShapeCinfo.h>
#include <Physics2012/Utilities/CharacterControl/CharacterRigidBody/hkpCharacterRigidBody.h>
#include <Physics2012/Utilities/CharacterControl/CharacterRigidBody/hkpCharacterRigidBodyListener.h>
#include <Physics2012/Collide/Shape/Convex/Capsule/hkpCapsuleShape.h>
#include <Physics2012/Collide/Shape/Convex/Cylinder/hkpCylinderShape.h>
#include <Physics2012/Dynamics/Phantom/hkpAabbPhantom.h>
#include <Physics2012/Collide/Shape/Query/hkpShapeRayCastInput.h>
#include <Physics2012/Collide/Shape/HeightField/TriSampledHeightField/hkpTriSampledHeightFieldCollection.h>
#include <Physics2012/Collide/Shape/HeightField/TriSampledHeightField/hkpTriSampledHeightFieldBvTreeShape.h> 
#include <Physics2012/Utilities/CharacterControl/StateMachine/hkpCharacterContext.h>
#include <Physics2012/Utilities/CharacterControl/StateMachine/hkpDefaultCharacterStates.h>
#include <Physics2012/Collide/Filter/Group/hkpGroupFilter.h>
#include <Physics2012/Collide/Agent3/Machine/Nn/hkpLinkedCollidable.h>
#include <Physics2012/Collide/Agent3/Machine/Nn/hkpAgentNnTrack.h>
#include <Physics2012/Dynamics/Collide/hkpSimpleConstraintContactMgr.h> 
#include <Physics2012/Dynamics/Collide/Deprecated/hkpCollisionListener.h>
#include <Physics2012/Collide/Query/CastUtil/hkpWorldRayCastOutput.h> 
#include <Physics2012/Collide/Query/CastUtil/hkpWorldRayCastInput.h> 
#include <Physics2012/Collide/Shape/HeightField/SampledHeightField/hkpSampledHeightFieldShape.h>
#include <Physics2012/Collide/Query/CastUtil/hkpLinearCastInput.h>
#include <Physics2012/Collide/Query/Collector/PointCollector/hkpClosestCdPointCollector.h>

// Visual debugger
#include <Common/Visualize/hkVisualDebugger.h>
#include <Physics2012/Utilities/VisualDebugger/hkpPhysicsContext.h>
#include <Common/Base/Reflection/Registry/hkVtableClassRegistry.h>

// Havok libs
#ifdef _DEBUG
#pragma comment(lib, "debug_dll/hkBase.lib")
#pragma comment(lib, "debug_dll/hkSerialize.lib")
#pragma comment(lib, "debug_dll/hkSceneData.lib")
#pragma comment(lib, "debug_dll/hkInternal.lib")
#pragma comment(lib, "debug_dll/hkGeometryUtilities.lib")
#pragma comment(lib, "debug_dll/hkVisualize.lib")
#pragma comment(lib, "debug_dll/hkCompat.lib")
#pragma comment(lib, "debug_dll/hkpCollide.lib")
#pragma comment(lib, "debug_dll/hkpConstraint.lib")
#pragma comment(lib, "debug_dll/hkpConstraintSolver.lib")
#pragma comment(lib, "debug_dll/hkpDynamics.lib")
#pragma comment(lib, "debug_dll/hkpInternal.lib")
#pragma comment(lib, "debug_dll/hkpUtilities.lib")
#pragma comment(lib, "debug_dll/hkpVehicle.lib")
#pragma comment(lib, "debug_dll/hkaAnimation.lib")
#pragma comment(lib, "debug_dll/hkaInternal.lib")
#pragma comment(lib, "debug_dll/hkaPhysics2012Bridge.lib")
#pragma comment(lib, "debug_dll/hkcdInternal.lib")
#pragma comment(lib, "debug_dll/hkcdCollide.lib")
#else
#pragma comment(lib, "release_dll/hkBase.lib")
#pragma comment(lib, "release_dll/hkSerialize.lib")
#pragma comment(lib, "release_dll/hkSceneData.lib")
#pragma comment(lib, "release_dll/hkInternal.lib")
#pragma comment(lib, "release_dll/hkGeometryUtilities.lib")
#pragma comment(lib, "release_dll/hkVisualize.lib")
#pragma comment(lib, "release_dll/hkCompat.lib")
#pragma comment(lib, "release_dll/hkpCollide.lib")
#pragma comment(lib, "release_dll/hkpConstraint.lib")
#pragma comment(lib, "release_dll/hkpConstraintSolver.lib")
#pragma comment(lib, "release_dll/hkpDynamics.lib")
#pragma comment(lib, "release_dll/hkpInternal.lib")
#pragma comment(lib, "release_dll/hkpUtilities.lib")
#pragma comment(lib, "release_dll/hkpVehicle.lib")
#pragma comment(lib, "release_dll/hkaAnimation.lib")
#pragma comment(lib, "release_dll/hkaInternal.lib")
#pragma comment(lib, "release_dll/hkaPhysics2012Bridge.lib")
#pragma comment(lib, "release_dll/hkcdInternal.lib")
#pragma comment(lib, "release_dll/hkcdCollide.lib")
#endif

#endif

namespace Maoli
{

	// Havok release wrapper
	template <typename T>
	inline void SafeRemoveReference( T*& obj )
	{
		if ( obj )
		{
			obj->removeReference();
			obj = nullptr;
		}
	}

	// Havok systems wrapper
	class Havok
	{
	public:

		// Setup
		static bool Init( int32 memoryPoolSize = 3 * 1024 * 1024 );

		// Cleanup
		static void Release();

	private:

		static bool		_isLoaded;

	};
}

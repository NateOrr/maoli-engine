//--------------------------------------------------------------------------------------
// File: BaseAnimationController.h
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "BaseAnimation.h"

namespace Maoli
{
	// Forward decl
	class BaseAnimationTrack;

	// An instance of an animation
	class BaseAnimationController : public Component
	{
		friend class BaseAnimation;
	public:

		// Ctor
		BaseAnimationController( Engine* engine );

		// Dtor
		virtual ~BaseAnimationController();


		//////////////////////////////////////////////
		// Required interface

		// Per-frame logic
		virtual void Update( float dt ) = 0;

		// Fill the bone palette
		virtual void FillBonePalette( float* palette ) = 0;

		// Build the current pose
		virtual void UpdatePose() = 0;

		// Add an animation track
		virtual void AddTrack( BaseAnimationTrack* track, uint32 index ) = 0;


		//////////////////////////////////////////////
		// Component interface

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		

		// Stop all tracks
		void ClearTracks(float easeDuration = 0.25f);

		// Get the number of bones
		inline int32 GetNumBones() const { return _numBones; }

		// Get the number of animation tracks
		inline uint32 GetNumTracks() const { return _tracks.Size(); }

		// Pause the animation update
		inline void Pause( bool paused ) { _playing = !paused; }

		// Load in an animation track
		bool LoadTrack( const char* fileName );
		bool LoadTrack( const char* fileName, uint32 trackIndex );

		// Delete an animation track
		void DeleteTrack( int32 trackIndex );

		// Get a track from the file name
		BaseAnimationTrack* GetTrackFromFileName( const char* filename ) const;

		// Get a track from the name
		BaseAnimationTrack* GetTrackFromName( const char* name ) const;

		// Get a track by index
		inline BaseAnimationTrack* GetTrack( uint32 track ) { return _tracks[track]; }

		// Check if a track is loaded
		bool IsTrackLoaded( const char* filename );
		

	protected:

		BaseAnimation::pointer			_controller;	// Animation that owns this
		Array<BaseAnimationTrack*>		_tracks;		// List of all tracks
		int32							_numBones;		// Total number of bones
		bool							_playing;		// True to play
	};
}
//--------------------------------------------------------------------------------------
// File: BaseAnimationManager.cpp
//
// Manages animations
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "BaseAnimationManager.h"
#include "Animation\Animation.h"

namespace Maoli
{
	// Ctor
	BaseAnimationManager::BaseAnimationManager( Engine* engine ) : System( engine ), _animations( engine )
	{

	}

	// Init
	bool BaseAnimationManager::Init()
	{
		return true;
	}

	// Cleanup
	void BaseAnimationManager::Release()
	{
		_animations.Release();
	}


	// Create an animation instance
	BaseAnimation::pointer BaseAnimationManager::CreateAnimation( const char* name )
	{
		auto ac = _animations.Get( name );
		if ( !ac )
		{
			ac = BaseAnimation::pointer( new Animation( _engine ) );
			_animations.Add( ac );
		}

		return ac;
	}

	// Register a component to the system
	bool BaseAnimationManager::RegisterComponent( Component* component )
	{
		if ( component->GetTypeID() == typeid(AnimationController) )
		{
			_components.Add( component );
			return true;
		}

		return false;
	}

	// Remove a component (assumes the component exists already in the system!)
	void BaseAnimationManager::UnregisterComponent( Component* component )
	{
		if ( component->GetTypeID() == typeid(AnimationController) )
			_components.Remove( component );
	}
}

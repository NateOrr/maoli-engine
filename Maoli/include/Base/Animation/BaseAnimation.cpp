//--------------------------------------------------------------------------------------
// File: BaseAnimation.cpp
//
// Animation interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "BaseAnimation.h"
#include "BaseAnimationController.h"

namespace Maoli
{

	// Ctor
	BaseAnimation::BaseAnimation(Engine* engine) : Resource(engine)
	{

	}

	// Dtor
	BaseAnimation::~BaseAnimation()
	{
		for ( uint32 i = 0; i < _instances.Size(); i++ )
			delete _instances[i];
	}

	// Add a controller instance to be managed by this animation
	void BaseAnimation::AddInstance( BaseAnimationController* a )
	{
		if ( !_instances.Contains( a ) )
			_instances.Add( a );
	}

	// Remove a controller instance that is no longer needed
	void BaseAnimation::RemoveInstance( BaseAnimationController* a )
	{
		_instances.Remove( a );
	}

	// Load a havok animation
	bool BaseAnimation::LoadTrack( const char* file )
	{
		return LoadTrack( file, _trackFiles.Size() );
	}

	// Rename a track
	void BaseAnimation::RenameTrack( uint32 trackIndex, const String& name )
	{
		_trackNames[trackIndex] = name;
		for ( uint32 i = 0; i < _instances.Size(); ++i )
		{
			_instances[i]->GetTrack( trackIndex )->SetName( name );
		}
	}

}
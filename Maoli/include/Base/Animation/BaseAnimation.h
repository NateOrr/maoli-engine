//--------------------------------------------------------------------------------------
// File: BaseAnimation.h
//
// Animation interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class BaseAnimationController;
	class Engine;

	// Animation interface
	class BaseAnimation : public Resource < Engine >
	{
		friend class AnimationController;
		friend class Pointer < BaseAnimation > ;
	public:

		// Pointer type
		typedef Pointer<BaseAnimation> pointer;

		// Load a havok animation
		bool LoadTrack( const char* file );
		virtual bool LoadTrack( const char* file, uint32 index ) = 0;

		// Delete an animation track, there must be more than 1 track for this to be valid
		virtual void DeleteTrack( uint32 trackIndex ) = 0;

		// Rename a track
		virtual void RenameTrack( uint32 trackIndex, const String& name );

		// Create a new instance of this controller for use in the game engine
		virtual BaseAnimationController* CreateInstance( BaseAnimationController* instance = nullptr ) = 0;

		// Add a controller instance to be managed by this animation
		void AddInstance( BaseAnimationController* a );

		// Remove a controller instance that is no longer needed
		void RemoveInstance( BaseAnimationController* a );

		// Get a track name
		inline const String& GetTrackName( uint32 i ) const { return _trackNames[i]; }

		// Get a track name
		inline const String& GetTrackFileName( uint32 i ) const { return _trackFiles[i]; }

	protected:

		// Ctor
		BaseAnimation( Engine* engine );

		// Dtor
		virtual ~BaseAnimation();

		// Cleanup
		virtual void Release() = 0;

		Array<BaseAnimationController*>	_instances;
		Array<String>					_trackNames;
		Array<String>					_trackFiles;

	private:

		// Prevent copies
		BaseAnimation( const BaseAnimation& );
		BaseAnimation& operator=(const BaseAnimation&);

	};

}

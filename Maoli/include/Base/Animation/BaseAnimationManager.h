//--------------------------------------------------------------------------------------
// File: BaseAnimationManager.h
//
// Manages animations
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Engine\System.h"
#include "BaseAnimation.h"

namespace Maoli
{
	class BaseAnimationManager : public System
	{
	public:

		// Ctor
		BaseAnimationManager(Engine* engine);


		////////////////////////////////
		// Required interface

		// Init
		virtual bool Init() = 0;

		// Frame update
		virtual void Update( float dt ) = 0;

		// Cleanup
		virtual void Release() = 0;

		

		// Register a component to the system
		virtual bool RegisterComponent(Component* component);

		// Remove a component (assumes the component exists already in the system!)
		virtual void UnregisterComponent(Component* component);

		// Create an animation
		BaseAnimation::pointer CreateAnimation( const char* name );

	private:

		ResourceManager<Engine, BaseAnimation>	_animations;

	};
}

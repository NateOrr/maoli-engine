//--------------------------------------------------------------------------------------
// File: BaseAnimationTrack.h
//
// Animation track interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Time based animation event
	struct AnimationEvent
	{
		Event id;
		float time;
	};

	// An animation track
	class BaseAnimationTrack
	{
		friend class BaseAnimationController;
	public:

		// Ctor
		BaseAnimationTrack( BaseAnimationController* controller );

		// Add a time event
		void AddEvent( const Event& id, float time );

		// Remove a time event
		void RemoveEvent( const Event& id );

		// Remove all events
		void RemoveAllEvents();

		// Register for an event
		template <typename T>
		void RegisterEventCallback( const Event& event, T* caller, void (T::*fn)(void*) )
		{
			_eventManager->Register( event, caller, fn );
		}

		// Unregister from an event
		void UnregisterEventCallback( const Event& event, void* caller );

		// Get the name
		inline const String& GetName() const { return _name; }

		// Set the name
		inline void SetName( const String& val ) { _name = val; }

		// Check if a track is looping
		inline bool IsLooping() const { return _loop; }

		// Get the playback speed
		inline float GetSpeed() const { return _speed; }

		// Get the blend weight
		inline float GetWeight() const { return _weight; }

		// Get the file name
		inline const String& GetFileName() const { return _filename; }

		// Get the local track time
		inline float GetTime() const { return _time; }

		// Get the duration
		inline float GetDuration() const { return _duration / _speed; }

		// Set the duration
		inline void SetDuration( float value ) { _duration = value; }

		// Check if this track is playing
		inline bool IsPlaying() const { return _isPlaying; }


		////////////////////////////////////////////////
		// Playback control

		// Update timing and process events
		virtual void Update( float dt ) = 0;

		// Set a single track to max weight, easing over a duration
		virtual void Play( float easeDuration = 0.35f ) = 0;

		// Ease a track into play over a duration
		virtual void EaseIn( float duration ) = 0;

		// Ease a track out of play over a duration
		virtual void EaseOut( float duration ) = 0;

		// Stop playing the track
		virtual void Stop() = 0;

		// Set the playback speed of a track
		virtual void SetSpeed( float speed ) = 0;

		// Set the blend weight for a track
		virtual void SetWeight( float weight ) = 0;

		// Enable or disable track looping
		virtual void EnableLoop( bool loop ) = 0;

		// Set the local time for the track
		virtual void SetTime( float time, bool updatePose = true ) = 0;

	protected:

		BaseAnimationController*		_controller;	// Animation controller
		bool							_loop;			// True if this track should loop
		float							_duration;		// Duration
		bool							_isPlaying;		// True if the track is playing
		float							_speed;			// Playback speed
		float							_weight;		// Blend weight
		Array<AnimationEvent>			_events;		// All events tied to this track
		EventManager					_eventManager;	// Manages event callbacks
		float							_time;			// Local track time
		float							_prevTime;		// Time from last frame
		String							_name;			// Name
		String							_filename;		// File name

	};
}
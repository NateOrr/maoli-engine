//--------------------------------------------------------------------------------------
// File: BaseAnimationController.cpp
//
// Animation controller interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "BaseAnimationController.h"
#include "BaseAnimationTrack.h"
#include "BaseAnimationManager.h"
#include "Engine\Engine.h"

namespace Maoli
{
	// Ctor
	BaseAnimationController::BaseAnimationController( Engine* engine ) : Component( engine )
	{

	}

	// Dtor
	BaseAnimationController::~BaseAnimationController()
	{
		for ( uint32 i = 0; i < _tracks.Size(); ++i )
			delete _tracks[i];

		if ( _controller )
			_controller->RemoveInstance( this );
	}


	// Load in a skin binding
	bool BaseAnimationController::LoadTrack( const char* fileName )
	{
		return LoadTrack( fileName, _tracks.Size() );
	}

	// Load in an animation track
	bool BaseAnimationController::LoadTrack( const char* fileName, uint32 trackIndex )
	{
		if ( !_controller )
		{
			_controller = _engine->GetAnimationManager()->CreateAnimation( fileName );
			_controller->AddInstance( this );
		}
		return _controller->LoadTrack( fileName, trackIndex );
	}

	// Delete an animation track
	void BaseAnimationController::DeleteTrack( int32 trackIndex )
	{
		_controller->DeleteTrack( trackIndex );
	}

	// Get a track from the file name
	BaseAnimationTrack* BaseAnimationController::GetTrackFromFileName( const char* filename ) const
	{
		for ( uint32 i = 0; i < _tracks.Size(); ++i )
		{
			if ( _tracks[i]->GetFileName() == filename )
				return _tracks[i];
		}
		return nullptr;
	}

	// Get a track from the name
	BaseAnimationTrack* BaseAnimationController::GetTrackFromName( const char* name ) const
	{
		for ( uint32 i = 0; i < _tracks.Size(); ++i )
		{
			if ( _tracks[i]->GetName() == name )
				return _tracks[i];
		}
		return nullptr;
	}

	// Check if a track is loaded
	bool BaseAnimationController::IsTrackLoaded( const char* filename )
	{
		for ( uint32 i = 0; i < _tracks.Size(); ++i )
		{
			if ( _tracks[i]->GetFileName() == filename )
				return true;
		}
		return false;
	}

	// Stop all tracks
	void BaseAnimationController::ClearTracks( float easeDuration /*= 0.25f*/ )
	{
		for ( uint32 i = 0; i < _tracks.Size(); ++i )
			_tracks[i]->EaseOut( easeDuration );
	}

	// Binary serialize
	void BaseAnimationController::Export( std::ofstream& fout )
	{
		uint32 numTracks = _tracks.Size();
		_controller->GetName().Serialize( fout );
		Maoli::Serialize( fout, &numTracks );
		for ( uint32 i = 0; i < numTracks; ++i )
		{
			_tracks[i]->GetFileName().Serialize( fout );
			_tracks[i]->GetName().Serialize( fout );
			bool loop = _tracks[i]->IsLooping();
			Maoli::Serialize( fout, &loop );
			float speed = _tracks[i]->GetSpeed();
			Maoli::Serialize( fout, &speed );
		}
	}

	// Binary deserialize
	void BaseAnimationController::Import( std::ifstream& fin )
	{
		uint32 numTracks;
		String controllerName;
		controllerName.Deserialize( fin );
		_controller = _engine->GetAnimationManager()->CreateAnimation( controllerName );
		_controller->CreateInstance( this );
		Maoli::Deserialize( fin, &numTracks );
		bool loop;
		float speed;
		String fileName, trackName;
		for ( uint32 i = 0; i < numTracks; ++i )
		{
			fileName.Deserialize( fin );
			LoadTrack( fileName );
			trackName.Deserialize( fin );
			_tracks[i]->SetName( trackName );
			Maoli::Deserialize( fin, &loop );
			_tracks[i]->EnableLoop( loop );
			Maoli::Deserialize( fin, &speed );
			_tracks[i]->SetSpeed( speed );
		}
		if ( numTracks )
			_tracks[0]->Play();
	}

	// Deep copy clone is required for all components
	Component* BaseAnimationController::Clone( Component* dest /*= nullptr */ ) const
	{
		auto controller = _controller;
		auto clone = controller->CreateInstance();
		if ( clone->GetNumTracks() )
			clone->GetTrack( 0 )->Play();
		for ( uint32 i = 0; i < clone->GetNumTracks(); ++i )
		{
			auto cloneTrack = clone->GetTrack( i );
			cloneTrack->SetSpeed( _tracks[i]->GetSpeed() );
			cloneTrack->EnableLoop( _tracks[i]->IsLooping() );
			cloneTrack->SetName( _tracks[i]->GetName() );
		}
		return clone;
	}


}
//--------------------------------------------------------------------------------------
// File: BaseAnimationTrack.cpp
//
// Animation track interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "BaseAnimationTrack.h"

namespace Maoli
{
	// Ctor
	BaseAnimationTrack::BaseAnimationTrack( BaseAnimationController* controller )
	{
		_controller = controller;
		_loop = true;
		_duration = 1.0f;
		_isPlaying = false;
		_speed = 1.0f;
		_weight = 0;
		_time = 0;
		_prevTime = 0;
		_name = "Track";
	}

	// Add a time event
	void BaseAnimationTrack::AddEvent( const Event& id, float time )
	{
		AnimationEvent& event = _events.Add();
		event.id = id;
		event.time = time;
	}

	// Remove a time event
	void BaseAnimationTrack::RemoveEvent( const Event& id )
	{
		for ( uint32 i = 0; i < _events.Size(); )
		{
			if ( _events[i].id == id )
				_events.RemoveAt( i );
			else
				++i;
		}
	}

	// Remove all events
	void BaseAnimationTrack::RemoveAllEvents()
	{
		_events.Clear();
		_eventManager.Clear();
	}

	// Unregister from an event
	void BaseAnimationTrack::UnregisterEventCallback( const Event& event, void* caller )
	{
		_eventManager.Unregister( event, caller );
	}


}
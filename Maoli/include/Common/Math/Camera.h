//--------------------------------------------------------------------------------------
// File: Camera.h
//
// First person camera
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Transform.h"

namespace Maoli
{
	// Camera types
	enum class CameraType
	{
		FreeRoam,
		Fixed,
	};


	class Camera : public Transform
	{
	public:

		COMPONENT_DECL( Camera );

		// Ctor
		Camera(Engine* engine);

		// Reset to default state
		void Reset();

		///////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update(float dt);

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import(std::ifstream& fin);

		// Deep copy clone is required for all components
		virtual Component* Clone(Component* dest = nullptr) const;

		// Register for any events that need to be handled
		virtual void RegisterEventCallbacks();

		// Get the typeid for this component. This is used as the key inside an entity component map.
		// This is required to be definied in order for inheritance and unique components to
		// function properly together. Only base components should define this.
		virtual std::type_index GetFamilyID() const { return typeid(Camera); }

		///////////////////////////////////////////////
		// Callbacks

		// Respond to position changed message
		void OnPositionChanged( Component* sender, void* userData );

		// Physics update callback
		void OnPhysicsUpdate( Component* sender, void* userData );


		// Construct the projection matrix
		void BuildProjectionMatrix(int32 width, int32 height);

		// Get the projection matrix
		const Matrix& GetProjectionMatrix() const;

		// Get the view matrix
		const Matrix& GetViewMatrix() const;

		// Get the view frustum
		Frustum& GetFrustum();

		// Get the near Z of the clipping plane
		float GetNearZ() const;

		// Set the near Z of the clipping plane
		inline void SetNearZ( float value ) { _nearZ = value; }

		// Get the far Z of the clipping plane
		float GetFarZ() const;

		// Set the far Z of the clipping plane
		inline void SetFarZ(float value){ _farZ = value; }

		// Get the field of view
		float GetFoV() const;

		// Set the field of view
		inline void SetFoV(float value){ _fov = value; }

		// Get a frustum corner
		const Vector3& GetFrustumCorner(FrustrumCorner corner) const;

		// Get the frustum corners
		const Vector3* GetFrustumCorners() const;

		// Update the view frustum
		void UpdateFrustum();

		// Get the width of the current view buffer
		const uint32 GetViewBufferWidth() const;

		// Get the height of the current view buffer
		const uint32 GetViewBufferHeight() const;

		// Set the camera type
		void SetType(CameraType type);
		const CameraType GetType() const;

		// Set the focal point
		void SetFocusPoint(const Vector3& pos);

		// Set the focus zoom
		void SetFocusZoom(float f);

		// Get the focus zoom
		const float GetFocusZoom() const;

		// Clamp the view angles to a reasonable range
		void ClampViewAngles();

	private:

		Matrix			_viewMatrix;			// Inverse camera matrix
		Matrix			_projMatrix;			// Perspective projection matrix
		float			_farZ;					// Far clip Z
		float			_nearZ;					// Near clip Z
		float			_fov;					// Field of view
		float			_fovRatio;				// Ratio of the field of view width/height
		float			_nearPlaneHeight;		// Near plane height
		float			_nearPlaneWidth;		// Near plane width
		float			_farPlaneHeight;		// Far plane height
		float			_farPlaneWidth;			// Far plane width
		float			_focusZoom;				// Zoom radius for fixed camera
		uint32			_viewWidth;				// Viewport width
		uint32			_viewHeight;			// Viewport height
		Vector3			_frustumCorners[8];		// Far then near frustum plane corners
		Vector3			_focusPoint;			// Focal point for fixed cameras
		Frustum			_frustum;				// View frustum
		CameraType		_type;					// The camera type
	};

}

#include "Camera.inl"

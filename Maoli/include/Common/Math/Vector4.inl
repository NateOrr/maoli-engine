//--------------------------------------------------------------------------------------
// File: Vector4.inl
//
// 4D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	inline Vector4::Vector4() : x( 0 ), y( 0 ), z( 0 ), w( 1.0f )
	{

	}

	// Ctor
	inline Vector4::Vector4( float vx, float vy, float vz, float vw /*= 1.0f*/ ) : x( vx ), y( vy ), z( vz ), w( vw )
	{

	}

	// Ctor
	inline Vector4::Vector4( const Vector3& v, float vw /*= 1.0f*/ ) : x( v.x ), y( v.y ), z( v.z ), w( vw )
	{

	}

	// Copy Ctor
	inline Vector4::Vector4( const Vector4& v ) : x( v.x ), y( v.y ), z( v.z ), w( v.w )
	{

	}

	// Move Ctor
	inline Vector4::Vector4( Vector4&& v ) : x( v.x ), y( v.y ), z( v.z ), w( v.w )
	{

	}

	// Assignment operator
	inline Vector4& Vector4::operator=(const Vector4& v)
	{
		x = v.x; y = v.y; z = v.z; w = v.w;
		return *this;
	}

	// Move assignment operator
	inline Vector4& Vector4::operator=(Vector4&& v)
	{
		x = v.x; y = v.y; z = v.z; w = v.w;
		return *this;
	}


	// Fill the data
	inline void Vector4::Set( float x, float y, float z, float w /*= 1.0f*/ )
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}

	// Bracket access
	inline float& Vector4::operator[]( int32 i )
	{
		return _f[i];
	}

	// Constant bracket access
	inline float Vector4::operator[]( int32 i ) const
	{
		return _f[i];
	}


	// Get the 3d component
	inline void Vector4::Load3d( Vector3& v )
	{
		v.x = x;
		v.y = y;
		v.z = z;
	}

	// Auto cast to float pointer
	inline Vector4::operator float*()
	{
		return (float*)this;
	}

	// Auto convert to vector3
	inline Vector4::operator Vector3()
	{
		return *((Vector3*)this);
	}

	// Negation
	inline Maoli::Vector4 Vector4::operator-() const
	{
		return Vector4( -x, -y, -z, -w );
	}

	// Subtraction
	inline Vector4 Vector4::operator-(const Vector4& v) const
	{
		return Vector4( x - v.x, y - v.y, z - v.z, w - v.w );
	}

	// Subtraction
	inline Vector4& Vector4::operator-=(const Vector4& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		w -= v.w;
		return *this;
	}

	// Addition
	inline Vector4 Vector4::operator+(const Vector4& v) const
	{
		return Vector4( x + v.x, y + v.y, z + v.z, w + v.w );
	}

	// Addition
	inline Vector4& Vector4::operator+=(const Vector4& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;
		return *this;
	}


	// Scalar multiplication
	inline Vector4 Vector4::operator*(float f) const
	{
		return Vector4( x*f, y*f, z*f, w*f );
	}


	// Scalar multiplication
	inline Vector4& Vector4::operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		w *= f;
		return *this;
	}


	// Scalar division
	inline Vector4 Vector4::operator/(float f) const
	{
		float d = 1.0f / f;
		return Vector4( x*d, y*d, z*d, w*d );
	}


	// Scalar division
	inline Vector4& Vector4::operator/=(float f)
	{
		float d = 1.0f / f;
		x *= d;
		y *= d;
		z *= d;
		w *= d;
		return *this;
	}



	// Dot product
	inline float Vector4::Dot( const Vector4& v ) const
	{
		return x*v.x + y*v.y + z*v.z + w*v.w;
	}

	// Dot product
	inline float Vector4::Dot( float f ) const
	{
		return x*f + y*f + z*f + w*f;
	}

	// Dot product
	inline float Vector4::Dot3( const Vector4& v ) const
	{
		return x*v.x + y*v.y + z*v.z;
	}

	// Dot product
	inline float Vector4::Dot3( const Vector3& v ) const
	{
		return x*v.x + y*v.y + z*v.z;
	}



	// Cross product of the 3d components
	inline Vector4 Vector4::Cross( const Vector4& v ) const
	{
		return Vector4(
			y*v.z - v.y*z,
			v.x*z - x*v.z,
			x*v.y - v.x*y );
	}

	// Cross product of the 3d components
	inline Vector4 Vector4::Cross( const Vector3& v ) const
	{
		return Vector4(
			y*v.z - v.y*z,
			v.x*z - x*v.z,
			x*v.y - v.x*y );
	}

	// Piecewise multiplication
	inline Vector4 Vector4::ComponentMult( const Vector4& v ) const
	{
		return Vector4( x*v.x, y*v.y, z*v.z, w*v.w );
	}


	// Length
	inline float Vector4::Length() const
	{
		return sqrtf( x*x + y*y + z*z + w*w );
	}

	// Length of the 3d component
	inline float Vector4::Length3d() const
	{
		return sqrtf( x*x + y*y + z*z );
	}


	// Normalize the vector
	inline Vector4& Vector4::Normalize()
	{
		float l = 1.0f / Length();
		x *= l;
		y *= l;
		z *= l;
		w *= l;
		return *this;
	}

	// Normalize the 3d compponent
	inline Vector4& Vector4::Normalize3d()
	{
		float l = 1.0f / Length3d();
		x *= l;
		y *= l;
		z *= l;
		return *this;
	}

	// Max vector
	inline Vector4 Vector4::Max( const Vector4& a, const Vector4& b )
	{
		return Vector4( std::max( a.x, b.x ), std::max( a.y, b.y ), std::max( a.z, b.z ), std::max( a.w, b.w ) );
	}

	// Min vector
	inline Vector4 Vector4::Min( const Vector4& a, const Vector4& b )
	{
		return Vector4( std::min( a.x, b.x ), std::min( a.y, b.y ), std::min( a.z, b.z ), std::min( a.w, b.w ) );
	}

	// Comparison
	inline bool Vector4::operator==(const Vector4& v) const
	{
		return (x == v.x && y == v.y && z == v.z && w == v.w);
	}


	// Comparison
	inline bool Vector4::operator!=(const Vector4& v) const
	{
		return (x != v.x || y != v.y || z != v.z || w != v.w);
	}

	// Vector 3 assignment
	inline Vector4& Vector4::operator=(const Vector3& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		return *this;
	}

	// Convert to 3d vector
	inline Vector3& Vector4::AsVector3()
	{
		return (Vector3&)*this;
	}

	// Convert to 3d vector
	inline const Vector3& Vector4::AsVector3() const
	{
		return (Vector3&)*this;
	}
}

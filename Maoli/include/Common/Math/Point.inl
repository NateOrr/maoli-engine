//--------------------------------------------------------------------------------------
// File: Point.cpp
//
// 2D Point
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	inline Point::Point()
	{
		x = y = 0;
	}

	// Ctor
	inline Point::Point(int32 px, int32 py)
	{
		x = px;
		y = py;
	}

	// Ctor from a vector
	inline Point::Point( const Vector2& v )
	{
		x = (int32)v.x;
		y = (int32)v.y;
	}

	// Copy ctor
	inline Point::Point( const Point& p ) : x(p.x), y(p.y)
	{

	}

	// Assignment op
	inline Point& Point::operator=(const Point& p)
	{
		x = p.x;
		y = p.y;
		return *this;
	}

	// Move ctor
	inline Point::Point( Point&& p ) : x(p.x), y(p.y)
	{

	}

	// Move assignment operator
	inline Point& Point::operator=( Point&& p )
	{
		x = p.x;
		y = p.y;
		return *this;
	}

	// Addition
	inline Point Point::operator+(const Point& p)
	{
		return Point(x+p.x, y+p.y);
	}

	// Addition
	inline Point& Point::operator+=(const Point& p)
	{
		x += p.x;
		y += p.y;
		return *this;
	}


	// Subtraction
	inline Point Point::operator-(const Point& p)
	{
		return Point(x-p.x, y-p.y);
	}

	// Subtraction
	inline Point& Point::operator-=(const Point& p)
	{
		x -= p.x;
		y -= p.y;
		return *this;
	}


	// Scalar multiplication
	inline Point Point::operator*(int32 scale)
	{
		return Point(x*scale, y*scale);
	}


	// Scalar multiplication
	inline Point& Point::operator*=(int32 scale)
	{
		x *= scale;
		y *= scale;
		return *this;
	}

	// Scalar multiplication
	inline Point Point::operator*(float scale)
	{
		return Point((int32)(x*scale), (int32)(y*scale));
	}


	// Scalar multiplication
	inline Point& Point::operator*=(float scale)
	{
		x = (int32)(x*scale);
		y = (int32)(y*scale);
		return *this;
	}


	// Scalar division
	inline Point Point::operator/(int32 scale)
	{
		float r = 1.0f / scale;
		return Point( int32(x*r), int32(y*r) );
	}


	// Scalar division
	inline Point& Point::operator/=(int32 scale)
	{
		float r = 1.0f / scale;
		x = int32(x*r);
		y = int32(y*r);
		return *this;
	}

	// Scalar division
	inline Point Point::operator/(float scale)
	{
		return Point((int32)(x/scale), (int32)(y/scale));
	}


	// Scalar division
	inline Point& Point::operator/=(float scale)
	{
		float r = 1.0f / scale;
		x = (int32)(x*r);
		y = (int32)(y*r);
		return *this;
	}


	// Comparison
	inline bool Point::operator==(const Point& p)
	{
		return x==p.x && x==p.y;
	}


	// Comparison
	inline bool Point::operator!=(const Point& p)
	{
		return x!=p.x || x!=p.y;
	}

	// Max
	inline Point Point::Max( const Point& a, const Point& b )
	{
		return Point( std::max( a.x, b.x ), std::max( a.y, b.y ) );
	}

	// Min
	inline Point Point::Min( const Point& a, const Point& b )
	{
		return Point( std::min( a.x, b.x ), std::min( a.y, b.y ) );
	}
}

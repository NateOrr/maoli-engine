//--------------------------------------------------------------------------------------
// File: ColorSSE.h
//
// RGBA Color using SSE
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

// SSE only supported on windows
#ifdef MAOLI_WINDOWS

namespace Maoli
{
	namespace SSE
	{
		__declspec(align(16)) class Color
		{
		public:
			// Ctor
			Color();

			// Ctor
			Color(float r, float g, float b, float a = 1.0f);

			// Ctor
			Color(__m128 vec);

			// Move Ctor
			Color(Color&& v);

			// Move assignment operator
			Color& operator=(Color&&);

			// Fill the data
			void Set(float x, float y, float z, float w = 1.0f);

			// Auto cast to float pointer
			operator float*();

			// Bracket access
			float& operator[](int32 i);

			// Constant bracket access
			float operator[](int32 i) const;

			// Subtraction
			Color  operator- (const Color& v);
			Color& operator-=(const Color& v);

			// Addition
			Color  operator+ (const Color& v);
			Color& operator+=(const Color& v);

			// Scalar multiplication
			Color  operator* (float f);
			Color& operator*=(float f);

			// Scalar division
			Color  operator/ (float f);
			Color& operator/=(float f);

			// Multiplication
			Color operator*(const Color& v);
			Color& operator*=(const Color& v);

			// Clamp to 0-1 range
			void Saturate();

			union
			{
				// Elements
				struct
				{	
					float r, g, b, a;
				};

				// Array
				float _f[4];

				// SSE
				__m128 _vec;
			};
		};
	}

}

#include "ColorSSE.inl"

#else

namespace Maoli
{
	namespace SSE
	{
		typedef ::Maoli::Color Color;
	}
}

#endif
//--------------------------------------------------------------------------------------
// File: Vector2.h
//
// 2D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// 2D Vector
	class Vector2
	{
	public:

		// Ctor
		Vector2();

		// Ctor
		Vector2(float x, float y);

		// Move Ctor
		Vector2(Vector2&& v);

		// Move assignment operator
		Vector2& operator=(Vector2&& v);

		// Negation
		Vector2 operator-() const;


		// Fill the data
		void Set(float x, float y);

		// Auto cast to float pointer
		operator float*();

		// Bracket access
		float& operator[](int32 i);

		// Constant bracket access
		float operator[](int32 i) const;


		// Subtraction
		Vector2  operator- (const Vector2& v) const;
		Vector2& operator-=(const Vector2& v);

		// Addition
		Vector2  operator+ (const Vector2& v) const;
		Vector2& operator+=(const Vector2& v);

		// Scalar multiplication
		Vector2  operator* (float f) const;
		Vector2& operator*=(float f);

		// Scalar division
		Vector2  operator/ (float f) const;
		Vector2& operator/=(float f);

		// Dot product
		float Dot(const Vector2& v) const;
		float operator*(const Vector2& v) const;

		// Comparison
		bool operator==(const Vector2& v) const;
		bool operator!=(const Vector2& v) const;

		// Length
		float Length() const;

		// Square of the length
		float LengthSquared() const;

		// Normalize the vector
		Vector2& Normalize();

		// Max vector
		static Vector2 Max(const Vector2& a, const Vector2& b);

		// Min vector
		static Vector2 Min(const Vector2& a, const Vector2& b);

		union
		{
			// Elements
			struct
			{
				float x,y;
			};

			// Array
			float _f[2];
		};
	};

}

#include "Vector2.inl"

//--------------------------------------------------------------------------------------
// File: Frustum.cpp
//
// View Frustum
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Frustum.h"

namespace Maoli
{
	// Builds the frustum from the view matrix
	void Frustum::Build( const Matrix& matView, const Matrix& matProj )
	{
		Matrix viewProj = matView * matProj;

		// Calculate the planes
		_planes[0].a = viewProj._14 + viewProj._13; // Near
		_planes[0].b = viewProj._24 + viewProj._23;
		_planes[0].c = viewProj._34 + viewProj._33;
		_planes[0].d = viewProj._44 + viewProj._43;
		_planes[0].Normalize();

		_planes[1].a = viewProj._14 - viewProj._13; // Far
		_planes[1].b = viewProj._24 - viewProj._23;
		_planes[1].c = viewProj._34 - viewProj._33;
		_planes[1].d = viewProj._44 - viewProj._43;
		_planes[1].Normalize();

		_planes[2].a = viewProj._14 + viewProj._11; // Left
		_planes[2].b = viewProj._24 + viewProj._21;
		_planes[2].c = viewProj._34 + viewProj._31;
		_planes[2].d = viewProj._44 + viewProj._41;
		_planes[2].Normalize();

		_planes[3].a = viewProj._14 - viewProj._11; // Right
		_planes[3].b = viewProj._24 - viewProj._21;
		_planes[3].c = viewProj._34 - viewProj._31;
		_planes[3].d = viewProj._44 - viewProj._41;
		_planes[3].Normalize();

		_planes[4].a = viewProj._14 - viewProj._12; // Top
		_planes[4].b = viewProj._24 - viewProj._22;
		_planes[4].c = viewProj._34 - viewProj._32;
		_planes[4].d = viewProj._44 - viewProj._42;
		_planes[4].Normalize();

		_planes[5].a = viewProj._14 + viewProj._12; // Bottom
		_planes[5].b = viewProj._24 + viewProj._22;
		_planes[5].c = viewProj._34 + viewProj._32;
		_planes[5].d = viewProj._44 + viewProj._42;
		_planes[5].Normalize();
	}

	// Build from a set of points.  The order must start with top left and go clockwise
	void Frustum::Build( const Vector3& camPos, const Vector3 corners[4], const float minZ, const float maxZ )
	{
		// Get the corners
		Vector3 nearCorners[4], farCorners[4];
		nearCorners[0] = camPos + corners[0] * minZ;
		nearCorners[1] = camPos + corners[1] * minZ;
		nearCorners[2] = camPos + corners[2] * minZ;
		nearCorners[3] = camPos + corners[3] * minZ;
		farCorners[0] = camPos + corners[0] * maxZ;
		farCorners[1] = camPos + corners[1] * maxZ;
		farCorners[2] = camPos + corners[2] * maxZ;
		farCorners[3] = camPos + corners[3] * maxZ;

		// Near
		_planes[0].FromPoints( nearCorners[2], nearCorners[1], nearCorners[0] );

		// Far
		_planes[1].FromPoints( farCorners[0], farCorners[1], farCorners[2] );

		// Left
		_planes[2].FromPoints( nearCorners[3], nearCorners[0], farCorners[0] );

		// Right
		_planes[3].FromPoints( farCorners[2], farCorners[1], nearCorners[1] );

		// Top
		_planes[4].FromPoints( nearCorners[1], farCorners[1], farCorners[0] );

		// Bottom
		_planes[5].FromPoints( nearCorners[3], farCorners[3], farCorners[2] );
	}

	// Build from start and end corner points, starts with top left and goes clockwise
	void Frustum::Build( const Vector3 nearCorners[4], const Vector3 farCorners[4] )
	{
		// Near
		_planes[0].FromPoints( nearCorners[2], nearCorners[1], nearCorners[0] );

		// Far
		_planes[1].FromPoints( farCorners[0], farCorners[1], farCorners[2] );

		// Left
		_planes[2].FromPoints( nearCorners[3], nearCorners[0], farCorners[0] );

		// Right
		_planes[3].FromPoints( farCorners[2], farCorners[1], nearCorners[1] );

		// Top
		_planes[4].FromPoints( nearCorners[1], farCorners[1], farCorners[0] );

		// Bottom
		_planes[5].FromPoints( nearCorners[3], farCorners[3], farCorners[2] );
	}

	// Checks if a point is in the frustum
	bool Frustum::CheckPoint( const Vector3& vec ) const
	{
		// Make sure point is in frustum
		for ( uint16 i = 0; i < 6; i++ )
		{
			if ( _planes[i].DotCoord( vec ) < 0.0f )
				return false;
		}

		return true;
	}

	// Checks if a cube is in the frustum
	bool Frustum::CheckCube( float XCenter, float YCenter, float ZCenter, float Size ) const
	{
		float minus[3], plus[3];
		minus[0] = XCenter - Size;
		minus[1] = YCenter - Size;
		minus[2] = ZCenter - Size;
		plus[0] = XCenter + Size;
		plus[1] = YCenter + Size;
		plus[2] = ZCenter + Size;

		Vector3 vec[8] =
		{
			Vector3( minus[0], minus[1], minus[2] ),
			Vector3( plus[0], minus[1], minus[2] ),
			Vector3( minus[0], plus[1], minus[2] ),
			Vector3( plus[0], plus[1], minus[2] ),
			Vector3( minus[0], minus[1], plus[2] ),
			Vector3( plus[0], minus[1], plus[2] ),
			Vector3( minus[0], plus[1], plus[2] ),
			Vector3( plus[0], plus[1], plus[2] ),
		};


		// Make sure at least one point is in frustum
		for ( uint16 i = 0; i < 6; i++ )
		{
			if ( _planes[i].DotCoord( vec[0] ) >= 0.0f )
				continue;
			if ( _planes[i].DotCoord( vec[1] ) >= 0.0f )
				continue;
			if ( _planes[i].DotCoord( vec[2] ) >= 0.0f )
				continue;
			if ( _planes[i].DotCoord( vec[3] ) >= 0.0f )
				continue;
			if ( _planes[i].DotCoord( vec[4] ) >= 0.0f )
				continue;
			if ( _planes[i].DotCoord( vec[5] ) >= 0.0f )
				continue;
			if ( _planes[i].DotCoord( vec[6] ) >= 0.0f )
				continue;
			if ( _planes[i].DotCoord( vec[7] ) >= 0.0f )
				continue;

			return false;
		}

		return true;
	}

	// Checks if a rectangle is in the frustum
	bool Frustum::CheckAABB( const Vector3& vPos, const Vector3& vBounds ) const
	{
		Vector3 p;
		Vector3 vMax = (Vector3)(vPos + vBounds);
		Vector3 vMin = (Vector3)(vPos - vBounds);
		for ( uint16 i = 0; i < 6; i++ )
		{
			p = vMin;
			if ( _planes[i].a >= 0 )
				p.x = vMax.x;
			if ( _planes[i].b >= 0 )
				p.y = vMax.y;
			if ( _planes[i].c >= 0 )
				p.z = vMax.z;
			if ( _planes[i].DotCoord( p ) < 0 )
				return false;
		}
		return true;
	}

	// Checks if a rectangle is in the frustum
	bool Frustum::CheckAABB( const Vector3& position, float size ) const
	{
		return CheckAABB(position, Vector3(size, size, size));
	}

	// TRUE if the rect intersects the frustum
	bool Frustum::CheckAABB( const BoundingVolume& bounds ) const
	{
		return CheckAABB( bounds.GetWorldPosition(), bounds.GetSize() );

	}

	// Checks is a sphere is in the frustum
	bool Frustum::CheckSphere( const Vector3& Pos, float Radius ) const
	{
		// Make sure radius is in frustum
		if ( _planes[0].DotCoord( Pos ) < -Radius ||
			_planes[1].DotCoord( Pos ) < -Radius ||
			_planes[2].DotCoord( Pos ) < -Radius ||
			_planes[3].DotCoord( Pos ) < -Radius ||
			_planes[4].DotCoord( Pos ) < -Radius ||
			_planes[5].DotCoord( Pos ) < -Radius )
			return false;

		return true;
	}

	// TRUE if the sphere intersects the frustum
	bool Frustum::CheckSphere( const BoundingVolume& bounds ) const
	{
		return CheckSphere( bounds.GetWorldPosition(), bounds.GetRadius() );
	}
}

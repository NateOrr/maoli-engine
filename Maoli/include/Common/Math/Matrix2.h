//--------------------------------------------------------------------------------------
// File: Matrix2.h
//
// 2x2 Matrix
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class Matrix2
	{
	public:

		// Ctor
		Matrix2();

		// Ctor
		Matrix2( float m11, float m12, 
				 float m21, float m22 );

		// Move Ctor
		Matrix2( Matrix2&& v );

		// Copy Ctor
		Matrix2( const Matrix2& v );

		// Assignment op
		Matrix2& operator=(const Matrix2& v);

		// Make an identity matrix
		Matrix2& Identity();

		// Make the zero matrix
		Matrix2& Zero();

		// Fill the matrix
		Matrix2& Set( float m11, float m12,
			float m21, float m22 );


		// Scalar multiplication
		Matrix2 operator*(float f) const;
		Matrix2& operator*=(float f);


		// Multiply
		Matrix2& Mult( const Matrix2& m );
		Matrix2 operator*(const Matrix2& m) const;
		Matrix2& operator*=(const Matrix2& m);

		// Transpose the matrix
		Matrix2& Transpose();

		// Get the transpose of this matrix
		Matrix2 GetTranspose() const;

		// Transform a vector
		Vector2 Transform( const Vector2& v ) const;

		// Auto cast to float pointer
		operator float*();

		// Auto cast to float pointer
		operator const float*() const;

		float& operator()( int32 x, int32 y );

		// Const element access
		float operator()( int32 x, int32 y ) const;

	private:

		// Data
		union
		{
			// Array access
			float _mat[2][2];

			// Axis array
			struct
			{
				Vector2 _axes[2];
			};

			// Numerical access
			struct
			{
				float _11, _12;
				float _21, _22;
			};
		};

	};
}

#include "Matrix2.inl"
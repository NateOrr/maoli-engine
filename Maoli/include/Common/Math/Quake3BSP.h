//--------------------------------------------------------------------------------------
// File: Quake3BSP.h
//
// File layout for quake 3 bsp's
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	namespace Quake3
	{

		// Lump directory info
		enum class LumpType
		{
			Entities,		// Game-related object descriptions
			Textures,		// Surface descriptions
			Planes,			// Planes used by map geometry
			Nodes,			// BSP tree nodes
			Leafs,			// BSP tree leaves
			Leaffaces,		// Lists of face indices, one list per leaf
			Leafbrushes,	// Lists of brush indices, one list per leaf
			Models,			// Descriptions of rigid world geometry in map
			Brushes,		// Convex polyhedra used to describe solid space
			Brushsides,		// Brush surfaces
			Vertexes,		// Vertices used to describe faces
			Meshverts,		// Lists of offsets, one list per mesh
			Effects,		// List of special map effects
			Faces,			// Surface geometry
			Lightmaps,		// Packed lightmap data
			Lightvols,		// Local illumination data
			Visdata,		// Cluster-cluster visibility data
		};


		// Each DirEntry locates a single lump in the BSP file
		struct DirEntry
		{
			int32 offset;		// Offset to start of lump, relative to beginning of file
			int32 length;		// Length of lump. Always a multiple of 4
		};


		// File header
		struct Header
		{
			char magic[4];				// Magic number. Always "IBSP"
			int32 version;				// Version number. 0x2e for the BSP files distributed with Quake 3
			DirEntry direntries[17];	// Lump directory, seventeen entries
		};	


		// Texture file info
		struct Texture
		{
			char name[64];	// Texture name
			int32 flags;		// Surface flags
			int32 contents;	// Content flags
		};


		// Vertex
		struct Vertex
		{
			float position[3];		// Vertex position
			float texcoord[2][2];	// Vertex texture coordinates. 0=surface, 1=lightmap
			float normal[3];		// Vertex normal
			byte color[4];			// Vertex color RGBA

			// Addition
			Vertex operator+(const Vertex& v);

			// Scalar mult
			Vertex operator*(float s);
		};


		// Face
		struct Face
		{
			int32 texture;			// Texture index
			int32 effect;				// Index into lump 12 (Effects), or -1
			int32 type;				// Face type 1=polygon, 2=patch, 3=mesh, 4=billboard
			int32 vertex;				// Index of first vertex
			int32 n_vertexes;			// Number of vertices
			int32 meshvert;			// Index of first meshvert
			int32 n_meshverts;		// Number of meshverts
			int32 lm_index;			// Lightmap index
			int32 lm_start[2];		// Corner of this face's lightmap image in lightmap
			int32 lm_size[2];			// Size of this face's lightmap image in lightmap
			float lm_origin[3];		// World space origin of lightmap
			float lm_vecs[2][3];	// World space lightmap s and t unit vectors
			float normal[3];		// Surface normal
			int32 size[2];			// Patch dimensions
		};

		// Every patch lump is split into groups of biquadratic patches
		class Patch
		{
		public:
			// Ctor
			Patch();

			// Dtor
			~Patch();

			// Tesselate the patch, generating the verts
			void Tesselate(int32 newTesselation);

			Vertex controlPoints[9];
			int32 tesselation;
			Vertex* vertices;
			int32* indices;
			int32 numVerts;
			int32 numIndices;

		};


		// Curved surface
		class PatchGroup
		{
		public:
			int32 textureIndex;
			int32 lightmapIndex;
			int32 width, height;

			int32 numQuadraticPatches;
			Patch* quadraticPatches;
		};

	}
}

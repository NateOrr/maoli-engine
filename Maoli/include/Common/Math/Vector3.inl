//--------------------------------------------------------------------------------------
// File: Vector3.cpp
//
// 4D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	inline Vector3::Vector3() : x( 0 ), y( 0 ), z( 0 )
	{

	}

	// Ctor
	inline Vector3::Vector3( float vx, float vy, float vz ) : x( vx ), y( vy ), z( vz )
	{

	}

	// Copy Ctor
	inline Vector3::Vector3( const Vector3& v ) : x( v.x ), y( v.y ), z( v.z )
	{

	}

	// Move Ctor
	inline Vector3::Vector3( Vector3&& v ) : x( v.x ), y( v.y ), z( v.z )
	{

	}

	// Move assignment operator
	inline Vector3& Vector3::operator=(const Vector3& v)
	{
		x = v.x; y = v.y; z = v.z;
		return *this;
	}

	// Move assignment operator
	inline Vector3& Vector3::operator=(Vector3&& v)
	{
		x = v.x; y = v.y; z = v.z;
		return *this;
	}


	// Fill the data
	inline void Vector3::Set( float x, float y, float z )
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	// Auto cast to float pointer
	inline Vector3::operator float*()
	{
		return (float*)this;
	}

	// Auto cast to float pointer
	inline Vector3::operator const float*() const
	{
		return (float*)this;
	}

	// Bracket access
	inline float& Vector3::operator[]( int32 i )
	{
		return _f[i];
	}

	// Constant bracket access
	inline float Vector3::operator[]( int32 i ) const
	{
		return _f[i];
	}

	// Negation
	inline Maoli::Vector3 Vector3::operator-() const
	{
		return Vector3( -x, -y, -z );
	}

	// Subtraction
	inline Vector3 Vector3::operator-(const Vector3& v) const
	{
		return Vector3( x - v.x, y - v.y, z - v.z );
	}

	// Subtraction
	inline Vector3& Vector3::operator-=(const Vector3& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	// Addition
	inline Vector3 Vector3::operator+(const Vector3& v) const
	{
		return Vector3( x + v.x, y + v.y, z + v.z );
	}


	// Addition
	inline Vector3& Vector3::operator+=(const Vector3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}


	// Scalar multiplication
	inline Vector3 Vector3::operator*(float f) const
	{
		return Vector3( x*f, y*f, z*f );
	}


	// Scalar multiplication
	inline Vector3& Vector3::operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		return *this;
	}


	// Scalar division
	inline Vector3 Vector3::operator/(float f) const
	{
		float r = 1.0f / f;
		return Vector3( x*r, y*r, z*r );
	}


	// Scalar division
	inline Vector3& Vector3::operator/=(float f)
	{
		float r = 1.0f / f;
		x *= r;
		y *= r;
		z *= r;
		return *this;
	}



	// Dot product
	inline float Vector3::Dot( const Vector3& v ) const
	{
		return x*v.x + y*v.y + z*v.z;
	}


	// Cross product of the 3d components
	inline Vector3 Vector3::Cross( const Vector3& v ) const
	{
		return Vector3( y*v.z - v.y*z,
			v.x*z - x*v.z,
			x*v.y - v.x*y );
	}


	// Length
	inline float Vector3::Length() const
	{
		return sqrtf( x*x + y*y + z*z );
	}

	// Square of the length
	inline float Vector3::LengthSquared() const
	{
		return x*x + y*y + z*z;
	}


	// Normalize the vector
	inline Vector3& Vector3::Normalize()
	{
		float l = 1.0f / Length();
		x *= l;
		y *= l;
		z *= l;
		return *this;
	}

	// Scalar mult
	inline Vector3 operator*(float s, const Vector3& v)
	{
		return Vector3( v.x*s, v.y*s, v.z*s );
	}

	// Max vector
	inline Vector3 Vector3::Max( const Vector3& a, const Vector3& b )
	{
		return Vector3( std::max( a.x, b.x ), std::max( a.y, b.y ), std::max( a.z, b.z ) );
	}

	// Min vector
	inline Vector3 Vector3::Min( const Vector3& a, const Vector3& b )
	{
		return Vector3( std::min( a.x, b.x ), std::min( a.y, b.y ), std::min( a.z, b.z ) );
	}

	// Piecewise multiplication
	inline Vector3 Vector3::ComponentMult( const Vector3& v ) const
	{
		return Vector3( x*v.x, y*v.y, z*v.z );
	}

	// Piecewise divition
	inline Vector3 Vector3::ComponentDiv( const Vector3& v ) const
	{
		return Vector3( x / v.x, y / v.y, z / v.z );
	}

	// Get the average of the components
	inline float Vector3::Average() const
	{
		return (x + y + z) * 0.333333f;
	}

	// Linear interpolation
	inline Vector3 Vector3::Lerp( const Vector3& a, const Vector3& b, float s )
	{
		return Vector3(
			Math::Lerp( a.x, b.x, s ),
			Math::Lerp( a.y, b.y, s ),
			Math::Lerp( a.z, b.z, s )
			);
	}

	// Comparison
	inline bool Vector3::operator==(const Vector3& v) const
	{
		return (x == v.x && y == v.y && z == v.z);
	}


	// Comparison
	inline bool Vector3::operator!=(const Vector3& v) const
	{
		return (x != v.x || y != v.y || z != v.z);
	}

	// Print a vector to console
	inline void Vector3::Print() const
	{
		std::cout << "(" << x << ", " << y << ", " << z << ")\n";
	}

	// Make all values position
	inline Vector3& Vector3::Absolute()
	{
		x = fabs( x );
		y = fabs( y );
		z = fabs( z );
		return *this;
	}

}

//--------------------------------------------------------------------------------------
// File: Camera.inl
//
// First person camera
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{
	// Ctor
	inline Camera::Camera( Engine* engine ) : Transform( engine )
	{
		Reset();
	}

	// Get the view matrix
	inline const Matrix& Camera::GetViewMatrix() const
	{
		return _viewMatrix;
	}

	// Get the view frustum
	inline Frustum& Camera::GetFrustum()
	{
		return _frustum;
	}

	// Get the projection matrix
	inline const Matrix& Camera::GetProjectionMatrix() const
	{
		return _projMatrix;
	}

	// Get the near Z of the clipping plane
	inline float Camera::GetNearZ() const
	{
		return _nearZ;
	}

	// Get the far Z of the clipping plane
	inline float Camera::GetFarZ() const
	{
		return _farZ;
	}

	// Get a frustum corner
	inline const Vector3& Camera::GetFrustumCorner( FrustrumCorner corner ) const
	{
		return _frustumCorners[(int32)corner];
	}

	// Get a frustum corner
	inline const Vector3* Camera::GetFrustumCorners() const
	{
		return _frustumCorners;
	}

	// Get the width of the current view buffer
	inline const uint32 Camera::GetViewBufferWidth() const
	{
		return _viewWidth;
	}

	// Get the height of the current view buffer
	inline const uint32 Camera::GetViewBufferHeight() const
	{
		return _viewHeight;
	}

	// Get the focus zoom amount
	inline const float Camera::GetFocusZoom() const
	{
		return _focusZoom;
	}


	// Set the focus zoom amount
	inline void Camera::SetFocusZoom( float f )
	{
		_focusZoom = f;
	}


	// Get the focus point
	inline void Camera::SetFocusPoint( const Vector3& pos )
	{
		_focusPoint = pos;
	}


	// Get the camera type
	inline const CameraType Camera::GetType() const
	{
		return _type;
	}


	// Set the camera type
	inline void Camera::SetType( CameraType type )
	{
		_type = type;
	}

	// Clamp the view angles to a reasonable range
	inline void Camera::ClampViewAngles()
	{
		if ( _theta.y < 0 ) _theta.y += Math::twopi;
		if ( _theta.y >= Math::twopi ) _theta.y -= Math::twopi;
		_theta.x = std::max( -Math::pi / 3.5f, std::min( _theta.x, Math::pi / 3.5f ) );
	}

	// Get the field of view
	inline float Camera::GetFoV() const
	{
		return _fov;
	}
}

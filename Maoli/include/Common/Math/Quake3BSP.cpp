//--------------------------------------------------------------------------------------
// File: Quake3BSP.cpp
//
// File layout for quake 3 bsp's
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Quake3BSP.h"

namespace Maoli
{
	namespace Quake3
	{


		// Ctor
		Patch::Patch() : vertices(NULL)
		{

		}

		// Dtor
		Patch::~Patch()
		{
			if(vertices)
				delete [] vertices; 
			vertices=NULL;

			if(indices)
				delete [] indices;
			indices=NULL;
		}

		// Tesselate the patch, generating the verts
		void Patch::Tesselate( int32 newTesselation )
		{
			tesselation=newTesselation;

			float px, py;
			Vertex temp[3];
			numVerts = (tesselation+1)*(tesselation+1);
			vertices = new Vertex[numVerts];

			for(int32 v=0; v<=tesselation; ++v)
			{
				px=(float)v/tesselation;

				vertices[v]=controlPoints[0]*((1.0f-px)*(1.0f-px))+
					controlPoints[3]*((1.0f-px)*px*2)+
					controlPoints[6]*(px*px);
			}

			for(int32 u=1; u<=tesselation; ++u)
			{
				py=(float)u/tesselation;

				temp[0]=controlPoints[0]*((1.0f-py)*(1.0f-py))+
					controlPoints[1]*((1.0f-py)*py*2)+
					controlPoints[2]*(py*py);

				temp[1]=controlPoints[3]*((1.0f-py)*(1.0f-py))+
					controlPoints[4]*((1.0f-py)*py*2)+
					controlPoints[5]*(py*py);

				temp[2]=controlPoints[6]*((1.0f-py)*(1.0f-py))+
					controlPoints[7]*((1.0f-py)*py*2)+
					controlPoints[8]*(py*py);

				for(int32 v=0; v<=tesselation; ++v)
				{
					px=(float)v/tesselation;

					vertices[u*(tesselation+1)+v] =	temp[0]*((1.0f-px)*(1.0f-px))+
						temp[1]*((1.0f-px)*px*2)+
						temp[2]*(px*px);
				}
			}

			//Create indices
			numIndices = tesselation*(tesselation+1)*2;
			indices = new int32[numIndices];
			for(int32 row=0; row<tesselation; ++row)
			{
				for(int32 point=0; point<=tesselation; ++point)
				{
					//calculate indices
					//reverse them to reverse winding
					indices[(row*(tesselation+1)+point)*2+1] = row*(tesselation+1)+point;
					indices[(row*(tesselation+1)+point)*2] = (row+1)*(tesselation+1)+point;
				}
			}
		}


		// Addition
		Vertex Vertex::operator+( const Vertex& v )
		{
			Vertex result;
			result.position[0] = position[0] + v.position[0];
			result.position[1] = position[1] + v.position[1];
			result.position[2] = position[2] + v.position[2];

			result.normal[0] = normal[0] + v.normal[0];
			result.normal[1] = normal[1] + v.normal[1];
			result.normal[2] = normal[2] + v.normal[2];

			result.texcoord[0][0] = texcoord[0][0] + v.texcoord[0][0];
			result.texcoord[0][1] = texcoord[0][1] + v.texcoord[0][1];
			result.texcoord[1][0] = texcoord[1][0] + v.texcoord[1][0];
			result.texcoord[1][1] = texcoord[1][1] + v.texcoord[1][1];

			result.color[0] = color[0] + v.color[0];
			result.color[1] = color[1] + v.color[1];
			result.color[2] = color[2] + v.color[2];
			result.color[3] = color[3] + v.color[3];

			return result;
		}

		// Scalar mult
		Vertex Vertex::operator*( float s )
		{
			Vertex result;
			result.position[0] = position[0] * s;
			result.position[1] = position[1] * s;
			result.position[2] = position[2] * s;

			result.normal[0] = normal[0] * s;
			result.normal[1] = normal[1] * s;
			result.normal[2] = normal[2] * s;

			result.texcoord[0][0] = texcoord[0][0] * s;
			result.texcoord[0][1] = texcoord[0][1] * s;
			result.texcoord[1][0] = texcoord[1][0] * s;
			result.texcoord[1][1] = texcoord[1][1] * s;

			result.color[0] = byte( float(color[0]) * s);
			result.color[1] = byte( float(color[1]) * s);
			result.color[2] = byte( float(color[2]) * s);
			result.color[3] = byte( float(color[3]) * s);

			return result;
		}

	}
}

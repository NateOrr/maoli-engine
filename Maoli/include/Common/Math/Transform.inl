//--------------------------------------------------------------------------------------
// File: Transform.cpp
//
// 3d transform component
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{
	// Ctor
	inline Transform::Transform( Engine* engine ) : Component( engine )
	{
		_isDirty = false;
		_linearVelocityDamping = 0.6f;
		_angularVelocityDamping = 0.3f;
		_cameraMode = true;
	}


	// Return true if changes have occured
	inline bool Transform::IsDirty() const
	{
		return _isDirty;
	}

	// Move forward/back
	inline void Transform::MoveForward( float amount )
	{
		_matrix.Position() += _matrix.Forward() * amount;
		_isDirty = true;
	}

	// Move left/right
	inline void Transform::MoveRight( float amount )
	{
		_matrix.Position() += _matrix.Right() * amount;
		_isDirty = true;
	}

	// Move up/down
	inline void Transform::MoveUp( float amount )
	{
		_matrix.Position() += _matrix.Up() * amount;
		_isDirty = true;
	}

	// Get the camera forward vector
	inline const Vector3& Transform::GetForwardVector() const
	{
		return _matrix.Forward();
	}

	// Get the camera right vector
	inline const Vector3& Transform::GetRightVector() const
	{
		return _matrix.Right();
	}

	// Get the camera up vector
	inline const Vector3& Transform::GetUpVector() const
	{
		return _matrix.Up();
	}


	// Set the position
	inline void Transform::SetPosition( float x, float y, float z )
	{
		_matrix.Position() = Vector3(x, y, z);
		_isDirty = true;
	}

	// Set the position
	inline void Transform::SetPosition( const Vector3& pos )
	{
		_matrix.Position() = pos;
		_isDirty = true;
	}

	// Get the position
	inline const Vector3& Transform::GetPosition() const
	{
		return _matrix.Position();
	}


	// Set the view direction
	inline void Transform::SetViewDirection( float x, float y, float z )
	{
		_matrix.Forward() = Vector3(x, y, z);
		_isDirty = true;
	}

	// Set the view direction
	inline void Transform::SetViewDirection( const Vector3& pos )
	{
		_matrix.Forward() = pos;
		_isDirty = true;
	}


	// Get the view direction
	inline const Vector3& Transform::GetViewDirection() const
	{
		return _matrix.Forward();
	}


	// Set the velocity
	inline void Transform::SetVelocity(const Vector3& vel)
	{
		_velocity = vel;
		_isDirty = true;
	}


	// Set the velocity
	inline void Transform::SetVelocity(float x, float y, float z)
	{
		_velocity = Vector3(x,y,z);
		_isDirty = true;
	}


	// Get the velocity
	inline const Vector3& Transform::GetVelocity() const
	{
		return _velocity;
	}


	// Set the current view X angle
	inline void Transform::SetXRotation( const float d )
	{
		_theta.x = d;
		_isDirty = true;
	}

	// Set the current view Y angle
	inline void Transform::SetYRotation( const float d )
	{
		_theta.y = d;
		_isDirty = true;
	}

	// Set the current view Z angle
	inline void Transform::SetZRotation( const float d )
	{
		_theta.z = d;
		_isDirty = true;
	}

	// Get the view x angle
	inline const float Transform::GetXRotation() const
	{
		return _theta.x;
	}

	// Get the view y angle
	inline const float Transform::GetYRotation() const
	{
		return _theta.y;
	}

	// Get the view z angle
	inline const float Transform::GetZRotation() const
	{
		return _theta.z;
	}


	// Set the current view angles
	inline void Transform::SetRotation( const Vector3& r )
	{
		_theta.x = r.x; _theta.y = r.y; _theta.z = r.z;
		_isDirty = true;
	}

	// Set the current view angles
	inline void Transform::SetRotation( float x, float y, float z )
	{
		_theta.x = x; _theta.y = y; _theta.z = z;
		_isDirty = true;
	}

	// Get the view angles
	inline const Vector3& Transform::GetRotation() const
	{
		return _theta;
	}


	// Set the current angular velocity
	inline void Transform::SetAngularVelocity( const Vector3& r )
	{
		_omega.x = r.x; _omega.y = r.y; _omega.z = r.z;
		_isDirty = true;
	}


	// Set the current angular velocity
	inline void Transform::SetAngularVelocity( float x, float y, float z )
	{
		_omega.x = x; _omega.y = y; _omega.z = z;
		_isDirty = true;
	}

	// Set the current angular velocity
	inline const Vector3& Transform::GetAngularVelocity() const
	{
		return _omega;
	}


	// Add to the position
	inline void Transform::AddToPosition(const Vector3& pos)
	{
		_matrix.Position() += pos;
		_isDirty = true;
	}


	// Add to the position
	inline void Transform::AddToPosition(float x, float y, float z)
	{
		_matrix.Position() += Vector3(x,y,z);
		_isDirty = true;
	}

	// Add to the velocity
	inline void Transform::AddToVelocity(const Vector3& vel)
	{
		_velocity += vel;
		_isDirty = true;
	}


	// Add to the velocity
	inline void Transform::AddToVelocity(float x, float y, float z)
	{
		_velocity += Vector3(x,y,z);
		_isDirty = true;
	}

	// Set the current angular velocity
	inline void Transform::AddToRotation( const Vector3& r )
	{
		_theta.x += r.x; _theta.y += r.y; _theta.z += r.z;
		_isDirty = true;
	}


	// Add to the current view angles
	inline void Transform::AddToRotation( float x, float y, float z )
	{
		_theta.x += x; _theta.y += y; _theta.z += z;
		_isDirty = true;
	}

	// Add to the current angular velocity
	inline void Transform::AddToAngularVelocity( const Vector3& r )
	{
		_omega.x += r.x; _omega.y += r.y; _omega.z += r.z;
		_isDirty = true;
	}


	// Add to the current angular velocity
	inline void Transform::AddToAngularVelocity( float x, float y, float z )
	{
		_omega.x += x; _omega.y += y; _omega.z += z;
		_isDirty = true;
	}



	// Get the orientation matrix
	inline const Matrix& Transform::GetOrientationMatrix() const
	{
		return _matrix;
	}

	// Get the orientation matrix
	inline Matrix& Transform::GetOrientationMatrix()
	{
		return _matrix;
	}

	// Set the orientation matrix
	inline void Transform::SetOrientationMatrix( const Matrix& matrix )
	{
		_matrix = matrix;
		_isDirty = true;
	}

	// Get a billboard matrix
	inline Matrix Transform::GetBillboardMatrix() const
	{
		return Matrix::CreateRotationYawPitchRoll(_theta.y, _theta.x, 0);
	}

	// Look at a point
	inline void Transform::LookAt(float x, float y, float z)
	{
		_matrix.LookAt(x, y, z);
		_isDirty = true;
	}

	// Look at a point
	inline void Transform::LookAt(const Vector3& at)
	{
		_matrix.LookAt(at);
		_isDirty = true;
	}

	// Look at a point
	inline void Transform::LookAt(const Vector3& position, const Vector3& at)
	{
		_matrix.LookAt(position, at);
		_isDirty = true;
	}


	// Look at a point
	inline void Transform::LookTo(float x, float y, float z)
	{
		_matrix.LookAt( _matrix.Position().x + x, 
						_matrix.Position().y + y, 
						_matrix.Position().z + z);
		_isDirty = true;
	}

	// Look at a point
	inline void Transform::LookTo(const Vector3& at)
	{
		_matrix.LookAt(_matrix.Position() + at);
		_isDirty = true;
	}

	// Look at a point
	inline void Transform::LookTo(const Vector3& position, const Vector3& at)
	{
		_matrix.LookAt( position, _matrix.Position() + at); 
		_isDirty = true;
	}

	// Set the linear velocity damping
	inline void Transform::SetLinearVelocityDamping( float damping )
	{
		_linearVelocityDamping = damping;
		_isDirty = true;
	}

	// Set the angular velocity damping
	inline void Transform::SetAngularVelocityDamping( float damping )
	{
		_angularVelocityDamping = damping;
		_isDirty = true;
	}

	// Get the linear velocity damping
	inline float Transform::GetLinearVelocityDamping() const
	{
		return _linearVelocityDamping;
	}

	// Get the angular velocity damping
	inline float Transform::GetAngularVelocityDamping() const
	{
		return _angularVelocityDamping;
	}

	// Update the orientation matrix
	inline void Transform::UpdateOrientation()
	{
		//_matrix = Matrix::CreateCameraMatrix(_matrix.GetPosition(), _theta.x, _theta.y);
		Matrix t = Matrix::CreateTranslation( _matrix.GetPosition() );
		Matrix r = Matrix::CreateRotationYawPitchRoll( _theta.y, _theta.x, _theta.z );
		_matrix = r*t;
		_isDirty = false;
	}

}

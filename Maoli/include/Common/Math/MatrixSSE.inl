//--------------------------------------------------------------------------------------
// File: MatrixSSE.inl
//
// 4x4 orthogonal transformation matrix using SSE instrinsics

//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{
	namespace SSE
	{

		// Print to the console
		inline void Matrix::Print()
		{
			using std::cout;
			using std::endl;
			cout << _11 << " " << _12 << " " << _13 << " " << _14 << endl;
			cout << _21 << " " << _22 << " " << _23 << " " << _24 << endl;
			cout << _31 << " " << _32 << " " << _33 << " " << _34 << endl;
			cout << _41 << " " << _42 << " " << _43 << " " << _44 << endl << endl;
		}

		// Ctor
		inline Matrix::Matrix() : _11(1), _12(0), _13(0), _14(0),
			_21(0), _22(1), _23(0), _24(0),
			_31(0), _32(0), _33(1), _34(0),
			_41(0), _42(0), _43(0), _44(1)
		{

		}


		// Ctor
		inline Matrix::Matrix( float m11, float m12 ,float m13, float m14,
			float m21, float m22 ,float m23, float m24,
			float m31, float m32 ,float m33, float m34,
			float m41, float m42 ,float m43, float m44 ) : 
		_11(m11), _12(m12), _13(m13), _14(m14),
			_21(m21), _22(m22), _23(m23), _24(m24),
			_31(m31), _32(m32), _33(m33), _34(m34),
			_41(m41), _42(m42), _43(m43), _44(m44)
		{

		}

		// Copy Ctor
		inline Matrix::Matrix(const Matrix& m) : 
		_11(m._11), _12(m._12), _13(m._13), _14(m._14),
			_21(m._21), _22(m._22), _23(m._23), _24(m._24),
			_31(m._31), _32(m._32), _33(m._33), _34(m._34),
			_41(m._41), _42(m._42), _43(m._43), _44(m._44)
		{

		}

		// Assignment op
		inline Matrix& Matrix::operator=(const Matrix& m)
		{
			for(int32 i=0; i<4; ++i)
				memcpy(&_axes[i], &m._axes[i], 16);
			return *this;
		}


		// Move Ctor
		inline Matrix::Matrix(Matrix&& m) : 
		_11(m._11), _12(m._12), _13(m._13), _14(m._14),
			_21(m._21), _22(m._22), _23(m._23), _24(m._24),
			_31(m._31), _32(m._32), _33(m._33), _34(m._34),
			_41(m._41), _42(m._42), _43(m._43), _44(m._44)
		{

		}

		// Fill the MatrixSSE
		inline Matrix& Matrix::Set( float m11, float m12 ,float m13, float m14,
			float m21, float m22 ,float m23, float m24,
			float m31, float m32 ,float m33, float m34,
			float m41, float m42 ,float m43, float m44 )
		{
			_11=m11;  _12=m12; _13=m13;  _14=m14;
			_21=m21;  _22=m22; _23=m23;  _24=m24;
			_31=m31;  _32=m32; _33=m33;  _34=m34;
			_41=m41;  _42=m42; _43=m43;  _44=m44;
			return *this;
		}

		// Scalar multiplication
		inline Matrix Matrix::operator*(float f) const
		{
			Matrix m;
			__m128 scale = _mm_set_ps1(f);
			m._vecX = _mm_mul_ps(_vecX, scale);
			m._vecY = _mm_mul_ps(_vecY, scale);
			m._vecZ = _mm_mul_ps(_vecZ, scale);
			m._vecW = _mm_mul_ps(_vecW, scale);
			return m;
		}

		// Scalar multiplication
		inline Matrix& Matrix::operator*=(float f)
		{
			__m128 scale = _mm_set_ps1(f);
			_vecX = _mm_mul_ps(_vecX, scale);
			_vecY = _mm_mul_ps(_vecY, scale);
			_vecZ = _mm_mul_ps(_vecZ, scale);
			_vecW = _mm_mul_ps(_vecW, scale);
			return *this;
		}

		// Auto cast to float pointer
		inline Matrix::operator float*()
		{
			return (float*)this;
		}

		// Auto cast to float pointer
		inline Matrix::operator const float*() const
		{
			return (float*)this;
		}


		// Make an identity MatrixSSE
		inline Matrix& Matrix::Identity()
		{
			*this = Matrix(1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1);
			return *this;
		}

		// Make the zero MatrixSSE
		inline Matrix& Matrix::Zero()
		{
			memset(this, 0, sizeof(Matrix));
			return *this;
		}

		// Transform a vector
		inline Vector4 Matrix::Transform( const Vector4& v ) const
		{
			__m128 t;
			t =    _mm_mul_ps(           _vec[0], _mm_set1_ps(v.x));
			t =    _mm_add_ps(_mm_mul_ps(_vec[1], _mm_set1_ps(v.y)), t);
			t =    _mm_add_ps(_mm_mul_ps(_vec[2], _mm_set1_ps(v.z)), t);
			return _mm_add_ps(_mm_mul_ps(_vec[3], _mm_set1_ps(v.w)), t);
		}

		// Transform a 3d vector
		inline Vector3 Matrix::Transform(const Vector3& v) const
		{
			return Vector3( _11*v.x + _21*v.y + _31*v.z + _41,
				_12*v.x + _22*v.y + _32*v.z + _42,
				_13*v.x + _23*v.y + _33*v.z + _43);
		}

		// Transform a 3d direction vector
		inline Vector3 Matrix::TransformNormal(const Vector3& v) const
		{
			return Vector3( _11*v.x + _21*v.y + _31*v.z,
				_12*v.x + _22*v.y + _32*v.z,
				_13*v.x + _23*v.y + _33*v.z );
		}


		// Multiply
		inline Matrix& Matrix::Mult( const Matrix& m )
		{
			__m128 t;
			for(int32 i=0; i<4; ++i)
			{
				t = _mm_mul_ps(           m._vec[0], _mm_set1_ps(_axes[i].x));
				t = _mm_add_ps(_mm_mul_ps(m._vec[1], _mm_set1_ps(_axes[i].y)), t);
				t = _mm_add_ps(_mm_mul_ps(m._vec[2], _mm_set1_ps(_axes[i].z)), t);
				_vec[i]   = _mm_add_ps(_mm_mul_ps(m._vec[3], _mm_set1_ps(_axes[i].w)), t);
			}

			return *this;
		}

		// Multiply
		inline Matrix Matrix::operator*(const Matrix& m) const
		{
			Matrix o;
			__m128 t;
			for(int32 i=0; i<4; ++i)
			{
				t = _mm_mul_ps(           m._vec[0], _mm_set1_ps(_axes[i].x));
				t = _mm_add_ps(_mm_mul_ps(m._vec[1], _mm_set1_ps(_axes[i].y)), t);
				t = _mm_add_ps(_mm_mul_ps(m._vec[2], _mm_set1_ps(_axes[i].z)), t);
				o._vec[i]   = _mm_add_ps(_mm_mul_ps(m._vec[3], _mm_set1_ps(_axes[i].w)), t);
			}
			return o;
		}

		// Multiply
		inline Matrix& Matrix::operator*=(const Matrix& m)
		{
			__m128 t;
			for(int32 i=0; i<4; ++i)
			{
				t = _mm_mul_ps(           m._vec[0], _mm_set1_ps(_axes[i].x));
				t = _mm_add_ps(_mm_mul_ps(m._vec[1], _mm_set1_ps(_axes[i].y)), t);
				t = _mm_add_ps(_mm_mul_ps(m._vec[2], _mm_set1_ps(_axes[i].z)), t);
				_vec[i]   = _mm_add_ps(_mm_mul_ps(m._vec[3], _mm_set1_ps(_axes[i].w)), t);
			}

			return *this;
		}


		// Identity MatrixSSE
		inline Matrix Matrix::CreateIdentity()
		{
			return Matrix( 1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1 );
		}

		// Translation MatrixSSE
		inline Matrix Matrix::CreateTranslation( float x, float y, float z )
		{
			return Matrix(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				x, y, z, 1);
		}

		// Translation MatrixSSE
		inline Matrix Matrix::CreateTranslation( const Vector3& v )
		{
			return Matrix(
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				v.x, v.y, v.z, 1);
		}

		// Make a scaling MatrixSSE
		inline Matrix Matrix::CreateScaling( float x, float y, float z )
		{
			return Matrix(
				x, 0, 0, 0,
				0, y, 0, 0,
				0, 0, z, 0,
				0, 0, 0, 1);
		}

		// Make a scaling MatrixSSE
		inline Matrix Matrix::CreateScaling( const Vector3& v )
		{
			return Matrix(
				v.x, 0, 0, 0,
				0, v.y, 0, 0,
				0, 0, v.z, 0,
				0, 0, 0, 1);
		}

		// Make a rotation MatrixSSE about the x-axis
		inline Matrix Matrix::CreateRotationX(float theta)
		{
			Matrix m;
			m._22 = m._33 = cosf(theta);
			m._23 = sinf(theta);
			m._32 = -m._23;
			return m;
		}

		// Make a rotation MatrixSSE about the y-axis
		inline Matrix Matrix::CreateRotationY(float theta)
		{
			Matrix m;
			m._11 = m._33 = cosf(theta);
			m._13 = sinf(theta);
			m._31 = -m._13;
			return m;
		}

		// Make a rotation MatrixSSE about the z-axis
		inline Matrix Matrix::CreateRotationZ(float theta)
		{
			Matrix m;
			m._11 = m._22 = cosf(theta);
			m._12 = sinf(theta);
			m._21 = -m._12;
			return m;
		}

		// Yaw pitch roll MatrixSSE
		inline Matrix Matrix::CreateRotationYawPitchRoll( float yaw, float pitch, float roll )
		{
			// todo: optimize
			Matrix out;
			out._11 = ( cos(roll) * cos(yaw) ) + ( sin(roll) * sin(pitch) * sin(yaw) );
			out._12 = ( sin(roll) * cos(pitch) );
			out._13 = ( cos(roll) * -sin(yaw) ) + ( sin(roll) * sin(pitch) * cos(yaw) );
			out._21 = (-sin(roll) * cos(yaw) ) + ( cos(roll) * sin(pitch) * sin(yaw) );
			out._22 = ( cos(roll) * cos(pitch) );
			out._23 = ( sin(roll) * sin(yaw) ) + ( cos(roll) * sin(pitch) * cos(yaw) );
			out._31 = ( cos(pitch) * sin(yaw) );
			out._32 = -sin(pitch);
			out._33 = ( cos(pitch) * cos(yaw) );
			out._44 = 1;
			return out;
		}

		// Create an arbitrary rotation
		inline Matrix Matrix::CreateCameraMatrixSSE(const Vector3& position, float thetaX, float thetaY)
		{
			float sinX = sinf(thetaX);
			float sinY = sinf(thetaY);
			float cosX = cosf(thetaX);
			float cosY = cosf(thetaY);

			return Matrix( 	cosY, 0, -sinY, 0,
				sinX * sinY, cosX, sinX * cosY, 0,
				cosX *  sinY, -sinX, cosX * cosY, 0,
				position.x, position.y, position.z, 1);
		}

		// Make an orthographic MatrixSSE
		inline Matrix Matrix::CreateOrthographic( float width, float height, float nearZ, float farZ )
		{
			return Matrix(
				2/width,  0,    0,         0,
				0,    2/height,  0,           0,
				0,    0,    1/(farZ-nearZ),   0,
				0,    0,    nearZ/(nearZ-farZ),  1
				);
		}


		// Perform a local rotation about the x-axis
		inline Matrix& Matrix::RotateLocalX(float theta)
		{
			*this = Matrix::CreateRotationX(theta) * (*this);
			return *this;
		}

		// Perform a local rotation about the y-axis
		inline Matrix& Matrix::RotateLocalY(float theta)
		{
			*this = Matrix::CreateRotationY(theta) * (*this);
			return *this;
		}

		// Perform a local rotation about the z-axis
		inline Matrix& Matrix::RotateLocalZ(float theta)
		{
			*this = Matrix::CreateRotationZ(theta) * (*this);
			return *this;
		}

		// Perform arotation about the x-axis
		inline Matrix& Matrix::RotateX(float theta)
		{
			return Mult(Matrix::CreateRotationX(theta));
		}

		// Perform arotation about the y-axis
		inline Matrix& Matrix::RotateY(float theta)
		{
			return Mult(Matrix::CreateRotationY(theta));
		}

		// Perform arotation about the z-axis
		inline Matrix& Matrix::RotateZ(float theta)
		{
			return Mult(Matrix::CreateRotationZ(theta));
		}


		// Perform a scale
		inline Matrix& Matrix::Scale(float x, float y, float z)
		{
			return Mult(Matrix::CreateScaling(x, y, z));
		}

		// Perform a scale
		inline Matrix& Matrix::Scale(const Vector3& v)
		{
			return Mult(Matrix::CreateScaling(v));
		}

		// Perform a translation
		inline Matrix& Matrix::Translate(float x, float y, float z)
		{
			return Mult(Matrix::CreateTranslation(x, y, z));
		}

		// Perform a translation
		inline Matrix& Matrix::Translate(const Vector3& v)
		{
			return Mult(Matrix::CreateTranslation(v));
		}


		// Transpose the MatrixSSE
		inline Matrix& Matrix::Transpose()
		{
			_MM_TRANSPOSE4_PS(_vecX, _vecY, _vecZ, _vecW);
			return *this;
		}

		// Get the transpose of this MatrixSSE
		inline Matrix Matrix::GetTranspose() const
		{
			Matrix m = *this;
			return m.Transpose();
		}


		// Transpose the orientation part
		inline Matrix& Matrix::TransposeOrientation()
		{
			std::swap(_21, _12);
			std::swap(_31, _13);
			std::swap(_32, _23);
			return *this;
		}

		// Element access
		inline float& Matrix::operator()( int32 x, int32 y )
		{
			return _mat[x][y];
		}

		// Const element access
		inline float Matrix::operator()( int32 x, int32 y ) const
		{
			return _mat[x][y];
		}


		// Orient the MatrixSSE to face the given point
		inline Matrix& Matrix::LookAt( const Vector3& point )
		{
			// Get the new forward vector
			_forward = point - _position;
			_forward.Normalize();

			// Compute the new right, using global up
			Vector3 up(0, 1, 0);
			_right = up.Cross(_forward);
			_right.Normalize();

			// Compute the local up
			_up = _forward.Cross(_right);
			_up.Normalize();

			return *this;
		}

		// Orient the MatrixSSE to face the given point
		inline Matrix& Matrix::LookAt( const Vector3& position, const Vector3& point )
		{
			// Get the new forward vector
			_position = position;
			_forward = point - _position;
			_forward.Normalize();

			// Compute the new right, using global up
			Vector3 up(0, 1, 0);
			_right = up.Cross(_forward);
			_right.Normalize();

			// Compute the local up
			_up = _forward.Cross(_right);
			_up.Normalize();

			return *this;
		}

		// Orient the MatrixSSE to face the given point
		inline Matrix& Matrix::LookAt( float x, float y, float z )
		{
			return LookAt(Vector3(x, y, z));
		}

		// Build a perspective projection MatrixSSE
		inline Matrix& Matrix::Perspective( float fovY, float aspect, float zNear, float zFar )
		{
			float yScale = tanf( 0.5f*(Math::pi - fovY) ); // cot(fovY/2)
			float xScale = yScale / aspect;
			float dz = zFar - zNear;
			float clipRatio1 = zFar / dz;
			float clipRatio2 = -zNear*zFar / dz;
			return Set(
				xScale, 0, 0, 0,
				0, yScale, 0, 0,
				0, 0, clipRatio1, 1.0f,
				0, 0, clipRatio2, 0);
		}

		// Make an orthographic MatrixSSE
		inline Matrix& Matrix::Orthographic( float width, float height, float nearZ, float farZ )
		{
			return Set(
				2/width,  0,    0,         0,
				0,    2/height,  0,           0,
				0,    0,    1/(farZ-nearZ),   0,
				0,    0,    nearZ/(nearZ-farZ),  1
				);
		}

		// Create an arbitrary rotation
		inline Matrix& Matrix::CameraMatrixSSE(const Vector3& position, float thetaX, float thetaY)
		{
			float sinX = sinf(thetaX);
			float sinY = sinf(thetaY);
			float cosX = cosf(thetaX);
			float cosY = cosf(thetaY);

			return Set( 	cosY, 0, -sinY, 0,
				sinX * sinY, cosX, sinX * cosY, 0,
				cosX *  sinY, -sinX, cosX * cosY, 0,
				position.x, position.y, position.z, 1);
		}

		// Inverse
		inline Matrix& Matrix::InverseOrthonormal()
		{
			const Vector3 w = _position;
			_position.x = -(_position.Dot(_right));
			_position.y = -(_position.Dot(_up));
			_position.z = -(_position.Dot(_forward));
			TransposeOrientation();
			return *this;
		}

		// Get the inverse of this MatrixSSE
		inline Matrix Matrix::GetInverse() const
		{
			Matrix m = *this;
			return m.Inverse();
		}

		// Get the position component
		inline const Vector3& Matrix::GetPosition() const
		{
			return _position;
		}

		// Get the determinant of this MatrixSSE
		inline Matrix& Matrix::Inverse()
		{
			// Compute the adjugate MatrixSSE
			float c1 = (_33*_44 - _34*_43);
			float c2 = (_23*_44 - _24*_43);
			float c3 = (_23*_34 - _24*_33);
			float c4 = (_13*_44 - _14*_43);
			float c5 = (_13*_34 - _14*_33);
			float c6 = (_13*_24 - _14*_23);
			float c7 = (_32*_44 - _34*_42);
			float c8 = (_22*_44 - _24*_42);
			float c9 = (_12*_44 - _14*_42);
			float c10 = (_22*_34 - _24*_32);
			float c11 = (_12*_34 - _14*_32);
			float c12 = (_12*_24 - _14*_22);
			float c13 = (_32*_43 - _33*_42);
			float c14 = (_22*_43 - _23*_42);
			float c15 = (_22*_33 - _23*_32);
			float c16 = (_12*_43 - _13*_42);
			float c17 = (_12*_33 - _13*_32);
			float c18 = (_12*_23 - _13*_22);

			Matrix a(
				(_22*c1 - _32*c2 + _42*c3),
				-(_12*c1 - _32*c4 + _42*c5),
				(_12*c2 - _22*c4 + _42*c6),
				-(_12*c3 - _22*c5 + _32*c6),

				-(_21*c1 - _31*c2 + _41*c3),
				(_11*c1 - _31*c4 + _41*c5),
				-(_11*c2 - _21*c4 + _41*c6),
				(_11*c3 - _21*c5 + _31*c6),

				(_21*c7 - _31*c8 + _41*c10),
				-(_11*c7 - _31*c9 + _41*c11),
				(_11*c8 - _21*c9 + _41*c12),
				-(_11*c10 - _21*c11 + _31*c12),

				-(_21*c13 - _31*c14 + _41*c15),
				(_11*c13 - _31*c16 + _41*c17),
				-(_11*c14 - _21*c16 + _41*c18),
				(_11*c15 - _21*c17 + _31*c18)
				);

			// Compute the inverse
			float det = (_11*a._11 + _12*a._21 + _13*a._31 +_14*a._41);
			if(det)
			{
				a *= 1.0f / det;
				memcpy(this, &a, sizeof(Matrix));
			}

			return *this;
		}
	}


}

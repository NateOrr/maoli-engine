//--------------------------------------------------------------------------------------
// File: Vector3.h
//
// 3D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// 3D Vector
	class Vector3
	{
	public:

		// Ctor
		Vector3();

		// Ctor
		Vector3( float x, float y, float z );

		// Copy ctor
		Vector3( const Vector3& v );

		// Move Ctor
		Vector3( Vector3&& v );

		// Assignment operator
		Vector3& operator=(const Vector3& v);

		// Move assignment operator
		Vector3& operator=(Vector3&& v);

		// Fill the data
		void Set( float x, float y, float z );

		// Auto cast to float pointer
		operator float*();
		operator const float*() const;

		// Bracket access
		float& operator[]( int32 i );

		// Constant bracket access
		float operator[]( int32 i ) const;

		// Negation
		Vector3 operator-() const;

		// Subtraction
		Vector3  operator- (const Vector3& v) const;
		Vector3& operator-=(const Vector3& v);

		// Addition
		Vector3  operator+ (const Vector3& v) const;
		Vector3& operator+=(const Vector3& v);

		// Scalar multiplication
		Vector3  operator* (float f) const;
		Vector3& operator*=(float f);

		// Scalar division
		Vector3  operator/ (float f) const;
		Vector3& operator/=(float f);

		// Comparison
		bool operator==(const Vector3& v) const;
		bool operator!=(const Vector3& v) const;

		// Dot product
		float Dot( const Vector3& v ) const;

		// Cross product of the 3d components
		Vector3 Cross( const Vector3& v ) const;

		// Length
		float Length() const;

		// Square of the length
		float LengthSquared() const;

		// Normalize the vector
		Vector3& Normalize();

		// Max vector
		static Vector3 Max( const Vector3& a, const Vector3& b );

		// Min vector
		static Vector3 Min( const Vector3& a, const Vector3& b );

		// Linear interpolation
		static Vector3 Lerp( const Vector3& a, const Vector3& b, float s );

		// Piecewise multiplication
		Vector3 ComponentMult( const Vector3& v ) const;

		// Piecewise division
		Vector3 ComponentDiv( const Vector3& v ) const;

		// Get the average of the components
		float Average() const;

		// Make all values position
		Vector3& Absolute();

		// Print a vector to console
		void Print() const;

		union
		{
			// Elements
			struct
			{
				float x, y, z;
			};

			// Array access
			float _f[3];
		};
	};

	// Scalar mult
	Vector3 operator*(float s, const Vector3& v);

}

#include "Vector3.inl"

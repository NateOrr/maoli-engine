//--------------------------------------------------------------------------------------
// File: Point.h
//
// 2D Point
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// 2D Point
	struct Point
	{
		// Ctor
		Point();
		Point(int32 x, int32 y);
		Point(const Vector2& v);

		// Copy ctor
		Point(const Point& p);

		// Assignment op
		Point& operator=(const Point& p);

		// Move operators
		Point(Point&& p);
		Point& operator=(Point&& p);

		// Addition
		Point operator+(const Point& p);
		Point& operator+=(const Point& p);

		// Subtraction
		Point operator-(const Point& p);
		Point& operator-=(const Point& p);

		// Scalar multiplication
		Point operator*(int32 scale);
		Point& operator*=(int32 scale);
		Point operator*(float scale);
		Point& operator*=(float scale);

		// Scalar division
		Point operator/(int32 scale);
		Point& operator/=(int32 scale);
		Point operator/(float scale);
		Point& operator/=(float scale);

		// Comparison
		bool operator==(const Point& p);
		bool operator!=(const Point& p);

		// Max
		static Point Max( const Point& a, const Point& b );

		// Min
		static Point Min( const Point& a, const Point& b );

		int32 x, y;	// Position
	};

}

#include "Point.inl"

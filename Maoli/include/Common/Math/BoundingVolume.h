//--------------------------------------------------------------------------------------
// File: BoundingVolume.h
//
// 3D bounding volume
// 
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// 3D bounding volume, with world position tracking
	class BoundingVolume
	{
	public:

		// Ctor
		BoundingVolume();

		// Set the bounding box
		void Set(const Vector3& pos, const Vector3& size);

		// Set the bounding sphere (this will make the extents a cube)
		void Set(const Vector3& pos, float r);

		// Set the world position
		void SetWorldPosition(const Vector3& pos);

		// Get local position
		const Vector3& GetLocalPosition() const;

		// Get the world position
		const Vector3& GetWorldPosition() const;

		// Get the size
		const Vector3& GetSize() const;

		// Get the radius
		float GetRadius() const;

	private:
		Vector3		_center;			// Local center
		Vector3		_position;			// Global
		Vector3		_size;				// Max extents of the box
		float		_radius;			// Radius
	};

}

#include "BoundingVolume.inl"

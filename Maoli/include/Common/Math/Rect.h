//--------------------------------------------------------------------------------------
// File: Rect.h
//
// 2D Rectangle
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Rectangle region
	struct Rect
	{
		// Ctor
		Rect();
		Rect(int32 rx, int32 ry, int32 rw, int32 rh);

		// Point addition
		Rect operator+(const Point& p);
		Rect& operator+=(const Point& p);

		// Point subtraction
		Rect operator-(const Point& p);
		Rect& operator-=(const Point& p);

		int32 x, y, width, height;
	};
}

#include "Rect.inl"

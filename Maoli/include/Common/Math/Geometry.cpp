//--------------------------------------------------------------------------------------
// File: Geometry.cpp
//
// 3D Geometry management
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Geometry.h"
#include "Engine\Engine.h"

namespace Maoli
{
	// Static vars
	std::map<String, Geometry*> Geometry::_resources;

	// Ctor
	BoneWeight::BoneWeight()
	{
		memset( bones, 0, 16 );
		memset( weights, 0, 16 );
	}

	// Ctor
	GeometryChunk::GeometryChunk()
	{
		startIndex = indexCount = 0;
		material = nullptr;
	}

	// Ctor
	Geometry::Geometry( Engine* engine ) : DataType( engine )
	{
		_numBones = 0;
	}

	// Dtor
	Geometry::~Geometry()
	{
		Release();
	}

	// Destructor
	void Geometry::Release()
	{
		_positions.Release();
		_texCoords.Release();
		_normals.Release();
		_indices.Release();
		_boneWeights.Release();
		for ( uint32 i = 0; i < _chunks.Size(); ++i )
			Material::Delete( _chunks[i].material );
		_chunks.Release();
	}

	// Centers the mesh about the origin
	void Geometry::CenterMesh()
	{
		// Calculate the center to offset the verts
		Vector3 vMax( -99999.0f, -99999.0f, -99999.0f ), vCenter, vMin( 99999.0f, 99999.0f, 99999.0f ), vTemp;
		for ( uint32 i = 0; i < _positions.Size(); i++ )
		{
			vTemp = _positions[i];
			vMin = Vector3::Min( vMin, vTemp );
			vMax = Vector3::Max( vMax, vTemp );
		}
		vCenter = Vector3( vMax.x - fabs( vMin.x ),
			vMax.y - fabs( vMin.y ),
			vMax.z - fabs( vMin.z ) ) / 2.0f;
		for ( uint32 i = 0; i < _positions.Size(); i++ )
			_positions[i] -= vCenter;
	}

	// Makes sure all coords are positive
	void Geometry::FixTexCoords()
	{
		// Have to go one GeometryChunk at a time
		uint32 badIndex = 0;
		for ( uint32 GeometryChunkID = 0; GeometryChunkID < _chunks.Size(); GeometryChunkID++ )
		{
			// First pass through finds the maximum negative value
			Vector2 baseCoord;
			int32 start = _chunks[GeometryChunkID].startIndex;
			int32 length = _chunks[GeometryChunkID].indexCount;
			for ( int32 i = 0; i < length; i++ )
			{
				int32 index = _indices[start + i];
				Vector2 coord = _texCoords[index];
				baseCoord.x = std::min( baseCoord.x, coord.x );
				baseCoord.y = std::min( baseCoord.y, coord.y );
			}
			baseCoord.x = ceil( -baseCoord.x );
			baseCoord.y = ceil( -baseCoord.y );

			// Second pass performs the transform
			for ( int32 i = 0; i < length; i++ )
			{
				int32 index = _indices[start + i];
				Vector2 coord = _texCoords[index];

				// Transform back to positive range
				if ( coord.x < 0 )
					coord.x = baseCoord.x + coord.x;
				if ( coord.y < 0 )
					coord.y = baseCoord.y + coord.y;
				_texCoords[index] = coord;
			}
		}
	}

	// Swap tex coords
	void Geometry::FlipUV()
	{
		for ( uint32 i = 0; i < _texCoords.Size(); i++ )
		{
			float t = _texCoords[i].x;
			_texCoords[i].x = _texCoords[i].y;
			_texCoords[i].y = t;
		}
	}

	// Invert Y tex coord
	void Geometry::InvertUV()
	{
		for ( uint32 i = 0; i < _texCoords.Size(); i++ )
			_texCoords[i].y = 1.0f - _texCoords[i].y;
	}

	// Change coordinate system (for 3ds type compatibility)
	void Geometry::FlipCoordinateSystem()
	{
		for ( uint32 i = 0; i < _positions.Size(); ++i )
		{
			std::swap( _positions[i].z, _positions[i].y );
			_positions[i].y = -_positions[i].y;
		}
	}


	// Reverse vertex winding
	void Geometry::ReverseFaceWinding()
	{
		for ( uint32 i = 0; i < _indices.Size(); i += 3 )
		{
			Swap( _indices[i], _indices[i + 2] );
		}
		for ( uint32 i = 0; i < _normals.Size(); i++ )
			_normals[i] *= -1.0f;
	}



	// Merges sumbeshes
	void Geometry::Optimize()
	{
		// Setup a tracking array
		Array<bool> wasDeleted;
		Stack<uint32> meshesToDelete;
		wasDeleted.Allocate( _chunks.Size() );
		for ( uint32 i = 0; i < _chunks.Size(); i++ )
			wasDeleted[i] = false;

		// Setup a new index list
		uint32 numIndices = _indices.Size();
		uint32* pNewData = new uint32[numIndices];
		uint32* pOldData = (uint32*)_indices.GetRawData();

		// Make sure there is only one GeometryChunk per material
		uint32 iCount = 0;
		for ( uint32 i = 0; i < _chunks.Size(); i++ )
		{
			if ( wasDeleted[i] )
				continue;

			// Copy these indices over
			GeometryChunk& s1 = _chunks[i];
			memcpy( pNewData + iCount, pOldData + s1.startIndex, s1.indexCount*sizeof(uint32) );
			iCount += s1.indexCount;

			// Check other GeometryChunk against this material, merge if needed
			Material* material = _chunks[i].material;
			for ( uint32 j = 0; j < _chunks.Size(); j++ )
			{
				if ( material == _chunks[j].material && i != j && !wasDeleted[j] )
				{
					// Merge
					GeometryChunk& s2 = _chunks[j];
					wasDeleted[j] = true;
					meshesToDelete.Push( (uint32)j );

					// Move all the indices over
					memcpy( pNewData + iCount, pOldData + s2.startIndex, s2.indexCount*sizeof(uint32) );
					iCount += s2.indexCount;

					// Adjust the size
					s1.indexCount += s2.indexCount;
				}
			}
		}

		// Remove GeometryChunks from the list
		while ( !meshesToDelete.IsEmpty() )
			_chunks.RemoveAt( meshesToDelete.Pop() );
		_chunks.Finalize();

		// Cleanup
		wasDeleted.Release();
		meshesToDelete.Release();
		_indices.TakeArray( pNewData, numIndices );
	}


	// Computes non-averaged normals for the given mesh
	void Geometry::RecomputeHardNormals( int32 index )
	{
		int32 offset = _chunks[index].startIndex;
		Vector3 tri[3], normal;
		for ( int32 i = 0; i < _chunks[index].indexCount; i += 3 )
		{
			tri[2] = _positions[_indices[offset + i]];
			tri[1] = _positions[_indices[offset + i + 1]];
			tri[0] = _positions[_indices[offset + i + 2]];
			Math::GetNormalFromTri( tri, normal );
			_normals[_indices[offset + i]] = normal;
			_normals[_indices[offset + i + 1]] = normal;
			_normals[_indices[offset + i + 2]] = normal;
		}
	}


	// Loads the optimized binary file format
	bool Geometry::Load( const char* file )
	{
		std::ifstream fin( file, std::ofstream::binary );
		if ( fin.is_open() )
		{
			Release();

			// First read the header info
			LoadInfo info;
			uint32 id;
			Deserialize( fin, &id );
			if ( id != _fileID )
			{
				fin.close();
				return false;
			}
			Deserialize( fin, &info );
			_numBones = info.numBones;

			// Read the mesh data
			_positions.Deserialize( fin );
			_texCoords.Deserialize( fin );
			_normals.Deserialize( fin );
			if ( _numBones )
				_boneWeights.Deserialize( fin );
			_indices.Deserialize( fin );

			// Read the GeometryChunk data
			_chunks.Allocate( info.numGeometryChunks );
			String dir = String( file ).GetDirectory();
			String meshName = String( file ).GetFile().RemoveFileExtension();
			for ( uint32 i = 0; i < info.numGeometryChunks; i++ )
			{
				GeometryChunkLoadInfo GeometryChunkInfo;
				Deserialize( fin, &GeometryChunkInfo );
				_chunks[i].indexCount = GeometryChunkInfo.numIndices;
				_chunks[i].startIndex = GeometryChunkInfo.startIndex;
				_chunks[i].name.Deserialize( fin );
				String str;
				str.Deserialize( fin );
				if ( str != String::none )
				{
					String matFile = dir + str + ".mat";
					_chunks[i].material = Material::CreateFromFile( matFile, _parent );
					if ( !_chunks[i].material )
					{
						_parent->GetPlatform()->MessageBox( String( "Unable to load material " ) + matFile );
						_chunks[i].material = Material::Create( "Default", _parent );
					}
				}
				else
					_chunks[i].material = Material::Create( "Default", _parent );
			}
			fin.close();

			// Compute bounding data
			ComputeBounds();

			return true;
		}

		return false;
	}

	// Save mesh to an optimized binary file format
	void Geometry::Save( const char* outFile )
	{
		std::ofstream fout( outFile, std::ofstream::binary );
		if ( fout.is_open() )
		{
			if ( !UpdateFilePath( outFile ) )
				return;

			// First write the header info
			Serialize( fout, &_fileID );
			LoadInfo info = { _positions.Size(), _indices.Size(), _chunks.Size(), _numBones, };
			Serialize( fout, &info );

			// Write the mesh data
			_positions.Serialize( fout );
			_texCoords.Serialize( fout );
			_normals.Serialize( fout );
			if ( _numBones )
				_boneWeights.Serialize( fout );
			_indices.Serialize( fout );

			// Save materials if needed
			String meshName = GetName().GetFile().RemoveFileExtension();
			String meshDir = String( outFile ).GetDirectory();
			for ( uint32 i = 0; i < _chunks.Size(); ++i )
			{
				// Export the material
				if ( _chunks[i].material )
				{
					_chunks[i].material->Save( meshDir + _chunks[i].material->GetName() + ".mat" );
					_chunks[i].material->MoveTextures( meshDir );
				}
			}

			// Write the submesh data
			for ( uint32 i = 0; i < info.numGeometryChunks; i++ )
			{
				GeometryChunkLoadInfo submeshInfo = { _chunks[i].startIndex, _chunks[i].indexCount, };
				Serialize( fout, &submeshInfo );
				_chunks[i].name.Serialize( fout );
				if ( _chunks[i].material )
					_chunks[i].material->GetName().Serialize( fout );
				else
					String::none.Serialize( fout );
			}
			fout.close();
		}
	}


	// Compute normals
	// todo: doesn't work in release mode
	void Geometry::RecomputeNormals()
	{
		Vector3 tri[3], normal;
		_normals.Zero();
		Array<int32> counts( _normals.Size() );
		counts.Zero();
		for ( uint32 i = 0; i < _indices.Size(); i += 3 )
		{
			tri[0] = _positions[_indices[i]];
			tri[1] = _positions[_indices[i + 1]];
			tri[2] = _positions[_indices[i + 2]];
			Math::GetNormalFromTri( tri, normal );
			++counts[_indices[i]];
			++counts[_indices[i + 1]];
			++counts[_indices[i + 2]];
			_normals[_indices[i]] += normal;
			_normals[_indices[i + 1]] += normal;
			_normals[_indices[i + 2]] += normal;
		}
		for ( uint32 i = 0; i < _normals.Size(); ++i )
			_normals[i] /= (float)counts[i];
	}

	// Delete a GeometryChunk
	// todo: this is broken
	void Geometry::DeleteGeometryChunk( int32 index )
	{
		// Remove indices
		for ( int32 i = 0; i < _chunks[index].indexCount; ++i )
			_indices.RemoveAt( _chunks[index].startIndex );
		_indices.Finalize();

		// Update start locations
		for ( uint32 i = index + 1; i < _chunks.Size(); ++i )
			_chunks[i].startIndex -= _chunks[index].indexCount;

		_chunks.RemoveAt( index );
	}

	// Remove duplicate verts
	void Geometry::RemoveDuplicateVerts()
	{
		// Find verts that need removal
		const float epsilon = 0.0001f;
		for ( uint32 i = 0; i < _positions.Size(); ++i )
		{
			for ( uint32 j = 0; j < _positions.Size(); )
			{
				if ( i != j )
				{
					if ( (_positions[i] - _positions[j]).Length() < epsilon )
					{
						// Remove all instances of vertex j
						// and adjust indices to account for removal of one vertex
						for ( uint32 k = 0; k < _indices.Size(); ++k )
						{
							if ( _indices[k] == j )
								_indices[k] = i;
							else if ( _indices[k] > j )
								--_indices[k];
						}
						_positions.RemoveAt( j );
					}
				}
				++j;
			}
		}
		_positions.Finalize();
	}

	// Compute bounding box
	void Geometry::ComputeBounds()
	{
		// Calculate the center to offset the verts
		Vector3 boundMax( -99999.0f, -99999.0f, -99999.0f ), center, boundMin( 99999.0f, 99999.0f, 99999.0f );
		for ( uint32 i = 0; i < GetNumVerts(); ++i )
		{
			boundMin = Vector3::Min( boundMin, GetVerts()[i] );
			boundMax = Vector3::Max( boundMax, GetVerts()[i] );
		}
		center = Vector3( boundMax.x - fabs( boundMin.x ),
			boundMax.y - fabs( boundMin.y ),
			boundMax.z - fabs( boundMin.z ) ) / 2.0f;

		boundMax -= center;
		_bounds = boundMax;
		_center = center;

		// Calculate the bounding spheres for each submesh
		for ( uint32 i = 0; i < _chunks.Size(); i++ )
		{
			boundMax = Vector3( -99999.0f, -99999.0f, -99999.0f );
			boundMin = Vector3( 99999.0f, 99999.0f, 99999.0f );
			for ( int32 e = _chunks[i].startIndex; e < _chunks[i].indexCount + _chunks[i].startIndex; e++ )
			{
				boundMin = Vector3::Min( boundMin, GetVerts()[GetIndices()[e]] );
				boundMax = Vector3::Max( boundMax, GetVerts()[GetIndices()[e]] );
			}
			center = Vector3( boundMax.x - fabs( boundMin.x ),
				boundMax.y - fabs( boundMin.y ),
				boundMax.z - fabs( boundMin.z ) ) / 2.0f;

			// can optimize this away
			boundMax = Vector3( -99999.0f, -99999.0f, -99999.0f );
			for ( int32 e = _chunks[i].startIndex; e < _chunks[i].indexCount + _chunks[i].startIndex; e++ )
			{
				boundMax = Vector3::Max( boundMax, Math::Absolute( GetVerts()[GetIndices()[e]] - center ) );
			}
			_chunks[i].bounds = boundMax;
			_chunks[i].localPosition = center;
		}
	}

}

//--------------------------------------------------------------------------------------
// File: Quadtree.h
//
// Loose quadtree
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Util/List.h"
#include "Util/Array.h"
#include "Vector3.h"
namespace Maoli
{
	// Forward decl
	class BoundingVolume;
	class Frustum;
	class Renderer;
	class Quadtree;
	class QuadtreeElement;

	// Loose quadtree
	class Quadtree
	{
		friend class QuadtreeElement;
		friend class DebugVisualizer;
	public:

		// Ctor
		Quadtree( const Vector3& worldCenter, const Vector3& worldBounds, int32 depth );

		// Dtor
		~Quadtree();

		// Insert an object into the tree, or update the object if it's already in the tree
		void Insert( QuadtreeElement* obj );

		// Remove an object from the tree
		void Remove( QuadtreeElement* obj );

		// Render the quadtree for visualization
		template <typename T>
		void RenderNodeBounds( void(T::*callback)(const Vector3&, const Vector3&, const Color&), T* caller )
		{
			RenderNode( _root, callback, caller );
		}

		// Set the callback for visibile objects
		template <typename T>
		void GetVisibleObjects( const Frustum& viewFrustum, void(T::*callback)(QuadtreeElement*), T* caller ) const
		{
			GetVisibleObjects( _root, viewFrustum, callback, caller );
		}

	private:

		// Internal node for quadtree
		struct QuadtreeNode
		{
			// Ctor
			QuadtreeNode()
			{
				children[0] = children[1] = children[2] = children[3] = nullptr;
				parent = nullptr;
				isLeaf = false;
			}

			// Dtor
			~QuadtreeNode()
			{
				delete children[0];
				delete children[1];
				delete children[2];
				delete children[3];
			}

			Vector3					center;			// QuadtreeNode center
			Vector3					size;			// Desired bounds by the tree
			QuadtreeNode*			children[4];	// Child nodes
			QuadtreeNode*			parent;			// Parent node
			Vector3					looseCenter;	// Center of all object bounds
			Vector3					looseSize;		// Exact size of volume from all objects
			List<QuadtreeElement*>	objects;		// Object in this node, for leaf nodes only
			bool					isLeaf;			// True if this is a leaf node
		};

		// Build the tree recursively
		void BuildTree( QuadtreeNode* root, int32 depth, int32 maxDepth );

		// Check if the point is contained in a node
		bool CheckPoint( QuadtreeNode* node, const Vector3& point );

		// Compute the loose bounds for a node
		void ComputeNodeBounds( QuadtreeNode* node );

		// Inset an object
		void Insert( QuadtreeElement* obj, QuadtreeNode* node );

		// Get all the objects contained within a frustum
		template <typename T>
		void GetVisibleObjects( QuadtreeNode* node, const Frustum& viewFrustum, void(T::*callback)(QuadtreeElement*), T* caller ) const
		{
			if ( viewFrustum.CheckAABB( node->looseCenter, node->looseSize ) )
			{
				if ( node->isLeaf )
				{
					for ( auto i = node->objects.Begin(); !i.End(); ++i )
						(caller->*callback)(*i);
				}
				else
				{
					for ( int32 i = 0; i < 4; ++i )
					{
						if ( node->children[i] )
							GetVisibleObjects( node->children[i], viewFrustum, callback, caller );
					}
				}
			}
		}

		// Visualize a node
		template <typename T>
		void RenderNode( QuadtreeNode* node, void(T::*callback)(const Vector3&, const Vector3&, const Color&), T* caller )
		{
			if ( node )
			{
				if ( node->objects.Size() > 0 )
				{
					(caller->*callback)( node->looseCenter, node->looseSize, Color::Red );
					(caller->*callback)( node->center, node->size, Color::White );
				}
				RenderNode( node->children[0], callback, caller );
				RenderNode( node->children[1], callback, caller );
				RenderNode( node->children[2], callback, caller );
				RenderNode( node->children[3], callback, caller );
			}
		}

		QuadtreeNode*							_root;				// Root node
	};


	// An object that can exist in a quadtree
	class QuadtreeElement
	{
		friend class Quadtree;
	public:

		// Ctor
		QuadtreeElement();

		// Get the bounding volume for this object
		virtual const BoundingVolume& GetBounds() const = 0;

	private:

		Quadtree::QuadtreeNode*	_node;
	};
}
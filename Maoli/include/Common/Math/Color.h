//--------------------------------------------------------------------------------------
// File: Color.h
//
// RGBA Color
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class Color
	{
	public:
		// Ctor
		Color();

		// Ctor
		Color(float r, float g, float b, float a = 1.0f);

		// Move Ctor
		Color(Color&& v);

		// Move assignment operator
		Color& operator=(Color&&);

		// Fill the data
		void Set(float x, float y, float z, float w = 1.0f);

		// Auto cast to float pointer
		operator float*();

		// Auto cast to float pointer
		operator const float*() const;

		// Bracket access
		float& operator[](int32 i);

		// Constant bracket access
		float operator[](int32 i) const;

		// Subtraction
		Color  operator- (const Color& v) const;
		Color& operator-=(const Color& v);

		// Addition
		Color  operator+ (const Color& v) const;
		Color& operator+=(const Color& v);

		// Scalar multiplication
		Color  operator* (float f) const;
		Color& operator*=(float f);

		// Scalar division
		Color  operator/ (float f) const;
		Color& operator/=(float f);

		// Multiplication
		Color operator*(const Color& v) const;
		Color& operator*=(const Color& v);

		// Clamp to 0-1 range
		void Saturate();

		union
		{
			// Elements
			struct
			{	
				float r, g, b, a;
			};

			// Array
			float _f[4];
		};


		// Some default colors
		static const Color White;
		static const Color Red;
		static const Color Green;
		static const Color Blue;
		static const Color Black;
	};

}

#include "Color.inl"

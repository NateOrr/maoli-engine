//--------------------------------------------------------------------------------------
// File: Rect.h
//
// 2D Rectangle
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{
	// Ctor
	inline Rect::Rect()
	{
		x = y = width = height = 0;
	}


	// Ctor
	inline Rect::Rect(int32 rx, int32 ry, int32 rw, int32 rh)
	{
		x = rx;
		y = ry;
		width = rw;
		height = rh;
	}


	// Point addition
	inline Rect Rect::operator+(const Point& p)
	{
		return Rect(x+p.x, y+p.y, width, height);
	}


	// Point addition
	inline Rect& Rect::operator+=(const Point& p)
	{
		x += p.x;
		y += p.y;
		return *this;
	}


	// Point subtraction
	inline Rect Rect::operator-(const Point& p)
	{
		return Rect(x-p.x, y-p.y, width, height);
	}


	// Point subtraction
	inline Rect& Rect::operator-=(const Point& p)
	{
		x -= p.x;
		y -= p.y;
		return *this;
	}
}

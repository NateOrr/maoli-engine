//--------------------------------------------------------------------------------------
// File: Color.inl
//
// RGBA Color
//
// Nate Orr
//--------------------------------------------------------------------------------------

#include <algorithm>

namespace Maoli
{
	// Ctor
	inline Color::Color() : r( 0 ), g( 0 ), b( 0 ), a( 1.0f )
	{

	}

	// Ctor
	inline Color::Color( float vx, float vy, float vz, float vw /*= 1.0f*/ ) : r( vx ), g( vy ), b( vz ), a( vw )
	{

	}

	// Move Ctor
	inline Color::Color( Color&& v ) : r( v.r ), g( v.g ), b( v.b ), a( v.a )
	{

	}

	// Move assignment operator
	inline Color& Color::operator=(Color&& v)
	{
		r = v.r; g = v.g; b = v.b; a = v.a;
		return *this;
	}


	// Fill the data
	inline void Color::Set( float r, float g, float b, float a /*= 1.0f*/ )
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	// Bracket access
	inline float& Color::operator[]( int32 i )
	{
		return _f[i];
	}

	// Constant bracket access
	inline float Color::operator[]( int32 i ) const
	{
		return _f[i];
	}


	// Auto cast to float pointer
	inline Color::operator float*()
	{
		return (float*)this;
	}

	// Auto cast to float pointer
	inline Color::operator const float*() const
	{
		return (const float*)this;
	}

	// Subtraction
	inline Color Color::operator-(const Color& v) const
	{
		return Color( r - v.r, g - v.g, b - v.b, a - v.a );
	}

	// Subtraction
	inline Color& Color::operator-=(const Color& v)
	{
		r -= v.r;
		g -= v.g;
		b -= v.b;
		a -= v.a;
		return *this;
	}

	// Addition
	inline Color Color::operator+(const Color& v) const
	{
		return Color( r + v.r, g + v.g, b + v.b, a + v.a );
	}

	// Addition
	inline Color& Color::operator+=(const Color& v)
	{
		r += v.r;
		g += v.g;
		b += v.b;
		a += v.a;
		return *this;
	}


	// Scalar multiplication
	inline Color Color::operator*(float f) const
	{
		return Color( r*f, g*f, b*f, a*f );
	}


	// Scalar multiplication
	inline Color& Color::operator*=(float f)
	{
		r *= f;
		g *= f;
		b *= f;
		a *= f;
		return *this;
	}


	// Scalar division
	inline Color Color::operator/(float f) const
	{
		float d = 1.0f / f;
		return Color( r*d, g*d, b*d, a*d );
	}


	// Scalar division
	inline Color& Color::operator/=(float f)
	{
		float d = 1.0f / f;
		r *= d;
		g *= d;
		b *= d;
		a *= d;
		return *this;
	}

	// Piecewise multiplication
	inline Color& Color::operator*=(const Color& v)
	{
		r *= v.r;
		g *= v.g;
		b *= v.b;
		a *= v.a;
		return *this;
	}

	// Piecewise multiplication
	inline Color Color::operator*(const Color& v) const
	{
		return Color( r*v.r, g*v.g, b*v.b, a*v.a );
	}


	// Clamp to 0-1 range
	inline void Color::Saturate()
	{
		r = std::max( std::min( r, 1.0f ), 0.0f );
		g = std::max( std::min( g, 1.0f ), 0.0f );
		b = std::max( std::min( b, 1.0f ), 0.0f );
		a = std::max( std::min( a, 1.0f ), 0.0f );
	}

}

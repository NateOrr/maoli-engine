//--------------------------------------------------------------------------------------
// File: Quadtree.cpp
//
// Loose quadtree
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Quadtree.h"

namespace Maoli
{
	// Element Ctor
	QuadtreeElement::QuadtreeElement()
	{
		_node = nullptr;
	}

	// Ctor
	Quadtree::Quadtree( const Vector3& worldCenter, const Vector3& worldBounds, int32 depth )
	{
		// Setup the root node, then build the tree
		float largestAxis = std::max( worldBounds.x, worldBounds.z );
		_root = new QuadtreeNode;
		_root->center = _root->looseCenter = worldCenter;
		_root->looseSize = _root->size = Vector3( largestAxis, worldBounds.y, largestAxis );
		BuildTree( _root, 1, depth );
	}

	// Dtor
	Quadtree::~Quadtree()
	{
		delete _root;
	}

	// Build the tree recursively
	void Quadtree::BuildTree( QuadtreeNode* root, int32 depth, int32 maxDepth )
	{
		// Abort if the max depth was reached
		if ( depth == maxDepth )
		{
			root->isLeaf = true;
			return;
		}

		// Size for each node
		float nodeSize = root->size.x * 0.5f;

		// Top left child
		root->children[0] = new QuadtreeNode;
		root->children[0]->center = root->children[0]->looseCenter = root->center + Vector3( -nodeSize, 0, nodeSize );
		root->children[0]->size = root->children[0]->looseSize = Vector3( nodeSize, root->size.y, nodeSize );
		root->children[0]->parent = root;

		// Top right child
		root->children[1] = new QuadtreeNode;
		root->children[1]->center = root->children[1]->looseCenter = root->center + Vector3( nodeSize, 0, nodeSize );
		root->children[1]->size = root->children[1]->looseSize = Vector3( nodeSize, root->size.y, nodeSize );
		root->children[1]->parent = root;

		// Bottom right child
		root->children[2] = new QuadtreeNode;
		root->children[2]->center = root->children[2]->looseCenter = root->center + Vector3( nodeSize, 0, -nodeSize );
		root->children[2]->size = root->children[2]->looseSize = Vector3( nodeSize, root->size.y, nodeSize );
		root->children[2]->parent = root;

		// Bottom left child
		root->children[3] = new QuadtreeNode;
		root->children[3]->center = root->children[3]->looseCenter = root->center + Vector3( -nodeSize, 0, -nodeSize );
		root->children[3]->size = root->children[3]->looseSize = Vector3( nodeSize, root->size.y, nodeSize );
		root->children[3]->parent = root;

		// Build the tree for each child
		BuildTree( root->children[0], depth + 1, maxDepth );
		BuildTree( root->children[1], depth + 1, maxDepth );
		BuildTree( root->children[2], depth + 1, maxDepth );
		BuildTree( root->children[3], depth + 1, maxDepth );
	}

	// Insert an object into the tree, or update the object if it's already in the tree
	void Quadtree::Insert( QuadtreeElement* obj )
	{
		// Remove the object from it's previous node if needed
		if ( obj->_node )
		{
			// If the object still exists inside this node, simply recompute the bounds
			if ( CheckPoint( obj->_node, obj->GetBounds().GetWorldPosition() ) )
			{
				ComputeNodeBounds( obj->_node );
				return;
			}
			else
			{
				// Otherwise remove from the node
				obj->_node->objects.Remove( obj );
				obj->_node = nullptr;
			}
		}

		// Insert into the tree
		Insert( obj, _root );
	}

	// Inset an object
	void Quadtree::Insert( QuadtreeElement* obj, QuadtreeNode* node )
	{
		// Check if this node contains the object
		if ( CheckPoint( node, obj->GetBounds().GetWorldPosition() ) )
		{
			// If this node is a leaf, add the object
			if ( node->isLeaf )
			{
				node->objects.Add( obj );
				obj->_node = node;
				ComputeNodeBounds( node );
			}
			else
			{
				// Otherwise, check the children
				for ( int32 i = 0; i < 4; ++i )
				{
					if ( node->children[i] )
						Insert( obj, node->children[i] );
				}
			}
		}
	}

	// Check if the point is contained in a node
	bool Quadtree::CheckPoint( QuadtreeNode* node, const Vector3& point )
	{
		return (
			(point.x > node->center.x - node->size.x && point.x <= node->center.x + node->size.x) &&
			(point.z > node->center.z - node->size.z && point.z <= node->center.z + node->size.z)
			);
	}

	// Compute the loose bounds for a node
	void Quadtree::ComputeNodeBounds( QuadtreeNode* node )
	{
		// Leaf nodes must compute exact bounds from their objects
		if ( node->isLeaf )
		{
			// If this node contains no objects, just use the original bounds
			if ( node->objects.IsEmpty() )
			{
				node->looseCenter = node->center;
				node->looseSize = node->size;
			}
			else
			{
				// Compute the AABB min/max
				Vector3 min( 9999, 9999, 9999 ), max( -9999, -9999, -9999 );
				for ( auto i = node->objects.Begin(); !i.End(); ++i )
				{
					QuadtreeElement* obj = *i;
					const Vector3& pos = obj->GetBounds().GetWorldPosition();
					const Vector3& size = obj->GetBounds().GetSize();
					min = Vector3::Min( pos - size, min );
					max = Vector3::Max( pos + size, max );
				}

				// Compute the new center / size
				node->looseCenter = (max + min) * 0.5f;
				node->looseSize = max - node->looseCenter;
			}
		}
		else
		{
			Vector3 min( 9999, 9999, 9999 ), max( -9999, -9999, -9999 );
			for ( int32 i = 0; i < 4; ++i )
			{
				min = Vector3::Min( node->children[i]->looseCenter - node->children[i]->looseSize, min );
				max = Vector3::Max( node->children[i]->looseCenter + node->children[i]->looseSize, max );
			}
		}

		// Update the bounds for the parent nodes up the chain
		if ( node->parent )
			ComputeNodeBounds( node->parent );
	}

	// Remove an object from the tree
	void Quadtree::Remove( QuadtreeElement* obj )
	{
		if ( obj->_node )
		{
			obj->_node->objects.Remove( obj );
			ComputeNodeBounds( obj->_node );
			obj->_node = nullptr;
		}
	}

}
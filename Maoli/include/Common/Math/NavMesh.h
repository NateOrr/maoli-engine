//--------------------------------------------------------------------------------------
// File: NavMesh.h
//
// Navigation mesh for pathfinding
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Game/Component.h"

namespace Maoli
{
	// Forward decl
	class Engine;
	class Geometry;

	// Navigation mesh for pathfinding
	class NavMesh : public Component
	{
	public:

		COMPONENT_DECL( NavMesh );

		// Ctor
		NavMesh( Engine* engine );

		// Dtor
		~NavMesh();

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Load geometry
		bool Load( const char* file );

		// Set the heuristic weight
		inline void SetHeuristicWeight( float weight ) { _heuristicWeight = weight; }

		// Get the heuristic weight
		inline float GetHeuristicWeight() const { return _heuristicWeight; }

		// Find a path between two points
		// Returns false if either point is outside the nav mesh
		bool FindPath( const Vector3& start, const Vector3& end, List<Vector3>& path );

	protected:

		// Forward decl
		struct Triangle;

		// Triangle edge
		struct Edge
		{
			// Ctor
			Edge() : next(nullptr) {}

			uint32 verts[2];		// Indices into the vertex array
			Triangle* next;		// Adjacent triangle
		private:
			int32 pad;			// Make 16 bytes
		};

		// Triangle node for A*
		struct Triangle
		{
			Edge edges[3];
			Vector3 center;
		};

		// Planning node for tracking the path with triangles
		struct PlannerNode
		{
			Triangle* triangle;		// Node index
			float givenCost;		// G
			float heuristicCost;	// H
			float finalCost;		// G+H
			Edge* entryEdge;		// Edge the path enters through
			PlannerNode* parent;	// Previous node in the path
			PlannerNode* child;		// Next node in the path
			uint32 refCount;			// Memory management
		};

		// Node comparison
		static inline bool CompareNodes( PlannerNode*const& a, PlannerNode*const& b ) { return a->finalCost > b->finalCost; }

		// Get the cost estimate between two nodes
		float EstimateCost( const Vector3& a, const Vector3& b ) const;

		// Process a successor node
		void ProcessNode( Triangle* triangle, Edge& entryEdge, PlannerNode* parent );

		// Get the closest edge to a point
		uint32 ClosestEdgeToPoint( Triangle& triangle, const Vector3& point );

		// Construct a final path using funneling
		void FunnelPath( PlannerNode* node, List<Vector3>& path );

		float												_heuristicWeight;	// Weight for heuristic cost
		Array<Vector3>										_verts;				// Nodes
		Array<uint32>											_faces;				// Triangles
		Array<Triangle>										_triangles;			// Triangles
		uint32												_goalIndex;			// Node index for the goal
		Vector3												_start;				// Start position
		Vector3												_goal;				// Goal position
		std::unordered_map<const Triangle*, PlannerNode*>	_visited;			// A* visited list
		PriorityQueue<PlannerNode*>							_open;				// Open list for A*
		List<Vector3>										_badPath;			// todo: remove
	};
}

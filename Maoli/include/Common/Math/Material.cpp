//--------------------------------------------------------------------------------------
// File: Material.cpp
//
// Surface properties
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Material.h"
#include "Engine\Engine.h"

namespace Maoli
{
	// Ctor
	Material::Material( Engine* engine ) : DataType( engine )
	{
		_properties.baseColor = Vector3( 1, 1, 1 );
		_properties.emissive = Vector4( 0, 0, 0, 0 );
		_properties.metalic = 0;
		_properties.roughness = 0.5f;
		_properties.transparency = 0;
		_twoSided = false;
		_alphaTesting = false;
		_refractive = false;

		for ( uint32 i = 0; i < TEX_SIZE; ++i )
		{
			_textureFiles[i] = String::none;
			_properties.textureFlag.Unset( i );
		}
	}

	// Dtor
	Material::~Material()
	{

	}

	// Write to binary file
	void Material::Serialize( std::ofstream& fout ) const
	{
		Maoli::Serialize( fout, &_properties );
		Maoli::Serialize( fout, &_alphaTesting );
		Maoli::Serialize( fout, &_twoSided );
		Maoli::Serialize( fout, &_refractive );
		String str;
		for ( int32 i = 0; i < TEX_SIZE; i++ )
			_textureFiles[i].Serialize( fout );
	}

	// Read from binary file
	void Material::Deserialize( std::ifstream& fin )
	{
		Maoli::Deserialize( fin, &_properties );
		Maoli::Deserialize( fin, &_alphaTesting );
		Maoli::Deserialize( fin, &_twoSided );
		Maoli::Deserialize( fin, &_refractive );
		String dir = GetFilePath().GetDirectory();
		for ( int32 i = 0; i < TEX_SIZE; i++ )
			_textureFiles[i].Deserialize( fin );
	}

	// Write to binary file
	void Material::Save( const char* file )
	{
		String path = file ? file : GetFilePath();
		std::ofstream fout( path );
		if ( fout.is_open() )
		{
			UpdateFilePath( path );
			MoveTextures( String( path ).GetDirectory() );
			Serialize( fout );
			fout.close();
		}
	}


	// Read from binary file
	bool Material::Load( const char* file )
	{
		std::ifstream fin( file );
		if ( fin.is_open() )
		{
			Deserialize( fin );

			// Update the texture paths
			String dir = String( file ).GetDirectory();
			for ( uint32 i = 0; i < TEX_SIZE; ++i )
			{
				if ( _textureFiles[i] != String::none )
					_textureFiles[i] = dir + _textureFiles[i].GetFile();
			}

			fin.close();
			return true;
		}
		return false;
	}

	// Move textures files to the directory
	void Material::MoveTextures( const char* dir )
	{
		String destDir = dir;
		for ( int32 i = 0; i < TEX_TYPE::TEX_SIZE; ++i )
		{
			if ( _textureFiles[i] != String::none )
			{
				_parent->GetPlatform()->CopyFile( _textureFiles[i], destDir + _textureFiles[i].GetFile() );
				_textureFiles[i] = _textureFiles[i].GetFile();
			}
		}
	}

	// Set a texture name
	bool Material::SetTextureName( const char* fileName, TEX_TYPE type )
	{
		if ( FileExists( fileName ) )
		{
			_textureFiles[type] = fileName;
			return true;
		}
		return false;
	}

	// Clone this material, with a new name
	Material* Material::Clone( const char* newName )
	{
		Material* clone = Material::Create( newName, _parent );
		memcpy( &clone->_properties, &_properties, sizeof(_properties) );
		clone->_alphaTesting = _alphaTesting;
		clone->_twoSided = _twoSided;
		clone->_refractive = _refractive;
		for ( int32 i = 0; i < TEX_SIZE; ++i )
			clone->_textureFiles[i] = _textureFiles[i];
		return clone;
	}

	// Transparency
	void Material::SetTransparency( float f )
	{
		bool prevTrans = IsTransparent();
		_properties.transparency = f;
		if ( prevTrans != IsTransparent() )
			_parent->BroadcastEvent( EngineEvent::MaterialChanged, this );
	}

	// Enable embedded alpha testing
	void Material::EnableAlphaTesting( bool value )
	{
		if ( _alphaTesting != value )
		{
			_alphaTesting = value;
			_parent->BroadcastEvent( EngineEvent::MaterialChanged, this );
		}
	}

	

}


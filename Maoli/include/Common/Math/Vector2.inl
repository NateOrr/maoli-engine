//--------------------------------------------------------------------------------------
// File: Vector2.cpp
//
// 4D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	inline Vector2::Vector2() : x( 0 ), y( 0 )
	{

	}

	// Ctor
	inline Vector2::Vector2( float vx, float vy ) : x( vx ), y( vy )
	{

	}

	// Move Ctor
	inline Vector2::Vector2( Vector2&& v ) : x( v.x ), y( v.y )
	{

	}

	// Move assignment operator
	inline Vector2& Vector2::operator=(Vector2&& v)
	{
		x = v.x; y = v.y;
		return *this;
	}

	// Auto cast to float pointer
	inline Vector2::operator float*()
	{
		return (float*)this;
	}

	// Bracket access
	inline float& Vector2::operator[]( int32 i )
	{
		return _f[i];
	}

	// Constant bracket access
	inline float Vector2::operator[]( int32 i ) const
	{
		return _f[i];
	}

	// Fill the data
	inline void Vector2::Set( float x, float y )
	{
		this->x = x;
		this->y = y;
	}

	// Negation
	inline Maoli::Vector2 Vector2::operator-() const
	{
		return Vector2( -x, -y );
	}

	// Subtraction
	inline Vector2 Vector2::operator-(const Vector2& v) const
	{
		return Vector2( x - v.x, y - v.y );
	}

	// Subtraction
	inline Vector2& Vector2::operator-=(const Vector2& v)
	{
		x -= v.x;
		y -= v.y;
		return *this;
	}

	// Addition
	inline Vector2 Vector2::operator+(const Vector2& v) const
	{
		return Vector2( x + v.x, y + v.y );
	}

	// Addition
	inline Vector2& Vector2::operator+=(const Vector2& v)
	{
		x += v.x;
		y += v.y;
		return *this;
	}


	// Scalar multiplication
	inline Vector2 Vector2::operator*(float f) const
	{
		return Vector2( x*f, y*f );
	}


	// Scalar multiplication
	inline Vector2& Vector2::operator*=(float f)
	{
		x *= f;
		y *= f;
		return *this;
	}


	// Scalar division
	inline Vector2 Vector2::operator/(float f) const
	{
		float r = 1.0f / f;
		return Vector2( x*r, y*r );
	}


	// Scalar division
	inline Vector2& Vector2::operator/=(float f)
	{
		float r = 1.0f / f;
		x *= r;
		y *= r;
		return *this;
	}



	// Dot product
	inline float Vector2::Dot( const Vector2& v ) const
	{
		return x*v.x + y*v.y;
	}


	// Dot product
	inline float Vector2::operator*(const Vector2& v) const
	{
		return Dot( v );
	}



	// Length
	inline float Vector2::Length() const
	{
		return sqrtf( x*x + y*y );
	}

	// Square of the length
	inline float Vector2::LengthSquared() const
	{
		return x*x + y*y;
	}


	// Normalize the vector
	inline Vector2& Vector2::Normalize()
	{
		float l = 1.0f / Length();
		x *= l;
		y *= l;
		return *this;
	}

	// Max vector
	inline Vector2 Vector2::Max( const Vector2& a, const Vector2& b )
	{
		return Vector2( std::max( a.x, b.x ), std::max( a.y, b.y ) );
	}

	// Min vector
	inline Vector2 Vector2::Min( const Vector2& a, const Vector2& b )
	{
		return Vector2( std::min( a.x, b.x ), std::min( a.y, b.y ) );
	}

	// Comparison
	inline bool Vector2::operator==(const Vector2& v) const
	{
		return (x == v.x && y == v.y);
	}


	// Comparison
	inline bool Vector2::operator!=(const Vector2& v) const
	{
		return (x != v.x || y != v.y);
	}

}

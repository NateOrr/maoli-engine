//--------------------------------------------------------------------------------------
// File: Matrix.h
//
// 4x4 orthogonal transformation matrix
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class Matrix
	{
		friend class Camera;
		friend class Frustum;
		friend class Transform;
		friend class Model;
	public:

		// Ctor
		Matrix();

		// Ctor
		Matrix( float m11, float m12, float m13, float m14,
			float m21, float m22, float m23, float m24,
			float m31, float m32, float m33, float m34,
			float m41, float m42, float m43, float m44 );

		// Move Ctor
		Matrix( Matrix&& v );

		// Copy Ctor
		Matrix( const Matrix& v );

		// Assignment op
		Matrix& operator=(const Matrix& v);

		// Make an identity matrix
		Matrix& Identity();

		// Make the zero matrix
		Matrix& Zero();

		// Fill the matrix
		Matrix& Set( float m11, float m12, float m13, float m14,
			float m21, float m22, float m23, float m24,
			float m31, float m32, float m33, float m34,
			float m41, float m42, float m43, float m44 );


		////////////////////////////////////////////////////////////
		// Transformation construction

		// Identity matrix
		static Matrix CreateIdentity();

		// Translation matrix
		static Matrix CreateTranslation( float x, float y, float z );
		static Matrix CreateTranslation( const Vector3& v );

		// Make a rotation matrix about the x-axis
		static Matrix CreateRotationX( float theta );

		// Make a rotation matrix about the y-axis
		static Matrix CreateRotationY( float theta );

		// Make a rotation matrix about the z-axis
		static Matrix CreateRotationZ( float theta );

		// Create a camera matrix
		static Matrix CreateCameraMatrix( const Vector3& position, float thetaX, float thetaY );

		// Yaw pitch roll matrix
		static Matrix CreateRotationYawPitchRoll( float yaw, float pitch, float roll );

		// Make a scaling matrix
		static Matrix CreateScaling( float x, float y, float z );
		static Matrix CreateScaling( const Vector3& v );

		// Make an orthographic matrix
		static Matrix CreateOrthographic( float width, float height, float nearZ, float farZ );

		// Create a lookat view matrix
		static Matrix CreateLookAtView( const Vector3& eye, const Vector3& at, const Vector3& up );

		////////////////////////////////////////////////////////////


		// Perform a local rotation about the x-axis
		Matrix& RotateLocalX( float theta );

		// Perform a local rotation about the y-axis
		Matrix& RotateLocalY( float theta );

		// Perform a local rotation about the z-axis
		Matrix& RotateLocalZ( float theta );

		// Perform a rotation about the x-axis
		Matrix& RotateX( float theta );

		// Perform a rotation about the y-axis
		Matrix& RotateY( float theta );

		// Perform a rotation about the z-axis
		Matrix& RotateZ( float theta );

		// Perform a scale
		Matrix& Scale( float x, float y, float z );
		Matrix& Scale( const Vector3& v );

		// Perform a translation
		Matrix& Translate( float x, float y, float z );
		Matrix& Translate( const Vector3& v );


		// Scalar multiplication
		Matrix operator*(float f) const;
		Matrix& operator*=(float f);


		// Multiply
		Matrix& Mult( const Matrix& m );
		Matrix operator*(const Matrix& m) const;
		Matrix& operator*=(const Matrix& m);

		// Transpose the matrix
		Matrix& Transpose();

		// Get the transpose of this matrix
		Matrix GetTranspose() const;

		// Invert an orthonormal matrix
		Matrix& InverseOrthonormal();

		// Inverse
		Matrix& Inverse();

		// Get the inverse of this matrix
		Matrix GetInverse() const;

		// Transform a vector
		Vector4 Transform( const Vector4& v ) const;

		// Transform a 3d vector
		Vector3 Transform( const Vector3& v ) const;

		// Transform a 3d direction vector
		Vector3 TransformNormal( const Vector3& v ) const;

		// Transform a vector array
		void TransformArray( const Vector4* source, uint32 count, Vector4* dest ) const;

		// Transform a 3d vector array
		void TransformArray( const Vector3* source, uint32 count, Vector3* dest ) const;

		// Transform a 3d direction vector array
		void TransformNormalArray( const Vector3* source, uint32 count, Vector3* dest ) const;

		// Get the 3x3 part of the matrix
		Matrix Get3x3() const;

		// Auto cast to float pointer
		operator float*();

		// Auto cast to float pointer
		operator const float*() const;

		// Print to console
		void Print();

		// Transpose the orientation part
		Matrix& TransposeOrientation();

		// Orient the matrix to face the given point
		Matrix& LookAt( const Vector3& point );
		Matrix& LookAt( const Vector3& position, const Vector3& point );
		Matrix& LookAt( float x, float y, float z );

		// Build a perspective projection matrix
		Matrix& Perspective( float fovY, float aspect, float zNear, float zFar );

		// Make an orthographic matrix
		Matrix& Orthographic( float width, float height, float nearZ, float farZ );

		// Create an arbitrary rotation
		Matrix& CameraMatrix( const Vector3& position, float thetaX, float thetaY );

		// Element access
		float& operator()( int32 x, int32 y );

		// Const element access
		float operator()( int32 x, int32 y ) const;

		// Get the position component
		const Vector3& GetPosition() const;

		// Access a row
		const Vector4& GetRow( int32 row ) const;

		// Get the forward vector
		const Vector3& GetForward() const;

		// Get the right vector
		const Vector3& GetRight() const;

		// Get the up vector
		const Vector3& GetUp() const;

	private:

		// Axis access
		inline Vector3& Right() { return *(Vector3*)&_11; }
		inline Vector3& Up() { return *(Vector3*)&_21; }
		inline Vector3& Forward() { return *(Vector3*)&_31; }
		inline Vector3& Position() { return *(Vector3*)&_41; }
		inline const Vector3& Right() const { return *(const Vector3*)&_11; }
		inline const Vector3& Up() const { return *(const Vector3*)&_21; }
		inline const Vector3& Forward() const { return *(const Vector3*)&_31; }
		inline const Vector3& Position() const { return *(const Vector3*)&_41; }

		// Axis array access
		inline Vector4* Axes() { return (Vector4*)&_11; }
		inline const Vector4* Axes() const { return (const Vector4*)&_11; }

		// Data
		union
		{
			struct
			{
				float _11, _12, _13, _14;
				float _21, _22, _23, _24;
				float _31, _32, _33, _34;
				float _41, _42, _43, _44;
			};

			struct 
			{
				float _mat[4][4];
			};
		};

	};

}

#include "Matrix.inl"

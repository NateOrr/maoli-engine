//--------------------------------------------------------------------------------------
// File: BoundingVolume.inl
//
// 3D bounding volume, with world position tracking
// 
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	inline BoundingVolume::BoundingVolume()
	{

	}

	// Set the bounding box
	inline void BoundingVolume::Set( const Vector3& pos, const Vector3& size )
	{
		_position = _center = pos;
		_size = size;
		_radius = _size.Length();
	}


	// Set the bounding sphere (this will make the extents a cube)
	inline void BoundingVolume::Set( const Vector3& pos, float r )
	{
		_position = _center = pos;
		_size = Vector3(r, r, r);
		_radius = r;
	}

	// Get local position
	inline const Vector3& BoundingVolume::GetLocalPosition( ) const
	{
		return _center;
	}

	// Get the world position
	inline const Vector3& BoundingVolume::GetWorldPosition( ) const
	{
		return _position;

	}

	// Set the world position to track
	inline void BoundingVolume::SetWorldPosition( const Vector3& pos )
	{
		_position = _center + pos;
	}

	// Get the size
	inline const Vector3& BoundingVolume::GetSize() const
	{
		return _size;
	}

	// Get the radius
	inline float BoundingVolume::GetRadius() const
	{
		return _radius;
	}

}

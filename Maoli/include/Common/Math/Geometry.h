//--------------------------------------------------------------------------------------
// File: Geometry.h
//
// 3D Geometry management
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Material.h"

namespace Maoli
{
	// Chunk of geometry that has it's own properties
	struct GeometryChunk
	{
		// Ctor
		GeometryChunk();

		int32 startIndex;
		int32 indexCount;
		Material* material;
		String name;
		Vector3 bounds;
		Vector3 localPosition;
	};

	// Vertex weights for animation
	struct BoneWeight
	{
		// Ctor
		BoneWeight();

		float weights[4];	// Blend weights
		uint32 bones[4];		// Bone indices
	};



	// Chunk of geometry, built of sub-meshes
	class Geometry : public DataType<Geometry, Engine>
	{
		friend DataType<Geometry, Engine>;
	public:

		// Get the GeometryChunk list
		inline const Array<GeometryChunk>& GetChunks() const { return _chunks; }

		// Get a GeometryChunk
		inline GeometryChunk* GetChunk( int32 i ) { return &_chunks[i]; }
		inline const GeometryChunk* GetChunk( int32 i ) const { return &_chunks[i]; }

		// Get the number of meshes
		inline uint32 GetNumChunks() const { return _chunks.Size(); }

		// Get the vertex data
		inline const Vector3* GetVerts() const { return _positions; }

		// Get the normal data
		inline const Vector3* GetNormals() const { return _normals; }

		// Get the texture data
		inline const Vector2* GetTexCoords() const { return _texCoords; }

		// Check if this mesh contains bone weights
		inline bool HasBoneWeights() const { return _boneWeights.Size() > 0; }

		// Get the bone weight data
		inline const BoneWeight* GetBoneWeights() const { return _boneWeights; }

		// Get the number of verts
		inline uint32 GetNumVerts() const { return _positions.Size(); }

		// Get the index data
		inline const uint32* GetIndices() const { return _indices; }

		// Get the number of indices
		inline uint32 GetNumIndices() const { return _indices.Size(); }

		// Get the number of triangles
		inline uint32 GetNumFaces() const { return _indices.Size() / 3; }

		// Get the AABB size
		inline const Vector3& GetBounds() const { return _bounds; }

		// Get the AABB center
		inline const Vector3& GetCenter() const { return _center; }

		// Centers the mesh about the origin
		void CenterMesh();

		// Swap tex coords
		void FlipUV();

		// Invert Y tex coord
		void InvertUV();

		// Change coordinate system (for 3ds type compatibility)
		void FlipCoordinateSystem();

		// Reverse vertex winding
		void ReverseFaceWinding();

		// Computes non-averaged normals for the given mesh
		void RecomputeHardNormals( int32 index );

		// Compute normals
		void RecomputeNormals();

		// Delete a GeometryChunk
		void DeleteGeometryChunk( int32 index );

		// Save to file
		void Save( const char* file );

	protected:

		Array<GeometryChunk>	_chunks;		// Geometry chunks
		uint32					_numBones;		// Number of bones if this geometry is rigged
		Vector3					_bounds;		// AABB
		Vector3					_center;		// Mesh center

		// Vertex data streams
		Array<Vector3>			_positions;
		Array<Vector2>			_texCoords;
		Array<Vector3>			_normals;
		Array<BoneWeight>		_boneWeights;
		Array<uint32>				_indices;

		// Used for binary load/save
		struct LoadInfo
		{
			uint32 numVerts;
			uint32 numIndices;
			uint32 numGeometryChunks;
			uint32 numBones;
		};
		struct GeometryChunkLoadInfo
		{
			int32 startIndex;
			int32 numIndices;
		};

		// Binary file id
		static const uint32 _fileID = 567938;

		// Loads the model
		bool Load( const char* file );

		// Merges sumbeshes
		void Optimize();

		// Tex coord correction
		void FixTexCoords();

		// Remove duplicate verts
		void RemoveDuplicateVerts();

		// Compute bounding box
		void ComputeBounds();

		// Cleanup
		void Release();

		// Ctor
		Geometry( Engine* engine );

		// Dtor
		~Geometry();

		// Prevent copying
		Geometry( const Geometry& );
		Geometry& operator=(const Geometry&);

		// Management
		static std::map<String, Geometry*> _resources;

	};
}

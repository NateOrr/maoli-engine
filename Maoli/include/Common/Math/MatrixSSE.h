//--------------------------------------------------------------------------------------
// File: MatrixSSE.h
//
// 4x4 orthogonal transformation matrix using SSE instrinsics
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

// SSE only supported on windows
#ifdef MAOLI_WINDOWS

namespace Maoli
{
	namespace SSE
	{

		__declspec(align(16)) class Matrix
		{
			friend class Camera;
			friend class Frustum;
		public:

			// Ctor
			Matrix();

			// Ctor
			Matrix( float m11, float m12 ,float m13, float m14,
				float m21, float m22 ,float m23, float m24,
				float m31, float m32 ,float m33, float m34,
				float m41, float m42 ,float m43, float m44 );

			// Move Ctor
			Matrix(Matrix&& v);

			// Copy Ctor
			Matrix(const Matrix& v);

			// Assignment op
			Matrix& operator=(const Matrix& v);

			// Make an identity MatrixSSE
			Matrix& Identity();

			// Make the zero MatrixSSE
			Matrix& Zero();

			// Fill the MatrixSSE
			Matrix& Set( float m11, float m12 ,float m13, float m14,
				float m21, float m22 ,float m23, float m24,
				float m31, float m32 ,float m33, float m34,
				float m41, float m42 ,float m43, float m44 );


			////////////////////////////////////////////////////////////
			// Transformation construction

			// Identity MatrixSSE
			static Matrix CreateIdentity();

			// Translation MatrixSSE
			static Matrix CreateTranslation(float x, float y, float z);
			static Matrix CreateTranslation(const Vector3& v);

			// Make a rotation MatrixSSE about the x-axis
			static Matrix CreateRotationX(float theta);

			// Make a rotation MatrixSSE about the y-axis
			static Matrix CreateRotationY(float theta);

			// Make a rotation MatrixSSE about the z-axis
			static Matrix CreateRotationZ(float theta);

			// Create a camera MatrixSSE
			static Matrix CreateCameraMatrixSSE(const Vector3& position, float thetaX, float thetaY);

			// Yaw pitch roll MatrixSSE
			static Matrix CreateRotationYawPitchRoll(float yaw, float pitch, float roll);

			// Make a scaling MatrixSSE
			static Matrix CreateScaling(float x, float y, float z);
			static Matrix CreateScaling(const Vector3& v);

			// Make an orthographic MatrixSSE
			static Matrix CreateOrthographic(float width, float height, float nearZ, float farZ);

			////////////////////////////////////////////////////////////


			// Perform a local rotation about the x-axis
			Matrix& RotateLocalX(float theta);

			// Perform a local rotation about the y-axis
			Matrix& RotateLocalY(float theta);

			// Perform a local rotation about the z-axis
			Matrix& RotateLocalZ(float theta);

			// Perform a rotation about the x-axis
			Matrix& RotateX(float theta);

			// Perform a rotation about the y-axis
			Matrix& RotateY(float theta);

			// Perform a rotation about the z-axis
			Matrix& RotateZ(float theta); 

			// Perform a scale
			Matrix& Scale(float x, float y, float z);
			Matrix& Scale(const Vector3& v);

			// Perform a translation
			Matrix& Translate(float x, float y, float z);
			Matrix& Translate(const Vector3& v);


			// Scalar multiplication
			Matrix operator*(float f) const;
			Matrix& operator*=(float f);


			// Multiply
			Matrix& Mult(const Matrix& m);
			Matrix operator*(const Matrix& m) const;
			Matrix& operator*=(const Matrix& m);

			// Transpose the MatrixSSE
			Matrix& Transpose();

			// Get the transpose of this MatrixSSE
			Matrix GetTranspose() const;

			// Invert an orthonormal MatrixSSE
			Matrix& InverseOrthonormal();

			// Inverse
			Matrix& Inverse();

			// Get the inverse of this MatrixSSE
			Matrix GetInverse() const;

			// Transform a vector
			Vector4 Transform(const Vector4& v) const;

			// Transform a 3d vector
			Vector3 Transform(const Vector3& v) const;

			// Transform a 3d direction vector
			Vector3 TransformNormal(const Vector3& v) const;


			// Auto cast to float pointer
			operator float*();

			// Auto cast to float pointer
			operator const float*() const;

			// Print to console
			void Print();

			// Transpose the orientation part
			Matrix& TransposeOrientation();

			// Orient the MatrixSSE to face the given point
			Matrix& LookAt(const Vector3& point);
			Matrix& LookAt(const Vector3& position, const Vector3& point);
			Matrix& LookAt(float x, float y, float z);

			// Build a perspective projection MatrixSSE
			Matrix& Perspective( float fovY, float aspect, float zNear, float zFar );

			// Make an orthographic MatrixSSE
			Matrix& Orthographic(float width, float height, float nearZ, float farZ);

			// Create an arbitrary rotation
			Matrix& CameraMatrixSSE(const Vector3& position, float thetaX, float thetaY);

			// Element access
			float& operator()(int32 x, int32 y);

			// Const element access
			float operator()(int32 x, int32 y) const;

			// Get the position component
			const Vector3& GetPosition() const;

		private:

			// Data
			union
			{

				// Array access
				float _mat[4][4];

				// Axis access
				struct 
				{
					Vector3 _right; float _padx;
					Vector3 _up; float _pady;
					Vector3 _forward; float _padz;
					Vector3 _position; float _padw;
				};

				// Axis 4d
				struct 
				{
					Vector4 _xAxis;
					Vector4 _yAxis;
					Vector4 _zAxis;
					Vector4 _wAxis;
				};

				// Axis array
				struct  
				{
					Vector4 _axes[4];
				};

				// Numerical access
				struct
				{
					float _11, _12, _13, _14;
					float _21, _22, _23, _24;
					float _31, _32, _33, _34;
					float _41, _42, _43, _44;
				};

				// SSE
				struct  
				{
					__m128 _vecX;
					__m128 _vecY;
					__m128 _vecZ;
					__m128 _vecW;
				};

				// Array sse
				__m128 _vec[4];
			};
		};

	}
}

#include "MatrixSSE.inl"

#else

namespace Maoli
{
	namespace SSE
	{
		typedef ::Maoli::Matrix Matrix;
	}
}

#endif
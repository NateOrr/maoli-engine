//--------------------------------------------------------------------------------------
// File: Vector4.h
//
// 4D Vector4
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Vector3.h"

namespace Maoli
{
	// 4D Vector
	class Vector4
	{
	public:
		// Ctor
		Vector4();

		// Ctor
		Vector4(float x, float y, float z, float w = 1.0f);

		// Ctor
		explicit Vector4( const Vector3& v, float w = 1.0f);

		// Copy Ctor
		Vector4( const Vector4& v );

		// Move Ctor
		Vector4(Vector4&& v);

		// Assignment operator
		Vector4& operator=(const Vector4& v);

		// Move assignment operator
		Vector4& operator=(Vector4&&);

		// Negation
		Vector4 operator-() const;

		// Vector 3 assignment
		Vector4& operator=(const Vector3& v);

		// Convert to 3d vector
		Vector3& AsVector3();
		const Vector3& AsVector3() const;

		// Fill the data
		void Set(float x, float y, float z, float w = 1.0f);

		// Get the 3d component
		void Load3d(Vector3& v);

		// Auto cast to float pointer
		operator float*();

		// Auto convert to vector3
		operator Vector3();

		// Bracket access
		float& operator[](int32 i);

		// Constant bracket access
		float operator[](int32 i) const;

		// Subtraction
		Vector4  operator- (const Vector4& v) const;
		Vector4& operator-=(const Vector4& v);
		Vector4  operator- (const Vector3& v) const;
		Vector4& operator-=(const Vector3& v);

		// Addition
		Vector4  operator+ (const Vector4& v) const;
		Vector4& operator+=(const Vector4& v);
		Vector4  operator+ (const Vector3& v) const;
		Vector4& operator+=(const Vector3& v);

		// Scalar multiplication
		Vector4  operator* (float f) const;
		Vector4& operator*=(float f);

		// Scalar division
		Vector4  operator/ (float f) const;
		Vector4& operator/=(float f);

		// Comparison
		bool operator==(const Vector4& v) const;
		bool operator!=(const Vector4& v) const;

		// Dot product
		float Dot(const Vector4& v) const;
		float Dot(float f) const;
		float Dot3(const Vector4& v) const;
		float Dot3(const Vector3& v) const;

		// Cross product of the 3d components
		Vector4 Cross(const Vector4& v) const;
		Vector4 Cross(const Vector3& v) const;

		// Piecewise multiplication
		Vector4 ComponentMult(const Vector4& v) const;

		// Length
		float Length() const;

		// Length of the 3d component
		float Length3d() const;

		// Normalize the vector
		Vector4& Normalize();

		// Normalize the 3d component
		Vector4& Normalize3d();

		// Max vector
		static Vector4 Max(const Vector4& a, const Vector4& b);

		// Min vector
		static Vector4 Min(const Vector4& a, const Vector4& b);

		union
		{
			// Elements
			struct
			{	
				float x,y,z,w;
			};

			// Array
			float _f[4];
		};
	};


}

#include "Vector4.inl"

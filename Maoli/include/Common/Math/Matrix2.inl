//--------------------------------------------------------------------------------------
// File: Matrix2.inl
//
// 2x2 matrix
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	inline Matrix2::Matrix2() : _11( 1 ), _12( 0 ),	_21( 0 ), _22( 1 ) 
	{

	}


	// Ctor
	inline Matrix2::Matrix2( float m11, float m12,
		float m21, float m22 ) : _11( m11 ), _12( m12 ), _21( m21 ), _22( m22 )
	{

	}

	// Copy Ctor
	inline Matrix2::Matrix2( const Matrix2& m ) : _11( m._11 ), _12( m._12 ), _21( m._21 ), _22( m._22 )
	{

	}

	// Assignment op
	inline Matrix2& Matrix2::operator=(const Matrix2& m)
	{
		memcpy( this, &m, 16 );
		return *this;
	}


	// Move Ctor
	inline Matrix2::Matrix2( Matrix2&& m ) : _11( m._11 ), _12( m._12 ), _21( m._21 ), _22( m._22 )
	{

	}

	// Fill the matrix
	inline Matrix2& Matrix2::Set( float m11, float m12,
		float m21, float m22 )
	{
		_11 = m11;  _12 = m12;
		_21 = m21;  _22 = m22;
		return *this;
	}

	// Scalar multiplication
	inline Matrix2 Matrix2::operator*(float f) const
	{
		Matrix2 m;
		m._axes[0] = _axes[0] * f;
		m._axes[1] = _axes[1] * f;
		return m;
	}

	// Scalar multiplication
	inline Matrix2& Matrix2::operator*=(float f)
	{
		_axes[0] *= f;
		_axes[1] *= f;
		return *this;
	}

	// Auto cast to float pointer
	inline Matrix2::operator float*()
	{
		return (float*)this;
	}

	// Auto cast to float pointer
	inline Matrix2::operator const float*() const
	{
		return (float*)this;
	}


	// Make an identity matrix
	inline Matrix2& Matrix2::Identity()
	{
		return Set( 1, 0, 0, 1 );
	}

	// Make the zero matrix
	inline Matrix2& Matrix2::Zero()
	{
		return Set( 0, 0, 0, 0 );
	}

	// Transform a vector
	inline Vector2 Matrix2::Transform( const Vector2& v ) const
	{
		return Vector2( _11*v.x + _12*v.y, _21*v.x + _22*v.y );
	}


	// Multiply
	inline Matrix2& Matrix2::Mult( const Matrix2& m )
	{
		return *this = *this * m;
	}

	// Multiply
	inline Matrix2 Matrix2::operator*(const Matrix2& m) const
	{
		return Matrix2(
			_11*m._11 + _12*m._21, _11*m._12 + _12*m._22,
			_21*m._11 + _22*m._21, _21*m._12 + _22*m._22 );
	}

	// Multiply
	inline Matrix2& Matrix2::operator*=(const Matrix2& m)
	{
		return Mult( m );
	}

	// Transpose the matrix
	inline Matrix2& Matrix2::Transpose()
	{
		return *this = Matrix2(
			_11, _21, 
			_12, _22 );
	}

	// Get the transpose of this matrix
	inline Matrix2 Matrix2::GetTranspose() const
	{
		return Matrix2(
			_11, _21, 
			_12, _22 );
	}


	// Element access
	inline float& Matrix2::operator()( int32 x, int32 y )
	{
		return _mat[x][y];
	}

	// Const element access
	inline float Matrix2::operator()( int32 x, int32 y ) const
	{
		return _mat[x][y];
	}

}

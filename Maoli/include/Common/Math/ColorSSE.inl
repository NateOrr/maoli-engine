//--------------------------------------------------------------------------------------
// File: ColorSSE.inl
//
// RGBA Color using SSE
//
// Nate Orr
//--------------------------------------------------------------------------------------

#include <algorithm>

namespace Maoli
{
	namespace SSE
	{
		// Ctor
		inline Color::Color() : r(0), g(0), b(0), a(1.0f)
		{

		}

		// Ctor
		inline Color::Color( float vx, float vy, float vz, float vw /*= 1.0f*/ ) : r(vx), g(vy), b(vz), a(vw)
		{

		}

		// Ctor
		inline Color::Color( __m128 vec ) : _vec(vec)
		{

		}

		// Move Ctor
		inline Color::Color(Color&& v) : r(v.r), g(v.g), b(v.b), a(v.a)
		{

		}

		// Move assignment operator
		inline Color& Color::operator=(Color&& v)
		{
			r=v.r; g=v.g; b=v.b; a=v.a;
			return *this;
		}


		// Fill the data
		inline void Color::Set( float r, float g, float b, float a /*= 1.0f*/ )
		{
			this->r=r; 
			this->g=g; 
			this->b=b; 
			this->a=a;
		}

		// Bracket access
		inline float& Color::operator[](int32 i)
		{
			return _f[i];
		}

		// Constant bracket access
		inline float Color::operator[](int32 i) const
		{
			return _f[i];
		}


		// Auto cast to float pointer
		inline Color::operator float*()
		{
			return (float*)this;
		}

		// Subtraction
		inline Color Color::operator-(const Color& v)
		{
			return _mm_sub_ps(_vec, v._vec);
		}

		// Subtraction
		inline Color& Color::operator-=(const Color& v)
		{
			_vec = _mm_sub_ps(_vec, v._vec);
			return *this;
		}

		// Addition
		inline Color Color::operator+(const Color& v)
		{
			return _mm_add_ps(_vec, v._vec);
		}

		// Addition
		inline Color& Color::operator+=(const Color& v)
		{
			_vec = _mm_add_ps(_vec, v._vec);
			return *this;
		}


		// Scalar multiplication
		inline Color Color::operator*(float f)
		{
			return _mm_mul_ps(_vec, _mm_set_ps1(f));
		}


		// Scalar multiplication
		inline Color& Color::operator*=(float f)
		{
			_vec = _mm_mul_ps(_vec, _mm_set_ps1(f));
			return *this;
		}


		// Scalar division
		inline Color Color::operator/(float f)
		{
			return _mm_mul_ps(_vec, _mm_set_ps1(1.0f/f));
		}


		// Scalar division
		inline Color& Color::operator/=(float f)
		{
			_vec = _mm_mul_ps(_vec, _mm_set_ps1(1.0f/f));
			return *this;
		}

		// Piecewise multiplication
		inline Color& Color::operator*=(const Color& v)
		{
			_vec = _mm_mul_ps(_vec, v._vec);
			return *this;
		}

		// Piecewise multiplication
		inline Color Color::operator*(const Color& v)
		{
			return _mm_mul_ps(_vec, v._vec);
		}


		// Clamp to 0-1 range
		inline void Color::Saturate()
		{
			r = std::max( std::min(r, 1.0f), 0.0f);
			g = std::max( std::min(g, 1.0f), 0.0f);
			b = std::max( std::min(b, 1.0f), 0.0f);
			a = std::max( std::min(a, 1.0f), 0.0f);
		}
	}

}

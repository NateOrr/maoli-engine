//--------------------------------------------------------------------------------------
// File: NavMesh.h
//
// Navigation mesh for pathfinding
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "NavMesh.h"
#include "Geometry.h"

#include "Platform/Platform.h"
#include "Engine\Engine.h"

namespace Maoli
{
	// Ctor
	NavMesh::NavMesh( Engine* engine ) : Component( engine ), _open( &NavMesh::CompareNodes )
	{
		_heuristicWeight = 1.3f;
	}

	// Dtor
	NavMesh::~NavMesh()
	{

	}

	// Load geometry
	bool NavMesh::Load( const char* file )
	{
		Geometry* geometry = Geometry::CreateFromFile( file, _engine );
		if ( geometry != nullptr )
		{
			// Build the triangle list
			_verts.FromArray( geometry->GetVerts(), geometry->GetNumVerts() );
			_faces.FromArray( geometry->GetIndices(), geometry->GetNumIndices() );

			// Build the triangle node set
			const uint32* faces = _faces;
			_triangles.Allocate( geometry->GetNumFaces() );
			for ( uint32 triIndex = 0; triIndex < _triangles.Size(); ++triIndex, faces += 3 )
			{
				// Link edges
				_triangles[triIndex].edges[0].verts[0] = faces[0];
				_triangles[triIndex].edges[0].verts[1] = faces[1];
				_triangles[triIndex].edges[1].verts[0] = faces[1];
				_triangles[triIndex].edges[1].verts[1] = faces[2];
				_triangles[triIndex].edges[2].verts[0] = faces[2];
				_triangles[triIndex].edges[2].verts[1] = faces[0];

				// Compute center
				_triangles[triIndex].center = (_verts[faces[0]] + _verts[faces[1]] + _verts[faces[2]]) / 3.0f;

				// Compute adjacency
				for ( uint32 edgeIndex = 0; edgeIndex < 3; ++edgeIndex )
				{
					Edge& edge = _triangles[triIndex].edges[edgeIndex];
					const uint32* otherFaces = _faces;
					for ( uint32 otherTriIndex = 0; otherTriIndex < _triangles.Size(); ++otherTriIndex, otherFaces += 3 )
					{
						if ( triIndex != otherTriIndex )
						{
							if ( (edge.verts[0] == otherFaces[0] || edge.verts[0] == otherFaces[1] || edge.verts[0] == otherFaces[2]) &&
								(edge.verts[1] == otherFaces[0] || edge.verts[1] == otherFaces[1] || edge.verts[1] == otherFaces[2]) )
							{
								edge.next = &_triangles[otherTriIndex];
								break;
							}
						}
					}
				}
			}

			// Cleanup
			Geometry::Delete( geometry );

			return true;
		}
		return false;
	}

	// Find a path between two points
	// Returns false if either point is outside the nav mesh
	bool NavMesh::FindPath( const Vector3& start, const Vector3& end, List<Vector3>& path )
	{
		// Find the start and end triangles
		bool foundStart = false, foundEnd = false;
		uint32 startIndex, endIndex;
		uint32* face = _faces;
		_goal = end;
		_start = start;
		for ( uint32 i = 0; (i < _faces.Size()) && !(foundStart&&foundEnd); i += 3, face += 3 )
		{
			// Start point
			if ( !foundStart && Math::PointInTriangle( start, _verts[face[0]], _verts[face[1]], _verts[face[2]] ) )
			{
				foundStart = true;
				startIndex = i / 3;
			}

			// End point
			if ( !foundEnd && Math::PointInTriangle( end, _verts[face[0]], _verts[face[1]], _verts[face[2]] ) )
			{
				foundEnd = true;
				endIndex = i / 3;
			}
		}

		// Check for valid path
		path.Clear();
		if ( !foundStart || !foundEnd )
			return false;

		// Handle general case
		_goalIndex = endIndex;
		if ( startIndex != endIndex )
		{
			// Clear data
			_open.Clear();
			_visited.clear();

			// Setup the queue
			PlannerNode* firstNode = new PlannerNode;
			firstNode->givenCost = 0;
			firstNode->finalCost = EstimateCost( start, end );
			firstNode->triangle = &_triangles[startIndex];
			firstNode->parent = nullptr;
			firstNode->refCount = 1;
			firstNode->entryEdge = &_triangles[startIndex].edges[ClosestEdgeToPoint( _triangles[startIndex], _goal )];
			_open.Enqueue( firstNode );
			_visited.emplace( firstNode->triangle, firstNode );

			// Start A*
			Array<PlannerNode*> trash;
			while ( !_open.IsEmpty() )
			{
				// Grab the next tile to work with
				PlannerNode* node = _open.Dequeue();
				trash.Add( node );

				// Check for end
				if ( node->triangle != &_triangles[_goalIndex] )
				{
					// Process adjacent verts
					for ( uint32 i = 0; i < 3; ++i )
					{
						if ( node->triangle->edges[i].next )
							ProcessNode( node->triangle->edges[i].next, node->triangle->edges[i], node );
					}
				}
				else
				{
					_badPath.Clear();
					_badPath.AddToFront( _goal );
					for ( PlannerNode* n = node; n; n = n->parent )
					{
						_badPath.AddToFront( (_verts[n->entryEdge->verts[0]] + _verts[n->entryEdge->verts[1]])*0.5f );
					}
					_badPath.AddToFront( _start );

					// Build the solution path
					FunnelPath( node, path );

					// Cleanup
					while ( !_open.IsEmpty() )
						trash.Add( _open.Dequeue() );
					for ( uint32 i = 0; i < trash.Size(); ++i )
					if ( --trash[i]->refCount == 0 )
						delete trash[i];

					return true;
				}
			}
		}

		// Handle case inside the same triangle
		else
		{
			path.AddToFront( end );
			path.AddToFront( start );
		}

		return true;
	}

	// Get the cost estimate between two nodes
	float NavMesh::EstimateCost( const Vector3& a, const Vector3& b ) const
	{
		return (b - a).Length();
	}

	// Process a successor node
	void NavMesh::ProcessNode( Triangle* triangle, Edge& entryEdge, PlannerNode* parent )
	{
		// Compute costs
		const Vector3 entryCenter = (_verts[entryEdge.verts[0]] + _verts[entryEdge.verts[1]])*0.5f;
		const Vector3 parentEntryCenter = (_verts[parent->entryEdge->verts[0]] + _verts[parent->entryEdge->verts[1]])*0.5f;
		float heuristicCost = EstimateCost( entryCenter, _goal );
		float newGivenCost = parent->givenCost + EstimateCost( entryCenter, parentEntryCenter );
		float finalCost = newGivenCost + heuristicCost * _heuristicWeight;

		auto iter = _visited.find( triangle );
		if ( iter == _visited.end() )
		{
			// Create a new node
			PlannerNode* node = new PlannerNode;
			node->parent = parent;
			node->heuristicCost = heuristicCost;
			node->givenCost = newGivenCost;
			node->finalCost = finalCost;
			node->triangle = triangle;
			node->entryEdge = &entryEdge;
			node->refCount = 1;
			_open.Enqueue( node );
			_visited.emplace( triangle, node );
		}
		else if ( iter->second->givenCost > newGivenCost )
		{
			// Update the existing node
			iter->second->givenCost = newGivenCost;
			iter->second->heuristicCost = heuristicCost;
			iter->second->finalCost = finalCost;
			iter->second->entryEdge = &entryEdge;
			iter->second->parent = parent;
			++iter->second->refCount;
			_open.Enqueue( iter->second );
		}
	}

	// Get the closest edge to a point
	uint32 NavMesh::ClosestEdgeToPoint( Triangle& triangle, const Vector3& point )
	{
 		float d1 = Math::PointToEdgeDistance( point, _verts[triangle.edges[0].verts[0]], _verts[triangle.edges[0].verts[1]] );
 		float d2 = Math::PointToEdgeDistance( point, _verts[triangle.edges[1].verts[0]], _verts[triangle.edges[1].verts[1]] );
 		float d3 = Math::PointToEdgeDistance( point, _verts[triangle.edges[2].verts[0]], _verts[triangle.edges[2].verts[1]] );
		float e1 = EstimateCost( point, (_verts[triangle.edges[0].verts[0]] + _verts[triangle.edges[0].verts[1]])*0.5f );
		float e2 = EstimateCost( point, (_verts[triangle.edges[1].verts[0]] + _verts[triangle.edges[1].verts[1]])*0.5f );
		float e3 = EstimateCost( point, (_verts[triangle.edges[2].verts[0]] + _verts[triangle.edges[2].verts[1]])*0.5f );

		float d = std::min( std::min( d1, d2 ), d3 );
		if ( d1 == d )
		{
			// Handle equal distances, just compare edge centers in these cases
			if ( d2 == d )
				return (e1 > e2) ? 1u : 0u;
			if ( d3 == d )
				return (e1 > e3) ? 2u : 0u;
			return 0u;
		}

		if ( d2 == d )
		{
			// Handle equal distances, just compare edge centers in these cases
			if ( d1 == d )
				return (e1 > e2) ? 1u : 0u;
			if ( d3 == d )
				return (e2 > e3) ? 2u : 1u;
			return 1u;
		}

		// Handle equal distances, just compare edge centers in these cases
		if ( d1 == d )
			return (e1 > e3) ? 2u : 0u;
		if ( d2 == d )
			return (e2 > e3) ? 2u : 1u;
		return 2u;
	}

	// Construct a final path using funneling
	// Note: this is done in reverse
	void NavMesh::FunnelPath( PlannerNode* root, List<Vector3>& path )
	{
		// Build the proper list order
		PlannerNode* endNode = root;
		root->child = nullptr;
		PlannerNode* node = root;
		while(true)
		{
			if ( node->parent == nullptr )
			{
				root = node;
				break;
			}
			node->parent->child = node;
			node = node->parent;
		}
		
		// Setup
		Vector3 portalApex = _start;
		Vector3 portalLeft = _start;
		Vector3 portalRight = _start;
		PlannerNode* rightNode = root;
		PlannerNode* leftNode = root;

		// Process each edge in the original path
		for ( node = root->child; node; node = node->child )
		{
			// Get the next edge
			const Vector3& left = _verts[node->entryEdge->verts[1]];
			const Vector3& right = _verts[node->entryEdge->verts[0]];

			// Update right vertex
			if ( Math::TriangleWinding( portalApex, portalRight, right ) <= 0.0f )
			{
				if ( (portalApex - portalRight).LengthSquared( )<Math::epsilon2 || Math::TriangleWinding( portalApex, portalLeft, right ) > 0.0f )
				{
					// Tighten the funnel
					portalRight = right;
					rightNode = node;
				}
				else
				{
					// Right over left, insert left to path and restart scan from portal left point
					path.Add( portalLeft );

					// Reset portal
					portalRight = portalApex = portalLeft;
					rightNode = leftNode;

					// Restart scan
					node = leftNode;
					continue;
				}
			}

			// Update left vertex
			if ( Math::TriangleWinding( portalApex, portalLeft, left ) >= 0.0f )
			{
				if ( (portalApex - portalLeft).LengthSquared( ) < Math::epsilon2 || Math::TriangleWinding( portalApex, portalRight, left ) < 0.0f )
				{
					// Tighten the funnel
					portalLeft = left;
					leftNode = node;
				}
				else
				{
					// Left over right, insert right to path and restart scan from portal right point
					path.Add( portalRight );

					// Reset portal
					portalLeft = portalApex = portalRight;
					leftNode = rightNode;

					// Restart scan
					node = rightNode;
					continue;
				}
			}
		}

		// Add the starting and ending locations
		path.AddToFront( _start );
		path.Add( _goal );
	}

	// Per-frame logic
	void NavMesh::Update( float dt )
	{

	}

	// Binary serialize
	void NavMesh::Export( std::ofstream& fout )
	{

	}

	// Binary file import
	void NavMesh::Import( std::ifstream& fin )
	{

	}

	// Deep copy clone is required for all components
	Component* NavMesh::Clone( Component* dest /*= nullptr */ ) const
	{
		return nullptr;
	}

}

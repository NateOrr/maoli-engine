//--------------------------------------------------------------------------------------
// File: Material.h
//
// Surface properties
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Util/ISerializable.h"

namespace Maoli
{
	// Forward decl
	class Engine;

	// Texture types
	enum TEX_TYPE
	{
		TEX_DIFFUSE = 0,
		TEX_NORMAL,
		TEX_SPECULAR,
		TEX_HEIGHT,
		TEX_ALPHA,
		TEX_EMISSIVE,

		TEX_SIZE,	// Only used for number of texture types
	};

	// Surface properties
	class Material : public ISerializable, public DataType<Material, Engine>
	{
		friend DataType<Material, Engine>;
		friend class MaterialBinding;

	public:

		// Ctor
		Material( Engine* engine );

		// Dtor
		virtual ~Material();

		// Check for alpha testing
		virtual bool IsAlphaTested() { return _alphaTesting; }

		// Enable embedded alpha testing
		void EnableAlphaTesting( bool value );

		// Load all textures
		virtual void ImportTextures() {}

		// Read from binary file
		bool Load( const char* file);

		// Write to binary file
		void Save( const char* file = nullptr );

		// Move textures files to the directory
		void MoveTextures( const char* dir );

		// Get the roughness factor
		inline float GetRoughness() { return _properties.roughness; }

		// Set the roughness factor
		inline void SetRoughness( float f ) { _properties.roughness = f; }

		// Get the metalic factor
		inline float GetMetallic() { return _properties.metalic; }

		// Set the metalic factor
		inline void SetMetallic( float f ) { _properties.metalic = f; }

		// Transparency
		inline float GetTransparency() { return _properties.transparency; }
		void SetTransparency( float f );

		// Base color
		inline const Vector3& GetBaseColor() const { return _properties.baseColor; }
		inline void SetBaseColor( const Vector3& vec ) { _properties.baseColor = vec; }
		inline void SetBaseColor( float r, float g, float b ) { _properties.baseColor = Vector3( r, g, b ); }

		// Emissive glow color
		inline const Vector4& GetEmissive() const { return _properties.emissive; }
		inline void SetEmissive( const Vector4& vec ) { _properties.emissive = vec; }
		inline void SetEmissive( float r, float g, float b, float a ) { _properties.emissive = Vector4( r, g, b, a ); }

		// Texture flags
		inline const uint32 GetTextureFlag() const { return _properties.textureFlag.GetBits(); }
		inline bool GetTextureFlags( int32 i ) { return _properties.textureFlag[i]; }

		// Set a texture name
		bool SetTextureName( const char* fileName, TEX_TYPE type );

		// Transparent
		inline bool IsTransparent() const { return (_properties.transparency > 0.0f); }

		// Two sided transparent
		inline bool TwoSidedTransparency() const { return _twoSided; }

		// Enable/disable two sided transparency
		inline void EnableTwoSidedTransparency( bool value ) { _twoSided = value; }

		// Get a pointer to the data to be sent to the constant buffer
		inline const void* GetShaderData() const { return &_properties; }

		// Get the size of the shader data
		inline uint32 GetShaderDataSize() const { return sizeof(_properties); }

		// Get the texture file names
		inline const String* GetTextureFileNames() const { return _textureFiles; }

		// Check for refraction
		inline bool IsRefractive() const { return _refractive; }

		// Enable refraction
		inline void EnableRefraction( bool value ) { _refractive = value; }

		// Clone this material, with a new name
		Material* Clone( const char* newName );

	
	protected:

		// Write to binary file
		virtual void Serialize( std::ofstream& file ) const;

		// Read from binary file
		virtual void Deserialize( std::ifstream& file );

		String			_textureFiles[TEX_SIZE];	// Texture file names
		bool			_twoSided;					// Two-sided transparency
		bool			_refractive;				// Cubemap refraction
		bool			_alphaTesting;				// True for alpha testing
		

		// Constant buffer data for the shader
		struct cbData
		{
			Vector3		baseColor;			// Material base color
			float		transparency;		// Transparency factor
			Vector4		emissive;			// Emissive lighting component (glow) (alpha channel is strength)
			float       roughness;			// Roughness factor for the specular BRDF
			float		metalic;			// Metalic factor
			Bitfield32  textureFlag;		// Texture flag bitfield
		} _properties;

	};
}

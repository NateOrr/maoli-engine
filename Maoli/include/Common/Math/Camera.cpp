//--------------------------------------------------------------------------------------
// File: Camera.cpp
//
// First person camera
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Camera.h"

#include "Game/Entity.h"
#include "Physics/RigidBody.h"

namespace Maoli
{
	// Reset to default state
	void Camera::Reset()
	{
		_matrix.Identity();
		_theta = _omega = Vector3();
		_isDirty = true;
		_velocity = Vector3();
		_viewMatrix.Identity();
		_focusPoint = Vector3();
		_fov = Math::pi / 3.0f;
		_farZ = 500.0f;
		_nearZ = 0.1f;
		_type = CameraType::FreeRoam;
	}

	// Initializes a camera object
	void Camera::BuildProjectionMatrix( int32 width, int32 height )
	{
		// Build the projection matrix
		_viewWidth = width;
		_viewHeight = height;
		_fovRatio = (float)width / (float)height;
		_nearPlaneHeight = 2.0f * tanf( _fov / 2.0f ) *_nearZ;
		_nearPlaneWidth = _nearPlaneHeight * _fovRatio;
		_farPlaneHeight = 2.0f * tan( _fov / 2.0f ) * _farZ;
		_farPlaneWidth = _farPlaneHeight * _fovRatio;
		_projMatrix.Perspective( _fov, _fovRatio, _nearZ, _farZ );
	}


	// Binary serialize
	void Camera::Export( std::ofstream& fout )
	{
		Transform::Export( fout );
		Maoli::Serialize( fout, &_viewMatrix );
		Maoli::Serialize( fout, &_projMatrix );
		Maoli::Serialize( fout, &_farZ );
		Maoli::Serialize( fout, &_nearZ );
		Maoli::Serialize( fout, &_fov );
		Maoli::Serialize( fout, &_fovRatio );
		Maoli::Serialize( fout, &_nearPlaneHeight );
		Maoli::Serialize( fout, &_nearPlaneWidth );
		Maoli::Serialize( fout, &_farPlaneHeight );
		Maoli::Serialize( fout, &_farPlaneWidth );
		Maoli::Serialize( fout, &_focusZoom );
		Maoli::Serialize( fout, &_viewWidth );
		Maoli::Serialize( fout, &_viewHeight );
		for ( int32 i = 0; i < 8; ++i )
			Maoli::Serialize( fout, &_frustumCorners[i] );
		Maoli::Serialize( fout, &_focusPoint );
		Maoli::Serialize( fout, &_frustum );
		Maoli::Serialize( fout, &_type );
	}

	// Binary file import
	void Camera::Import( std::ifstream& fin )
	{
		Transform::Import( fin );
		Maoli::Deserialize( fin, &_viewMatrix );
		Maoli::Deserialize( fin, &_projMatrix );
		Maoli::Deserialize( fin, &_farZ );
		Maoli::Deserialize( fin, &_nearZ );
		Maoli::Deserialize( fin, &_fov );
		Maoli::Deserialize( fin, &_fovRatio );
		Maoli::Deserialize( fin, &_nearPlaneHeight );
		Maoli::Deserialize( fin, &_nearPlaneWidth );
		Maoli::Deserialize( fin, &_farPlaneHeight );
		Maoli::Deserialize( fin, &_farPlaneWidth );
		Maoli::Deserialize( fin, &_focusZoom );
		Maoli::Deserialize( fin, &_viewWidth );
		Maoli::Deserialize( fin, &_viewHeight );
		for ( int32 i = 0; i < 8; ++i )
			Maoli::Deserialize( fin, &_frustumCorners[i] );
		Maoli::Deserialize( fin, &_focusPoint );
		Maoli::Deserialize( fin, &_frustum );
		Maoli::Deserialize( fin, &_type );
	}

	// Deep copy clone is required for all components
	Component* Camera::Clone( Component* dest ) const
	{
		Camera* clone = dest ? (Camera*)dest : new Camera( _engine );
		Transform::Clone( clone );
		clone->_viewMatrix = _viewMatrix;
		clone->_projMatrix = _projMatrix;
		clone->_farZ = _farZ;
		clone->_nearZ = _nearZ;
		clone->_fov = _fov;
		clone->_fovRatio = _fovRatio;
		clone->_nearPlaneHeight = _nearPlaneHeight;
		clone->_nearPlaneWidth = _nearPlaneWidth;
		clone->_farPlaneHeight = _farPlaneHeight;
		clone->_farPlaneWidth = _farPlaneWidth;
		clone->_focusZoom = _focusZoom;
		clone->_viewWidth = _viewWidth;
		clone->_viewHeight = _viewHeight;
		clone->_focusPoint = _focusPoint;
		clone->_frustum = _frustum;
		clone->_type = _type;
		for ( int32 i = 0; i < 8; ++i )
			clone->_frustumCorners[i] = _frustumCorners[i];

		return clone;
	}


	// Respond to position changed message
	void Camera::OnPositionChanged( Component* sender, void* userData )
	{
		UpdateFrustum();
	}

	// Register for any events that need to be handled
	void Camera::RegisterEventCallbacks()
	{
		REGISTER_EVENT_CALLBACK( ComponentEvent::TransformChanged, &Camera::OnPositionChanged );
		REGISTER_EVENT_CALLBACK( ComponentEvent::PhysicsTransformChanged, &Camera::OnPhysicsUpdate );
	}

	// Per-frame logic
	void Camera::Update( float dt )
	{
		// Update view angles
		if ( GetAngularVelocity().LengthSquared() )
		{
			AddToRotation( GetAngularVelocity() * dt );

			// Velocity damping
			SetAngularVelocity( GetAngularVelocity()*GetAngularVelocityDamping() );
		}

		// Update position
		if ( GetVelocity().LengthSquared() )
		{
			AddToPosition( GetVelocity() * dt );

			// Velocity damping
			SetVelocity( GetVelocity()*GetLinearVelocityDamping() );
		}

		// Clamp the view angles within the unit circle
		if ( IsDirty() )
		{
			// Build the orientation matrix
			ClampViewAngles();
			UpdateOrientation();

			// Notify other components
			_entity->BroadcastMessage( ComponentEvent::TransformChanged, this, nullptr );
		}
	}


	// Physics update callback
	void Camera::OnPhysicsUpdate( Component* sender, void* userData )
	{
		// Build the new camera matrix from the rigid body position
		RigidBody* body = (RigidBody*)sender;
		SetPosition( body->GetPosition() );
		UpdateOrientation();
		SetVelocity( body->GetLinearVelocity() );

		// Let other components know the transform was updated
		_entity->BroadcastMessage( ComponentEvent::TransformChanged, this, nullptr );
	}

	// Update the view frustum
	void Camera::UpdateFrustum()
	{
		// Construct the view matrix
		_viewMatrix = GetOrientationMatrix().GetInverse();

		// Build the frustum
		_frustum.Build( _viewMatrix, _projMatrix );

		// Compute the far plane corners
		Vector3 fc = _farZ*Vector3( 0, 0, 1 );
		Vector3 up = Vector3( 0, _farPlaneHeight / 2.0f, 0 );
		Vector3 right = Vector3( _farPlaneWidth / 2.0f, 0, 0 );
		_frustumCorners[(int32)FrustrumCorner::FarTopLeft] = fc + up - right;
		_frustumCorners[(int32)FrustrumCorner::FarTopRight] = fc + up + right;
		_frustumCorners[(int32)FrustrumCorner::FarBottomLeft] = fc - up - right;
		_frustumCorners[(int32)FrustrumCorner::FarBottomRight] = fc - up + right;

		// Compute the near plane corners
		fc = _nearZ*Vector3( 0, 0, 1 );
		up = Vector3( 0, _nearPlaneHeight / 2.0f, 0 );
		right = Vector3( _nearPlaneWidth / 2.0f, 0, 0 );
		_frustumCorners[(int32)FrustrumCorner::NearTopLeft] = fc + up - right;
		_frustumCorners[(int32)FrustrumCorner::NearTopRight] = fc + up + right;
		_frustumCorners[(int32)FrustrumCorner::NearBottomLeft] = fc - up - right;
		_frustumCorners[(int32)FrustrumCorner::NearBottomRight] = fc - up + right;
	}

}

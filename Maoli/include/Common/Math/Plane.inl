//--------------------------------------------------------------------------------------
// File: Plane.inl
//
// 3D plane
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{
	// Ctor
	inline Plane::Plane() : d( 0 )
	{

	}

	// Cpoy ctor
	inline Plane::Plane( const Plane& p ) : a( p.a ), b( p.b ), c( p.c ), d( p.d )
	{

	}

	// Move ctor
	inline Plane::Plane( Plane&& p ) : a( p.a ), b( p.b ), c( p.c ), d( p.d )
	{

	}

	// Construct from points
	inline Plane::Plane( const Vector3& a, const Vector3& b, const Vector3& c )
	{
		Normal() = (b - a).Cross( c - a ).Normalize();
		d = -Normal().Dot( a );
	}

	// Construct from a Normal() and a point
	inline Plane::Plane( const Vector3& n, const Vector3& p ) : a( n.x ), b( n.y ), c( n.z ), d( -n.Dot( p ) )
	{

	}

	// Assignment op
	inline Plane& Plane::operator=(const Plane& p)
	{
		a = p.a;
		b = p.b;
		c = p.c;
		d = p.d;
		return *this;
	}

	// Move Assignment op
	inline Plane& Plane::operator=(Plane&& p)
	{
		a = p.a;
		b = p.b;
		c = p.c;
		d = p.d;
		return *this;
	}

	// Construct from points
	inline Plane& Plane::FromPoints( const Vector3& a, const Vector3& b, const Vector3& c )
	{
		Normal() = (b - a).Cross( c - a ).Normalize();
		d = -Normal().Dot( a );
		return *this;
	}

	// Construct from a Normal() and a point
	inline Plane& Plane::FromNormalAndPoint( const Vector3& n, const Vector3& p )
	{
		Normal() = n;
		d = -Normal().Dot( p );
		return *this;
	}

	// Normalize the plane
	inline Plane& Plane::Normalize()
	{
		float l = 1.0f / Normal().Length();
		Normal() *= l;
		d *= l;
		return *this;
	}

	// Dot with a vector
	inline float Plane::Dot( const Vector3& v ) const
	{
		return GetNormal().Dot( v );
	}


	// Coord dot
	inline float Plane::DotCoord( const Vector3& v ) const
	{
		return GetNormal().Dot( v ) + d;
	}

	// Get the plane Normal()
	inline const Vector3& Plane::GetNormal() const
	{
		return *(Vector3*)this;
	}

}

// Print to console
inline std::ostream& operator<<(std::ostream& os, const Maoli::Plane& plane)
{
	return os << plane.a << " " << plane.b << " " << plane.c << " " << plane.d << std::endl;
}

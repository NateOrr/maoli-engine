//--------------------------------------------------------------------------------------
// File: Transform.h
//
// 3d transform component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Game/Component.h"

namespace Maoli
{
	class Transform : public Component
	{
		friend class Camera;
	public:

		//////////////////////////////////////////////////////
		// Component interface

		// Register as a component
		COMPONENT_DECL( Transform );

		// Ctor
		Transform( Engine* engine );

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Get the typeid for this component. This is used as the key inside an entity component map.
		// This is required to be definied in order for inheritance and unique components to
		// function properly together. Only base components should define this.
		virtual std::type_index GetFamilyID() const { return typeid(Transform); }

		// Returns true if an entity can contain only one component of this type
		virtual bool IsUnique() const { return true; }

		// Register for any events that need to be handled
		virtual void RegisterEventCallbacks();

		// Physics update callback
		void OnPhysicsUpdate( Component* sender, void* userData );



		// Return true if changes have occured
		bool IsDirty() const;

		// Move forward/back
		void MoveForward(float amount);

		// Move left/right
		void MoveRight(float amount);

		// Move up/down
		void MoveUp(float amount);

		// Get the forward orientation vector
		const Vector3& GetForwardVector() const;

		// Get the right orientation vector
		const Vector3& GetRightVector() const;

		// Get the up orientation vector
		const Vector3& GetUpVector() const;

		// Set the position
		void SetPosition(const Vector3& pos);
		void SetPosition(float x, float y, float z);

		// Get the position
		const Vector3& GetPosition() const;

		// Set the view direction
		void SetViewDirection(const Vector3& pos);
		void SetViewDirection(float x, float y, float z);

		// Get the view direction
		const Vector3& GetViewDirection() const;

		// Set the velocity
		void SetVelocity(const Vector3& vel);
		void SetVelocity(float x, float y, float z);

		// Get the velocity
		const Vector3& GetVelocity() const;

		// Sets the rotation about the x axis
		void SetXRotation(const float d);

		// Sets the rotation about the y axis
		void SetYRotation(const float d);

		// Sets the rotation about the y axis
		void SetZRotation( const float d );
		
		// Gets the rotation about the x axis
		const float GetXRotation() const;

		// Gets the rotation about the y axis
		const float GetYRotation() const;

		// Gets the rotation about the z axis
		const float GetZRotation() const;

		// Set the current view angles
		void SetRotation(float x, float y, float z);

		// Set the current view angles
		void SetRotation(const Vector3& r);

		// Get the view angles
		const Vector3& GetRotation() const;

		// Set the current angular velocity
		void SetAngularVelocity(float x, float y, float z);

		// Set the current angular velocity
		void SetAngularVelocity(const Vector3& r);

		// Get the current angular velocity
		const Vector3& GetAngularVelocity() const;


		// Add to the position
		void AddToPosition(const Vector3& pos);
		void AddToPosition(float x, float y, float z);

		// Add to the velocity
		void AddToVelocity(const Vector3& vel);
		void AddToVelocity(float x, float y, float z);

		// Add to the current view angles
		void AddToRotation(const Vector3& r);
		void AddToRotation(float x, float y, float z);

		// Add to the current angular velocity
		void AddToAngularVelocity(const Vector3& r);
		void AddToAngularVelocity(float x, float y, float z);
		

		// Get the orientation matrix
		const Matrix& GetOrientationMatrix() const;
		Matrix& GetOrientationMatrix();
		void SetOrientationMatrix( const Matrix& matrix );

		// Get a billboard matrix
		Matrix GetBillboardMatrix() const;

		// Look at a point
		void LookAt(float x, float y, float z);
		void LookAt(const Vector3& at);
		void LookAt(const Vector3& position, const Vector3& at);

		// Look in a direction
		void LookTo(float x, float y, float z);
		void LookTo(const Vector3& at);
		void LookTo(const Vector3& position, const Vector3& at);

		// Turn towards a target
		void TurnTo( const Vector3& target, float turnRate, float dt );

		// Set the linear velocity damping
		void SetLinearVelocityDamping(float damping);

		// Set the angular velocity damping
		void SetAngularVelocityDamping(float damping);

		// Get the linear velocity damping
		float GetLinearVelocityDamping() const;

		// Get the angular velocity damping
		float GetAngularVelocityDamping() const;

		// Update the orientation matrix
		virtual void UpdateOrientation();

		// Enable / disable camera mode
		void SetCameraMode( bool value );

	protected:

		Matrix		_matrix;					// World matrix
		Vector3		_theta;						// View angles
		Vector3		_omega;						// Angular velocity
		Vector3		_velocity;					// Linear velocity
		bool		_isDirty;					// Dirty flag, true if changes occured during the last frame
		float		_linearVelocityDamping;		// Damping effect on for veloctiy
		float		_angularVelocityDamping;	// Damping effect on for veloctiy
		bool		_cameraMode;				// If true, orientation matrix will be built from rotation
	};
}

#include "Transform.inl"

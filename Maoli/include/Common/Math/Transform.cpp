//--------------------------------------------------------------------------------------
// File: Transform.cpp
//
// 3d transform component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Transform.h"

#include "Game/Entity.h"
#include "Physics/RigidBody.h"

namespace Maoli
{
	// Binary serialize
	void Transform::Export( std::ofstream& fout )
	{
		Maoli::Serialize( fout, &_matrix );
		Maoli::Serialize( fout, &_theta );
		Maoli::Serialize( fout, &_omega );
		Maoli::Serialize( fout, &_velocity );
		Maoli::Serialize( fout, &_linearVelocityDamping );
		Maoli::Serialize( fout, &_angularVelocityDamping );
	}

	// Binary file import
	void Transform::Import( std::ifstream& fin )
	{
		Maoli::Deserialize( fin, &_matrix );
		Maoli::Deserialize( fin, &_theta );
		Maoli::Deserialize( fin, &_omega );
		Maoli::Deserialize( fin, &_velocity );
		Maoli::Deserialize( fin, &_linearVelocityDamping );
		Maoli::Deserialize( fin, &_angularVelocityDamping );
		_isDirty = true;
	}

	// Deep copy clone is required for all components
	Component* Transform::Clone( Component* dest ) const
	{
		Transform* clone = dest ? (Transform*)dest : new Transform( _engine );
		clone->_matrix = _matrix;
		clone->_theta = _theta;
		clone->_omega = _omega;
		clone->_velocity = _velocity;
		clone->_linearVelocityDamping = _linearVelocityDamping;
		clone->_angularVelocityDamping = _angularVelocityDamping;
		clone->_isDirty = true;
		return clone;
	}

	// Register for any events that need to be handled
	void Transform::RegisterEventCallbacks()
	{
		REGISTER_EVENT_CALLBACK( ComponentEvent::PhysicsTransformChanged, &Transform::OnPhysicsUpdate );
	}

	// Physics update callback
	void Transform::OnPhysicsUpdate( Component* sender, void* userData )
	{
		RigidBody* body = (RigidBody*)sender;
		body->GetTransform( _matrix );
		_entity->BroadcastMessage( ComponentEvent::TransformChanged, this, nullptr );
	}

	// Per-frame logic
	void Transform::Update( float dt )
	{
		// Update view angles
		if ( GetAngularVelocity().LengthSquared() )
		{
			AddToRotation( GetAngularVelocity() * dt );

			// Velocity damping
			SetAngularVelocity( GetAngularVelocity()*GetAngularVelocityDamping() );
		}

		// Update position
		if ( GetVelocity().LengthSquared() )
		{
			AddToPosition( GetVelocity() * dt );

			// Velocity damping
			SetVelocity( GetVelocity() * powf( GetLinearVelocityDamping(), dt * 20.0f ) );
		}

		// Clamp the view angles within the unit circle
		if ( IsDirty() )
		{
			// Build the orientation matrix
			if ( _cameraMode )
				UpdateOrientation();

			// Notify other components
			_entity->BroadcastMessage( ComponentEvent::TransformChanged, this, nullptr );
		}
	}

	// Turn towards a target
	void Transform::TurnTo( const Vector3& target, float turnRate, float dt )
	{
		const float _turnEpsilon = 0.001f;
		const float _fineTurnEpsilon = 0.0001f;

		// Turn to face the target
		Vector3 dir = target - GetPosition();
		dir.Normalize();
		float dTheta = dir.Dot( GetRightVector() );
		float theta = GetYRotation();
		if ( fabs( dTheta ) > _turnEpsilon )
		{
			SetYRotation( theta + dTheta*turnRate );
		}
		else if ( fabs( dTheta ) > _fineTurnEpsilon )
		{
			SetYRotation( theta + dTheta*turnRate*0.2f );
		}
	}

	// Set the camera mode
	void Transform::SetCameraMode( bool value )
	{
		_cameraMode = value;
	}

}

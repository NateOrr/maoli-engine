//--------------------------------------------------------------------------------------
// File: Frustum.h
//
// View Frustum
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	// Frustum corders
	enum class FrustrumCorner
	{
		NearTopRight,
		NearTopLeft,
		NearBottomRight,
		NearBottomLeft,
		FarTopRight,
		FarTopLeft,
		FarBottomRight,
		FarBottomLeft,
	};


	// View Frustum
	// Defined by 6 planes; supports basic primitive tests
	class Frustum
	{
	public:

		// Contruct the frustum from view and projection matrices	
		void Build(const Matrix& matView, const Matrix& matProj);

		// Build from a set of points.  The order must start with top left and go clockwise
		void Build(const Vector3& camPos, const Vector3 corners[4], const float minZ, const float maxZ);

		// Build from start and end corner points, starts with top left and goes clockwise
		void Build( const Vector3 nearCorners[4], const Vector3 farCorners[4] );

		// TRUE if the point lies inside the frustum
		bool CheckPoint(const Vector3& vec)  const;

		// TRUE if the cube intersects the frustum
		bool CheckCube(float XCenter, float YCenter, float ZCenter, float Size)  const;
		
		// TRUE if the rect intersects the frustum
		bool CheckAABB( const Vector3& vPos, const Vector3& vBounds )  const;
		bool CheckAABB( const Vector3& vPos, float size )  const;
		bool CheckAABB( const BoundingVolume& bounds )  const;
		
		// TRUE if the sphere intersects the frustum
		bool CheckSphere(const Vector3& Pos, float Radius)  const;
		bool CheckSphere(const BoundingVolume& bounds)  const;

	protected:

		Plane _planes[6];	// Bounding planes
	};
}

//--------------------------------------------------------------------------------------
// File: Plane.h
//
// 3D plane
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma  once

namespace Maoli
{
	class Plane
	{
	public:

		// Ctor
		Plane();

		// Copy ctor
		Plane( const Plane& p );

		// Move ctor
		Plane( Plane&& p );

		// Construct from points
		Plane( const Vector3& a, const Vector3& b, const Vector3& c );

		// Construct from a normal and a point
		Plane( const Vector3& n, const Vector3& p );

		// Assignment op
		Plane& operator=( const Plane& p );

		// Move Assignment op
		Plane& operator=( Plane&& p);

		// Normalize the plane
		Plane& Normalize();

		// Dot with a vector
		float Dot( const Vector3& v ) const;

		// Coord dot
		float DotCoord( const Vector3& v ) const;

		// Construct from points
		Plane& FromPoints( const Vector3& a, const Vector3& b, const Vector3& c );

		// Construct from a normal and a point
		Plane& FromNormalAndPoint( const Vector3& n, const Vector3& p );

		// Get the plane normal
		const Vector3& GetNormal() const;

		float a, b, c, d;

	private:

		// Normal access
		inline Vector3& Normal() { return *(Vector3*)this; }

	};

}

// Print to console
std::ostream& operator<<(std::ostream& os, const Maoli::Plane& plane);

#include "Plane.inl"

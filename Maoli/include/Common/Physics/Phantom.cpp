//--------------------------------------------------------------------------------------
// File: Phantom.cpp
//
// Collision phantom
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Phantom.h"

#include "Game/Entity.h"
#include "hkpMaoliPhantom.h"

namespace Maoli
{
	// Ctor
	Phantom::Phantom( Engine* engine ) : Component( engine )
	{
		_phantom = nullptr;
		Create( Vector3( 1, 1, 1 ) );
	}

	// Dtor
	Phantom::~Phantom()
	{
		SafeRemoveReference( _phantom );
	}

	// Per-frame logic
	void Phantom::Update( float dt )
	{

	}

	// Binary serialize
	void Phantom::Export( std::ofstream& fout )
	{
		Maoli::Serialize( fout, &_size );
		Maoli::Serialize( fout, &_position );
		bool value = _phantom ? _phantom->_checkFixedBodies : false;
		Maoli::Serialize( fout, &value );
	}

	// Binary file import
	void Phantom::Import( std::ifstream& fin )
	{
		Maoli::Deserialize( fin, &_size );
		Maoli::Deserialize( fin, &_position );
		bool fixedCheck;
		Maoli::Deserialize( fin, &fixedCheck );
		Create( _size );
		_phantom->EnableFixedBodyCollisions( fixedCheck );
	}

	// Deep copy clone is required for all components
	Component* Phantom::Clone( Component* dest ) const
	{
		Phantom* clone = dest ? (Phantom*)dest : new Phantom( _engine );
		clone->Create( _size );
		return clone;
	}

	// Register for any events that need to be handled
	void Phantom::RegisterEventCallbacks()
	{
		REGISTER_EVENT_CALLBACK( ComponentEvent::TransformChanged, &Phantom::OnTransformChanged );
	}

	// Position update
	void Phantom::OnTransformChanged( Component* sender, void* userData )
	{
		Transform* transform = (Transform*)sender;
		const Vector3& pos = transform->GetPosition() + _position;
		hkAabb bounds;
		bounds.m_min.set( pos.x - _size.x, pos.y - _size.y, pos.z - _size.z );
		bounds.m_max.set( pos.x + _size.x, pos.y + _size.y, pos.z + _size.z );
		_phantom->setAabb( bounds );
	}

	// Process collisions
	void Phantom::ProcessCollisions()
	{
		_phantom->ensureDeterministicOrder();
		hkArray<hkpCollidable*>& collisions = _phantom->getOverlappingCollidables();
		for ( int32 i = 0; i < collisions.getSize(); ++i )
		{
			hkpRigidBody* body = hkpGetRigidBody( collisions[i] );
			if ( body )
				_entity->BroadcastMessage( ComponentEvent::PhantomCollision, (RigidBody*)body->getUserData(), nullptr );
		}
	}

	// Create an AABB phantom
	void Phantom::Create( const Vector3& size )
	{
		hkAabb bounds;
		_size = size;
		if ( _entity )
		{
			auto transform = _entity->GetComponent<Transform>();
			if ( transform )
			{
				const Vector3& pos = transform->GetPosition() + _position;
				bounds.m_min.set( pos.x - _size.x, pos.y - _size.y, pos.z - _size.z );
				bounds.m_max.set( pos.x + _size.x, pos.y + _size.y, pos.z + _size.z );
			}
		}
		else
		{
			bounds.m_min.set( _position.x - _size.x, _position.y - _size.y, _position.z - _size.z );
			bounds.m_max.set( _position.x + _size.x, _position.y + _size.y, _position.z + _size.z );
		}
		_phantom = new hkpMaoliPhantom( bounds, this );
	}

	// Set the size of the AABB
	void Phantom::SetSize( const Vector3& size )
	{
		if ( !_phantom )
		{
			Create( size );
			return;
		}

		hkAabb bounds;
		_size = size;
		if ( _entity )
		{
			auto transform = _entity->GetComponent<Transform>();
			if ( transform )
			{
				const Vector3& pos = transform->GetPosition() + _position;
				bounds.m_min.set( pos.x - _size.x, pos.y - _size.y, pos.z - _size.z );
				bounds.m_max.set( pos.x + _size.x, pos.y + _size.y, pos.z + _size.z );
			}
		}
		else
		{
			bounds.m_min.set( _position.x - _size.x, _position.y - _size.y, _position.z - _size.z );
			bounds.m_max.set( _position.x + _size.x, _position.y + _size.y, _position.z + _size.z );
		}
		_phantom->setAabb( bounds );
	}

	// Set the local center of the AABB
	void Phantom::SetPosition( const Vector3& value )
	{
		_position = value;
		if ( !_phantom )
			Create( _size );
		else
			SetSize( _size );
	}

	// Enable / disable collisions with fixed bodies
	void Phantom::EnableFixedBodyCollisions( bool value )
	{
		_phantom->EnableFixedBodyCollisions( value );
	}



}

//--------------------------------------------------------------------------------------
// File: Physics.cpp
//
// Physics system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "hkpMaoliPhantom.h"
#include "CollisionListener.h"

namespace Maoli
{

	// Simulation time step
	const float Physics::TimeStep = 1.0f / 60.0f;

	// Ctor
	Physics::Physics( Engine* engine ) : System( engine )
	{
		_world = nullptr;
		_elapsedTime = 0;
		_context = nullptr;
		_visualDebugger = nullptr;
		_collisionFilter = nullptr;
		_debug = false;
	}

	// Setup
	bool Physics::Init()
	{
		// Havok systems
		if ( !Havok::Init() )
			return false;

		// Setup the world
		hkpWorldCinfo info;
		info.m_gravity.set( 0, -9.8f, 0 );
		info.setBroadPhaseWorldSize( 2048.0f );
		info.m_contactPointGeneration = hkpWorldCinfo::CONTACT_POINT_ACCEPT_ALWAYS;
		info.setupSolverInfo( hkpWorldCinfo::SOLVER_TYPE_4ITERS_MEDIUM );
		info.m_simulationType = hkpWorldCinfo::SIMULATION_TYPE_CONTINUOUS;
		_world = new hkpWorld( info );

		// Register all collision agents
		hkpAgentRegisterUtil::registerAllAgents( _world->getCollisionDispatcher() );

		// Setup the collision filter
		_collisionFilter = new hkpGroupFilter;
		_world->setCollisionFilter( _collisionFilter );

		// Listener for handling collision events
		_world->addContactListener( new CollisionListener );

		return true;
	}

	// Frame update
	void Physics::Update( float dt )
	{
		// Update transforms
		UpdateTransforms( dt );

		// Step the simulation
		_elapsedTime += dt;
		while ( _elapsedTime >= Physics::TimeStep )
		{
			// Step the world
			_world->stepDeltaTime( Physics::TimeStep );

			// Step the debugger
			if ( _visualDebugger )
				_visualDebugger->step( Physics::TimeStep * 1000.0f );

			_elapsedTime -= Physics::TimeStep;
		}

		// Handle rigid body collisions
		for ( uint32 i = 0; i < _rigidBodies.Size(); ++i )
			_rigidBodies[i]->ProcessTransform();

		// Handle character collisions
		for ( uint32 i = 0; i < _characters.Size(); ++i )
			_characters[i]->ProcessTransform();

		// Handle phantom collisions
		for ( uint32 i = 0; i < _phantoms.Size(); ++i )
			_phantoms[i]->ProcessCollisions();

		// Show debug info
		if ( _debug )
			RenderDebugInfo();
	}

	// Cleanup
	void Physics::Release()
	{
		SafeRemoveReference( _world );
		_rigidBodies.Release();
		_characters.Release();
		_phantoms.Release();
		SafeRemoveReference( _visualDebugger );
		SafeRemoveReference( _context );
		SafeRemoveReference( _collisionFilter );
	};

	// Register a component to the system
	bool Physics::RegisterComponent( Component* component )
	{
		// Transform
		if ( component->GetTypeID() == typeid(Transform) )
		{
			_transforms.Add( (Transform*)component );
			return true;
		}

		// Physics body
		if ( component->GetTypeID() == typeid(RigidBody) )
		{
			RigidBody* body = (RigidBody*)component;
			_components.Add( component );
			_world->addEntity( body->_body );
			_rigidBodies.Add( body );
			body->_active = true;
			return true;
		}

		// Game character
		if ( component->GetTypeID() == typeid(CharacterController) )
		{
			CharacterController* body = (CharacterController*)component;
			_world->addEntity( body->_body );
			_components.Add( component );
			_characters.Add( body );
			return true;
		}

		// Phantoms
		if ( component->GetTypeID() == typeid(Phantom) )
		{
			Phantom* phantom = (Phantom*)component;
			_components.Add( component );
			_world->addPhantom( phantom->_phantom );
			_phantoms.Add( phantom );
			return true;
		}

		return false;
	}

	// Remove a component (assumes the component exists already in the system!)
	void Physics::UnregisterComponent( Component* component )
	{
		// Transforms
		if ( component->GetTypeID() == typeid(Transform) )
		{
			_transforms.Remove( (Transform*)component );
		}

		// Physics body
		if ( component->GetTypeID() == typeid(RigidBody) )
		{
			_components.Remove( component );
			RigidBody* body = (RigidBody*)component;
			_world->removeEntity( body->_body );
			_rigidBodies.Remove( body );
			body->_active = false;
			return;
		}

		// Game character
		if ( component->GetTypeID() == typeid(CharacterController) )
		{
			_components.Remove( component );
			CharacterController* body = (CharacterController*)component;
			_world->removeEntity( body->_body );
			_characters.Remove( body );
			return;
		}

		// Phantoms
		if ( component->GetTypeID() == typeid(Phantom) )
		{
			_components.Remove( component );
			Phantom* phantom = (Phantom*)component;
			_world->removePhantom( phantom->_phantom );
			_phantoms.Remove( phantom );
			return;
		}
	}

	// Update the transforms
	void Physics::UpdateTransforms( float dt )
	{
		for ( uint32 i = 0; i < _transforms.Size(); ++i )
			_transforms[i]->Update( dt );
	}

	// Enable visual debugger support
	void Physics::EnableVisualDebugger()
	{
		_context = new hkpPhysicsContext();
		_context->addWorld( _world );
		hkpPhysicsContext::registerAllPhysicsProcesses();
		hkArray<hkProcessContext*> contexts;
		contexts.pushBack( _context );
		_visualDebugger = new hkVisualDebugger( contexts );
		_visualDebugger->serve();
	}

	// Render debug visualizations for all rigid bodies
	void Physics::RenderDebugInfo()
	{
		if ( !_debug )
			return;
		auto graphics = _engine->GetGraphics()->GetDebugVisualizer();

		// Rigid bodies
		for ( uint32 i = 0; i < _rigidBodies.Size(); ++i )
		{
			if ( !_rigidBodies[i]->_transform )
				continue;

			// Get the body transform
			Matrix transform = Matrix::CreateTranslation( _rigidBodies[i]->GetLocalPosition() ) * _rigidBodies[i]->_transform->GetOrientationMatrix();

			// Draw each shape type
			RigidBodyDesc& desc = _rigidBodies[i]->_desc;
			switch ( desc.shape )
			{
				case PhysicsShape::Box:
				{
					transform = Matrix::CreateScaling( desc.size ) * transform;
					graphics->DrawBox( (Matrix&)transform, Color::Blue );
					break;
				}

				case PhysicsShape::Sphere:
				{
					transform = Matrix::CreateScaling( desc.radius, desc.radius, desc.radius ) * transform;
					graphics->DrawSphere( (Matrix&)transform, Color::Blue );
					break;
				}

				case PhysicsShape::Capsule:
				{
					float height = desc.size.y + desc.radius;
					float radius = std::max( desc.size.y, desc.radius );
					transform = Matrix::CreateScaling( radius, height, radius ) * transform;
					graphics->DrawCapsule( (Matrix&)transform, Color::Blue );
					break;
				}

				case PhysicsShape::Cylinder:
				{
					float height = desc.size.y + desc.radius;
					float radius = std::max( desc.size.y, desc.radius );
					transform = Matrix::CreateScaling( radius, height, radius ) * transform;
					graphics->DrawCapsule( (Matrix&)transform, Color::Blue );
					break;
				}

				case PhysicsShape::Mesh:
				{
					if ( !_rigidBodies[i]->_mesh )
						_rigidBodies[i]->CreateDebugMesh();
					_rigidBodies[i]->_mesh->SetTransform( (Matrix&)transform );
					_engine->GetGraphics()->DrawMesh( _rigidBodies[i]->_mesh );
					break;
				}
			}
		}

		// Character controllers
		for ( uint32 i = 0; i < _characters.Size(); ++i )
		{
			// Get the body transform
			SSE::Matrix transform;
			_characters[i]->_body->getTransform().get4x4ColumnMajor( transform );

			// Draw each shape type
			float height = _characters[i]->_height + _characters[i]->_radius;
			float radius = std::max( _characters[i]->_height, _characters[i]->_radius );
			transform = SSE::Matrix::CreateScaling( radius, height, radius ) * SSE::Matrix::CreateTranslation( _characters[i]->GetLocalPosition() ) * transform;
			graphics->DrawCapsule( (Matrix&)transform, Color::Blue );
		}

		// Phantoms
		for ( uint32 i = 0; i < _phantoms.Size(); ++i )
		{
			// Draw each shape type
			Matrix transform = Matrix::CreateScaling( _phantoms[i]->_size ) * Matrix::CreateTranslation( _phantoms[i]->_position + _phantoms[i]->GetOwner()->GetComponent<Transform>()->GetPosition() );
			graphics->DrawBox( transform, Color::Red );
		}
	}

	// Linear cast a rigid body
	bool Physics::LinearCast( const Vector3& start, const Vector3& end, RigidBody* sweepBody, float& dist, RigidBody** hitBody )
	{
		hkpLinearCastInput input;
		sweepBody->SetPosition( start );
		input.m_to.set( end.x, end.y, end.z );

		hkpClosestCdPointCollector hitCollector;
		_world->linearCast( sweepBody->_body->getCollidable(), input, hitCollector );

		//	Check for hits
		if ( hitCollector.hasHit() )
		{
			auto body1 = hkpGetRigidBody( hitCollector.getHit().m_rootCollidableA );
			auto body2 = hkpGetRigidBody( hitCollector.getHit().m_rootCollidableB );
			RigidBody* component1 = (RigidBody*)body1->getUserData();
			RigidBody* component2 = (RigidBody*)body2->getUserData();
			*hitBody = (component1 == sweepBody) ? component2 : component1;
			dist = (end - start).Length() * hitCollector.getHitContact().getDistance();
			return true;
		}

		return false;
	}



}

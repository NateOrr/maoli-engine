//--------------------------------------------------------------------------------------
// File: hkpMaoliPhantom.h
//
// Custom hkpPhantom
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Phantom;

	// Custom hkpPhantom
	class hkpMaoliPhantom : public hkpAabbPhantom
	{
		friend class Phantom;
	public:

		// Ctor
		hkpMaoliPhantom( const hkAabb& aabb, Phantom* owner );

		// Gets the hkpPhantom type 
		virtual hkpPhantomType getType() const { return HK_PHANTOM_USER0; }

		// Called by the broadphase for every new overlapping collidable
		virtual void addOverlappingCollidable( hkpCollidable *collidable );

		// Called by the broadphase for every removed collidable - 
		// This may get called even if there was no corresponding addOverlappingCollidable() 
		// if the bodies are filtered not to collide.  
		virtual void removeOverlappingCollidable( hkpCollidable *collidable );

		// Get the current AABB. 
		virtual void calcAabb( hkAabb &aabb );

		// Enable / disable collisions with fixed bodies
		inline void EnableFixedBodyCollisions( bool value ) { _checkFixedBodies = value; }

	private:

		Phantom*	_phantom;			// Owner
		bool		_checkFixedBodies;	// True to trigger collision events for fixed bodies
	};
}

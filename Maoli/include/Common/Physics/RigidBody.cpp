//--------------------------------------------------------------------------------------
// File: RigidBody.cpp
//
// Physics rigid body
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "SampledHeightfieldShape.h"

namespace Maoli
{
	// Ctor
	RigidBodyDesc::RigidBodyDesc()
	{
		shape = PhysicsShape::Box;
		geometry = nullptr;
		motionType = MotionType::Dynamic;
		size = Vector3( 1, 1, 1 );
		radius = size.Length();
		mass = 1.0f;
		restitution = 0.5f;
	}

	// Ctor
	RigidBodyDesc::RigidBodyDesc( PhysicsShape shape, MotionType motionType, Geometry* geometry, const Matrix& transform, float mass, float restitution )
	{
		this->shape = shape;
		this->motionType = motionType;
		this->geometry = geometry;
		this->mass = mass;
		this->restitution = restitution;
		this->transform = transform;
	}

	// Ctor
	RigidBodyDesc::RigidBodyDesc( PhysicsShape shape, MotionType motionType, const Vector3& size, float mass, float restitution )
	{
		this->shape = shape;
		this->motionType = motionType;
		this->size = size;
		this->geometry = nullptr;
		this->mass = mass;
		this->restitution = restitution;
	}

	// Ctor
	RigidBodyDesc::RigidBodyDesc( PhysicsShape shape, MotionType motionType, float radius, float mass, float restitution )
	{
		this->shape = shape;
		this->motionType = motionType;
		this->radius = radius;
		this->geometry = nullptr;
		this->mass = mass;
		this->restitution = restitution;
	}


	// Ctor
	RigidBody::RigidBody( Engine* engine ) : Component( engine )
	{
		_body = nullptr;
		_mesh = nullptr;
		_collisionLayer = 0;
		_active = false;
		_transform = nullptr;
		Create( _desc );
	}

	// Dtor
	RigidBody::~RigidBody()
	{
		SafeRemoveReference( _body );
		SafeDelete( _mesh );
	}


	// Per-frame logic
	void RigidBody::Update( float dt )
	{
		_position = reinterpret_cast<const Vector3&>(_body->getPosition()) + _localPosition;
	}

	// Binary serialize
	void RigidBody::Export( std::ofstream& fout )
	{
		// Shared properties
		Maoli::Serialize( fout, &_desc.shape );
		Maoli::Serialize( fout, &_desc.motionType );
		Maoli::Serialize( fout, &_desc.mass );
		Maoli::Serialize( fout, &_desc.restitution );
		Maoli::Serialize( fout, &_desc.size );
		Maoli::Serialize( fout, &_desc.radius );
		Maoli::Serialize( fout, &_collisionLayer );
		Maoli::Serialize( fout, &_localPosition );

		// Mesh name if needed
		if ( _desc.shape == PhysicsShape::Mesh || _desc.shape == PhysicsShape::ConvexHull )
			_desc.geometry->GetFilePath().Serialize( fout );
	}

	// Binary file import
	void RigidBody::Import( std::ifstream& fin )
	{
		// Shared properties
		Maoli::Deserialize( fin, &_desc.shape );
		Maoli::Deserialize( fin, &_desc.motionType );
		Maoli::Deserialize( fin, &_desc.mass );
		Maoli::Deserialize( fin, &_desc.restitution );
		Maoli::Deserialize( fin, &_desc.size );
		Maoli::Deserialize( fin, &_desc.radius );
		Maoli::Deserialize( fin, &_collisionLayer );
		Maoli::Deserialize( fin, &_localPosition );

		// Mesh name if needed
		if ( _desc.shape == PhysicsShape::Mesh || _desc.shape == PhysicsShape::ConvexHull )
		{
			String str;
			str.Deserialize( fin );
			_desc.geometry = Geometry::CreateFromFile( str, _engine );
		}

		// Setup
		Create( _desc );

		// Set the layer
		SetCollisionLayer( _collisionLayer );
	}

	// Deep copy clone is required for all components
	Component* RigidBody::Clone( Component* dest ) const
	{
		RigidBody* clone = dest ? (RigidBody*)dest : new RigidBody( _engine );
		clone->Create( _desc );
		clone->SetCollisionLayer( _collisionLayer );
		return clone;
	}

	// Set the position
	void RigidBody::SetPosition( float x, float y, float z )
	{
		hkVector4 pos( x, y, z );
		_body->setPosition( pos );
		_position = reinterpret_cast<const Vector3&>(_body->getPosition()) + _localPosition;
	}

	// Set the position
	void RigidBody::SetPosition( const Vector3& p )
	{
		SetPosition( p.x, p.y, p.z );
	}

	// Set the linear velocity
	void RigidBody::SetLinearVelocity( const Vector3& v )
	{
		SetLinearVelocity( v.x, v.y, v.z );
	}

	// Set the linear velocity
	void RigidBody::SetLinearVelocity( float x, float y, float z )
	{
		hkVector4 vel( x, y, z );
		_body->setLinearVelocity( vel );
	}

	// Add to the linear velocity
	void RigidBody::AddToLinearVelocity( const Vector3& v )
	{
		SetLinearVelocity( v.x, v.y, v.z );
	}

	// Add to the linear velocity
	void RigidBody::AddToLinearVelocity( float x, float y, float z )
	{
		hkVector4 vel( x, y, z );
		vel.add( _body->getLinearVelocity() );
		_body->setLinearVelocity( vel );
	}

	// Set the angular velocity
	void RigidBody::SetAngularVelocity( float x, float y, float z )
	{
		hkVector4 vel( x, y, z );
		_body->setAngularVelocity( vel );
	}

	// Set the angular velocity
	void RigidBody::SetAngularVelocity( const Vector3& v )
	{
		SetAngularVelocity( v.x, v.y, v.z );
	}

	// Set a force
	void RigidBody::ApplyForce( float x, float y, float z )
	{
		hkVector4 force( x, y, z );
		_body->applyForce( _engine->GetFrameTime(), force );
	}

	// Set a force
	void RigidBody::ApplyForce( const Vector3& f )
	{
		ApplyForce( f.x, f.y, f.z );
	}

	// Process any changes in transform after the simulation step
	void RigidBody::ProcessTransform()
	{
		// Only process dynamic bodies
		if ( _body->getMotionType() != hkpMotion::MOTION_FIXED )
		{
			_entity->BroadcastMessage( ComponentEvent::PhysicsTransformChanged, this, nullptr );
		}
	}

	// Get the linear velocity
	const Vector3& RigidBody::GetLinearVelocity() const
	{
		return reinterpret_cast<const Vector3&>(_body->getLinearVelocity());
	}

	// Get the angular velocity
	const Vector3& RigidBody::GetAngularVelocity() const
	{
		return reinterpret_cast<const Vector3&>(_body->getAngularVelocity());
	}


	// Get the transformation matrix
	void RigidBody::GetTransform( Matrix& transform ) const
	{
		static SSE::Matrix alignedTransform;
		_body->getTransform().get4x4ColumnMajor( alignedTransform );
		memcpy( transform, alignedTransform, sizeof(Matrix) );
		transform.Translate( _localPosition );
	}


	// Test for ray intersection
	bool RigidBody::RayIntersect( const Vector3& start, const Vector3& dir, float rayLength, Vector3& hitLocation, float& dist )
	{
		hkpShapeRayCastInput input;
		hkpShapeRayCastOutput output;
		input.m_from.set( start.x, start.y, start.z );
		Vector3 end = start + dir*rayLength;
		input.m_to.set( end.x, end.y, end.z );
		_body->getCollidable()->getShape()->castRay( input, output );
		if ( output.hasHit() )
		{
			dist = output.m_hitFraction * rayLength;
			hitLocation = start + dir*dist;
		}
		return output.hasHit();
	}

	// Set the rotation
	void RigidBody::SetRotation( float x, float y, float z )
	{
		hkQuaternion q;
		hkVector4 axis;
		float angle;
		Math::AngleAxis( x, y, z, angle, (Vector3&)axis );
		q.setAxisAngle( axis, angle );
		_body->setRotation( q );
	}

	// Set the rotation
	void RigidBody::SetRotation( const Vector3& value )
	{
		SetRotation( value.x, value.y, value.z );
	}



	// Create a box shaped rigid body
	void RigidBody::CreateBox( const RigidBodyDesc& desc )
	{
		// Setup the havoc desc
		hkpRigidBodyCinfo bodyInfo;
		hkVector4 boxSize( desc.size.x, desc.size.y, desc.size.z );
		bodyInfo.m_shape = new hkpBoxShape( boxSize );
		bodyInfo.m_motionType = (hkpMotion::MotionType)desc.motionType;
		bodyInfo.m_restitution = desc.restitution;
		bodyInfo.m_mass = desc.mass;

		// Create the body
		SafeRemoveReference( _body );
		_body = new hkpRigidBody( bodyInfo );
		bodyInfo.m_shape->removeReference();
		_body->setUserData( (hkUlong)this );

		// Set the internal desc
		_desc = desc;
	}

	// Create a box shaped rigid body
	void RigidBody::CreateBox( MotionType motionType, const Vector3& size, float mass /*= 1.0f*/, float restitution /*= 0.5f */ )
	{
		Create( RigidBodyDesc( PhysicsShape::Box, motionType, size, mass, restitution ) );
	}


	// Create a sphere shaped rigid body
	void RigidBody::CreateSphere( const RigidBodyDesc& desc )
	{
		// Setup the havoc desc
		hkpRigidBodyCinfo bodyInfo;
		bodyInfo.m_shape = new hkpSphereShape( desc.radius );
		bodyInfo.m_motionType = (hkpMotion::MotionType)desc.motionType;
		bodyInfo.m_restitution = desc.restitution;
		bodyInfo.m_mass = desc.mass;

		// Create the body
		SafeRemoveReference( _body );
		_body = new hkpRigidBody( bodyInfo );
		bodyInfo.m_shape->removeReference();
		_body->setUserData( (hkUlong)this );

		// Set the internal desc
		_desc = desc;
	}

	// Create a sphere shaped rigid body
	void RigidBody::CreateSphere( MotionType motionType, float radius, float mass /*= 1.0f*/, float restitution /*= 0.5f */ )
	{
		Create( RigidBodyDesc( PhysicsShape::Sphere, motionType, radius, mass, restitution ) );
	}

	// Create a capsule shaped rigid body
	void RigidBody::CreateCapsule( const RigidBodyDesc& desc )
	{
		// Construct a shape
		float h = desc.size.y;
		hkVector4 vertexA( 0, -h, 0 );
		hkVector4 vertexB( 0, h, 0 );

		// Setup the havoc desc
		hkpRigidBodyCinfo bodyInfo;
		bodyInfo.m_shape = new hkpCapsuleShape( vertexA, vertexB, desc.radius );
		bodyInfo.m_motionType = (hkpMotion::MotionType)desc.motionType;
		bodyInfo.m_restitution = desc.restitution;
		bodyInfo.m_mass = desc.mass;

		// Create the body
		SafeRemoveReference( _body );
		_body = new hkpRigidBody( bodyInfo );
		bodyInfo.m_shape->removeReference();
		_body->setUserData( (hkUlong)this );

		// Set the internal desc
		_desc = desc;
	}

	// Create a capsule
	void RigidBody::CreateCapsule( MotionType motionType, float height, float radius, float mass /*= 1.0f*/, float restitution /*= 0.5f */ )
	{
		RigidBodyDesc desc;
		desc.motionType = motionType;
		desc.radius = radius;
		desc.size.y = height;
		desc.mass = mass;
		desc.restitution = restitution;
		desc.shape = PhysicsShape::Capsule;
		Create( desc );

	}

	// Create a capsule shaped rigid body
	void RigidBody::CreateCylinder( const RigidBodyDesc& desc )
	{
		// Construct a shape
		float h = desc.size.y;
		hkVector4 vertexA( 0, -h, 0 );
		hkVector4 vertexB( 0, h, 0 );

		// Setup the havoc desc
		hkpRigidBodyCinfo bodyInfo;
		bodyInfo.m_shape = new hkpCylinderShape( vertexA, vertexB, desc.radius );
		bodyInfo.m_motionType = (hkpMotion::MotionType)desc.motionType;
		bodyInfo.m_restitution = desc.restitution;
		bodyInfo.m_mass = desc.mass;

		// Create the body
		SafeRemoveReference( _body );
		_body = new hkpRigidBody( bodyInfo );
		bodyInfo.m_shape->removeReference();
		_body->setUserData( (hkUlong)this );

		// Set the internal desc
		_desc = desc;
	}

	// Create a cylinder
	void RigidBody::CreateCylinder( MotionType motionType, float height, float radius, float mass /*= 1.0f*/, float restitution /*= 0.5f */ )
	{
		RigidBodyDesc desc;
		desc.motionType = motionType;
		desc.radius = radius;
		desc.size.y = height;
		desc.mass = mass;
		desc.restitution = restitution;
		desc.shape = PhysicsShape::Cylinder;
		Create( desc );
	}


	// Create a convex hull rigid body
	void RigidBody::CreateConvexHull( const RigidBodyDesc& desc )
	{
		// Setup the havoc desc
		hkpRigidBodyCinfo bodyInfo;
		bodyInfo.m_shape = BuildConvexHull( desc.geometry, desc.transform );
		bodyInfo.m_motionType = (hkpMotion::MotionType)desc.motionType;
		bodyInfo.m_restitution = desc.restitution;
		bodyInfo.m_mass = desc.mass;

		// Create the body
		SafeRemoveReference( _body );
		_body = new hkpRigidBody( bodyInfo );
		bodyInfo.m_shape->removeReference();
		_body->setUserData( (hkUlong)this );

		// Set the internal desc
		_desc = desc;
	}

	// Create a convex hull rigid body
	void RigidBody::CreateConvexHull( MotionType motionType, Geometry* geometry, const Matrix& transform, float mass /*= 1.0f*/, float restitution /*= 0.5f */ )
	{
		Create( RigidBodyDesc( PhysicsShape::ConvexHull, motionType, geometry, transform, mass, restitution ) );
	}


	// Create a mesh rigid body
	void RigidBody::CreateMesh( const RigidBodyDesc& desc )
	{
		// Setup the havoc desc
		hkpRigidBodyCinfo bodyInfo;
		bodyInfo.m_shape = BuildStaticMeshShape( desc.geometry, desc.transform );
		bodyInfo.m_motionType = (hkpMotion::MotionType)desc.motionType;
		bodyInfo.m_restitution = desc.restitution;
		bodyInfo.m_mass = desc.mass;

		// Create the body
		SafeRemoveReference( _body );
		_body = new hkpRigidBody( bodyInfo );
		bodyInfo.m_shape->removeReference();
		_body->setUserData( (hkUlong)this );

		// Set the internal desc
		_desc = desc;
	}

	// Create a mesh rigid body
	void RigidBody::CreateMesh( MotionType motionType, Geometry* geometry, const Matrix& transform, float mass /*= 1.0f*/, float restitution /*= 0.5f */ )
	{
		Create( RigidBodyDesc( PhysicsShape::Mesh, motionType, geometry, transform, mass, restitution ) );
	}


	// Construct geometry from a mesh
	hkGeometry* RigidBody::BuildGeometry( Geometry* geometry, const Matrix& transform ) const
	{
		// Setup the geometry
		hkGeometry* havokGeometry = new hkGeometry;
		{
			// Verts
			for ( uint32 i = 0; i < geometry->GetNumVerts(); ++i )
			{
				const Vector3& pos = transform.Transform( geometry->GetVerts()[i] );
				havokGeometry->m_vertices.pushBack( hkVector4( pos.x, pos.y, pos.z ) );
			}

			// Faces
			hkGeometry::Triangle face;
			auto indices = geometry->GetIndices();
			for ( uint32 i = 0; i < geometry->GetNumIndices(); i += 3 )
			{
				face.set( indices[i], indices[i + 1], indices[i + 2] );
				havokGeometry->m_triangles.pushBack( face );
			}
		}
		return havokGeometry;
	}


	// Create a static mesh shape
	hkpShape* RigidBody::BuildStaticMeshShape( Geometry* geometry, const Matrix& transform ) const
	{
		// Setup the geometry
		hkGeometry* havokGeometry = BuildGeometry( geometry, transform );

		// Create the static mesh shape
		// todo: Implement your own version of hkpBvCompressedMeshShape
		hkpDefaultBvCompressedMeshShapeCinfo cInfo( havokGeometry );
		return new hkpBvCompressedMeshShape( cInfo );
	}

	// Build a convex hull from a mesh
	hkpShape* RigidBody::BuildConvexHull( Geometry* geometry, const Matrix& transform ) const
	{
		// Build the convex hull
		hkGeometry* havokGeometry = BuildGeometry( geometry, transform );
		hkpConvexVerticesShape::BuildConfig config;
		config.m_convexRadius = 0.1f;
		hkpConvexVerticesShape* shape = new hkpConvexVerticesShape( hkStridedVertices( havokGeometry->m_vertices ), config );
		havokGeometry->removeReference();
		return shape;
	}

	// Create from a descriptor
	bool RigidBody::Create( const RigidBodyDesc& desc )
	{
		// Allow for re-creation
		bool inWorld = _active;
		if ( inWorld )
			_system->UnregisterComponent( this );

		switch ( desc.shape )
		{
			case Maoli::PhysicsShape::Box:
				CreateBox( desc );
				break;

			case Maoli::PhysicsShape::Sphere:
				CreateSphere( desc );
				break;

			case Maoli::PhysicsShape::Capsule:
				CreateCapsule( desc );
				break;

			case Maoli::PhysicsShape::Cylinder:
				CreateCylinder( desc );
				break;

			case Maoli::PhysicsShape::ConvexHull:
				CreateConvexHull( desc );
				break;

			case Maoli::PhysicsShape::Mesh:
				CreateMesh( desc );
				break;

			default:
				return false;
		}

		// Make sure the body starts with the correct orientation
		if ( _transform )
			SetTransform( _transform->GetOrientationMatrix() );

		// Re-insert into the scene if needed
		if ( inWorld )
			_system->RegisterComponent( this );

		return true;
	}

	// Create a heightfield rigid body, supports static motion type only
	void RigidBody::CreateHeightfield( float* heightfield, float heightScale, uint32 size, float restitution /*= 0.5f */ )
	{
		// Find the min and max height in the heightfield
		hkpSampledHeightFieldBaseCinfo ci;
		ci.m_xRes = ci.m_zRes = size;

		// Setup the shape
		SampledHeightFieldShape* heightFieldShape = new SampledHeightFieldShape( heightfield, heightScale, size, ci );
		//hkpTriSampledHeightFieldCollection* collection = new hkpTriSampledHeightFieldCollection( heightFieldShape );
		//hkpTriSampledHeightFieldBvTreeShape* bvTree = new hkpTriSampledHeightFieldBvTreeShape( collection );

		// Setup the havoc desc
		hkpRigidBodyCinfo bodyInfo;
		bodyInfo.m_shape = heightFieldShape;
		bodyInfo.m_motionType = (hkpMotion::MotionType)MotionType::Static;
		bodyInfo.m_restitution = restitution;
		bodyInfo.m_mass = 100000.0f;

		// Create the body
		SafeRemoveReference( _body );
		_body = new hkpRigidBody( bodyInfo );
		bodyInfo.m_shape->removeReference();
		_body->setUserData( (hkUlong)this );
	}

	// Create a debug mesh for rendering
	void RigidBody::CreateDebugMesh()
	{
		// Build a mesh from the geometry
		if ( _desc.shape == PhysicsShape::Mesh )
		{
			SafeDelete( _mesh );
			_mesh = new DynamicMesh( _engine->GetGraphics() );
			_mesh->FillVerts( _desc.geometry->GetVerts(), _desc.geometry->GetNumVerts() );
			_mesh->FillIndices( _desc.geometry->GetIndices(), _desc.geometry->GetNumIndices() );
			auto material = _engine->GetGraphics()->CreateMaterialBinding( "Physics" );
			material->GetMaterial()->SetBaseColor( 0, 0, 1 );
			_mesh->SetMaterial( material );
			_mesh->EnableWireframe( true );
		}
	}

	// Get the height below a point
	void RigidBody::GetAttachLocation( Vector3& position )
	{
		float dist = 0;
		RayIntersect( position, Vector3( 0, -1, 0 ), 200.0f, position, dist );
	}

	// Set the transform
	void RigidBody::SetTransform( const Matrix& transform )
	{
		hkTransform hkt;
		SSE::Matrix alignedTransform;
		memcpy( alignedTransform, transform, 64 );
		hkt.set4x4ColumnMajor( alignedTransform );
		_body->setTransform( hkt );
	}

	// Process component dependencies
	void RigidBody::RegisterDependency( Component* dependency )
	{
		if ( dependency->GetTypeID() == typeid(Transform) )
		{
			_transform = (Transform*)dependency;
			if ( _body )
				SetTransform( _transform->GetOrientationMatrix() );
			return;
		}

	}

	// Handle mesh updates
	void RigidBody::OnMeshChanged( Component* sender, void* data )
	{
		if ( _desc.shape == PhysicsShape::Mesh )
		{
			auto model = (Model*)sender;
			CreateMesh( _desc.motionType, model->GetGeometry(), model->GetWorldMatrix() );
		}
	}

	// Register for entity events
	void RigidBody::RegisterEventCallbacks()
	{
		REGISTER_EVENT_CALLBACK( ComponentEvent::MeshChanged, &RigidBody::OnMeshChanged );
	}

	// Local position for the body, offset from the entity center
	void RigidBody::SetLocalPosition( const Vector3& val )
	{
		_localPosition = val;
		SetPosition( _transform->GetPosition() + _localPosition );
	}


}

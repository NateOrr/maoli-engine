//--------------------------------------------------------------------------------------
// File: Physics.h
//
// Physics system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Engine\System.h"

// Forward decl
class hkpWorld;
class hkpShape;
class hkpPhysicsContext;

namespace Maoli
{
	// Forward decl
	class RigidBody;
	class Geometry;
	class Phantom;

	// Physics system
	class Physics : public System
	{
	public:

		// Simulation time step
		static const float TimeStep;

		// Ctor
		Physics( Engine* engine );

		// Setup
		virtual bool Init();

		// Frame update
		virtual void Update( float dt );

		// Cleanup
		virtual void Release();

		// Register a component to the system
		virtual bool RegisterComponent( Component* component );

		// Remove a component (assumes the component exists already in the system!)
		virtual void UnregisterComponent( Component* component );

		// Update the transforms
		void UpdateTransforms( float dt );

		// Enable visual debugger support
		void EnableVisualDebugger();

		// Enable collisions between groups
		template <typename T>
		void EnableLayerCollisions( const T& group, const T& target )
		{
			_collisionFilter->enableCollisionsBetween( (int32)group, (int32)target );
		}

		// Disable collisions between groups
		template <typename T>
		void DisableLayerCollisions( const T& group, const T& target )
		{
			_collisionFilter->disableCollisionsBetween( (int32)group, (int32)target );
		}

		// Enable / disable debug rendering
		void EnableDebugRendering( bool value ) { _debug = value; }

		// Render debug visualizations for all rigid bodies
		void RenderDebugInfo();

		///////////////////////////////////////////////////////
		// Collision Queries

		// Test for ray intersection
		template <typename T>
		bool RayIntersect( const Vector3& start, const Vector3& dir, float rayLength, const T& collisionLayer, Vector3& hitLocation, float& dist )
		{
			hkpWorldRayCastInput input;
			hkpWorldRayCastOutput output;
			input.m_from.set( start.x, start.y, start.z );
			Vector3 end = start + dir*rayLength;
			input.m_to.set( end.x, end.y, end.z );
			input.m_filterInfo = hkpGroupFilter::calcFilterInfo( (int32)collisionLayer );
			_world->castRay( input, output );
			if ( output.hasHit() )
			{
				dist = output.m_hitFraction * rayLength;
				hitLocation = start + dir*dist;
			}
			return output.hasHit();
		}

		// Test for ray intersection
		template <typename T>
		RigidBody* RayIntersect( const Vector3& start, const Vector3& dir, float rayLength, const T& collisionLayer )
		{
			hkpWorldRayCastInput input;
			hkpWorldRayCastOutput output;
			input.m_from.set( start.x, start.y, start.z );
			Vector3 end = start + dir*rayLength;
			input.m_to.set( end.x, end.y, end.z );
			input.m_filterInfo = hkpGroupFilter::calcFilterInfo( (int32)collisionLayer );
			_world->castRay( input, output );
			if ( output.hasHit() )
			{
				return (RigidBody*)hkpGetRigidBody( output.m_rootCollidable )->getUserData();
			}
			return nullptr;
		}

		// Linear cast a rigid body
		bool LinearCast( const Vector3& start, const Vector3& end, RigidBody* sweepBody, float& dist, RigidBody** hitBody );

	private:

		hkpWorld*						_world;				// Game world
		Array<RigidBody*>				_rigidBodies;		// Rigid bodies in the world
		Array<CharacterController*>		_characters;		// Game characters in the world
		Array<Phantom*>					_phantoms;			// Phantom collision regions
		Array<Transform*>				_transforms;		// Positions / orientations
		float							_elapsedTime;		// Elapsed time since the last update
		hkpGroupFilter*					_collisionFilter;	// Filter used for collision groups
		bool							_debug;				// True to show debug bodies

		// Debugging
		hkVisualDebugger*				_visualDebugger;	// Visual debugger
		hkpPhysicsContext*				_context;			// For visual debugger
	};

}

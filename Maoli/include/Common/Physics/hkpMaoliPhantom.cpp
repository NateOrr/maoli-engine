//--------------------------------------------------------------------------------------
// File: hkpMaoliPhantom.cpp
//
// Custom hkpPhantom
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "hkpMaoliPhantom.h"

namespace Maoli
{
	// Ctor
	hkpMaoliPhantom::hkpMaoliPhantom( const hkAabb& aabb, Phantom* owner ) : hkpAabbPhantom( aabb )
	{
		_phantom = owner;
		_checkFixedBodies = false;
	}

	// Called by the broadphase for every new overlapping collidable
	void hkpMaoliPhantom::addOverlappingCollidable( hkpCollidable *collidable )
	{
		// Ignore collidables within the same entity
		hkpRigidBody* body = hkpGetRigidBody( collidable );
		if ( body && (_checkFixedBodies || !body->isFixed()) )
		{
			RigidBody* maoliBody = (RigidBody*)body->getUserData();
			if ( maoliBody->GetOwner() != _phantom->GetOwner() )
				hkpAabbPhantom::addOverlappingCollidable( collidable );
		}
	}


	// Called by the broadphase for every removed collidable - 
	// This may get called even if there was no corresponding addOverlappingCollidable() 
	// if the bodies are filtered not to collide. 
	void hkpMaoliPhantom::removeOverlappingCollidable( hkpCollidable *collidable )
	{
		// Ignore collidables within the same entity
		hkpRigidBody* body = hkpGetRigidBody( collidable );
		if ( body && (_checkFixedBodies || !body->isFixed()) )
		{
			RigidBody* maoliBody = (RigidBody*)body->getUserData();
			if ( maoliBody->GetOwner() != _phantom->GetOwner() )
				hkpAabbPhantom::removeOverlappingCollidable( collidable );
		}
	}

	// Get the current AABB 
	void hkpMaoliPhantom::calcAabb( hkAabb &aabb )
	{
		hkpAabbPhantom::calcAabb( aabb );
	}

}

//--------------------------------------------------------------------------------------
// File: CollisionListener.h
//
//  Listens for collisions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class CollisionListener : public hkpCollisionListener
	{
	public:

		// Called just after all collision detection between 2 rigid bodies has been executed.
		virtual void contactProcessCallback( hkpContactProcessEvent &event );


	private:

	};
}
//--------------------------------------------------------------------------------------
// File: CollisionListener.cpp
//
//  Listens for collisions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "CollisionListener.h"
#include "Game/Entity.h"

namespace Maoli
{

	// Called just after all collision detection between 2 rigid bodies has been executed.
	void CollisionListener::contactProcessCallback( hkpContactProcessEvent &event )
	{
		auto body1 = hkpGetRigidBody( event.m_collidableA );
		auto body2 = hkpGetRigidBody( event.m_collidableB );
		RigidBody* component1 = (RigidBody*)body1->getUserData();
		RigidBody* component2 = (RigidBody*)body2->getUserData();
		hkVector4& location = event.m_collisionData->getContactPoint( 0 ).m_contact.getPosition();
		component1->GetOwner()->BroadcastMessage( ComponentEvent::Collision, component2, &location );
		component2->GetOwner()->BroadcastMessage( ComponentEvent::Collision, component1, &location );
	}

}
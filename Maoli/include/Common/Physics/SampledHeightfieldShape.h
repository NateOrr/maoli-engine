//--------------------------------------------------------------------------------------
// File: SampledHeightFieldShape.h
//
// Physics rigid body for heightfield terrains
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class SampledHeightFieldShape : public hkpSampledHeightFieldShape
	{
	public:

		// Ctor
		SampledHeightFieldShape( hkReal* heightfield, float heightScale, int32 dimension, const hkpSampledHeightFieldBaseCinfo& ci );

		// My simple height lookup.
		HK_FORCE_INLINE hkReal getHeightAtImpl( int32 x, int32 z ) const
		{
			return _heightfield[z * _dimension + x] * _heightScale;
		}

		// Assuming each height field quad is defined as four points { 00, 01, 11, 10 },
		// this should return true if the two triangles share the edge p00-p11.
		// Otherwise it should return false if the triangles share the edge p01-p10 
		HK_FORCE_INLINE hkBool getTriangleFlipImpl() const
		{
			return false;
		}

		// Forward to collideSpheres Implementation.
		virtual void collideSpheres( const CollideSpheresInput& input, SphereCollisionOutput* outputArray ) const;

	private:

		hkReal*		_heightfield;
		int32			_dimension;
		float		_heightScale;
	};
}
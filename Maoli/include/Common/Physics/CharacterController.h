//--------------------------------------------------------------------------------------
// File: CharacterController.h
//
// Physics body for a controllable game character
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

// Forward decl
class hkpCharacterContext;
class hkpCharacterRigidBody;

namespace Maoli
{
	class CharacterController : public RigidBody
	{
		friend class Physics;
	public:
		COMPONENT_DECL( CharacterController );

		// Ctor
		CharacterController( Engine* engine );

		// Dtor
		virtual ~CharacterController();

		// Create a character controller
		void Create( float height, float radius, float mass );


		//////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Register for any events that need to be handled
		virtual void RegisterEventCallbacks();

		// Register component dependencies
		virtual void RegisterDependency( Component* dependency );


		//////////////////////////////////////////////
		// Callbacks

		// Update the controller when the camera moves
		void OnTransformChanged( Component* sender, void* userData );

		// Get the ground point below the character
		inline const Vector3& GetGroundPoint() const { return _groundPoint; }

		// Get the mass
		inline float GetMass() const { return _mass; }

		// Get the height
		inline float GetHeight() const { return _height; }

		// Get the raius
		inline float GetRadius() const { return _radius; }

		// Check if the controller is grounded
		inline bool IsGrounded() const { return (_context->getState() == HK_CHARACTER_ON_GROUND); }

		// Get the distance to the ground
		inline float GetDistanceToGround() const { return _groundHeight; }

		// Enable/disable gravity
		inline void EnableGravity( bool value ) { _gravity = value; }

		// Get the transformation matrix
		virtual void GetTransform( Matrix& transform ) const;

		// Set the local rotation
		void SetLocalRotation( const Vector3& value );

		// Get the local rotation
		inline const Vector3& GetLocalRotation() { return _rotation; }

	private:

		hkpShape*					_shape;				// Full sized capsule shape
		hkpCharacterRigidBody*		_characterBody;		// The main rigid body object
		hkpCharacterContext*		_context;			// Context for the state machine
		float						_mass;				// Mass in kg
		float						_height;			// Height of the character
		float						_radius;			// Radius of the character
		float						_groundHeight;		// Distance to the ground below
		Vector3						_groundPoint;		// Point on the ground below the character
		bool						_gravity;			// True for gravity
		Vector3						_rotation;			// Local rotation

		// Dependencies
		Transform*					_transform;			// Entity transform

	};
}

//--------------------------------------------------------------------------------------
// File: CharacterController.h
//
// Physics body for a controllable game character
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "CharacterController.h"

#include "Game/Entity.h"

namespace Maoli
{
	// Ctor
	CharacterController::CharacterController( Engine* engine ) : RigidBody( engine )
	{
		_shape = nullptr;
		_characterBody = nullptr;
		_height = 1.0f;
		_radius = 0.5f;
		_mass = 50.0f;
		_transform = nullptr;
		_context = nullptr;
		_gravity = true;
		Create( 1.0f, 0.5f, 50.0f );
	}

	// Dtor
	CharacterController::~CharacterController()
	{
		SafeRemoveReference( _shape );
		SafeRemoveReference( _characterBody );
		delete _context;
	}

	// Register component dependencies
	void CharacterController::RegisterDependency( Component* dependency )
	{
		RigidBody::RegisterDependency( dependency );

		// Local dependencies
		if ( dependency->GetOwner() == GetOwner() )
		{
			if ( dependency->GetTypeID() == typeid(Transform) || dependency->GetFamilyID() == typeid(Camera) )
			{
				_transform = (Transform*)dependency;
				return;
			}
		}

		// Global dependencies
		else
		{

		}
	}


	// Per-frame logic
	void CharacterController::Update( float dt )
	{
		hkpCharacterInput input;
		hkpCharacterOutput output;
		{
			input.m_inputLR = 0;
			input.m_inputUD = 0;

			input.m_wantJump = false;
			input.m_atLadder = false;

			input.m_up.set( 0, 1, 0 );
			input.m_forward.set( _transform->GetForwardVector().x, _transform->GetForwardVector().y, _transform->GetForwardVector().z );

			hkStepInfo stepInfo;
			stepInfo.m_deltaTime = dt;
			stepInfo.m_invDeltaTime = 1.0f / dt;

			input.m_stepInfo = stepInfo;

			input.m_characterGravity.set( 0, 0, 0 );
			input.m_velocity = _body->getLinearVelocity();
			input.m_position = _body->getPosition();

			_characterBody->checkSupport( stepInfo, input.m_surfaceInfo );
		}

		// Apply the character state machine
		_context->update( input, output );

		//Apply the player character controller
		_characterBody->setLinearVelocity( output.m_velocity, Physics::TimeStep );

		// Apply gravity
		if ( _gravity && _context->getState() == HK_CHARACTER_IN_AIR )
			_transform->AddToVelocity( 0, -250 * dt, 0 );

		// Update the rigid body
		RigidBody::Update( dt );
	}

	// Binary serialize
	void CharacterController::Export( std::ofstream& fout )
	{
		Maoli::Serialize( fout, &_mass );
		Maoli::Serialize( fout, &_height );
		Maoli::Serialize( fout, &_radius );
		Maoli::Serialize( fout, &_collisionLayer );
		Maoli::Serialize( fout, &_rotation );
	}

	// Binary file import
	void CharacterController::Import( std::ifstream& fin )
	{
		Maoli::Deserialize( fin, &_mass );
		Maoli::Deserialize( fin, &_height );
		Maoli::Deserialize( fin, &_radius );
		Maoli::Deserialize( fin, &_collisionLayer );
		Maoli::Deserialize( fin, &_rotation );
		Create( _height, _radius, _mass );
		SetCollisionLayer( _collisionLayer );
	}

	// Deep copy clone is required for all components
	Component* CharacterController::Clone( Component* dest ) const
	{
		CharacterController* clone = dest ? (CharacterController*)dest : new CharacterController( _engine );
		clone->Create( _height, _radius, _body->getMass() );
		clone->_gravity = _gravity;
		clone->SetCollisionLayer(GetCollisionLayer());
		return clone;
	}


	// Register for any events that need to be handled
	void CharacterController::RegisterEventCallbacks()
	{
		REGISTER_EVENT_CALLBACK( ComponentEvent::TransformChanged, &CharacterController::OnTransformChanged );
	}


	// Update the controller
	void CharacterController::OnTransformChanged( Component* sender, void* userData )
	{
		Transform* transform = (Transform*)sender;
		SetLinearVelocity( transform->GetVelocity() );
		SetRotation( Vector3( transform->GetRotation().y, 0, 0 ) );
	}

	// Create a character controller
	void CharacterController::Create( float height, float radius, float mass )
	{
		// Construct a shape
		float h = height / 2.0f;
		hkVector4 vertexA( 0, -h, 0 );
		hkVector4 vertexB( 0, h, 0 );

		// Create a capsule to represent the character standing
		_shape = new hkpCapsuleShape( vertexA, vertexB, radius );
		_height = height;
		_radius = radius;
		_mass = mass;

		// Construct a character rigid body
		hkpCharacterRigidBodyCinfo info;
		info.m_mass = 100.0f;
		info.m_shape = _shape;
		info.m_mass = mass;
		info.m_maxForce = 1000.0f;
		info.m_up.set( 0, 1, 0 );
		info.m_maxSlope = 50.0f * HK_REAL_DEG_TO_RAD;

		// Setup the rigid body
		_characterBody = new hkpCharacterRigidBody( info );
		{
			hkpCharacterRigidBodyListener* listener = new hkpCharacterRigidBodyListener();
			_characterBody->setListener( listener );
			listener->removeReference();
		}
		_body = _characterBody->getRigidBody();
		_body->addReference();
		_body->setUserData( (hkUlong)this );

		// Create the Character state machine and context
		{
			hkpCharacterState* state;
			hkpCharacterStateManager* manager = new hkpCharacterStateManager();

			state = new hkpCharacterStateOnGround();
			manager->registerState( state, HK_CHARACTER_ON_GROUND );
			state->removeReference();

			state = new hkpCharacterStateInAir();
			manager->registerState( state, HK_CHARACTER_IN_AIR );
			state->removeReference();

			state = new hkpCharacterStateJumping();
			manager->registerState( state, HK_CHARACTER_JUMPING );
			state->removeReference();

			state = new hkpCharacterStateClimbing();
			manager->registerState( state, HK_CHARACTER_CLIMBING );
			state->removeReference();

			_context = new hkpCharacterContext( manager, HK_CHARACTER_ON_GROUND );
			manager->removeReference();
		}

		// Set character type
		_context->setCharacterType( hkpCharacterContext::HK_CHARACTER_RIGIDBODY );

		// Keep the rotation current
		SetRotation( _rotation );
	}

	// Get the transformation matrix
	void CharacterController::GetTransform( Matrix& transform ) const
	{
		static SSE::Matrix alignedTransform;
		_body->getTransform().get4x4ColumnMajor( alignedTransform );
		memcpy( transform, alignedTransform, sizeof(Matrix) );
	}

	// Set the local rotation
	void CharacterController::SetLocalRotation( const Vector3& value )
	{
		SetRotation( value );
		_rotation = value;
	}



}

//--------------------------------------------------------------------------------------
// File: SampledHeightFieldShape.cpp
//
// Physics rigid body for heightfield terrains
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "SampledHeightfieldShape.h"

namespace Maoli
{
	// Ctor
	SampledHeightFieldShape::SampledHeightFieldShape( hkReal* heightfield, float heightScale, int32 dimension, const hkpSampledHeightFieldBaseCinfo& ci ) 
		: _heightfield(heightfield), _heightScale(heightScale), _dimension(dimension), hkpSampledHeightFieldShape( ci )
	{

	}

	// Forward to collideSpheres Implementation.
	void SampledHeightFieldShape::collideSpheres( const CollideSpheresInput& input, SphereCollisionOutput* outputArray ) const
	{
		return hkSampledHeightFieldShape_collideSpheres( *this, input, outputArray );
	}


}
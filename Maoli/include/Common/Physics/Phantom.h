//--------------------------------------------------------------------------------------
// File: Phantom.h
//
// Collision phantom
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Game/Component.h"

namespace Maoli
{
	// Forward decl
	class hkpMaoliPhantom;

	class Phantom : public Component
	{
		friend class Physics;
	public:

		COMPONENT_DECL( Phantom );

		// Ctor
		Phantom( Engine* engine );

		// Dtor
		virtual ~Phantom();

		// Create an AABB phantom
		void Create( const Vector3& size );

		// Set the size of the AABB
		void SetSize( const Vector3& value );

		// Get the size
		inline const Vector3& GetSize() { return _size; }

		// Set the local center of the AABB
		void SetPosition( const Vector3& value );

		// Get the local position
		inline const Vector3& GetPosition() { return _position; }


		////////////////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Register for any events that need to be handled
		virtual void RegisterEventCallbacks();

		// Enable / disable collisions with fixed bodies
		void EnableFixedBodyCollisions( bool value );


		////////////////////////////////////////////////////////
		// Callbacks

		// Position update
		void OnTransformChanged( Component* sender, void* userData );

	private:

		// Process collisions
		void ProcessCollisions();

		Vector3				_size;		// AABB size
		Vector3				_position;	// Local position
		hkpMaoliPhantom*	_phantom;	// Phantom object

	};
}

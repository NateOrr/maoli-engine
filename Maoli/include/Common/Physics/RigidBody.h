//--------------------------------------------------------------------------------------
// File: RigidBody.h
//
// Physics rigid body
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Game/Component.h"

// Forward decl
class hkpRigidBody;
class hkpWorld;
class hkpShape;
struct hkGeometry;

namespace Maoli
{
	// Forward decl
	class DynamicMesh;

	// Physics body types
	enum class PhysicsShape
	{
		Box,
		Sphere,
		Capsule,
		Cylinder,
		ConvexHull,
		Mesh,
		Character,
	};

	// Maps to hkpMotion::MotionType
	enum class MotionType
	{
		Invalid,			// Not used
		Dynamic,			// Fully dynamic, chooses wither SphereInertia or BoxInertia at creation
		SphereInertia,		// Fully dynamic sphere
		BoxInertia,			// Fully dynamic box
		Keyframed,			// Not used
		Static,				// Static
		ThinBoxInertia,		// Fully dynamic box
		Character,			// Character controller
	};

	// Body desctriptor
	struct RigidBodyDesc
	{
		// Ctor
		RigidBodyDesc();
		RigidBodyDesc( PhysicsShape shape, MotionType motionType, Geometry* geometry, const Matrix& transform = Matrix::CreateIdentity(), float mass = 1.0f, float restitution = 0.5f );
		RigidBodyDesc( PhysicsShape shape, MotionType motionType, const Vector3& size, float mass = 1.0f, float restitution = 0.5f );
		RigidBodyDesc( PhysicsShape shape, MotionType motionType, float radius, float mass = 1.0f, float restitution = 0.5f );

		PhysicsShape			shape;			// The shape of the body
		MotionType				motionType;		// Motion type
		Vector3					size;			// AABB
		float					radius;			// Bound radius
		Geometry*				geometry;		// Parent mesh
		float					restitution;	// Object restitution (bounciness)
		float					mass;			// Mass in kg
		Matrix					transform;		// Geometry transform
	};

	// Physics rigid body
	class RigidBody : public Component
	{
		friend class Physics;
	public:

		// Register the component
		COMPONENT_DECL( RigidBody );

		// Ctor
		RigidBody( Engine* engine );

		// Dtor
		virtual ~RigidBody();


		//////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Process component dependencies
		virtual void RegisterDependency( Component* dependency );
		
		// Register for entity events
		virtual void RegisterEventCallbacks();


		// Process any changes in transform after the simulation step
		virtual void ProcessTransform();

		// Set the position
		void SetPosition( const Vector3& p );
		void SetPosition( float x, float y, float z );

		// Set the transform
		void SetTransform( const Matrix& transform );

		// Set the rotation
		void SetRotation( float x, float y, float z );
		void SetRotation( const Vector3& value );

		// Get the position
		inline const Vector3& GetPosition() const{ return _position; }

		// Set the linear velocity
		void SetLinearVelocity( const Vector3& v );
		void SetLinearVelocity( float x, float y, float z );

		// Add to the linear velocity
		void AddToLinearVelocity( const Vector3& v );
		void AddToLinearVelocity( float x, float y, float z );

		// Get the linear velocity
		const Vector3& GetLinearVelocity() const;

		// Set the angular velocity
		void SetAngularVelocity( const Vector3& v );
		void SetAngularVelocity( float x, float y, float z );

		// Get the angular velocity
		const Vector3& GetAngularVelocity() const;

		// Apply a force
		void ApplyForce( const Vector3& f );
		void ApplyForce( float x, float y, float z );

		// Get the transformation matrix
		virtual void GetTransform( Matrix& transform ) const;

		// Test for ray intersection
		bool RayIntersect( const Vector3& start, const Vector3& dir, float rayLength, Vector3& hitLocation, float& dist );

		// Get the motion type
		inline MotionType GetMotionType() const { return _desc.motionType; }

		// Get the shape
		inline PhysicsShape GetShape() const { return _desc.shape; }

		// Get the desc
		inline const RigidBodyDesc& GetDesc() const { return _desc; }


		////////////////////////////////////////////////////////
		// Shape creation

		// Create from a descriptor
		bool Create( const RigidBodyDesc& desc );

		// Create a box shaped rigid body
		void CreateBox( MotionType motionType, const Vector3& size, float mass = 1.0f, float restitution = 0.5f );

		// Create a sphere shaped rigid body
		void CreateSphere( MotionType motionType, float radius, float mass = 1.0f, float restitution = 0.5f );

		// Create a convex hull rigid body
		void CreateConvexHull( MotionType motionType, Geometry* geometry, const Matrix& transform = Matrix::CreateIdentity(), float mass = 1.0f, float restitution = 0.5f );

		// Create a mesh rigid body
		void CreateMesh( MotionType motionType, Geometry* geometry, const Matrix& transform = Matrix::CreateIdentity(), float mass = 1.0f, float restitution = 0.5f );

		// Create a heightfield rigid body, supports static motion type only
		void CreateHeightfield( float* heightfield, float heightScale, uint32 size, float restitution = 0.5f );

		// Create a capsule
		void CreateCapsule( MotionType motionType, float height, float radius, float mass = 1.0f, float restitution = 0.5f );

		// Create a cylinder
		void CreateCylinder( MotionType motionType, float height, float radius, float mass = 1.0f, float restitution = 0.5f );

		// Return true if this body has an active shape
		inline bool IsActive() const { return _active; }

		// Set the collision group
		template <typename T>
		void SetCollisionLayer( const T& group )
		{
			bool inWorld = _active;
			if ( inWorld )
				_system->UnregisterComponent( this );
			_collisionLayer = (int32)group;
			_body->setCollisionFilterInfo( hkpGroupFilter::calcFilterInfo( _collisionLayer ) );
			if ( inWorld )
				_system->RegisterComponent( this );
		}

		// Get the collision layer
		int32 GetCollisionLayer() const { return _collisionLayer; }

		// Get the height below a point
		void GetAttachLocation( Vector3& position );

		// Local position for the body, offset from the entity center
		inline const Vector3& GetLocalPosition() const { return _localPosition; }
		void SetLocalPosition( const Vector3& val );

	protected:

		// Create a box shaped rigid body
		void CreateBox( const RigidBodyDesc& desc );

		// Create a sphere shaped rigid body
		void CreateSphere( const RigidBodyDesc& desc );

		// Create a capsule shaped rigid body
		void CreateCapsule( const RigidBodyDesc& desc );

		// Create a cylinder shaped rigid body
		void CreateCylinder( const RigidBodyDesc& desc );

		// Create a convex hull rigid body
		void CreateConvexHull( const RigidBodyDesc& desc );

		// Create a mesh rigid body
		void CreateMesh( const RigidBodyDesc& desc );

		// Create a debug mesh for rendering
		void CreateDebugMesh();


		// Construct geometry from a mesh
		hkGeometry* BuildGeometry( Geometry* geometry, const Matrix& transform ) const;

		// Create a static mesh shape
		hkpShape* BuildStaticMeshShape( Geometry* geometry, const Matrix& transform ) const;

		// Build a convex hull from a mesh
		hkpShape* BuildConvexHull( Geometry* geometry, const Matrix& transform ) const;

		// Handle mesh updates
		void OnMeshChanged(Component* sender, void* data);

		hkpRigidBody*	_body;					// The havok body
		RigidBodyDesc	_desc;					// Descriptor for easy property access
		DynamicMesh*	_mesh;					// Mesh for visualization
		int32				_collisionLayer;		// Collision layer
		bool			_active;				// True if in the world
		Vector3			_localPosition;			// Local position for the body, offset from the entity center
		Vector3			_position;				// Global position

		// Dependencies
		Transform*		_transform;
	};
}

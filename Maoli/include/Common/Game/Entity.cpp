//--------------------------------------------------------------------------------------
// File: Entity.cpp
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Game/Entity.h"
#include "Game/Component.h"
#include "Engine\System.h"

namespace Maoli
{
	// Ctor
	Entity::Entity( Engine* engine, const char* name ) : _name( name )
	{
		_engine = engine;
		_active = false;
		_flaggedForDelete = _flaggedForRemove = false;
	}

	// Dtor
	Entity::~Entity()
	{
		// Delete the components
		for ( auto iter = _components.begin(); iter != _components.end(); ++iter )
			delete iter->second;
	}

	// Add a component
	bool Entity::AddComponent( Component* component )
	{
		// Make sure there are no duplicate types
		if ( _components.find( component->GetTypeID() ) != _components.end() )
		{
			String msg = "An entity cannot have two components of the same type: ";
			msg += component->GetTypeName();
			_engine->GetPlatform()->MessageBox( msg );
			return false;
		}

		// Add the component
		_components.emplace( component->GetTypeID(), component );
		component->SetOwner( this );
		component->RegisterEventCallbacks();

		// Register internal dependencies
		for ( auto iter = _components.begin(); iter != _components.end(); ++iter )
		{
			component->RegisterDependency( iter->second );
			iter->second->RegisterDependency( component );
		}

		// Register with the engine if needed
		if ( _active )
			_engine->RegisterComponent( component );

		return true;
	}


	// Remove a component
	void Entity::RemoveComponent( Component* component )
	{
		auto iter = _components.find( component->GetTypeID() );
		if ( iter != _components.end() )
		{
			// Remove from the entity
			component->SetOwner( nullptr );
			UnregisterComponent( component );
			_components.erase( iter );

			// Unregister with the engine if needed
			if ( _active )
				component->GetSystem()->UnregisterComponent( component );
		}
	}

	// Clone the entity
	Entity* Entity::Clone() const
	{
		// Clone all components
		Entity* e = _engine->CreateEntity( _name );
		for ( auto iter = _components.begin(); iter != _components.end(); ++iter )
			e->AddComponent( iter->second->Clone() );

		// Template tracking
		e->_templateName = GetTemplateName();
			
		return e;
	}

	// Register a callback for a component event
	void Entity::RegisterCallback( const EventID& msg, Component* component, MessageCallback callback )
	{
		auto iter = _eventCallbacks.find( msg.GetID() );
		std::map<Component*, MessageCallback>& callbacks = (iter == _eventCallbacks.end()) ? _eventCallbacks.emplace( msg.GetID(), std::map<Component*, MessageCallback>() ).first->second : iter->second;
		callbacks.emplace( component, callback );
		component->_messages.Add( msg.GetID() );
	}

	// Let components know about an event
	void Entity::BroadcastMessage( const EventID& msg, Component* sender, void* data )
	{
		auto eventIter = _eventCallbacks.find( msg.GetID() );
		if ( eventIter != _eventCallbacks.end() )
		{
			std::map<Component*, MessageCallback>& callbacks = eventIter->second;
			for ( auto callbackIter = callbacks.begin(); callbackIter != callbacks.end(); ++callbackIter )
				callbackIter->second( sender, data );
		}
	}

	// Unregister component callbacks
	void Entity::UnregisterComponent( Component* component )
	{
		for ( uint32 i = 0; i < component->_messages.Size(); ++i )
		{
			auto eventIter = _eventCallbacks.find( component->_messages[i] );
			if ( eventIter != _eventCallbacks.end() )
			{
				// This assumes the callback is currently registered
				std::map<Component*, MessageCallback>& callbacks = eventIter->second;
				callbacks.erase( callbacks.find( component ) );
				if ( callbacks.empty() )
					_eventCallbacks.erase( eventIter );
			}
		}
	}

	// Access a component by index
	Component* Entity::GetComponent( uint32 index )
	{
		uint32 count = 0;
		auto iter = _components.begin();
		for ( ; iter != _components.end(); ++iter, ++count )
		{
			if ( index == count )
				break;
		}
		return iter->second;
	}

	// Access a component by name
	Component* Entity::GetComponent( const std::type_index& type )
	{
		auto iter = _components.find( type );
		if ( iter != _components.end() )
			return iter->second;
		return nullptr;
	}

}

//--------------------------------------------------------------------------------------
// File: ScriptObject.h
//
// c++ script created from a dll
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Engine;
	class Entity;

	class ScriptObject
	{
		friend class ScriptManager;
		friend class Script;
	public:

		// Ctor
		ScriptObject();

		// Set the entity that owns this script
		inline void SetEntity( Entity* entity ) { _entity = entity; }

		// Setup goes here
		virtual bool Init();

		// Per-frame logic
		virtual void Update( float dt );

		// Cleanup goes here
		virtual void Release();

		// Called when a component is added to the parent entity
		virtual void OnComponentAdded(Component* component);

		// Called when a component is removed from the parent entity
		virtual void OnComponentRemoved(Component* component);

	protected:

		Engine* _engine;
		Entity* _entity;

	private:
		String	_name;
	};

	// Export Helper
#define EXPORT_SCRIPT(x)\
	extern "C"\
	{\
	__declspec(dllexport) x* CreateScript##x()\
	{\
	return new x;\
	}\
	__declspec(dllexport) void DeleteScript##x(x* script)\
	{\
	delete script;\
	}\
	}
}
//--------------------------------------------------------------------------------------
// File: Component.h
//
// A component is a set of functionality provided to an entity
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Event.h"

// I hate you windows...
#undef PostMessage

// Creation method macro
#define COMPONENT_DECL(TYPE) inline static Component* Instantiate(Engine* engine){ return new TYPE(engine); }

// Regiter a message callback
#define REGISTER_EVENT_CALLBACK(id, fn) \
	_entity->RegisterCallback( id, this, std::bind( fn, this, std::placeholders::_1, std::placeholders::_2 ) )

namespace Maoli
{
	// Forward decl
	class Entity;
	class Engine;
	class System;

	// A component is a set of functionality provided to an entity
	class Component
	{
		friend class Entity;
		friend class Engine;
	public:

		// Ctor
		Component( Engine* engine );

		// Dtor
		virtual ~Component();

		// Per-frame logic
		virtual void Update( float dt ) = 0;

		// Binary serialize
		virtual void Export( std::ofstream& fout ) = 0;

		// Binary file import
		virtual void Import( std::ifstream& fin ) = 0;

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const = 0;

		// Get the typeid for this component. This is used as the key inside an entity component map.
		// This is required to be definied in order for inheritance and unique components to
		// function properly together. Only base components should define this.
		virtual std::type_index GetFamilyID() const { return typeid(*this); }

		// Set the owner, and grab any attributes required by the component
		void SetOwner( Entity* owner ) { _entity = owner; }

		// Register for any messages that need to be handled
		virtual void RegisterEventCallbacks() {}

		// Register component dependencies
		virtual void RegisterDependency( Component* dependency ) {}

		// Returns true if an entity can contain only one component of this type
		virtual bool IsUnique() const { return false; }

		// Get the component typeid
		inline std::type_index GetTypeID() const { return typeid(*this); }

		// Get the component type name
		inline String GetTypeName() const { return std::type_index( typeid(*this) ).name(); }

		// Get the entity that owns this component
		inline Entity* GetOwner() { return _entity; }


		// Set the system that will manage this component
		inline void SetSystem( System* system ) { _system = system; }

		// Get the system that manages this component
		inline System* GetSystem() const { return _system; }

		// Register a new component
		template <typename T>
		static void RegisterType()
		{
			_creationMethods.emplace( typeid(T).name(), std::bind( &T::Instantiate, std::placeholders::_1 ) );
		}

		// Create a dependency link
		void AddDependency( Component* target );

		// Break a dependency link
		void RemoveDependency( Component* target );

		// Export dependencies to file
		void ExportDependencies( std::ofstream& fout ) const;

		// Import dependencies
		void ImportDependencies( std::ifstream& fin );


		// Access the engine that owns the component
		inline Engine* GetEngine() { return _engine; }

		// Create a component by typename
		static Component* Create( Engine* engine, const char* typeName );

	protected:

		Engine*			_engine;			// Engine that owns this component
		Entity*			_entity;			// Entity that owns this component
		System*			_system;			// Systom that manages this component

	private:

		// Creation mapping
		static std::map<String, std::function<Component*(Engine*)>>	_creationMethods;

		Array<int32>		_messages;	// Tracks events currently registered to

		// A dependency between components
		struct ComponentDependency
		{
			// Ctor
			ComponentDependency( Engine* engine, Component* component, const char* targetName, const std::type_index& targetType );

			// Apply a component link
			bool Link();

			String				targetEntityName;		// Name of the entity that contains the dependency
			std::type_index		targetComponentType;	// Type of the component dependency
			Engine*				engine;					// Engine that contains the components
			Component*			component;				// Dependent component
		};

		Array<ComponentDependency*>	_links;				// Component dependencies
	};
}

//--------------------------------------------------------------------------------------
// File: ScriptManager.cpp
//
// Scripting engine, using c++ dll
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "ScriptManager.h"
#include "ScriptObject.h"
#include "Script.h"

namespace Maoli
{
	// Ctor
	ScriptManager::ScriptManager( Engine* engine ) : System( engine )
	{

	}

	// Setup
	bool ScriptManager::Init()
	{
		return true;
	}

	// Cleanup
	void ScriptManager::Release()
	{
		// Free all active scripts and their library files
		for ( auto iter = _scriptInfo.begin(); iter != _scriptInfo.end(); ++iter )
		{
			ScriptInfo& info = iter->second;

			// Free scripts
			for ( auto scriptIter = info.activeScripts.Begin(); !scriptIter.End(); ++scriptIter )
				info.Delete( *scriptIter );
			info.activeScripts.Release();

			// Unload the library
			FreeLibrary( info.library );
		}
		_scriptInfo.clear();
	}

	// Update
	void ScriptManager::Update( float dt )
	{

	}

	// Register a component to the system
	bool ScriptManager::RegisterComponent( Component* component )
	{
		if ( component->GetTypeID() == typeid(Script) )
		{
			_components.Add( component );
			return true;
		}

		return false;
	}

	// Remove a component (assumes the component exists already in the system!)
	void ScriptManager::UnregisterComponent( Component* component )
	{
		if ( component->GetTypeID() == typeid(Script) )
		{
			_components.Remove( component );
			return;
		}
	}

	// Register component types with the engine
	void ScriptManager::RegisterComponentTypes()
	{
		Component::RegisterType<Script>();
	}

	// Load a script type
	bool ScriptManager::ImportScript( const char* file )
	{
		// Make sure this script is not loaded already
		String name = String( file ).GetFile().RemoveFileExtension();
		if ( _scriptInfo.find( name.c_str() ) != _scriptInfo.end() )
			return true;

		// Load the dll
		ScriptInfo info;
		info.library = LoadLibraryA( file );
		if ( info.library == nullptr )
		{
			_engine->GetPlatform()->MessageBox( String( "Could not load script file " ) + file );
			return false;
		}

		// Get the creation and delete functions
		info.Create = (CreateScriptFn)GetProcAddress( info.library, String("CreateScript") + name );
		info.Delete = (DeleteScriptFn)GetProcAddress( info.library, String("DeleteScript") + name );

		// Validate
		if ( !info.Create )
		{
			_engine->GetPlatform()->MessageBox( String( "Could not find function CreateScript in script file" ) + file );
			return false;
		}

		// Validate
		if ( !info.Delete )
		{
			_engine->GetPlatform()->MessageBox( String( "Could not find function DeleteScript in script file" ) + file );
			return false;
		}

		// Add to the map
		_scriptInfo.emplace( name.c_str(), info );

		return true;
	}

	// Create a script from name
	ScriptObject* ScriptManager::CreateScript( const char* name )
	{
		auto iter = _scriptInfo.find( name );
		if ( iter != _scriptInfo.end() )
		{
			// Create the script
			ScriptInfo& info = iter->second;
			auto script = info.Create();
			script->_engine = _engine;
			script->_name = name;
			
			// Try to call init
			if(!script->Init())
			{
				info.Delete(script);
				return nullptr;
			}
			
			// Add it to the active list
			info.activeScripts.Add( script );
			return script;
		}
		return nullptr;
	}

	// Delete a script
	void ScriptManager::DeleteScript( ScriptObject* script )
	{
		auto iter = _scriptInfo.find( script->_name.c_str() );
		if ( iter != _scriptInfo.end() )
		{
			ScriptInfo& info = iter->second;
			info.activeScripts.Remove( script );
			script->Release();
			info.Delete( script );
		}
	}

	// Load all scripts in a given folder
	void ScriptManager::ImportScriptSet( const String& dir )
	{
		Array<String> files;
		_engine->GetPlatform()->GetFilesInDirectory( dir + "\\*.dll", files );
		for ( uint32 i = 0; i < files.Size(); ++i )
			ImportScript( dir + files[i] );
	}

}
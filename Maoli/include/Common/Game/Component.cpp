//--------------------------------------------------------------------------------------
// File: Component.cpp
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Game/Component.h"
#include <mutex>

namespace Maoli
{
	// Creation mapping
	std::map<String, std::function<Component*(Engine*)>> Component::_creationMethods;

	// Ctor
	Component::ComponentDependency::ComponentDependency( Engine* engine, Component* component, const char* targetName, const std::type_index& targetType )
		: targetComponentType( targetType ), targetEntityName( targetName )
	{
		this->engine = engine;
		this->component = component;
	}

	// Apply a component link
	bool Component::ComponentDependency::Link()
	{
		// Get the depenent entity and component
		Entity* entity = component->GetOwner();

		// Get the target entity and component
		Entity* targetEntity = engine->GetEntity( targetEntityName );
		if ( !targetEntity )
			return false;
		Component* targetComponent = targetEntity->GetComponent( targetComponentType );
		if ( !targetComponent )
			return false;

		// Link them
		component->RegisterDependency( targetComponent );

		return true;
	}



	// Ctor
	Component::Component( Engine* engine ) : _engine( engine )
	{
		_system = nullptr;
		_entity = nullptr;
	}

	// Dtor
	Component::~Component()
	{
		for ( uint32 i = 0; i < _links.Size(); ++i )
			delete _links[i];
	}

	// Create a dependency link
	void Component::AddDependency( Component* target )
	{
		if ( target->GetOwner() != GetOwner() )
		{
			ComponentDependency* link = new ComponentDependency( _engine, this, target->GetOwner()->GetName(), target->GetTypeID() );
			for ( uint32 i = 0; i < _links.Size(); ++i )
			{
				if ( _links[i]->targetEntityName == link->targetEntityName &&
					_links[i]->targetComponentType == link->targetComponentType )
					return;
			}
			_links.Add( link );
			_links[_links.Size() - 1]->Link();
		}
	}

	// Break a dependency link
	void Component::RemoveDependency( Component* target )
	{
		if ( target->GetOwner() != GetOwner() )
		{
			for ( uint32 i = 0; i < _links.Size(); ++i )
			{
				if ( _links[i]->targetEntityName == target->GetOwner()->GetName() &&
					_links[i]->targetComponentType == target->GetTypeID() )
				{
					delete _links.RemoveAt( i );
					return;
				}
			}
		}
	}

	// Export dependencies to file
	void Component::ExportDependencies( std::ofstream& fout ) const
	{
		uint32 count = _links.Size();
		Maoli::Serialize( fout, &count );
		for ( uint32 i = 0; i < count; ++i )
		{
			_links[i]->targetEntityName.Serialize( fout );
			Maoli::Serialize( fout, &_links[i]->targetComponentType );
		}
	}

	// Import dependencies
	void Component::ImportDependencies( std::ifstream& fin )
	{
		uint32 count;
		Maoli::Deserialize( fin, &count );
		_links.Allocate( count );
		String targetName;
		std::type_index targetType = typeid(Maoli::Component);
		for ( uint32 i = 0; i < count; ++i )
		{
			targetName.Deserialize( fin );
			Maoli::Deserialize( fin, &targetType );
			_links[i] = new ComponentDependency( _engine, this, targetName, targetType );
		}
	}

	// Create a component by typename
	Component* Component::Create( Engine* engine, const char* typeName )
	{
		return _creationMethods[typeName]( engine );
	}
}
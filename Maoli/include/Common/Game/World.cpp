//--------------------------------------------------------------------------------------
// File: World.cpp
//
// Game world, contains a set of entities
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "World.h"
namespace Maoli
{


	// Ctor
	World::World( Engine* engine )
	{
		_engine = engine;
	}

	// Dtor
	World::~World()
	{

	}

	// Save out all entities in the world
	void World::Save( const char* file )
	{
		// Export the map data
		std::ofstream fout( file, std::ofstream::binary );
		String fileDir = String( file ).GetDirectory();
		String fileName = String( file ).GetFile().RemoveFileExtension();
		if ( fout.is_open() )
		{
			// Export all entities
			uint32 count = _entities.Size();
			Maoli::Serialize( fout, &count );
			for ( uint32 i = 0; i < _entities.Size(); ++i )
			{
				_entities[i]->GetName().Serialize( fout );
				_engine->SaveEntity( _entities[i], fout );
			}

			// Export properties
			Maoli::Serialize( fout, &_properties );

			// Terrain
			bool hasTerrain = _engine->GetGraphics()->GetTerrain();
			Maoli::Serialize( fout, &hasTerrain );
			if ( hasTerrain )
			{
				auto terrain = _engine->GetGraphics()->GetTerrain();
				terrain->ExportHeightmap( fileDir + fileName + ".dds" );
				terrain->ExportBlendmap( fileDir + fileName + "_blend.png" );

				// Save terrain materials
				for ( uint32 i = 0; i < terrain->GetNumLayers(); ++i )
				{
					if ( terrain->GetLayerMaterial( i ) )
					{
						terrain->GetLayerMaterial( i )->GetMaterial()->Save();
						_engine->GetGraphics()->GetDebugVisualizer()->DrawMaterialPreview( terrain->GetLayerMaterial( i ) );
					}
				}

				_engine->GetGraphics()->GetTerrain()->Export( fout );
			}

			// Ocean
			bool hasOcean = _engine->GetGraphics()->HasOcean();
			Maoli::Serialize( fout, &hasOcean );
			if ( hasOcean )
				_engine->GetGraphics()->GetOcean()->Export( fout );

			fout.close();
		}

	}

	static String RemoveNumbers( String in )
	{
		for ( int32 i = 0; i < in.Length(); ++i )
			if ( in[i] >= '0' && in[i] <= '9' )
				return in.Substring( 0, i );
		return in;
	}

	// Load in a game world
	bool World::Load( const char* file )
	{
		// Import the map data
		std::ifstream fin( file, std::ifstream::binary );
		String fileDir = String( file ).GetDirectory();
		String fileName = String( file ).GetFile().RemoveFileExtension();
		if ( fin.is_open() )
		{
			// Import all entities
			uint32 count;
			String name;
			Maoli::Deserialize( fin, &count );
			_entities.Allocate( count );
			for ( uint32 i = 0; i < _entities.Size(); ++i )
			{
				name.Deserialize( fin );
				auto entity = _engine->CreateEntity( name );
				_entities[i] = entity;
				_engine->LoadEntity( _entities[i], fin );
			}

			// Import properties
			Maoli::Deserialize( fin, &_properties );

			// Terrain
			bool hasTerrain;
			Maoli::Deserialize( fin, &hasTerrain );
			if ( hasTerrain )
			{
				auto terrain = _engine->GetGraphics()->ImportHeightmap( fileDir + fileName + ".dds" );
				_engine->GetGraphics()->GetTerrain()->LoadBlendmap( fileDir + fileName + "_blend.png" );
				_engine->GetGraphics()->GetTerrain()->Import( fin );
			}

			// Ocean
			bool hasOcean;
			Maoli::Deserialize( fin, &hasOcean );
			_engine->GetGraphics()->EnableOcean( hasOcean );
			if ( hasOcean )
				_engine->GetGraphics()->GetOcean()->Import( fin );

			fin.close();

			// Broadcast the event
			_engine->BroadcastEvent( EngineEvent::WorldLoaded, this );

			// Build the quadtree for the scene
			_engine->GetGraphics()->BuildQuadtree();

			//Save(file);
			return true;
		}

		return false;
	}

}




//--------------------------------------------------------------------------------------
// File: Entity.h
//
// Game entity, essentially just a collection of components
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Component;
	class Engine;

	class Entity
	{
		friend class Engine;
		friend class World;
		friend class EntityManager;
	public:

		// Internal data types
		typedef std::unordered_map<std::type_index, Component*> ComponentMap;
		typedef std::unordered_map<int32, std::map<Component*, MessageCallback>> EventMap;

		// Add a component
		bool AddComponent(Component* component);

		// Remove a component
		void RemoveComponent(Component* component);

		// Access a component by index
		Component* GetComponent( uint32 index );

		// Access a component by name
		Component* GetComponent( const std::type_index& type );

		// Access a component by type
		template <typename T>
		T* GetComponent()
		{
			auto iter = _components.find( typeid(T) );
			return (iter != _components.end( )) ? (T*)iter->second : nullptr;
		}

		// Get the component map
		inline const ComponentMap& GetComponentMap() { return _components; }

		// Get the name
		inline const String& GetName() const { return _name; }
		inline void SetName( String name ) { _name = name; }

		// Get the name of the template that spawned this entity
		inline const String& GetTemplateName() const { return (_templateName.Length() > 0) ? _templateName : _name; }

		// Clone the entity
		Entity* Clone() const;

		// Register a callback for a component event
		void RegisterCallback( const EventID& msg, Component* component, MessageCallback callback );

		// Let components know about an event
		void BroadcastMessage( const EventID& msg, Component* sender, void* data );

		// Check if the entity is active in the world
		inline bool IsActive() const { return _active; }

		// Access the engine that owns the component
		inline Engine* GetEngine() { return _engine; }

	private:

		// Ctor
		Entity( Engine* engine, const char* name = "Entity" );

		// Dtor
		~Entity();

		// Unregister component callbacks
		void UnregisterComponent(Component* component);

		String			_name;				// Name id
		String			_templateName;		// Name of the template that spawned this entity
		Engine*			_engine;			// Game engine that owns this entity
		ComponentMap	_components;		// Components mapped by typeid
		EventMap		_eventCallbacks;	// Registered component callback methods for events
		bool			_active;			// True if the entity is in the world

		// Engine flags
		bool			_flaggedForDelete;
		bool			_flaggedForRemove;
	};

}

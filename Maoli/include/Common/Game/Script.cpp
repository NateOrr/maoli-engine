//--------------------------------------------------------------------------------------
// File: Script.cpp
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Game/Script.h"
#include "Game/ScriptObject.h"
namespace Maoli
{
	// Ctor
	Script::Script( Engine* engine ) : Component( engine )
	{
		_script = nullptr;
	}

	// Dtor
	Script::~Script()
	{
		if ( _script )
			_engine->GetScriptManager()->DeleteScript( _script );
	}

	// Binary serialize
	void Script::Export( std::ofstream& fout )
	{
		bool hasScript = (_script != nullptr);
		Maoli::Serialize( fout, &hasScript );
		if ( hasScript )
			_script->_name.Serialize( fout );
	}

	// Binary file import
	void Script::Import( std::ifstream& fin )
	{
		String name;
		name.Deserialize( fin );
		_script = _engine->GetScriptManager()->CreateScript( name );
	}

	// Deep copy clone is required for all components
	Component* Script::Clone( Component* dest /*= nullptr */ ) const
	{
		Script* clone = new Script( _engine );
		if ( _script )
			clone->_script = _engine->GetScriptManager()->CreateScript( _script->_name );
		return clone;
	}

	// Register component dependencies
	void Script::RegisterDependency( Component* dependency )
	{
		if ( _script )
			_script->OnComponentAdded( dependency );
	}

	// Create a script from file
	bool Script::Load( const char* file )
	{
		// Cleanup previous script
		if ( _script )
		{
			_engine->GetScriptManager()->DeleteScript( _script );
			_script = nullptr;
		}

		// Create the script
		if(_engine->GetScriptManager()->ImportScript(file))
		{
			String name = String( file ).GetFile().RemoveFileExtension();
			_script = _engine->GetScriptManager()->CreateScript( name );
			return true;
		}

		return false;
	}




}

//--------------------------------------------------------------------------------------
// File: Event.h
//
// Entity/component Event system framework
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Component;

	// A unique event ID
	class EventID
	{
		friend class ComponentEvent;
	public:

		// Ctor
		EventID( int32 id = 4654981 ) : _id(id) {}

		// Get the message ID
		inline int32 GetID() const { return _id; }

	private:
		int32 _id;
	};

	// Game engine message
	class ComponentEvent
	{
		friend class EventID;
		friend class Engine;
	public:

		// Get a unique message ID
		static EventID CreateEventID(const String& name);

		// Get the name of a message
		static const String& GetEventName( int32 id );

		// Check if a message already exists
		static bool EventExists( const String& name );

		// Built in messages
		static const EventID TransformChanged;			// Entity transform has changed
		static const EventID PhysicsTransformChanged;	// Physics simulation has modified the transform
		static const EventID Collision;					// A collision with another entity has happened
		static const EventID PhantomCollision;			// An entity has penetrated the phantom collision region
		static const EventID AnimationFinished;			// A non-looping animation has just finished playing
		static const EventID AnimationUpdated;		    // A bone palette is ready to be sent to the gpu
		static const EventID AnimationLoaded;			// An animation skin was loaded
		static const EventID AbilityCast;				// An ability needs to be cast
		static const EventID MeshChanged;				// A model has been moved or modified

	private:

		// Register engine reserved types
		static void RegisterEngineEvents();

		static int32 _baseID;
		static std::unordered_map<int32, String> _messageMap;
		static std::map<String, int32> _nameMap;
	};

	// Callback format
	typedef std::function<void( Component*, void* )> MessageCallback;
}

//--------------------------------------------------------------------------------------
// File: Event.cpp
//
// Entity/component event system framework
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Event.h"

namespace Maoli
{
	// Engine reserved ID's
	const EventID ComponentEvent::TransformChanged;
	const EventID ComponentEvent::PhysicsTransformChanged;
	const EventID ComponentEvent::Collision;
	const EventID ComponentEvent::PhantomCollision;
	const EventID ComponentEvent::AnimationFinished;
	const EventID ComponentEvent::AnimationUpdated;
	const EventID ComponentEvent::AbilityCast;
	const EventID ComponentEvent::AnimationLoaded;
	const EventID ComponentEvent::MeshChanged;
	int32 ComponentEvent::_baseID = 0;
	std::map<String, int32> ComponentEvent::_nameMap;
	std::unordered_map<int32, String> ComponentEvent::_messageMap;

	// Get a unique message ID
	Maoli::EventID ComponentEvent::CreateEventID( const String& name )
	{
		auto iter = _nameMap.find( name );
		if ( iter == _nameMap.end() )
		{
			EventID id;
			id._id = _baseID++;
			_messageMap[id._id] = name;
			return id;
		}
		return iter->second;
	}

	// Get the name of a message
	const String& ComponentEvent::GetEventName( int32 id )
	{
		return _messageMap.find( id )->second;
	}

	// Check if a message already exists
	bool ComponentEvent::EventExists( const String& name )
	{
		return (_nameMap.find( name ) == _nameMap.end());
	}

	// Register engine reserved types
	void ComponentEvent::RegisterEngineEvents()
	{
		// Message creation helper
#define MESSAGE_DEF(x) const_cast<EventID&>(ComponentEvent::x) = ComponentEvent::CreateEventID( "x" );

		MESSAGE_DEF( TransformChanged );
		MESSAGE_DEF( PhysicsTransformChanged );
		MESSAGE_DEF( Collision );
		MESSAGE_DEF( PhantomCollision );
		MESSAGE_DEF( AnimationFinished );
		MESSAGE_DEF( AnimationUpdated );
		MESSAGE_DEF( AbilityCast );
		MESSAGE_DEF( AnimationLoaded );
		MESSAGE_DEF( MeshChanged );
	}

}
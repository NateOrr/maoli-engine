//--------------------------------------------------------------------------------------
// File: ScriptManager.h
//
// Scripting engine, using c++ dll
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Engine\System.h"

namespace Maoli
{
	// Forward decl
	class ScriptObject;
	class Engine;

	class ScriptManager : public System
	{
	public:

		// Ctor
		ScriptManager( Engine* engine );

		// Setup
		virtual bool Init();

		// Register a component to the system
		virtual bool RegisterComponent( Component* component );

		// Remove a component (assumes the component exists already in the system!)
		virtual void UnregisterComponent( Component* component );

		// Register component types with the engine
		virtual void RegisterComponentTypes();

		// Update the system internal state
		virtual void Update( float dt );

		// Cleanup
		virtual void Release();

		// Create a script instance from name
		ScriptObject* CreateScript( const char* name );

		// Delete a script
		void DeleteScript( ScriptObject* script );

		// Load a script type
		bool ImportScript( const char* file );

		// Load all scripts in a given folder
		void ImportScriptSet( const String& dir );

	private:

		// Create and delete script function formats
		typedef ScriptObject* (*CreateScriptFn)();
		typedef void( *DeleteScriptFn )(ScriptObject*);

		// Script data
		struct ScriptInfo
		{
			// Creation callback
			CreateScriptFn Create;

			// Delete callback
			DeleteScriptFn Delete;

			// DLL that contains the script
			AppInstance library;

			// Active scripts of this type
			List<ScriptObject*> activeScripts;
		};

		std::unordered_map<std::string, ScriptInfo>		_scriptInfo;		// Available script functions
	};
}
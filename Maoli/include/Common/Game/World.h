//--------------------------------------------------------------------------------------
// File: World.h
//
// Game world, contains a set of entities
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Engine;
	class Entity;

	class World
	{
	public:

		// Ctor
		World( Engine* engine );

		// Dtor
		~World();

		// Get the entities
		inline Array<Entity*>& GetEntities() { return _entities; }

		// Save out all entities in the world
		void Save( const char* file );

		// Load in a game world
		bool Load( const char* file );

	private:

		Engine*				_engine;
		Array<Entity*>		_entities;
		
		// Properties
		struct
		{
			Color				_ambientLighting;
			bool				_sky;
		} _properties;

	};
}

//--------------------------------------------------------------------------------------
// File: Script.h
//
// Game script
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Component.h"

namespace Maoli
{
	// Forward decl
	class Entity;
	class Engine;
	class ScriptObject;

	class Script : public Component
	{
		friend class ScriptManager;
	public:

		COMPONENT_DECL( Script );

	private:

		//////////////////////////////////////
		// Component interface

		// Ctor
		Script( Engine* engine );

		// Dtor
		virtual ~Script();

		// Per-frame logic
		virtual void Update( float dt ) {}

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Register component dependencies
		virtual void RegisterDependency( Component* dependency );

		// Create a script from file
		bool Load(const char* file);
		
		ScriptObject*	_script;	// Pointer to the script object created from a DLL
	};

}

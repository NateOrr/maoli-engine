//--------------------------------------------------------------------------------------
// File: ScriptObject.cpp
//
// c++ script created from a dll
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "ScriptObject.h"

namespace Maoli
{
	// Ctor
	ScriptObject::ScriptObject()
	{
		_engine = nullptr;
		_entity = nullptr;
	}

	// Setup goes here
	bool ScriptObject::Init()
	{
		return true;
	}

	// Per-frame logic
	void ScriptObject::Update( float dt )
	{

	}

	// Cleanup goes here
	void ScriptObject::Release()
	{

	}

	// Called when a component is added to the parent entity
	void ScriptObject::OnComponentAdded( Component* component )
	{

	}

	// Called when a component is removed from the parent entity
	void ScriptObject::OnComponentRemoved( Component* component )
	{

	}

}

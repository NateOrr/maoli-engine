//--------------------------------------------------------------------------------------
// File: Probe.cpp
//
// Environment mapping and planar reflections
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Probe.h"
#include "Graphics/Renderer.h"

namespace Maoli
{

	// Ctor
	EnvironmentProbe::EnvironmentProbe( Renderer* graphics )
	{
		_graphics = graphics;
		_surface = nullptr;
		SetBounds(Vector3(0, 0, 0), 200.0f);
		_dirty = true;
	}

	// Dtor
	EnvironmentProbe::~EnvironmentProbe()
	{
		Release();
	}

	// Create a new cubemap probe
	HRESULT EnvironmentProbe::CreateCubemap(uint32 size, DXGI_FORMAT format, uint32 miplevels, bool dynamic)
	{
		// Release previous probe
		Release();

		// Create the render target
		_surface = _graphics->CreateTextureCube(size, size, format, miplevels, false);
		if(!_surface)
		{
			M_ERROR("Failed to create m_Surface");
			return E_FAIL;
		}

		// Setup the properties
		_isDynamic = dynamic;

		return S_OK;
	}

	// Load a cubemap from file
	HRESULT EnvironmentProbe::LoadCubemap( const char* file )
	{
		// Load in the file
		_surface = _graphics->CreateTextureFromFile(file, true);
		if(!_surface)
		{
			M_ERROR("Failed to load m_Surface");
			return E_FAIL;
		}

		// Setup the properties
		_isDynamic = false;

		return S_OK;
	}

	// Free mem
	void EnvironmentProbe::Release()
	{
		_surface = nullptr;
		_isDynamic = false;
	}

	// Set the region
	void EnvironmentProbe::SetBounds( const Vector3& position, float radius )
	{
		_bounds.Set(position, radius);
	}

}

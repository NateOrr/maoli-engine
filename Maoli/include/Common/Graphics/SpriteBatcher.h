//--------------------------------------------------------------------------------------
// File: SpriteBatcher.h
//
// 2D Sprite rendering
// 
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Texture.h"
#include "Graphics/Buffer.h"
#include "Graphics/Font.h"

namespace Maoli
{
	// Forward decl
	class Renderer;

	class SpriteBatcher
	{
	public:

		// Ctor
		SpriteBatcher(Renderer* graphics);

		// Dtor
		~SpriteBatcher();

		// Setup effect variables and passes
		void BindEffectVariables();

		// Draw a solid color recangle
		void Draw( int32 x, int32 y, int32 width, int32 height, const Color& color = Color::White );

		// Draw a textured rectangle
		void Draw( int32 x, int32 y, int32 width, int32 height, Texture::pointer texture, const Color& color = Color::White );

		// Draw a textured rectangle region
		void Draw( int32 x, int32 y, int32 width, int32 height, const Rect& region, Texture::pointer texture, const Color& color = Color::White );

		// Draw some text
		void DrawText( const char* text, Font::pointer font, int32 x, int32 y, float scale = 1.0f, const Color& color = Color::White );

		
		// Render all sprite batches
		void RenderSprites();

	private:

		// In this case, a sprite is a quad with a transformation matrix and texture coords
		struct Sprite
		{
			Vector2 scale;		// Scaling factor
			Vector2 position;	// Screen space position
			Color color;		// Material color
			Vector4 region;		// Region inside the texture
		};

		// Batch info
		struct Batch
		{
			Buffer<Sprite>::pointer instanceData;	// Buffer containing sprite instance data
			Array<Sprite>			instances;		// Instances for this frame
			Texture::pointer		texture;		// Texture used for this batch
			bool					needsRender;	// True when this batch has sprites to render
		};

		// Get/create the batch info for a texture
		Batch& GetBatch(Texture::pointer tex);

		// Helper types
		typedef std::pair<int32, Batch> BatchPair;

		static const int32					_maxInstances = 512;	// Max number of sprites for a given texture
		Renderer*							_graphics;				// Graphics engine
		std::unordered_map<int32, Batch>		_sprites;				// Each texture has a buffer of sprite instance data
		Array<Batch*>						_batchesToRender;		// List of batches that require rendering
		Buffer<SpriteVertex>::pointer		_quadVertexBuffer;		// The vertex buffer used for instances
		Matrix								_orthoMatrix;			// Orthographic matrix for sprite rendering
		Texture::pointer					_whiteTexture;			// Empty default texture

		// Effect variables
		ShaderMatrix*		_worldMatrixVariable;
		ShaderResource*		_spriteTextureVariable;
		ShaderResource*		_spriteInstanceDataVariable;

		// Effect passes
		EffectPass*			_effectPass;
	};

}

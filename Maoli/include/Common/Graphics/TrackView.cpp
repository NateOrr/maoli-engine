//--------------------------------------------------------------------------------------
// File: TrackView.cpp
//
// Trackview for cameras
// 
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/TrackView.h"

namespace Maoli
{
	// Track Ctor
	TrackView::Track::Track()
	{
		currentLocation = 0;
		time = 0;
		state = TrackState::Moving;
	}


	// Add a new track location
	void TrackView::AddTrackLocation( Camera& obj, const Vector3& location, const Vector3& lookAt, float moveTime, float waitTime )
	{
		// Grab the track, adding one for the object if one does not already exist
		auto trackIter = _tracks.find(&obj);
		Track& track = (trackIter != _tracks.end()) ? trackIter->second : (_tracks.insert(TrackPair(&obj, Track())).first->second);

		// Get the starting location and rotation
		const int32 endLocationIndex = track.locations.size()-1;
		const Vector3& startLocation = (endLocationIndex>=0) ? track.locations[endLocationIndex].position : obj.GetPosition();

		// Setup the track location
		TrackLocation trackLocation;
		trackLocation.position = location;
		trackLocation.lookAt = lookAt;
		trackLocation.moveDuration = moveTime;
		trackLocation.waitDuration = waitTime;

		// Compute the deltas
		trackLocation.deltaPosition = (location-startLocation) / moveTime;

		// Add this track location to the list
		track.locations.push_back(trackLocation);
	}


	// Update all tracks
	void TrackView::Update( float dt )
	{
		// Process each track
		for(auto i=_tracks.begin(); i!=_tracks.end();)
		{
			// Grab the track and the object
			Camera* obj = i->first;
			Track& track = i->second;
			TrackLocation& location = track.locations[track.currentLocation];
			
			// Update the timer
			track.time += dt;

			// Handle the current state
			if(track.state == TrackState::Moving)
			{
				// Check the time
				if(track.time < location.moveDuration)
				{
					// Update the position and rotation
					obj->AddToPosition(location.deltaPosition*dt);
					obj->LookAt(location.lookAt);
				}
				else
				{
					// Change to the waiting state
					dt = track.time - location.moveDuration;
					obj->AddToPosition(location.deltaPosition*dt);
					track.time = 0;
					track.state = TrackState::Waiting;
				}

				// Increment the iterator
				++i;
			}
			else
			{
				// Check the time
				if(track.time >= location.waitDuration)
				{
					// Move on to the next location
					++track.currentLocation;
					if(track.currentLocation < track.locations.size())
					{
						// Reset
						track.time = 0;
						track.state = TrackState::Moving;
						
						// Increment the iterator
						++i;
					}
					else
					{
						// No more locations exist, so remove this track
						i = _tracks.erase(i);
					}
				}
			}
		}
	}
	

}

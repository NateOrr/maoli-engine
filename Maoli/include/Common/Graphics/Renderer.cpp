//--------------------------------------------------------------------------------------
// File: Renderer.cpp
//
// Deferred rendering system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Renderer.h"

#include "Effect/EffectLoader.h"
#include "Graphics/DebugVisualizer.h"
#include "Sky.h"
#include "Water.h"
#include "HDR.h"
#include "Graphics/ShadowManager.h"
#include "Graphics/SkyBox.h"
#include "Graphics/InstanceSet.h"
#include "DirectXTex.h"
#include "Platform/Win32.h"

namespace Maoli
{
	// Statics
	uint32 Renderer::ProbeSettings::mipLevels;
	DXGI_FORMAT Renderer::ProbeSettings::format;
	uint32 Renderer::ProbeSettings::miplevels;

	// Ctor
	Renderer::Renderer( Engine* engine, WindowHandle window, bool windowed ) : System( engine ), _textureManager( this ), _fontManager( this ),
		_materialManager( this ), _meshManager( this )
	{
		// D3D
		_swapChain = nullptr;
		_context = nullptr;
		_backBuffer = nullptr;
		_device = nullptr;
		_driverType = D3D_DRIVER_TYPE_NULL;
		_width = 0;
		_height = 0;
		_lostDevice = false;
		_window = nullptr;
		_vsync = 0;
		_timer = 0;
		memset( _clearColor, 0, sizeof(float)* 4 );

		// Rendering stuff
		_quadVertexBuffer = nullptr;
		_skyNeedsUpdate = true;
		_effect = nullptr;
		_spriteBatcher = nullptr;
		_visualizer = nullptr;
		_window = window;
		_sky = nullptr;
		_ocean = nullptr;
		_hdr = nullptr;
		_hasSky = false;
		_shadowManager = nullptr;
		_sunColor = Color::White;
		_sunShadowSoftness = 2;
		_terrain = nullptr;
		_camera = nullptr;
		_hasOcean = false;
		_skybox = nullptr;
		_sunShadows = true;
		_quadtree = nullptr;
		_showTerrain = true;
		_mouseOnTerrain = false;
		_box = nullptr;
		_fullscreen = !windowed;

		// Default settings
		_settings.fxaa = false;
		_settings.ssao = false;
		_settings.hdr = true;
		_settings.motionBlur = false;

		// Cluster data
		_tileSize = 32;
		_clusterSlices = 16;
		_maxVisibleLights = 256;
		_maxVisibleLightsPerCluster = 64;

		_currentVelocityIndex = 0;
		_previousVelocityIndex = 0;
	}


#pragma region D3D Core

#pragma region D3D Stuff

	// Resize the swap chain
	HRESULT Renderer::OnResizeWindow()
	{
		// Get the new width and height
		RECT rc;
		GetClientRect( _window, &rc );
		int32 width = rc.right - rc.left;
		int32 height = rc.bottom - rc.top;
		if ( _width == width && height == _height )
			return S_OK;
		_width = width;
		_height = height;

		// Release the old views, as they hold references to the buffers we
		// will be destroying.  Also release the old depth/stencil buffer.
		_backBuffer = nullptr;
		_depthStencil = nullptr;

		// Resize the swap chain and recreate the render target view.
		HRESULT hr = _swapChain->ResizeBuffers( 1, _width, _height, DXGI_FORMAT_R8G8B8A8_UNORM, 0 );
		M_ASSERT( SUCCEEDED( hr ), "Swap chain failed to resize" );

		// Create the new render target
		ID3D11Texture2D* backBufferTex;
		ID3D11RenderTargetView* backBufferRTV;
		_swapChain->GetBuffer( 0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBufferTex) );
		hr = _device->CreateRenderTargetView( backBufferTex, 0, &backBufferRTV );
		M_ASSERT( SUCCEEDED( hr ), "ResizeSwapChain failed" );

		// Create the depth stencil
		_depthStencil = CreateDepthStencil( _width, _height, nullptr, true );
		if ( !_depthStencil->CreateReadOnlyView() )
		{
			return false;
		}

		// Create a texture to hold the back buffer
		_backBuffer = Texture::pointer( new Texture( this ) );
		_backBuffer->StoreRenderTarget( backBufferTex, backBufferRTV, _depthStencil );

		// Setup the viewport
		D3D11_VIEWPORT vp;
		vp.Width = (float)_width;
		vp.Height = (float)_height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		_context->RSSetViewports( 1, &vp );

		// Reset the resolution dependent resources
		OnReleasingSwapChain();
		_context->ClearState();
		_state.ResetBindings();
		OnResetSwapChain();

		return S_OK;
	}



	// Clears the current frame
	void Renderer::Clear()
	{
		_context->ClearRenderTargetView( _backBuffer->GetRTV()[0], _clearColor );
	}


	// Set the clear color
	void Renderer::SetClearColor( const float clearColor[4] )
	{
		memcpy( _clearColor, clearColor, sizeof(float)* 4 );
	}

	// Set the clear color
	void Renderer::SetClearColor( float r, float g, float b, float a )
	{
		_clearColor[0] = r;
		_clearColor[1] = g;
		_clearColor[2] = b;
		_clearColor[3] = a;
	}

	// Presents to the back buffer
	void Renderer::Present()
	{
		_swapChain->Present( _vsync, 0 );
	}


#pragma endregion



#pragma region Factory Methods


	// Depth stencil creation
	Pointer<DepthStencil> Renderer::CreateDepthStencil( uint32 width, uint32 height, DXGI_SAMPLE_DESC* pMSAA, bool asTexture )
	{
		Pointer<DepthStencil> ds( new DepthStencil( this ) );
		auto hr = ds->Create( width, height, pMSAA, asTexture );
		M_ASSERT( hr == S_OK, "Depth Stencil creation failed" );
		return ds;
	}

	// Depth stencil creation
	Pointer<DepthStencil> Renderer::CreateDepthStencilCube( uint32 width, uint32 height )
	{
		Pointer<DepthStencil> ds( new DepthStencil( this ) );
		auto hr = ds->CreateCube( width, height );
		M_ASSERT( hr == S_OK, "Depth Stencil creation failed" );
		return ds;
	}


	// Load a texture from file
	Texture::pointer Renderer::CreateTextureFromFile( const char* file, bool cubemap )
	{
		if ( !cubemap )
		{
			Texture::pointer tex = _textureManager.Load( file );
			if ( !tex )
				_engine->GetPlatform()->MessageBox( String( "Failed to load texture " ) + file );
			return tex;
		}
		Texture::pointer tex = _textureManager.Create( file );
		if ( !tex->Load( file, cubemap ) )
		{
			tex = nullptr;
		}
		return tex;
	}

	// Create a texture array from a sprite sheet
	Texture::pointer Renderer::CreateTextureArrayFromSpriteSheet( const char* file, int32 frameWidth, int32 frameHeight, int32 width, int32 height )
	{
		Texture::pointer tex = _textureManager.Create( file );
		if ( !tex->CreateSpriteSheet( file, frameWidth, frameHeight, width, height ) )
		{
			tex = nullptr;
		}
		return tex;
	}


	// 	Create from an existing 2d texture
	Texture::pointer Renderer::CreateTextureFromTexture( ID3D11Texture2D* pTex, bool isRTV, bool isSRV )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->CreateFromTexture( pTex, isRTV, isSRV );
		M_ASSERT( SUCCEEDED( hr ), "Failed to create texture from texture" );
		return tex;
	}



	// Create a 2d texture
	Texture::pointer Renderer::CreateTexture( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels,
		DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->Create( width, height, format, mipLevels, pMSAA, usage, isRTV, isSRV );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTexture" );
		return tex;
	}

	// Create a texture from some initial data
	Texture::pointer Renderer::CreateTextureFromData( uint32 width, uint32 height, DXGI_FORMAT format, D3D11_USAGE usage, bool srv, void* data )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->CreateFromData( width, height, format, usage, srv, data );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTextureFromData" );
		return tex;
	}


	// Create a 2d texture with more options.
	Texture::pointer Renderer::CreateTextureEx( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage,
		DXGI_FORMAT rtvFormat, DXGI_FORMAT srvFormat, DXGI_FORMAT uavFormat )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->CreateEx( width, height, format, mipLevels, pMSAA, usage, rtvFormat, srvFormat, uavFormat );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTextureEx" );
		return tex;
	}


	// 	Create a an array of 2d textures
	Texture::pointer Renderer::CreateTextureArray( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT* formats, uint32 mipLevels,
		DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->CreateArray( width, height, arraySize, formats, mipLevels, pMSAA, usage, isRTV, isSRV );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTextureArray" );
		return tex;
	}


	// Create a texture array, where the actual resource is bound as an array
	// Currently only SRV arrays are supported
	Texture::pointer Renderer::CreateTextureArrayResource( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels, bool rtv )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->CreateArrayResource( width, height, arraySize, format, mipLevels, rtv );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTextureArrayResource" );
		return tex;
	}


	// 	Create a cubemap texture.
	Texture::pointer Renderer::CreateTextureCube( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, bool asSingleCube )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->CreateCube( width, height, format, mipLevels, asSingleCube );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTextureCube" );
		return tex;
	}

	// 	Create a cubemap texture array
	Texture::pointer Renderer::CreateTextureCubeArray( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels )
	{
		Texture::pointer tex = _textureManager.Create();
		auto hr = tex->CreateCubeArray( width, height, arraySize, format, mipLevels );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTextureCube" );
		return tex;
	}


	// Insert a texture into an array resource
	HRESULT Renderer::InsertTextureIntoTextureArray( const char* file, Texture::pointer tex, uint32 index ) const
	{
		return tex->InsertTexture( file, index );
	}


	// Load a font from file
	Font::pointer Renderer::CreateFontFromFile( const char* file )
	{
		return _fontManager.Load( file );
	}
#pragma endregion



#pragma region Texture Saving and Loading

	// Save the texture to file
	void Renderer::SaveDDS( const char* file, Texture::pointer tex, D3DX11_IMAGE_FILE_FORMAT format ) const
	{
		wchar_t szBuff[128];
		Win32::AnsiToUnicode( const_cast<LPSTR>(file), szBuff, 128 );

		DirectX::Image img;
		img.format = DXGI_FORMAT_R8G8B8A8_UNORM;
		img.width = tex->GetWidth();
		img.height = tex->GetHeight();
		DirectX::ComputePitch( img.format, img.width, img.height, img.rowPitch, img.slicePitch );
		img.pixels = reinterpret_cast<uint8_t*>(tex->Map( D3D11_MAP_READ_WRITE ));
		DirectX::SaveToDDSFile( img, DirectX::DDS_FLAGS_NONE, szBuff );
		tex->Unmap();
	}

	// Saves a DDS from a staging texture
	void Renderer::SaveStagingDDS( const char* file, byte* tex, uint32 width, uint32 height ) const
	{
		wchar_t szBuff[128];
		Win32::AnsiToUnicode( const_cast<LPSTR>(file), szBuff, 128 );

		DirectX::Image img;
		img.format = DXGI_FORMAT_R8G8B8A8_UNORM;
		img.width = width;
		img.height = height;
		DirectX::ComputePitch( img.format, width, height, img.rowPitch, img.slicePitch );
		img.pixels = reinterpret_cast<uint8_t*>(tex);
		DirectX::SaveToDDSFile( img, DirectX::DDS_FLAGS_NONE, szBuff );
	}


	// Saves a DDS from a staging texture
	void Renderer::SaveStagingDDS( const char* file, Texture::pointer tex ) const
	{
		wchar_t szBuff[128];
		Win32::AnsiToUnicode( const_cast<LPSTR>(file), szBuff, 128 );

		DirectX::Image img;
		img.format = DXGI_FORMAT_R8G8B8A8_UNORM;
		img.width = tex->GetWidth();
		img.height = tex->GetHeight();
		DirectX::ComputePitch( img.format, img.width, img.height, img.rowPitch, img.slicePitch );
		img.pixels = reinterpret_cast<uint8_t*>(tex->Map( D3D11_MAP_READ_WRITE ));
		DirectX::SaveToDDSFile( img, DirectX::DDS_FLAGS_NONE, szBuff );
		tex->Unmap();
	}

	// Save a texture to file
	void Renderer::SaveTexture( const char* file, Texture::pointer tex, D3DX11_IMAGE_FILE_FORMAT format ) const
	{
		D3DX11SaveTextureToFileA( _context, tex->GetTex()[0], format, file );
	}

	// Save a texture to file
	void Renderer::SaveTexture( const char* file, Texture::pointer tex, uint32 index, D3DX11_IMAGE_FILE_FORMAT format ) const
	{
		D3DX11SaveTextureToFileA( _context, tex->GetTex()[index], format, file );
	}


	// Load a DDS to a staging texture
	bool Renderer::StageDDS( const char* file, Texture::pointer tex )
	{
		wchar_t szBuff[128];
		Win32::AnsiToUnicode( const_cast<LPSTR>(file), szBuff, 128 );

		DirectX::TexMetadata mdata;
		if ( FAILED( DirectX::GetMetadataFromDDSFile( szBuff, DirectX::DDS_FLAGS_NONE, mdata ) ) )
			return false;

		DirectX::ScratchImage img;
		if ( FAILED( DirectX::LoadFromDDSFile( szBuff, DirectX::DDS_FLAGS_NONE, &mdata, img ) ) )
			return false;

		byte* pixels = reinterpret_cast<byte*>(tex->Map( D3D11_MAP_READ_WRITE ));
		memcpy( pixels, img.GetPixels(), tex->GetWidth()*tex->GetHeight() );
		img.Release();

		return true;
	}

	// Load a DDS to a staging texture
	bool Renderer::StageDDS( const char* file, byte* tex, uint32 width, uint32 height )
	{
		wchar_t szBuff[128];
		Win32::AnsiToUnicode( const_cast<LPSTR>(file), szBuff, 128 );

		DirectX::TexMetadata mdata;
		if ( FAILED( DirectX::GetMetadataFromDDSFile( szBuff, DirectX::DDS_FLAGS_NONE, mdata ) ) )
			return false;

		DirectX::ScratchImage img;
		if ( FAILED( DirectX::LoadFromDDSFile( szBuff, DirectX::DDS_FLAGS_NONE, &mdata, img ) ) )
			return false;

		memcpy( tex, img.GetPixels(), width*height * 4 );
		img.Release();

		return true;
	}



#pragma endregion



#pragma region Texture Copying


	// Filter a texture
	void Renderer::FilterTexture( D3DX11_FILTER_FLAG filter, Texture::pointer tex, int32 mipSlot )
	{
		D3DX11FilterTexture( _context, tex->GetTex()[0], mipSlot, filter );
	}


	// Resolve the MSAA target into a regular target
	void Renderer::ResolveTexture( Texture::pointer dest, Texture::pointer source ) const
	{
		D3D11_RENDER_TARGET_VIEW_DESC rtDesc;
		source->GetRTV()[0]->GetDesc( &rtDesc );
		_context->ResolveSubresource( dest->GetTex()[0], D3D10CalcSubresource( 0, 0, 1 ), source->GetTex()[0], D3D10CalcSubresource( 0, 0, 1 ), rtDesc.Format );
	}

	// 	Copy a render target into this texture.
	void Renderer::CopyRenderTargetToTexture( Texture::pointer dest, ID3D11RenderTargetView* source ) const
	{
		ID3D11Resource* pRC = nullptr;
		source->GetResource( &pRC );
		_context->CopyResource( dest->GetTex()[0], pRC );
		pRC->Release();
	}


	// 	Copy a another texture into this texture
	void Renderer::CopyTextureToTexture( Texture::pointer dest, Texture::pointer source ) const
	{
		_context->CopyResource( dest->GetTex()[0], source->GetTex()[0] );
	}


	// 	Copy a another texture into this texture
	void Renderer::CopyTextureToTexture( Texture::pointer dest, ID3D11Texture2D* source ) const
	{
		_context->CopyResource( dest->GetTex()[0], source );
	}


	// Copy a region of a texture into a buffer
	// This function assumes the regions are valid for performance reasons
	// --------------------------------------------------------------------------------------
	HRESULT Renderer::CopyTextureToBuffer( uint32 texelSize, BYTE* pDest, uint32 dx, uint32 dy, uint32 destWidth,
		Texture::pointer pSource, uint32 subresource, uint32 left, uint32 top, uint32 right, uint32 bottom ) const
	{
		// Map the texture
		D3D11_MAPPED_SUBRESOURCE mapped;
		if ( FAILED( _context->Map( pSource->GetTex()[0], subresource, D3D11_MAP_READ_WRITE, 0, &mapped ) ) )
			return E_FAIL;

		// Width of the copy region in bytes
		uint32 sourceRowSize = texelSize*(right - left);

		// Width of each texture in bytes
		uint32 destStride = destWidth*texelSize;
		uint32 sourceStride = mapped.RowPitch;

		// Initial offset (y*width+x)
		uint32 destOffset = dy*destStride + dx*texelSize;
		uint32 sourceOffset = top*sourceStride + left*texelSize;

		// Copy each row
		BYTE* pSourceData = static_cast<BYTE*>(mapped.pData);
		for ( uint32 y = top; y < bottom; y++, destOffset += destStride, sourceOffset += sourceStride )
			CopyMemory( pDest + destOffset, pSourceData + sourceOffset, sourceRowSize );

		// Unmap
		_context->Unmap( pSource->GetTex()[0], subresource );
		return S_OK;
	}

	// Copy a region of a buffer into a texture
	// This function assumes the regions are valid for performance reasons
	HRESULT Renderer::CopyBufferToTexture( uint32 texelSize, Texture::pointer pDest, uint32 subresource, uint32 dx, uint32 dy,
		BYTE* pSource, uint32 left, uint32 top, uint32 right, uint32 bottom, uint32 sourceWidth ) const
	{
		// Map the texture
		D3D11_MAPPED_SUBRESOURCE mapped;
		if ( FAILED( _context->Map( pDest->GetTex()[0], subresource, D3D11_MAP_READ_WRITE, 0, &mapped ) ) )
			return E_FAIL;

		// Width of the copy region in bytes
		uint32 sourceRowSize = texelSize*(right - left);

		// Width of each texture in bytes
		uint32 destStride = mapped.RowPitch;
		uint32 sourceStride = sourceWidth*texelSize;

		// Initial offset (y*width+x)
		uint32 destOffset = dy*destStride + dx*texelSize;
		uint32 sourceOffset = top*sourceStride + left*texelSize;

		// Copy each row
		BYTE* pDestData = static_cast<BYTE*>(mapped.pData);
		for ( uint32 y = top; y < bottom; y++, destOffset += destStride, sourceOffset += sourceStride )
			CopyMemory( pDestData + destOffset, pSource + sourceOffset, sourceRowSize );

		// Unmap
		_context->Unmap( pDest->GetTex()[0], subresource );
		return S_OK;
	}

	// Copy a region of a texture into another buffer
	// This function assumes the regions are valid for performance reasons
	HRESULT Renderer::CopyBufferRegion( uint32 texelSize, BYTE* pDest, uint32 dx, uint32 dy, uint32 destWidth,
		BYTE* pSource, uint32 left, uint32 top, uint32 right, uint32 bottom, uint32 sourceWidth ) const
	{
		// Width of the copy region in bytes
		uint32 sourceRowSize = texelSize*(right - left);

		// Width of each texture in bytes
		uint32 destStride = destWidth*texelSize;
		uint32 sourceStride = sourceWidth*texelSize;

		// Initial offset (y*width+x)
		uint32 destOffset = dy*destStride + dx*texelSize;
		uint32 sourceOffset = top*sourceStride + left*texelSize;

		// Copy each row
		for ( uint32 y = top; y < bottom; y++, destOffset += destStride, sourceOffset += sourceStride )
			CopyMemory( pDest + destOffset, pSource + sourceOffset, sourceRowSize );

		return S_OK;
	}


#pragma endregion


#pragma region Mesh Loading

	// Creates a mesh binding from existing geometry
	MeshBinding::pointer Renderer::GetMeshBinding( Geometry* geometry )
	{
		MeshBinding::pointer binding = _meshManager.Create( geometry->GetName() );
		binding->Init( geometry );
		return binding;
	}

	// Create a new mesh binding from file
	MeshBinding::pointer Renderer::GetMeshBindingFromFile( const char* file )
	{
		return GetMeshBinding( Geometry::CreateFromFile( file, _engine ) );
	}

#pragma endregion


#pragma region Material


	// Create a material binding
	MaterialBinding::pointer Renderer::CreateMaterialBinding( Material* material )
	{
		MaterialBinding::pointer binding = _materialManager.Create( material->GetName() );
		binding->Init( material );
		return binding;
	}

	// Create a new material binding by name
	MaterialBinding::pointer Renderer::CreateMaterialBinding( const char* name )
	{
		return CreateMaterialBinding( Material::Create( name, _engine ) );
	}

	// Create a new material binding from file
	MaterialBinding::pointer Renderer::CreateMaterialBindingFromFile( const char* file )
	{
		auto mat = Material::CreateFromFile( file, _engine );
		if ( !mat )
		{
			_engine->GetPlatform()->MessageBox( String( "Failed to load material " ) + file );
			return CreateMaterialBinding( "Default" );
		}
		return CreateMaterialBinding( mat );
	}



#pragma endregion



#pragma endregion



	// Create a debug visualizer for the renderer to use
	bool Renderer::CreateDebugVisualizer()
	{
		_visualizer = new DebugVisualizer( this );
		if ( FAILED( _visualizer->Init() ) )
			return false;
		_visualizer->BindEffectVariables();
		return true;
	}

	// Cleanup
	void Renderer::Release()
	{
		// Unregister from events
		_engine->UnregisterEventCallback( EngineEvent::WorldLoaded, this );

		OnReleasingSwapChain();
		OnDestroyDevice();
		_depthStencil = nullptr;

		// Resources
		_meshManager.Release();
		_materialManager.Release();
		_fontManager.Release();
		_textureManager.Release();

		// Clear out the context
		if ( _context ) _context->Flush();
		if ( _context ) _context->ClearState();

		// Clear any views
		_depthStencil = nullptr;
		_backBuffer = nullptr;

		// Clear the device
		SafeRelease( _context );
		SafeRelease( _swapChain );
		SafeRelease( _device );
	}


	// Register a component to the system
	bool Renderer::RegisterComponent( Component* component )
	{
		// Models
		if ( component->GetTypeID() == typeid(Model) )
		{
			_components.Add( component );
			AddModel( (Model*)component );
			if ( _quadtree )
				_quadtree->Insert( (Model*)component );
			return true;
		}

		// Lights
		if ( component->GetTypeID() == typeid(Light) )
		{
			_components.Add( component );
			AddLight( (Light*)component );
			return true;
		}

		// Cameras
		if ( component->GetFamilyID() == typeid(Camera) )
		{
			SetCamera( (Camera*)component );
			UpdateCameraVariables();
			return true;
		}

		// Particle systems
		if ( component->GetTypeID() == typeid(ParticleEmitter) )
		{
			_components.Add( component );
			auto emitter = (ParticleEmitter*)component;
			_emitters.Add( emitter );
			emitter->BindEffect( _effect );
			return true;
		}

		return false;
	}


	// Remove a component (assumes the component exists already in the system!)
	void Renderer::UnregisterComponent( Component* component )
	{
		// Lights
		if ( component->GetTypeID() == typeid(Light) )
		{
			_components.Remove( component );
			RemoveLight( (Light*)component );
			return;
		}

		// Models
		if ( component->GetTypeID() == typeid(Model) )
		{
			_components.Remove( component );
			RemoveModel( (Model*)component );
			if ( _quadtree )
				_quadtree->Remove( (Model*)component );
			return;
		}

		// Cameras
		if ( component->GetFamilyID() == typeid(Camera) )
		{
			SetCamera( nullptr );
			return;
		}

		// Particle systems
		if ( component->GetTypeID() == typeid(ParticleEmitter) )
		{
			_components.Remove( component );
			_emitters.Remove( (ParticleEmitter*)component );
			return;
		}
	}

	// Sets the current time, and adjusts the sun position accordingly
	void Renderer::SetTimeOfDay( int32 hours, int32 minutes, int32 seconds )
	{
		_sky->SetTimeOfDay( hours, minutes, seconds );
		_skyNeedsUpdate = true;
	}

	// Get the time of day hours
	int32 Renderer::GetTimeOfDayHours() const
	{
		return _sky->GetTimeOfDayHours();
	}

	// Sets the current date, and adjusts the sun position accordingly
	void Renderer::SetDate( int32 month, int32 day, int32 year )
	{
		_sky->SetDate( month, day, year );
	}

	// Setup the renderer
	bool Renderer::Init()
	{
		// Setup d3d
		M_ASSERT( _window, "No window set for d3d" );

		// Get the window dimensions
		HRESULT hr = S_OK;
		RECT rc;
		HWND hWnd = _window;
		GetClientRect( hWnd, &rc );
		uint32 width = rc.right - rc.left;
		uint32 height = rc.bottom - rc.top;

		uint32 createDeviceFlags = D3D11_CREATE_DEVICE_SINGLETHREADED; // not interested in multi-threading for now
#ifdef _DEBUG
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

		// Feature level (currently d3d11 only)
		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
		};

		// Setup the swap chain desc
		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory( &sd, sizeof(sd) );
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = !_fullscreen;

		// Try creating the device
		hr = D3D11CreateDeviceAndSwapChain( nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createDeviceFlags,
			featureLevels, 1, D3D11_SDK_VERSION, &sd, &_swapChain,
			&_device, nullptr, &_context );
		if ( FAILED( hr ) )
			return false;

		// Create the render target view
		ID3D11Texture2D* backBufferTex;
		if ( FAILED( hr = _swapChain->GetBuffer( 0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBufferTex) ) ) )
		{
			return false;
		}
		ID3D11RenderTargetView* backBufferRTV;
		if ( FAILED( hr = _device->CreateRenderTargetView( backBufferTex, nullptr, &backBufferRTV ) ) )
		{
			return false;
		}

		// Create the depth stencil
		_depthStencil = CreateDepthStencil( width, height, nullptr, true );
		if ( !_depthStencil->CreateReadOnlyView() )
		{
			return false;
		}

		// Create a texture to hold the back buffer
		_backBuffer = Texture::pointer( new Texture( this ) );
		_backBuffer->StoreRenderTarget( backBufferTex, backBufferRTV, _depthStencil );

		// Bind the render target
		_context->OMSetRenderTargets( 1, _backBuffer->GetRTV(), _depthStencil->GetDSV() );


		// Setup the viewport
		D3D11_VIEWPORT vp;
		vp.Width = static_cast<FLOAT>(width);
		vp.Height = static_cast<FLOAT>(height);
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		_context->RSSetViewports( 1, &vp );

		_width = width;
		_height = height;

		// Setup device resources
		if ( FAILED( OnCreateDevice() ) )
		{
			ErrorMessage( "Failed to create device resources" );
			return false;
		}

		// Setup resolution dependent resources
		if ( FAILED( OnResetSwapChain() ) )
		{
			ErrorMessage( "Failed to create resolution dependent resources" );
			return false;
		}

		return true;
	}


	// Update scene objects
	void Renderer::Update( float dt )
	{
		// Reset any engine states
		_state.Reset();
		_timer += dt;
		_elapsedTimeVariable->SetFloat( dt );
		_timerVariable->SetFloat( _timer );

		// Get the mouse pos
		const Point& mousePos = _engine->GetPlatform()->GetMouseLocation();

		// Get the pick point on the terrain
		const Vector3& rayPos = GetPickRayPosition();
		const Vector3& rayDir = GetPickRayDirection();
		float dist = 0;
		_mouseOnTerrain = false;
		if ( _terrain && _terrain->RayIntersection( rayPos, rayDir, dist ) )
		{
			_terrainPickPoint = rayPos + rayDir * dist;
			_mouseOnTerrain = true;
		}

		// Update variables that change once per frame
		if ( _camera )
		{
			_oldViewProjMatrix = _viewProjMatrix;
			_previousViewProjectionVariable->SetMatrix( _oldViewProjMatrix );
			_viewProjMatrix = _camera->GetViewMatrix()*_camera->GetProjectionMatrix();
			_viewProjectionVariable->SetMatrix( _viewProjMatrix );
			_inverseViewProjectionVariable->SetMatrix( _viewProjMatrix.GetInverse() );
			_viewMatrixVariable->SetMatrix( _camera->GetViewMatrix() );
			_billboardMatrix = _camera->GetBillboardMatrix();
			_fovVariable->SetFloat( _camera->GetFoV() );

			// Camera position and direction
			_cameraPositionVariable->SetFloatVector( _camera->GetPosition() );
			_cameraDirectionVariable->SetFloatVector( _camera->GetViewDirection() );

			// Update the world space reconstruction basis
			_screenToWorldXVariable->SetFloatVector( _camera->GetOrientationMatrix().Transform( _screenToViewX ) );
			_screenToWorldYVariable->SetFloatVector( _camera->GetOrientationMatrix().Transform( _screenToViewY ) );
			_screenToWorldZVariable->SetFloatVector( _camera->GetOrientationMatrix().Transform( _screenToViewZ ) );

			// Picking ray
			Math::GetPickRay( mousePos.x, mousePos.y, *_camera, _pickRayPos, _pickRayDir );

			// Cull and sort meshes
			DetermineVisibleMeshes();
		}

		// Check if mouse is on screen
		_mouseOnScreen = (mousePos.x >= 0 && mousePos.y >= 0 && mousePos.x < _width && mousePos.y < _height);

		// Update water			// VTUNE: 8 - 15 FPS Increase
#ifndef _DEBUG
		if ( HasOcean() )
			_ocean->Update( dt );
#endif
	}


#pragma region Init

	// Re-binds effect variables after recompiling a shader
	HRESULT Renderer::BindEffectVariables()
	{
		// State
		_state.BindEffectVariables( _effect );

		// Debug
		if ( _visualizer )
			_visualizer->BindEffectVariables();

		// Attach hdr shader variables
		_hdr->BindEffectVariables();

		// Sky
		_sky->BindEffectVariables();

		// Water
		_ocean->BindEffectVariables();

		// Hammersley sampling points
		ComputeHammersleySequence( 1024 );

		// Screen and clipping
		Vector4 screenSize( (float)_width, (float)_height,
			1.0f / (float)_width, 1.0f / (float)_height );
		_screenSizeVariable->SetFloatVector( (float*)&screenSize );

		// Shadows
		_shadowManager->BindEffectVariables();

		// Sprite renderer
		_spriteBatcher->BindEffectVariables();

		// Clustered shading
		_tileWidth = (int32)ceil( float( _width ) / float( _tileSize ) );
		_tileHeight = (int32)ceil( float( _height ) / float( _tileSize ) );
		_tileWidthVariable->SetInt( _tileWidth );
		_tileHeightVariable->SetInt( _tileHeight );
		_clusterSlicesVariable->SetInt( _clusterSlices );
		_tileSizeVariable->SetInt( _tileSize );
		SetSunColor( _sunColor );
		_lightIndexDataUAV->SetResource( *_lightIndexBuffer );
		_clusterIndicesOffsetUAV->SetResource( *_clusterIndexOffsetBuffer );
		_clusterIndicesUAV->SetResource( *_clusterIndexBuffer );
		_numClustersVariable->SetInt( _numClusters );
		_maxLightsPerClusterVariable->SetInt( _maxVisibleLightsPerCluster );
		_lightIndexDataVariable->SetResource( *_lightIndexBuffer );
		_clusterIndicesVariable->SetResource( *_clusterIndexBuffer );
		_pointLightListVariable->SetResource( *_pointLightListBuffer );
		_spotLightListVariable->SetResource( *_spotLightListBuffer );

		// Update variables that depend on the camera
		UpdateCameraVariables();

		// Set values
		_ambientVariable->SetFloatVector( (float*)&_ambient );

		// Sky
		if ( _skybox )
			_skybox->BindEffectVariables();

		// Terrain
		if ( _terrain )
			_terrain->BindEffectVariables();

		// One-time binds
		_depthBufferVariable->SetResource( *_depthStencil );

		return S_OK;
	}

	// Update variables that depend on the camera
	void Renderer::UpdateCameraVariables()
	{
		if ( _camera )
		{
			// Projection ratio for linear depth
			float nearZ = _camera->GetNearZ();
			float farZ = _camera->GetFarZ();
			_projRatioVariable->SetFloatVector( Vector2( farZ / (farZ - nearZ), nearZ / (nearZ - farZ) ) );

			// Basis for world space reconstruction
			{
				// Projection ratio
				float fProjectionRatio = float( _width ) / float( _height );
				float fWorldHeightDiv2 = nearZ * tanf( _camera->GetFoV()*0.5f );
				float fWorldWidthDiv2 = fWorldHeightDiv2 * fProjectionRatio;
				float k = farZ / nearZ;

				// Build the basis
				_screenToViewX = Vector3( 1, 0, 0 ) * fWorldWidthDiv2 * k;
				_screenToViewY = Vector3( 0, 1, 0 ) * fWorldHeightDiv2 * k;
				_screenToViewZ = Vector3( 0, 0, nearZ ) * k;
				_screenToViewZ = _screenToViewZ - _screenToViewX;
				_screenToViewX *= (2.0f / float( _width ));
				_screenToViewZ = _screenToViewZ + _screenToViewY;
				_screenToViewY *= -(2.0f / float( _height ));
				_screenToViewX.w = _screenToViewY.w = _screenToViewZ.w = 0;
			}

			// Build clustered shading data
			BuildClusters();
		}
	}


	// Reload the effect
	HRESULT Renderer::LoadEffect( bool bind )
	{
		// Allocate the effect if needed
		if ( !_effect )
			_effect = new Effect( this );
		else
			_effect->Release();

		// Load the file
		if ( !_effect->AddMFX( "Shaders\\main.mfx" ) )
		{
			M_ERROR( "Failed to load maoli effect" );
			return E_FAIL;
		}

		InitVariables();
		if ( bind )
			BindEffectVariables();
		return S_OK;
	}


	// Reload the effect
	void Renderer::ReloadEffect()
	{
		_effect->Reload();
	}


	// Setup any d3d objects here that are not dependent on screen size changes
	HRESULT Renderer::OnCreateDevice()
	{
		// Register for events
		_engine->RegisterEventCallback( EngineEvent::MaterialChanged, this, &Renderer::OnMaterialChanged );

		// Set the background color to black
		SetClearColor( 0, 0, 0 );

		// Load the effect
		LoadEffect( false );

		// Set the device context for the state manager
		_state.SetDeviceContext( _context );

		// Create the input layout for each Vertex format
		{
			SafeRelease( Vertex::pInputLayout );
			H_RETURN( _effect->GetPassByName( "Sky" )->CreateInputLayout( Vertex::Desc, 3, &Vertex::pInputLayout ), "Failed to create input layout" );

			SafeRelease( Vertex::pStreamInputLayout );
			H_RETURN( _effect->GetPassByName( "Sky" )->CreateInputLayout( Vertex::StreamDesc, 3, &Vertex::pStreamInputLayout ), "Failed to create stream input layout" );

			SafeRelease( Vertex::pInstancedInputLayout );
			H_RETURN( _effect->GetPassByName( "GBufferInstancedAlpha" )->CreateInputLayout( Vertex::InstancedStreamDesc, 7, &Vertex::pInstancedInputLayout ), "Failed to create instanced input layout" );

			SafeRelease( PosVertex::pInputLayout );
			H_RETURN( _effect->GetPassByName( "ZFill" )->CreateInputLayout( PosVertex::Desc, 1, &PosVertex::pInputLayout ), "Failed to create input layout" );

			SafeRelease( SpriteVertex::pInputLayout );
			H_RETURN( _effect->GetPassByName( "Sprite" )->CreateInputLayout( SpriteVertex::Desc, 1, &SpriteVertex::pInputLayout ), "Failed to create sprite input layout" );

			SafeRelease( Vertex::pAnimationInputLayout );
			H_RETURN( _effect->GetPassByName( "ShadeSkinned" )->CreateInputLayout( Vertex::AnimationDesc, 5, &Vertex::pAnimationInputLayout ), "Failed to create animation input layout" );

			SafeRelease( PosVertex::pAnimationInputLayout );
			H_RETURN( _effect->GetPassByName( "ZFillSkinned" )->CreateInputLayout( PosVertex::AnimationDesc, 3, &PosVertex::pAnimationInputLayout ), "Failed to create animation input layout" );
		}

		// Instance data buffer
		_instanceBuffer = CreateVertexBuffer<Matrix>( _maxInstances, D3D11_USAGE_DYNAMIC );

		// Reset the device states
		_state.Reset();

		// Setup the shadow system
		_shadowManager = new ShadowManager( this );
		if ( !_shadowManager->Init( 512, 8, 4, 4 ) )
			return E_FAIL;

		// Water
		_ocean = new Ocean( this );
		if ( FAILED( _ocean->Init() ) )
		{
			M_ERROR( "Water system failed to load." );
			return E_FAIL;
		}

		// Setup the shadow system
		_sky = new Sky( this );
		if ( FAILED( _sky->Init() ) )
		{
			M_ERROR( "Sky system failed to load." );
			return E_FAIL;
		}

		// Scene settings
		ProbeSettings::Init();

		// Set the default ambient light level
		SetAmbientLighting( 0.3f, 0.3f, 0.3f );

		// Setup the environment probe system
		_probeDepthStencil = CreateDepthStencil( ProbeSettings::mipLevels, ProbeSettings::mipLevels, nullptr, false );
		_probeProjMatrix.Perspective( Math::pi*0.5f, 1.0f, 0.05f, 300.0f );


		// Setup the debug renderer
		if ( _visualizer )
			_visualizer->Init();

		// Setup the sprite batcher
		_spriteBatcher = new SpriteBatcher( this );

		// Create the point light buffer
		_pointLightData.Allocate( _maxVisibleLights );
		_pointLightListBuffer = CreateBuffer<Light::PointLightShaderData>( _maxVisibleLights, D3D11_USAGE_DEFAULT, false, true );
		if ( !_pointLightListBuffer )
		{
			M_ERROR( "Could not create _pointLightListBuffer" );
			return E_FAIL;
		}

		// Create the spot light buffer
		_spotLightData.Allocate( _maxVisibleLights );
		_spotLightListBuffer = CreateBuffer<Light::SpotLightShaderData>( _maxVisibleLights, D3D11_USAGE_DEFAULT, false, true );
		if ( !_spotLightListBuffer )
		{
			M_ERROR( "Could not create _spotLightListBuffer" );
			return E_FAIL;
		}

		// Setup the default material
		auto defaultMat = CreateMaterialBinding( "Default" );
		defaultMat->LoadTexture( "Resources\\Textures\\default_texture.jpg", TEX_DIFFUSE );
		defaultMat->GetMaterial()->SetRoughness( 0.5f );
		defaultMat->GetMaterial()->SetMetallic( 0 );
		defaultMat->GetMaterial()->SetBaseColor( 1, 1, 1 );

		// Box mesh
		_box = Model::CreateFromFile( "Resources\\Models\\cube.mesh", _engine );

		return S_OK;
	}


	// Setup any d3d objects here that depend on the screen size
	HRESULT Renderer::OnResetSwapChain()
	{
		_postFXBuffer[0] = CreateTexture( _width, _height, DXGI_FORMAT_R8G8B8A8_UNORM, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_postFXBuffer[0] )
		{
			M_ERROR( "Failed to create m_PostFXBuffer" );
			return E_FAIL;
		}
		_postFXBuffer[0]->AttachDepthStencil( _depthStencil );
		_postFXBuffer[1] = CreateTexture( _width, _height, DXGI_FORMAT_R8G8B8A8_UNORM, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_postFXBuffer[1] )
		{
			M_ERROR( "Failed to create m_PostFXBuffer" );
			return E_FAIL;
		}
		_postFXBuffer[1]->AttachDepthStencil( _depthStencil );

		// Frame buffer texture copy
		_frameBuffer = CreateTexture( _width, _height, DXGI_FORMAT_R16G16B16A16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_frameBuffer )
		{
			M_ERROR( "Failed to create _frameBuffer" );
			return E_FAIL;
		}

		// Reflection texture dsv
		_reflectionDepthStencil = CreateDepthStencil( _width, _height, nullptr, false );
		if ( !_reflectionDepthStencil )
		{
			M_ERROR( "Failed to create m_ReflectionDepth" );
			return E_FAIL;
		}

		// Build the camera projection matrix
		if ( _camera )
		{
			_camera->BuildProjectionMatrix( _width, _height );
			_projMatrixVariable->SetMatrix( _camera->GetProjectionMatrix() );
		}

		// Create a quad for rendering the light passes
		// The normal contains the frustum corner for that vertex
		// which is essentially the view direction to use for
		// reconstructing the position
		Vertex pNewVerts[6] =
		{
			{ Vector3( 1.0f, 1.0f, 0.5f ), 1.0f, 0.0f, Vector3() },
			{ Vector3( 1.0f, -1.0f, 0.5f ), 1.0f, 1.0f, Vector3() },
			{ Vector3( -1.0f, -1.0f, 0.5f ), 0.0f, 1.0f, Vector3() },
			{ Vector3( -1.0f, -1.0f, 0.5f ), 0.0f, 1.0f, Vector3() },
			{ Vector3( -1.0f, 1.0f, 0.5f ), 0.0f, 0.0f, Vector3() },
			{ Vector3( 1.0f, 1.0f, 0.5f ), 1.0f, 0.0f, Vector3() },
		};
		CopyMemory( _quadVerts, pNewVerts, 6 * sizeof(Vertex) );


		// Fill the vertex buffer
		_quadVertexBuffer = CreateVertexBuffer( 6, D3D11_USAGE_DYNAMIC, _quadVerts );
		if ( !_quadVertexBuffer )
		{
			M_ERROR( "Failed to create quad vertex buffer" );
			return E_FAIL;
		}

		// Velocity buffer
		_velocityMap[0] = CreateTexture( _width, _height, DXGI_FORMAT_R16G16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		_velocityMap[1] = CreateTexture( _width, _height, DXGI_FORMAT_R16G16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		_velocityMap[0]->AttachDepthStencil( _depthStencil );
		_velocityMap[1]->AttachDepthStencil( _depthStencil );

		// Create the HDR system
		_hdr = new HDRSystem( this );
		if ( FAILED( _hdr->Create( _width, _height, _depthStencil, _effect ) ) )
		{
			M_ERROR( "Failed to create hdr system" );
			return E_FAIL;
		}

		// Half res frame buffer
		for ( int32 i = 0; i < 2; ++i )
		{
			_halfResFrameBuffer[i] = CreateTexture( _width / 2, _height / 2, DXGI_FORMAT_R16G16B16A16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
			if ( !_halfResFrameBuffer[i] )
			{
				M_ERROR( "Could not create _halfResFrameBuffer" );
				return E_FAIL;
			}
		}

		// Create the light index buffer
		_numClusters = (int32)ceil( float( _width ) / float( _tileSize ) )*(int32)ceil( float( _height ) / float( _tileSize ) ) * _clusterSlices;
		_lightIndexData.Allocate( _numClusters );
		_lightIndexData.Zero();
		_lightIndexBuffer = CreateBuffer<LightIndexData>( _numClusters, D3D11_USAGE_DEFAULT, true, true );
		if ( !_lightIndexBuffer )
		{
			M_ERROR( "Could not create _lightIndexBuffer" );
			return E_FAIL;
		}
		_lightIndexBuffer->SetDebugName( "LightIndexData" );

		// Index variable for building light lists in the CS
		_clusterIndexOffsetBuffer = CreateBuffer<uint32>( _numClusters, D3D11_USAGE_DEFAULT, true, false );
		if ( !_clusterIndexOffsetBuffer )
		{
			M_ERROR( "Could not create _clusterIndexOffsetBuffer" );
			return E_FAIL;
		}
		_clusterIndexOffsetBuffer->SetDebugName( "ClusterIndexOffset" );

		// Cluster index buffer
		_clusterIndices.Allocate( _numClusters * _maxVisibleLightsPerCluster );
		_clusterIndexBuffer = CreateBuffer<uint32>( _numClusters * _maxVisibleLights, D3D11_USAGE_DEFAULT, true, true );
		if ( !_clusterIndexBuffer )
		{
			M_ERROR( "Could not create _clusterIndexBuffer" );
			return E_FAIL;
		}
		_clusterIndexBuffer->SetDebugName( "ClusterIndices" );

		// Create the GBuffers
		DXGI_FORMAT formats[] = {
			DXGI_FORMAT_R8G8B8A8_UNORM,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
		};
		_gbuffer = CreateTextureArray( _width, _height, DS_SIZE, formats, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_gbuffer )
		{
			M_ERROR( "Failed to create _gbuffer" );
			return E_FAIL;
		}
		_gbuffer->AttachDepthStencil( _depthStencil );

		_idTexture = CreateTexture( _width, _height, DXGI_FORMAT_R8_UINT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_idTexture )
		{
			M_ERROR( "Failed to create _idTexture" );
			return E_FAIL;
		}

		DXGI_FORMAT aoFormats[] = {
			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R8G8_UNORM,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
		};
		_gbufferHalfRes = CreateTextureArray( _width / 2, _height / 2, 3, aoFormats, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_gbufferHalfRes )
		{
			M_ERROR( "Failed to create _gbufferHalfRes" );
			return E_FAIL;
		}

		_aoTexture = CreateTexture( _width / 2, _height / 2, DXGI_FORMAT_R8_UNORM, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_aoTexture )
		{
			M_ERROR( "Failed to create _aoTexture" );
			return E_FAIL;
		}
		_aoTexture->SetClearColor( 1, 1, 1, 1 );

		_aoBlurTexture = CreateTexture( _width / 2, _height / 2, DXGI_FORMAT_R8_UNORM, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_aoBlurTexture )
		{
			M_ERROR( "Failed to create _aoBlurTexture" );
			return E_FAIL;
		}

		_emissiveBlurTexture = CreateTexture( _width / 2, _height / 2, DXGI_FORMAT_R16G16B16A16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_emissiveBlurTexture )
		{
			M_ERROR( "Failed to create _emissiveBlurTexture" );
			return E_FAIL;
		}

		// Setup the effect variables
		BindEffectVariables();

		return S_OK;
	}


	// Construct the hammersley sequence buffer and bind it to the shader
	// From: "Sampling with Hammersley and Halton Points", Wong et al
	void Renderer::ComputeHammersleySequence( int32 n )
	{
		// Construct the sequence
		Vector2* points = new Vector2[n];
		float p, u, v;
		int32 k, kk, pos;
		for ( k = 0, pos = 0; k < n; k++ )
		{
			u = 0;
			for ( p = 0.5f, kk = k; kk; p *= 0.5f, kk >>= 1 )
				if ( kk & 1 ) // kk mod 2 == 1
					u += p;
			v = (k + 0.5f) / float( n );
			points[pos].x = u;
			points[pos].y = v;
			++pos;
		}

		// Fill the buffer
		_hammersleyBuffer = CreateBuffer<Vector2>( n, D3D11_USAGE_IMMUTABLE, false, true, points );
		//_effect->GetVariableByName( "g_Hammersley" )->AsShaderResource()->SetResource( *_hammersleyBuffer );
		delete[] points;
	}



	// Release objects created in OnResizeSwapChain
	void Renderer::OnReleasingSwapChain()
	{
		// Deferred shading stuff
		_gbuffer = nullptr;
		_gbufferHalfRes = nullptr;
		_aoTexture = nullptr;
		_aoBlurTexture = nullptr;
		_idTexture = nullptr;

		// Other render targets/buffers
		_velocityMap[0] = nullptr;
		_velocityMap[1] = nullptr;
		_postFXBuffer[0] = nullptr;
		_postFXBuffer[1] = nullptr;
		_lightIndexBuffer = nullptr;
		_clusterIndexBuffer = nullptr;
		_clusterIndexOffsetBuffer = nullptr;
		_halfResFrameBuffer[0] = nullptr;
		_halfResFrameBuffer[1] = nullptr;
		_frameBuffer = nullptr;

		// HDR
		SafeReleaseDelete( _hdr );

		_reflectionDepthStencil = nullptr;

		// The fullscreen quad buffer
		_quadVertexBuffer = nullptr;
	}


	// Release objects created in OnCreateDevice
	void Renderer::OnDestroyDevice()
	{
		SafeDelete( _box );

		// Unset all resources from the device
		_context->ClearState();

		// Clear the scene
		ClearScene();

		// Clustered shading stuff
		_pointLightListBuffer = nullptr;
		_spotLightListBuffer = nullptr;
		SafeDelete( _shadowManager );

		// Debug renderer
		SafeReleaseDelete( _visualizer );

		// SSAO jitter tex
		_hammersleyBuffer = nullptr;

		// Instancing
		_instanceBuffer = nullptr;

		// Free the input layouts
		SafeRelease( Vertex::pInputLayout );
		SafeRelease( Vertex::pStreamInputLayout );
		SafeRelease( Vertex::pInstancedInputLayout );
		SafeRelease( PosVertex::pInputLayout );
		SafeRelease( SpriteVertex::pInputLayout );
		SafeRelease( Vertex::pAnimationInputLayout );
		SafeRelease( PosVertex::pAnimationInputLayout );

		// Delete the sprite system
		SafeDelete( _spriteBatcher );

		// Water
		SafeReleaseDelete( _ocean );

		// Sky system
		SafeReleaseDelete( _sky );
		SafeDelete( _skybox );

		// Release the effect
		delete _effect;
		_effect = nullptr;
	}




#pragma endregion


#pragma region Scene Management

	// Clear the scene
	void Renderer::ClearScene()
	{
		// Bindings
		_state.ResetBindings();
		if ( _context ) _context->Flush();
		if ( _context ) _context->ClearState();
		_depthStencil->Clear();
		_backBuffer->Clear();
		InitVariables();

		// Quadtree
		SafeDelete( _quadtree );

		// Lights
		_lights.Release();

		// Models
		_models.Release();
		_visibleMeshes.Release();
		_visibletransparentMeshes.Release();
		_renderList.Release();

		// Terrain
		SafeDelete( _terrain );

		// Instances
		for ( auto iter = _instanceSets.begin(); iter != _instanceSets.end(); ++iter )
			delete iter->second;
		_instanceSets.clear();

		// Environment probes
		for ( uint32 i = 0; i < _probes.Size(); i++ )
		{
			_probes[i]->Release();
			delete _probes[i];
		}
		_probes.Release();
		_probeDepthStencil = nullptr;

		// Rendering lists
		_renderList.Release();
	}


	// Adds a light to the scene
	void Renderer::AddLight( Light* light )
	{
		// Add the light ptr to the generic light list
		_lights.Add( light );
		light->_active = true;

		if ( light->GetType() == LightType::Point )
		{
			if ( light->CastsShadows() )
				_pointShadowLights.Add( light );
			else
				_pointLights.Add( light );
		}

		if ( light->GetType() == LightType::Spot )
		{
			if ( light->CastsShadows() )
				_spotShadowLights.Add( light );
			else
				_spotLights.Add( light );
		}
	}


	// Removes a light from the scene
	void Renderer::RemoveLight( Light* light )
	{
		_lights.Remove( light );
		light->_active = false;

		if ( light->GetType() == LightType::Point )
		{
			if ( light->CastsShadows() )
				_pointShadowLights.Remove( light );
			else
				_pointLights.Remove( light );
		}

		if ( light->GetType() == LightType::Spot )
		{
			if ( light->CastsShadows() )
				_spotShadowLights.Remove( light );
			else
				_spotLights.Remove( light );
		}
	}

	// Create an empty mesh
	Model* Renderer::CreateMesh()
	{
		return new Model( GetEngine() );
	}


	// Adds a mesh object to the scene.  Animated meshes are added to the front of the
	// list, and static are added to the end.  This keeps things somewhat sorted
	// without having to maintain two seperate lists
	void Renderer::AddModel( Model* model )
	{
		// Add it to the scene
		_models.Add( model );

		// Sort into the render list
		uint32 modelID = (model->GetMeshBinding()->GetID() & 0xFFF);
		for ( uint32 i = 0; i < model->GetNumSubMesh(); i++ )
			AddSubmesh( model->GetSubMesh( i ) );
	}


	// Removes a mesh from the scene
	void Renderer::RemoveModel( Model* pMesh )
	{
		// Remove it from the mesh list
		_models.Remove( pMesh );

		// Remove from the render list
		for ( uint32 i = 0; i < pMesh->GetNumSubMesh(); i++ )
			RemoveSubmesh( pMesh->GetSubMesh( i ) );
	}

	// Deletes a mesh permanently
	void Renderer::DeleteMesh( Model* pMesh )
	{
		// Remove then delete
		RemoveModel( pMesh );
		pMesh->Release();
		delete pMesh;
		pMesh = nullptr;
	}

	// Deletes a sub-mesh permanently
	void Renderer::DeleteSubMesh( Model* pMesh, int32 index )
	{
		_renderList.Remove( pMesh->GetSubMesh( index ) );
	}


	// Create a new probe
	EnvironmentProbe* Renderer::AddProbe( bool dynamic )
	{
		EnvironmentProbe* probe = new EnvironmentProbe( this );
		if ( FAILED( probe->CreateCubemap( ProbeSettings::mipLevels, ProbeSettings::format, ProbeSettings::miplevels, dynamic ) ) )
			return nullptr;
		probe->GetTexture()->AttachDepthStencil( _probeDepthStencil );
		_probes.Add( probe );
		return probe;
	}


	// Add an file-based probe
	EnvironmentProbe* Renderer::AddProbe( const char* file )
	{
		EnvironmentProbe* probe = new EnvironmentProbe( this );
		if ( FAILED( probe->LoadCubemap( file ) ) )
			return nullptr;
		probe->GetTexture()->AttachDepthStencil( _probeDepthStencil );
		_probes.Add( probe );
		return probe;
	}


	// Remove a probe
	void Renderer::RemoveProbe( EnvironmentProbe* probe )
	{
		if ( !probe )
			return;

		// Remove the probe
		_probes.Remove( probe );
		probe->Release();
		delete probe;
	}


	// Checks if a light is within the camera view.  This does not take occulsion into account
	bool Renderer::IsLightVisible( Light& light )
	{
		M_ASSERT( _camera, "No camera exists in the scene" );
		return _camera->GetFrustum().CheckSphere( light.GetPosition(), light.GetRange() );
	}

	// Set the color of the ambient lighting
	void Renderer::SetAmbientLighting( float r, float g, float b )
	{
		_ambient = Vector4( r, g, b, 0 );
		_ambientVariable->SetFloatVector( (float*)&_ambient );
	}

	// Render all visible meshes
	void Renderer::RenderVisibleMeshes()
	{
		// Normal meshes
		for ( auto iter = _visibleMeshes.Begin(); !iter.End(); ++iter )
		{
			RenderNode* node = (RenderNode*)*iter;
			node->Render();
		}

		// Instances
		RenderInstanceSets();
	}

	// Render all instance sets
	void Renderer::RenderInstanceSets()
	{
		for ( auto iter = _instanceSets.begin(); iter != _instanceSets.end(); ++iter )
		{
			// Copy the instance data to the gpu
			InstanceSet* set = iter->second;
			auto instanceData = _instanceBuffer->Map( D3D11_MAP_WRITE_DISCARD );
			memcpy( instanceData, set->GetInstanceData(), sizeof(Matrix)* set->GetInstanceCount() );
			_instanceBuffer->Unmap();

			// Set buffers
			const RenderNode& node = *set->GetRenderNode();
			AddVertexStream( node.vertexBuffers[VERTEX_STREAM_POS], Vertex::pStrides[VERTEX_STREAM_POS] );
			AddVertexStream( node.vertexBuffers[VERTEX_STREAM_TEXCOORD], Vertex::pStrides[VERTEX_STREAM_TEXCOORD] );
			AddVertexStream( node.vertexBuffers[VERTEX_STREAM_NORMAL], Vertex::pStrides[VERTEX_STREAM_NORMAL] );
			AddVertexStream( _instanceBuffer->GetBuffer(), sizeof(Matrix) );
			SetInputLayout( Vertex::pInstancedInputLayout );
			BindVertexStreams();
			SetIndexBuffer( node.indexBuffer, DXGI_FORMAT_R32_UINT );

			// Set material and render
			SetMaterial( node.GetMaterialBinding() );
			SetWorldMatrix( *node.worldMatrix );
			if ( node.GetMaterialBinding()->IsAlphaTested() )
				_gbufferInstancedAlphaPass->Apply();
			else
				_gbufferInstancedPass->Apply();
			_context->DrawIndexedInstanced( node.indexCount, set->GetInstanceCount(), node.startIndex, 0, 0 );
			set->Clear();
		}

	}

	// Render all transparent meshes
	void Renderer::RenderTransparentMeshes()
	{
		for ( auto iter = _visibletransparentMeshes.Begin(); !iter.End(); ++iter )
		{
			RenderNode* node = (RenderNode*)*iter;
			node->Render();
		}
	}

	// Perform a pre-z fill pass
	void Renderer::ZFill()
	{
		// 		for ( auto iter = _visibleMeshes.Begin(); !iter.End(); ++iter )
		// 		{
		// 			auto node = *iter;
		// 			if ( !node->bonePalette )
		// 				RenderSubMeshPos( *node, _zfillPass, false );
		// 			else
		// 				RenderSubMeshPosSkinned( *node, _zfillSkinnedPass, false );
		// 		}
	}

	// Process a visible model
	void Renderer::ProcessVisibleMesh( QuadtreeElement* obj )
	{
		Model* model = (Model*)obj;
		for ( uint32 j = 0; j < model->GetNumSubMesh(); ++j )
		{
			// Get view space z depth for sorting
			RenderNode* node = model->GetSubMesh( j );
			node->depth = _camera->GetForwardVector().Dot( node->bounds.GetWorldPosition() - _camera->GetPosition() );

			if ( node->GetMaterialBinding()->GetMaterial()->IsTransparent() )
			{
				InsertVisibleTransparentRenderNode( node );
			}
			else
			{
				// Check if instancing should be used
				InstanceSet* instanceSet = nullptr;
				if(model->Instanced())
				{
					auto instanceIter = _instanceSets.find( node->GetInstanceID() );
					instanceSet = (instanceIter != _instanceSets.end()) ? instanceIter->second : nullptr;
				}
				if ( instanceSet && instanceSet->GetRefCount() > 1 )
				{
					instanceSet->AddInstance( *node->worldMatrix );
				}
				else
				{
					// Setup the sort key
					//uint32 depthMask = uint32( node->depth * 100.0f ) & 0xFFFF;
					//uint32 materialMask = (node->GetMaterialBinding()->GetMaterial()->GetID() & 0xFF) << 16;
					//uint32 effectMask = 0;//(node->GetEffectPass()->GetID() & 0xFF) << 24;
					node->sortKey = node->GetMaterialBinding()->GetMaterial()->GetID();
					InsertVisibleRenderNode( node );
				}
			}
		}
	}

	// Perform frustum culling for all meshes
	void Renderer::DetermineVisibleMeshes()
	{
		// Quadtree culling
		_visibleMeshes.Clear();
		_visibletransparentMeshes.Clear();
		if ( _quadtree )
		{
			_quadtree->GetVisibleObjects( _camera->GetFrustum(), &Renderer::ProcessVisibleMesh, this );
		}
		else
		{
			for ( uint32 i = 0; i < _models.Size(); ++i )
			{
				if ( _camera->GetFrustum().CheckAABB( _models[i]->GetBounds() ) )
				{
					ProcessVisibleMesh( _models[i] );
				}
			}
		}
	}


#pragma endregion



#pragma region Main Rendering

	// Renders the scene
	void Renderer::Render()
	{
		Clear();
		RenderScene();
		Present();
	}


	// Draws the scene
	void Renderer::RenderScene()
	{
		// Set the input layout and topology
		SetInputLayout( Vertex::pInputLayout );
		SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
		SetViewport( _hdr->Buffer()->GetViewport() );

		// Reset material
		SetMaterial( MaterialBinding::pointer( nullptr ) );

		if ( _camera )
		{

			// Update the terrain clipmaps
			uint32 offset = 0;
			if ( _showTerrain && GetTerrain() )
			{
				SetRenderToQuad();
				GetTerrain()->UpdateClipmaps( *_effect );
			}

			// Clear the render target
			float clearColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
			_hdr->Buffer()->Clear();
			_depthStencil->Clear();

			// Z-fill
			// 		BindDepthStencilWithoutRenderTarget( _depthStencil );
			// 		SetViewport( _hdr->Buffer()->GetViewport() );
			// 		ZFill();

			// Render the environment maps
			RenderProbes();

			// Render the meshes
			SetViewport( _hdr->Buffer()->GetViewport() );
			FillGBuffers();

			// Shading
			DeferredShading();

			// Draw the sky
			BindRenderTarget( *_hdr->Buffer() );
			if ( _hasSky )
				RenderSky( _camera->GetPosition(), _hdr->Buffer() );
			else if ( _skybox )
				_skybox->Render();

			// Draw water						// VTUNE: 5 - 10 FPS GAIN
#ifndef _DEBUG
			if ( HasOcean() )
				RenderWater();
#endif

			// Particle emitters
			BindRenderTargetWithReadOnlyDepth( *_hdr->Buffer() );
			_depthBufferVariable->SetResource( *_depthStencil );
			for ( uint32 i = 0; i < _emitters.Size(); ++i )
				_emitters[i]->Render();

			// Transparents
			BindRenderTargetWithReadOnlyDepth( *_hdr->Buffer() );
			RenderTransparentMeshes();

			// Copy the current frame buffer for refraction (should be able to optimize this out)
			CopyTextureToTexture( _frameBuffer, _hdr->Buffer() );
			_frameBufferVariable->SetResource( *_frameBuffer );

			// Render game specific special effects
			BindRenderTargetWithReadOnlyDepth( *_hdr->Buffer() );
			RenderEffects();

			// Downsample the hdr back buffer
			//  		SetRenderToQuad();
			// 		SetViewport( _halfResFrameBuffer[0]->GetViewport() );
			// 		BindRenderTargetWithoutDepth( *_halfResFrameBuffer[0] );
			// 		_postFxVariable->SetResource( *_hdr->Buffer() );
			// 		_downscalePass->Apply();
			// 		_context->Draw( 6, 0 );
			// 
			// 		// Screen space reflection
			// 		_halfResFrameBuffer[1]->Clear();
			// 		BindRenderTargetWithoutDepth( *_halfResFrameBuffer[1] );
			// 		_postFxVariable->SetResource( *_halfResFrameBuffer[0] );
			// 		_screenReflectPass->Apply();
			// 		_context->Draw( 6, 0 );
			// 
			// 		// Upsample the reflection texture
			// 		SetViewport( _hdr->Buffer()->GetViewport() );
			// 		BindRenderTargetWithoutDepth( *_hdr->Buffer() );
			// 		_postFxVariable->SetResource( *_halfResFrameBuffer[1] );
			// 		_bilateralUpsamplePass->Apply();
			// 		_context->Draw( 6, 0 );

			// Apply local reflections
			// 		_probeVariable->SetResource(*_probes[0]->GetTexture());
			// 		_localReflectionPass->Apply();
			// 		_context->Draw( 6, 0 );

			// Perform the HDR image processing
			_hdr->ProcessHDRI( _postFXBuffer[0] );

			// Motion blur
			{
				// Build a velocity map
				if ( _currentVelocityIndex == 0 ) { _currentVelocityIndex = 1; _previousVelocityIndex = 0; }
				else { _currentVelocityIndex = 0; _previousVelocityIndex = 1; }
				_velocityMap[_currentVelocityIndex]->Clear();
				BindRenderTargetWithoutDepth( *_velocityMap[_currentVelocityIndex] );
				SetViewport( _velocityMap[_currentVelocityIndex]->GetViewport() );
				_velocityMapPass->Apply();
				_context->Draw( 6, 0 );

				// Perform the blur
				BindRenderTargetWithoutDepth( *_postFXBuffer[1] );
				SetViewport( _hdr->Buffer()->GetViewport() );
				_frameBufferVariable->SetResource( *_postFXBuffer[0] );
				_postFxVariable->SetResource( *_velocityMap[_currentVelocityIndex] );
				_postFxVariable2->SetResource( *_velocityMap[_previousVelocityIndex] );
				_motionBlurPass->Apply();
				_context->Draw( 6, 0 );
			}

			// FXAA
			BindRenderTarget( *_backBuffer );
			SetViewport( _hdr->Buffer()->GetViewport() );
			_frameBufferVariable->SetResource( *_postFXBuffer[1] );
			_fxaaPass->Apply();
			_context->Draw( 6, 0 );

			// Dynamic meshes
			for ( uint32 i = 0; i < _dynamicMeshes.Size(); ++i )
				RenderDynamicMesh( *_dynamicMeshes[i] );
			_dynamicMeshes.Clear();
			SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
		}

		// Render sprites
		BindRenderTarget( *_backBuffer );
		_spriteBatcher->RenderSprites();

		// Render the debug info
		if ( _visualizer )
			_visualizer->Render();
	}


	// Renders the sky
	void Renderer::RenderSky( const Vector3& pos, Texture::pointer target, uint32 targetIndex )
	{
		// Compute the scattering
		if ( _skyNeedsUpdate )
		{
			SetRenderToQuad();
			_sky->ComputeScattering( *_camera );
			_skyNeedsUpdate = false;

			// Update the sun direction
			_sunDirection = _sky->GetSunDirection();
			_sunDirectionVariable->SetFloatVector( _sunDirection );
		}

		// Render the sky
		BindRenderTarget( *target, targetIndex );
		SetViewport( target->GetViewport() );
		_sky->Render( pos, _skyPass );
	}


	// Renders terrains
	void Renderer::RenderTerrain( EffectPass* pPass )
	{
		// Render the terrain
		if ( !GetTerrain() || !GetTerrain()->IsLoaded() || !_showTerrain )
			return;

		// Render the terrain
		SetInputLayout( PosVertex::pInputLayout );
		SetVertexBuffer( GetTerrain()->GetVertexBuffer(), PosVertex::size );
		SetIndexBuffer( GetTerrain()->GetIndexBuffer(), DXGI_FORMAT_R16_UINT );
		_terrain->BindLayerMaterials();

		_terrain->Render( pPass );
	}



	// Renders all the probes
	void Renderer::RenderProbes( bool forceRender )
	{
		// Make sure there are probes
		if ( _probes.IsEmpty() )
			return;

		// Set the viewport for rendering to the probes
		SetViewport( _probes[0]->GetTexture()->GetViewport() );

		// Now update all the probe render targets
		for ( uint32 i = 0; i < _probes.Size(); i++ )
		{
			if ( forceRender || (_probes[i]->IsDynamic() && _probes[i]->IsDirty() && _camera->GetFrustum().CheckSphere( _probes[i]->GetBounds() )) )
			{
				//_probes[i]->Update();
				RenderProbe( *_probes[i] );
			}
		}

		// Restore the camera view and projection matrices
		Matrix mViewProj = _camera->GetViewMatrix() * _camera->GetProjectionMatrix();
		_viewProjectionVariable->SetMatrix( mViewProj );
		_cameraPositionVariable->SetFloatVector( _camera->GetPosition() );
	}

	// Render the scene into the cube map
	void Renderer::RenderProbe( EnvironmentProbe& probe )
	{
		// Render to each face of the cube map
		Vector3 vLookDir;
		Vector3 vUpDir;
		Matrix mView;
		Frustum frustum;
		_probeProjMatrix.Perspective( Math::pi*0.5f, 1.0f, 0.05f, 300.0f );
		_cameraPositionVariable->SetFloatVector( probe.GetPosition() );
		for ( int32 j = 0; j < 6; j++ )
		{
			// Clear and set the render target/depth buffer
			probe.GetTexture()->Clear( j );
			probe.GetTexture()->ClearDSV();
			BindRenderTarget( *probe.GetTexture(), j );

			// Set the view matrix for this face
			switch ( (CubemapFace)j )
			{
				case CubemapFace::PositiveX:
					vLookDir = Vector3( 1.0f, 0.0f, 0.0f );
					vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
					break;
				case CubemapFace::NegativeX:
					vLookDir = Vector3( -1.0f, 0.0f, 0.0f );
					vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
					break;
				case CubemapFace::PositiveY:
					vLookDir = Vector3( 0.0f, 1.0f, 0.0f );
					vUpDir = Vector3( 0.0f, 0.0f, -1.0f );
					break;
				case CubemapFace::NegativeY:
					vLookDir = Vector3( 0.0f, -1.0f, 0.0f );
					vUpDir = Vector3( 0.0f, 0.0f, 1.0f );
					break;
				case CubemapFace::PositiveZ:
					vLookDir = Vector3( 0.0f, 0.0f, 1.0f );
					vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
					break;
				case CubemapFace::NegativeZ:
					vLookDir = Vector3( 0.0f, 0.0f, -1.0f );
					vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
					break;
			}

			// Set the view transform for this cubemap face
			auto v = probe.GetPosition();
			mView = Matrix::CreateLookAtView( probe.GetPosition(), probe.GetPosition() + vLookDir, vUpDir );
			frustum.Build( mView, _probeProjMatrix );
			mView *= _probeProjMatrix;
			_viewProjectionVariable->SetMatrix( mView );

			// Set the layout
			SetInputLayout( Vertex::pInputLayout );
			SetViewport( probe.GetTexture()->GetViewport() );

			// Now render each submesh
			for ( uint32 i = 0; i < _renderList.Size(); i++ )
			{
				RenderNode& mesh = *_renderList[i];
				if ( mesh.cubeMap != probe.GetTexture() && frustum.CheckAABB( mesh.bounds ) )
				{
					if ( mesh.bonePalette )
						RenderSubMeshSkinned( mesh, _forwardShadingSkinnedPass );
					else
						RenderSubMesh( mesh, _forwardShadingPass );
				}
			}

			// Render the terrain
			if ( _terrain )
				RenderTerrain( _forwardTerrainPass );

			// Render the sky
			if ( _hasSky )
				RenderSky( probe.GetPosition(), probe.GetTexture(), j );
		}

		// Generate the mip maps
		if ( Renderer::ProbeSettings::miplevels > 1 )
			probe.GetTexture()->GenerateMips();
	}

	// Saves a screenshot (Thanks to Jack Hoxley)
	void Renderer::TakeScreenShot()
	{
		/*static int32 s_FrameNumber = 0;
		++s_FrameNumber;
		String outDir = _engine->GetPlatform()->GetWorkingDirectory();
		outDir += "Screen ";
		outDir += s_FrameNumber;
		outDir += ".jpg";

		// Extract the actual back buffer
		ID3D10Texture2D* pBackBuffer;
		DXUTGetDXGISwapChain()->GetBuffer( 0, __uuidof( ID3D10Texture2D ), reinterpret_cast< void** >( &pBackBuffer ) );

		// Save the backbuffer directly
		if( FAILED( D3DX10SaveTextureToFileA( pBackBuffer, D3DX10_IFF_JPG , outDir.c_str() ) ) )
		{
		Log::_Print("Unable to save back-buffer texture to file!");
		return;
		}*/
	}

#pragma endregion


#pragma region Mesh Rendering

	// Render a mesh
	void Renderer::RenderSubMesh( RenderNode& mesh )
	{
		// Set buffers
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_POS], Vertex::pStrides[VERTEX_STREAM_POS] );
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_TEXCOORD], Vertex::pStrides[VERTEX_STREAM_TEXCOORD] );
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_NORMAL], Vertex::pStrides[VERTEX_STREAM_NORMAL] );
		SetInputLayout( Vertex::pStreamInputLayout );
		BindVertexStreams();
		SetIndexBuffer( mesh.indexBuffer, DXGI_FORMAT_R32_UINT );

		// Draw to screen
		_context->DrawIndexed( mesh.indexCount, mesh.startIndex, 0 );
	}

	// Render a mesh, no effects or variables are applied
	void Renderer::RenderSubMeshPos( RenderNode& mesh )
	{
		// Set buffers
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_POS], Vertex::pStrides[VERTEX_STREAM_POS] );
		if ( mesh.bonePalette )
		{
			AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_BONE], PosVertex::animationStride );
			SetInputLayout( PosVertex::pAnimationInputLayout );
		}
		else
			SetInputLayout( PosVertex::pInputLayout );
		BindVertexStreams();
		SetIndexBuffer( mesh.indexBuffer, DXGI_FORMAT_R32_UINT );

		// Draw to screen
		_context->DrawIndexed( mesh.indexCount, mesh.startIndex, 0 );
	}

	// Render a mesh, no effects or variables are applied
	void Renderer::RenderSubMeshSkinned( RenderNode& mesh )
	{
		// Set buffers
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_POS], Vertex::pStrides[VERTEX_STREAM_POS] );
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_TEXCOORD], Vertex::pStrides[VERTEX_STREAM_TEXCOORD] );
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_NORMAL], Vertex::pStrides[VERTEX_STREAM_NORMAL] );
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_BONE], Vertex::pStrides[VERTEX_STREAM_BONE] );
		SetInputLayout( Vertex::pAnimationInputLayout );
		BindVertexStreams();
		SetIndexBuffer( mesh.indexBuffer, DXGI_FORMAT_R32_UINT );

		// Draw to screen
		_context->DrawIndexed( mesh.indexCount, mesh.startIndex, 0 );
	}

	// Render a mesh, no effects or variables are applied
	void Renderer::RenderSubMeshPosSkinned( RenderNode& mesh )
	{
		// Set buffers
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_POS], Vertex::pStrides[VERTEX_STREAM_POS] );
		AddVertexStream( mesh.vertexBuffers[VERTEX_STREAM_BONE], Vertex::pStrides[VERTEX_STREAM_BONE] );
		SetInputLayout( PosVertex::pAnimationInputLayout );
		BindVertexStreams();
		SetIndexBuffer( mesh.indexBuffer, DXGI_FORMAT_R32_UINT );

		// Draw to screen
		_context->DrawIndexed( mesh.indexCount, mesh.startIndex, 0 );
	}


	// Render a submesh, automatically applying the material/world matrix
	void Renderer::RenderSubMesh( RenderNode& mesh, EffectPass* effect, bool useMaterial )
	{
		if ( useMaterial )
		{
			SetMaterial( mesh.GetMaterialBinding() );
			SetCubeMap( mesh.cubeMap );
		}
		SetWorldMatrix( *mesh.worldMatrix );
		_objectIDVariable->SetInt( mesh.GetModel()->GetID() );
		effect->Apply();
		RenderSubMesh( mesh );
	}

	// Render a submesh, automatically applying the material/world matrix
	void Renderer::RenderSubMeshPos( RenderNode& mesh, EffectPass* effect, bool useMaterial )
	{
		if ( useMaterial )
			SetMaterial( mesh.GetMaterialBinding() );
		SetWorldMatrix( *mesh.worldMatrix );
		_objectIDVariable->SetInt( mesh.GetModel()->GetID() );
		effect->Apply();
		RenderSubMeshPos( mesh );
	}

	// Render a submesh, automatically applying the material/world matrix
	void Renderer::RenderSubMeshSkinned( RenderNode& mesh, EffectPass* effect, bool useMaterial )
	{
		if ( useMaterial )
		{
			SetMaterial( mesh.GetMaterialBinding() );
			SetCubeMap( mesh.cubeMap );
		}
		SetWorldMatrix( *mesh.worldMatrix );
		_bonePaletteVariable->SetResource( *mesh.bonePalette );
		_objectIDVariable->SetInt( mesh.GetModel()->GetID() );
		effect->Apply();
		RenderSubMeshSkinned( mesh );
	}

	// Render a submesh, automatically applying the material/world matrix
	void Renderer::RenderSubMeshPosSkinned( RenderNode& mesh, EffectPass* effect, bool useMaterial )
	{
		if ( useMaterial )
			SetMaterial( mesh.GetMaterialBinding() );
		SetWorldMatrix( *mesh.worldMatrix );
		_bonePaletteVariable->SetResource( *mesh.bonePalette );
		effect->Apply();
		RenderSubMeshPosSkinned( mesh );
	}



	// Draw a submesh a number of times
	void Renderer::RenderSubMeshInstanced( RenderNode& mesh, EffectPass* pass, int32 numInstances )
	{
		// Set buffers
		static const uint32 offset = 0;
		//SetVertexBuffer(mesh.pVertexBuffer, Vertex::size);
		SetIndexBuffer( mesh.indexBuffer, DXGI_FORMAT_R32_UINT );

		// Apply the shaders
		_objectIDVariable->SetInt( 0 );
		pass->Apply();

		// Draw to screen
		_context->DrawIndexedInstanced( mesh.indexCount, numInstances, mesh.startIndex, 0, 0 );
	}

#pragma endregion



#pragma region Water Rendering


	// Draw ocean water
	void Renderer::RenderWater()
	{
		// Draw the reflection texture
		RenderWaterReflection( _postFXBuffer[0] );

		// Animate normal map coords based on time
		static Vector4 normalCoords;
		normalCoords.x += 0.42f * _engine->GetFrameTime();
		normalCoords.z += 0.15f * _engine->GetFrameTime();
		normalCoords.w += 0.18f * _engine->GetFrameTime();
		_waterUVVariable->SetFloatVector( (float*)&normalCoords );

		// Copy the current frame buffer for refraction (should be able to optimize this out)
		CopyTextureToTexture( _frameBuffer, _hdr->Buffer() );
		_frameBufferVariable->SetResource( *_frameBuffer );

		// Bind render target
		BindRenderTargetWithReadOnlyDepth( *_hdr->Buffer() );

		// Render the water grid
		SetInputLayout( Vertex::pInputLayout );
		_waveFieldVariable->SetResource( *_ocean->GetWaveField() );
		_waterNormalVariable->SetResource( *_ocean->GetWaveNormals() );
		SetMaterial( _ocean->GetMaterial() );
		_skyCubeVariable->SetResource( *_sky->GetCubemap() );
		_postFxVariable->SetResource( *_postFXBuffer[0] );
		SetVertexBuffer( _ocean->GetVertexBuffer(), Vertex::size );
		SetIndexBuffer( _ocean->GetIndexBuffer(), DXGI_FORMAT_R32_UINT );
		_worldMatrixVariable->SetMatrix( Matrix::CreateTranslation( 0, _ocean->GetWaterLevel(), 0 ) );
		_ocean->SetShaderData();
		_waterPass->Apply();
		_context->DrawIndexed( _ocean->GetNumIndices(), 0, 0 );
	}



	// Creates a planar reflection texture
	void Renderer::RenderWaterReflection( Texture::pointer surface )
	{
		// For now do simple reflection about the water plane, add generalized later
		// todo: add reflection to matrix
		Matrix matRef;
		matRef( 1, 1 ) = -1;
		matRef( 3, 1 ) = 2 * _ocean->GetWaterLevel();
		_mirrorMatrixVariable->SetMatrix( matRef );
		_reflectionDepthStencil->Clear();
		surface->Clear();
		BindRenderTarget( *surface );
		_context->RSSetViewports( 1, &surface->GetViewport() );

		// Render terrain
		RenderTerrain( _terrainReflectPass );

		// Meshes
		for ( uint32 i = 0; i < _renderList.Size(); i++ )
			RenderSubMesh( *_renderList[i], _reflectPass );
	}


#pragma endregion


#pragma region State Management

	// Set the quad vertex buffer
	void Renderer::SetRenderToQuad()
	{
		SetVertexBuffer( _quadVertexBuffer->GetBuffer(), Vertex::size );
		SetInputLayout( Vertex::pInputLayout );
	}

	// Checks if a mesh within the camera view.  This does not take occulsion into account
	bool Renderer::IsMeshVisible( Model& mesh, Camera& camera )
	{
		return camera.GetFrustum().CheckSphere( mesh.GetBounds() );
	}


	// Checks if a submesh within the camera view.  This does not take occulsion into account
	bool Renderer::IsMeshVisible( RenderNode& mesh, Camera& camera )
	{
		return camera.GetFrustum().CheckSphere( mesh.bounds );
	}


#pragma endregion





#pragma region Effect Stuff

	// Setup the shader variables
	HRESULT Renderer::InitVariables()
	{
		// Get the passes
		_zfillPass = _effect->GetPassByName( "ZFill" );
		_zfillSkinnedPass = _effect->GetPassByName( "ZFillSkinned" );
		_wireframeState = _effect->GetPassByName( "Wireframe" );
		_noCullState = _effect->GetPassByName( "NoCull" );
		_smoothTesselateState = _effect->GetPassByName( "SmoothTessellation" );
		_forwardPass = _effect->GetPassByName( "Unlit" );
		_forwardTerrainPass = _effect->GetPassByName( "ForwardTerrain" );
		_forwardShadingPass = _effect->GetPassByName( "Shade" );
		_forwardShadingAlphaPass = _effect->GetPassByName( "ShadeAlpha" );
		_forwardShadingInstancedPass = _effect->GetPassByName( "ShadeInstanced" );
		_forwardShadingSkinnedPass = _effect->GetPassByName( "ShadeSkinned" );
		_reflectPass = _effect->GetPassByName( "UnlitReflected" );
		_terrainReflectPass = _effect->GetPassByName( "ForwardTerrainReflect" );
		_fxaaPass = _effect->GetPassByName( "FXAA" );
		_positionOnlyWireframePass = _effect->GetPassByName( "PosWireframe" );
		_positionOnlyColorPass = _effect->GetPassByName( "PosColor" );
		_skyPass = _effect->GetPassByName( "Sky" );
		_waterPass = _effect->GetPassByName( "Water" );
		_velocityMapPass = _effect->GetPassByName( "VelocityMap" );
		_motionBlurPass = _effect->GetPassByName( "MotionBlur" );
		_gaussianBlurHorizontalPass = _effect->GetPassByName( "GaussianBlurH" );
		_gaussianBlurVerticalPass = _effect->GetPassByName( "GaussianBlurV" );
		_boxBlurHorizontalPass = _effect->GetPassByName( "BoxBlurH" );
		_boxBlurVerticalPass = _effect->GetPassByName( "BoxBlurV" );
		_bilateralBlurHorizontalPass = _effect->GetPassByName( "BilateralBlurH" );
		_bilateralBlurVerticalPass = _effect->GetPassByName( "BilateralBlurV" );
		_downscalePass = _effect->GetPassByName( "Downscale" );
		_bilateralUpsamplePass = _effect->GetPassByName( "BilateralUpsample" );
		_screenReflectPass = _effect->GetPassByName( "ScreenSpaceReflection" );
		_localReflectionPass = _effect->GetPassByName( "LocalReflection" );

		// Misc textures
		_frameBufferVariable = _effect->GetVariableByName( "g_txFrameBuffer" )->AsShaderResource();
		_bonePaletteVariable = _effect->GetVariableByName( "g_BonePalette" )->AsShaderResource();
		_depthBufferVariable = _effect->GetVariableByName( "g_txDepth" )->AsShaderResource();
		_objectIDVariable = _effect->GetVariableByName( "g_ObjectID" )->AsScalar();

		// Water
		_mirrorMatrixVariable = _effect->GetVariableByName( "g_mMirror" )->AsMatrix();
		_skyCubeVariable = _effect->GetVariableByName( "g_txSkyCube" )->AsShaderResource();
		_waterUVVariable = _effect->GetVariableByName( "g_WaterUV" )->AsVector();
		_waveFieldVariable = _effect->GetVariableByName( "g_txWater" )->AsShaderResource();
		_waterNormalVariable = _effect->GetVariableByName( "g_txWaterNormals" )->AsShaderResource();

		// HDR
		_elapsedTimeVariable = _effect->GetVariableByName( "g_ElapsedTime" )->AsScalar();
		_timerVariable = _effect->GetVariableByName( "g_Timer" )->AsScalar();

		// Scene
		_farZVariable = GetEffect()->GetVariableByName( "g_FarZ" )->AsScalar();
		_nearZVariable = GetEffect()->GetVariableByName( "g_NearZ" )->AsScalar();
		_ambientVariable = GetEffect()->GetVariableByName( "g_Ambient" )->AsVector();

		// Post processing
		_postFxVariable = _effect->GetVariableByName( "g_txPostFX" )->AsShaderResource();
		_postFxVariable2 = _effect->GetVariableByName( "g_txPostFX2" )->AsShaderResource();
		_filterKernelVariable = _effect->GetVariableByName( "g_FilterKernel" )->AsScalar();

		// Material Properties
		_materialTextureVariable = _effect->GetVariableByName( "g_txMaterial" )->AsShaderResource();
		_materialTextureFlagVariable = _effect->GetVariableByName( "g_MaterialTextureFlag" )->AsScalar();
		_probeVariable = _effect->GetVariableByName( "g_txCubeProbe" )->AsShaderResource();

		// Clustered shading
		_spotLightListVariable = _effect->GetVariableByName( "g_SpotLights" )->AsShaderResource();
		_pointLightListVariable = _effect->GetVariableByName( "g_PointLights" )->AsShaderResource();
		_lightIndexDataVariable = _effect->GetVariableByName( "g_ClusterIndexData" )->AsShaderResource();
		_clusterIndicesVariable = _effect->GetVariableByName( "g_ClusterIndices" )->AsShaderResource();
		_tileWidthVariable = _effect->GetVariableByName( "g_TileWidth" )->AsScalar();
		_tileHeightVariable = _effect->GetVariableByName( "g_TileHeight" )->AsScalar();
		_clusterSlicesVariable = _effect->GetVariableByName( "g_NumSlices" )->AsScalar();
		_tileSizeVariable = _effect->GetVariableByName( "g_TileSize" )->AsScalar();
		_sunDirectionVariable = _effect->GetVariableByName( "g_SunDirection" )->AsVector();
		_sunColorVariable = _effect->GetVariableByName( "g_SunColor" )->AsVector();
		_clusterLightCullPointCS = _effect->GetPassByName( "ClusterLightCullPoint" );
		_clusterLightCullPointShadowCS = _effect->GetPassByName( "ClusterLightCullPointShadow" );
		_clusterLightCullSpotCS = _effect->GetPassByName( "ClusterLightCullSpot" );
		_clusterLightCullSpotShadowCS = _effect->GetPassByName( "ClusterLightCullSpotShadow" );
		_resetClustersCS = _effect->GetPassByName( "ResetClusters" );
		_clusterIndicesOffsetUAV = _effect->GetVariableByName( "g_IndexOffset" )->AsUnorderedAccessView();
		_lightIndexDataUAV = _effect->GetVariableByName( "g_IndexDataRW" )->AsUnorderedAccessView();
		_clusterIndicesUAV = _effect->GetVariableByName( "g_ClusterIndicesRW" )->AsUnorderedAccessView();
		_maxLightsPerClusterVariable = _effect->GetVariableByName( "g_MaxLightsPerCluster" )->AsScalar();
		_numClustersVariable = _effect->GetVariableByName( "g_NumClusters" )->AsScalar();
		_numSpotLightsVariable = _effect->GetVariableByName( "g_NumSpotLights" )->AsScalar();
		_numPointLightsVariable = _effect->GetVariableByName( "g_NumPointLights" )->AsScalar();
		_numShadowSpotLightsVariable = _effect->GetVariableByName( "g_NumShadowSpotLights" )->AsScalar();
		_numShadowPointLightsVariable = _effect->GetVariableByName( "g_NumShadowPointLights" )->AsScalar();

		// Transforms and camera variables
		_worldMatrixVariable = _effect->GetVariableByName( "g_mWorld" )->AsMatrix();
		_viewProjectionVariable = _effect->GetVariableByName( "g_mViewProjection" )->AsMatrix();
		_previousViewProjectionVariable = _effect->GetVariableByName( "g_mOldViewProjection" )->AsMatrix();
		_inverseViewProjectionVariable = _effect->GetVariableByName( "g_mInvViewProjection" )->AsMatrix();
		_cameraPositionVariable = _effect->GetVariableByName( "g_CameraPos" )->AsVector();
		_cameraDirectionVariable = _effect->GetVariableByName( "g_CameraDir" )->AsVector();
		_screenSizeVariable = _effect->GetVariableByName( "g_ScreenSize" )->AsVector();
		_projMatrixVariable = _effect->GetVariableByName( "g_mProj" )->AsMatrix();
		_viewMatrixVariable = _effect->GetVariableByName( "g_mView" )->AsMatrix();
		_projRatioVariable = _effect->GetVariableByName( "g_ProjRatio" )->AsVector();
		_screenToWorldXVariable = _effect->GetVariableByName( "g_ScreenToWorldX" )->AsVector();
		_screenToWorldYVariable = _effect->GetVariableByName( "g_ScreenToWorldY" )->AsVector();
		_screenToWorldZVariable = _effect->GetVariableByName( "g_ScreenToWorldZ" )->AsVector();
		_fovVariable = _effect->GetVariableByName( "g_FoV" )->AsScalar();

		// Forward passes
		_forwardTerrainPass = _effect->GetPassByName( "ForwardTerrain" );

		// Material blendling
		_blendMaterialPass = _effect->GetPassByName( "MaterialBlend" );
		_bumpToNormalPass = _effect->GetPassByName( "BumpToNormal" );
		_heightInNormalPass = _effect->GetPassByName( "MergeHeightAndNormal" );
		_blendTextureBaseVariable = _effect->GetVariableByName( "g_txBlendBase" )->AsShaderResource();
		_blendTextureLayerVariable = _effect->GetVariableByName( "g_txBlendLayer" )->AsShaderResource();
		_blendTextureMaskVariable = _effect->GetVariableByName( "g_txBlendMask" )->AsShaderResource();

		// Render states
		_frontCullRS = _effect->GetVariableByName( "FrontCullRS" )->AsRasterizerState();
		_defaultRS = _effect->GetVariableByName( "DefaultRS" )->AsRasterizerState();
		_wireframeRS = _effect->GetVariableByName( "WireframeRS" )->AsRasterizerState();

		// Deferred shading effect vars
		_gbufferColorVariable = _effect->GetVariableByName( "g_txColor" )->AsShaderResource();
		_gbufferNormalVariable = _effect->GetVariableByName( "g_txNormal" )->AsShaderResource();
		_gbufferFlatNormalVariable = _effect->GetVariableByName( "g_txFlatNormal" )->AsShaderResource();
		_gbufferMaterialVariable = _effect->GetVariableByName( "g_txProperties" )->AsShaderResource();
		_gbufferEmissiveVariable = _effect->GetVariableByName( "g_txEmissive" )->AsShaderResource();
		_gbufferIDVariable = _effect->GetVariableByName( "g_txObjectID" )->AsShaderResource();
		_deferredCubeIBLPass = _effect->GetPassByName( "CubeIBL" );
		_shadingPass = _effect->GetPassByName( "ShadeScene" );
		_shadingPassNoSun = _effect->GetPassByName( "ShadeSceneNoSun" );
		_shadingPassNoSunShadows = _effect->GetPassByName( "ShadeSceneSunNoShadow" );
		_decalTextureVariable = _effect->GetVariableByName( "g_txDecal" )->AsShaderResource();
		_decalColorVariable = _effect->GetVariableByName( "g_DecalColor" )->AsVector();
		_decalMatrixVariable = _effect->GetVariableByName( "g_DecalMatrix" )->AsMatrix();
		_decalPass = _effect->GetPassByName( "Decal" );

		// GBuffer fills
		_gbufferPass = _effect->GetPassByName( "GBuffer" );
		_gbufferAlphaPass = _effect->GetPassByName( "GBufferAlpha" );
		_gbufferSkinnedPass = _effect->GetPassByName( "GBufferSkinned" );
		_gbufferCubemapPass = _effect->GetPassByName( "GBufferCubeMap" );
		_gbufferRefractPass = _effect->GetPassByName( "GBufferRefract" );
		_gbufferTerrainPass = _effect->GetPassByName( "GBufferTerrain" );
		_gbufferDownscalePass = _effect->GetPassByName( "DownscaleGBuffer" );
		_gbufferInstancedPass = _effect->GetPassByName( "GBufferInstanced" );
		_gbufferInstancedAlphaPass = _effect->GetPassByName( "GBufferInstancedAlpha" );

		// Misc
		_ambientOcclusionPass = _effect->GetPassByName( "AmbientOcclusion" );

		// Transparency
		_transparentPass = _effect->GetPassByName( "TransparentShade" );
		_transparentAlphaPass = _effect->GetPassByName( "TransparentShadeAlpha" );
		_transparentSkinnedPass = _effect->GetPassByName( "TransparentShadeSkinned" );

		return S_OK;
	}

#pragma endregion



	// Linearly blend two materials
	void Renderer::BlendMaterials( MaterialBinding::pointer base, MaterialBinding::pointer layer, Texture::pointer mask, MaterialBinding::pointer result )
	{
		// Blend each texture
		_blendTextureMaskVariable->SetResource( *mask );
		for ( int32 i = 0; i < TEX_SIZE; ++i )
		{
			TEX_TYPE type = (TEX_TYPE)i;
			Texture::pointer baseTex = base->GetTexture( type );
			Texture::pointer layerTex = layer->GetTexture( type );

			// If the layer doesn't have this texture, skip it			
			if ( !layerTex )
				continue;

			// If the base does not have the texture, just assign it
			if ( !baseTex )
			{
				result->SetTexture( layerTex, type );
				continue;
			}

			// Otherwise we need a full on blend, into a new texture
			// Always using the base texture size
			if ( !result->GetTexture( type ) || !result->GetTexture( type )->GetRTV() )
			{
				Texture::pointer blendedTex = CreateTexture( baseTex->GetWidth(), baseTex->GetHeight(), DXGI_FORMAT_R8G8B8A8_UNORM,
					0, nullptr, D3D11_USAGE_DEFAULT, true, true );
				result->SetTexture( blendedTex, type );
			}
			SetRenderToQuad();
			SetViewport( result->GetTexture( type )->GetViewport() );
			BindRenderTarget( *result->GetTexture( type ) );
			_blendTextureBaseVariable->SetResource( *baseTex );
			_blendTextureLayerVariable->SetResource( *layerTex );
			_blendMaterialPass->Apply();
			_context->Draw( 6, 0 );
		}
	}



	// Compute a normal map from a bump map
	Texture::pointer Renderer::ComputeNormalMap( Texture::pointer bumpMap )
	{
		// Create the normal map texture
		Texture::pointer normalMap = CreateTexture( bumpMap->GetWidth(), bumpMap->GetHeight(), DXGI_FORMAT_R8G8B8A8_UNORM,
			0, nullptr, D3D11_USAGE_DEFAULT, true, true );

		// Run the shader
		_blendTextureBaseVariable->SetResource( *bumpMap );
		BindRenderTarget( *normalMap );
		SetRenderToQuad();
		SetViewport( normalMap->GetViewport() );
		_bumpToNormalPass->Apply();
		_context->Draw( 6, 0 );

		// Mips
		normalMap->GenerateMips();

		return normalMap;
	}



	// Store a height map in the alpha channel of a normal map
	Texture::pointer Renderer::StoreHeightInNormalMap( Texture::pointer normalMap, Texture::pointer heightMap )
	{
		// Create the normal map texture
		Texture::pointer newNormalMap = CreateTexture( normalMap->GetWidth(), normalMap->GetHeight(), DXGI_FORMAT_R8G8B8A8_UNORM,
			0, nullptr, D3D11_USAGE_DEFAULT, true, true );

		// Run the shader
		_blendTextureBaseVariable->SetResource( *normalMap );
		_blendTextureLayerVariable->SetResource( *heightMap );
		BindRenderTarget( *newNormalMap );
		SetRenderToQuad();
		SetViewport( newNormalMap->GetViewport() );
		_heightInNormalPass->Apply();
		_context->Draw( 6, 0 );

		// Mips
		newNormalMap->GenerateMips();

		return newNormalMap;
	}


	// Set the camera for the current scene
	void Renderer::SetCamera( Camera* camera )
	{
		if ( _camera )
			_components.Remove( _camera );
		_camera = camera;
		if ( _camera )
		{
			_components.Add( _camera );
			_camera->BuildProjectionMatrix( GetWidth(), GetHeight() );
			_farZVariable->SetFloat( _camera->GetFarZ() );
			_nearZVariable->SetFloat( _camera->GetNearZ() );
			_projMatrixVariable->SetMatrix( _camera->GetProjectionMatrix() );
		}
	}

	// Creates a new light
	Light* Renderer::CreateLight() const
	{
		return new Light( _engine );
	}

	// Render a dynamic mesh
	void Renderer::RenderDynamicMesh( DynamicMesh& mesh )
	{
		if ( !mesh._vertexBuffer )
			return;

		// World matrix
		SetWorldMatrix( mesh._transform );

		// Set the material
		SetMaterial( mesh._material );

		// Setup the effect pass
		_positionOnlyColorPass->Apply( nullptr, nullptr, mesh._wireframe ? _wireframeRS : nullptr );

		// Set render mode
		SetPrimitiveTopology( mesh._topology );

		// Set buffers
		SetVertexBuffer( mesh._vertexBuffer->GetBuffer(), sizeof(Vector3) );
		SetInputLayout( PosVertex::pInputLayout );

		// Render
		if ( mesh._indexBuffer )
		{
			SetIndexBuffer( mesh._indexBuffer->GetBuffer(), DXGI_FORMAT_R32_UINT );
			_context->DrawIndexed( mesh._numIndices, 0, 0 );
		}
		else
		{
			_context->Draw( mesh._numVerts, 0 );
		}
	}

	// Build the light list each frame
	void Renderer::BuildVisibleLightBuffer()
	{
		// Clear the light lists
		_visibleSpotShadowLights.Clear();
		_visibleSpotLights.Clear();
		_visiblePointShadowLights.Clear();
		_visiblePointLights.Clear();
		_numVisibleLights = 0;

		// Reset active clusters
		_activeClusters.clear();

		// Shadow casting spot lights
		uint32 spotIndex = 0;
		for ( uint32 i = 0; i < _spotShadowLights.Size(); ++i )
		{
			Light& light = *_spotShadowLights[i];
			if ( IsLightVisible( light ) )
			{
				ProcessLightClusters( _camera->GetViewMatrix(), _viewProjMatrix, light, spotIndex,
					ClusterLightType::SpotShadow, _activeClusters );
				_visibleSpotShadowLights.Add( &light );
				memcpy( &_spotLightData[spotIndex++], light.GetShaderData(), _spotLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Shadow casting point lights
		uint32 pointIndex = 0;
		for ( uint32 i = 0; i < _pointShadowLights.Size(); ++i )
		{
			Light& light = *_pointShadowLights[i];
			if ( IsLightVisible( light ) )
			{
				ProcessLightClusters( _camera->GetViewMatrix(), _viewProjMatrix, light, pointIndex,
					ClusterLightType::PointShadow, _activeClusters );
				_visiblePointShadowLights.Add( &light );
				memcpy( &_pointLightData[pointIndex++], light.GetShaderData(), _pointLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Spot lights
		for ( uint32 i = 0; i < _spotLights.Size(); ++i )
		{
			Light& light = *_spotLights[i];
			if ( IsLightVisible( light ) )
			{
				ProcessLightClusters( _camera->GetViewMatrix(), _viewProjMatrix, light, spotIndex,
					ClusterLightType::Spot, _activeClusters );
				_visibleSpotLights.Add( &light );
				memcpy( &_spotLightData[spotIndex++], light.GetShaderData(), _spotLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Point lights
		for ( uint32 i = 0; i < _pointLights.Size(); ++i )
		{
			Light& light = *_pointLights[i];
			if ( IsLightVisible( light ) )
			{
				ProcessLightClusters( _camera->GetViewMatrix(), _viewProjMatrix, light, pointIndex,
					ClusterLightType::Point, _activeClusters );
				_visiblePointLights.Add( &light );
				memcpy( &_pointLightData[pointIndex++], light.GetShaderData(), _pointLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Render all shadow maps
		_shadowManager->RenderSpotLightShadows( _visibleSpotShadowLights );
		_shadowManager->RenderPointLightShadows( _visiblePointShadowLights );
		if ( _hasSky && _sunShadows )
			_shadowManager->RenderSunShadows( _sunDirection, _sunShadowSoftness );

		// Process the visible clusters
		if ( _activeClusters.size() )
			ProcessClusters();

		// Copy visible light data
		if ( spotIndex )
			_spotLightListBuffer->Update( _spotLightData, 0, spotIndex );

		// Copy visible light data
		if ( pointIndex )
			_pointLightListBuffer->Update( _pointLightData, 0, pointIndex );

		// Copy index data to the gpu
		_lightIndexBuffer->Update( _lightIndexData, 0, _lightIndexData.Size() );

		// Copy cluster indices to the gpu
		if ( _clusterIndices.Size() )
			_clusterIndexBuffer->Update( _clusterIndices, 0, _clusterIndices.Size() );

	}


	// Perform cluster light culling on the GPU
	void Renderer::BuildVisibleLightBufferGPU()
	{
		// Clear the light lists
		_visibleSpotShadowLights.Clear();
		_visibleSpotLights.Clear();
		_visiblePointShadowLights.Clear();
		_visiblePointLights.Clear();
		_numVisibleLights = 0;

		// Shadow casting spot lights
		uint32 spotIndex = 0;
		for ( uint32 i = 0; i < _spotShadowLights.Size(); ++i )
		{
			Light& light = *_spotShadowLights[i];
			if ( IsLightVisible( light ) )
			{
				_visibleSpotShadowLights.Add( &light );
				memcpy( &_spotLightData[spotIndex++], light.GetShaderData(), _spotLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Shadow casting point lights
		uint32 pointIndex = 0;
		for ( uint32 i = 0; i < _pointShadowLights.Size(); ++i )
		{
			Light& light = *_pointShadowLights[i];
			if ( IsLightVisible( light ) )
			{
				_visiblePointShadowLights.Add( &light );
				memcpy( &_pointLightData[pointIndex++], light.GetShaderData(), _pointLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Spot lights
		for ( uint32 i = 0; i < _spotLights.Size(); ++i )
		{
			Light& light = *_spotLights[i];
			if ( IsLightVisible( light ) )
			{
				_visibleSpotLights.Add( &light );
				memcpy( &_spotLightData[spotIndex++], light.GetShaderData(), _spotLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Point lights
		for ( uint32 i = 0; i < _pointLights.Size(); ++i )
		{
			Light& light = *_pointLights[i];
			if ( IsLightVisible( light ) )
			{
				_visiblePointLights.Add( &light );
				memcpy( &_pointLightData[pointIndex++], light.GetShaderData(), _pointLightListBuffer->GetByteStride() );
				++_numVisibleLights;
			}
		}

		// Render all shadow maps
		_shadowManager->RenderSpotLightShadows( _visibleSpotShadowLights );
		_shadowManager->RenderPointLightShadows( _visiblePointShadowLights );
		if ( _hasSky && _sunShadows )
			_shadowManager->RenderSunShadows( _sunDirection, _sunShadowSoftness );

		// Copy visible light data
		if ( spotIndex )
			_spotLightListBuffer->Update( _spotLightData, 0, spotIndex );

		// Copy visible light data
		if ( pointIndex )
			_pointLightListBuffer->Update( _pointLightData, 0, pointIndex );

		// Update counts
		uint32 numSpotLights = spotIndex - _visibleSpotShadowLights.Size();
		uint32 numPointLights = pointIndex - _visiblePointShadowLights.Size();
		_numSpotLightsVariable->SetInt( numSpotLights );
		_numPointLightsVariable->SetInt( numPointLights );
		_numShadowSpotLightsVariable->SetInt( _visibleSpotShadowLights.Size() );
		_numShadowPointLightsVariable->SetInt( _visiblePointShadowLights.Size() );

		// Clear the clusters
		const uint32 dispatchSize = 64u;
		_resetClustersCS->Apply();
		_context->Dispatch( (_numClusters / dispatchSize) + 1, 1, 1 );

		// Build light lists for each cluster
		if ( _visibleSpotShadowLights.Size() )
		{
			_clusterLightCullSpotShadowCS->Apply();
			_context->Dispatch( (_visibleSpotShadowLights.Size() / dispatchSize) + 1, 1, 1 );
		}

		if ( _visiblePointShadowLights.Size() )
		{
			_clusterLightCullPointShadowCS->Apply();
			_context->Dispatch( (_visiblePointShadowLights.Size() / dispatchSize) + 1, 1, 1 );
		}

		if ( numSpotLights )
		{
			_clusterLightCullSpotCS->Apply();
			_context->Dispatch( (numSpotLights / dispatchSize) + 1, 1, 1 );
		}

		if ( numPointLights )
		{
			_clusterLightCullPointCS->Apply();
			_context->Dispatch( (numPointLights / dispatchSize) + 1, 1, 1 );
		}
	}


	// Construct the clusters in view space
	void Renderer::BuildClusters()
	{
		// Setup the depth distribution
		const float farZ = _camera->GetFarZ();
		const float nearZ = _camera->GetNearZ();
		//		float depths[] = { nearZ, 5.0f, 6.8f, 9.2f, 12.6f, 17.1f, 23.2f, 31.5f, 42.9f, 58.3f, 79.2f, 108, 146, 199, 271, 368, 500 };
		Array<float> depths( _clusterSlices + 1 );
		for ( uint32 i = 0; i <= _clusterSlices; ++i )
		{
			float depth = float( i ) / float( _clusterSlices );
			depths[i] = nearZ + depth*(farZ - nearZ);
		}

		// Build cluster set
		Vector4 clipCorners[4];
		Vector3 corners[4], start[4], end[4], dummy;
		uint32 tileWidth = (uint32)ceil( (float)_width / (float)_tileSize ) * _tileSize;
		uint32 tileHeight = (uint32)ceil( (float)_height / (float)_tileSize ) * _tileSize;
		_clusters.Allocate( _numClusters );
		uint32 clusterIndex = 0;
		const Matrix inverseProjectionMatrix = _camera->GetProjectionMatrix().GetInverse();
		for ( float y = 0; y < tileHeight; y += _tileSize )
		{
			for ( float x = 0; x < tileWidth; x += _tileSize )
			{
				// Compute the direction vectors to the far plane corners for the tile.
				Math::GetPickRay( (int32)x, (int32)y, *_camera, dummy, corners[0] );
				Math::GetPickRay( (int32)x + _tileSize, (int32)y, *_camera, dummy, corners[1] );
				Math::GetPickRay( (int32)x + _tileSize, (int32)y + _tileSize, *_camera, dummy, corners[2] );
				Math::GetPickRay( (int32)x, (int32)y + _tileSize, *_camera, dummy, corners[3] );

				// Subdivide this tile into clusters
				for ( uint32 slice = 1; slice <= _clusterSlices; ++slice )
				{
					// Build the AABB for the cluster
					Cluster& cluster = _clusters[clusterIndex];
					cluster.id = clusterIndex++;
					cluster.active = false;
					cluster.min.Set( std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max() );
					cluster.max.Set( std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min() );
					for ( uint32 i = 0; i < 4; ++i )
					{
						start[i] = corners[i] * depths[slice - 1];
						end[i] = corners[i] * depths[slice];
						cluster.start[i] = corners[i] * depths[slice - 1];
						cluster.end[i] = corners[i] * depths[slice];
						cluster.min = Vector3::Min( start[i], cluster.min );
						cluster.min = Vector3::Min( end[i], cluster.min );
						cluster.max = Vector3::Max( start[i], cluster.max );
						cluster.max = Vector3::Max( end[i], cluster.max );
					}
				}
			}
		}
	}

	// Add light to clusters it intersects with
	void Renderer::ProcessLightClusters( const Matrix& viewMatrix, const Matrix& viewProjMatrix, const Light& light,
		uint32 lightIndex, ClusterLightType type, ClusterMap& activeClusters )
	{
		// Get the depth range
		const Vector3 viewPos = viewMatrix.Transform( light.GetPosition() );
		float viewDist = viewPos.Length();
		bool camInLight = viewDist < light.GetRange();
		float minDepth = viewDist - light.GetRange();
		float maxDepth = viewDist + light.GetRange();
		minDepth = Math::NormalizeDepth( minDepth, *_camera );
		maxDepth = Math::NormalizeDepth( maxDepth, *_camera );
		if ( viewPos.z < 0 && !camInLight )
			return;
		int32 minZ = int32( minDepth * _clusterSlices );
		int32 maxZ = int32( maxDepth * _clusterSlices );
		Math::Clamp( minZ, 0, (int32)_clusterSlices - 1 );
		Math::Clamp( maxZ, 0, (int32)_clusterSlices - 1 );

		// Get screen space xy bounds to find the tiles it belongs to
		int32 minX, minY, maxX, maxY;
		if ( !camInLight )
		{
			// Otherwise, project the light into 2d space and estimate the range
			Point lightCenter = Math::Project( light.GetPosition(), viewProjMatrix, _width, _height );
			Point lightMin, lightMax;
			Math::GetSphereScreenSpaceAABB( viewPos, light.GetRange(), *_camera, _width, _height, lightMin, lightMax );

			// Get the tile range
			minX = lightMin.x / int32( _tileSize );
			minY = lightMin.y / int32( _tileSize );
			maxX = lightMax.x / int32( _tileSize );
			maxY = lightMax.y / int32( _tileSize );
		}
		else
		{
			// Inside the light volume, all tiles are always affected
			minX = minY = 0;
			maxX = _tileWidth - 1;
			maxY = _tileHeight - 1;
		}


		// Add the light to the clusters of this tile
		for ( int32 ty = minY; ty <= maxY; ++ty )
		{
			for ( int32 tx = minX; tx <= maxX; ++tx )
			{
				int32 clusterID = ty*_clusterSlices*_tileWidth + tx*_clusterSlices;
				for ( int32 i = minZ; i <= maxZ; ++i )
				{
					Cluster& cluster = _clusters[i + clusterID];
					//if ( Math::AABBSphereIntersect( cluster.min, cluster.max, viewPos, light.GetRange() ) )
					{
						if ( !cluster.active )
						{
							activeClusters[&cluster] = &cluster;
							cluster.active = true;
						}
						cluster.lights[(int32)type].Add( lightIndex );
					}
				}
			}
		}
	}

	// Process the active clusters, building the final index buffer
	void Renderer::ProcessClusters()
	{
		_clusterIndices.Clear();
		for ( auto iter = _activeClusters.begin(); iter != _activeClusters.end(); ++iter )
		{
			Cluster& cluster = *iter->second;
			LightIndexData& indexData = _lightIndexData[cluster.id];

			// Setup the light index offsets/counts
			indexData.start = _clusterIndices.Size();
			indexData.numShadowSpotLights = cluster.lights[0].Size();
			indexData.numShadowPointLights = cluster.lights[1].Size();
			indexData.numSpotLights = cluster.lights[2].Size();
			indexData.numPointLights = cluster.lights[3].Size();

			// Copy the index data
			for ( int32 i = 0; i < 4; ++i )
			{
				_clusterIndices.AddRange( cluster.lights[i], cluster.lights[i].Size() );
				cluster.lights[i].Clear();
			}

			// Flag as inactive
			cluster.active = false;
		}
	}

	// Render a transparent mesh
	void Renderer::RenderTwoSidedSubMesh( RenderNode& mesh, EffectPass* pass, bool useMaterial )
	{
		// Backfaces
		RenderSubMesh( mesh, pass, useMaterial );

		// Front faces
		SetRasterizerState( _defaultRS->D3DObject );
		RenderSubMesh( mesh );
	}

	// Render a transparent mesh with skinning
	void Renderer::RenderTwoSidedSubMeshSkinned( RenderNode& mesh, EffectPass* pass, bool useMaterial )
	{
		// Backfaces
		RenderSubMeshSkinned( mesh, pass, useMaterial );

		// Front faces
		SetRasterizerState( _defaultRS->D3DObject );
		RenderSubMeshSkinned( mesh );
	}

	// Set the sun color
	void Renderer::SetSunColor( const Color& color )
	{
		_sunColor = color;
		_sunColorVariable->SetFloatVector( color );
	}

	// Enable sky rendering
	void Renderer::EnableSky( bool b )
	{
		_shadowManager->EnableSunShadows( b );
		_hasSky = b;
	}

	// Load a skybox
	bool Renderer::LoadSkyboxCubemap( const char* file )
	{
		SafeDelete( _skybox );
		_skybox = new Skybox( _engine );
		if ( !_skybox->Init( file ) )
		{
			SafeDelete( _skybox );
			return false;
		}
		_skybox->BindEffectVariables();
		return true;
	}


	// Total number of polygons in the current scene
	uint32 Renderer::GetPolyCount() const
	{
		uint32 count = 0;
		for ( uint32 i = 0; i < _models.Size(); ++i )
			count += _models[i]->GetGeometry()->GetNumFaces();
		return count;
	}


	// Get a list of models that are contained by a frustum
	bool Renderer::GetModelsInFrustum( const Frustum& frustum, Array<Model*>& models )
	{
		models.Clear();
		for ( uint32 i = 0; i<_models.Size(); ++i )
		{
			if ( frustum.CheckAABB( _models[i]->GetBounds() ) )
				models.Add( _models[i] );
		}
		return (models.Size() > 0);
	}

	// Build a quadtree for the current scene
	void Renderer::BuildQuadtree()
	{
		SafeDelete( _quadtree );

		// Compute the bounds for terrain
		Vector3 min( 9999, 9999, 9999 ), max( -9999, -9999, -9999 );
		if ( _terrain )
		{
			min = Vector3( -1, 0, -1 ) * (_terrain->GetSize() * _terrain->GetScale() * 0.5f);
			max = Vector3( 1, 0, 1 ) * (_terrain->GetSize() * _terrain->GetScale() * 0.5f);
			for ( int32 i = 0; i < _terrain->GetSize() * _terrain->GetSize(); ++i )
			{
				max.y = std::max( max.y, _terrain->GetHeightfield()[i] * _terrain->GetHeightScale() );
				min.y = std::min( min.y, _terrain->GetHeightfield()[i] * _terrain->GetHeightScale() );
			}
		}

		// Models
		for ( uint32 i = 0; i < _models.Size(); ++i )
		{
			_models[i]->Update( 0.016f );
			min = Vector3::Min( _models[i]->GetBounds().GetWorldPosition() - _models[i]->GetBounds().GetSize(), min );
			max = Vector3::Max( _models[i]->GetBounds().GetWorldPosition() + _models[i]->GetBounds().GetSize(), max );
		}

		// Create the quadtree
		Vector3 worldCenter = (max + min) * 0.5f;
		_quadtree = new Quadtree( worldCenter, max - worldCenter, 6 );
		for ( uint32 i = 0; i < _models.Size(); ++i )
			_quadtree->Insert( _models[i] );
	}

	// Render the quadtree
	void Renderer::RenderQuadtree()
	{
		_quadtree->RenderNodeBounds( &DebugVisualizer::DrawBox, _visualizer );
	}

	// Update the quadtree
	void Renderer::UpdateQuadtree( Model* model )
	{
		if ( _quadtree )
			_quadtree->Insert( model );
	}

	// Creates a terrain from a heightmap file
	Terrain* Renderer::ImportHeightmap( const char* file )
	{
		Terrain* terrain = new Terrain( _engine );
		if ( !terrain->CreateFromFile( file ) )
		{
			delete terrain;
			return false;
		}

		SafeDelete( _terrain );
		_terrain = terrain;
		_terrain->BindEffectVariables();
		return _terrain;
	}

	// Saves the terrain heightmap to a png
	void Renderer::ExportHeightmap( const char* file )
	{
		if ( _terrain )
			_terrain->ExportHeightmap( file );
	}

	// Creates a blank terrain
	Terrain* Renderer::CreateHeightmap( uint32 size )
	{
		Terrain* terrain = new Terrain( _engine );
		if ( !terrain->Create( size ) )
		{
			delete terrain;
			return false;
		}

		SafeDelete( _terrain );
		_terrain = terrain;
		_terrain->BindEffectVariables();
		return _terrain;
	}

	// Material update
	void Renderer::OnMaterialChanged( void* userData )
	{
		auto nodeIter = _materialNodeMap.find( (Material*)userData );
		if ( nodeIter != _materialNodeMap.end() )
		{
			List<RenderNode*>& nodes = nodeIter->second;
			for ( auto iter = nodes.Begin(); !iter.End(); ++iter )
			{
				// Update the callback
				RenderNode* node = *iter;
				SetRenderCallback( *node );
			}
		}
	}

	// Add a submesh to the scene
	void Renderer::AddSubmesh( RenderNode* node )
	{
		// Add to the render list
		_renderList.Add( node );
		node->ComputeInstanceID();
		CreateInstanceSet( node );

		// Add to the material map
		auto mat = node->GetMaterialBinding()->GetMaterial();
		auto nodeIter = _materialNodeMap.find( mat );
		List<RenderNode*>& materialNodes = (nodeIter == _materialNodeMap.end()) ? _materialNodeMap.emplace( mat, List<RenderNode*>() ).first->second : nodeIter->second;
		materialNodes.Add( node );

		// Determine the render callback for this mesh
		SetRenderCallback( *node );
	}

	// Remove a submesh from the scene
	void Renderer::RemoveSubmesh( RenderNode* node )
	{
		// Remove from the render list
		_renderList.Remove( node );
		RemoveInstanceSet( node );

		// Remove from the material map
		auto mat = node->GetMaterialBinding()->GetMaterial();
		auto nodeIter = _materialNodeMap.find( mat );
		if ( nodeIter != _materialNodeMap.end() )
		{
			List<RenderNode*>& materialNodes = nodeIter->second;
			materialNodes.Remove( node );
		}
	}

	// Insert a visible render node to be rendered
	void Renderer::InsertVisibleRenderNode( RenderNode* node )
	{
		for ( auto iter = _visibleMeshes.Begin(); !iter.End(); ++iter )
		{
			RenderNode* thisNode = (RenderNode*)*iter;
			if ( thisNode->sortKey > node->sortKey )
			{
				_visibleMeshes.Insert( iter, node );
				return;
			}
		}
		_visibleMeshes.Add( node );
	}

	// Insert a visible render node to be rendered
	void Renderer::InsertVisibleTransparentRenderNode( RenderNode* node )
	{
		for ( auto iter = _visibletransparentMeshes.Begin(); !iter.End(); ++iter )
		{
			RenderNode* thisNode = (RenderNode*)*iter;
			if ( thisNode->depth > node->depth )
			{
				_visibletransparentMeshes.Insert( iter, node );
				return;
			}
		}
		_visibletransparentMeshes.Add( node );
	}

	// Delete the terrain
	void Renderer::ClearTerrain()
	{
		SafeDelete( _terrain );
	}

	// 	Sets the viewport for the current d3d11 context
	void Renderer::SetViewport( const D3D11_VIEWPORT& viewPort )
	{
		_context->RSSetViewports( 1, &viewPort );
	}

	// Draw a decal at a location
	void Renderer::DrawDecal( uint32 hitID, const Vector3& location, const Vector3& size, const Vector3& rotation, Texture::pointer decal, float duration, const Color& color )
	{
		DecalInfo info = { hitID, location, size, rotation, Color::White, decal, duration };
		Matrix r = Matrix::CreateRotationYawPitchRoll( rotation.y, rotation.x, rotation.z );
		Matrix s = Matrix::CreateScaling( size );
		Matrix t = Matrix::CreateTranslation( location );
		info.matrix = s*r*t;

		//Matrix view = Matrix::CreateLookAtView( _camera->GetPosition(), location, Vector3( 0, 1, 0 ) );
		Vector3 invViewDir = (_camera->GetPosition() - location).Normalize();
		Vector3 projPoint = location + invViewDir.ComponentMult( size );
		Matrix view = Matrix::CreateLookAtView( projPoint, location, Vector3( 0, 1, 0 ) );
		Matrix proj = Matrix(
			2 / size.x, 0, 0, 0,
			0, 2 / size.y, 0, 0,
			0, 0, 1.0f, 0,
			0, 0, 0, 1 );
		info.viewProjMatrix = view * proj;

		_decals.Add( info );
	}

	// Get the pick point in the scene
	Vector3 Renderer::GetPickPoint( Model* modelToIgnore ) const
	{
		float dist = std::numeric_limits<float>::max();
		float t;
		for ( uint32 i = 0; i < _models.Size(); ++i )
		{
			if ( modelToIgnore != _models[i] && _camera->GetFrustum().CheckAABB( _models[i]->GetBounds() ) )
			{
				if ( _models[i]->RayIntersect( _pickRayPos, _pickRayDir, true, t ) && t < dist )
				{
					dist = t;
				}
			}
		}

		Vector3 point = _pickRayPos + _pickRayDir * dist;
		float pointToCam = (point - _pickRayPos).LengthSquared();
		float terrainToCam = (_terrainPickPoint - _pickRayPos).LengthSquared();
		return (pointToCam < terrainToCam) ? point : _terrainPickPoint;
	}

	// Create a new instance set if needed
	void Renderer::CreateInstanceSet( RenderNode* node )
	{
		if ( node->bonePalette )
			return;
		uint32 id = node->GetInstanceID();
		auto iter = _instanceSets.find( id );
		if ( iter == _instanceSets.end() )
		{
			_instanceSets.emplace( id, new InstanceSet( node ) );
		}
		else
		{
			iter->second->AddRef();
		}
	}

	// Remove an instance set if needed
	void Renderer::RemoveInstanceSet( RenderNode* node )
	{
		uint32 id = node->GetInstanceID();
		auto iter = _instanceSets.find( id );
		if ( iter != _instanceSets.end() )
		{
			InstanceSet* set = iter->second;
			if ( !set->RemoveRef() )
			{
				_instanceSets.erase( iter );
				delete set;
			}
		}
	}

	// Fill the deferred shading gbuffers
	void Renderer::FillGBuffers()
	{
		// Clear gbuffers
		_gbuffer->Clear( DS_COLOR );
		_gbuffer->Clear( DS_NORMAL );
		_gbuffer->Clear( DS_FLAT_NORMAL );
		_gbuffer->Clear( DS_MATERIAL );
		_gbuffer->Clear( DS_EMISSIVE );
		_idTexture->Clear();

		// Setup the render targets
		ClearOutputBindings();
		ID3D11RenderTargetView* gbuffers[6] = {
			_gbuffer->GetRTV()[0],
			_gbuffer->GetRTV()[1],
			_gbuffer->GetRTV()[2],
			_gbuffer->GetRTV()[3],
			_gbuffer->GetRTV()[4],
			_idTexture->GetRTV()[0],
		};
		_context->OMSetRenderTargets( 6, gbuffers, _depthStencil->GetDSV() );
		//BindRenderTargetArray( *_gbuffer );

		// Normal meshes
		RenderVisibleMeshes();

		// Terrain
		RenderTerrain( _gbufferTerrainPass );

		// Decal rendering
		RenderDecals();
	}

	// Performs the shading phase on the GBuffers
	void Renderer::DeferredShading()
	{
		// Construct the visible light cluster data, and render all shadow maps
		BuildVisibleLightBufferGPU();

		// Gbuffer
		_gbufferColorVariable->SetResource( *_gbuffer, DS_COLOR );
		_gbufferNormalVariable->SetResource( *_gbuffer, DS_NORMAL );
		_gbufferMaterialVariable->SetResource( *_gbuffer, DS_MATERIAL );
		_gbufferFlatNormalVariable->SetResource( *_gbuffer, DS_FLAT_NORMAL );
		_gbufferEmissiveVariable->SetResource( *_gbuffer, DS_EMISSIVE );

		// Downscale the gbuffer
		SetRenderToQuad();
		SetViewport( _aoTexture->GetViewport() );
		BindRenderTargetArray( *_gbufferHalfRes );
		_gbufferDownscalePass->Apply();
		_context->Draw( 6, 0 );

		// Ambient occlusion
		_aoTexture->Clear();
		_depthBufferVariable->SetResource( *_gbufferHalfRes, 0 );
		_gbufferFlatNormalVariable->SetResource( *_gbufferHalfRes, 1 );
		BindRenderTargetWithoutDepth( *_aoTexture );
		_ambientOcclusionPass->Apply();
		_context->Draw( 6, 0 );

		// Blur the emissive buffer
		{
			_emissiveBlurTexture->Clear();
			_filterKernelVariable->SetInt( 4 );
			_postFxVariable->SetResource( *_gbufferHalfRes, 2 );
			BindRenderTargetWithoutDepth( *_emissiveBlurTexture );
			_boxBlurHorizontalPass->Apply();
			_context->Draw( 6, 0 );

			_postFxVariable->SetResource( *_emissiveBlurTexture );
			BindRenderTargetWithoutDepth( *_gbufferHalfRes, 2 );
			_boxBlurVerticalPass->Apply();
			_context->Draw( 6, 0 );
		}

		// Shading
		_depthBufferVariable->SetResource( *_depthStencil );
		_gbufferFlatNormalVariable->SetResource( *_gbuffer, DS_FLAT_NORMAL );
		_gbufferEmissiveVariable->SetResource( *_gbufferHalfRes, 2 );
		SetViewport( _hdr->Buffer()->GetViewport() );
		_postFxVariable->SetResource( *_aoTexture );
		BindRenderTargetWithoutDepth( *_hdr->Buffer() );
		if ( !HasSky() )
			_shadingPassNoSun->Apply();
		else
		{
			if ( _sunShadows )
				_shadingPass->Apply();
			else
				_shadingPassNoSunShadows->Apply();
		}
		_context->Draw( 6, 0 );
	}

	// Render the decals
	void Renderer::RenderDecals()
	{
		// Render into the diffuse gbuffer
		Color finalColor;
		BindRenderTargetWithoutDepth( *_gbuffer, DS_COLOR );
		_gbufferIDVariable->SetResource( *_idTexture );
		for ( auto iter = _decals.Begin(); !iter.End(); )
		{
			// Update timers
			DecalInfo& info = *iter;
			info.timer += _engine->GetFrameTime();
			if ( info.timer > info.duration )
				iter = _decals.Remove( iter );

			else
			{
				float timeLeft = info.duration - info.timer;
				finalColor = Color( info.color.r, info.color.g, info.color.b );
				finalColor.a = (timeLeft < 1.0f) ? timeLeft : 1.0f;

				_decalTextureVariable->SetResource( *info.decal );
				_decalMatrixVariable->SetMatrix( info.viewProjMatrix );
				_decalColorVariable->SetFloatVector( finalColor );
				_objectIDVariable->SetInt( info.hitID );
				SetWorldMatrix( info.matrix );
				_decalPass->Apply();
				RenderSubMesh( *_box->GetSubMesh( 0 ) );

				//GetDebugVisualizer()->DrawBox( info.matrix, info.color );
				++iter;
			}
		}
	}

	// Set the render callback for a submesh
	void Renderer::SetRenderCallback( RenderNode& mesh )
	{
		// Transparents
		Renderer* caller = (Renderer*)this;
		if ( mesh.GetMaterialBinding()->GetMaterial()->IsTransparent() )
		{
			// Standard
			if ( !mesh.GetMaterialBinding()->GetMaterial()->TwoSidedTransparency() )
			{
				if ( mesh.bonePalette )
					mesh.SetRenderCallback( &Renderer::RenderSubMeshSkinned, caller, _transparentSkinnedPass );
				else if(mesh.GetMaterialBinding()->GetMaterial()->IsAlphaTested())
					mesh.SetRenderCallback( &Renderer::RenderSubMesh, caller, _transparentAlphaPass );
				else
					mesh.SetRenderCallback( &Renderer::RenderSubMesh, caller, _transparentPass );
			}

			// Dual sided
			else
			{
				if ( mesh.bonePalette )
					mesh.SetRenderCallback( &Renderer::RenderTwoSidedSubMeshSkinned, caller, _transparentSkinnedPass );
				else if ( mesh.GetMaterialBinding()->GetMaterial()->IsAlphaTested() )
					mesh.SetRenderCallback( &Renderer::RenderTwoSidedSubMesh, caller, _transparentAlphaPass );
				else
					mesh.SetRenderCallback( &Renderer::RenderTwoSidedSubMesh, caller, _transparentPass );
			}
			return;
		}

		// Opaques
		if ( mesh.bonePalette )
		{
			mesh.SetRenderCallback( &Renderer::RenderSubMeshSkinned, caller, _gbufferSkinnedPass );
		}
		else if ( mesh.GetMaterialBinding()->IsAlphaTested() )
		{
			mesh.SetRenderCallback( &Renderer::RenderSubMesh, caller, _gbufferAlphaPass );
		}
		else if ( mesh.cubeMap )
		{
			if ( mesh.GetMaterialBinding()->GetMaterial()->IsRefractive() )
				mesh.SetRenderCallback( &Renderer::RenderSubMesh, caller, _gbufferRefractPass );
			else
				mesh.SetRenderCallback( &Renderer::RenderSubMesh, caller, _gbufferCubemapPass );
		}
		else
		{
			mesh.SetRenderCallback( &Renderer::RenderSubMesh, caller, _gbufferPass );
		}
	}

}

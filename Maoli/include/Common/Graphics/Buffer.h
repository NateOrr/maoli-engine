//--------------------------------------------------------------------------------------
// File: Buffer.h
//
// ID3D11Buffer wrapper class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Renderer;

	// Simplifies the usage of d3d11 buffers
	template <class T>
	class Buffer : public Resource<Renderer>, public PipeplineResource
	{
		friend class Pointer<Buffer<T>>;
		friend class Renderer;

	public:

		// Smart pointer
		typedef Pointer<Buffer<T>> pointer;

		// Clear to the given value.
		void ClearUAV(const uint32 values[4]);

		// Clear to the given value.
		void ClearUAV(const float values[4]);


		// Returns a pointer to the ID3D11Buffer interface
		inline ID3D11Buffer* GetBuffer(){ return _buffer; }

		// Get the unordered access view
		virtual ID3D11UnorderedAccessView** GetUAV(){ return _uav; }

		// Get the shader resource view
		virtual ID3D11ShaderResourceView** GetSRV(){ return _srv; }

		// Get the render target view (required from the interface
		virtual ID3D11RenderTargetView**	GetRTV(){ return nullptr; }
		
		// Gets length of the buffer (number of the template objects stored)
		inline int32 GetLength(){ return _length; }

		// Gets the byte stride (the size of the template parameter)
		inline int32 GetByteStride(){ return _byteStride; }

		// Gets the size of the buffer in bytes.
		inline int32 GetSize(){ return _length*_byteStride; }

		// Set the debugging name
		void SetDebugName(const char* file);


		// Maps the buffer memory (staging and dynamic buffers only).
		// Unmap() must be called before the buffer can be used again.
		T* Map(D3D11_MAP mapping);

		// Completes the Map operation on the buffer
		void Unmap();

		// Update a portion of the buffer
		void Update( const T* data, uint32 start, uint32 count );


	private:

		// Ctor
		Buffer(Renderer* parent);

		// Dtor
		~Buffer();

		// Create a new buffer
		HRESULT Create(int32 length, D3D11_USAGE usage, bool UAV, bool SRV, const T* data = nullptr);

		// Create a vertex buffer
		HRESULT CreateVertexBuffer( int32 length, D3D11_USAGE usage, const T* data = nullptr );

		// Create an index buffer
		HRESULT CreateIndexBuffer( int32 length, D3D11_USAGE usage, const T* data = nullptr );

		// Release resources associated with this buffer.  The destructor will call this.
		void Release();

		ID3D11Buffer*				_buffer;			// Buffer object
		ID3D11UnorderedAccessView*	_uav[1];			// CS var
		ID3D11ShaderResourceView*   _srv[1];			// PS var
		int32							_length;			// Number of elements
		int32							_byteStride;		// Size of T
		D3D11_MAPPED_SUBRESOURCE    _mappedResource;	// For mapping
	};

}

#include "Buffer.inl"

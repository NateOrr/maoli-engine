//--------------------------------------------------------------------------------------
// File: HDR.h
//
// HDR System.  Handles buffer creation and HDR post-processing
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once
#include "Graphics/Texture.h"
#include "Graphics/DepthStencil.h"

namespace Maoli
{

	// Forward decl
	class Renderer;

	class HDRSystem
	{
	public:
		HDRSystem(Renderer* graphics)
		{
			_graphics = graphics;
			m_LuminanceDepth = nullptr;
			m_CurrentLum = 1;
			m_OldLum = 0;
		}

		// Create the final scene image with HDR post-processing
		void ProcessHDRI(Texture::pointer pBackBuffer);			
		
		// Setup the hdr system
		HRESULT Create(uint32 width, uint32 height, DepthStencil::pointer pDSV, Effect* pEffect);

		// Bind the effect variables
		void BindEffectVariables();
		
		// Free all hdr related resources
		void Release();		

		Texture::pointer Buffer(){ return m_HDRBuffer; }
		
	private:

		// Compute the gaussian offsets and weights, and send them to the shader
		void ComputeGaussians(uint32 width, uint32 height); 
		
		Renderer*						_graphics;
		Texture::pointer				m_HDRBuffer;			// RT to store the 128-bit hdr scene rendering
		Array<Texture::pointer>			m_HDRScaledLuminance;	// Stores the luminance data, each scaled RT is 1/2 the original size
		Array<DepthStencil::pointer>	m_ScaledLuminanceDepth;
		Texture::pointer				m_HDRLuminance[2];		// Luminance texture
		DepthStencil::pointer			m_LuminanceDepth;
		int32								m_CurrentLum;			// Current luminance texture
		int32								m_OldLum;				// Lum from last frame
		uint32							m_Width;				// Buffer size
		uint32							m_Height;				// Buffer size


		// Effect variables
		ShaderResource*			HDRVariable;
		ShaderResource*			LuminanceVariable;
		ShaderResource*			AdaptedLuminanceVariable;
		ShaderScalar*			GaussianWeightsH;
		ShaderScalar*			GaussianWeightsV;
		ShaderScalar*			GaussianOffsetsH;
		ShaderScalar*			GaussianOffsetsV;

		// Effect passes
		EffectPass* _downscalePass;
		EffectPass* _downscaleLuminancePass	;
		EffectPass* _adaptLuminancePass;
		EffectPass* _brightPass;
		EffectPass* _tonemapPass;

	};

}

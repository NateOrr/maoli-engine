//--------------------------------------------------------------------------------------
// File: ParticleEmitterState.cpp
//
// A state within a particle emitter, can contain multiple particle types
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "ParticleEmitterState.h"

namespace Maoli
{
	// Ctor
	ParticleEmitterState::ParticleEmitterState()
	{
		_name = "NewState";
		_duration = 0;
		_nextState = nullptr;
	}

	// Add a particle set
	void ParticleEmitterState::AddParticleSet( ParticleSet* set )
	{
		if ( !_sets.Contains( set ) )
			_sets.Add( set );
	}

	// Remove a particle set
	void ParticleEmitterState::RemoveParticleSet( ParticleSet* set )
	{
		_sets.Remove( set );
	}

	// Remove a particle set
	void ParticleEmitterState::RemoveParticleSet( int32 index )
	{
		_sets.RemoveAt( index );
	}

}
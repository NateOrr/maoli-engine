//--------------------------------------------------------------------------------------
// File: DynamicMesh.h
//
// Immediate mesh rendering, this is meant for debugging and visualization only
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Buffer.h"
#include "Graphics/MaterialBinding.h"

namespace Maoli
{
	// Forward decl
	class Renderer;

	class DynamicMesh
	{
		friend class Renderer;
	public:

		// Ctor
		DynamicMesh( Renderer* graphics );

		// Set the max number of verts
		void SetMaxVerts( uint32 value );

		// Clear the vertex data
		void Clear();

		// Fill the vertex buffer
		bool FillVerts( const Vector3* verts, uint32 count );

		// Fill the index buffer
		bool FillIndices( const uint32* indices, uint32 count );

		// Set the material
		inline void SetMaterial( MaterialBinding::pointer mat ) { _material = mat; }

		// Set the render mode
		inline void SetTopology( D3D11_PRIMITIVE_TOPOLOGY topology ) { _topology = topology; }

		// Set the transform
		inline void SetTransform( const Matrix& worldMatrix ) { _transform = worldMatrix; }

		// Enable / disable wireframe
		inline void EnableWireframe( bool value ) { _wireframe = value; }

	private:

		Renderer*					_graphics;			// Renderer
		Buffer<Vector3>::pointer	_vertexBuffer;		// Dynamic vertex buffer
		Buffer<uint32>::pointer		_indexBuffer;		// Dynamic index buffer
		uint32						_maxVerts;			// Vertex buffer capacity
		uint32						_maxIndices;		// Index buffer capacity
		uint32						_numVerts;			// Current number of verts
		uint32						_numIndices;		// Current number of indices
		MaterialBinding::pointer	_material;			// Material to draw with
		D3D11_PRIMITIVE_TOPOLOGY	_topology;			// Primitive type
		Matrix						_transform;			// World matrix
		bool						_wireframe;			// Wireframe rendernig
	};

}

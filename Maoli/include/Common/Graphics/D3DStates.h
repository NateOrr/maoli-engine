//--------------------------------------------------------------------------------------
// File: D3DStates.h
//
// State management
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Effect;
	class ShaderScalar;

	// Frame stats
	struct FrameStats
	{
		// Ctor
		FrameStats();

		// Reset the stats
		void Reset();

		int32 materialChanges;
		int32 vertexBufferChanges;
		int32 indexBufferChanges;
		int32 drawCalls;
		int32 polygonsDrawn;
		int32 polygonsProcessed;
		int32 processedMessages;
		int32 resourceBinds;
	};


	// Rendering pipeline states
	class RenderState
	{
	public:

		// Ctor
		RenderState();

		// Reset the state
		void Reset();

		// Reset all bindings
		void ResetBindings();

		// Access the frame stats
		inline const FrameStats& GetFrameStats() const { return _frameStats; }

		// Set the device context
		inline void SetDeviceContext( ID3D11DeviceContext* dc ) { _context = dc; }

		// Setup effect variables
		void BindEffectVariables( Effect* effect );

		// Bind only a depth stencil to the graphics output
		void BindDepthStencilWithoutRenderTarget( DepthStencil::pointer depthStencil );

		// Bind a render target to the pipeline without dsv
		void BindRenderTargetWithoutDepth( PipeplineResource* resource, uint32 index = 0 );

		// 	Bind the render target to the d3d11 pipeline
		void BindRenderTarget( PipeplineResource* resource, uint32 index = 0 );

		// 	Bind the render target to the d3d11 pipeline using a read-only depth buffer
		void BindRenderTargetWithReadOnlyDepth( PipeplineResource* resource, uint32 index = 0 );

		// 	Bind render target array to the d3d11 pipeline
		void BindRenderTargetArray( PipeplineResource* resource, uint32 numResources = 0 );

		// Bind a single UAV to the graphics pipeline
		void BindUnorderedAccessView( uint32 slot, PipeplineResource* resource );

		// Set render targets and uavs for the graphics pipeline
		void BindRenderTargetsAndUnorderedAccessViews( PipeplineResource* rtvResource, uint32 numResources, PipeplineResource* uavResource, uint32 uavSlot, uint32 uavCount );

		// Clear the render target/dsv
		void ClearRenderTarget();


		// Set the input layout
		void SetInputLayout( ID3D11InputLayout* pInputLayout );

		// Set the material
		void SetMaterial( MaterialBinding::pointer pMat );

		// Bind a single material texture
		void BindMaterialTexture( MaterialBinding::pointer mat, TEX_TYPE type );
		void BindMaterialTexture( Texture::pointer tex, TEX_TYPE type );

		// Set only the textures for a mterial
		void BindMaterialTextures( MaterialBinding::pointer mat );

		// Set the vertex buffer
		void SetVertexBuffer( ID3D11Buffer* pVB, uint32 stride );

		// Set a null vertex buffer
		void ClearVertexBuffer();

		// Set the index buffer
		void SetIndexBuffer( ID3D11Buffer* pIB, DXGI_FORMAT size );

		// Set the cubemap
		void SetCubeMap( Texture::pointer pCubeMap );

		// Set the topology
		void SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY type );

		// Set the compute shader
		void SetComputeShader( ID3D11DeviceChild* shader );

		// Set the vertex
		void SetVertexShader( ID3D11DeviceChild* shader );

		// Set the hull shader
		void SetHullShader( ID3D11DeviceChild* shader );

		// Set the domain shader
		void SetDomainShader( ID3D11DeviceChild* shader );

		// Set the geometry
		void SetGeometryShader( ID3D11DeviceChild* shader );

		// Set the pixel shader
		void SetPixelShader( ID3D11DeviceChild* shader );

		// Set the blend state
		void SetBlendState( ID3D11BlendState* state, FLOAT factor[4], uint32 mask );

		// Set the rasterizer state
		void SetRasterizerState( ID3D11RasterizerState* state );

		// Set the depth stencil state
		void SetDepthStencilState( ID3D11DepthStencilState* state, uint32 stencilRef );

		// Unbind a shader
		void ClearShader( ShaderType shader );

		// Apply a shader to the pipeline
		void SetShader( Shader* shader );

		// Set a shader resource
		void SetShaderResource( uint32 shaderType, uint32 slot, PipeplineResource* resource, ID3D11ShaderResourceView* srv );

		// Set the quad vertex buffer
		void SetRenderToQuad();

		// Add on a vertex stream
		void AddVertexStream( ID3D11Buffer* pStream, uint32 stride );

		// Bind the available streams and reset them
		void BindVertexStreams();

		// Clear all output bindings
		void ClearOutputBindings(bool nullTarget);

		// Clear input bindings from a texture
		void ClearInputBindings( PipeplineResource* resource );


	private:

		// Context the state is bound to
		ID3D11DeviceContext* _context;

		// Misc resources
		MaterialBinding::pointer	_materialBinding;
		ID3D11InputLayout*			_inputLayout;
		D3D11_PRIMITIVE_TOPOLOGY	_topology;
		Texture::pointer			_cubemap;
		Texture::pointer			_shadowMap;
		Texture::pointer			_cubeShadowMap;
		ID3D11Buffer*				_vertexBuffer;
		ID3D11Buffer*				_indexBuffer;
		FrameStats					_frameStats;
		float						_clearColor[4];

		// Shaders
		ID3D11DeviceChild*			_shaders[(int32)ShaderType::NumTypes];

		// Render states
		ID3D11BlendState*			_blendState;
		ID3D11RasterizerState*		_rasterizerState;
		ID3D11DepthStencilState*	_depthStencilState;

		// Resource bindings
		static const int32			_maxResourceBindings = D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT;
		static const int32			_maxConstantBufferBindings = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT;
		static const int32			_maxSamplerBindings = D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT;
		ConstantBuffer*				_constantBuffers[(int32)ShaderType::NumTypes][_maxConstantBufferBindings];
		ID3D11SamplerState*			_samplerStates[(int32)ShaderType::NumTypes][_maxSamplerBindings];
		ID3D11ShaderResourceView*	_resourceViews[(int32)ShaderType::NumTypes][_maxResourceBindings];
		PipeplineResource*			_resourceBindings[(int32)ShaderType::NumTypes][_maxResourceBindings];
		Array<PipeplineResource*>	_outputTargets;
		Array<PipeplineResource*>	_computeUAVs;

		// Vertex streams
		ID3D11Buffer*				_vertexStreams[VERTEX_STREAM_MAX];
		uint32						_vertexStrides[VERTEX_STREAM_MAX];
		uint32						_currentStream;

		// Material Properties
		ConstantBuffer*				_materialCBVariable;			// Material constant buffer
		ShaderResource*				_materialBufferVariable;		// Global material buffer
		ShaderResource*				_materialTextureVariable;		// Material texture array
		ShaderScalar*				_materialTextureFlagVariable;	// Array of booleans for material texture array flags
		ShaderResource*				_probeCubeVariable;				// Cube map


		// Light Properties
		ConstantBuffer*				_lightCBVariable;			// Light constant buffer
	};

}

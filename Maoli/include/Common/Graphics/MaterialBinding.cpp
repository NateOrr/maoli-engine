//--------------------------------------------------------------------------------------
// File: Material.cpp
//
// Renderable surface properties
//
// Nate Orr
//--------------------------------------------------------------------------------------

#include "Maoli.h"
#include "Graphics/MaterialBinding.h"
#include "Graphics/Renderer.h"

namespace Maoli
{

	// Constructor
	MaterialBinding::MaterialBinding( Renderer* graphics ) : Resource( graphics )
	{
		Release();
	}

	// Dtor
	MaterialBinding::~MaterialBinding()
	{
		Release();
	}


	// Clear the texture
	void MaterialBinding::ClearTexture( TEX_TYPE type )
	{
		if ( _textures[type] )
		{
			_textures[type] = nullptr;
			_srv[type] = nullptr;
			_material->_properties.textureFlag.Unset( type );
		}
	}

	// Texture loading
	Texture::pointer MaterialBinding::LoadTexture( const char* file, TEX_TYPE type )
	{
		// Clear any old texture
		ClearTexture( type );

		// Load the texture
		_textures[type] = _parent->CreateTextureFromFile( file );

		if ( _textures[type] )
		{
			_material->_properties.textureFlag.Set( type );
			_material->SetTextureName( file, type );
			_srv[type] = _textures[type]->GetSRV()[0];
			return _textures[type];
		}
		return Texture::pointer( nullptr );
	}


	// Releases the material
	void MaterialBinding::Release()
	{
		for ( int32 i = 0; i < TEX_SIZE; i++ )
		{
			if ( _textures[i] )
			{
				_textures[i] = nullptr;
				_srv[i] = nullptr;
			}
		}
	}


	// Create from a material
	HRESULT MaterialBinding::Init( Material* material )
	{
		if ( material == _material )
			return S_OK;
		_material = material;
		return ImportTextures();
	}

	// Load all textures
	HRESULT MaterialBinding::ImportTextures()
	{
		String dir = _material->GetFilePath().GetDirectory();
		for ( int32 i = 0; i < TEX_TYPE::TEX_SIZE; ++i )
		{
			String tex = _material->_textureFiles[i];
			if ( _material->_textureFiles[i] == String::none )
				continue;

			LoadTexture( _material->_textureFiles[i], (TEX_TYPE)i );

			// Try the local path if it failed
			if ( !_textures[i] )
				LoadTexture( dir + _material->_textureFiles[i], (TEX_TYPE)i );
		}

		return S_OK;
	}

	// Get the file path to the material preview
	Maoli::String MaterialBinding::GetPreviewFile()
	{
		return GetMaterial()->GetFilePath().RemoveFileExtension() + "_preview.jpg";
	}


}

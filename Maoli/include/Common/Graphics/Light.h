//--------------------------------------------------------------------------------------
// File: Light.h
//
// 3D Light
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Texture.h"
#include "Game/Component.h"

namespace Maoli
{

	// Forward decl
	class Engine;
	class Renderer;

	// Types of lights
	enum class LightType
	{
		Point,
		Spot,
	};

	// A light object
	//
	// Lights can be either directional, spot, or point.  Directional lights have no
	// attenuation and don't rely on a position, while point and spot lights have a
	// range based falloff.  
	class Light : public Component
	{
		friend class Model;
		friend class Effect;
		friend class Renderer;
		friend class ForwardRenderer;
		friend class RenderState;

	public:

		COMPONENT_DECL( Light )

			// Ctor
			Light( Engine* engine );

		// Dtor
		~Light();


		/////////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Register for any events that need to be handled
		virtual void RegisterEventCallbacks();


		/////////////////////////////////////////////////
		// Event callbacks

		// Update position
		void OnTransformChanged( Component* sender, void* userData );


		// Free all memory associated with this object
		void Release();


		// Set the local position
		void SetLocalPosition( const Vector3& pos );

		// Set the local position
		void SetLocalPosition( float x, float y, float z );

		// Add to local the position
		void AddToLocalPosition( const Vector3& pos );

		// Get the local position
		inline const Vector3& GetLocalPosition() const { return _position; }

		// Get the position in world space
		inline const Vector3& GetPosition( ) const { return _properties.lightData.position; }


		// Set the local direction
		void SetLocalDirection( const Vector3& pos );

		// Set the local direction
		void SetLocalDirection( float x, float y, float z );

		// Add to the local direction
		void AddToLocalDirection( const Vector3& pos );

		// Get the local direction
		inline const Vector3& GetLocalDirection() const { return _properties.direction; }

		// Get the local direction
		inline const Vector3& GetDirection() const { return _properties.direction; }



		// Set the range
		void SetRange( float range );

		// Add to the range
		void AddToRange( float pos );

		// Get the range
		inline float GetRange( ) const { return _properties.lightData.range; }



		// Set the color
		inline void SetColor( const Color& color ) { _properties.lightData.color = color; }
		inline void SetColor( float r, float g, float b ) { _properties.lightData.color = Color( r, g, b ); }

		// Get the color
		inline const Color& GetColor( ) const { return _properties.lightData.color; }


		// Set the inner spotlight radius
		inline void SetInnerRadius( float r ) { _properties.innerRadius = r; }

		// Set the outer spotlight radius
		inline void SetOuterRadius( float r ) { _properties.outerRadius = r; }

		// Get the inner spotlight radius
		inline float GetInnerRadius() const { return _properties.innerRadius; }

		// Get the outer spotlight radius
		inline float GetOuterRadius() const { return _properties.outerRadius; }


		// Set the light type
		void SetType( LightType type );

		// Get the light type
		inline LightType GetType() const { return _type; }

		// Enable or disable shadow casting
		void EnableShadows( bool b );

		// Check if this light casts shadows
		inline bool CastsShadows() const { return _hasShadows; }


		//  Get the projective texture
		inline const Texture::pointer GetProjectiveTexture() const { return _projectiveTexture; }

		// Get the constant buffer data
		inline const void* GetShaderData() const { return (_type == LightType::Point) ? (void*)&_properties.lightData : (void*)&_properties; }

		// Clear t projective texture
		void ClearProjectiveTexture();

		// Load a projective texture
		Texture::pointer LoadProjectiveTexture( const char* file );

		// Set the projective texture
		void SetProjectiveTexture( Texture::pointer tex );


		int32				ShadowSoftness;		// Controls shadow softness
		Matrix			worldMatrix;		// World matrix
		Frustum			frustum;			// View frustum, for spotlights only

	private:

		// Light constant buffer
		struct PointLightShaderData
		{
			Color		color;
			Vector3		position;
			float		range;
		};

		struct SpotLightShaderData
		{
			PointLightShaderData lightData;
			Vector3		direction;
			float		innerRadius;
			float		outerRadius;
		};

		// Update the light after the type has been changed
		void UpdateType(LightType newType);

		SpotLightShaderData _properties;

		Engine*				_engine;				// Parent game engine
		Renderer*			_graphics;				// Graphics engine that owns the light
		Texture::pointer	_projectiveTexture;		// Projective texture
		Vector3				_position;				// Local position
		Vector3				_direction;				// Local direction
		bool				_isDirty;				// True if local transorm has changed
		Matrix*				_entityMatrix;			// Pointer to the entity transform matrix, if one exists
		LightType			_type;					// Point or spot
		bool				_active;				// True if in the scene
		bool				_hasShadows;			// True if the light uses shadow maps
	};

}

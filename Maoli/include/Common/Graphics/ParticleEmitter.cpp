//--------------------------------------------------------------------------------------
// File: ParticleEmitter.cpp
//
// Particle systems
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "ParticleEmitter.h"
#include "ParticleSet.h"
#include "ParticleEmitterState.h"
#include "Graphics/Renderer.h"
#include "Physics\RigidBody.h"
#include "Game\Entity.h"

namespace Maoli
{

	// Ctor
	ParticleEmitter::ParticleEmitter( Engine* engine ) : Component( engine )
	{
		_graphics = engine->GetGraphics();
		_inputLayout = nullptr;
		_effectPass = nullptr;
		_textureVariable = nullptr;
		_transform = nullptr;
		_activeState = nullptr;
		_stateTimer = 0;

		// Setup the quad vertex buffer
		Vertex quadVerts[6] =
		{
			{ Vector3( 1.0f, 1.0f, 0.5f ), 1.0f, 0.0f, Vector3( 0, 0, -1 ) },
			{ Vector3( 1.0f, -1.0f, 0.5f ), 1.0f, 1.0f, Vector3( 0, 0, -1 ) },
			{ Vector3( -1.0f, -1.0f, 0.5f ), 0.0f, 1.0f, Vector3( 0, 0, -1 ) },
			{ Vector3( -1.0f, -1.0f, 0.5f ), 0.0f, 1.0f, Vector3( 0, 0, -1 ) },
			{ Vector3( -1.0f, 1.0f, 0.5f ), 0.0f, 0.0f, Vector3( 0, 0, -1 ) },
			{ Vector3( 1.0f, 1.0f, 0.5f ), 1.0f, 0.0f, Vector3( 0, 0, -1 ) },
		};
		_vertexBuffer = _graphics->CreateVertexBuffer<Vertex>( 6, D3D11_USAGE_IMMUTABLE, quadVerts );
		_buffers[0] = _vertexBuffer->GetBuffer();
		_strides[0] = _vertexBuffer->GetByteStride();
		_offsets[0] = _offsets[1] = 0;
	}

	// Dtor
	ParticleEmitter::~ParticleEmitter()
	{
		SafeRelease( _inputLayout );

		for ( uint32 i = 0; i < _states.Size(); ++i )
		{
			for ( uint32 j = 0; j < _states[i]->GetNumParticleSets(); ++j )
				delete _states[i]->GetParticleSet( j );
			delete _states[i];
		}
	}

	// Binary serialize
	void ParticleEmitter::Export( std::ofstream& fout )
	{
		uint32 numStates = _states.Size();
		Maoli::Serialize( fout, &numStates );
		Maoli::Serialize( fout, &_position );
		for ( uint32 i = 0; i < numStates; ++i )
		{
			// State
			_states[i]->GetName().Serialize( fout );
			Maoli::Serialize( fout, &_states[i]->_duration );
			bool transition = (_states[i]->_nextState != nullptr);
			Maoli::Serialize( fout, &transition );
			if ( transition )
				_states[i]->_nextState->GetName().Serialize( fout );

			// Sets for each state
			uint32 numSets = _states[i]->GetNumParticleSets();
			Maoli::Serialize( fout, &numSets );
			for ( uint32 j = 0; j < numSets; ++j )
				_states[i]->GetParticleSet( j )->Export( fout );
		}

		// Save the active state index
		if ( _activeState )
		{
			for ( uint32 i = 0; i < numStates; ++i )
			{
				if ( _activeState->GetName() == _states[i]->GetName() )
				{
					Maoli::Serialize( fout, &i );
					break;
				}
			}
		}
		else
		{
			uint32 i = 123456789;
			Maoli::Serialize( fout, &i );
		}
	}

	// Binary file import
	void ParticleEmitter::Import( std::ifstream& fin )
	{
		bool transition;
		String str;
		uint32 numStates;
		Maoli::Deserialize( fin, &numStates );
		Maoli::Deserialize( fin, &_position );
		_states.Allocate( numStates );
		Array<String> transitionNames( numStates );
		for ( uint32 i = 0; i < numStates; ++i )
		{
			// State
			str.Deserialize( fin );
			_states[i] = CreateState( str );
			Maoli::Deserialize( fin, &_states[i]->_duration );
			Maoli::Deserialize( fin, &transition );
			if ( transition )
			{
				transitionNames[i].Deserialize( fin );
			}
			else
				transitionNames[i] = String::none;

			// Sets for each state
			uint32 numSets;
			Maoli::Deserialize( fin, &numSets );
			for ( uint32 j = 0; j < numSets; ++j )
			{
				auto set = CreateParticleSet();
				set->Import( fin );
				_states[i]->AddParticleSet( set );
			}
		}

		// Link the state transitions
		for ( uint32 i = 0; i < _states.Size(); ++i )
		{
			if ( transitionNames[i] != String::none )
			{
				// Find the state to link
				for ( uint32 j = 0; j < _states.Size(); ++j )
				{
					if ( transitionNames[i] == _states[j]->GetName() )
					{
						// Link it
						_states[i]->SetNextState( _states[j] );
						break;
					}
				}
			}
		}

		// Set the default state
		uint32 activeIndex;
		Maoli::Deserialize( fin, &activeIndex );
		if ( activeIndex < _states.Size() )
			SetState( _states[activeIndex] );
	}

	// Deep copy clone is required for all components
	Component* ParticleEmitter::Clone( Component* dest /*= nullptr */ ) const
	{
		ParticleEmitter* clone = dest ? (ParticleEmitter*)dest : new ParticleEmitter( _engine );

		// Clone each state
		for ( uint32 i = 0; i < _states.Size(); ++i )
		{
			ParticleEmitterState* state = clone->CreateState( _states[i]->GetName() );
			clone->AddState( state );
			state->SetDuration( _states[i]->GetDuration() );

			// Clone the sets
			for ( uint32 set = 0; set < _states[i]->GetNumParticleSets(); ++set )
				state->AddParticleSet( _states[i]->GetParticleSet( set )->Clone( clone ) );
		}

		// Link the state transitions
		for ( uint32 i = 0; i < _states.Size(); ++i )
		{
			if ( _states[i]->GetNextState() )
			{
				// Find the state to link
				for ( uint32 j = 0; j < _states.Size(); ++j )
				{
					if ( _states[i]->GetNextState() == _states[j] )
					{
						// Link it
						clone->GetState( i )->SetNextState( clone->GetState( j ) );
						break;
					}
				}
			}
		}

		// Set the active state
		if ( _activeState )
			clone->SetState( _activeState->GetName() );

		return clone;
	}

	// Register component dependencies
	void ParticleEmitter::RegisterDependency( Component* dependency )
	{
		// Local
		if ( dependency->GetOwner() == GetOwner() )
		{
			if ( dependency->GetTypeID() == typeid(Transform) )
			{
				_transform = (Transform*)dependency;
				return;
			}
		}
	}

	// Per-frame logic
	void ParticleEmitter::Update( float dt )
	{
		if ( _activeState )
		{
			// Manage state transitions
			if ( _activeState->GetNextState() )
			{
				_stateTimer += dt;
				if ( _stateTimer > _activeState->GetDuration() )
					SetState( _activeState->GetNextState() );
			}

			// Update the active state
			for ( uint32 i = 0; i < _activeState->GetNumParticleSets(); ++i )
			{
				_activeState->GetParticleSet( i )->Update( dt );
			}
		}

		// Update each transitioning set
		for ( auto iter = _transitions.Begin(); !iter.End(); )
		{
			// Remove the set if it has no more active particles
			auto set = *iter;
			if ( set->GetNumActiveParticles() == 0 )
			{
				iter = _transitions.Remove( iter );
			}
			else
			{
				set->Update( dt );
				++iter;
			}
		}
	}


	// Render all particle systems
	void ParticleEmitter::Render()
	{
		// Setup for particle rendering
		if ( HasActiveParticles() )
		{
			_graphics->SetVertexBuffer( nullptr, 0 );
			_graphics->SetWorldMatrix( _graphics->GetBillboardMatrix() );
			_graphics->SetInputLayout( _inputLayout );
		}

		// Draw the active state
		if ( _activeState )
		{
			for ( uint32 i = 0; i < _activeState->GetNumParticleSets(); ++i )
				RenderSet( _activeState->GetParticleSet( i ) );
		}

		// Draw each transitioning set
		for ( auto iter = _transitions.Begin(); !iter.End(); ++iter )
			RenderSet( *iter );
	}

	// Bind effect variables for rendering
	void ParticleEmitter::BindEffect( Effect* effect )
	{
		_textureVariable = effect->GetVariableByName( "g_txParticle" )->AsShaderResource();
		_effectPass = effect->GetPassByName( "Particle" );
		_animatedPass = effect->GetPassByName( "AnimatedParticle" );
		_animatedSoftPass = effect->GetPassByName( "AnimatedSoftParticle" );

		_spriteDimensionsVariable = effect->GetVariableByName( "g_SpriteDimensions" )->AsVector();
		_animationFrameSizeVariable = effect->GetVariableByName( "g_AnimationFrameSize" )->AsVector();
		_textureDimensionsVariable = effect->GetVariableByName( "g_TextureDimensions" )->AsVector();
		_spriteUVScaleVariable = effect->GetVariableByName( "g_SpriteUVScale" )->AsVector();

		const D3D11_INPUT_ELEMENT_DESC layoutDesc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "ParticlePos", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "ParticleColor", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "ParticleSize", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "SpriteFrame", 0, DXGI_FORMAT_R32_UINT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		};
		SafeRelease( _inputLayout );
		_effectPass->CreateInputLayout( layoutDesc, sizeof(layoutDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC), &_inputLayout );
	}

	// Create a new state
	ParticleEmitterState* ParticleEmitter::CreateState( const char* name )
	{
		ParticleEmitterState* state = new ParticleEmitterState;
		state->SetName( name );
		return state;
	}

	// Add an existing state
	void ParticleEmitter::AddState( ParticleEmitterState* state )
	{
		_states.Add( state );
	}

	// Remove a state
	void ParticleEmitter::RemoveState( const char* name )
	{
		for ( uint32 i = 0; i < _states.Size(); ++i )
		{
			if ( _states[i]->GetName() == name )
			{
				ParticleEmitterState* state = _states[i];
				_states.RemoveAt( i );
				return;
			}
		}
	}

	// Remove a state
	void ParticleEmitter::RemoveState( ParticleEmitterState* state )
	{
		_states.Remove( state );
	}

	// Set the active state
	void ParticleEmitter::SetState( ParticleEmitterState* state )
	{
		if ( state == _activeState )
			return;

		// De-activate the current state
		if ( _activeState )
		{
			// Handle transition periods
			for ( uint32 i = 0; i < _activeState->GetNumParticleSets(); ++i )
			{
				_activeState->GetParticleSet( i )->Disable();
				_transitions.Add( _activeState->GetParticleSet( i ) );
			}
		}

		// Set the new state and reset it
		_activeState = state;
		_stateTimer = 0;
		Clear();
		if ( _activeState )
		{
			for ( uint32 i = 0; i < _activeState->GetNumParticleSets(); ++i )
				_activeState->GetParticleSet( i )->Enable();
		}
	}

	// Set the active state
	void ParticleEmitter::SetState( const char* name )
	{
		for ( uint32 i = 0; i < _states.Size(); ++i )
		{
			if ( _states[i]->GetName() == name )
			{
				SetState( _states[i] );
				return;
			}
		}
	}

	// Set the active state
	void ParticleEmitter::SetState( std::nullptr_t null )
	{
		SetState( (ParticleEmitterState*)nullptr );
	}


	// Reset all the active systems
	void ParticleEmitter::Clear()
	{
		if ( _activeState )
		{
			for ( uint32 i = 0; i < _activeState->GetNumParticleSets(); ++i )
				_activeState->GetParticleSet( i )->Clear();
		}
	}

	// Create a new particle set
	ParticleSet* ParticleEmitter::CreateParticleSet()
	{
		return new ParticleSet( _graphics, this );
	}

	// Render a particle set
	void ParticleEmitter::RenderSet( ParticleSet* set )
	{
		// Bind the instance buffer
		_buffers[1] = set->_instanceBuffer->GetBuffer();
		_strides[1] = set->_instanceBuffer->GetByteStride();
		_graphics->GetContext()->IASetVertexBuffers( 0, 2, _buffers, _strides, _offsets );

		// Bind the texture
		_textureVariable->SetResource( set->_texture ? *set->_texture : nullptr );

		// Shader vars
		if ( set->IsAnimated() )
		{
			uint32 spriteDim[] = { set->GetSpriteSheetWidth(), set->GetSpriteSheetHeight() };
			Vector2 framesize = Vector2( (float)set->GetAnimationFrameWidth(), (float)set->GetAnimationFrameHeight() );
			Vector2 texSize = Vector2( (float)set->GetTexture()->GetWidth(), (float)set->GetTexture()->GetHeight() );
			Vector2 texScale = Vector2( framesize.x / texSize.x, framesize.y / texSize.y );
			_spriteDimensionsVariable->SetIntVector( spriteDim );
			_animationFrameSizeVariable->SetFloatVector( framesize );
			_textureDimensionsVariable->SetFloatVector( texSize );
			_spriteUVScaleVariable->SetFloatVector( texScale );

			if ( _engine->GetPlatform()->KeyDown( Keys::L ) )
				_animatedSoftPass->Apply();
			else
				_animatedPass->Apply();
		}
		else
		{
			_effectPass->Apply();
		}

		// Render this set
		_graphics->GetContext()->DrawInstanced( 6, set->_vbData.Size(), 0, 0 );
	}

}
//--------------------------------------------------------------------------------------
// File: ParticleEmitter.h
//
// Particle systems
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Game/Component.h"

namespace Maoli
{
	// Forward decl
	class ParticleEmitterState;

	// Particle system
	class ParticleEmitter : public Component
	{
		friend class Renderer;

	public:
		COMPONENT_DECL( ParticleEmitter );

		// Ctor
		ParticleEmitter( Engine* engine );

		// Dtor
		virtual ~ParticleEmitter();

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Register component dependencies
		virtual void RegisterDependency( Component* dependency );


		// Get the local position
		const Vector3& GetPosition() const { return _position; }

		// Set the local position
		void SetPosition( const Vector3& val ) { _position = val; }

		// Render all systems
		void Render();

		// Create a new particle set
		ParticleSet* CreateParticleSet();

		// Create a new state
		ParticleEmitterState* CreateState( const char* name );

		// Add an existing state
		void AddState( ParticleEmitterState* state );

		// Remove a state
		void RemoveState( const char* name );
		void RemoveState( ParticleEmitterState* state );

		// Set the active state
		void SetState( ParticleEmitterState* state );
		void SetState( const char* name );
		void SetState( std::nullptr_t null );

		// Get a state by index
		inline ParticleEmitterState* GetState( int32 index ) { return _states[index]; }

		// Get the number of states
		inline uint32 GetNumStates() const { return _states.Size(); }

		// Reset all the active systems
		void Clear();

		// Get the active state
		inline ParticleEmitterState* GetActiveState() { return _activeState; }

		// Get the parent transform
		inline Transform* GetTransform() { return _transform; }

		// Return true if there are active particles
		inline bool HasActiveParticles() const { return (_activeState != nullptr || !_transitions.IsEmpty()); }

	private:

		// Bind effect variables for rendering
		void BindEffect( Effect* effect );

		// Render a particle set
		void RenderSet( ParticleSet* set );


		Renderer*						_graphics;			// Renderer that owns the system
		Buffer<Vertex>::pointer			_vertexBuffer;		// Particle quad vertex buffer
		ID3D11Buffer*					_buffers[2];		// Vertex buffers
		uint32							_strides[2];		// Vertex buffer byte strides
		uint32							_offsets[2];		// Dummy var
		ShaderResource*					_textureVariable;	// Texture for the particle shader
		EffectPass*						_effectPass;		// Rendering effect
		EffectPass*						_animatedPass;		// Rendering effect
		EffectPass*						_animatedSoftPass;	// Rendering effect
		ID3D11InputLayout*				_inputLayout;		// Vertex layout
		Vector3							_position;			// Local position
		Array<ParticleEmitterState*>	_states;			// All states
		List<ParticleSet*>				_transitions;		// Transitioning systems
		ParticleEmitterState*			_activeState;		// Currently active state
		float							_stateTimer;		// Timer for tracking state transitions

		// Shader data
		ShaderVector*					_spriteDimensionsVariable;
		ShaderVector*					_animationFrameSizeVariable;
		ShaderVector*					_textureDimensionsVariable;
		ShaderVector*					_spriteUVScaleVariable;

		// Dependencies
		Transform*			_transform;
	};
}
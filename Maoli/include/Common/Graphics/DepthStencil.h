//--------------------------------------------------------------------------------------
// File: DepthStencil.h
//
// Depth stencil texture class
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma once

#include "Graphics/PipelineResource.h"

namespace Maoli
{
	// Forward decl
	class Renderer;
	class ShaderResource;

	// Manages a depth stencil view.
	// Typically used with the Texture class.
	class DepthStencil : public Resource<Renderer>, public PipeplineResource
	{
		friend class Renderer;
		friend class Pointer<DepthStencil>;
	public:

		// Smart pointer
		typedef Pointer<DepthStencil> pointer;

		// Clear the DSV.
		void Clear();

		// Get the shader resource views
		virtual ID3D11ShaderResourceView**	GetSRV(){ return _srv; }

		// Get the render target views
		virtual ID3D11RenderTargetView**	GetRTV(){ return nullptr; }

		// Get the unordered access views
		virtual ID3D11UnorderedAccessView**	GetUAV(){ return nullptr; }

		// Returns a pointer to the dsv
		inline ID3D11DepthStencilView* GetDSV(){ return _dsv; }

		// Returns a pointer to the read only dsv
		inline ID3D11DepthStencilView* GetReadOnlyDSV() { return _dsvReadOnly; }

		// Create a read only dsv
		bool CreateReadOnlyView();

	private:

		// Ctor
		DepthStencil(Renderer* parent);

		// Dtor
		~DepthStencil();

		// Release resources associated with this buffer.  The destructor will call this.
		void Release();

		// Create a standard 2d depth stencil view
		HRESULT Create(uint32 width, uint32 height, DXGI_SAMPLE_DESC* pMSAA, bool asTexture);

		// Creates a cubemap depth stencil view
		HRESULT CreateCube(uint32 width, uint32 height);

		// Textures
		ID3D11Texture2D*				_tex;
		ID3D11ShaderResourceView*		_srv[1];
		ID3D11DepthStencilView*			_dsvReadOnly;
		ID3D11DepthStencilView*			_dsv;
	};

}

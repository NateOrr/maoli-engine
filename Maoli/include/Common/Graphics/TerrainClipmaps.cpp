//--------------------------------------------------------------------------------------
// File: TerrainClipmaps.cpp
//
// Implementation of terrain clipmap system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Terrain.h"
#include "Graphics/Renderer.h"

namespace Maoli
{

	// Creates the clipmap building blocks
	void Terrain::SetupClipmap(int32 size, int32 numLevels)
	{
		if(size>1023)
			return;

		// Sizes
		m_ClipmapSize = size;
		m_BlockSize = (size+1)/4;
		m_ClipmapLevels = numLevels;


		// Get the total number of verts needed
		// so we can consolodate into a single
		// vertex buffer to eliminate state changes
		int32 totalVertLength = m_BlockSize*m_BlockSize +		// Block
			m_BlockSize*3 +				// Ring fix
			m_BlockSize*3 +				// Ring fix
			8*m_BlockSize+4 +				// Trim
			8*m_BlockSize+4 +				// Trim
			8*m_BlockSize+4 +				// Trim
			8*m_BlockSize+4 +				// Trim
			8*m_BlockSize;				// Center trim

		// Allocate the vertex array
		Array<PosVertex> vertexList;
		vertexList.Allocate(totalVertLength);

		// MxM block verts
		m_VBOffsets[VB_BLOCK] = 0;
		int32 index=0;
		for(int32 j=0; j<m_BlockSize; j++)
			for(int32 i=0; i<m_BlockSize; i++, index++)
				vertexList[index].pos = Vector3((float)i, 0, -(float)j);		

		// Mx3 ring fix verts
		m_VBOffsets[VB_RING1] = index;
		for(int32 j=0; j<3; j++)
			for(int32 i=0; i<m_BlockSize; i++, index++)
				vertexList[index].pos = Vector3((float)i, 0, -(float)j);		

		// 3xM ring fix verts
		m_VBOffsets[VB_RING2] = index;
		for(int32 i=0; i<m_BlockSize; i++)
			for(int32 j=0; j<3; j++, index++)
				vertexList[index].pos = Vector3((float)j, 0, -(float)i);

		// Trim verts (top and left)
		m_VBOffsets[VB_TRIM_TL] = index;
		for(int32 j=0; j<2; j++)
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)i, 0, -(float)j);
		for(int32 j=0; j<2; j++)	
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)j, 0, -(float)i);

		// Trim verts (top and right)
		m_VBOffsets[VB_TRIM_TR] = index;
		for(int32 j=0; j<2; j++)
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)i, 0, -(float)j);
		for(int32 j=0; j<2; j++)	
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)(j+2*m_BlockSize-1), 0, -(float)i);

		// Trim verts (bottom and right)
		m_VBOffsets[VB_TRIM_BR] = index;
		for(int32 j=0; j<2; j++)
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)i, 0, (float)(-j-(2*m_BlockSize-1)));
		for(int32 j=0; j<2; j++)	
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)(j+2*m_BlockSize-1), 0, (float)(-i+1));

		// Trim verts (bottom and left)
		m_VBOffsets[VB_TRIM_BL] = index;
		for(int32 j=0; j<2; j++)
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)i, 0, (float)(-j-(2*m_BlockSize-1)));
		for(int32 j=0; j<2; j++)	
			for(int32 i=0; i<2*m_BlockSize+1; i++, index++)
				vertexList[index].pos = Vector3((float)j, 0, (float)(-i+1));

		// Center Trim verts
		m_VBOffsets[VB_CENTER_TRIM] = index;
		for(int32 j=0; j<2; j++)
			for(int32 i=0; i<2*m_BlockSize; i++, index++)
				vertexList[index].pos = Vector3((float)i, 0, -(float)j);
		for(int32 j=0; j<2; j++)	
			for(int32 i=0; i<2*m_BlockSize; i++, index++)
				vertexList[index].pos = Vector3((float)j, 0, -(float)i);



		// Get the total index length
		int32 totalIndexLength = (m_BlockSize-1)*(m_BlockSize-1)*6 +	// Block Indices
			(m_BlockSize-1)*12 +					// Ring fix Indices
			(m_BlockSize-1)*12 +					// Ring fix Indices
			(2*m_BlockSize)*12-6 +				// Trim Indices
			(2*m_BlockSize-1)*12;				// CenterTrim Indices

		// Allocate the index array
		Array<USHORT> indexList;
		indexList.Allocate(totalIndexLength);

		// Block Indices
		index=0;
		m_IBOffsets[IB_BLOCK] = index;
		for(int32 j=0; j<m_BlockSize-1; j++)
		{
			for(int32 i=0; i<m_BlockSize-1; i++, index+=6)
			{
				// Set the indices for this quad
				int32 nIndex = i+j*m_BlockSize;
				indexList[index+5]   = nIndex;
				indexList[index+4] = nIndex + m_BlockSize;
				indexList[index+3] = nIndex + m_BlockSize + 1;
				indexList[index+2] = nIndex + m_BlockSize + 1;
				indexList[index+1] = nIndex + 1;
				indexList[index] = nIndex;
			}
		}
		m_IBCount[IB_BLOCK] = index-m_IBOffsets[IB_BLOCK];

		// Ring fix Indices
		m_IBOffsets[IB_RING1] = index;
		for(int32 j=0; j<2; j++)
		{
			for(int32 i=0; i<m_BlockSize-1; i++, index+=6)
			{
				// Set the indices for this quad
				int32 nIndex = i+j*m_BlockSize;
				indexList[index+5]   = nIndex;
				indexList[index+4] = nIndex + m_BlockSize;
				indexList[index+3] = nIndex + m_BlockSize + 1;
				indexList[index+2] = nIndex + m_BlockSize + 1;
				indexList[index+1] = nIndex + 1;
				indexList[index] = nIndex;
			}
		}
		m_IBCount[IB_RING1] = index-m_IBOffsets[IB_RING1];

		// Ring fix Indices
		m_IBOffsets[IB_RING2] = index;
		for(int32 i=0; i<m_BlockSize-1; i++)
		{
			for(int32 j=0; j<2; j++, index+=6)
			{
				// Set the indices for this quad
				int32 nIndex = j+i*3;
				indexList[index+5]   = nIndex;
				indexList[index+4] = nIndex + 3;
				indexList[index+3] = nIndex + 3 + 1;
				indexList[index+2] = nIndex + 3 + 1;
				indexList[index+1] = nIndex + 1;
				indexList[index] = nIndex;
			}
		}
		m_IBCount[IB_RING2] = index-m_IBOffsets[IB_RING2];

		// Trim Indices
		m_IBOffsets[IB_TRIM] = index;
		for(int32 i=0; i<2*m_BlockSize; i++, index+=6)
		{
			// Set the indices for this quad
			int32 nIndex = i;
			indexList[index+5]   = nIndex;
			indexList[index+4] = nIndex + 2*m_BlockSize+1;
			indexList[index+3] = nIndex + 2*m_BlockSize+1 + 1;
			indexList[index+2] = nIndex + 2*m_BlockSize+1 + 1;
			indexList[index+1] = nIndex + 1;
			indexList[index] = nIndex;
		}
		for(int32 i=1; i<2*m_BlockSize; i++, index+=6)
		{
			// Set the indices for this quad
			int32 nIndex = i + 4*m_BlockSize+2;
			indexList[index]   = nIndex;
			indexList[index+1] = nIndex + 2*m_BlockSize+1;
			indexList[index+2] = nIndex + 2*m_BlockSize+1 + 1;
			indexList[index+3] = nIndex + 2*m_BlockSize+1 + 1;
			indexList[index+4] = nIndex + 1;
			indexList[index+5] = nIndex;
		}
		m_IBCount[IB_TRIM] = index-m_IBOffsets[IB_TRIM];

		// CenterTrim Indices
		m_IBOffsets[IB_CENTER_TRIM] = index;
		for(int32 i=0; i<2*m_BlockSize-1; i++, index+=6)
		{
			// Set the indices for this quad
			int32 nIndex = i;
			indexList[index+5]   = nIndex;
			indexList[index+4] = nIndex + 2*m_BlockSize;
			indexList[index+3] = nIndex + 2*m_BlockSize + 1;
			indexList[index+2] = nIndex + 2*m_BlockSize + 1;
			indexList[index+1] = nIndex + 1;
			indexList[index] = nIndex;
		}
		for(int32 i=0; i<2*m_BlockSize-1; i++, index+=6)
		{
			// Set the indices for this quad
			int32 nIndex = i + 4*m_BlockSize;
			indexList[index]   = nIndex;
			indexList[index+1] = nIndex + 2*m_BlockSize;
			indexList[index+2] = nIndex + 2*m_BlockSize + 1;
			indexList[index+3] = nIndex + 2*m_BlockSize + 1;
			indexList[index+4] = nIndex + 1;
			indexList[index+5] = nIndex;
		}
		m_IBCount[IB_CENTER_TRIM] = index-m_IBOffsets[IB_CENTER_TRIM];


		// Fill the buffers
		{
			// Master vertex buffer
			D3D11_BUFFER_DESC bd;
			bd.Usage = D3D11_USAGE_IMMUTABLE;
			bd.ByteWidth = sizeof( PosVertex ) * vertexList.Size();
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;
			D3D11_SUBRESOURCE_DATA InitData;
			InitData.pSysMem = (PosVertex*)vertexList;
			_graphics->GetDevice()->CreateBuffer( &bd, &InitData, &m_VertexBuffer );

			// Master index buffer
			bd.ByteWidth = sizeof( USHORT ) * indexList.Size();;
			bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
			InitData.pSysMem = (USHORT*)indexList;
			_graphics->GetDevice()->CreateBuffer( &bd, &InitData, &m_IndexBuffer );
		}

		vertexList.Release();
		indexList.Release();

		//-----------------
		// Block positions
		float fBlockSize = (float)m_BlockSize;
		Vector2 offsetPos = Vector2(-2*(fBlockSize-1), (fBlockSize-1));

		// Top row
		m_BlockOffsets[0] = offsetPos+Vector2(0, 0);
		m_BlockOffsets[1] = offsetPos+Vector2((fBlockSize-1), 0);
		m_BlockOffsets[2] = offsetPos+Vector2(2*(fBlockSize-1)+2, 0);
		m_BlockOffsets[3] = offsetPos+Vector2(3*(fBlockSize-1)+2, 0);
		// Bottom row
		m_BlockOffsets[4] = offsetPos+Vector2(0, -3*(fBlockSize-1)-2);
		m_BlockOffsets[5] = offsetPos+Vector2((fBlockSize-1), -3*(fBlockSize-1)-2);
		m_BlockOffsets[6] = offsetPos+Vector2(2*(fBlockSize-1)+2, -3*(fBlockSize-1)-2);
		m_BlockOffsets[7] = offsetPos+Vector2(3*(fBlockSize-1)+2, -3*(fBlockSize-1)-2);
		// Left column
		m_BlockOffsets[8] = offsetPos+Vector2(0, -(fBlockSize-1));
		m_BlockOffsets[9] = offsetPos+Vector2(0, -2*(fBlockSize-1)-2);
		// Right column
		m_BlockOffsets[10] = offsetPos+Vector2(3*(fBlockSize-1)+2, -(fBlockSize-1));
		m_BlockOffsets[11] = offsetPos+Vector2(3*(fBlockSize-1)+2, -2*(fBlockSize-1)-2);

		// Setup the trim positioning arrays
		m_TrimXSide = new BYTE[m_ClipmapLevels];
		m_TrimZSide = new BYTE[m_ClipmapLevels];
		memset(m_TrimXSide, 0, m_ClipmapLevels);
		memset(m_TrimZSide, 0, m_ClipmapLevels);

		// Cached clipmap positions
		m_ClipmapOffsets = new Vector2[m_ClipmapLevels-1];

		// Create the clipmap height textures
		DXGI_FORMAT* formats = new DXGI_FORMAT[m_ClipmapLevels-1];
		for(int32 i=0; i<m_ClipmapLevels-1; i++)
			formats[i] = DXGI_FORMAT_R32_FLOAT;
		m_Clipmaps = _graphics->CreateTextureArray(m_ClipmapSize, m_ClipmapSize, m_ClipmapLevels-1, formats, 1, nullptr, D3D11_USAGE_DEFAULT, true, true);
		if(!m_Clipmaps)
		{
			ErrorMessage("failed to m_Clipmaps.CreateArray");
		}

		delete[] formats;
	}



	// Update a clipmap
	void Terrain::UpdateClipmaps(Effect& effect)
	{
		// Setup the viewport and ortho matrix
		_graphics->SetViewport(m_Clipmaps->GetViewport());

		// Set the heightmap
		HeightmapVariable->SetResource(*m_Heightmap);

		// Update each level
		float size = 1.0f;
		Vector3 camPos(floorf(_graphics->GetCamera()->GetPosition().x), 0, floorf(_graphics->GetCamera()->GetPosition().z));
		for(int32 i=0; i<m_ClipmapLevels-1; i++, size*=2)
		{
			// Calculate the new position
			int32 G = (int32)size;
			int32 N = m_ClipmapSize;

			// Calculate the modulo to G2 of the new position.
			// This makes sure that the current level always fits in the hole of the
			// coarser level. The gridspacing of the coarser level is G * 2
			int32 G2 = G*2;
			int32 modX = (int32)(camPos.x) % G2;
			int32 modY = (int32)(camPos.z) % G2;
			modX += modX < 0 ? G2 : 0;
			modY += modY < 0 ? G2 : 0;
			m_ClipmapOffsets[i].x = (camPos.x)+(G2 - modX);
			m_ClipmapOffsets[i].y = (camPos.z)+(G2 - modY);

			// Compute the trim locations
			bool update = false;	
			if(G2 - modX > G)
			{
				if(m_TrimXSide[i]==0)
					update = true;
				m_TrimXSide[i] = 1;
			}
			else
			{
				if(m_TrimXSide[i]==1)
					update = true;
				m_TrimXSide[i] = 0;
			}

			if(G2 - modY > G)
			{
				if(m_TrimZSide[i]==0)
					update = true;
				m_TrimZSide[i] = 1;
			}
			else
			{
				if(m_TrimZSide[i]==1)
					update = true;
				m_TrimZSide[i] = 0;
			}

			if(!update && !m_FlagForUpdate)
				continue;

			// Update!
			ClipmapOffsetVariable->SetFloatVector((float*)&m_ClipmapOffsets[i]);
			ClipmapScaleVariable->SetFloat(size);
			ClipmapSizeVariable->SetFloat((float)m_ClipmapSize);
			HeightmapSizeVariable->SetFloat((float)m_Size);
			_graphics->BindRenderTarget(*m_Clipmaps, i);
			_clipmapUpdatePass->Apply();
			_graphics->GetContext()->Draw(6, 0);
		}
		m_FlagForUpdate = false;
	}



	// Draws a clipmap level
	int32 Terrain::Render(EffectPass* effectPass)
	{
		// Set the blend map if needed
		if(m_RenderMode == TerrainMode::Blendmap)
			BlendmapVariable->SetResource(*m_Blendmap);

		// Used for offsetting the building blocks
		Vector2 gridOffset;
		Vector3 blockBounds, cullPos, cullBounds;

		// Just for a little optimizing (not worth it i know :)
		const float b2 = m_BlockSize*0.5f;
		const float fBlockSize = (float)m_BlockSize;

		// Discrete camera location in the grid
		Vector3 camPos(floorf(_graphics->GetCamera()->GetPosition().x), 0, floorf(_graphics->GetCamera()->GetPosition().z));

		ClipmapSizeVariable->SetFloat((float)m_ClipmapSize);
		HeightmapSizeVariable->SetFloat((float)m_Size);
		HeightmapScaleVariable->SetFloat(m_HeightScale);

		// Compute the height bounds
		float hCenter = (m_HeightExtents.y-m_HeightExtents.x)*0.5f;
		float hMax = m_HeightExtents.y-hCenter;

		// Keep track of poly count
		int32 polyCount = 0;

		// Patch size
		float size = 1;

		// The finest level is different
		{
			// Bind the clipmaps as textures
			ClipmapVariable->SetResource(*m_Clipmaps);

			// Set the patch size
			ClipmapScaleVariable->SetFloat(size);
			ClipmapFixVariable->SetFloatVector((float*)&m_ClipmapOffsets[0]);

			// Offset
			Vector2 offsetTrim;
			int32 modX = (int32)camPos.x % 2;
			int32 modY = (int32)camPos.z % 2;
			modX += modX < 0 ? 2 : 0;
			modY += modY < 0 ? 2 : 0;
			offsetTrim.x = (float)(modX-2);
			offsetTrim.y = (float)(modY-2);

			//------------
			// Draw the trim

			// Set the patch offset position
			gridOffset = offsetTrim + Vector2(1-fBlockSize, fBlockSize);
			ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
			
			// Draw the trim
			effectPass->Apply();
			_graphics->GetContext()->DrawIndexed(m_IBCount[IB_CENTER_TRIM], m_IBOffsets[IB_CENTER_TRIM], m_VBOffsets[VB_CENTER_TRIM]);
			polyCount += m_IBCount[IB_CENTER_TRIM]/3;


			//------------
			// Draw the blocks

			// Block positions
			Vector2 offsets[4];
			offsets[0] = Vector2(1, 1);
			offsets[1] = Vector2(2-fBlockSize, 1);
			offsets[2] = Vector2(1, 2-fBlockSize);
			offsets[3] = Vector2(2-fBlockSize, 2-fBlockSize);

			// 4 blocks
			blockBounds = Vector3(fBlockSize, hMax, fBlockSize);
			cullBounds = blockBounds*0.5f;
			for(int32 i=0; i<4; i++)
			{
				// Set the patch offset position
				gridOffset = offsetTrim + Vector2(offsets[i].x, offsets[i].y + (m_BlockSize-1)-1);
				if(_graphics->GetCamera()->GetFrustum().CheckAABB(camPos+Vector3(gridOffset.x + b2, hCenter, gridOffset.y - b2), blockBounds))
				{
					// Draw the patch
					ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
					effectPass->Apply();
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_BLOCK], m_IBOffsets[IB_BLOCK], m_VBOffsets[VB_BLOCK]);
					polyCount += m_IBCount[IB_BLOCK]/3;
				}
			}
		}

		// Render the coarser levels
		for(int32 level=0, size=1; level<m_ClipmapLevels-1; level++, size*=2)
		{
			// Set the level size
			ClipmapScaleVariable->SetFloat((float)size);

			// Bind the clipmaps as textures
			ClipmapVariable->SetResource(*m_Clipmaps, level);

			// Set the offset
			ClipmapFixVariable->SetFloatVector((float*)&m_ClipmapOffsets[level]);

			// Draw the trim
			{
				// Trim offset
				gridOffset = Vector2(-size*(fBlockSize+1), size*(fBlockSize-1));					

				// Draw the trim
				ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
				effectPass->Apply();
				if(m_TrimZSide[level]==1 && m_TrimXSide[level]==0)
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_TRIM], m_IBOffsets[IB_TRIM], m_VBOffsets[VB_TRIM_TL]);
				else if(m_TrimZSide[level]==1 && m_TrimXSide[level]==1)
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_TRIM], m_IBOffsets[IB_TRIM], m_VBOffsets[VB_TRIM_TR]);
				else if(m_TrimZSide[level]==0 && m_TrimXSide[level]==1)
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_TRIM], m_IBOffsets[IB_TRIM], m_VBOffsets[VB_TRIM_BR]);
				else
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_TRIM], m_IBOffsets[IB_TRIM], m_VBOffsets[VB_TRIM_BL]);
				polyCount += m_IBCount[IB_TRIM]/3;
			}

			// Draw the blocks
			blockBounds = Vector3(fBlockSize*size, hMax, fBlockSize*size);
			cullBounds = blockBounds*0.5f;
			float b2s = b2*size;
			for(int32 i=0; i<12; i++)
			{
				// Set the patch offset position
				gridOffset = Vector2(size*(m_BlockOffsets[i].x-2), size*(m_BlockOffsets[i].y+m_BlockSize-1));

				// Draw the patch if it is visible and contained inside the terrain bounds
				cullPos = camPos+Vector3(gridOffset.x+b2s, hCenter, gridOffset.y-b2s);
				if(_graphics->GetCamera()->GetFrustum().CheckAABB(cullPos, blockBounds) &&
					!( cullPos.x-cullBounds.x > m_Size/2 || cullPos.x+cullBounds.x < -m_Size/2 ||
					cullPos.z-cullBounds.z > m_Size/2 || cullPos.z+cullBounds.z < -m_Size/2 ))
				{
					ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
					effectPass->Apply();
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_BLOCK], m_IBOffsets[IB_BLOCK], m_VBOffsets[VB_BLOCK]);
					polyCount += m_IBCount[IB_BLOCK]/3;
				}
			}


			// Draw the ring fix strips
			{
				// Left
				gridOffset = Vector2(-2*(float)size*fBlockSize, 0);
				blockBounds = Vector3(fBlockSize*(float)size, hMax, 6*(float)size);
				cullBounds = blockBounds*0.5f;
				cullPos = camPos+Vector3(gridOffset.x + b2s, hCenter, gridOffset.y);
				if(_graphics->GetCamera()->GetFrustum().CheckAABB(cullPos, blockBounds) &&
					!( cullPos.x-cullBounds.x > m_Size/2 || cullPos.x+cullBounds.x < -m_Size/2 ||
					cullPos.z-cullBounds.z > m_Size/2 || cullPos.z+cullBounds.z < -m_Size/2 ))
				{
					ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
					effectPass->Apply();
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_RING1], m_IBOffsets[IB_RING1], m_VBOffsets[VB_RING1]);
					polyCount += m_IBCount[IB_RING1]/3;
				}

				// Right
				gridOffset = Vector2((float)size*(fBlockSize-1), 0);
				cullPos = camPos+Vector3(gridOffset.x +b2s, hCenter, gridOffset.y);
				if(_graphics->GetCamera()->GetFrustum().CheckAABB(cullPos, blockBounds) &&
					!( cullPos.x-cullBounds.x > m_Size/2 || cullPos.x+cullBounds.x < -m_Size/2 ||
					cullPos.z-cullBounds.z > m_Size/2 || cullPos.z+cullBounds.z < -m_Size/2 ))
				{
					ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
					effectPass->Apply();
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_RING1], m_IBOffsets[IB_RING1], m_VBOffsets[VB_RING1]);
					polyCount += m_IBCount[IB_RING1]/3;
				}

				// Top
				gridOffset = Vector2(-2.0f*size, 2.0f*size*(fBlockSize-1));
				blockBounds = Vector3(6.0f*size, (float)hMax, fBlockSize*size);
				cullBounds = blockBounds*0.5f;
				cullPos = camPos+Vector3(gridOffset.x+1.5f*size, hCenter, gridOffset.y - b2s);
				if(_graphics->GetCamera()->GetFrustum().CheckAABB(cullPos, blockBounds) &&
					!( cullPos.x-cullBounds.x > m_Size/2 || cullPos.x+cullBounds.x < -m_Size/2 ||
					cullPos.z-cullBounds.z > m_Size/2 || cullPos.z+cullBounds.z < -m_Size/2 ))
				{
					ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
					effectPass->Apply();
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_RING2], m_IBOffsets[IB_RING2], m_VBOffsets[VB_RING2]);
					polyCount += m_IBCount[IB_RING2]/3;
				}

				// Bottom
				gridOffset = Vector2(-2.0f*size, -size*(fBlockSize+1));
				cullPos = camPos+Vector3(gridOffset.x+1.5f*size, (float)hCenter, gridOffset.y - b2s);
				if(_graphics->GetCamera()->GetFrustum().CheckAABB(cullPos, blockBounds) &&
					!( cullPos.x-cullBounds.x > m_Size/2 || cullPos.x+cullBounds.x < -m_Size/2 ||
					cullPos.z-cullBounds.z > m_Size/2 || cullPos.z+cullBounds.z < -m_Size/2 ))
				{
					ClipmapOffsetVariable->SetFloatVector((float*)&gridOffset);
					effectPass->Apply();
					_graphics->GetContext()->DrawIndexed(m_IBCount[IB_RING2], m_IBOffsets[IB_RING2], m_VBOffsets[VB_RING2]);
					polyCount += m_IBCount[IB_RING2]/3;
				}
			}

		}
		return polyCount;
	}

}

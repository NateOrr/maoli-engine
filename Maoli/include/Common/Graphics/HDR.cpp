//--------------------------------------------------------------------------------------
// File: HDR.cpp
//
// HDR System.  Handles buffer creation and HDR post-processing
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/HDR.h"
#include "Graphics/Renderer.h"

namespace Maoli
{

	// Setup the hdr system
	HRESULT HDRSystem::Create(uint32 width, uint32 height, DepthStencil::pointer dsv, Effect* pEffect)
	{
		m_Width = width;
		m_Height = height;
		
		// Screen sized hdr buffer
		m_HDRBuffer = _graphics->CreateTextureEx(width, height, DXGI_FORMAT_R16G16B16A16_TYPELESS, 1, nullptr, D3D11_USAGE_DEFAULT, 
			DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_R16G16B16A16_FLOAT);
		if(!m_HDRBuffer)
		{
			M_ERROR("Failed to create HDR texture");
			return E_FAIL;
		}

		// Get the max power of 2 number that is <= buffersize/4
		Vector2 bufferSize = Vector2((float)(width/4), (float)(height/4));
		int32 count=0;
		while(bufferSize.x>2.0f && bufferSize.y>2.0f)
		{
			count++;
			bufferSize *= 0.5f;
		}
		
		// Create a series of scaled luminance buffers
		m_HDRScaledLuminance.Allocate(count);
		m_ScaledLuminanceDepth.Allocate(count);
		int32 pow2 = 2;
		for(int32 i=count-1; i>=2; i--)
		{
			m_HDRScaledLuminance[i] = _graphics->CreateTexture(width/pow2, height/pow2, DXGI_FORMAT_R16G16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true);
			m_ScaledLuminanceDepth[i] = _graphics->CreateDepthStencil(width/pow2, height/pow2, nullptr, false);
			m_HDRScaledLuminance[i]->AttachDepthStencil(m_ScaledLuminanceDepth[i]);
			pow2 *= 2;
		}
		m_HDRScaledLuminance[1] = _graphics->CreateTexture(4, 4, DXGI_FORMAT_R16G16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true);
		m_ScaledLuminanceDepth[1] = _graphics->CreateDepthStencil(4, 4, nullptr, false);
		m_HDRScaledLuminance[1]->AttachDepthStencil(m_ScaledLuminanceDepth[1]);
		m_HDRScaledLuminance[0] = _graphics->CreateTexture(2, 2, DXGI_FORMAT_R16G16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true);
		m_ScaledLuminanceDepth[0] = _graphics->CreateDepthStencil(2, 2, nullptr, false);
		m_HDRScaledLuminance[0]->AttachDepthStencil(m_ScaledLuminanceDepth[0]);
		

		// Luminance buffers
		m_HDRLuminance[0] = _graphics->CreateTexture(1, 1, DXGI_FORMAT_R16G16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true);
		m_HDRLuminance[1] = _graphics->CreateTexture(1, 1, DXGI_FORMAT_R16G16_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true);
		m_LuminanceDepth = _graphics->CreateDepthStencil(1, 1, nullptr, false);
		m_HDRLuminance[0]->AttachDepthStencil(m_LuminanceDepth);
		m_HDRLuminance[1]->AttachDepthStencil(m_LuminanceDepth);

		// Bind depth buffers
		m_HDRBuffer->AttachDepthStencil(dsv);

		// Bind the effect vars
		BindEffectVariables();

		return S_OK;
	}

	// Bind the effect variables
	void HDRSystem::BindEffectVariables()
	{
		// Get variables
		HDRVariable = _graphics->GetEffect()->GetVariableByName( "g_txHDR" )->AsShaderResource(); 
		LuminanceVariable = _graphics->GetEffect()->GetVariableByName( "g_txLuminance" )->AsShaderResource(); 
		AdaptedLuminanceVariable = _graphics->GetEffect()->GetVariableByName( "g_txAdaptedLuminance" )->AsShaderResource(); 
		GaussianWeightsH = _graphics->GetEffect()->GetVariableByName( "g_GWeightsH" )->AsScalar(); 
		GaussianWeightsV = _graphics->GetEffect()->GetVariableByName( "g_GWeightsV" )->AsScalar(); 
		GaussianOffsetsH = _graphics->GetEffect()->GetVariableByName( "g_GOffsetsH" )->AsScalar();
		GaussianOffsetsV = _graphics->GetEffect()->GetVariableByName( "g_GOffsetsV" )->AsScalar(); 

		// Passes
		_downscalePass				= _graphics->GetEffect()->GetPassByName( "DownScale" );
		_downscaleLuminancePass		= _graphics->GetEffect()->GetPassByName( "DownScaleLuminance" );
		_adaptLuminancePass			= _graphics->GetEffect()->GetPassByName( "Adapt" );
		_brightPass					= _graphics->GetEffect()->GetPassByName( "BrightPass" );
		_tonemapPass				= _graphics->GetEffect()->GetPassByName( "ToneMap" );

		// Build the gaussian blur filters
		ComputeGaussians(m_Width/2, m_Height/2);
	}


	// Free all hdr related resources
	void HDRSystem::Release()
	{
		m_HDRBuffer = nullptr;
		for(uint32 i=0; i<m_HDRScaledLuminance.Size(); i++)
		{
			m_HDRScaledLuminance[i] = nullptr;
			m_ScaledLuminanceDepth[i] = nullptr;
		}
		m_HDRScaledLuminance.Release();
		m_ScaledLuminanceDepth.Release();
		m_HDRLuminance[0] = nullptr;
		m_HDRLuminance[1] = nullptr;
		m_LuminanceDepth = nullptr;
	}

	
	// Create the final scene image with HDR post-processing
	void HDRSystem::ProcessHDRI(Texture::pointer backBuffer)
	{
		// Down-sample the scene to a 1/4 size luminance texture
		_graphics->SetRenderToQuad();
		int32 scaledIndex = m_HDRScaledLuminance.Size() - 1;
		m_HDRScaledLuminance[scaledIndex]->Clear();
		_graphics->BindRenderTarget(*m_HDRScaledLuminance[scaledIndex]);
		_graphics->SetViewport(m_HDRScaledLuminance[scaledIndex]->GetViewport());
		HDRVariable->SetResource(*m_HDRBuffer);		
		_downscalePass->Apply();
		_graphics->GetContext()->Draw(6, 0);
		scaledIndex--;
		
		// Progressively downscale the luminance values
		for(; scaledIndex>=0; scaledIndex--)
		{
			// Set the current scaled target to render to
			_graphics->BindRenderTarget(*m_HDRScaledLuminance[scaledIndex]);
			m_HDRScaledLuminance[scaledIndex]->Clear();			
			_graphics->GetContext()->RSSetViewports(1, &m_HDRScaledLuminance[scaledIndex]->GetViewport());
			
			// Set the previous scaled target as the texture
			LuminanceVariable->SetResource(*m_HDRScaledLuminance[scaledIndex+1]);

			// Continue the downscaling
			_downscaleLuminancePass->Apply();
			_graphics->GetContext()->Draw(6, 0);
		}

		// Compute the final adapted luminance value
		if(m_CurrentLum==0){ m_CurrentLum=1; m_OldLum=0; }
		else { m_CurrentLum=0; m_OldLum=1; }
		m_HDRLuminance[m_CurrentLum]->Clear();
		_graphics->BindRenderTarget(*m_HDRLuminance[m_CurrentLum]);
		_graphics->GetContext()->RSSetViewports(1, &m_HDRLuminance[m_CurrentLum]->GetViewport());
		LuminanceVariable->SetResource(*m_HDRScaledLuminance[0]);
		AdaptedLuminanceVariable->SetResource(*m_HDRLuminance[m_OldLum]);
		_adaptLuminancePass->Apply();
		_graphics->GetContext()->Draw(6, 0);	

		// Tone-map the image, and send the bright pixels to be bloomed
		_graphics->BindRenderTarget(*backBuffer);
		AdaptedLuminanceVariable->SetResource(*m_HDRLuminance[m_CurrentLum]);
		_graphics->SetViewport( backBuffer->GetViewport() );
		_tonemapPass->Apply();
		_graphics->GetContext()->Draw(6, 0);	
	}


	//-----------------------------------------------------------------------------
	// Compute the gaussian offsets and weights, and send them to the shader
	//-----------------------------------------------------------------------------
	void HDRSystem::ComputeGaussians(uint32 width, uint32 height)
	{
		const int32 numSamples = 9;
		const float g_BrightThreshold = 0.8f;       // A configurable parameter into the pixel shader
		const float g_GaussMultiplier = 0.4f;       // Default multiplier
		const float g_GaussMean = 0.0f;             // Default mean for gaussian distribution
		const float g_GaussStdDev = 0.2f;           // Default standard deviation for gaussian distribution

		// Configure the horizontal sampling offsets and their weights
		float HBloomWeights[numSamples];
		float HBloomOffsets[numSamples];
		for( int32 i = 0; i < numSamples; i++ )
		{
			// Compute the offsets. We take 9 samples - 4 either side and one in the middle:
			//     i =  0,  1,  2,  3, 4,  5,  6,  7,  8
			//Offset = -4, -3, -2, -1, 0, +1, +2, +3, +4
			HBloomOffsets[i] = ( static_cast< float >( i ) - 4.0f ) * ( 1.0f / static_cast< float >( width ) );

			// 'x' is just a simple alias to map the [0,8] range down to a [-1,+1]
			float x = ( static_cast< float >( i ) - 4.0f ) / 4.0f;

			// Use a gaussian distribution. Changing the standard-deviation
			// (second parameter) as well as the amplitude (multiplier) gives
			// distinctly different results.
			HBloomWeights[i] = g_GaussMultiplier * Math::ComputeGaussianValue( x, g_GaussMean, g_GaussStdDev );
		}

		// Configure the vertical sampling offsets and their weights
		float VBloomWeights[numSamples];
		float VBloomOffsets[numSamples];
		for( int32 i = 0; i < numSamples; i++ )
		{
			// Compute the offsets. We take 9 samples - 4 either side and one in the middle:
			//     i =  0,  1,  2,  3, 4,  5,  6,  7,  8
			//Offset = -4, -3, -2, -1, 0, +1, +2, +3, +4
			VBloomOffsets[i] = ( static_cast< float >( i ) - 4.0f ) * ( 1.0f / static_cast< float >( height ) );

			// 'x' is just a simple alias to map the [0,8] range down to a [-1,+1]
			float x = ( static_cast< float >( i ) - 4.0f ) / 4.0f;

			// Use a gaussian distribution. Changing the standard-deviation
			// (second parameter) as well as the amplitude (multiplier) gives
			// distinctly different results.
			VBloomWeights[i] = g_GaussMultiplier * Math::ComputeGaussianValue( x, g_GaussMean, g_GaussStdDev );
		}

		// Send these arrays to the shader
		GaussianOffsetsH->SetFloatArray(HBloomOffsets, 0, numSamples);
		GaussianOffsetsV->SetFloatArray(VBloomOffsets, 0, numSamples);
		GaussianWeightsH->SetFloatArray(HBloomWeights, 0, numSamples);
		GaussianWeightsV->SetFloatArray(VBloomWeights, 0, numSamples);			
	}


}

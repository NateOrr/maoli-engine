//--------------------------------------------------------------------------------------
// File: SpriteBatcher.cpp
//
// 2D Sprite rendering
// 
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/SpriteBatcher.h"
#include "Graphics/Renderer.h"

namespace Maoli
{
	// Ctor
	SpriteBatcher::SpriteBatcher( Renderer* graphics )
	{
		_graphics = graphics;

		// Create a quad
		SpriteVertex verts[] = {
			Vector4( 0.0f,  0.0f, 0.5f, 1.0f ),
			Vector4( 1.0f,  0.0f, 0.5f, 1.0f ),
			Vector4( 1.0f,  1.0f, 0.5f, 1.0f ),
			Vector4( 1.0f,  1.0f, 0.5f, 1.0f ),
			Vector4( 0.0f,  1.0f, 0.5f, 1.0f ),
			Vector4( 0.0f,  0.0f, 0.5f, 1.0f ), };

		// Setup the quad vertex buffer
		_quadVertexBuffer = _graphics->CreateVertexBuffer<SpriteVertex>(6, D3D11_USAGE_IMMUTABLE, verts);
		M_ASSERT(_quadVertexBuffer, "Failed to create the quad vertex buffer for the sprite batcher");

		// Setup the orthographic projection matrix
		_orthoMatrix.Orthographic((float)_graphics->GetWidth(), (float)_graphics->GetHeight(), 0.1f, 1.0f);

		// Load the default texture
		_whiteTexture = _graphics->CreateTextureFromFile( "Resources\\Textures\\white.jpg" );
	}

	// Dtor
	SpriteBatcher::~SpriteBatcher()
	{
		_sprites.clear();
		_batchesToRender.Release();		
		_quadVertexBuffer = nullptr;	
		_whiteTexture = nullptr;		
	}


	// Get/create the batch info for a texture
	SpriteBatcher::Batch& SpriteBatcher::GetBatch( Texture::pointer tex )
	{
		// Check if this texture already has a buffer, and return it if so
		auto spriteIter = _sprites.find(tex->GetID());
		if(spriteIter != _sprites.end())
			return spriteIter->second;

		// It does not have a buffer, so create one
		Batch batch;
		batch.instanceData = _graphics->CreateBuffer<Sprite>(_maxInstances, D3D11_USAGE_DYNAMIC, false, true, nullptr);
		M_ASSERT(batch.instanceData, "Failed to create an instance buffer for the sprite batcher");
		batch.texture = tex;
		batch.needsRender = false;
		return _sprites.insert(BatchPair(tex->GetID(), batch)).first->second;
	}

	// Render all sprite batches
	void SpriteBatcher::RenderSprites()
	{
		// Set the vertex buffer
		_graphics->SetVertexBuffer(_quadVertexBuffer->GetBuffer(), SpriteVertex::size);
		_graphics->SetInputLayout(SpriteVertex::pInputLayout);

		// Set the projection
		_worldMatrixVariable->SetMatrix(_orthoMatrix);


		for(uint32 i=0; i<_batchesToRender.Size(); ++i)
		{
			// Copy data to vram
			auto mappedData = _batchesToRender[i]->instanceData->Map(D3D11_MAP_WRITE_DISCARD);
			memcpy(mappedData, _batchesToRender[i]->instances.GetRawData(), sizeof(Sprite)*_batchesToRender[i]->instances.Size());
			_batchesToRender[i]->instanceData->Unmap();

			// Set the sprite texture and instance data
			if(_batchesToRender[i]->texture)
				_spriteTextureVariable->SetResource(*_batchesToRender[i]->texture);
			else
				_spriteTextureVariable->SetResource(nullptr);
			_spriteInstanceDataVariable->SetResource(*_batchesToRender[i]->instanceData);
			_effectPass->Apply();

			// Draw the instances
			_graphics->GetContext()->DrawInstanced(6, _batchesToRender[i]->instances.Size(), 0, 0);

			// Clear the instance resource
			_spriteInstanceDataVariable->SetResource(nullptr);
			_effectPass->Apply();

			// Reset the instance counter
			_batchesToRender[i]->instances.Clear();
			_batchesToRender[i]->needsRender = false;
		}
		_batchesToRender.Clear();
	}


	// Draw a textured rectangle region
	void SpriteBatcher::Draw( int32 x, int32 y, int32 width, int32 height, const Rect& region, Texture::pointer texture, const Color& color )
	{
		// First grab the batch
		Batch& batch = GetBatch(texture);

		// Ignore this request if the max number of instances is already reached
		if(batch.instances.Size() == _maxInstances)
			return;

		// Set the sprite data
		Sprite& sprite = batch.instances.Add();
		sprite.scale = Vector2( float(width), float(-height) );
		sprite.position = Vector2( float(x), float(-y) );
		sprite.color = color;
		sprite.region = Vector4((float)region.x, (float)region.y, (float)region.width, (float)region.height);

		// Add to the render list
		if(!batch.needsRender)
		{
			_batchesToRender.Add(&batch);
			batch.needsRender = true;
		}
	}

	// Draw a rectangle
	void SpriteBatcher::Draw( int32 x, int32 y, int32 width, int32 height, Texture::pointer texture, const Color& color )
	{
		Draw(x, y, width, height, Rect(0, 0, texture->GetWidth(), texture->GetHeight()), texture, color);
	}

	// Draw a solid color recangle
	void SpriteBatcher::Draw( int32 x, int32 y, int32 width, int32 height, const Color& color )
	{
		Draw(x, y, width, height, Rect(), _whiteTexture, color);
	}



	// Draw some text
	void SpriteBatcher::DrawText( const char* text, Font::pointer font, int32 x, int32 y, float scale, const Color& color )
	{
		int32 index = 0;
		POINT location = {x, y};
		const int32 spaceSize = int32(32*scale);
		while(text[index])
		{
			char actualChar = text[index];
			if(font->_characters.find(actualChar) != font->_characters.end())
			{
				const Rect& r = font->_characters[actualChar];

				Draw(location.x, location.y, int32(scale*r.width), int32(scale*r.height), r, font->_sheet, color);

				location.x += (int32)(r.width * scale);
			}
			else
				location.x += spaceSize;
			++index;
		}
	}


	// Setup effect variables and passes
	void SpriteBatcher::BindEffectVariables()
	{
		_worldMatrixVariable = _graphics->GetEffect()->GetVariableByName( "g_mWorld" )->AsMatrix();
		_spriteTextureVariable = _graphics->GetEffect()->GetVariableByName( "g_txSprite" )->AsShaderResource(); 
		_spriteInstanceDataVariable = _graphics->GetEffect()->GetVariableByName( "g_SpriteInstances" )->AsShaderResource();

		_effectPass = _graphics->GetEffect()->GetPassByName("Sprite");
	}

}

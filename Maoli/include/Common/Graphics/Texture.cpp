//--------------------------------------------------------------------------------------
// File: Texture.cpp
//
// Render target texture class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Texture.h"
#include "DirectXTex.h"
#include "Platform/Win32.h"

namespace Maoli
{
	// Constructor
	Texture::Texture( Renderer* parent ) : Resource( parent )
	{
		_arraySize = 0;
		_resourceArraySize = 0;
		_dsv = nullptr;
		_isCube = false;
		_clearColor[0] = _clearColor[1] = _clearColor[2] = _clearColor[3] = 0.0f;
		_width = _height = 0;
	}

	// Dtor
	Texture::~Texture()
	{
		Release();
	}

	// Create a 2D render target from a texture
	HRESULT Texture::CreateFromTexture( ID3D11Texture2D* pTex, bool isRTV, bool isSRV )
	{
		// Release previous texture
		Release();

		// Allocate the arrays
		_arraySize = 1;
		_tex.Allocate( _arraySize, true );
		if ( isRTV )
			_rtv.Allocate( _arraySize, true );
		if ( isSRV )
			_srv.Allocate( _arraySize, true );

		// Misc flags
		uint32 mipLevels = 1;

		// Get the texture info
		D3D11_TEXTURE2D_DESC td;
		pTex->GetDesc( &td );
		_width = td.Width;
		_height = td.Height;

		// Set the texture
		_tex[0] = pTex;

		// Create RTV
		if ( isRTV )
		{
			D3D11_RENDER_TARGET_VIEW_DESC dsrtv;
			dsrtv.Format = td.Format;
			dsrtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			dsrtv.Texture2D.MipSlice = 0;
			auto hr = _parent->GetDevice()->CreateRenderTargetView( _tex[0], &dsrtv, &_rtv[0] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to CreateRenderTargetView" );
		}

		// Create SRV
		if ( isSRV )
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC dssrv;
			dssrv.Format = td.Format;
			dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			dssrv.Texture2D.MipLevels = mipLevels;
			dssrv.Texture2D.MostDetailedMip = 0;
			auto hr = _parent->GetDevice()->CreateShaderResourceView( _tex[0], &dssrv, &_srv[0] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to CreateShaderResourceView" );
		}

		// Setup the viewport
		_viewport.Height = (float)td.Height;
		_viewport.Width = (float)td.Width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		return S_OK;
	}


	// Create a 2d texture
	// SRV and RTV use same format as the texture
	HRESULT Texture::Create( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV )
	{
		DXGI_FORMAT rtvFormat = isRTV ? format : DXGI_FORMAT_UNKNOWN;
		DXGI_FORMAT srvFormat = isSRV ? format : DXGI_FORMAT_UNKNOWN;
		return CreateEx( width, height, format, mipLevels, pMSAA, usage, rtvFormat, srvFormat, DXGI_FORMAT_UNKNOWN );
	}

	// Create a texture from some initial data
	HRESULT Texture::CreateFromData( uint32 width, uint32 height, DXGI_FORMAT format, D3D11_USAGE usage, bool srv, void* data )
	{
		// Release previous texture
		Release();

		// Allocate the arrays
		_arraySize = 1;
		_tex.Allocate( _arraySize, true );
		if ( srv )
			_srv.Allocate( _arraySize, true );

		// Setup the texture desc
		D3D11_TEXTURE2D_DESC dstex;

		// Bind flags
		dstex.BindFlags = srv ? D3D11_BIND_SHADER_RESOURCE : 0;

		// Misc flags
		dstex.MiscFlags = 0;

		// MSAA
		dstex.SampleDesc.Count = 1;
		dstex.SampleDesc.Quality = 0;

		// Usage and cpu access
		dstex.Usage = usage;
		dstex.CPUAccessFlags = 0;
		if ( usage == D3D11_USAGE_DYNAMIC )
			dstex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		else if ( usage == D3D11_USAGE_STAGING )
			dstex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;

		// Rest of the properties
		dstex.ArraySize = 1;
		dstex.MipLevels = 1;
		_width = dstex.Width = width;
		_height = dstex.Height = height;
		dstex.Format = format;

		// Figure out the byte stride from the format
		int32 stride = 4;
		M_ASSERT( format == DXGI_FORMAT_R8G8B8A8_UNORM || format == DXGI_FORMAT_R32_FLOAT, "Format must be DXGI_FORMAT_R8G8B8A8_UNORM" );

		// Initial data
		D3D11_SUBRESOURCE_DATA initialData;
		initialData.pSysMem = data;
		initialData.SysMemPitch = width*stride;
		initialData.SysMemSlicePitch = 0;

		// Create the texture
		auto hr = _parent->GetDevice()->CreateTexture2D( &dstex, &initialData, &_tex[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTexture2D" );

		// Create SRV
		if ( srv )
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC dssrv;
			dssrv.Format = format;
			dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			dssrv.Texture2D.MipLevels = 1;
			dssrv.Texture2D.MostDetailedMip = 0;
			auto hr = _parent->GetDevice()->CreateShaderResourceView( _tex[0], &dssrv, &_srv[0] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to CreateShaderResourceView" );
		}

		// Setup the viewport
		_viewport.Height = (float)height;
		_viewport.Width = (float)width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		return S_OK;
	}


	// Create a 2D texture, with more options
	// Use DXGI_FORMAT_UNKNOWN when no view is to be created
	HRESULT Texture::CreateEx( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage,
		DXGI_FORMAT rtvFormat, DXGI_FORMAT srvFormat, DXGI_FORMAT uavFormat )
	{
		// Release previous texture
		Release();

		// Creation flags
		bool isRTV = (rtvFormat != DXGI_FORMAT_UNKNOWN);
		bool isSRV = (srvFormat != DXGI_FORMAT_UNKNOWN);
		bool isUAV = (uavFormat != DXGI_FORMAT_UNKNOWN);

		// Allocate the arrays
		_arraySize = 1;
		_tex.Allocate( _arraySize, true );
		if ( isRTV )
			_rtv.Allocate( _arraySize, true );
		if ( isSRV )
			_srv.Allocate( _arraySize, true );
		if ( isUAV )
			_uav.Allocate( _arraySize, true );


		// Setup the texture desc
		D3D11_TEXTURE2D_DESC dstex;

		// Bind flags
		dstex.BindFlags = 0;
		if ( isRTV )
			dstex.BindFlags = (dstex.BindFlags == 0) ? D3D11_BIND_RENDER_TARGET : dstex.BindFlags | D3D11_BIND_RENDER_TARGET;
		if ( isSRV )
			dstex.BindFlags = (dstex.BindFlags == 0) ? D3D11_BIND_SHADER_RESOURCE : dstex.BindFlags | D3D11_BIND_SHADER_RESOURCE;
		if ( isUAV )
			dstex.BindFlags = (dstex.BindFlags == 0) ? D3D11_BIND_UNORDERED_ACCESS : dstex.BindFlags | D3D11_BIND_UNORDERED_ACCESS;

		// Compute mip levels
		if ( mipLevels == 0 )
		{
			uint32 dim = std::max( width, height );
			while ( dim > 4 )
			{
				++mipLevels;
				dim /= 2;
			}
		}

		// Misc flags
		uint32 flags = 0;
		if ( mipLevels != 1 && isRTV )
			flags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
		dstex.MiscFlags = flags;

		// MSAA
		dstex.SampleDesc.Count = 1;
		dstex.SampleDesc.Quality = 0;
		if ( pMSAA )
		{
			dstex.SampleDesc.Count = pMSAA->Count;
			dstex.SampleDesc.Quality = pMSAA->Quality;
		}

		// Usage and cpu access
		dstex.Usage = usage;
		dstex.CPUAccessFlags = 0;
		if ( usage == D3D11_USAGE_DYNAMIC )
			dstex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		else if ( usage == D3D11_USAGE_STAGING )
			dstex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;

		// Rest of the properties
		dstex.ArraySize = 1;
		dstex.MipLevels = mipLevels;
		_width = dstex.Width = width;
		_height = dstex.Height = height;
		dstex.Format = format;

		// Create the texture
		auto hr = _parent->GetDevice()->CreateTexture2D( &dstex, nullptr, &_tex[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTexture2D" );

		// Create RTV
		if ( isRTV )
		{
			D3D11_RENDER_TARGET_VIEW_DESC dsrtv;
			dsrtv.Format = rtvFormat;
			if ( pMSAA )
				dsrtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
			else
			{
				dsrtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
				dsrtv.Texture2D.MipSlice = 0;
			}
			auto hr = _parent->GetDevice()->CreateRenderTargetView( _tex[0], &dsrtv, &_rtv[0] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to CreateRenderTargetView" );
		}

		// Create SRV
		if ( isSRV )
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC dssrv;
			dssrv.Format = srvFormat;
			if ( pMSAA )
				dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
			else
			{
				dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				dssrv.Texture2D.MipLevels = mipLevels;
				dssrv.Texture2D.MostDetailedMip = 0;
			}
			auto hr = _parent->GetDevice()->CreateShaderResourceView( _tex[0], &dssrv, &_srv[0] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to CreateShaderResourceView" );
		}

		// Create UAV
		if ( isUAV )
		{
			D3D11_UNORDERED_ACCESS_VIEW_DESC dsuav;
			dsuav.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
			dsuav.Format = uavFormat;
			dsuav.Texture2D.MipSlice = 0;
			auto hr = _parent->GetDevice()->CreateUnorderedAccessView( _tex[0], &dsuav, &_uav[0] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to CreateUnorderedAccessView" );
		}


		// Setup the viewport
		_viewport.Height = (float)height;
		_viewport.Width = (float)width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		return S_OK;
	}

	// Create a 2D texture array
	HRESULT Texture::CreateArray( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT* formats, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV )
	{
		// Release previous texture
		Release();

		// Allocate the arrays
		HRESULT hr;
		_arraySize = arraySize;
		_tex.Allocate( _arraySize, true );
		if ( isRTV )
			_rtv.Allocate( _arraySize, true );
		if ( isSRV )
			_srv.Allocate( _arraySize, true );


		// Setup the texture desc
		D3D11_TEXTURE2D_DESC dstex;

		// Bind flags
		dstex.BindFlags = 0;
		if ( isRTV )
			dstex.BindFlags = (dstex.BindFlags == 0) ? D3D11_BIND_RENDER_TARGET : dstex.BindFlags | D3D11_BIND_RENDER_TARGET;
		if ( isSRV )
			dstex.BindFlags = (dstex.BindFlags == 0) ? D3D11_BIND_SHADER_RESOURCE : dstex.BindFlags | D3D11_BIND_SHADER_RESOURCE;

		// Misc flags
		uint32 flags = 0;
		if ( mipLevels > 1 && isRTV && isSRV )
			flags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
		if ( mipLevels == 0 ) mipLevels++;
		dstex.MiscFlags = flags;

		// MSAA
		dstex.SampleDesc.Count = 1;
		dstex.SampleDesc.Quality = 0;
		if ( pMSAA )
		{
			dstex.SampleDesc.Count = pMSAA->Count;
			dstex.SampleDesc.Quality = pMSAA->Quality;
		}

		// Usage and cpu access
		dstex.Usage = usage;
		dstex.CPUAccessFlags = 0;
		if ( usage == D3D11_USAGE_DYNAMIC )
			dstex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		else if ( usage == D3D11_USAGE_STAGING )
			dstex.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ;

		// Rest of the properties
		dstex.ArraySize = 1;
		dstex.MipLevels = mipLevels;
		_width = dstex.Width = width;
		_height = dstex.Height = height;

		// Create the textures
		for ( uint32 i = 0; i < _arraySize; i++ )
		{
			dstex.Format = formats[i];
			hr = _parent->GetDevice()->CreateTexture2D( &dstex, nullptr, &_tex[i] );
			M_ASSERT( SUCCEEDED( hr ), "CreateTexture2D Failed" );
		}

		// Create RTV
		if ( isRTV )
		{
			D3D11_RENDER_TARGET_VIEW_DESC dsrtv;
			if ( pMSAA )
				dsrtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
			else
			{
				dsrtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
				dsrtv.Texture2D.MipSlice = 0;
			}
			for ( uint32 i = 0; i < _arraySize; i++ )
			{
				dsrtv.Format = formats[i];
				hr = _parent->GetDevice()->CreateRenderTargetView( _tex[i], &dsrtv, &_rtv[i] );
				M_ASSERT( SUCCEEDED( hr ), "CreateRenderTargetView Failed" );
			}
		}

		// Create SRV
		if ( isSRV )
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC dssrv;
			if ( pMSAA )
				dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
			else
			{
				dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				dssrv.Texture2D.MipLevels = mipLevels;
				dssrv.Texture2D.MostDetailedMip = 0;
			}
			for ( uint32 i = 0; i < _arraySize; i++ )
			{
				dssrv.Format = formats[i];
				hr = _parent->GetDevice()->CreateShaderResourceView( _tex[i], &dssrv, &_srv[i] );
				M_ASSERT( SUCCEEDED( hr ), "CreateShaderResourceView Failed" );
			}
		}


		// Setup the viewport
		_viewport.Height = (float)height;
		_viewport.Width = (float)width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		return S_OK;
	}


	// Create a texture array, where the actual resource is bound as an array
	HRESULT Texture::CreateArrayResource( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels, bool rtv )
	{
		// Allocate the arrays
		_arraySize = 1;
		_resourceArraySize = arraySize;
		_tex.Allocate( 1 );
		_srv.Allocate( 1 );
		if ( rtv )
			_rtv.Allocate( arraySize );

		// Compute mip levels
		if ( mipLevels == 0 )
		{
			uint32 dim = std::max( width, height );
			while ( dim > 4 )
			{
				++mipLevels;
				dim /= 2;
			}
		}


		// Setup the texture desc
		D3D11_TEXTURE2D_DESC dstex;
		dstex.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		if ( rtv )
			dstex.BindFlags |= D3D11_BIND_RENDER_TARGET;
		dstex.SampleDesc.Count = 1;
		dstex.SampleDesc.Quality = 0;
		dstex.Usage = D3D11_USAGE_DEFAULT;
		dstex.CPUAccessFlags = 0;
		dstex.ArraySize = arraySize;
		dstex.MipLevels = mipLevels;
		_width = dstex.Width = width;
		_height = dstex.Height = height;
		dstex.Format = format;

		// Misc flags
		uint32 flags = 0;
		if ( mipLevels != 1 && rtv )
			flags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
		dstex.MiscFlags = flags;


		// Create the texture
		HRESULT hr = _parent->GetDevice()->CreateTexture2D( &dstex, nullptr, &_tex[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateTexture2D" );

		// Create the srv
		D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory( &SRVDesc, sizeof(SRVDesc) );
		SRVDesc.Format = dstex.Format;
		SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2DARRAY;
		SRVDesc.Texture2DArray.FirstArraySlice = 0;
		SRVDesc.Texture2DArray.MostDetailedMip = 0;
		SRVDesc.Texture2DArray.MipLevels = dstex.MipLevels;
		SRVDesc.Texture2DArray.ArraySize = arraySize;
		hr = _parent->GetDevice()->CreateShaderResourceView( _tex[0], &SRVDesc, &_srv[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to CreateShaderResourceView" );

		// Create rtv's
		if ( rtv )
		{
			D3D11_RENDER_TARGET_VIEW_DESC dsrtv;
			dsrtv.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
			dsrtv.Texture2DArray.MipSlice = 0;
			dsrtv.Texture2DArray.ArraySize = 1;
			dsrtv.Format = format;
			for ( uint32 i = 0; i < arraySize; ++i )
			{
				dsrtv.Texture2DArray.FirstArraySlice = i;
				hr = _parent->GetDevice()->CreateRenderTargetView( _tex[0], &dsrtv, &_rtv[i] );
				M_ASSERT( SUCCEEDED( hr ), "CreateRenderTargetView Failed" );
			}
		}

		return S_OK;
	}

	// Insert a texture into an array resource
	HRESULT Texture::InsertTexture( const char* file, uint32 index )
	{
		D3DX11_IMAGE_LOAD_INFO loadInfo;
		memset( &loadInfo, 0, sizeof(D3DX11_IMAGE_LOAD_INFO) );
		loadInfo.Width = _width;
		loadInfo.Height = _height;
		loadInfo.Depth = 0;
		loadInfo.FirstMipLevel = 0;
		loadInfo.MipLevels = 1;
		loadInfo.Usage = D3D11_USAGE_STAGING;
		loadInfo.BindFlags = 0;
		loadInfo.CpuAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
		loadInfo.MiscFlags = 0;
		loadInfo.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		loadInfo.Filter = D3DX11_FILTER_LINEAR;
		loadInfo.MipFilter = D3DX11_FILTER_LINEAR;

		// Load the Texture
		ID3D11Resource* pRes = nullptr;
		HRESULT hr = D3DX11CreateTextureFromFileA( _parent->GetDevice(), file, &loadInfo, nullptr, &pRes, nullptr );
		M_ASSERT( SUCCEEDED( hr ), "Failed to load texture for array insertion" );

		ID3D11Texture2D* pTemp;
		D3D11_TEXTURE2D_DESC desc;
		pRes->QueryInterface( __uuidof(ID3D11Texture2D), (LPVOID*)&pTemp );
		pTemp->GetDesc( &desc );

		D3D11_MAPPED_SUBRESOURCE mappedTex2D;
		if ( desc.MipLevels > 4 )
			desc.MipLevels -= 4;

		for ( UINT iMip = 0; iMip < desc.MipLevels; iMip++ )
		{
			hr = _parent->GetContext()->Map( pTemp, iMip, D3D11_MAP_READ, 0, &mappedTex2D );
			M_ASSERT( SUCCEEDED( hr ), "Failed to map resource" );

			_parent->GetContext()->UpdateSubresource( _tex[0],
				D3D11CalcSubresource( iMip, index, desc.MipLevels ),
				nullptr,
				mappedTex2D.pData,
				mappedTex2D.RowPitch,
				0 );

			_parent->GetContext()->Unmap( pTemp, iMip );
		}

		SafeRelease( pRes );
		SafeRelease( pTemp );

		return S_OK;
	}


	// Create a cube map render target
	HRESULT Texture::CreateCube( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, bool asSingleCube )
	{
		// Release previous texture
		Release();

		_isCube = true;
		_tex.Allocate( 1 );
		if ( !asSingleCube )
		{
			_arraySize = 6;
			_rtv.Allocate( _arraySize, true );
		}
		else
		{
			_arraySize = 1;
			_rtv.Allocate( 1 );
		}
		_srv.Allocate( _arraySize, true );

		// Create cube map texture
		D3D11_TEXTURE2D_DESC dstex;
		ZeroMemory( &dstex, sizeof(dstex) );
		_width = dstex.Width = width;
		_height = dstex.Height = height;
		dstex.MipLevels = mipLevels;
		dstex.ArraySize = 6;
		dstex.SampleDesc.Count = 1;
		dstex.SampleDesc.Quality = 0;
		dstex.Format = format;
		dstex.Usage = D3D11_USAGE_DEFAULT;
		dstex.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		dstex.CPUAccessFlags = 0;
		dstex.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS | D3D11_RESOURCE_MISC_TEXTURECUBE;
		HRESULT hr = _parent->GetDevice()->CreateTexture2D( &dstex, nullptr, &_tex[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to create dstex" );

		// Create the one-face render target views
		D3D11_RENDER_TARGET_VIEW_DESC DescRT;
		ZeroMemory( &DescRT, sizeof(DescRT) );
		DescRT.Format = dstex.Format;
		DescRT.Texture2DArray.MipSlice = 0;
		if ( asSingleCube )
		{
			DescRT.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
			DescRT.Texture2DArray.ArraySize = 6;
			DescRT.Texture2DArray.FirstArraySlice = 0;
			hr = _parent->GetDevice()->CreateRenderTargetView( _tex[0], &DescRT, &_rtv[0] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to create m_RTV[0]" );

		}
		else
		{
			DescRT.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
			DescRT.Texture2DArray.ArraySize = 1;
			for ( int32 i = 0; i < 6; ++i )
			{
				DescRT.Texture2DArray.FirstArraySlice = i;
				hr = _parent->GetDevice()->CreateRenderTargetView( _tex[0], &DescRT, &_rtv[i] );
				M_ASSERT( SUCCEEDED( hr ), "Failed to create m_RTV[i]" );
			}
		}


		// Create the shader resource view for the cubic env map
		D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory( &SRVDesc, sizeof(SRVDesc) );
		SRVDesc.Format = dstex.Format;
		SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		SRVDesc.TextureCube.MipLevels = mipLevels;
		SRVDesc.TextureCube.MostDetailedMip = 0;
		hr = _parent->GetDevice()->CreateShaderResourceView( _tex[0], &SRVDesc, &_srv[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to create m_SRV[0]" );


		// Setup the viewport
		_viewport.Height = (float)dstex.Height;
		_viewport.Width = (float)dstex.Width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		return S_OK;
	}

	// Create a cube map array
	HRESULT Texture::CreateCubeArray( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels )
	{
		// Release previous texture
		Release();

		_isCube = true;
		_tex.Allocate( 1 );
		_srv.Allocate( 1 );
		_arraySize = arraySize;
		_rtv.Allocate( _arraySize * 6, true );

		// Compute mip levels
		if ( mipLevels == 0 )
		{
			uint32 dim = std::max( width, height );
			while ( dim > 4 )
			{
				++mipLevels;
				dim /= 2;
			}
		}

		// Create cube map texture
		D3D11_TEXTURE2D_DESC dstex;
		ZeroMemory( &dstex, sizeof(dstex) );
		_width = dstex.Width = width;
		_height = dstex.Height = height;
		dstex.MipLevels = mipLevels;
		dstex.ArraySize = 6 * arraySize;
		dstex.SampleDesc.Count = 1;
		dstex.SampleDesc.Quality = 0;
		dstex.Format = format;
		dstex.Usage = D3D11_USAGE_DEFAULT;
		dstex.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		dstex.CPUAccessFlags = 0;
		dstex.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;
		if ( mipLevels != 1 )
			dstex.MiscFlags |= D3D11_RESOURCE_MISC_GENERATE_MIPS;
		HRESULT hr = _parent->GetDevice()->CreateTexture2D( &dstex, nullptr, &_tex[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to create dstex" );

		// Create the one-face render target views
		D3D11_RENDER_TARGET_VIEW_DESC DescRT;
		ZeroMemory( &DescRT, sizeof(DescRT) );
		DescRT.Format = dstex.Format;
		DescRT.Texture2DArray.MipSlice = 0;
		DescRT.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
		DescRT.Texture2DArray.ArraySize = 1;
		for ( uint32 i = 0; i < 6 * arraySize; ++i )
		{
			DescRT.Texture2DArray.FirstArraySlice = i;
			hr = _parent->GetDevice()->CreateRenderTargetView( _tex[0], &DescRT, &_rtv[i] );
			M_ASSERT( SUCCEEDED( hr ), "Failed to create m_RTV[i]" );
		}


		// Create the shader resource view for the each cubic env map
		D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory( &SRVDesc, sizeof(SRVDesc) );
		SRVDesc.Format = dstex.Format;
		SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBEARRAY;
		SRVDesc.TextureCubeArray.MipLevels = mipLevels;
		SRVDesc.TextureCubeArray.MostDetailedMip = 0;
		SRVDesc.TextureCubeArray.First2DArrayFace = 0;
		SRVDesc.TextureCubeArray.NumCubes = arraySize;
		hr = _parent->GetDevice()->CreateShaderResourceView( _tex[0], &SRVDesc, &_srv[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed to create m_SRV[0]" );


		// Setup the viewport
		_viewport.Height = (float)dstex.Height;
		_viewport.Width = (float)dstex.Width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		return S_OK;
	}


	// Loads a texture from a file
	bool Texture::Load( const char* file, bool cube )
	{
		if ( !file )
			return false;

		// First try to load with the raw path
		return LoadFromFile( file, cube );
	}


	// Load a TGA from file
	ID3D11Texture2D* Texture::LoadBaseTexture( const char* file )
	{
		// Load it
		String ext = String( file ).GetFileExtension();
		WCHAR fileName[256];
		Win32::AnsiToUnicode( file, fileName, 256 );
		DirectX::ScratchImage loadImage;
		if ( ext == "tga" )
		{
			HRESULT hr = DirectX::LoadFromTGAFile( fileName, nullptr, loadImage );
			M_ASSERT( SUCCEEDED( hr ), "Failed LoadFromTGAFile" );
		}
		else if ( ext == "dds" )
		{
			HRESULT hr = DirectX::LoadFromDDSFile( fileName, DirectX::DDS_FLAGS_NONE, nullptr, loadImage );
			M_ASSERT( SUCCEEDED( hr ), "Failed LoadFromDDSFile" );
		}
		else
		{
			HRESULT hr = DirectX::LoadFromWICFile( fileName, DirectX::WIC_FLAGS_NONE, nullptr, loadImage );
			M_ASSERT( SUCCEEDED( hr ), "Failed LoadFromWICFile" );
		}

		// Generate the mip chain
		DirectX::ScratchImage mipChain;
		HRESULT hr;
		if(ext == "dds")
			hr = E_FAIL;
		else
			hr = DirectX::GenerateMipMaps( loadImage.GetImages(), loadImage.GetImageCount(), loadImage.GetMetadata(), DirectX::TEX_FILTER_DEFAULT, 0, mipChain );
		DirectX::ScratchImage& image = SUCCEEDED( hr ) ? mipChain : loadImage;
		const DirectX::Image* img = image.GetImage( 0, 0, 0 );
		int32 numMips = image.GetMetadata().mipLevels;

		// Setup the desc
		D3D11_TEXTURE2D_DESC td;
		ZeroMemory( &td, sizeof(D3D11_TEXTURE2D_DESC) );
		td.ArraySize = 1;
		td.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		td.Format = img->format;
		td.MipLevels = numMips;
		td.Width = img->width;
		td.Height = img->height;
		td.SampleDesc.Count = 1;
		td.Usage = D3D11_USAGE_IMMUTABLE;
		td.CPUAccessFlags = 0;

		// Initial data
		Array<D3D11_SUBRESOURCE_DATA> initialData( numMips );
		for ( int32 i = 0; i < numMips; ++i )
		{
			const DirectX::Image* mip = image.GetImage( i, 0, 0 );
			initialData[i].pSysMem = mip->pixels;
			initialData[i].SysMemPitch = mip->rowPitch;
			initialData[i].SysMemSlicePitch = mip->slicePitch;
		}

		// Create the texture
		ID3D11Texture2D* tex = nullptr;
		hr = _parent->GetDevice()->CreateTexture2D( &td, initialData, &tex );
		M_ASSERT( SUCCEEDED( hr ), "Failed CreateTexture2D" );

		return tex;
	}


	// Loads a texture from a file
	bool Texture::LoadFromFile( const char* file, bool cube )
	{
		if ( !file )
			return false;

		// Release previous texture
		Release();

		// Load the file into a temp texture
		ID3D11Texture2D* tTex = nullptr;
		
		if ( !cube )
			tTex = LoadBaseTexture( file );
		else
			D3DX11CreateTextureFromFileA( _parent->GetDevice(), file, nullptr, nullptr, (ID3D11Resource**)&tTex, nullptr );

		// If the load was successful, store the new texture
		if ( !_tex.IsEmpty() )
			Release();
		_tex.Allocate( 1 );
		_tex[0] = tTex;

		// Setup the SRV desc
		D3D11_TEXTURE2D_DESC td;
		tTex->GetDesc( &td );
		D3D11_SHADER_RESOURCE_VIEW_DESC dssrv;
		dssrv.Format = td.Format;
		if ( cube )
		{
			dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
			dssrv.TextureCube.MipLevels = td.MipLevels;
			dssrv.TextureCube.MostDetailedMip = 0;
		}
		else
		{
			dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			dssrv.Texture2D.MipLevels = td.MipLevels;
			dssrv.Texture2D.MostDetailedMip = 0;
		}

		// Create the SRV
		_srv.Allocate( 1 );
		HRESULT hr = _parent->GetDevice( )->CreateShaderResourceView( _tex[0], &dssrv, &_srv[0] );
		M_ASSERT( SUCCEEDED( hr ), "Failed CreateShaderResourceView" );

		// Get the width and height
		_width = td.Width;
		_height = td.Height;

		// Setup the viewport
		_viewport.Height = (float)td.Height;
		_viewport.Width = (float)td.Width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		_name = file;
		_arraySize = 1;

		return true;
	}


	// 	 Maps the texture memory (staging and dynamic textures only).
	// 	 Unmap() must be called before the buffer can be used again.
	void* Texture::Map( D3D11_MAP mapping, int32 subresource )
	{
		HRESULT hr = _parent->GetContext()->Map( _tex[0], subresource, mapping, 0, &_mappedResource );
		M_ASSERT( SUCCEEDED( hr ), "Failed to map texture" );
		return _mappedResource.pData;
	}


	// Completes the Map operation on the texture
	void Texture::Unmap()
	{
		_parent->GetContext()->Unmap( _tex[0], 0 );
	}


	// Set the clear color
	void Texture::SetClearColor( float r, float g, float b, float a )
	{
		_clearColor[0] = r;
		_clearColor[1] = g;
		_clearColor[2] = b;
		_clearColor[3] = a;
	}


	// Clear the render targets
	void Texture::Clear( int32 index )
	{
		if ( index == -1 )
		{
			for ( uint32 i = 0; i < _arraySize; i++ )
				_parent->GetContext()->ClearRenderTargetView( _rtv[i], _clearColor );
		}
		else
			_parent->GetContext()->ClearRenderTargetView( _rtv[index], _clearColor );
	}

	// Clear the Depth target
	void Texture::ClearDSV()
	{
		_dsv->Clear();
	}



	// 	Generate the mip maps.  Does not apply to arrays or cubemaps
	void Texture::GenerateMips()
	{
		_parent->GetContext()->GenerateMips( _srv[0] );
	}



	// Free resources
	void Texture::Release()
	{
		// Unbind from the pipeline to prevent corruption
		_parent->ClearInputBindings(this);
		if(_outputBinding.boundToPipeline)
			_parent->ClearOutputBindings();

		for ( uint32 i = 0; i < _tex.Size(); i++ )
			SafeRelease( _tex[i] );
		for ( uint32 i = 0; i < _rtv.Size(); i++ )
			SafeRelease( _rtv[i] );
		for ( uint32 i = 0; i < _srv.Size(); i++ )
			SafeRelease( _srv[i] );
		for ( uint32 i = 0; i < _uav.Size(); i++ )
			SafeRelease( _uav[i] );
		_dsv = nullptr;
		_tex.Release();
		_rtv.Release();
		_srv.Release();
		_uav.Release();
	}

	// Create from a pre-existing d3d11 resource
	void Texture::StoreRenderTarget( ID3D11Texture2D* tex, ID3D11RenderTargetView* rtv, DepthStencil::pointer dsv )
	{
		// Release previous texture
		Release();

		// Store the resource
		_arraySize = 1;
		_tex.Allocate( 1 );
		_rtv.Allocate( 1 );
		_tex[0] = tex;
		_rtv[0] = rtv;
		_dsv = dsv;

		// Get the width and height
		D3D11_TEXTURE2D_DESC td;
		tex->GetDesc( &td );
		_width = td.Width;
		_height = td.Height;

		// Setup the viewport
		_viewport.Height = (float)td.Height;
		_viewport.Width = (float)td.Width;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;
	}

	// Load a sprite sheet into a texture array
	bool Texture::CreateSpriteSheet( const char* file, int32 frameWidth, int32 frameHeight, int32 width, int32 height )
	{
		// Release previous texture
		Release();

		// Load it
		String ext = String( file ).GetFileExtension();
		WCHAR fileName[256];
		Win32::AnsiToUnicode( file, fileName, 256 );
		DirectX::ScratchImage loadImage;
		if ( ext == "tga" )
		{
			HRESULT hr = DirectX::LoadFromTGAFile( fileName, nullptr, loadImage );
			M_ASSERT( SUCCEEDED( hr ), "Failed LoadFromTGAFile" );
		}
		else if ( ext == "dds" )
		{
			HRESULT hr = DirectX::LoadFromDDSFile( fileName, DirectX::DDS_FLAGS_NONE, nullptr, loadImage );
			M_ASSERT( SUCCEEDED( hr ), "Failed LoadFromDDSFile" );
		}
		else
		{
			HRESULT hr = DirectX::LoadFromWICFile( fileName, DirectX::WIC_FLAGS_NONE, nullptr, loadImage );
			M_ASSERT( SUCCEEDED( hr ), "Failed LoadFromWICFile" );
		}
		const DirectX::Image* img = loadImage.GetImage( 0, 0, 0 );

		// Validate the width and height
		if( (img->width < uint32(frameWidth * width)) || (img->height < uint32(frameHeight * height)) )
			return false;

		// Setup the desc
		int32 numFrames = width * height;
		D3D11_TEXTURE2D_DESC td;
		ZeroMemory( &td, sizeof(D3D11_TEXTURE2D_DESC) );
		td.ArraySize = numFrames;
		td.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		td.Format = img->format;
		td.MipLevels = 1;
		td.Width = frameWidth;
		td.Height = frameHeight;
		td.SampleDesc.Count = 1;
		td.Usage = D3D11_USAGE_IMMUTABLE;
		td.CPUAccessFlags = 0;

		// Buffers to hold each frame
		int32 texelSize = img->rowPitch / img->width;
		Array<Array<byte>> frames( numFrames );

		// Setup initial data
		Array<D3D11_SUBRESOURCE_DATA> initialData( numFrames);
		int32 i = 0;
		for ( int32 y = 0; y < height; ++y )
		{
			for ( int32 x = 0; x < width; ++x, ++i)
			{
				// Copy over the frame
				int32 startTexel = y * frameHeight * img->width * texelSize + x * texelSize * frameWidth;
				int32 rowPitch = frameWidth*texelSize;
				frames[i].Allocate( texelSize * frameWidth * frameHeight );
 				for ( int32 row = 0; row < frameHeight; ++row )
					memcpy( frames[i] + row*rowPitch, img->pixels + startTexel + row*img->width*texelSize, rowPitch );

				// Setup initial data
				initialData[i].pSysMem = frames[i];
				initialData[i].SysMemPitch = texelSize * frameWidth;
				initialData[i].SysMemSlicePitch = 0;
			}
		}

		// Create the texture array
		_arraySize = 1;
		_resourceArraySize = numFrames;
		_tex.Allocate( 1 );
		_srv.Allocate( 1 );
		HRESULT hr = _parent->GetDevice()->CreateTexture2D( &td, initialData, &_tex[0] );
		if ( FAILED( hr ) )
			return false;

		// Create the srv
		D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory( &SRVDesc, sizeof(SRVDesc) );
		SRVDesc.Format = td.Format;
		SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2DARRAY;
		SRVDesc.Texture2DArray.FirstArraySlice = 0;
		SRVDesc.Texture2DArray.MostDetailedMip = 0;
		SRVDesc.Texture2DArray.MipLevels = 1;
		SRVDesc.Texture2DArray.ArraySize = numFrames;
		hr = _parent->GetDevice()->CreateShaderResourceView( _tex[0], &SRVDesc, &_srv[0] );
		if ( FAILED( hr ) )
			return false;


		return true;
	}

}

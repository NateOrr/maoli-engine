//--------------------------------------------------------------------------------------
// File: MeshInstance.h
//
// Simple mesh instance
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class MeshInstance
	{
	public:

		// Get the mesh
		inline const MeshBinding::pointer& GetMesh() const { return _mesh; }

		// Set the mesh
		inline void SetMesh( const MeshBinding::pointer& val ) { _mesh = val; }

		// Get the position
		inline const Maoli::Vector3& GetPosition() const { return _position; }
		
		// Set the position
		inline void SetPosition( const Maoli::Vector3& val ) { _position = val; }
		inline void SetPosition( float x, float y, float z ) { _position = Vector3(x, y, z); }

		// Get the scale
		inline const Maoli::Vector3& GetScale() const { return _scale; }
		
		// Set the scale
		inline void SetScale( const Maoli::Vector3& val ) { _scale = val; }
		inline void SetScale( float x, float y, float z ) { _scale = Vector3( x, y, z ); }

		// Get the rotation
		inline const Maoli::Vector3& GetRotation() const { return _rotation; }
		
		// Set the rotation
		inline void SetRotation( const Maoli::Vector3& val ) { _rotation = val; }
		inline void SetRotation( float x, float y, float z ) { _rotation = Vector3( x, y, z ); }

		// Get the world matrix for this instance
		const Matrix& GetWorldMatrix() const { return _worldMatrix; }

		// Get the bounding volume of this instance
		const BoundingVolume& GetBoundingVolume() const { return _bounds; }

		// Update the world matrix and bounding volume
		void Update();

	private:

		MeshBinding::pointer	_mesh;
		Vector3					_position;
		Vector3					_scale;
		Vector3					_rotation;
		Matrix					_worldMatrix;
		BoundingVolume			_bounds;
	};
}
//--------------------------------------------------------------------------------------
// File: InstanceSet.h
//
// A set of instances of a single mesh
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class RenderNode;

	// Set of instances using a common mesh
	class InstanceSet
	{
		friend class Renderer;
		friend class ShadowManager;
	public:

		// Ctor
		InstanceSet( RenderNode* base );

		// Get the current instance count
		inline uint32 GetInstanceCount() const { return _instances.Size(); }

		// Add to the current instance count
		inline void AddInstance( const Matrix& worldMatrix ) { _instances.Add( worldMatrix.GetTranspose() ); }

		// Clear active instances
		inline void Clear() { _instances.Clear(); }

		// Get the base render node
		inline RenderNode* GetRenderNode() { return _node; }

		// Get the instance data
		void* GetInstanceData() { return _instances.GetRawData(); }

		// Increase reference count
		inline void AddRef() { ++_refCount; }

		// Decrease ref count
		inline bool RemoveRef() { --_refCount; return (_refCount > 0); }

		// Get the ref count
		inline uint32 GetRefCount() const { return _refCount; }

	private:

		RenderNode*			_node;				// Render node the set is based off
		Array<Matrix>		_instances;			// Local instance data
		int32					_refCount;			// Reference tracking
	};
}
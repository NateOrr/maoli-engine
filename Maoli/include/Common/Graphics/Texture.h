//--------------------------------------------------------------------------------------
// File: Texture.h
//
// Texture
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "DepthStencil.h"

namespace Maoli
{
	// Forward decl
	class Renderer;

	// Simplifies the usage of textures in d3d11
	class Texture : public Resource<Renderer>, public PipeplineResource
	{
		friend class ResourceManager<Renderer, Texture>;
		friend class Pointer<Texture>;
		friend class Renderer;
		friend class Renderer;
		friend class RenderState;
	public:

		// Smart pointer
		typedef Pointer<Texture> pointer;

		// Get the graphics engine
		inline Renderer* GetGraphics(){ return _parent; }
		
		// 	Attach a depth stencil to this texture so it can be bound as a RTV.
		void AttachDepthStencil(DepthStencil::pointer depth){ _dsv = depth; }

		// 	Generate the mip maps.  Does not apply to arrays or cubemaps
		void GenerateMips();


		// 	Set color used to clear render targets
		void SetClearColor(float r, float g, float b, float a);

		// 	Clear the render targets.
		void Clear(int32 index = -1);

		// 	Clears the DSV
		void ClearDSV();

				
		// 	Checks if this texture is an array
		inline bool IsArray(){ return (_arraySize>1); }

		// 	Checks if this texture is a cubemap.
		inline bool IsCube(){ return _isCube; }

		// 	Checks if the texture has been created.
		inline bool Exists(){ return _tex!=nullptr; }

		// 	Bracket [] style access for the texture.
		inline ID3D11Texture2D* operator[](int32 i){ return _tex[i]; }

		// Accessors
		inline ID3D11Texture2D**			GetTex()					  { return _tex;				 }
		virtual ID3D11ShaderResourceView**	GetSRV()					  { return _srv;				 }
		virtual ID3D11RenderTargetView**	GetRTV()					  { return _rtv;				 }
		virtual ID3D11UnorderedAccessView**	GetUAV()					  { return _uav;				 }
		virtual DepthStencil::pointer		GetDSV()				const { return _dsv;				 }
		inline const D3D11_VIEWPORT&		GetViewport()			const { return _viewport;			 }
		virtual uint32						GetArraySize()			const { return _arraySize;			 }
		inline int32							GetWidth()				const { return _width;				 }
		inline int32							GetHeight()				const { return _height;				 }
		inline uint32							GetResourceArraySize()	const { return _resourceArraySize;   }


		// 	 Maps the texture memory (staging and dynamic textures only).
		// 	 Unmap() must be called before the buffer can be used again.
		void* Map(D3D11_MAP mapping, int32 subresource = 0);

		// Completes the Map operation on the texture
		void Unmap();

	private:

		// Ctor
		Texture(Renderer* parent);

		// Dtor
		~Texture();

		// Create from a pre-existing d3d11 resource
		void StoreRenderTarget(ID3D11Texture2D* tex, ID3D11RenderTargetView* rtv, DepthStencil::pointer dsv);

		// 	Create from an existing 2d texture
		HRESULT CreateFromTexture(ID3D11Texture2D* pTex, bool isRTV, bool isSRV);

		// Create a 2d texture
		HRESULT Create(uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV);

		// Create a texture from some initial data
		HRESULT CreateFromData(uint32 width, uint32 height, DXGI_FORMAT format, D3D11_USAGE usage, bool srv, void* data);

		// Create a 2d texture with more options.
		HRESULT CreateEx(uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, 
			DXGI_FORMAT rtvFormat, DXGI_FORMAT srvFormat, DXGI_FORMAT uavFormat);

		// 	Create a an array of 2d textures
		HRESULT CreateArray(uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT* formats, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV);

		// Create a texture array, where the actual resource is bound as an array
		// Currently only SRV arrays are supported
		HRESULT CreateArrayResource( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels, bool rtv );

		// Create a cube map array
		HRESULT CreateCubeArray( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels );


		// Insert a texture into an array resource
		HRESULT InsertTexture(const char* file, uint32 index);

		// 	Create a cubemap texture.
		HRESULT CreateCube(uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, bool asSingleCube);

		// 	Loads a texture from file.
		bool Load(const char* file, bool cube = false);

		// Loads a texture from file
		bool LoadFromFile(const char* file, bool cube);

		// Load a sprite sheet into a texture array
		bool CreateSpriteSheet( const char* file, int32 frameWidth, int32 frameHeight, int32 width, int32 height );

		// Load a TGA from file
		ID3D11Texture2D* LoadBaseTexture(const char* file);

		// 	Free resources used by the texture.  The destructor will call this.
		void Release();
		
		// Textures and views
		Array<ID3D11Texture2D*>				_tex;
		Array<ID3D11RenderTargetView*>		_rtv;
		Array<ID3D11ShaderResourceView*>	_srv;	
		Array<ID3D11ShaderResourceView*>	_nullSRV;	
		Array<ID3D11UnorderedAccessView*>	_uav;

		// Depth stencil
		DepthStencil::pointer _dsv;

		// Render target viewport
		D3D11_VIEWPORT _viewport;

		// Misc properties
		bool	_isCube;
		uint32	_arraySize;
		uint32	_resourceArraySize;
		float	_clearColor[4];
		int32		_width;
		int32		_height;

		// Used for mapping
		D3D11_MAPPED_SUBRESOURCE    _mappedResource;
		
	};

}

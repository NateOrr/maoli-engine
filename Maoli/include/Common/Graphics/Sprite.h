//--------------------------------------------------------------------------------------
// File: Sprite.h
//
// Animated sprite base class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class Sprite : public ISerializable
	{
	public:

		// Ctor
		Sprite();

		// Enable / disable animations
		inline void EnableAnimation( bool value ) { _animated = value; }

		// Check if the emitter uses animations
		inline bool IsAnimated() const { return _animated; }

		// Set the number of frames in the animation
		void SetNumAnimationFrames( int32 numFrames );

		// Get the number of frames in the animation
		inline int32 GetNumAnimationFrames() const { return _numFrames; }

		// Set the length of the animation
		void SetAnimationLength( float length );

		// Get the length of the animation
		inline float GetAnimationLength() const { return _animationLength; }

		// Set the frame size of the animation
		void SetAnimationFrameWidth( int32 value );
		void SetAnimationFrameHeight( int32 value );

		// Get the frame size of the animation
		inline int32 GetAnimationFrameWidth() const { return _frameWidth; }
		inline int32 GetAnimationFrameHeight() const { return _frameHeight; }

		// Set the sprite sheet dimentions
		void SetSpriteSheetWidth( int32 value );
		void SetSpriteSheetHeight( int32 value );

		// Get the sprite sheet dimentions
		inline int32 GetSpriteSheetWidth() const { return _spriteWidth; }
		inline int32 GetSpriteSheetHeight() const { return _spriteHeight; }

		// Update the sprite
		void UpdateAnimation( float dt );

		// Get the current frame of animation
		inline int32 GetFrame() const { return _frame; }

		// Get the length of each animation frame
		inline float GetAnimationFrameLength() const { return _frameLength; }

		// Write to binary file
		virtual void Serialize( std::ofstream& file ) const;

		// Read from binary file
		virtual void Deserialize( std::ifstream& file );

		// Texture
		inline Texture::pointer GetTexture() const { return _texture; }
		inline void SetTexture( Texture::pointer val ) { _texture = val; }

	protected:

		bool			 _animated;			// True if the system uses a sprite sheet for animations
		float			 _animationLength;	// Length of the animation
		float			 _frameLength;		// Length of each frame
		int32				 _numFrames;		// Number of frames in the animation
		int32				 _frameWidth;		// Size of an animation frame
		int32				 _frameHeight;		// Size of an animation frame
		int32				 _spriteWidth;		// Size of the sprite sheet
		int32				 _spriteHeight;		// Size of the sprite sheet
		int32				 _frame;			// Current frame
		float			 _frameTimer;		// Timer for the frame
		Texture::pointer _texture;			// Texture to use
		
	};
}
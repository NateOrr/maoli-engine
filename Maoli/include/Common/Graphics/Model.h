//--------------------------------------------------------------------------------------
// File: Model.h
//
// An instance of a 3D mesh
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/MeshBinding.h"
#include "Graphics/Texture.h"
#include "Graphics/Buffer.h"
#include "Game/Component.h"

//todo: merge world matrix into single matrix?

namespace Maoli
{
	// Forward decl
	class Engine;
	class Renderer;
	class EnvironmentProbe;
	class AnimationController;

	class Model : public Component, public QuadtreeElement
	{
		friend class Renderer;
		friend class MeshBinding;
	public:

		COMPONENT_DECL( Model );

		// Ctor
		Model( Engine* engine );

		// Dtor
		~Model();

		// Create from file
		static Model* CreateFromFile( const char* name, Engine* engine );


		//////////////////////////////////////////////
		// Component interface

		// Per-frame logic
		virtual void Update( float dt );

		// Binary serialize
		virtual void Export( std::ofstream& fout );

		// Binary file import
		virtual void Import( std::ifstream& fin );

		// Deep copy clone is required for all components
		virtual Component* Clone( Component* dest = nullptr ) const;

		// Register for any events that need to be handled
		virtual void RegisterEventCallbacks();

		// Register component dependencies
		virtual void RegisterDependency( Component* dependency );


		//////////////////////////////////////////////
		// Event callbacks

		// Update position
		void OnTransformChanged( Component* sender, void* userData );

		// Animation updated the bone palette
		void OnAnimationUpdated( Component* sender, void* userData );

		// Animation skin loaded
		void OnAnimationLoaded( Component* sender, void* userData );


		// Frees mem and makes an empty mesh	
		void Release();

		// Create from geometry
		bool Create( Geometry* geometry );


		// Get the parent mesh
		inline MeshBinding::pointer GetMeshBinding() { return _binding; }


		// Get the position
		inline const Vector3& GetPos() const { return _position; }

		// Get the rotation
		inline const Vector3& GetRot() const { return _rotation; }

		// Get the scale
		inline const Vector3& GetScale() const { return _scale; }


		// Set the position
		void SetPosition( const Vector3& v );

		// Set the position
		void SetPosition( float x, float y, float z );

		// Add to the position
		void AddToPos( const Vector3& v );

		// Add to the position
		void AddToPos( float x, float y, float z );


		// Set the rotation
		void SetRotation( const Vector3& v );

		// Set the rotation
		void SetRotation( float x, float y, float z );

		// Add to the rotation
		void AddToRot( const Vector3& v );

		// Add to the rotation
		void AddToRot( float x, float y, float z );


		// Set the scale
		void SetScale( const Vector3& v );

		// Set a uniform scale
		void SetScale( float scale );

		// Set the scale
		void SetScale( float x, float y, float z );

		// Add to the scale
		void AddToScale( const Vector3& v );

		// Add to the scale
		void AddToScale( float x, float y, float z );

		// Gets the world matrix
		inline const Matrix& GetWorldMatrix() const { return _worldMatrix; }
		inline const Matrix& GetOldWorldMatrix() const { return _prevWorldMatrix; }

		// Manually update the world matrix
		void UpdateWorldMatrix();

		// Checks for an intersection with a an AABB
		bool BoxMeshIntersect( Vector3& min, Vector3& max );

		// Set a material
		void SetMaterial( MaterialBinding::pointer mat, int32 i );

		// Get the bounding volume
		inline BoundingVolume& GetBounds() { return _bounds; }
		virtual const BoundingVolume& GetBounds() const { return _bounds; }


		// Get the submesh array
		inline const Array<RenderNode>& GetSubMesh() const { return _renderNodes; }

		// Get a submesh
		inline RenderNode* GetSubMesh( int32 i ) { return &_renderNodes[i]; }

		// Get the number of submeshes
		inline uint32 GetNumSubMesh() const { return _renderNodes.Size(); }

		// Gets the number of faces
		inline uint32 GetNumFaces() const { return _geometry->GetNumFaces(); }

		// Remove a submesh
		void DeleteSubMesh( int32 index );

		// Access the geometry
		inline Geometry* GetGeometry() { return _geometry; }

		// Set the model dimensions
		void SetDimensions( float x, float y, float z );
		void SetDimensions( const Vector3& dim );

		// Get the model dimensions
		Vector3 GetDimensions();


		// Set this mesh up for animation
		void SetAnimationController( AnimationController* ac );

		// Get the animation controller
		inline AnimationController* GetAnimationController() { return _animation; }

		// Update the animation
		void UpdateAnimation( float dT = 0.016f );

		// True if it uses animation
		inline bool IsAnimated() const { return _animation != nullptr; }

		// Get the engine that owns it
		inline Engine* GetEngine() { return _engine; }

		// Test for a ray intersection
		bool RayIntersect( const Vector3& origin, const Vector3& direction, bool initialBoxTest );
		bool RayIntersect( const Vector3& origin, const Vector3& direction, bool initialBoxTest, float& dist );

		// Set the environment probe
		void SetEnvironmentProbe( EnvironmentProbe* probe );

		// Get the environment probe
		inline EnvironmentProbe* GetEnvironmentProbe() { return _probe; }

		// Get the object id
		inline uint32 GetID() const { return _id; }

		// Enable/disable automatic instancing
		inline void EnableInstancing(bool value){ _instancing = value; }

		// Check for instancing support
		inline bool Instanced() const { return _instancing; }

	protected:

		// Update bounding box
		void UpdateBounds();

		// Update the submeshes
		void UpdateSubMeshes();


		Engine*							_engine;				// Parent game engine
		Renderer*						_graphics;				// Parent renderer
		MeshBinding::pointer			_binding;				// Mesh binding for the renderer
		Geometry*						_geometry;				// Geometry the model is built from
		Array<RenderNode>				_renderNodes;			// The sub meshes
		EnvironmentProbe*				_probe;					// Cube map

		uint32					_id;					// Unique model ID
		Vector3					_position;				// The local position
		Vector3					_rotation;				// The local rotation angles
		Vector3					_scale;					// The local object scale
		Matrix					_translationMatrix;		// Local translation matrix
		Matrix					_rotationMatrix;		// Local rotation matrix
		Matrix					_scaleMatrix;			// Local scale matrix
		Matrix					_worldMatrix;			// The world matrix
		Matrix					_prevWorldMatrix;		// The world matrix from the previous frame
		BoundingVolume			_bounds;				// Bounding volume
		bool					_isDirty;				// True if the local transform has changed
		bool					_instancing;			// True for auto instancing


		Buffer<Matrix>::pointer		_bonePalette;		// Bone matrix palette for animation
		AnimationController*					_animation;			// Animation that controls this model
		Transform*					_transform;
	};

}

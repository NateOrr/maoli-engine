//--------------------------------------------------------------------------------------
// File: Model.cpp
//
// A static 3D mesh.
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Model.h"
#include "Graphics/MeshBinding.h"
#include "Engine\Engine.h"
#include "Graphics/Renderer.h"
#include "Game/Entity.h"
#include "Graphics/Probe.h"
#include <mutex>

namespace Maoli
{
	// Constructor
	Model::Model( Engine* engine ) : Component( engine ), _position( 0, 0, 0 ), _rotation( 0, 0, 0 ), _scale( 1, 1, 1 )
	{
		_engine = engine;
		_graphics = _engine->GetGraphics();
		_binding = nullptr;
		_animation = nullptr;
		_geometry = nullptr;
		_transform = nullptr;
		_probe = nullptr;
		_isDirty = true;
		_instancing = true;

		static uint32 idCounter = 0;
		_id = (++idCounter) % 255;
	}

	// Destructor
	Model::~Model()
	{
		Release();
	}
	// Manually update the world matrix
	void Model::UpdateWorldMatrix()
	{
		_worldMatrix = _scaleMatrix * _rotationMatrix * _translationMatrix;
		if ( _transform )
			_worldMatrix.Mult( _transform->GetOrientationMatrix() );
	}

	// Per-frame logic
	void Model::Update( float dt )
	{
		if ( _isDirty )
		{
			// Set the previous world matrix
			_prevWorldMatrix = _worldMatrix;

			// Compute the new world matrix matrix
			_worldMatrix = _scaleMatrix * _rotationMatrix * _translationMatrix;
			if ( _transform )
				_worldMatrix.Mult( _transform->GetOrientationMatrix() );

			// Compute new bounds
			UpdateBounds();

			// Let other components know about the updates
			//if ( _entity )
			//	_entity->BroadcastMessage( ComponentEvent::MeshChanged, this, nullptr );

			// Update the renderer tree
			_graphics->UpdateQuadtree( this );

			_isDirty = false;
		}
	}

	// Binary serialize
	void Model::Export( std::ofstream& fout )
	{
		// Mesh file name
		_geometry->GetFilePath().Serialize( fout );

		// Properties
		Maoli::Serialize( fout, &_position );
		Maoli::Serialize( fout, &_rotation );
		Maoli::Serialize( fout, &_scale );

		// Submesh data
		for ( uint32 i = 0; i < _renderNodes.Size(); ++i )
		{
			_renderNodes[i].name.Serialize( fout );
			String materialFileName = _renderNodes[i].GetMaterialBinding()->GetMaterial()->GetFilePath();
			String meshDir = _geometry->GetFilePath().GetDirectory();
			_renderNodes[i].GetMaterialBinding()->GetMaterial()->Save( meshDir + _geometry->GetName() + _renderNodes[i].name + ".mat" );
			materialFileName = _renderNodes[i].GetMaterialBinding()->GetMaterial()->GetFilePath();
			materialFileName.Serialize( fout );
		}
	}

	// Binary file import
	void Model::Import( std::ifstream& fin )
	{
		// Mesh file
		String name;
		name.Deserialize( fin );
		auto geometry = Geometry::CreateFromFile( name, _engine );
		Create( geometry );

		// Properties
		Vector3 v;
		Maoli::Deserialize( fin, &v );
		SetPosition( v );
		Maoli::Deserialize( fin, &v );
		SetRotation( v );
		Maoli::Deserialize( fin, &v );
		SetScale( v );

		// Submesh data
		String matName;
		for ( uint32 i = 0; i < _renderNodes.Size(); ++i )
		{
			_renderNodes[i].name.Deserialize( fin );
			matName.Deserialize( fin );
			if ( matName == String::none )
				_renderNodes[i].SetMaterialBinding( _graphics->CreateMaterialBinding( "Default" ) );
			else
				_renderNodes[i].SetMaterialBinding( _graphics->CreateMaterialBindingFromFile( matName ) );
		}

		// Initial setup
		UpdateBounds();
	}

	// Deep copy clone is required for all components
	Component* Model::Clone( Component* dest ) const
	{
		Model* clone = dest ? (Model*)dest : new Model( _engine );

		// Properties
		clone->Create( Geometry::CreateFromFile( _geometry->GetFilePath(), _engine ) );
		clone->SetPosition( _position );
		clone->SetRotation( _rotation );
		clone->SetScale( _scale );

		// Submesh data
		for ( uint32 i = 0; i < _renderNodes.Size(); ++i )
		{
			clone->_renderNodes[i].name = _renderNodes[i].name;
			if ( _renderNodes[i].GetMaterialBinding()->GetMaterial()->FileBased() )
				clone->_renderNodes[i].SetMaterialBinding( _graphics->CreateMaterialBindingFromFile( _renderNodes[i].GetMaterialBinding()->GetMaterial()->GetName() ) );
			else
				clone->_renderNodes[i].SetMaterialBinding( _graphics->CreateMaterialBinding( _renderNodes[i].GetMaterialBinding()->GetMaterial()->GetName() ) );
		}

		return clone;
	}

	// Register for any events that need to be handled
	void Model::RegisterEventCallbacks()
	{
		REGISTER_EVENT_CALLBACK( ComponentEvent::TransformChanged, &Model::OnTransformChanged );
		REGISTER_EVENT_CALLBACK( ComponentEvent::AnimationUpdated, &Model::OnAnimationUpdated );
		REGISTER_EVENT_CALLBACK( ComponentEvent::AnimationLoaded, &Model::OnAnimationLoaded );
	}

	// Register component dependencies
	void Model::RegisterDependency( Component* dependency )
	{
		if ( dependency->GetOwner() == GetOwner() )
		{
			if ( dependency->GetTypeID() == typeid(AnimationController) )
			{
				SetAnimationController( (AnimationController*)dependency );
				return;
			}

			if ( dependency->GetTypeID() == typeid(Transform) )
			{
				_transform = (Transform*)dependency;
				UpdateBounds();
			}
		}
	}

	// Update position
	void Model::OnTransformChanged( Component* sender, void* userData )
	{
		_isDirty = true;
	}

	// Animation updated the bone palette
	void Model::OnAnimationUpdated( Component* sender, void* userData )
	{
		AnimationController* anim = (AnimationController*)sender;
		if ( anim == _animation )
		{
			Matrix* pData = _bonePalette->Map( D3D11_MAP_WRITE_DISCARD );
			_animation->FillBonePalette( reinterpret_cast<float*>(pData) );
			_bonePalette->Unmap();
		}
	}

	// Animation skin loaded
	void Model::OnAnimationLoaded( Component* sender, void* userData )
	{
		AnimationController* anim = (AnimationController*)sender;
		if ( anim == _animation )
		{
			SetAnimationController( anim );
		}
	}

	// Set this mesh up for animation
	void Model::SetAnimationController( AnimationController* ac )
	{
		// Setup the bone palette buffer
		if ( ac && ac->GetNumTracks() > 0 )
		{
			_bonePalette = _graphics->CreateBuffer<Matrix>( ac->GetNumBones(), D3D11_USAGE_DYNAMIC, false, true, nullptr );
			if ( !_bonePalette )
			{
				M_ERROR( "Failed to create bone palette" );
				return;
			}
			for ( uint32 i = 0; i < _renderNodes.Size(); i++ )
			{
				_renderNodes[i].bonePalette = _bonePalette;
				_renderNodes[i].vertexBuffers[VERTEX_STREAM_BONE] = _binding->_boneWeightVertexBuffer->GetBuffer();
				_graphics->SetRenderCallback( _renderNodes[i] );
			}

			_bonePalette->SetDebugName( _binding->GetName() );
		}
		else
		{
			_bonePalette = nullptr;
			for ( uint32 i = 0; i < _renderNodes.Size(); i++ )
			{
				_renderNodes[i].bonePalette = nullptr;
				_renderNodes[i].vertexBuffers[VERTEX_STREAM_BONE] = nullptr;
				_graphics->SetRenderCallback( _renderNodes[i] );
			}
		}
		_animation = ac;
	}


	// Releases the object
	void Model::Release()
	{
		if ( _geometry )
		{
			// Release the submeshes
			_renderNodes.Release();

			// Deref the geometry pointer
			Geometry::Delete( _geometry );

			// Binding
			_binding->_models.Remove( this );
			_binding = nullptr;

			// Animation
			_bonePalette = nullptr;
		}
	}


	// Creates from an existing mesh
	bool Model::Create( Geometry* geometry )
	{
		// Reset the object
		if ( _geometry )
			Release();
		_geometry = geometry;

		// Grab the mesh binding
		_binding = _graphics->GetMeshBinding( geometry );
		_binding->_models.Add( this );

		// Position the model
		_position = Vector3( 0, 0, 0 );

		// Setup the initial world matrix
		_translationMatrix = Matrix::CreateTranslation( _position.x, _position.y, _position.z );
		_rotationMatrix = Matrix::CreateRotationYawPitchRoll( _rotation.y, _rotation.x, _rotation.z );
		_scaleMatrix = Matrix::CreateScaling( _scale.x, _scale.y, _scale.z );

		_worldMatrix = _scaleMatrix * _rotationMatrix * _translationMatrix;
		_renderNodes.Allocate( _geometry->GetNumChunks() );
		for ( uint32 i = 0; i < _geometry->GetNumChunks(); ++i )
		{
			// Set the submesh pointers
			_renderNodes[i] = _binding->GetRenderNodes()[i];
			_renderNodes[i].worldMatrix = &_worldMatrix;
			_renderNodes[i].worldMatrixPrev = &_prevWorldMatrix;
			_renderNodes[i].SetMaterialBinding( _binding->GetRenderNodes()[i].GetMaterialBinding() );
			_renderNodes[i].SetModel( this );
		}

		UpdateBounds();

		return true;
	}


	// Update the submeshes
	void Model::UpdateSubMeshes()
	{
		for ( uint32 i = 0; i < _renderNodes.Size(); i++ )
		{
			_renderNodes[i].indexBuffer = _binding->GetRenderNodes()[i].indexBuffer;
			_renderNodes[i].vertexBuffers[VERTEX_STREAM_POS] = _binding->GetRenderNodes()[i].vertexBuffers[VERTEX_STREAM_POS];
			_renderNodes[i].vertexBuffers[VERTEX_STREAM_TEXCOORD] = _binding->GetRenderNodes()[i].vertexBuffers[VERTEX_STREAM_TEXCOORD];
			_renderNodes[i].vertexBuffers[VERTEX_STREAM_NORMAL] = _binding->GetRenderNodes()[i].vertexBuffers[VERTEX_STREAM_NORMAL];
			_renderNodes[i].vertexBuffers[VERTEX_STREAM_BONE] = _binding->GetRenderNodes()[i].vertexBuffers[VERTEX_STREAM_BONE];
		}
	}


	// Checks for an intersection with a an AABB
	bool Model::BoxMeshIntersect( Vector3& min, Vector3& max )
	{
		// There is an intersection if any of the meshes
		// bounding box points are contained in the given box
		return Math::PointInBox( _position + Vector3( GetBounds().GetSize().x, GetBounds().GetSize().y, GetBounds().GetSize().z ), min, max ) ||
			Math::PointInBox( _position + Vector3( -GetBounds().GetSize().x, GetBounds().GetSize().y, -GetBounds().GetSize().z ), min, max ) ||
			Math::PointInBox( _position + Vector3( -GetBounds().GetSize().x, GetBounds().GetSize().y, GetBounds().GetSize().z ), min, max ) ||
			Math::PointInBox( _position + Vector3( GetBounds().GetSize().x, GetBounds().GetSize().y, -GetBounds().GetSize().z ), min, max ) ||
			Math::PointInBox( _position + Vector3( GetBounds().GetSize().x, -GetBounds().GetSize().y, GetBounds().GetSize().z ), min, max ) ||
			Math::PointInBox( _position + Vector3( -GetBounds().GetSize().x, -GetBounds().GetSize().y, -GetBounds().GetSize().z ), min, max ) ||
			Math::PointInBox( _position + Vector3( -GetBounds().GetSize().x, -GetBounds().GetSize().y, GetBounds().GetSize().z ), min, max ) ||
			Math::PointInBox( _position + Vector3( GetBounds().GetSize().x, -GetBounds().GetSize().y, -GetBounds().GetSize().z ), min, max );
	}


#pragma region Transforms


	// Set the position
	void Model::SetPosition( const Vector3& v )
	{
		_translationMatrix = Matrix::CreateTranslation( _position = v );
		_isDirty = true;
	}


	// Set the position
	void Model::SetPosition( float x, float y, float z )
	{
		_position.x = x; _position.y = y; _position.z = z;
		_translationMatrix = Matrix::CreateTranslation( _position );
		_isDirty = true;
	}


	// Add to the position
	void Model::AddToPos( const Vector3& v )
	{
		_position += v;
		_translationMatrix = Matrix::CreateTranslation( _position );
		_isDirty = true;
	}


	// Add to the position
	void Model::AddToPos( float x, float y, float z )
	{
		_position.x += x; _position.y += y; _position.z += z;
		_translationMatrix = Matrix::CreateTranslation( _position );
		_isDirty = true;
	}


	// Set the rotation
	void Model::SetRotation( const Vector3& v )
	{
		_rotation = v;
		_rotationMatrix = Matrix::CreateRotationYawPitchRoll( _rotation.y, _rotation.x, _rotation.z );
		_isDirty = true;
	}


	// Set the rotation
	void Model::SetRotation( float x, float y, float z )
	{
		_rotation.x = x; _rotation.y = y; _rotation.z = z;
		_rotationMatrix = Matrix::CreateRotationYawPitchRoll( _rotation.y, _rotation.x, _rotation.z );
		_isDirty = true;
	}


	// Add to the rotation
	void Model::AddToRot( const Vector3& v )
	{
		_rotation += v;
		_rotationMatrix = Matrix::CreateRotationYawPitchRoll( _rotation.y, _rotation.x, _rotation.z );
		_isDirty = true;
	}


	// Add to the rotation
	void Model::AddToRot( float x, float y, float z )
	{
		_rotation.x += x; _rotation.y += y; _rotation.z += z;
		_rotationMatrix = Matrix::CreateRotationYawPitchRoll( _rotation.y, _rotation.x, _rotation.z );
		_isDirty = true;
	}


	// Set the scale
	void Model::SetScale( const Vector3& v )
	{
		_scale = v;
		_scaleMatrix = Matrix::CreateScaling( _scale.x, _scale.y, _scale.z );
		_isDirty = true;
	}


	// Set the scale
	void Model::SetScale( float x, float y, float z )
	{
		_scale.x = x; _scale.y = y; _scale.z = z;
		_scaleMatrix = Matrix::CreateScaling( _scale.x, _scale.y, _scale.z );
		_isDirty = true;
	}


	// Set a uniform scale
	void Model::SetScale( float scale )
	{
		_scale.x = _scale.y = _scale.z = scale;
		_scaleMatrix = Matrix::CreateScaling( _scale.x, _scale.y, _scale.z );
		_isDirty = true;
	}


	// Add to the scale
	void Model::AddToScale( const Vector3& v )
	{
		_scale += v;
		_scaleMatrix = Matrix::CreateScaling( _scale.x, _scale.y, _scale.z );
		_isDirty = true;
	}


	// Add to the scale
	void Model::AddToScale( float x, float y, float z )
	{
		_scale.x += x; _scale.y += y; _scale.z += z;
		_scaleMatrix = Matrix::CreateScaling( _scale.x, _scale.y, _scale.z );
		_isDirty = true;
	}


#pragma endregion


	// Remove a submesh
	void Model::DeleteSubMesh( int32 index )
	{
		_renderNodes.RemoveAt( index );
		_geometry->DeleteGeometryChunk( index );
		UpdateSubMeshes();
	}

	// Create from file
	Model* Model::CreateFromFile( const char* name, Engine* engine )
	{
		Model* model = new Model( engine );
		Geometry* geometry = Geometry::CreateFromFile( name, engine );
		if ( !geometry )
		{
			engine->GetPlatform()->MessageBox( String( "Failed to load model " ) + name );
			delete model;
			return nullptr;
		}
		model->Create( geometry );
		return model;
	}

	// Test for a ray intersection
	bool Model::RayIntersect( const Vector3& origin, const Vector3& direction, bool initialBoxTest )
	{
		if ( !_binding )
			return 0;

		// First perform a bounding box test to see if its worth checking per polygon
		float dist;
		if ( initialBoxTest && !Math::RayBoxIntersect( origin, direction, _bounds.GetWorldPosition(), _bounds.GetSize(), dist ) )
			return false;

		// Perform a per-poly test
		Vector3 a, b, c;
		for ( uint32 i = 0; i < _renderNodes.Size(); i++ )
		{
			for ( int32 j = _renderNodes[i].startIndex; j < _renderNodes[i].startIndex + _renderNodes[i].indexCount; j += 3 )
			{
				a = _worldMatrix.Transform( _geometry->GetVerts()[_geometry->GetIndices()[j]] );
				b = _worldMatrix.Transform( _geometry->GetVerts()[_geometry->GetIndices()[j + 1]] );
				c = _worldMatrix.Transform( _geometry->GetVerts()[_geometry->GetIndices()[j + 2]] );
				if ( Math::RayTriangleIntersect( origin, direction, a, b, c ) )
					return true;
			}
		}

		return false;
	}

	// Test for a ray intersection
	bool Model::RayIntersect( const Vector3& origin, const Vector3& direction, bool initialBoxTest, float& dist )
	{
		if ( !_binding )
			return 0;

		// First perform a bounding box test to see if its worth checking per polygon
		float boxDist;
		if ( initialBoxTest && !Math::RayBoxIntersect( origin, direction, _bounds.GetWorldPosition(), _bounds.GetSize(), boxDist ) )
			return false;

		// Perform a per-poly test
		Vector3 a, b, c;
		float minDist = std::numeric_limits<float>::max();
		float t;
		bool hit = false;
		for ( uint32 i = 0; i < _renderNodes.Size(); i++ )
		{
			for ( int32 j = _renderNodes[i].startIndex; j < _renderNodes[i].startIndex + _renderNodes[i].indexCount; j += 3 )
			{
				a = _worldMatrix.Transform( _geometry->GetVerts()[_geometry->GetIndices()[j]] );
				b = _worldMatrix.Transform( _geometry->GetVerts()[_geometry->GetIndices()[j + 1]] );
				c = _worldMatrix.Transform( _geometry->GetVerts()[_geometry->GetIndices()[j + 2]] );
				if ( Math::RayTriangleIntersect( origin, direction, a, b, c, t ) && t < minDist )
				{
					minDist = t;
					hit = true;
				}
			}
		}

		if ( hit )
		{
			dist = minDist;
			return true;
		}

		return false;
	}

	// Update bounding box
	void Model::UpdateBounds()
	{
		// todo: this is overkill when just position changes!
		Matrix mat = _scaleMatrix * _rotationMatrix;
		if ( _transform )
			mat.Mult( _transform->GetOrientationMatrix().Get3x3() );
		Vector3 center = mat.Transform( _geometry->GetCenter() );
		_bounds.Set( _position + center, mat.TransformNormal( _geometry->GetBounds() ).Absolute() );
		if ( _transform )
			_bounds.SetWorldPosition( _transform->GetPosition() );
		for ( uint32 i = 0; i < _renderNodes.Size(); ++i )
		{
			_renderNodes[i].bounds.Set( mat.Transform( _geometry->GetChunk( i )->localPosition ) + _position, mat.TransformNormal( _geometry->GetChunk( i )->bounds ).Absolute() );
			if ( _transform )
				_renderNodes[i].bounds.SetWorldPosition( _transform->GetPosition() );
		}
	}

	// Set the environment probe
	void Model::SetEnvironmentProbe( EnvironmentProbe* probe )
	{
		_probe = probe;
		for ( uint32 i = 0; i < _renderNodes.Size(); ++i )
		{
			_renderNodes[i].cubeMap = _probe->GetTexture();
			_graphics->SetRenderCallback( _renderNodes[i] );
		}
	}

	// Set the model dimensions
	void Model::SetDimensions( float x, float y, float z )
	{
		// Compute the scale factors
		float sx = x / _geometry->GetBounds().x;
		float sy = y / _geometry->GetBounds().y;
		float sz = z / _geometry->GetBounds().z;
		SetScale( sx, sy, sz );
	}

	// Set the model dimensions
	void Model::SetDimensions( const Vector3& dim )
	{
		SetDimensions( dim.x, dim.y, dim.z );
	}

	// Get the model dimensions
	Maoli::Vector3 Model::GetDimensions()
	{
		float x = _scale.x * _geometry->GetBounds().x;
		float y = _scale.y * _geometry->GetBounds().y;
		float z = _scale.z * _geometry->GetBounds().z;
		return Vector3( x, y, z );
	}

	// Set a material
	void Model::SetMaterial( MaterialBinding::pointer mat, int32 i )
	{
		_renderNodes[i].SetMaterialBinding( mat );
	}

}

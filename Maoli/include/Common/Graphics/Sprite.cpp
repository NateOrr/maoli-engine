//--------------------------------------------------------------------------------------
// File: Sprite.cpp
//
// Animated sprite base class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Sprite.h"

namespace Maoli
{
	// Ctor
	Sprite::Sprite()
	{
		_frame = 0;
		_frameTimer = 0;
		_animated = false;
		_animationLength = 0;
		_frameLength = 0;
		_numFrames = 1;
		_frameWidth = 32;
		_frameHeight = 32;
		_spriteWidth = _spriteHeight = 1;
	}

	// Set the number of frames in the animation
	void Sprite::SetNumAnimationFrames( int32 numFrames )
	{
		_numFrames = numFrames;
		_frameLength = _animationLength / float( _numFrames );
	}

	// Set the length of the animation
	void Sprite::SetAnimationLength( float length )
	{
		_animationLength = length;
		_frameLength = _animationLength / float( _numFrames );
	}

	// Set the frame size of the animation
	void Sprite::SetAnimationFrameWidth( int32 value )
	{
		if ( _frameWidth != value )
		{
			_frameWidth = value;
		}
	}

	// Set the frame size of the animation
	void Sprite::SetAnimationFrameHeight( int32 value )
	{
		if ( _frameHeight != value )
		{
			_frameHeight = value;
		}
	}

	// Set the sprite sheet dimentions
	void Sprite::SetSpriteSheetWidth( int32 value )
	{
		if ( _spriteWidth != value )
		{
			_spriteWidth = value;
			SetNumAnimationFrames( GetSpriteSheetWidth() * GetSpriteSheetHeight() );
		}
	}

	// Set the sprite sheet dimentions
	void Sprite::SetSpriteSheetHeight( int32 value )
	{
		if ( _spriteHeight != value )
		{
			_spriteHeight = value;
			SetNumAnimationFrames( GetSpriteSheetWidth() * GetSpriteSheetHeight() );
		}
	}

	// Animation update
	void Sprite::UpdateAnimation( float dt )
	{
		if ( _animated )
		{
			_frameTimer += dt;
			if ( _frameTimer > _frameLength )
			{
				++_frame;
				_frameTimer = 0;
				if ( _frame == _numFrames )
					_frame = 0;
			}
		}
	}

	// Write to binary file
	void Sprite::Serialize( std::ofstream& fout ) const
	{
		Maoli::Serialize( fout, &_animated );
		Maoli::Serialize( fout, &_spriteWidth );
		Maoli::Serialize( fout, &_spriteHeight );
		Maoli::Serialize( fout, &_frameWidth );
		Maoli::Serialize( fout, &_frameHeight );
		Maoli::Serialize( fout, &_animationLength );
	}

	// Read from binary file
	void Sprite::Deserialize( std::ifstream& fin )
	{
		Maoli::Deserialize( fin, &_animated );
		Maoli::Deserialize( fin, &_spriteWidth );
		Maoli::Deserialize( fin, &_spriteHeight );
		Maoli::Deserialize( fin, &_frameWidth );
		Maoli::Deserialize( fin, &_frameHeight );
		Maoli::Deserialize( fin, &_animationLength );
		SetNumAnimationFrames( GetSpriteSheetWidth() * GetSpriteSheetHeight() );
	}
}
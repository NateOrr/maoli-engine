////--------------------------------------------------------------------------------------
// File: DebugVisualizer.cpp
//
// Debug visualization for the renderer
//
// Nate Orr
////--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/DebugVisualizer.h"
#include "Graphics/Renderer.h"
#include "Engine\Engine.h"
#include "Graphics/HDR.h"

namespace Maoli
{
	// Ctor
	DebugVisualizer::VisualizableObject::VisualizableObject()
	{
		mesh = nullptr;
		isWireframe = false;
		submesh = 0;
		submeshOnly = false;
		useTransform = false;
	}


	// Ctor
	DebugVisualizer::DebugVisualizer( Renderer* graphics )
	{
		_graphics = graphics;
		_demoModeEnabled = false;
		_wireframeMode = false;
		_showTerrainWidget = false;

		_selectedSumMeshIndex = 0;
		_lightRenderingScale = 1.0f;
		_showGrid = false;
		_needsVTPainting = false;
		_materialPreviewMesh = nullptr;
		_showSelectionBox = false;
		_showLights = false;

		_terrainSculptRadius = 5.0f;
		_terrainSculptHardness = 2.5f;
		_terrainSculptStrength = 1.0f;
		_terrainSculptDelta = 0.001f;
		_terrainSculptLayer = 0;
		_terrainSculptingMode = SCULPT_RAISE;
	}

	// Setup the debug renderer
	HRESULT DebugVisualizer::Init()
	{
		// Setup the light outline lines
		float edge = 0.35f;
		Vertex lightOutlineVerts[16];
		lightOutlineVerts[0].pos = Vector3( -1.0f, 1.0f - edge, 0.0f );
		lightOutlineVerts[1].pos = Vector3( -1.0f, 1.0f, 0.0f );
		lightOutlineVerts[2].pos = Vector3( -1.0f, 1.0f, 0.0f );
		lightOutlineVerts[3].pos = Vector3( -1.0f + edge, 1.0f, 0.0f );

		lightOutlineVerts[4].pos = Vector3( 1.0f - edge, 1.0f, 0.0f );
		lightOutlineVerts[5].pos = Vector3( 1.0f, 1.0f, 0.0f );
		lightOutlineVerts[6].pos = Vector3( 1.0f, 1.0f, 0.0f );
		lightOutlineVerts[7].pos = Vector3( 1.0f, 1.0f - edge, 0.0f );

		lightOutlineVerts[8].pos = Vector3( 1.0f, -1.0f + edge, 0.0f );
		lightOutlineVerts[9].pos = Vector3( 1.0f, -1.0f, 0.0f );
		lightOutlineVerts[10].pos = Vector3( 1.0f, -1.0f, 0.0f );
		lightOutlineVerts[11].pos = Vector3( 1.0f - edge, -1.0f, 0.0f );

		lightOutlineVerts[12].pos = Vector3( -1.0f + edge, -1.0f, 0.0f );
		lightOutlineVerts[13].pos = Vector3( -1.0f, -1.0f, 0.0f );
		lightOutlineVerts[14].pos = Vector3( -1.0f, -1.0f, 0.0f );
		lightOutlineVerts[15].pos = Vector3( -1.0f, -1.0f + edge, 0.0f );

		// Create the vertex buffer for the line outline
		_lightBorderVertexBuffer = _graphics->CreateVertexBuffer<Vertex>( 16, D3D11_USAGE_IMMUTABLE, lightOutlineVerts );

		// Create the single line buffer
		Vector3 lineVerts[2];
		lineVerts[0] = Vector3( 0, 0, 0 );
		lineVerts[1] = Vector3( 0, 0, 0.7f );
		_lineVetexBuffer = _graphics->CreateVertexBuffer<Vector3>( 2, D3D11_USAGE_IMMUTABLE, lineVerts );

		// Selection vertex buffer
		_quadVertexBuffer = _graphics->CreateVertexBuffer<Vertex>( 6, D3D11_USAGE_DYNAMIC );

		// Create the line box buffer
		Vector3 boxOutlineVerts[48];

		// Front face
		boxOutlineVerts[0] = Vector3( -1.0f, 1.0f, -1.0f );
		boxOutlineVerts[1] = Vector3( 1.0f, 1.0f, -1.0f );
		boxOutlineVerts[2] = Vector3( 1.0f, 1.0f, -1.0f );
		boxOutlineVerts[3] = Vector3( 1.0f, -1.0f, -1.0f );
		boxOutlineVerts[4] = Vector3( 1.0f, -1.0f, -1.0f );
		boxOutlineVerts[5] = Vector3( -1.0f, -1.0f, -1.0f );
		boxOutlineVerts[6] = Vector3( -1.0f, -1.0f, -1.0f );
		boxOutlineVerts[7] = Vector3( -1.0f, 1.0f, -1.0f );

		// Back face
		boxOutlineVerts[15] = Vector3( -1.0f, 1.0f, 1.0f );
		boxOutlineVerts[14] = Vector3( 1.0f, 1.0f, 1.0f );
		boxOutlineVerts[13] = Vector3( 1.0f, 1.0f, 1.0f );
		boxOutlineVerts[12] = Vector3( 1.0f, -1.0f, 1.0f );
		boxOutlineVerts[11] = Vector3( 1.0f, -1.0f, 1.0f );
		boxOutlineVerts[10] = Vector3( -1.0f, -1.0f, 1.0f );
		boxOutlineVerts[9] = Vector3( -1.0f, -1.0f, 1.0f );
		boxOutlineVerts[8] = Vector3( -1.0f, 1.0f, 1.0f );

		// Left face
		boxOutlineVerts[16] = Vector3( -1.0f, 1.0f, 1.0f );
		boxOutlineVerts[17] = Vector3( -1.0f, 1.0f, -1.0f );
		boxOutlineVerts[18] = Vector3( -1.0f, 1.0f, -1.0f );
		boxOutlineVerts[19] = Vector3( -1.0f, -1.0f, -1.0f );
		boxOutlineVerts[20] = Vector3( -1.0f, -1.0f, -1.0f );
		boxOutlineVerts[21] = Vector3( -1.0f, -1.0f, 1.0f );
		boxOutlineVerts[22] = Vector3( -1.0f, -1.0f, 1.0f );
		boxOutlineVerts[23] = Vector3( -1.0f, 1.0f, 1.0f );

		// Right face
		boxOutlineVerts[31] = Vector3( 1.0f, 1.0f, 1.0f );
		boxOutlineVerts[30] = Vector3( 1.0f, 1.0f, -1.0f );
		boxOutlineVerts[29] = Vector3( 1.0f, 1.0f, -1.0f );
		boxOutlineVerts[28] = Vector3( 1.0f, -1.0f, -1.0f );
		boxOutlineVerts[27] = Vector3( 1.0f, -1.0f, -1.0f );
		boxOutlineVerts[26] = Vector3( 1.0f, -1.0f, 1.0f );
		boxOutlineVerts[25] = Vector3( 1.0f, -1.0f, 1.0f );
		boxOutlineVerts[24] = Vector3( 1.0f, 1.0f, 1.0f );

		// Top face
		boxOutlineVerts[32] = Vector3( -1.0f, 1.0f, 1.0f );
		boxOutlineVerts[33] = Vector3( 1.0f, 1.0f, 1.0f );
		boxOutlineVerts[34] = Vector3( 1.0f, 1.0f, 1.0f );
		boxOutlineVerts[35] = Vector3( 1.0f, 1.0f, -1.0f );
		boxOutlineVerts[36] = Vector3( 1.0f, 1.0f, -1.0f );
		boxOutlineVerts[37] = Vector3( -1.0f, 1.0f, -1.0f );
		boxOutlineVerts[38] = Vector3( -1.0f, 1.0f, -1.0f );
		boxOutlineVerts[39] = Vector3( -1.0f, 1.0f, 1.0f );

		// Bottom face
		boxOutlineVerts[47] = Vector3( -1.0f, -1.0f, 1.0f );
		boxOutlineVerts[46] = Vector3( 1.0f, -1.0f, 1.0f );
		boxOutlineVerts[45] = Vector3( 1.0f, -1.0f, 1.0f );
		boxOutlineVerts[44] = Vector3( 1.0f, -1.0f, -1.0f );
		boxOutlineVerts[43] = Vector3( 1.0f, -1.0f, -1.0f );
		boxOutlineVerts[42] = Vector3( -1.0f, -1.0f, -1.0f );
		boxOutlineVerts[41] = Vector3( -1.0f, -1.0f, -1.0f );
		boxOutlineVerts[40] = Vector3( -1.0f, -1.0f, 1.0f );

		// Create the box line buffer
		_boxOutlineVertexBuffer = _graphics->CreateVertexBuffer<Vector3>( 48, D3D11_USAGE_IMMUTABLE, boxOutlineVerts );

		// Setup the sphere
		{
			Array<Vector3> sphereVerts;

			// X-Axis
			const float dt = Math::pi / 8.0f;
			for ( float theta = 0; theta < Math::pi*2.0f; theta += dt )
			{
				Vector3& v1 = sphereVerts.Add();
				v1.x = cosf( theta );
				v1.y = sinf( theta );
				v1.z = 0;

				Vector3& v2 = sphereVerts.Add();
				v2.x = cosf( theta + dt );
				v2.y = sinf( theta + dt );
				v2.z = 0;
			}

			// Y-Axis
			for ( float theta = 0; theta < Math::pi*2.0f; theta += dt )
			{
				Vector3& v1 = sphereVerts.Add();
				v1.x = cosf( theta );
				v1.y = 0;
				v1.z = sinf( theta );

				Vector3& v2 = sphereVerts.Add();
				v2.x = cosf( theta + dt );
				v2.y = 0;
				v2.z = sinf( theta + dt );
			}

			// Z-Axis
			for ( float theta = 0; theta < Math::pi*2.0f; theta += dt )
			{
				Vector3& v1 = sphereVerts.Add();
				v1.x = 0;
				v1.y = sinf( theta );
				v1.z = cosf( theta );

				Vector3& v2 = sphereVerts.Add();
				v2.x = 0;
				v2.y = sinf( theta + dt );
				v2.z = cosf( theta + dt );
			}

			_spherePrimitiveCount = sphereVerts.Size();
			_sphereVertexBuffer = _graphics->CreateVertexBuffer<Vector3>( _spherePrimitiveCount, D3D11_USAGE_IMMUTABLE, sphereVerts );
		}

		// Setup the capsule
		{
			Array<Vector3> capsuleVerts;

			// Caps
			const float dt = Math::pi / 8.0f;
			{
				// Circle bases
				for ( float theta = 0; theta < Math::pi*2.0f; theta += dt )
				{
					Vector3& v1 = capsuleVerts.Add();
					v1.x = cosf( theta )*0.5f;
					v1.y = 0.5f;
					v1.z = sinf( theta )*0.5f;

					Vector3& v2 = capsuleVerts.Add();
					v2.x = cosf( theta + dt )*0.5f;
					v2.y = 0.5f;
					v2.z = sinf( theta + dt )*0.5f;

					Vector3& v3 = capsuleVerts.Add();
					v3.x = cosf( theta )*0.5f;
					v3.y = -0.5f;
					v3.z = sinf( theta )*0.5f;

					Vector3& v4 = capsuleVerts.Add();
					v4.x = cosf( theta + dt )*0.5f;
					v4.y = -0.5f;
					v4.z = sinf( theta + dt )*0.5f;
				}

				// Body
				const float dh = 0.1f;
				for ( float h = 0; h < 0.5f; h += dh )
				{
					for ( float theta = 0; theta < Math::pi*2.0f; theta += Math::pi / 2 )
					{
						Vector3& v5 = capsuleVerts.Add();
						v5.x = cosf( theta )*0.5f;
						v5.y = h;
						v5.z = sinf( theta )*0.5f;

						Vector3& v6 = capsuleVerts.Add();
						v6.x = cosf( theta )*0.5f;
						v6.y = h + dh;
						v6.z = sinf( theta )*0.5f;

						Vector3& v7 = capsuleVerts.Add();
						v7.x = cosf( theta )*0.5f;
						v7.y = -h;
						v7.z = sinf( theta )*0.5f;

						Vector3& v8 = capsuleVerts.Add();
						v8.x = cosf( theta )*0.5f;
						v8.y = -h - dh;
						v8.z = sinf( theta )*0.5f;
					}
				}

				// X-axis
				for ( float theta = 0; theta < Math::pi - dt; theta += dt )
				{
					Vector3& v1 = capsuleVerts.Add();
					v1.x = cosf( theta )*0.5f;
					v1.y = sinf( theta )*0.5f + 0.5f;
					v1.z = 0;

					Vector3& v2 = capsuleVerts.Add();
					v2.x = cosf( theta + dt )*0.5f;
					v2.y = sinf( theta + dt )*0.5f + 0.5f;
					v2.z = 0;

					Vector3& v3 = capsuleVerts.Add();
					v3.x = cosf( theta + Math::pi )*0.5f;
					v3.y = sinf( theta + Math::pi )*0.5f - 0.5f;
					v3.z = 0;

					Vector3& v4 = capsuleVerts.Add();
					v4.x = cosf( theta + Math::pi + dt )*0.5f;
					v4.y = sinf( theta + Math::pi + dt )*0.5f - 0.5f;
					v4.z = 0;
				}

				// Z-axis
				for ( float theta = 0; theta < Math::pi - dt; theta += dt )
				{
					Vector3& v1 = capsuleVerts.Add();
					v1.z = cosf( theta )*0.5f;
					v1.y = sinf( theta )*0.5f + 0.5f;
					v1.x = 0;

					Vector3& v2 = capsuleVerts.Add();
					v2.z = cosf( theta + dt )*0.5f;
					v2.y = sinf( theta + dt )*0.5f + 0.5f;
					v2.x = 0;

					Vector3& v3 = capsuleVerts.Add();
					v3.z = cosf( theta + Math::pi )*0.5f;
					v3.y = sinf( theta + Math::pi )*0.5f - 0.5f;
					v3.x = 0;

					Vector3& v4 = capsuleVerts.Add();
					v4.z = cosf( theta + Math::pi + dt )*0.5f;
					v4.y = sinf( theta + Math::pi + dt )*0.5f - 0.5f;
					v4.x = 0;
				}
			}

			_capsulePrimitiveCount = capsuleVerts.Size();
			_capsuleVertexBuffer = _graphics->CreateVertexBuffer<Vector3>( _capsulePrimitiveCount, D3D11_USAGE_IMMUTABLE, capsuleVerts );
		}

		// Load light texture
		_lightTexture = _graphics->CreateTextureFromFile( "Resources\\Textures\\Editor\\lightbulb.png" );

		// Setup for material preview rendering
		_materialPreviewMesh = Model::CreateFromFile( "Resources\\Models\\sphere.mesh", _graphics->GetEngine() );
		_materialPreviewDepthStencil = _graphics->CreateDepthStencil( 106, 103, nullptr, false );
		_materialPreviewTarget = _graphics->CreateTexture( 106, 103, DXGI_FORMAT_R8G8B8A8_UNORM, 1, nullptr, D3D11_USAGE_DEFAULT, true, false );
		_materialPreviewTarget->AttachDepthStencil( _materialPreviewDepthStencil );

		// Setup for mesh preview rendering
		DXGI_SAMPLE_DESC sd;
		sd.Count = 1;
		sd.Quality = 0;
		_meshPreviewTargetMSAA = _graphics->CreateTexture( 189, 134, DXGI_FORMAT_R8G8B8A8_UNORM, 1, &sd, D3D11_USAGE_DEFAULT, true, false );
		_meshPreviewDepthStencilMSAA = _graphics->CreateDepthStencil( 189, 134, &sd, false );
		_meshPreviewTargetMSAA->AttachDepthStencil( _meshPreviewDepthStencilMSAA );
		_meshPreviewTarget = _graphics->CreateTexture( 189, 134, DXGI_FORMAT_R8G8B8A8_UNORM, 1, nullptr, D3D11_USAGE_DEFAULT, true, false );

		// Spere rendering
		_sphereMesh = Model::CreateFromFile( "Resources\\Models\\sphere.mesh", _graphics->GetEngine() );

		return S_OK;
	}



	// Release debug renderer
	void DebugVisualizer::Release()
	{
		_lightBorderVertexBuffer = nullptr;
		_boxOutlineVertexBuffer = nullptr;
		_lineVetexBuffer = nullptr;
		_lightTexture = nullptr;

		_lightRenderList.Release();
		_meshRenderList.Release();

		SafeDelete( _materialPreviewMesh );
		SafeDelete( _sphereMesh );

		_materialPreviewTarget = nullptr;
		_materialPreviewDepthStencil = nullptr;
		_meshPreviewTargetMSAA = nullptr;
		_meshPreviewDepthStencilMSAA = nullptr;
		_meshPreviewTargetMSAA = nullptr;
		_meshPreviewTarget = nullptr;
		_quadVertexBuffer = nullptr;
	}


	// Adds a mesh to the debug render list
	void DebugVisualizer::DrawMesh( Model* pMesh, int32 submesh, bool submeshOnly, bool wire, bool boundingBox )
	{
		VisualizableObject rt;
		rt.mesh = pMesh;
		rt.pos = pMesh->GetPos();
		rt.submesh = submesh;
		rt.color[0] = Color( 0.85f, 0.25f, 0, 0 );
		rt.color[1] = Color( 0, 0.25f, 0.85f, 0 );
		rt.isWireframe = wire;
		rt.boundingBox = boundingBox;
		rt.submeshOnly = submeshOnly;
		rt.ignoreDepth = false;
		_meshRenderList.Add( rt );
	}

	// Adds a mesh to the debug render list
	void DebugVisualizer::DrawMesh( Model* pMesh, const Color& color, bool ignoreDepth )
	{
		VisualizableObject rt;
		rt.mesh = pMesh;
		rt.pos = pMesh->GetPos();
		rt.submesh = 0;
		rt.color[0] = color;
		rt.color[1] = color;
		rt.isWireframe = false;
		rt.boundingBox = false;
		rt.submeshOnly = false;
		rt.ignoreDepth = ignoreDepth;
		_meshRenderList.Add( rt );
	}

	// Adds a mesh to the render list
	void DebugVisualizer::DrawMesh( Model* pMesh, const Color& color, const Vector3& pos, const Vector3& rot, const Vector3& scale, bool ignoreDepth )
	{
		VisualizableObject rt;
		rt.mesh = pMesh;
		rt.pos = pMesh->GetPos();
		rt.submesh = 0;
		rt.color[0] = color;
		rt.color[1] = color;
		rt.isWireframe = false;
		rt.boundingBox = false;
		rt.submeshOnly = false;
		rt.position = pos;
		rt.rotation = rot;
		rt.scale = scale;
		rt.useTransform = true;
		rt.ignoreDepth = ignoreDepth;
		_meshRenderList.Add( rt );
	}

	// Adds a bounding box to the debug render list
	void DebugVisualizer::DrawBox( const Vector3& pos, const Vector3& size, const Color& color )
	{
		DrawBox( Matrix::CreateScaling( size ) * Matrix::CreateTranslation( pos ), color );
	}

	// Add a box to the render list
	void DebugVisualizer::DrawBox( const Matrix& transform, const Color& color )
	{
		DebugObject box = { transform, color };
		_boxRenderList.Add( box );
	}

	// Draw a billboarded quad
	void DebugVisualizer::DrawBillboard( const Vector3& pos, const Vector3& scale, Texture::pointer texture, const Color& color )
	{
		Billboard obj = { pos, scale, color, texture };
		_billboards.Add( obj );
	}

	// Add a sphere to the render list
	void DebugVisualizer::DrawSphere( const Vector3& pos, float radius, const Color& color )
	{
		DrawSphere( Matrix::CreateScaling( radius, radius, radius ) * Matrix::CreateTranslation( pos ), color );
	}

	// Add a sphere to the render list
	void DebugVisualizer::DrawSphere( const Matrix& transform, const Color& color )
	{
		DebugObject sphere = { transform, color };
		_sphereRenderList.Add( sphere );
	}

	// Add a capsule to the render list
	void DebugVisualizer::DrawCapsule( const Vector3& pos, float height, float radius, const Color& color )
	{
		DebugObject& capsule = _capsuleRenderList.Add();
		capsule.color = color;
		capsule.transform = Matrix::CreateScaling( radius, height, radius ) * Matrix::CreateTranslation( pos );
	}

	// Add a capsule to the render list
	void DebugVisualizer::DrawCapsule( const Matrix& transform, const Color& color )
	{
		DebugObject& capsule = _capsuleRenderList.Add();
		capsule.color = color;
		capsule.transform = transform;
	}


	// Render lights
	void DebugVisualizer::Render()
	{
		////////////////////////////////////////
		// Render terrain quadtree
		//if(_graphics->GetTerrain())
		//_graphics->GetTerrain()->RenderQuadtreeDebug(m_BoxMesh, Device::Effect);
		//	_graphics->GetTerrain()->RenderDebug(m_BoxMesh, Device::Effect);

		// Render the queued shapes
		RenderLines();
		RenderBoxes();
		RenderSpheres();
		RenderCapsules();
		RenderBillboards();
		if ( _showTerrainWidget )
			RenderTerrainWidget();

		// Selection box
		if ( _showSelectionBox )
			ShowSelectionBox();

		////////////////////////////////////////
		// Render lights
		Bitfield32 flag;
		if ( _graphics->GetCamera() && _showLights )
		{
			_showLights = false;

			// Set textures
			Texture::pointer tempTextures[TEX_SIZE];
			for ( int32 i = 0; i < TEX_SIZE; i++ )
				tempTextures[i] = nullptr;
			tempTextures[TEX_DIFFUSE] = tempTextures[TEX_ALPHA] = _lightTexture;
			_materialTexturesVariable->SetResourceArray( tempTextures, 0, TEX_SIZE );
			flag.Set( TEX_DIFFUSE );
			flag.Set( TEX_ALPHA );
			_materialTextureFlagsVariable->SetInt( flag.GetBits() );

			// Billboard matrix
			Matrix mWorld, mT;
			Matrix mR = _graphics->GetCamera()->GetBillboardMatrix();
			Matrix mS = Matrix::CreateScaling( _lightRenderingScale*0.35f, _lightRenderingScale*0.5f, 0.0f );

			// Set vertex buffer
			_graphics->SetRenderToQuad();

			// Render each light as a bilboarded quad
			for ( uint32 i = 0; i < _graphics->GetNumLights(); i++ )
			{
				Light* light = _graphics->GetLight( i );

				// Set proper color
				_materialDiffuseVariable->SetFloatVector( light->GetColor() );

				// Set world matrix
				mT = Matrix::CreateTranslation( light->GetPosition().x, light->GetPosition().y, light->GetPosition().z );
				mWorld = mS*mR*mT;
				_worldMatrixVariable->SetMatrix( mWorld );

				// Render the quad
				_forwardPass->Apply();
				_graphics->GetContext()->Draw( 6, 0 );
			}

			// Render selected light border
			float dist = 1000;
			for ( uint32 i = 0; i < _lightRenderList.Size(); i++ )
			{
				_graphics->SetVertexBuffer( _lightBorderVertexBuffer->GetBuffer(), Vertex::size );
				_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINELIST );
				Vector3 color( 0, 0.5f, 0.95f );
				_materialDiffuseVariable->SetFloatVector( color );
				flag.Unset( TEX_DIFFUSE );
				flag.Unset( TEX_ALPHA );
				_materialTextureFlagsVariable->SetInt( flag.GetBits() );
				for ( float s = 1.0f; s > 0.95f; s -= 0.0004f )
				{
					// Set world matrix
					mT = Matrix::CreateTranslation( _lightRenderList[i]->GetPosition().x, _lightRenderList[i]->GetPosition().y, _lightRenderList[i]->GetPosition().z );
					mS = Matrix::CreateScaling( 0.6f*_lightRenderingScale*(s - 0.3f), 0.6f*_lightRenderingScale*s, 0 );
					mWorld = mS*mR*mT;
					_worldMatrixVariable->SetMatrix( mWorld );

					// Render the lines
					_positionOnlyWireframePass->Apply();
					_graphics->GetContext()->Draw( 16, 0 );
				}
				_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
			}
			_lightRenderList.Clear();
		}

		// Render the debug meshes
		flag.Unset( TEX_DIFFUSE );
		flag.Unset( TEX_ALPHA );
		_materialTextureFlagsVariable->SetInt( flag.GetBits() );
		for ( uint32 i = 0; i < _meshRenderList.Size(); i++ )
		{
			// Get the mesh
			Model* pMesh = _meshRenderList[i].mesh;

			// Pass the matrix pallete to the shader (for skinned meshes)
			if ( pMesh )
			{
				// Render each submesh
				for ( uint32 k = 0; k < pMesh->GetNumSubMesh(); k++ )
				{
					if ( _meshRenderList[i].submeshOnly && k != _meshRenderList[i].submesh )
						continue;

					// Set world matrix
					if ( _meshRenderList[i].useTransform )
					{
						pMesh->SetPosition( _meshRenderList[i].position );
						pMesh->SetRotation( _meshRenderList[i].rotation );
						pMesh->SetScale( _meshRenderList[i].scale );
						pMesh->UpdateWorldMatrix();
					}

					// Color
					if ( k == _meshRenderList[i].submesh )
						_materialDiffuseVariable->SetFloatVector( _meshRenderList[i].color[1] );
					else
						_materialDiffuseVariable->SetFloatVector( _meshRenderList[i].color[0] );

					// Render the mesh
					_graphics->SetWorldMatrix( *pMesh->GetSubMesh( k )->worldMatrix );
					if ( _meshRenderList[i].isWireframe )
					{
						if ( pMesh->GetSubMesh( k )->bonePalette )
						{
							if ( _meshRenderList[i].ignoreDepth )
								_positionOnlyWireframeSkinnedPass->Apply( _ignoreDepthState, nullptr, nullptr );
							else
								_positionOnlyWireframeSkinnedPass->Apply();
							_graphics->RenderSubMeshPosSkinned( *pMesh->GetSubMesh( k ) );
						}
						else
						{
							if ( _meshRenderList[i].ignoreDepth )
								_positionOnlyWireframePass->Apply( _ignoreDepthState, nullptr, nullptr );
							else
								_positionOnlyWireframePass->Apply();
							_graphics->RenderSubMeshPos( *pMesh->GetSubMesh( k ) );
						}
					}
					else
					{
						if ( _meshRenderList[i].ignoreDepth )
							_positionOnlyColorPass->Apply( _ignoreDepthState, nullptr, nullptr );
						else
							_positionOnlyColorPass->Apply();
						_graphics->RenderSubMeshPos( *pMesh->GetSubMesh( k ) );
					}
				}
			}
		}
		_meshRenderList.Clear();

		_needsTerrainSculpt = false;

		// Handle material preview rendering
		RenderMaterialPreviews();
	}



	// Renders lines
	void DebugVisualizer::RenderLines()
	{
		// Render the line
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINELIST );
		_graphics->SetInputLayout( PosVertex::pInputLayout );
		_graphics->SetVertexBuffer( _lineVetexBuffer->GetBuffer(), sizeof(Vector3) );
		for ( uint32 i = 0; i < _lineRenderList.Size(); ++i )
		{
			const Matrix t = Matrix::CreateTranslation( _lineRenderList[i].pos );
			const Matrix r = Matrix::CreateRotationYawPitchRoll( _lineRenderList[i].rot.y, _lineRenderList[i].rot.x, _lineRenderList[i].rot.z );
			const Matrix s = Matrix::CreateScaling( _lineRenderList[i].scale );
			const Matrix world = s*r*t;
			_worldMatrixVariable->SetMatrix( world );
			_materialDiffuseVariable->SetFloatVector( _lineRenderList[i].color );
			if ( _lineRenderList[i].ignoreDepth )
				_positionOnlyColorPass->Apply( _ignoreDepthState, nullptr, nullptr );
			else
				_positionOnlyColorPass->Apply();
			_graphics->GetContext()->Draw( 2, 0 );
		}
		_graphics->SetInputLayout( Vertex::pInputLayout );
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
		_lineRenderList.Clear();
	}

	// Render boxes
	void DebugVisualizer::RenderBoxes()
	{
		_graphics->SetInputLayout( PosVertex::pInputLayout );
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINELIST );
		_graphics->SetVertexBuffer( _boxOutlineVertexBuffer->GetBuffer(), _boxOutlineVertexBuffer->GetByteStride() );
		for ( uint32 i = 0; i < _boxRenderList.Size(); ++i )
		{
			// Set world matrix
			_worldMatrixVariable->SetMatrix( _boxRenderList[i].transform );

			// Color
			_materialDiffuseVariable->SetFloatVector( _boxRenderList[i].color );

			// Render the lines
			_positionOnlyWireframePass->Apply();
			_graphics->GetContext()->Draw( 48, 0 );
		}
		_boxRenderList.Clear();
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
		_graphics->SetInputLayout( Vertex::pInputLayout );
	}

	// Render the billboarded quads
	void DebugVisualizer::RenderBillboards()
	{
		Bitfield32 flag;
		_graphics->SetRenderToQuad();
		for ( uint32 i = 0; i < _billboards.Size(); ++i )
		{
			// Set world matrix
			Matrix scale = Matrix::CreateScaling( _billboards[i].scale );
			Matrix trans = Matrix::CreateTranslation( _billboards[i].pos );
			_worldMatrixVariable->SetMatrix( scale * _graphics->GetBillboardMatrix() * trans );

			// Color and texture
			_graphics->BindMaterialTexture( _billboards[i].texture, TEX_DIFFUSE );
			if ( _billboards[i].texture )
				flag.Set( TEX_DIFFUSE );
			else
				flag.Unset( TEX_DIFFUSE );
			_materialTextureFlagsVariable->SetInt( flag.GetBits() );
			_materialDiffuseVariable->SetFloatVector( _billboards[i].color );

			// Render the lines
			_forwardPass->Apply();
			_graphics->GetContext()->Draw( 6, 0 );
		}
		_billboards.Clear();
	}

	// Render spheres
	void DebugVisualizer::RenderSpheres()
	{
		_graphics->SetInputLayout( Vertex::pInputLayout );
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINELIST );
		_graphics->SetVertexBuffer( _sphereVertexBuffer->GetBuffer(), sizeof(Vector3) );
		for ( uint32 i = 0; i < _sphereRenderList.Size(); ++i )
		{
			// Set world matrix
			_worldMatrixVariable->SetMatrix( _sphereRenderList[i].transform );

			// Color
			_materialDiffuseVariable->SetFloatVector( _sphereRenderList[i].color );

			// Render the lines
			_positionOnlyWireframePass->Apply();
			_graphics->GetContext()->Draw( _spherePrimitiveCount, 0 );
		}
		_sphereRenderList.Clear();
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
	}

	// Render the capsules
	void DebugVisualizer::RenderCapsules()
	{
		_graphics->SetInputLayout( Vertex::pInputLayout );
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_LINELIST );
		_graphics->SetVertexBuffer( _capsuleVertexBuffer->GetBuffer(), sizeof(Vector3) );
		for ( uint32 i = 0; i < _capsuleRenderList.Size(); ++i )
		{
			// Set world matrix
			_worldMatrixVariable->SetMatrix( _capsuleRenderList[i].transform );

			// Color
			_materialDiffuseVariable->SetFloatVector( _capsuleRenderList[i].color );

			// Render the lines
			_positionOnlyWireframePass->Apply();
			_graphics->GetContext()->Draw( _capsulePrimitiveCount, 0 );
		}
		_capsuleRenderList.Clear();
		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
	}


	// Render the terrain sculpt tool
	void DebugVisualizer::RenderTerrainWidget()
	{
		_showTerrainWidget = false;
		if ( !_graphics->GetTerrain() ) return;

		// Bind the current layer material data
		_graphics->GetTerrain()->BindLayer( _terrainSculptLayer, (_terrainSculptingMode == SCULPT_PAINT) );

		// Grid lines
		_graphics->SetRenderToQuad();
		_graphics->_depthBufferVariable->SetResource( *_graphics->_depthStencil );
		_graphics->BindRenderTargetWithoutDepth( *_graphics->_backBuffer );
		if ( _showGrid )
		{
			_gridPass->Apply();
			_graphics->GetContext()->Draw( 6, 0 );
		}

		// Render the widget
		static bool mouseDown = false;
		static Vector2 mouseTexCoords;
		bool update = false;
		const Point& mousePos = _graphics->GetEngine()->GetPlatform()->GetMouseLocation();
		if ( _graphics->MouseOnScreen() )
		{
			// Get the intersection point (only on the first click for grab mode)
			if ( _terrainSculptingMode == SCULPT_GRAB )
			{
				if ( _graphics->GetEngine()->GetPlatform()->MouseDown( MouseButton::Left ) && !mouseDown )
				{
					mouseDown = true;
					update = true;
				}
				if ( !_graphics->GetEngine()->GetPlatform()->MouseDown( MouseButton::Left ) )
				{
					mouseDown = false;
					update = true;
				}
			}
			else
			{
				update = true;
			}

			// Render the widget
			_terrainWidgetPass->Apply();
			_graphics->GetContext()->Draw( 6, 0 );

			static bool isSculpting = false;

			// Get the mouse-terrain pick point
			if ( !update || (update && _graphics->MouseOnTerrain()) )
			{
				if ( update )
				{
					_mouseIntersectVariable->SetFloatVector( _graphics->GetTerrainPickPoint() );
				}

				// Sculpting
				if ( _needsTerrainSculpt )
				{
					_graphics->GetTerrain()->CacheSculptRegion( _graphics->GetTerrainPickPoint(), _terrainSculptRadius );
					DoTerrainSculpt( _graphics->GetTerrainPickPoint() );
				}
			}

			// Virtual texture painting
			if ( _terrainSculptingMode == SCULPT_PAINT )
				HandleTerrainPaint();
		}
	}

	// Perform terrain sculpting
	void DebugVisualizer::DoTerrainSculpt( Vector3 pickPoint )
	{
		// Do the sculpting
		if ( _terrainSculptingMode == SCULPT_PAINT )
		{
			QueueTerrainPaint();
		}
		else
		{
			_graphics->GetTerrain()->Sculpt( pickPoint, _terrainSculptRadius, _terrainSculptHardness,
				_terrainSculptStrength, _terrainSculptDelta, _terrainSculptLayer, _terrainSculptingMode );
		}

	}


	// Perform the virtual texture painting
	void DebugVisualizer::DoVirtualTexturePaint()
	{
		// Paint to terrain
		if ( _graphics->GetTerrain() )
		{
			// Set the paint layer info and texture
			_graphics->SetMaterial( MaterialBinding::pointer( nullptr ) );
			_graphics->GetTerrain()->BindLayer( _terrainSculptLayer );

			// Paint to the VT or the terrain, depending on the terrain type
			if ( _graphics->GetTerrain()->GetRenderMode() == TerrainMode::Blendmap )
				_graphics->GetTerrain()->PaintTerrain( _graphics->GetTerrainPickPoint(), _terrainSculptRadius );
		}
	}

	// Renders a material preview texture and saves it to a file
	void DebugVisualizer::RenderMaterialPreviews()
	{
		if ( _materialPreviews.IsEmpty() )
			return;

		_graphics->SetMaterial( MaterialBinding::pointer( nullptr ) );
		for ( uint32 i = 0; i < _materialPreviews.Size(); ++i )
		{
			auto mat = _materialPreviews[i];
			_graphics->SetViewport( _materialPreviewTarget->GetViewport() );
			_materialPreviewTarget->SetClearColor( 0.3f, 0.3f, 0.3f, 0.0f );
			_materialPreviewTarget->Clear();
			_materialPreviewTarget->ClearDSV();
			_graphics->BindRenderTarget( *_materialPreviewTarget );
			_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

			// Setup the matrices
			Matrix mWorld, mView, mProj, mViewProj;
			mView = Matrix::CreateLookAtView( Vector3( -1, 0.5, -1.75 ), Vector3( 0, 0, 0 ), Vector3( 0, 1, 0 ) );
			mProj.Perspective( Math::pi / 3.0f, 1.0f, 0.01f, 100.0f );
			mViewProj = mView * mProj;
			_worldMatrixVariable->SetMatrix( mWorld );
			_graphics->_viewProjectionVariable->SetMatrix( mViewProj );

			// Render the sphere mesh
			_graphics->SetMaterial( mat );
			_materialPreviewPass->Apply();
			_graphics->RenderSubMesh( *_materialPreviewMesh->GetSubMesh( 0 ) );

			// Restore the view / projection variable
			mViewProj = _graphics->GetCamera()->GetViewMatrix() *_graphics->GetCamera()->GetProjectionMatrix();
			_graphics->_viewProjectionVariable->SetMatrix( mViewProj );

			// Save the texture to a file
			String file = mat->GetMaterial()->GetFilePath().RemoveFileExtension() + "_preview.jpg";
			_graphics->SaveTexture( file, _materialPreviewTarget, D3DX11_IFF_JPG );
		}
		_materialPreviews.Clear();

		// Restore the backbuffer
		_graphics->SetViewport( _graphics->_backBuffer->GetViewport() );
		_graphics->BindRenderTarget( *_graphics->_backBuffer );
	}

	// Renders a mesh preview texture and saves it to a file
	String DebugVisualizer::RenderMeshPreview( Model* pMesh )
	{
		// 		_graphics->SetViewport( _meshPreviewTargetMSAA->GetViewport() );
		// 		_meshPreviewTargetMSAA->SetClearColor( 0.7f, 0.7f, 0.7f, 0.7f );
		// 		_meshPreviewTargetMSAA->Clear();
		// 		_meshPreviewTargetMSAA->ClearDSV();
		// 		_graphics->BindRenderTarget( *_meshPreviewTargetMSAA );
		// 
		// 		_graphics->SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
		// 		_graphics->SetInputLayout( Vertex::pInputLayout );
		// 
		// 		// Setup the matrices
		// 		Matrix mWorld, mView, mProj, mViewProj;
		// 		mView.LookAt( Vector3( 0, 0, -1.5f*pMesh->GetBounds().GetRadius() ), Vector3( 0, 0, 0 ) );
		// 		mProj.Perspective( Math::pi / 3.0f, 1.0f, 0.01f, 100.0f );
		// 		mViewProj = mView * mProj;
		// 		_worldMatrixVariable->SetMatrix( mWorld );
		// 		_graphics->_viewProjectionVariable->SetMatrix( mViewProj );
		// 
		// 		// Build the correct world matrix, using the camera view angles to rotate the mesh
		// 		Vector3 cachedRot = pMesh->GetRot();
		// 		Vector3 cachedPos = pMesh->GetPos();
		// 		pMesh->SetPosition( 0, 0, 0 );
		// 		pMesh->SetRotation( 0, 0, 0 );
		// 
		// 		// First do an ambient pass to fill the zbuffer
		// 		// Render it to the preview texture
		// 		for ( uint32 i = 0; i < pMesh->GetNumSubMesh(); i++ )
		// 		{
		// 			RenderNode& mesh = *pMesh->GetSubMesh( i );
		// 
		// 			// World matrix
		// 			_worldMatrixVariable->SetMatrix( *mesh.worldMatrix );
		// 
		// 			// Set the material
		// 			_graphics->SetMaterial( mesh.materialBinding );
		// 
		// 			_graphics->RenderSubMesh( mesh, _forwardAmbientMSAAPass );
		// 		}
		// 
		// 		// Render it to the preview texture
		// 		for ( uint32 i = 0; i < pMesh->GetNumSubMesh(); i++ )
		// 		{
		// 			RenderNode& mesh = *pMesh->GetSubMesh( i );
		// 
		// 			// World matrix
		// 			_worldMatrixVariable->SetMatrix( *mesh.worldMatrix );
		// 
		// 			// Set the material
		// 			_graphics->SetMaterial( mesh.materialBinding );
		// 
		// 			// Render it for each light
		// 			for ( int32 e = 0; e < 4; e++ )
		// 			{
		// 				//_graphics->SetLight( _previewLights[e] );
		// 				M_ASSERT( false, "NEED SOLUTION" );
		// 				_graphics->RenderSubMesh( mesh, _forwardShadeMSAAPass );
		// 			}
		// 		}
		// 
		// 		// Restore
		// 		pMesh->SetRotation( cachedRot );
		// 		pMesh->SetPosition( cachedPos );
		// 		mViewProj = _graphics->GetCamera()->GetViewMatrix() * _graphics->GetCamera()->GetProjectionMatrix();
		// 		_graphics->_viewProjectionVariable->SetMatrix( mViewProj );
		// 
		// 		// Resolve the msaa target
		// 		_meshPreviewTarget->Clear();
		// 		_graphics->ResolveTexture( _meshPreviewTarget, _meshPreviewTargetMSAA );
		// 
		// 		// Save the texture to a file
		// 		String file = _graphics->GetEngine()->GetPlatform()->GetWorkingDirectory() + "Resources\\Textures\\Editor\\Mesh Previews\\preview.jpg";
		// 		_graphics->SaveTexture( file, _meshPreviewTarget, D3DX11_IFF_JPG );
		// 		return file;
		M_ASSERT( false, "Not Implemented" );
		return "";
	}

	void DebugVisualizer::BindEffectVariables()
	{
		_forwardPass = _graphics->GetEffect()->GetPassByName( "Unlit" );
		_forwardNoDepthPass = _graphics->GetEffect()->GetPassByName( "UnlitNoDepth" );
		_worldMatrixVariable = _graphics->GetEffect()->GetVariableByName( "g_mWorld" )->AsMatrix();
		_positionOnlyWireframeSkinnedPass = _graphics->GetEffect()->GetPassByName( "PosWireframeSkinned" );
		_mouseIntersectPass = _graphics->GetEffect()->GetPassByName( "MouseIntersect" );
		_selectionBoxPass = _graphics->GetEffect()->GetPassByName( "SelectionBox" );
		_terrainWidgetPass = _graphics->GetEffect()->GetPassByName( "PaintWidget" );
		_gridPass = _graphics->GetEffect()->GetPassByName( "Grid" );
		_positionOnlyWireframePass = _graphics->GetEffect()->GetPassByName( "PosWireframe" );
		_positionOnlyColorPass = _graphics->GetEffect()->GetPassByName( "PosColor" );
		_materialPreviewPass = _graphics->GetEffect()->GetPassByName( "MaterialPreview" );
		_ignoreDepthState = _graphics->GetEffect()->GetVariableByName( "DisableDepthDS" )->AsDepthStencilState();

		TerrainWidgetRadiusVariable = _graphics->GetEffect()->GetVariableByName( "g_TWRadius" )->AsScalar();
		TerrainWidgetHardnessVariable = _graphics->GetEffect()->GetVariableByName( "g_TWHardness" )->AsScalar();
		TerrainWidgetStrengthVariable = _graphics->GetEffect()->GetVariableByName( "g_TWStrength" )->AsScalar();

		_mouseIntersectVariable = _graphics->GetEffect()->GetVariableByName( "g_MouseSceneIntersect" )->AsVector();

		_materialTextureFlagsVariable = _graphics->GetEffect()->GetVariableByName( "g_MaterialTextureFlag" )->AsScalar();
		_materialDiffuseVariable = _graphics->GetEffect()->GetVariableByName( "g_MaterialColor" )->AsVector();
		_materialTexturesVariable = _graphics->GetEffect()->GetVariableByName( "g_txMaterial" )->AsShaderResource();

		// Set terrain sculpting stuff
		SetTerrainSculptMode( _terrainSculptingMode );
		SetTerrainWidgetDelta( _terrainSculptDelta );
		SetTerrainWidgetDetail( _terrainSculptLayer );
		SetTerrainWidgetHardness( _terrainSculptHardness );
		SetTerrainWidgetRadius( _terrainSculptRadius );
		SetTerrainWidgetStrength( _terrainSculptStrength );
	}


	// Handle vt painting update
	void DebugVisualizer::HandleTerrainPaint()
	{
		if ( _needsVTPainting )
		{
			DoVirtualTexturePaint();
			_needsVTPainting = false;
		}
	}

	// Draw a line
	void DebugVisualizer::DrawLine( const Vector3& pos, const Vector3& rot, const Vector3& scale, const Color& color, bool ignoreDepth )
	{
		DebugLine line = { pos, rot, scale, color, ignoreDepth };
		_lineRenderList.Add( line );
	}

	// Draw a selection box
	void DebugVisualizer::ShowSelectionBox()
	{
		_showSelectionBox = false;

		// Make the verts
		// (p1 and p2 are two opposing corners of the box)
		Vertex verts[6];
		memcpy( verts, _graphics->_quadVerts, 6 * sizeof(Vertex) );
		Vector2 p1( _selectionBox.x, _selectionBox.y );
		Vector2 p2( _selectionBox.z, _selectionBox.w );
		p1.x = 2.0f*p1.x / _graphics->GetWidth() - 1.0f;
		p1.y = -(2.0f*p1.y / _graphics->GetHeight() - 1.0f);
		p2.x = 2.0f*p2.x / _graphics->GetWidth() - 1.0f;
		p2.y = -(2.0f*p2.y / _graphics->GetHeight() - 1.0f);
		verts[0].pos = Vector3( p1.x, p1.y, 0.5f );
		verts[1].pos = Vector3( p2.x, p1.y, 0.5f );
		verts[2].pos = Vector3( p2.x, p2.y, 0.5f );
		verts[3].pos = Vector3( p2.x, p2.y, 0.5f );
		verts[4].pos = Vector3( p1.x, p2.y, 0.5f );
		verts[5].pos = Vector3( p1.x, p1.y, 0.5f );

		// Fill the vertex buffer
		auto mappedVerts = _quadVertexBuffer->Map( D3D11_MAP_WRITE_DISCARD );
		memcpy( mappedVerts, verts, sizeof(Vertex)* 6 );
		_quadVertexBuffer->Unmap();

		// Set the effect
		_selectionBoxPass->Apply();

		// Render the buffer
		_graphics->SetVertexBuffer( _quadVertexBuffer->GetBuffer(), sizeof(Vertex) );
		_graphics->GetContext()->Draw( 6, 0 );
	}

	// Clear the render lists
	void DebugVisualizer::Clear()
	{
		_meshRenderList.Clear();
		_sphereRenderList.Clear();
		_capsuleRenderList.Clear();
		_lineRenderList.Clear();
	}

	// Virtual texture painting
	void DebugVisualizer::QueueTerrainPaint()
	{
		_needsVTPainting = true;
	}

	// Set the detail level for the terrain sculpting tool
	void DebugVisualizer::SetTerrainWidgetDetail( int32 f )
	{
		_terrainSculptLayer = f;
	}

	// Render a preview for a material
	void DebugVisualizer::DrawMaterialPreview( MaterialBinding::pointer mat )
	{
		_materialPreviews.Add( mat );
	}

}


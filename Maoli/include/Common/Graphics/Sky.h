//--------------------------------------------------------------------------------------
// File: Sky.h
//
// Implementation of "Precomputed Atmospheric Scattering"
// 
// Simulates multiple scattering effects from any view point
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Texture.h"
#include "spa/spa.h"

namespace Maoli
{

	// Forward decl
	class Renderer;

	// Manages the sky
	// Atmospheric scattering, time of day
	class Sky
	{
	public:

		// Ctor
		Sky(Renderer* graphics);
		
		// Creates the sky system
		HRESULT Init();

		// Free resources
		void Release();

		// Draw the sky
		void Render(const Vector3& pos, EffectPass* pass);

		// Build the scattering textures
		void ComputeScattering(Camera& camera);

		// Binds the effect variables when the effect is reloaded
		void BindEffectVariables();

		// Set the date
		void SetDate(int32 month, int32 day, int32 year){
			m_Month = month;
			m_Day = day;
			m_Year = year;
		}
		
		// Set the time of day
		void SetTimeOfDay(int32 hour, int32 minute, int32 second){
			m_Hour = hour;
			m_Minute = minute;
			m_Second = second;
		}

		// Get the time of day hours
		inline int32 GetTimeOfDayHours() const { return m_Hour; }
		
		// Set the location on earth
		void SetViewerLocation(int32 latitude, int32 longitude, int32 elevation){
			m_TOD.latitude = latitude;
			m_TOD.longitude = longitude;
			m_TOD.elevation = elevation;
		}


		// Get the sun direction
		Vector3 GetSunDirection(){
			return -m_SunDirection;
		}

		// Get the cube map reflection texture
		Texture::pointer GetCubemap(){ return m_Cubemap; }



	private:

		// Constants needed for rendering
		struct ScbConstant
		{
			float PI;
			float InnerRadius;
			float OuterRadius;
			float fScale;
			float KrESun;
			float KmESun;
			float Kr4PI;
			float Km4PI;
			int32 tNumSamples;
			int32 iNumSamples;
			Vector2 v2dRayleighMieScaleHeight;
			Vector3 WavelengthMie;
			float InvOpticalDepthN;
			Vector3 v3HG;
			float InvOpticalDepthNLessOne;
			Vector2 InvRayleighMieN;
			Vector2 InvRayleighMieNLessOne;
			float HalfTexelOpticalDepthN;
			Vector3 InvWavelength4;
		};

		Renderer*				_graphics;
		ID3D11Buffer*			m_pVB;
		ID3D11Buffer*			m_pIB;
		uint32					m_NumVerts;
		uint32					m_NumIndices;
		Texture::pointer		m_PrecomputedScattering;	// Precomputed scattering textures
		Texture::pointer		m_CloudTexture;
		DepthStencil::pointer	m_Depth;
		uint32					m_ScatteringTextureSize;	// Dimension of the scattering texture	
		uint32					m_DomeSize;					// Dimension of dome
		uint32					m_OpticalDepthSize;			// Optical depth texture size
		ScbConstant				m_Constants;				// Sky shader constants

		// Cubemap
		Texture::pointer		m_Cubemap;
		DepthStencil::pointer	m_CubeDepth;

		int32						m_Month;
		int32						m_Day;
		int32						m_Year;
		int32						m_Hour;
		int32						m_Minute;
		int32						m_Second;

		spa_data				m_TOD;						// Time of day and sun angle data
		Vector3				m_SunDirection;				// Direction of the sun in the sky

		// Effect variables
		ShaderResource*		ScatteringTexturesVariable;
		ConstantBuffer*		SkyConstantBuffer;
		ShaderVector*		SunDirectionVariable;
		ShaderMatrix*		WorldMatrixVariable;
		ShaderMatrix*       ViewProjectionVariable;

		// Effect passes
		EffectPass*			_computeScatteringPass;
		EffectPass*			_skyReflectPass;

		// Builds the dome mesh
		HRESULT BuildDome();

		// Sets up the constants
		void SetConstants();

		// Computes the sun direction from the spa data
		void ComputeSunDirection();

		// Setup the tod system
		void InitTOD();
	};
}

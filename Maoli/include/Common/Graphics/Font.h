//--------------------------------------------------------------------------------------
// File: Font.h
//
// Bitmap fonts
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Texture.h"

namespace Maoli
{

	// Forward decl
	class Renderer;

	// Font resource
	class Font : public Resource<Renderer>
	{
		friend class ResourceManager<Renderer, Font>;
		friend class Pointer<Font>;
		friend class Renderer;
		friend class SpriteBatcher;

	public:

		// Smart pointer
		typedef Pointer<Font> pointer;

		// Set the width adjustment
		void SetWidthAdjustment(int32 w);

	private:

		Font(Renderer* graphics);
		~Font();

		// Load from a file
		bool Load(const char* imagefile);

		// Cleanup
		void Release();

		Texture::pointer					_sheet;				// Font sprite sheet
		std::unordered_map<char, Rect>		_characters;		// Character map
		bool								_adjusted;			// Width can only be adjusted once
	};
}


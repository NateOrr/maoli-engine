//--------------------------------------------------------------------------------------
// File: Mesh.h
//
// Binds geometry to the rendering pipeline
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/RenderNode.h"

namespace Maoli
{
	// Forward decl
	class Model;
	class Renderer;

	// Binds geometry to the rendering pipeline
	class MeshBinding : public Resource<Renderer>
	{
		friend class Renderer;
		friend class Model;
		friend ResourceManager<Renderer, MeshBinding>;
		friend Pointer<MeshBinding>;
	public:

		// Smart pointer
		typedef Pointer<MeshBinding> pointer;

		// Create from geometry
		HRESULT Init( Geometry* geometry );

		// Updates the vertex/index buffers
		HRESULT Update();

		// Get the render nodes
		inline Array<RenderNode>& GetRenderNodes() { return _renderNodes; }

		// Access the geometry
		inline Geometry* GetGeometry() { return _geometry; }

		// Cleanup
		void Release();

	protected:

		// Geometry the mesh is built from
		Geometry*	_geometry;

		// Vertex buffers
		Buffer<Vector3>::pointer     _positionVertexBuffer;
		Buffer<Vector2>::pointer     _texCoordVertexBuffer;
		Buffer<Vector3>::pointer     _normalVertexBuffer;
		Buffer<BoneWeight>::pointer	 _boneWeightVertexBuffer;
		Buffer<uint32>::pointer		 _indexBuffer;

		// Rendering
		Array<RenderNode>			 _renderNodes;

		// Models that use this mesh
		Array<Model*>				_models;


		// Ctor
		MeshBinding( Renderer* graphics );

		// Dtor
		~MeshBinding();

	private:

		// Prevent copying
		MeshBinding( const MeshBinding& );
		MeshBinding& operator=(const MeshBinding&);
	};


}

//--------------------------------------------------------------------------------------
// File: PipelineResource.h
//
// Graphics pipeline resource interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Shader;
	class DepthStencil;

	// Current bind location for a resource
	struct InputBinding
	{
		// Ctor
		InputBinding();

		uint32 slot;			// Slot the resource is currently bound to for input
		uint32 shaderType;	// Shader type the resource is bound within
	};

	// Current bind location for a render target
	struct OutputBinding
	{
		// Ctor
		OutputBinding();

		bool boundToPipeline;		// True if this resource is bound to the grapics pipeline
		bool fullyBound;			// True if all indices are bound
		uint32 index;					// Index of the resource that is bound
	};

	// A resource that can be bound to the rendering pipeline
	class PipeplineResource
	{
		friend class RenderState;
	public:

		// Ctor
		PipeplineResource();

		// Get the shader resource views
		virtual ID3D11ShaderResourceView**	GetSRV() = 0;

		// Get the render target views
		virtual ID3D11RenderTargetView**	GetRTV() = 0;

		// Get the unordered access views
		virtual ID3D11UnorderedAccessView**	GetUAV() = 0;

		// Get the depth stencil view
		virtual Pointer<DepthStencil> GetDSV() const { return Pointer<DepthStencil>( nullptr ); }

		// Get a resource array size
		virtual uint32 GetArraySize() const { return 0; }

		// Debug name
		const String& GetDebugName() const { return _debugName; }
		void SetDebugName( const char* value ) { _debugName = value; }

	protected:

		List<InputBinding>		_inputBindings;		// Holds input bindings within shaders
		OutputBinding			_outputBinding;		// Holds output binding slot info

	private:

		// Add an input binding
		void AddInputBinding( uint32 shaderType, int32 slot );

		// Clear an input binding
		void ClearInputBinding( uint32 shaderType, int32 slot );

		// Debug name
		String _debugName;

	};
}

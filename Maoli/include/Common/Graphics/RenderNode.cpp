//--------------------------------------------------------------------------------------
// File: RenderNode.cpp
//
// A RenderNode is defined as a set of renderable geometry with a single material.
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/RenderNode.h"

namespace Maoli
{
	// Constructors/Destructors
	RenderNode::RenderNode( Renderer* graphics )
	{
		indexBuffer = nullptr;
		_graphics = graphics;
		name = "SubMesh";
		startIndex = indexCount = 0;
		worldMatrix = nullptr;
		cubeMap = nullptr;
		bonePalette = nullptr;
		_effectPass = nullptr;
		_model = nullptr;
	}

	// Set the material binding
	void RenderNode::SetMaterialBinding( MaterialBinding::pointer mat )
	{
		_materialBinding = mat;
		if ( _model )
			_graphics->SetRenderCallback( *this );
	}

	// Compute the instance ID
	void RenderNode::ComputeInstanceID()
	{
		if ( _model )
		{
			uint32 modelID = (_model->GetMeshBinding()->GetID() & 0xFFF);
			uint32 submeshID = (_index & 0xFF) << 12;
			uint32 materialID = (_materialBinding->GetMaterial()->GetID() & 0xFFF) << 20;
			_instanceID = modelID | submeshID | materialID;
		}
	}


}

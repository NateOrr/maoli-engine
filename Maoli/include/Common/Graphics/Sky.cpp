//--------------------------------------------------------------------------------------
// File: Sky.cpp
//
// Implementation of "Precomputed Atmospheric Scattering"
// 
// Simulates multiple scattering effects from any view point
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Sky.h"
#include "Graphics/Renderer.h"

namespace Maoli
{
	// Ctor
	Sky::Sky( Renderer* graphics )
	{
		_graphics = graphics;
		m_pIB = nullptr;
		m_pVB = nullptr;
		m_CloudTexture = nullptr;
		m_Depth = nullptr;
		m_CloudTexture = nullptr;
		m_CubeDepth = nullptr;
		m_Cubemap = nullptr;
		m_PrecomputedScattering = nullptr;
	}

	// Creates the sky system
	HRESULT Sky::Init()
	{
		// Setup the tod system
		InitTOD();
		
		m_DomeSize = 128;
		m_OpticalDepthSize = 256;
		m_ScatteringTextureSize = 128;

		// Create the scattering textures
		DXGI_FORMAT formats[] = {
			DXGI_FORMAT_R16G16B16A16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
		};
		m_PrecomputedScattering = _graphics->CreateTextureArray(m_ScatteringTextureSize, m_ScatteringTextureSize/2, 2, formats, 1, nullptr, D3D11_USAGE_DEFAULT, true, true);
		if(!m_PrecomputedScattering)
		{
			M_ERROR("Scattering textures failed to create");
			return E_FAIL;
		}

		m_Depth = _graphics->CreateDepthStencil(m_ScatteringTextureSize, m_ScatteringTextureSize/2, nullptr, false);
		if(!m_Depth)
		{
			M_ERROR("Sky depth stencil failed to create");
			return E_FAIL;
		}
		m_PrecomputedScattering->AttachDepthStencil(m_Depth);

		// Setup the default time of day values
		m_Month = 6;
		m_Day = 16;
		m_Year = 2004;
		m_Hour = 12;
		m_Minute = 10;
		m_Second = 15;

		// Load a test sky texture
		m_CloudTexture = _graphics->CreateTextureFromFile("Resources\\Textures\\clouds.bmp");
		if(!m_CloudTexture)
		{
			M_ERROR("Cloud texture failed to load");
			return E_FAIL;
		}

		// Setup the effect variables
		BindEffectVariables();

		// Create the cubemap
		m_CubeDepth = _graphics->CreateDepthStencil(512, 512, nullptr, false);
		if(!m_CubeDepth)
		{
			M_ERROR("m_CubeDepth fail");
			return E_FAIL;
		}

		m_Cubemap = _graphics->CreateTextureCube(512, 512, DXGI_FORMAT_R8G8B8A8_UNORM, 1, false);
		if(!m_Cubemap)
		{
			M_ERROR("m_Cubemap fail");
			return E_FAIL;
		}
		m_Cubemap->AttachDepthStencil(m_CubeDepth);

		// Construct the dome mesh
		return BuildDome();
	}


	// Binds the effect variables when the effect is reloaded
	void Sky::BindEffectVariables()
	{
		// Grab variables and passes
		ScatteringTexturesVariable = _graphics->GetEffect()->GetVariableByName( "g_txScattering" )->AsShaderResource(); 
		SkyConstantBuffer = _graphics->GetEffect()->GetConstantBufferByName("cbSky");
		SunDirectionVariable = _graphics->GetEffect()->GetVariableByName("g_SunDir")->AsVector();
		WorldMatrixVariable = _graphics->GetEffect()->GetVariableByName( "g_mWorld" )->AsMatrix(); 
		ViewProjectionVariable = _graphics->GetEffect()->GetVariableByName( "g_mViewProjection" )->AsMatrix(); 
		_computeScatteringPass = _graphics->GetEffect()->GetPassByName( "ComputeScattering" );
		_skyReflectPass = _graphics->GetEffect()->GetPassByName( "SkyReflected" );

		SetConstants();
	}


	
	// Setup the constants
	void Sky::SetConstants()
	{
		m_Constants.PI = Math::pi;
		float ESun = 20.0f;
		float Kr = 0.0025f;
		float Km = 0.0010f;
		m_Constants.KrESun = Kr * ESun;
		m_Constants.KmESun = Km * ESun;
		m_Constants.Kr4PI = Kr * 4.0f * m_Constants.PI;
		m_Constants.Km4PI = Km * 4.0f * m_Constants.PI;
		m_Constants.InnerRadius = 6356.7523142f;
		m_Constants.OuterRadius = m_Constants.InnerRadius * 1.0157313f; // karman line
		m_Constants.fScale = 1.0f / (m_Constants.OuterRadius - m_Constants.InnerRadius);
		m_Constants.v2dRayleighMieScaleHeight.x = 0.25f; 
		m_Constants.v2dRayleighMieScaleHeight.y = 0.1f;

		m_Constants.InvWavelength4.x = 1.0f / powf( 0.650f, 4.0f );
		m_Constants.InvWavelength4.y = 1.0f / powf( 0.570f, 4.0f );
		m_Constants.InvWavelength4.z = 1.0f / powf( 0.475f, 4.0f );
		m_Constants.WavelengthMie.x = powf( 0.650f, -0.84f );
		m_Constants.WavelengthMie.y = powf( 0.570f, -0.84f );
		m_Constants.WavelengthMie.z = powf( 0.475f, -0.84f );

		float g = -0.995f;
		float g2 = g * g;
		m_Constants.v3HG.x = 1.5f * ( (1.0f - g2) / (2.0f + g2) );
		m_Constants.v3HG.y = 1.0f + g2;
		m_Constants.v3HG.z = 2.0f * g;

		m_Constants.tNumSamples = 50;
		m_Constants.iNumSamples = 20;

		m_Constants.InvOpticalDepthN = 1.0f / float( m_OpticalDepthSize );
		m_Constants.InvOpticalDepthNLessOne = 1.0f / float( m_OpticalDepthSize - 1.0f );
		m_Constants.HalfTexelOpticalDepthN = 0.5f / float( m_OpticalDepthSize );
		m_Constants.InvRayleighMieN = Vector2( 1.0f / float( m_ScatteringTextureSize ), 1.0f / float( m_ScatteringTextureSize / 2 ) );
		m_Constants.InvRayleighMieNLessOne = Vector2( 1.0f / float( m_ScatteringTextureSize - 1 ), 1.0f / float( m_ScatteringTextureSize / 2 - 1) );

		SkyConstantBuffer->Fill(&m_Constants);
	}


	// Setup the tod system
	void Sky::InitTOD()
	{
		m_TOD.year          = m_Year;
		m_TOD.month         = m_Month;
		m_TOD.day           = m_Day;
		m_TOD.hour          = m_Hour;
		m_TOD.minute        = m_Minute;
		m_TOD.second        = m_Second;
		m_TOD.timezone      = -7.0;
		m_TOD.delta_t       = 67;
		m_TOD.longitude     = -105.1786;
		m_TOD.latitude      = 0;
		m_TOD.elevation     = 1830.14;
		m_TOD.pressure      = 820;
		m_TOD.temperature   = 11;
		m_TOD.slope         = 30;
		m_TOD.azm_rotation  = -10;
		m_TOD.atmos_refract = 0.5667;
		m_TOD.function      = SPA_ALL;
	}


	// Free resources
	void Sky::Release()
	{
		m_PrecomputedScattering = nullptr;
		m_Depth = nullptr;
		m_CloudTexture = nullptr;
		m_Cubemap = nullptr;
		m_CubeDepth = nullptr;
		SafeRelease(m_pVB);
		SafeRelease(m_pIB);
	}


	// Computes the sun direction from the spa data
	void Sky::ComputeSunDirection()
	{
		/*const double d2r = 0.0174532925;
		
		// Get the sun data
		InitTOD();
		Log::_Print("spa result %d", spa_calculate(&m_TOD));
		Log::_Print("zenith %f", m_TOD.zenith);
		Log::_Print("azimuth %f", m_TOD.azimuth);
		Log::_Print("incidience %f", m_TOD.incidence);
		Log::_Print("hours %d %d", m_TOD.hour, m_Hour);


		// Convert the angles into a direction vector
		Matrix mRot, mRot2;
		D3DXMatrixRotationX(&mRot, -((90.0f-m_TOD.zenith)*d2r));
		D3DXMatrixRotationY(&mRot2, m_TOD.azimuth*d2r);
		D3DXMatrixMultiply(&mRot, &mRot2, &mRot);
		D3DXVec3TransformNormal(&m_SunDirection, &Vector3(0,0,1), &mRot);*/

		// Convert the hours to the angle in the sphere
		float deg = ((float)m_Hour / 24.0f) * (2.0f*Math::pi);
		Matrix mRot = Matrix::CreateRotationZ(deg);
		m_SunDirection = mRot.TransformNormal(Vector3(1,0,0));

		// Set it in the shader
		SunDirectionVariable->SetFloatVector((float*)&m_SunDirection);
	}

	
	// Build the scattering textures
	void Sky::ComputeScattering(Camera& camera)
	{
		// Set the sun direction
		ComputeSunDirection();
		
		// Compute the scattering integrals into a texture
		_graphics->BindRenderTargetArray(*m_PrecomputedScattering);
		_graphics->SetViewport(m_PrecomputedScattering->GetViewport());
		_computeScatteringPass->Apply();
		_graphics->GetContext()->Draw(6, 0);
		//_graphics->FrameStats.DrawCalls++;
		//_graphics->FrameStats.PolysDrawn += 2;


		// Setup for rendering
		_graphics->GetContext()->RSSetViewports(1, &m_Cubemap->GetViewport());
		ScatteringTexturesVariable->SetResourceArray(*m_PrecomputedScattering, 0, m_PrecomputedScattering->GetArraySize());
		_graphics->SetVertexBuffer(m_pVB, Vertex::size);
		_graphics->SetIndexBuffer(m_pIB, DXGI_FORMAT_R32_UINT);
		_graphics->SetInputLayout(Vertex::pInputLayout);

		// Center the dome at the camera position
		WorldMatrixVariable->SetMatrix( Matrix::CreateTranslation(camera.GetPosition()) );

		// Render to each face of the cube map
		Vector3 vLookDir;
		Vector3 vUpDir;
		Matrix mView;
		for(int32 j=0; j<6; j++)
		{
			// Clear and set the render target/depth buffer
			m_Cubemap->Clear(j);
			m_CubeDepth->Clear();
			_graphics->BindRenderTarget(*m_Cubemap, j);

			// Set the view matrix for this face
			switch( (CubemapFace)j )
			{
			case CubemapFace::PositiveX:
				vLookDir = Vector3( 1.0f, 0.0f, 0.0f );
				vUpDir   = Vector3( 0.0f, 1.0f, 0.0f );
				break;
			case CubemapFace::NegativeX:
				vLookDir = Vector3(-1.0f, 0.0f, 0.0f );
				vUpDir   = Vector3( 0.0f, 1.0f, 0.0f );
				break;
			case CubemapFace::PositiveY:
				vLookDir = Vector3( 0.0f, 1.0f, 0.0f );
				vUpDir   = Vector3( 0.0f, 0.0f,-1.0f );
				break;
			case CubemapFace::NegativeY:
				vLookDir = Vector3( 0.0f,-1.0f, 0.0f );
				vUpDir   = Vector3( 0.0f, 0.0f, 1.0f );
				break;
			case CubemapFace::PositiveZ:
				vLookDir = Vector3( 0.0f, 0.0f, 1.0f );
				vUpDir   = Vector3( 0.0f, 1.0f, 0.0f );
				break;
			case CubemapFace::NegativeZ:
				vLookDir = Vector3( 0.0f, 0.0f,-1.0f );
				vUpDir   = Vector3( 0.0f, 1.0f, 0.0f );
				break;
			}

			// Set the view transform for this cubemap face
			mView.LookAt(camera.GetPosition(), camera.GetPosition()+vLookDir);
			mView.Mult(camera.GetProjectionMatrix());
			ViewProjectionVariable->SetMatrix(mView);

			// Draw the sky dome
			_skyReflectPass->Apply();
			_graphics->GetContext()->DrawIndexed(m_NumIndices, 0, 0);
			//_graphics->FrameStats.DrawCalls++;
			//_graphics->FrameStats.PolysDrawn += m_NumIndices/3;
		}		

		//D3DX11SaveTextureToFileA(_graphics->GetContext(), m_Cubemap[0], D3DX11_IFF_DDS, "skycube.dds");

		mView = camera.GetViewMatrix() * camera.GetProjectionMatrix();
		ViewProjectionVariable->SetMatrix(mView);

	}

	// Draw the sky
	void Sky::Render(const Vector3& pos, EffectPass* pass)
	{
		// Center the dome at the camera position
		WorldMatrixVariable->SetMatrix( Matrix::CreateTranslation(pos) );
		
		// Draw the sky dome
		_graphics->SetInputLayout(Vertex::pInputLayout);
		ScatteringTexturesVariable->SetResourceArray(*m_PrecomputedScattering, 0, m_PrecomputedScattering->GetArraySize());
		_graphics->SetVertexBuffer(m_pVB, Vertex::size);
		_graphics->SetIndexBuffer(m_pIB, DXGI_FORMAT_R32_UINT);
		pass->Apply();
		_graphics->GetContext()->DrawIndexed(m_NumIndices, 0, 0);
		//_graphics->FrameStats.DrawCalls++;
		//_graphics->FrameStats.PolysDrawn += m_NumIndices/3;
	}


	// Builds the dome mesh
	HRESULT Sky::BuildDome()
	{
		HRESULT hr;
		int32 Latitude = m_DomeSize/2;
		int32 Longitude = m_DomeSize;
		int32 DVSize = Longitude * Latitude;
		int32 DISize = (Longitude - 1) * (Latitude - 1) * 2;
		DVSize *= 2;
		DISize *= 2;

		// Create vertex/index buffers
		m_NumVerts = DVSize;
		m_NumIndices = DISize*3;
		Vertex* pVertices = new Vertex[m_NumVerts];
		uint32* pIndices = new uint32[m_NumIndices];

		// Generate verts
		int32 DomeIndex = 0;
		for( int32 i = 0; i < Longitude; i++ )
		{
			//const double dDistribution = (1.0 - exp(-0.5 * (i*10.0) / double(m_DomeSize - 1)));
			//const double MoveXZ = 100.0 * dDistribution * pi / 180.0;
			const float MoveXZ = 100.0f * (i / float( Longitude - 1 )) * Math::pi / 180.0f;

			for( int32 j = 0; j < Latitude; j++ )
			{	
				//const double MoveY = (pi * 2.0) * j / (m_DomeSize - 1) ;
				const float MoveY = Math::pi * j / (Latitude - 1);

				pVertices[DomeIndex].pos.x = sinf( MoveXZ ) * cosf( MoveY  );
				pVertices[DomeIndex].pos.y = cosf( MoveXZ );
				pVertices[DomeIndex].pos.z = sinf( MoveXZ ) * sinf( MoveY  );

				pVertices[DomeIndex].pos *= 10.0f;

				pVertices[DomeIndex].tu = 0.5f / float( Longitude ) + i / float( Longitude );	// [0.5, Texsize-0.5] 
				pVertices[DomeIndex].tv = 0.5f / float( Latitude ) + j / float( Latitude );	// [0.5, Texsize-0.5]

				DomeIndex++;
			}
		}
		for( int32 i = 0; i < Longitude; i++ )
		{
			//const double dDistribution = (1.0 - exp(-0.5 * (i*10.0) / double(m_DomeSize - 1)));
			//const double MoveXZ = 100.0 * dDistribution * pi / 180.0;
			const float MoveXZ = 100.0f * (i / float( Longitude - 1 )) * Math::pi / 180.0f;

			for( int32 j = 0; j < Latitude; j++ )
			{	
				//const double MoveY = (pi * 2.0) * j / (m_DomeSize - 1) ;
				const float MoveY = (Math::pi * 2.0f) - (Math::pi * j / (Latitude - 1));

				pVertices[DomeIndex].pos.x = sinf( MoveXZ ) * cosf( MoveY  );
				pVertices[DomeIndex].pos.y = cosf( MoveXZ );
				pVertices[DomeIndex].pos.z = sinf( MoveXZ ) * sinf( MoveY  );

				pVertices[DomeIndex].pos *= 10.0f;

				pVertices[DomeIndex].tu = 0.5f / float( Longitude ) + i / float( Longitude );	// [0.5, Texsize-0.5] 
				pVertices[DomeIndex].tv = 0.5f / float( Latitude ) + j / float( Latitude );	// [0.5, Texsize-0.5]

				DomeIndex++;
			}
		}

		// Generate faces
		int32 faceCount = 0;
		for( uint16 i = 0; i < Longitude - 1; i++)
		{
			for( uint16 j = 0; j < Latitude - 1; j++)
			{
				pIndices[faceCount] = i * Latitude + j;
				pIndices[faceCount+1] = (i + 1) * Latitude + j;
				pIndices[faceCount+2] = (i + 1) * Latitude + j + 1;

				pIndices[faceCount+3] = (i + 1) * Latitude + j + 1;
				pIndices[faceCount+4] = i * Latitude + j + 1;
				pIndices[faceCount+5] = i * Latitude + j;
				faceCount+=6;
			}
		}

		const uint32 Offset = Latitude * Longitude;
		for( uint16 i = 0; i < Longitude - 1; i++)
		{
			for( uint16 j = 0; j < Latitude - 1; j++)
			{
				pIndices[faceCount] = Offset + i * Latitude + j;
				pIndices[faceCount+1] = Offset + (i + 1) * Latitude + j + 1;
				pIndices[faceCount+2] = Offset + (i + 1) * Latitude + j;

				pIndices[faceCount+3] = Offset + i * Latitude + j + 1;
				pIndices[faceCount+4] = Offset + (i + 1) * Latitude + j + 1;
				pIndices[faceCount+5] = Offset + i * Latitude + j;
				faceCount+=6;
			}
		}

		// Fill the vertex buffer
		D3D11_BUFFER_DESC bd;
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = sizeof( Vertex ) * m_NumVerts;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		InitData.pSysMem = (void*)pVertices;
		hr = _graphics->GetDevice()->CreateBuffer( &bd, &InitData, &m_pVB );
		if( FAILED(hr) )  
			return hr;

		// Fill the index buffer
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = sizeof( uint32 ) * m_NumIndices;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		InitData.pSysMem = (void*)pIndices;
		hr = _graphics->GetDevice()->CreateBuffer( &bd, &InitData, &m_pIB );
		if( FAILED(hr) )
			return hr;

		delete[] pVertices;
		delete[] pIndices;
		
		return S_OK;
	}
}

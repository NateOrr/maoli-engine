//--------------------------------------------------------------------------------------
// File: PipelineResource.cpp
//
// Graphics pipeline resource interface
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "PipelineResource.h"

namespace Maoli
{
	// Ctor
	InputBinding::InputBinding()
	{
		slot = -1;
		shaderType = -1;
	}

	// Ctor
	OutputBinding::OutputBinding()
	{
		boundToPipeline = false;
		index = 0;
		fullyBound = false;
	}

	// Clear an input binding
	void PipeplineResource::ClearInputBinding( uint32 shaderType, int32 slot )
	{
		for ( auto iter = _inputBindings.Begin(); !iter.End(); ++iter )
		{
			InputBinding& binding = *iter;
			if ( binding.shaderType == shaderType && binding.slot == slot )
			{
				_inputBindings.Remove( iter );
				return;
			}
		}
	}

	// Add an input binding
	void PipeplineResource::AddInputBinding( uint32 shaderType, int32 slot )
	{
		InputBinding& binding = _inputBindings.Add();
		binding.shaderType = shaderType;
		binding.slot = slot;
	}

	// Ctor
	PipeplineResource::PipeplineResource()
	{
		_debugName = "NoName";
	}

}

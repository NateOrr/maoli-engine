//--------------------------------------------------------------------------------------
// File: DebugVisualizer.cpp
//
// Debug visualization for the renderer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Terrain.h"

namespace Maoli
{

	// Forward decl
	class Renderer;
	class Terrain;

	class DebugVisualizer
	{
	public:

		// A generic object that can be rendered for debugging
		struct VisualizableObject
		{
			// Ctor
			VisualizableObject();

			Model* mesh;
			int32 submesh;
			bool isWireframe;
			bool submeshOnly;
			Color color[2];
			bool boundingBox;
			Vector3 pos;
			Vector3 boundSize;
			Vector3 position, rotation, scale;
			bool useTransform;
			bool ignoreDepth;
		};

		// Debug line
		struct DebugLine
		{
			Vector3 pos, rot, scale;
			Color color;
			bool ignoreDepth;
		};

		// Debug bounding box
		struct DebugObject
		{
			Matrix transform;
			Color color;
		};

		// Debug bounding box
		struct Billboard
		{
			Vector3 pos;
			Vector3 scale;
			Color color;
			Texture::pointer texture;
		};

		// Ctor
		DebugVisualizer( Renderer* graphics );

		// Setup
		HRESULT Init();

		// Cleanup
		void Release();


		// Rendering
		void Render();


		// Setup shaders
		void BindEffectVariables();

		// Render a mesh preview texture to a file, and return the path to the file
		String RenderMeshPreview( Model* pMesh );

		// Clear the render lists
		void Clear();

		// Adds a mesh to the render list
		void DrawMesh( Model* pMesh, int32 submesh = -1, bool submeshOnly = false, bool wire = true, bool boundingBox = true );
		void DrawMesh( Model* pMesh, const Color& color, bool ignoreDepth = false );
		void DrawMesh( Model* pMesh, const Color& color, const Vector3& pos, const Vector3& rot, const Vector3& scale, bool ignoreDepth = false );

		// Add a box to the render list
		void DrawBox( const Vector3& pos, const Vector3& size, const Color& color );
		void DrawBox( const Matrix& transform, const Color& color );

		// Add a sphere to the render list
		void DrawSphere( const Vector3& pos, float radius, const Color& color );
		void DrawSphere( const Matrix& transform, const Color& color );

		// Add a capsule to the render list
		void DrawCapsule( const Vector3& pos, float height, float radius, const Color& color );
		void DrawCapsule( const Matrix& transform, const Color& color );

		// Draw a billboarded quad
		void DrawBillboard( const Vector3& pos, const Vector3& scale, Texture::pointer texture, const Color& color = Color::White );

		// Draw a line
		void DrawLine( const Vector3& pos, const Vector3& rot, const Vector3& scale, const Color& color, bool ignoreDepth );

		// Add a light to the render list
		inline void VisualizeLight( Light* pLight ) { _lightRenderList.Add( pLight ); }

		// Add a custom object to the render list
		inline void VisualizeObject( VisualizableObject& rt ) { _meshRenderList.Add( rt ); }

		// Draw a selection box
		inline void DrawSelectionBox( float x1, float y1, float x2, float y2 ) { _showSelectionBox = true; _selectionBox = Vector4( x1, y1, x2, y2 ); }

		// Enable/disable "play" mode in the editor
		inline void EnableDemoMode( bool b ) { _demoModeEnabled = b; }


		// Set the radius for the terrain sculpting tool
		inline void SetTerrainWidgetRadius( float f ) { _terrainSculptRadius = f; TerrainWidgetRadiusVariable->SetFloat( f ); }
		inline float GetTerrainWidgetRadius() { return _terrainSculptRadius; }

		// Set the hardness for the terrain sculpting tool
		inline void SetTerrainWidgetHardness( float f ) { _terrainSculptHardness = f; TerrainWidgetHardnessVariable->SetFloat( f ); }
		inline float GetTerrainWidgetHardness() { return _terrainSculptHardness; }

		// Set the strength for the terrain sculpting tool
		inline void SetTerrainWidgetStrength( float f ) { _terrainSculptStrength = f; TerrainWidgetStrengthVariable->SetFloat( f ); }
		inline float GetTerrainWidgetStrength() { return _terrainSculptStrength; }

		// Set the detail level for the terrain sculpting tool
		void SetTerrainWidgetDetail( int32 f );

		// Set the delta for the terrain sculpting tool
		void SetTerrainWidgetDelta( float f ) { _terrainSculptDelta = f; }

		// Set the terrain sculpting mode (push, pull, etc)
		void SetTerrainSculptMode( SCULPT_TYPE mode ) { _terrainSculptingMode = mode; }

		// Get the sculpt mode
		inline SCULPT_TYPE GetTerrainSculptMode() { return _terrainSculptingMode; }

		// Terrain sculpting
		inline void QueueTerrainSculpt() { _needsTerrainSculpt = true; }

		// Virtual texture painting
		void QueueTerrainPaint();

		// Handle vt painting update
		void HandleTerrainPaint();



		// Check if grid visualization is enables
		inline bool IsGridEnabled() { return _showGrid; }

		// Enable grid visualization
		inline void EnableGridLines( bool enable ) { _showGrid = enable; }

		// Render lights
		inline void DrawLights() { _showLights = true; }

		// Render a preview for a material
		void DrawMaterialPreview( MaterialBinding::pointer mat );

		// Show the terrain widget
		inline void DrawTerrainWidget() { _showTerrainWidget = true; }

	private:

		// Perform virtual texture painting
		void DoVirtualTexturePaint();

		// Perform terrain sculpting
		void DoTerrainSculpt( Vector3 pickPoint );

		// Render the terrain sculpt tool
		void RenderTerrainWidget();

		// Show the selection box
		void ShowSelectionBox();

		// Render lines
		void RenderLines();

		// Render boxes
		void RenderBoxes();

		// Render spheres
		void RenderSpheres();

		// Render the capsules
		void RenderCapsules();

		// Render the billboarded quads
		void RenderBillboards();

		// Render a material preview texture to a file
		void RenderMaterialPreviews();


		Renderer*					_graphics;					// Parent graphics engine
		Texture::pointer			_lightTexture;				// Material for the light billboard
		int32			 				_selectedSumMeshIndex;		// Index of the submesh for the most recent picked mesh
		Buffer<Vertex>::pointer		_lightBorderVertexBuffer;	// Vertex buffer to store the lines for the light borders
		Buffer<Vector3>::pointer	_lineVetexBuffer;			// A single line buffer
		Buffer<Vector3>::pointer	_boxOutlineVertexBuffer;	// A buffer for a box made out of lines
		Buffer<Vector3>::pointer	_sphereVertexBuffer;		// Sphere visualization
		Buffer<Vector3>::pointer	_capsuleVertexBuffer;		// capsule visualization
		uint32						_spherePrimitiveCount;		// Number of lines to draw
		uint32						_capsulePrimitiveCount;		// Number of lines to draw
		bool						_demoModeEnabled;			// Demo mode
		Array<Model*>				_demoModeObjects;			// For demo mode
		Array<Light*>				_demoModeLights;			// For demo mode
		float						_lightRenderingScale;		// Size scale for rendering lights in debug mode
		bool						_showGrid;					// True if grid visualization is enabled
		bool						_wireframeMode;				// True if wireframe mode is enabled
		bool						_needsVTPainting;			// True when a virtual texture paint operation is requested
		bool						_showLights;				// True to render lights
		bool						_showTerrainWidget;			// True to show the terrain sculpting tool widget
		Array<VisualizableObject>	_meshRenderList;			// Render queue for meshes
		Array<DebugLine>			_lineRenderList;
		Array<DebugObject>			_boxRenderList;
		Array<Billboard>			_billboards;
		Array<DebugObject>			_sphereRenderList;
		Array<DebugObject>			_capsuleRenderList;
		Array<Light*>				_lightRenderList;			// Render queue for lights
		Vector4						_selectionBox;				// Selection box location
		bool						_showSelectionBox;			// True to render the box this frame
		Buffer<Vertex>::pointer		_quadVertexBuffer;			// Selection box vb
		Model*						_sphereMesh;

		Array<MaterialBinding::pointer> _materialPreviews;		// Materials that need previews rendered


		// Terrain sculpting
		SCULPT_TYPE				_terrainSculptingMode;
		float					_terrainSculptDelta;
		float					_terrainSculptRadius;
		float					_terrainSculptHardness;
		float					_terrainSculptStrength;
		int32						_terrainSculptLayer;
		bool 					_needsTerrainSculpt;


		// Material preview texture rendering
		DepthStencil::pointer	_materialPreviewDepthStencil;
		Texture::pointer		_materialPreviewTarget;
		Model*					_materialPreviewMesh;

		// Mesh preview rendering
		Texture::pointer		_meshPreviewTargetMSAA;
		DepthStencil::pointer	_meshPreviewDepthStencilMSAA;
		Texture::pointer		_meshPreviewTarget;


		// Effect variables
		ShaderScalar*			TerrainWidgetRadiusVariable;
		ShaderScalar*			TerrainWidgetHardnessVariable;
		ShaderScalar*			TerrainWidgetStrengthVariable;
		ShaderScalar*			_materialTextureFlagsVariable;
		ShaderVector*			_materialDiffuseVariable;
		ShaderResource*			_materialTexturesVariable;
		ShaderMatrix*			_worldMatrixVariable;
		ShaderVector*			_mouseIntersectVariable;

		// Effect passes
		EffectPass*			_forwardPass;
		EffectPass*			_forwardNoDepthPass;
		EffectPass*			_gridPass;
		EffectPass*			_terrainWidgetPass;
		EffectPass*			_mouseIntersectPass;
		EffectPass*			_selectionBoxPass;
		EffectPass*			_positionOnlyWireframePass;		// Wireframe render
		EffectPass*			_positionOnlyWireframeSkinnedPass;		// Wireframe render
		EffectPass*			_positionOnlyColorPass;			// Material color
		EffectPass*			_materialPreviewPass;			// Material preview rendering
		DepthStencilState*	_ignoreDepthState;				// Ignore depth stencil

	};
}




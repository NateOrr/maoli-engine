//--------------------------------------------------------------------------------------
// File: TerrainSculpting.cpp
//
// Terrain sculpting functions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Terrain.h"
#include "Graphics/Renderer.h"

namespace Maoli
{

	// Default layer
	Terrain::PaintLayer::PaintLayer( MaterialBinding::pointer material )
	{
		Slope[0] = 0.0f;
		Slope[1] = 100000.0f;
		Elevation[0] = -100000.0f;
		Elevation[1] = 100000.0f;
		materialBinding = material;
		Mask = nullptr;
		PaintMode = PAINT_TILE;
		MaskMode = PAINT_PROJECT;
		MaskInvert = false;
		MaskAbsolute = false;
		MaskModulate = false;
		MaskColor = false;
		MaskInvertColor = false;
	}

	// Load a layer normalmap
	void Terrain::LoadLayerMask( const char* file, int32 i )
	{
		Texture::pointer tex = _graphics->CreateTextureFromFile( file );
		if ( tex )
		{
			ClearLayerMask( i );
			m_Layers[i]->Mask = tex;
		}
	}


	// Get a layer mask file name
	const String& Terrain::GetLayerMaskName( int32 i )
	{
		if ( m_Layers[i]->Mask )
			return m_Layers[i]->Mask->GetName();
		else
			return String::none;
	}


	// Clear a mask texture
	void Terrain::ClearLayerMask( int32 i )
	{
		m_Layers[i]->Mask = nullptr;
	}

	// Attaches a material to a layer
	// Note: matID is only used for gui
	void Terrain::AttachMaterial( MaterialBinding::pointer material, int32 i )
	{
		m_Layers[i]->materialBinding = material;
		BindLayer( i );
		UpdateLayerMaterials();
	}



	// Binds the textures and normal maps
	void Terrain::BindLayer( int32 i, bool mask )
	{
		m_CurrentLayer = i;

		// Masking texture
		if ( mask && m_Layers[i]->Mask )
		{
			PaintMaskVariable->SetResource( *m_Layers[i]->Mask );
			PaintMaskFlagVariable->SetBool( true );
		}
		else
		{
			PaintMaskVariable->SetResource( nullptr );
			PaintMaskFlagVariable->SetBool( false );
		}

		// Abort if no valid material
		if ( !m_Layers[i]->materialBinding )
			return;

		// Bind the textures and the usage flags
		_graphics->SetMaterial( m_Layers[i]->materialBinding );

		// Props
		if ( m_Layers[i]->materialBinding->GetTexture( TEX_DIFFUSE ) != nullptr )
		{
			int32 size[2];
			size[0] = m_Layers[i]->materialBinding->GetTexture( TEX_DIFFUSE )->GetWidth();
			size[1] = m_Layers[i]->materialBinding->GetTexture( TEX_DIFFUSE )->GetHeight();
			PaintLayerSizeVariable->SetIntVector( size );
		}
		PaintModeVariable->SetInt( (int32)m_Layers[i]->PaintMode );
		PaintMaskModeVariable->SetInt( (int32)m_Layers[i]->MaskMode );
		PaintMaskInvertVariable->SetBool( m_Layers[i]->MaskInvert );
		PaintMaskInvertColorVariable->SetBool( m_Layers[i]->MaskInvertColor );
		PaintMaskAbsoluteVariable->SetBool( m_Layers[i]->MaskAbsolute );
		PaintMaskColorVariable->SetBool( m_Layers[i]->MaskColor );
		PaintMaskModulateVariable->SetBool( m_Layers[i]->MaskModulate );
		PaintLayerIDVariable->SetFloat( (float)m_CurrentLayer );
	}


	// Update the layer materials in the shader
	void Terrain::UpdateLayerMaterials()
	{
		// Make sure the texture arrays match material textures
		memset( &cbTerrainMaterial, 0, sizeof(cbTerrainMaterial) );
		for ( int32 i = 0; i < m_MaxLayers; i++ )
		{
			if ( m_Layers[i]->materialBinding )
			{
				// Get the srvs for textures
				m_TerrainDiffuse[i] = m_Layers[i]->materialBinding->GetTexture( TEX_DIFFUSE );
				m_TerrainNormal[i] = m_Layers[i]->materialBinding->GetTexture( TEX_NORMAL );
				m_TerrainSpecular[i] = m_Layers[i]->materialBinding->GetTexture( TEX_SPECULAR );

				// Material data
				const Vector3& baseColor = m_Layers[i]->materialBinding->GetMaterial()->GetBaseColor();
				cbTerrainMaterial.layers[i].color = Color( baseColor.x, baseColor.y, baseColor.z, 1.0f );
				cbTerrainMaterial.layers[i].roughness = 0.5f;
				cbTerrainMaterial.layers[i].texFlag = m_Layers[i]->materialBinding->GetMaterial()->GetTextureFlag();
				cbTerrainMaterial.layers[i].texScale = Vector2( 160, 160 );
			}
		}

		// Primary layer material
		if ( m_Layers[0]->materialBinding )
		{
			cbTerrainMaterial.coneStepMapping = false;
			cbTerrainMaterial.csmScale = 0.02f;			
			cbTerrainMaterial.csmSamples = 25;			
		}

		// Set the cb
		TerrainMaterialCB->Fill( &cbTerrainMaterial );
	}


	// Binds all paint layer materials for terrain rendering
	// --------------------------------------------------------------------------------------
	void Terrain::BindLayerMaterials()
	{
		// Set the textures
		TerrainDiffuseVariable->SetResourceArray( m_TerrainDiffuse, 0, m_MaxLayers );
		TerrainNormalVariable->SetResourceArray( m_TerrainNormal, 0, m_MaxLayers );
		TerrainRoughnessVariable->SetResourceArray( m_TerrainNormal, 0, m_MaxLayers );
	}

	// Paint to the blend map
	void Terrain::PaintTerrain( const Vector3& pos, float radius )
	{
		// Only paint with a valid layer
		if ( !m_Layers[m_CurrentLayer]->materialBinding )
			return;

		// Get the positions in the region
		float halfSize = m_Size*0.5f;
		float invSize = 1.0f / m_Size;
		float minX = (halfSize + pos.x - radius);
		float minY = (halfSize + pos.z - radius);
		float maxX = (halfSize + pos.x + radius);
		float maxY = (halfSize + pos.z + radius);

		// Clamp to the terrain size
		Math::Clamp( minX, 0.0f, (float)m_Size );
		Math::Clamp( minY, 0.0f, (float)m_Size );
		Math::Clamp( maxX, 0.0f, (float)m_Size );
		Math::Clamp( maxY, 0.0f, (float)m_Size );

		// Convert to tex coords
		minX *= invSize;
		minY *= invSize;
		maxX *= invSize;
		maxY *= invSize;

		// Set the paint range
		Vector4 paintRegion( minX, minY, maxX - minX, maxY - minY );
		PaintRegionVariable->SetFloatVector( (float*)&paintRegion );

		// Set the texture uav and run the CS
		HeightmapSizeVariable->SetFloat( (float)m_Size );
		MouseIntersectVariable->SetFloatVector( (float*)&pos );
		_paintingUAVVariable->SetResource( *m_Blendmap );
		PaintTerrainPass->Apply();
		_graphics->GetContext()->Dispatch( 9, 9, 1 );

		// Finish the mip chain
		m_Blendmap->GenerateMips();
	}

	// Terrain sculpting
	void Terrain::Sculpt( Vector3 center, float radius, float hardness, float strength, float delta, int32 detail, SCULPT_TYPE mode )
	{
		// Determine the region we need to use
		Vector2 pos = Vector2( floorf( center.x ), floorf( center.z ) ) + Vector2( 0.5f, 0.5f )*(float)m_Size;
		Vector2 minBounds = pos - Vector2( radius, radius );
		Vector2 maxBounds = pos + Vector2( radius, radius );
		Math::Clamp( minBounds, 0, (float)(m_Size - 1) );
		Math::Clamp( maxBounds, 0, (float)(m_Size - 1) );
		if ( mode == SCULPT_LOWER )
			delta *= -1;

		// Map the heightmap
		if ( mode != SCULPT_PAINT )
		{
			D3D11_MAPPED_SUBRESOURCE mappedTexture;
			float* pTexels;
			_graphics->GetContext()->Map( m_StagingHeightmap, 0, D3D11_MAP_READ_WRITE, 0, &mappedTexture );
			pTexels = (float*)mappedTexture.pData;

			// Process the sculpting
			for ( int32 row = (int32)minBounds.y; row < (int32)maxBounds.y; row++ )
			{
				UINT rowStart = row * mappedTexture.RowPitch / 4;
				for ( int32 col = (int32)minBounds.x; col < (int32)maxBounds.x; col++ )
				{
					// Convert this index to world space coords
					Vector2 worldPos;
					worldPos.x = (float)col;
					worldPos.y = (float)row;

					// Only operate within the radius
					float dist = (worldPos - pos).Length();
					if ( dist >= radius )
						continue;

					// Smooth strength falloff
					float s = strength * 2;
					if ( dist > hardness )
						s *= (radius - dist) / (radius - hardness);

					// Modify the heightmap
					if ( mode == SCULPT_SMOOTH )
					{
						// Get the nearby heights and average them
						int32 count = 0;
						float avgHeight = 0;
						for ( int32 y = -2; y < 2; y++ )
							for ( int32 x = -2; x < 2; x++, count++ )
							{
								int32 rx = static_cast<int32>(col)+x;
								int32 ry = static_cast<int32>(row)+y;
								Math::Clamp( rx, 0, m_Size - 1 );
								Math::Clamp( ry, 0, m_Size - 1 );
								avgHeight += (float)m_Height[ry*m_Size + rx];
							}
						avgHeight /= count;
						pTexels[rowStart + col] = float( Math::Lerp( (float)m_Height[row*m_Size + col], avgHeight, s*0.03f ) );
					}
					else
						pTexels[rowStart + col] = pTexels[rowStart + col] + (float)(delta*s);

					// Update the height
					m_Height[row*m_Size + col] = pTexels[rowStart + col];

					// Track the min/max
					float h = m_Height[row*m_Size + col] * m_HeightScale;
					m_HeightExtents.x = std::min( m_HeightExtents.x, h );
					m_HeightExtents.y = std::max( m_HeightExtents.y, h );
				}
			}
			_graphics->GetContext()->Unmap( m_StagingHeightmap, 0 );

			// Copy the subregion back to the main heightmap
			D3D11_BOX sourceRegion;
			sourceRegion.left = (UINT)minBounds.x;
			sourceRegion.top = (UINT)minBounds.y;
			sourceRegion.right = (UINT)maxBounds.x;
			sourceRegion.bottom = (UINT)maxBounds.y;
			sourceRegion.front = 0;
			sourceRegion.back = 1;
			_graphics->GetContext()->CopySubresourceRegion( m_Heightmap->GetTex()[0], 0, (UINT)minBounds.x, (UINT)minBounds.y, 0,
				m_StagingHeightmap, 0, &sourceRegion );

			// Flag all clipmaps to be updated
			m_FlagForUpdate = true;
		}

	}



	// Reverses the last sculpt action
	void Terrain::UndoSculpt()
	{
		if ( m_UndoStack.IsEmpty() )
			return;

		// Perform the undo
		SculptRegion r = m_UndoStack.Pop();
		ApplyUndo( r );

		// Force a clipmap update
		m_FlagForUpdate = true;

		// Push to the redo stack
		m_RedoStack.Push( r );
	}


	// Re-applies the last undone action
	void Terrain::RedoSculpt()
	{
		if ( m_RedoStack.IsEmpty() )
			return;

		// Perform the redo
		SculptRegion r = m_RedoStack.Pop();
		ApplyUndo( r );

		// Force a clipmap update
		m_FlagForUpdate = true;

		// Push to the undo stack
		m_UndoStack.Push( r );
	}


	// Actually applies the sculpt undo to the maps
	void Terrain::ApplyUndo( SculptRegion &r )
	{
		D3D11_BOX localRegion;
		localRegion.left = 0;
		localRegion.right = r.region.right - r.region.left;
		localRegion.front = 0;
		localRegion.back = 1;
		localRegion.top = 0;
		localRegion.bottom = r.region.bottom - r.region.top;

		Vector2 minBounds( (float)r.region.left, (float)r.region.top );
		Vector2 maxBounds( (float)r.region.right, (float)r.region.bottom );

		{
			_graphics->GetContext()->CopySubresourceRegion( m_StagingHeightmap, 0, r.region.left, r.region.top, 0,
				r.tex, 0, &localRegion );

			_graphics->GetContext()->CopySubresourceRegion( m_Heightmap->GetTex()[0], 0, r.region.left, r.region.top, 0,
				r.tex, 0, &localRegion );

			// Update the height values
			D3D11_MAPPED_SUBRESOURCE mappedTex;
			_graphics->GetContext()->Map( m_StagingHeightmap, 0, D3D11_MAP_READ_WRITE, 0, &mappedTex );
			float* pTexels = (float*)mappedTex.pData;
			for ( UINT row = (UINT)minBounds.y; row < (UINT)maxBounds.y; row++ )
			{
				UINT rowStart = row * mappedTex.RowPitch / 2;
				for ( UINT col = (UINT)minBounds.x; col < (UINT)maxBounds.x; col++ )
				{
					// Update the height
					m_Height[row*m_Size + col] = pTexels[rowStart + col];

					// Track the min/max
					float h = m_Height[row*m_Size + col] * m_HeightScale;
					m_HeightExtents.x = std::min( m_HeightExtents.x, h );
					m_HeightExtents.y = std::max( m_HeightExtents.y, h );
				}
			}
			_graphics->GetContext()->Unmap( m_StagingHeightmap, 0 );
		}
	}



	// Used to cache a region onto the undo stack
	void Terrain::CacheSculptRegion( Vector3 pickPoint, float radius )
	{
		// Determine the region we need to use
		Vector2 pos = Vector2( floorf( pickPoint.x ), floorf( pickPoint.z ) ) + Vector2( 0.5f, 0.5f )*(float)m_Size;
		Vector2 minBounds = pos - Vector2( radius, radius );
		Vector2 maxBounds = pos + Vector2( radius, radius );
		Math::Clamp( minBounds, 0, (float)(m_Size - 1) );
		Math::Clamp( maxBounds, 0, (float)(m_Size - 1) );

		// Copy the subregion back to the main heightmap
		D3D11_BOX sourceRegion;
		sourceRegion.left = (UINT)minBounds.x;
		sourceRegion.top = (UINT)minBounds.y;
		sourceRegion.right = (UINT)maxBounds.x;
		sourceRegion.bottom = (UINT)maxBounds.y;
		sourceRegion.front = 0;
		sourceRegion.back = 1;

		// Cache this texture onto the undo-stack
		ID3D11Texture2D* pTexToCache;
		D3D11_TEXTURE2D_DESC td;
		pTexToCache = m_StagingHeightmap;

		SculptRegion region;
		region.region = sourceRegion;

		pTexToCache->GetDesc( &td );
		td.MipLevels = 1;
		td.Width = (UINT)(maxBounds.x - minBounds.x);
		td.Height = (UINT)(maxBounds.y - minBounds.y);
		_graphics->GetDevice()->CreateTexture2D( &td, nullptr, &region.tex );
		_graphics->GetContext()->CopySubresourceRegion( region.tex, 0, 0, 0, 0,
			pTexToCache, 0, &sourceRegion );

		// Clear the redo stack
		while ( !m_RedoStack.IsEmpty() )
		{
			SculptRegion r = m_RedoStack.Pop();
			m_UndoMemUsage -= r.mem;
			r.tex->Release();
		}

		// In order to cache this, make sure the memory usage is within limits
		region.mem = (td.Width*td.Height * 2) / 1000000.0;
		/*while(m_UndoMemUsage+region.mem > m_UndoMaxMem && !m_UndoStack.IsEmpty())
		{
		// We need to free some memory from the back of the stack
		SculptRegion r = m_UndoStack.PopBack();
		m_UndoMemUsage -= r.mem;
		r.tex->Release();
		}*/

		// Add to the stack
		m_UndoMemUsage += region.mem;
		m_UndoStack.Push( region );
	}
}

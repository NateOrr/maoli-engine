//--------------------------------------------------------------------------------------
// File: Water.h
//
// Ocean rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Water.h"
#include "Graphics/Renderer.h"
#include "fftw3.h"

namespace Maoli
{
	// Ctor
	Ocean::Ocean( Renderer* graphics )
	{
		_graphics = graphics;
		_vertexBuffer = nullptr;
		_indexBuffer = nullptr;
		_numIndices = 0;
		_gridSize = 0;
		_fftHoPos = nullptr;
		_fftHoNeg = nullptr;
		_fftIn = nullptr;
		_fftInDX = nullptr;
		_fftInDZ = nullptr;
		_fftHeight = nullptr;
		_fftDisplacementX = nullptr;
		_fftDisplacementZ = nullptr;
		_waterLevel = 0;
		_windVelocity = 0;
		_waveFacetSize = 0;
		_waveConstant = 0;
		_waveGravity = 0;
		_waveLength = 0;
		_waveHeightScale = 0;
		_waveDisplacementScale = 0;
		_normalMaps[0] = _normalMaps[1] = nullptr;
	}


	// Dtor
	Ocean::~Ocean()
	{
		Release();
	}


	// Sets up for water rendering
	HRESULT Ocean::Init()
	{
		// Setup effect variables
		BindEffectVariables();

		// Setup some default values
		_windDirection = Vector2( -0.5f, -0.5f );
		_windVelocity = 12.0f;
		_gridSize = 128;
		_waveFacetSize = 0.1f;
		_waveConstant = 0.25f;
		_waveGravity = 9.81f;
		_waveLength = 1.0f;
		_waveHeightScale = 1.0f;
		_waveDisplacementScale = 1.0f;

		// Load normal maps
		if ( !(_normalMaps[0] = _graphics->CreateTextureFromFile( "Resources\\Textures\\waternormal.jpg" )) )
			return E_FAIL;
		if ( !(_normalMaps[1] = _graphics->CreateTextureFromFile( "Resources\\Textures\\waternormal2.jpg" )) )
			return E_FAIL;

		// Set the textures in the material
		_materialBinding = _graphics->CreateMaterialBinding( "Water" );
		_materialBinding->SetTexture( _normalMaps[0], TEX_NORMAL );
		_materialBinding->SetTexture( _normalMaps[1], TEX_SPECULAR );

		// Generate a grid of water
		float w = 1024, h = 1024;
		int32 numVerts = (int32)(w*h), numFaces = (int32)((w - 1)*(h - 1) * 6);
		_numIndices = numFaces;
		Vertex* pVerts = new Vertex[numVerts];
		uint32* pFaces = new uint32[numFaces];
		int32 vindex = 0, findex = 0;
		for ( float y = 0; y < h; y++ )
		{
			for ( float x = 0; x < w; x++, vindex++ )
			{
				// Vertex grid point
				pVerts[vindex].pos = Vector3( x - w / 2, 0, y - h / 2 );
				pVerts[vindex].tu = x / _gridSize;
				pVerts[vindex].tv = y / _gridSize;

				// Quad
				if ( x < w - 1 && y < h - 1 )
				{
					pFaces[findex++] = (uint32)(y*h + x);
					pFaces[findex++] = (uint32)((y + 1)*h + x);
					pFaces[findex++] = (uint32)((y + 1)*h + (x + 1));
					pFaces[findex++] = (uint32)((y + 1)*h + (x + 1));
					pFaces[findex++] = (uint32)(y*h + x + 1);
					pFaces[findex++] = (uint32)(y*h + x);
				}
			}
		}

		// Fill the vertex buffer
		_vertexBuffer = _graphics->CreateVertexBuffer<Vertex>( numVerts, D3D11_USAGE_IMMUTABLE, pVerts );
		if ( !_vertexBuffer )
		{
			M_ERROR( "Failed to create water vertex buffer" );
			return E_FAIL;

		}

		// Fill the index buffer
		_indexBuffer = _graphics->CreateIndexBuffer<uint32>( numFaces, D3D11_USAGE_IMMUTABLE, pFaces );
		if ( !_indexBuffer )
		{
			M_ERROR( "Failed to create water index buffer" );
			return E_FAIL;

		}

		delete[] pVerts;
		delete[] pFaces;

		// Create a wave field data
		_waveField = _graphics->CreateTexture( _gridSize, _gridSize, DXGI_FORMAT_R32G32B32_FLOAT, 1, nullptr, D3D11_USAGE_DYNAMIC, false, true );
		if ( !_waveField )
		{
			M_ERROR( "m_WaveField falied" );
			return E_FAIL;
		}
		_waveNormals = _graphics->CreateTexture( _gridSize, _gridSize, DXGI_FORMAT_R8G8B8A8_UNORM, 1, nullptr, D3D11_USAGE_DYNAMIC, false, true );
		if ( !_waveNormals )
		{
			M_ERROR( "m_WaveNormals falied" );
			return E_FAIL;
		}
		_fftHoPos = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );
		_fftHoNeg = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );
		_fftIn = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );
		_fftInDX = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );
		_fftInDZ = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );
		_fftHeight = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );
		_fftDisplacementX = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );
		_fftDisplacementZ = (Math::Complex*)fftw_malloc( sizeof(Math::Complex) * _gridSize*_gridSize );

		// Build the wave field
		CreateWaveField();

		// Set default colors
		SetBackscatterColor( 0.2f, 0.2f, 0.2f );
		SetExtinctionColor( 0.2f, 0.2f, 0.2f );

		return S_OK;
	}

	// Wave field setup.  Uses Phillips spectrum from Tessendorf's paper "Simulating Ocean Water"
	void Ocean::CreateWaveField()
	{
		// Setup constants
		const double      V = _windVelocity;	// Wind velocity (_/s)
		const double      g = _waveGravity;		// Gravitational constant
		const Vector2	  W = _windDirection;	// Wind direction
		const double      A = _waveConstant;	// Arbitrary wave constant
		const int32         M = _gridSize;		// Grid size
		const int32         N = _gridSize;		// Grid size
		const double      L = (V*V) / g;		// L constant
		const double     lx = _waveLength;		// Wavelength (m)
		const double     lz = _waveLength;		// Wavelength (m)

		// Generate initial grid
		int32 index = 0;
		for ( int32 m = 1; m < M + 1; m++ )
		{
			for ( int32 n = 1; n < N + 1; n++, index++ )
			{
				// K
				double kx = (2.0*Math::pi*n) / lx;
				double kz = (2.0*Math::pi*m) / lz;
				double k = sqrt( kx*kx + kz*kz );
				if ( k == 0 ) k = 1;
				kx /= k;
				kz /= k;

				double    kL2 = (k*L)*(k*L);					// (kL)^2
				if ( kL2 == 0 ) kL2 = 1;
				double     k4 = k*k*k*k;						// k^4
				if ( k4 == 0 )  k4 = 1;
				double      P = exp( -1.0f / kL2 ) / k4;			// Phillips factor

				// (K dot W)^2
				double KW2 = kx*(double)W.x + kz*(double)W.y;
				KW2 = KW2*KW2;

				// Phillips spectrum for Ho(K)      (See Tessendorf Paper)
				double ho = A * P * KW2;
				ho = sqrt( ho / 2.0 );
				_fftHoPos[index].real = Math::ComputeRandomGauss( 0, 1 )*ho;
				_fftHoPos[index].imaginary = Math::ComputeRandomGauss( 0, 1 )*ho;

				// Now do Ho(-K)
				kx = -kx;
				kz = -kz;
				KW2 = kx*(double)W.x + kz*(double)W.y;
				KW2 = KW2*KW2;
				ho = P * KW2;
				ho = sqrt( ho / 2.0 );
				_fftHoNeg[index].real = Math::ComputeRandomGauss( 0, 1 )*ho;
				_fftHoNeg[index].imaginary = Math::ComputeRandomGauss( 0, 1 )*ho;
			}
		}
	}

	// Wave simulation.  Uses a FFT based on Tessendorf's paper "Simulating Ocean Water"
	void Ocean::Update( float dt )
	{
		// Constants
		const double      g = _waveGravity;				// Gravitational constant
		const double     lx = _waveLength;					// Wavelength (m)
		const double     lz = _waveLength;					// Wavelength (m)
		const int32         M = _gridSize;				// Grid size
		const int32         N = _gridSize;				// Grid size

		// Get current time in sec
		double t = timeGetTime()*0.001;

		// Animate the wave data with FFT
		int32 index = 0;
		double ax[2], ay[2];
		for ( int32 m = 1; m < M + 1; m++ )
		{
			for ( int32 n = 1; n < N + 1; n++, index++ )
			{
				// K
				double kx = (2.0*Math::pi*n) / lx;
				double kz = (2.0*Math::pi*m) / lz;
				double k = sqrt( kx*kx + kz*kz );
				if ( k == 0 ) k = 1;
				kx /= k;
				kz /= k;

				// w(k)^2 = g*k
				double wt = sqrt( g*k ) * t;

				// H(K,t) => FFT( H~(K) = ho(K)*exp(i*w(k)*t) + ho(K-)*exp(-i*w(k)*t) )
				ax[0] = _fftHoPos[index].real;
				ax[1] = _fftHoPos[index].imaginary;
				ay[0] = _fftHoNeg[index].real;
				ay[1] = _fftHoNeg[index].imaginary;
				_fftIn[index].real = (ax[0] + ay[0]) * cos( wt ) - (ax[1] + ay[1]) * sin( wt );
				_fftIn[index].imaginary = (ax[1] - ay[1]) * cos( -wt ) + (ax[0] - ay[0]) * sin( -wt );

				// D(X,t) => FFT( -i*(K/k)*H~(K) )
				_fftInDX[index].real = -_fftIn[index].imaginary*kx;
				_fftInDX[index].imaginary = _fftIn[index].real*kx;
				_fftInDZ[index].real = -_fftIn[index].imaginary*kz;
				_fftInDZ[index].imaginary = _fftIn[index].real*kz;
			}
		}

		// Perform the FFT
		fftw_plan p = fftw_plan_dft_2d( _gridSize, _gridSize,
			reinterpret_cast<fftw_complex*>(_fftIn), reinterpret_cast<fftw_complex*>(_fftHeight), FFTW_BACKWARD, FFTW_ESTIMATE );
		fftw_execute( p );
		fftw_destroy_plan( p );

		// Perform the FFT's
		fftw_plan p2 = fftw_plan_dft_2d( _gridSize, _gridSize,
			reinterpret_cast<fftw_complex*>(_fftInDX), reinterpret_cast<fftw_complex*>(_fftDisplacementX), FFTW_BACKWARD, FFTW_ESTIMATE );
		fftw_execute( p2 );
		fftw_destroy_plan( p2 );
		fftw_plan p3 = fftw_plan_dft_2d( _gridSize, _gridSize,
			reinterpret_cast<fftw_complex*>(_fftInDZ), reinterpret_cast<fftw_complex*>(_fftDisplacementZ), FFTW_BACKWARD, FFTW_ESTIMATE );
		fftw_execute( p3 );
		fftw_destroy_plan( p3 );


		// Map the wave field into the texture for the shader
		D3D11_MAPPED_SUBRESOURCE mappedWave;
		_graphics->GetContext()->Map( _waveField->GetTex()[0], 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedWave );
		float* mappedWaveData = (float*)mappedWave.pData;
		index = 0;
		for ( int32 j = 0; j < _gridSize; j++ )
			for ( int32 i = 0; i < _gridSize; i++, index++ )
			{
				mappedWaveData[index * 3] = (float)_fftDisplacementX[index].real * _waveDisplacementScale;
				mappedWaveData[index * 3 + 1] = (float)_fftHeight[index].real * _waveHeightScale;
				mappedWaveData[index * 3 + 2] = (float)_fftDisplacementZ[index].real * _waveDisplacementScale;
			}
		_graphics->GetContext()->Unmap( _waveField->GetTex()[0], 0 );

		// Build the normal map
		D3DX11ComputeNormalMap( _graphics->GetContext(), _waveField->GetTex()[0], 0, D3DX11_CHANNEL_GREEN, 1.0f, _waveNormals->GetTex()[0] );
	}


	// Frees memory used by the water system
	void Ocean::Release()
	{
		_vertexBuffer = nullptr;
		_indexBuffer = nullptr;
		_waveField = nullptr;
		_waveNormals = nullptr;

		_materialBinding = nullptr;

		fftw_free( _fftHoPos ); _fftHoPos = nullptr;
		fftw_free( _fftHoNeg ); _fftHoNeg = nullptr;
		fftw_free( _fftIn ); _fftIn = nullptr;
		fftw_free( _fftInDX ); _fftInDX = nullptr;
		fftw_free( _fftInDZ ); _fftInDZ = nullptr;
		fftw_free( _fftHeight ); _fftHeight = nullptr;
		fftw_free( _fftDisplacementX ); _fftDisplacementX = nullptr;
		fftw_free( _fftDisplacementZ ); _fftDisplacementZ = nullptr;
	}


	// Set the water level
	void Ocean::SetWaterLevel( float height )
	{
		_waterLevel = height;
	}

	// Set the wind direction
	void Ocean::SetWindDirection( float x, float z )
	{
		_windDirection = Vector2( x, z );
		CreateWaveField();
	}

	// Set the wind direction
	void Ocean::SetWindDirection( const Vector2& value )
	{
		SetWindDirection( value.x, value.y );
	}


	// Set the wind velocity
	void Ocean::SetWindVelocity( float v )
	{
		_windVelocity = v;
		CreateWaveField();
	}


	// Set the wave facet size
	void Ocean::SetWaveFacetSize( float f )
	{
		_waveFacetSize = f;
		CreateWaveField();
	}


	// Set the wave constant
	void Ocean::SetWaveConstant( float f )
	{
		_waveConstant = f;
		CreateWaveField();
	}


	// Set the wave gravity
	void Ocean::SetWaveGravity( float f )
	{
		_waveGravity = f;
		CreateWaveField();
	}


	// Set the wave length
	void Ocean::SetWaveLength( float f )
	{
		_waveLength = f;
		CreateWaveField();
	}


	// Set the extinction color
	void Ocean::SetExtinctionColor( float r, float g, float b )
	{
		_extinctionColor.Set( r, g, b );
	}

	// Set the extinction color
	void Ocean::SetExtinctionColor( const Color& value )
	{
		SetExtinctionColor( value.r, value.g, value.b );
	}

	// Set the backscatter color
	void Ocean::SetBackscatterColor( float r, float g, float b )
	{
		_backScatterColor.Set( r, g, b );
	}

	// Set the backscatter color
	void Ocean::SetBackscatterColor( const Color& value )
	{
		SetBackscatterColor( value.r, value.g, value.b );
	}


	// Setup effect variables
	void Ocean::BindEffectVariables()
	{
		WaterHeightVariable = _graphics->GetEffect()->GetVariableByName( "g_WaterLevel" )->AsScalar();
		WaterExtinctionColorVariable = _graphics->GetEffect()->GetVariableByName( "g_WaterExtinctionColor" )->AsVector();
		WaterBackscatterColorVariable = _graphics->GetEffect()->GetVariableByName( "g_WaterBackscatterColor" )->AsVector();
	}

	// Binary serialize
	void Ocean::Export( std::ofstream& fout )
	{
		Maoli::Serialize( fout, &_waterLevel );
		Maoli::Serialize( fout, &_windDirection );
		Maoli::Serialize( fout, &_windVelocity );
		Maoli::Serialize( fout, &_waveFacetSize );
		Maoli::Serialize( fout, &_waveConstant );
		Maoli::Serialize( fout, &_waveGravity );
		Maoli::Serialize( fout, &_waveLength );
		Maoli::Serialize( fout, &_waveHeightScale );
		Maoli::Serialize( fout, &_waveDisplacementScale );
		Maoli::Serialize( fout, &_extinctionColor );
		Maoli::Serialize( fout, &_backScatterColor );
	}

	// Binary file import
	void Ocean::Import( std::ifstream& fin )
	{
		Maoli::Deserialize( fin, &_waterLevel );
		Maoli::Deserialize( fin, &_windDirection );
		Maoli::Deserialize( fin, &_windVelocity );
		Maoli::Deserialize( fin, &_waveFacetSize );
		Maoli::Deserialize( fin, &_waveConstant );
		Maoli::Deserialize( fin, &_waveGravity );
		Maoli::Deserialize( fin, &_waveLength );
		Maoli::Deserialize( fin, &_waveHeightScale );
		Maoli::Deserialize( fin, &_waveDisplacementScale );
		Maoli::Deserialize( fin, &_extinctionColor );
		Maoli::Deserialize( fin, &_backScatterColor );

		SetWaterLevel( _waterLevel );
		SetWindDirection( _windDirection );
		SetWindVelocity( _windVelocity );
		SetWaveFacetSize( _waveFacetSize );
		SetWaveConstant( _waveConstant );
		SetWaveGravity( _waveGravity );
		SetWaveLength( _waveLength );
		SetWaveHeightScale( _waveHeightScale );
		SetWaveDisplacementScale( _waveDisplacementScale );
		SetExtinctionColor( _extinctionColor );
		SetBackscatterColor( _backScatterColor );
	}

	// Set the shader data
	void Ocean::SetShaderData()
	{
		WaterBackscatterColorVariable->SetFloatVector( _backScatterColor );
		WaterExtinctionColorVariable->SetFloatVector( _extinctionColor );
		WaterHeightVariable->SetFloat( _waterLevel );
	}


}

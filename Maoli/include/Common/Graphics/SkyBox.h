//--------------------------------------------------------------------------------------
// File: Skybox.h
//
// Basic skybox rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	// Forward decl
	class Renderer;

	class Skybox
	{
	public:

		// Ctor
		Skybox(Engine* engine);

		// Dtor
		~Skybox();

		// Setup
		bool Init(const char* file);

		// Effect vars
		void BindEffectVariables();

		// Cleanup
		void Release();

		// Render the skybox
		void Render();

		// Return true if the skybox is loaded
		inline bool IsActive() const { return _cubemap!=nullptr; }

	private:

		Texture::pointer	_cubemap;
		Model*				_model;
		Renderer*			_graphics;
		EffectPass*			_pass;
	};


}

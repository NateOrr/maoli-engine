//--------------------------------------------------------------------------------------
// File: ShadowManager.h
//
// Manages shadow map resources and rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class ShadowManager
	{
	public:

		// Ctor
		ShadowManager( Renderer* graphics );

		// Setup
		bool Init( uint32 resolution, uint32 numSamples, uint32 maxSpotLights, uint32 maxPointLights );

		// Enable support for sun shadows
		bool EnableSunShadows( bool value );

		// Update effect variables
		void BindEffectVariables();

		// Render directional light shadows
		void RenderSunShadows( const Vector3& sunDirection, int32 shadowSoftness );

		// Render spot light shadows
		void RenderSpotLightShadows( const Array<Light*>& spotLights );

		// Render point light shadows
		void RenderPointLightShadows( const Array<Light*>& pointLights );


	private:

		// Compute the AABB for the directional light frustum
		void ComputeFrustumExtents( const Camera& camera, const Matrix& lightViewProj, Vector3& outMin, Vector3& outMax );

		// Compute the view and projection matrices for a directional light
		void ComputeDirectionalLightMatrices( const Camera& camera, const Vector3& direction, Matrix& view, Matrix& proj );

		// Compute the partitions for sdsm, gbuffer textures must be bound first
		void ComputePartitions( const Vector3& lightSpaceBorder, const Vector3& maxScale );

		// Build the partition matrices
		void BuildPartitionMatrices( const Matrix& lightProj );

		// Render all visible models for the sun shadow map
		void RenderModelForSunShadows( QuadtreeElement* obj );

		// SDSM Partition
		__declspec(align(16))
		struct Partition
		{
			float intervalBegin;
			float intervalEnd;
			Vector3 scale;
			Vector3 bias;
		};

		// SDSM partition bounds
		struct BoundsFloat
		{
			Vector3 minCoord;
			Vector3 maxCoord;
		};

		// SDSM Settings
		static const uint32	_numPartitions = 4;
		static const uint32	_tileSize = 128;

		Renderer*				_graphics;					// Graphics engine
		uint32					_resolution;				// Resolution for all shadow maps
		uint32					_numSpotShadowMaps;			// Number of shadow maps in the array
		uint32					_numCubeShadowMaps;			// Number of shadow maps in the array
		D3D11_VIEWPORT			_viewport;					// Shared viewport
		Frustum					_frustum;					// View frustum for mesh culling
		Vector4					_texelSize;					// Size of a shadow map texel
		Matrix					_viewMatrix;				// Shared matrices
		Matrix					_projectionMatrix;			// Shared matrices 
		Matrix					_viewProjectionMatrix;		// Shared matrices
		Array<Matrix>			_spotShadowMatrices;		// Spot light shadow matrix palette
		Array<Vector2>			_spotShadowRanges;			// Optimized light space depth ranges for spot shadows

		// Resources
		Texture::pointer			_cascades;					// Sun shadow cascades
		Texture::pointer			_spotShadowMapArrayTexture;	// Array that contains ALL spot shadow maps
		Texture::pointer			_cubeShadowMapArrayTexture;	// Array that contains ALL cube shadow maps
		DepthStencil::pointer		_depthStencilMSAA;			// Shared MSAA depth stencil
		Texture::pointer			_blurTexture[2];			// Shared texture for blurring
		Buffer<Matrix>::pointer		_spotShadowMatricesBuffer;	// Matrix palette for spot shadow maps
		Buffer<Vector2>::pointer	_spotShadowRangesBuffer;	// Optimized light space depth ranges for spot shadows

		// SDSM
		Partition						_partitions[_numPartitions];			// Partition data
		Buffer<Partition>::pointer		_partitionBuffer;						// SDSM partitions
		Buffer<Partition>::pointer		_partitionReadbackBuffer;				// SDSM partitions staging buffer
		Buffer<BoundsFloat>::pointer	_partitionBoundsBuffer;					// SDSM partition bounds
		Buffer<BoundsFloat>::pointer	_partitionBoundsReadbackBuffer;			// SDSM partition bounds staging buffer
		Matrix							_partitionProjMatrices[_numPartitions]; // Matrix for each partition split

		// SDSM Effect passes
		EffectPass* _shadowmapSDSMPass;					// Fill shadow map sdsm
		EffectPass* _shadowmapTerrainSDSMPass;			// Fill shadow map sdsm
		EffectPass* _shadowmapInstancedSDSMPass;		// Fill shadow map sdsm with instancing
		EffectPass* _shadowmapAlphaSDSMPass;			// Fill shadow map sdsm, alpha tested geometry
		EffectPass* _shadowmapAlphaInstancedSDSMPass;	// Fill shadow map sdsm, alpha tested geometry with instancing
		EffectPass* _shadowmapSkinnedSDSMPass;			// Fill shadow map sdsm, skinned geometry
		EffectPass* _msaaToEvsmPass;					// Convert raw msaa depth buffer to vsm
		EffectPass* _clearPartitionsPass;				// Clear SDSM partitions
		EffectPass* _partitionBoundsPass;				// Compute tight partition bounds
		EffectPass* _partitionsPass;					// Compute the actual partitions
		EffectPass* _buildPartitionsPass;				// Setup for partition building
		EffectPass* _reducePartitionBoundsPass;			// Build bounds for partitions
		EffectPass* _clearPartionBoundsPass;			// Clear bound data before building it
		EffectPass* _partitionShadingPass;				// Shading for a partition slice
		EffectPass* _blurSDSMPass;						// Box blur for partitions

		// SDSM Effect variables
		ShaderResource*		_cascadeMapVariable;				// Shadow map
		ShaderVector*		_lightSpaceBorderVariable;			// Border for sdsm partition generation
		ShaderVector*		_maxScaleVariable;					// For sdsm partition generation
		ShaderScalar*		_dilationFactorVariable;			// Dilation factor for partition building
		ShaderResource*		_partitionsVariable;				// Readonly partitions for the CS
		ShaderResource*		_partitionBoundsVariable;			// Readonly partitions bounds for the CS
		ShaderScalar*		_partitionIDVariable;				// Current partition
		ShaderUAV*			_partitionsUintUAVVariable;			// Partitions buffer for output
		ShaderUAV*			_partitionsFloatUAVVariable;		// Partitions buffer for output
		ShaderUAV*			_partitionBoundsUintUAVVariable;	// Partitions bounds buffer for output
		ShaderVector*		_blurFilterSizeVariable;			// Box blur filter size
		ShaderScalar*		_blurDirectionVariable;				// Box blur direction (0 = h, 1 = v)

		// Shared shader variables
		ShaderResource*		_spotShadowResource;		// Spotlight shadow maps
		ShaderResource*		_cubeShadowResource;		// Point light shadow maps
		ShaderResource*		_msaaDepthVariable;			// MSAA shadow map
		ShaderResource*		_shadowMatricesVariable;	// Shadow map matrix palette
		//ShaderResource*		_shadowRangesVariable;		// Shadow map range palette
		ShaderMatrix*		_shadowMatrixVariable;		// Shadow projection matrix
		ShaderVector*		_textureSizeVariable;		// Texture size info for blur shaders
		ShaderResource*		_postFxVariable;			// PostFX textures
		ShaderScalar*		_filterKernelVariable;		// Kernel for blur filters
		//ShaderVector*		_projRatioVariable;			// Projection ratio for linear depth
		ShaderResource*		_bonePaletteVariable;		// Bone palette for skinned meshes

		// Effect passes for spot/point shadows
		EffectPass*		_shadowmapPass;				// Fill shadow map
		EffectPass*		_skinnedShadowmapPass;		// Fill shadow map with skeletal animation
		EffectPass*		_resolvePass;				// Custom resolve of EVSM to avoid numerical instability
		EffectPass*		_boxBlurHorizontalPass;		// Separable box blur
		EffectPass*		_boxBlurVerticalPass;		// Separable box blur
	};
}
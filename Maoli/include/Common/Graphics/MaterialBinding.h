//--------------------------------------------------------------------------------------
// File: Material.h
//
// Binds a material to the graphics pipeline
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Texture.h"

namespace Maoli
{
	// Forward decl
	class Renderer;

	// Shading models
	enum SHADE_TYPE : int32
	{
		SHADE_PHONG = 0,
		SHADE_COOK_TORRANCE,
		SHADE_OREN_NAYAR,
		SHADE_STRAUSS,
		SHADE_WARD,
		SHADE_ASHIKHMIN_SHIRLEY,
		SHADE_VEGETATION,
	};


	// Binds a material to the graphics pipeline
	class MaterialBinding : public Resource<Renderer>
	{
		friend class Renderer;
		friend class Terrain;
		friend class Renderer;
		friend class Effect;
		friend class Model;
		friend class MeshBinding;
		friend class RenderState;
		friend ResourceManager<Renderer, MaterialBinding>;
		friend Pointer<MaterialBinding>;
	public:

		// Smart pointer
		typedef Pointer<MaterialBinding> pointer;

		// Create from a material
		HRESULT Init( Material* material );

		// Load all textures
		HRESULT ImportTextures();

		// Check for alpha testing
		bool IsAlphaTested() const { return _material->IsAlphaTested(); }


		// Sets a texture
		inline void SetTexture( const Texture::pointer& tex, TEX_TYPE type )
		{
			_textures[type] = tex;
			_srv[type] = _textures[type]->GetSRV()[0];
			_material->_properties.textureFlag.Set( type );
		}
		inline void SetTextureSRV( ID3D11ShaderResourceView* pNewTex, TEX_TYPE type )
		{
			_srv[type] = pNewTex;
			_material->_properties.textureFlag.Set( type );
		}

		// Gets a texture
		inline Texture::pointer GetTexture( TEX_TYPE type ) const { return _textures[type]; }

		// Clear the texture
		void ClearTexture( TEX_TYPE type );

		// Full arrays
		inline Texture::pointer* GetTextures() { return _textures; }
		inline ID3D11ShaderResourceView** GetTextureResources() { return _srv; }

		// Texture loading
		Texture::pointer LoadTexture( const char* file, TEX_TYPE type );

		// Get the material
		inline Material* GetMaterial() { return _material; }

		// Get the material
		inline const Material* GetMaterial() const { return _material; }

		// Get shader data
		inline const void* GetShaderData() const { return _material->GetShaderData(); }

		// Get the size of shader data
		inline uint32 GetShaderDataSize() const { return _material->GetShaderDataSize(); }

		// Get the file path to the material preview
		String GetPreviewFile();

	private:

		// Ctor
		MaterialBinding( Renderer* graphics );

		// Dtor
		~MaterialBinding();

		// Releases the material
		void Release();

		Material*					_material;					// Parent material
		Texture::pointer			_textures[TEX_SIZE];		// Texture array	
		ID3D11ShaderResourceView*	_srv[TEX_SIZE];				// SRV pointer array
	};

}

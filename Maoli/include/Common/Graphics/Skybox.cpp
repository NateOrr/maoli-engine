//--------------------------------------------------------------------------------------
// File: Skybox.cpp
//
// Basic skybox rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/SkyBox.h"
#include "Graphics/Renderer.h"

namespace Maoli
{


	// Ctor
	Skybox::Skybox(Engine* engine)
	{
		_graphics = engine->GetGraphics();
		_pass = nullptr;
		_model = nullptr;
	}


	// Dtor
	Skybox::~Skybox()
	{
		Release();
	}

	
	// Cleanup
	void Skybox::Release()
	{
		_cubemap = nullptr;
		SafeDelete( _model );
	}

	
	// Setup
	bool Skybox::Init( const char* file )
	{
		_cubemap = _graphics->CreateTextureFromFile(file, true);
		_model = Model::CreateFromFile("Resources\\Models\\cube.mesh", _graphics->GetEngine());
		return (_cubemap!=nullptr);
	}

	// Effect vars
	void Skybox::BindEffectVariables()
	{
		_pass = _graphics->GetEffect()->GetPassByName("Skybox");
	}

	
	// Render the skybox
	void Skybox::Render()
	{
		_graphics->SetCubeMap(_cubemap);
		_model->SetPosition(_graphics->GetCamera()->GetPosition());
		_model->UpdateWorldMatrix();
		_graphics->RenderSubMesh(*_model->GetSubMesh(0), _pass, false);
	}

}

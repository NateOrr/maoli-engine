//--------------------------------------------------------------------------------------
// File: DynamicMesh.cpp
//
// Immediate mesh rendering, this is meant for debugging and visualization only
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "DynamicMesh.h"
#include "Graphics/Renderer.h"

namespace Maoli
{
	// Ctor
	DynamicMesh::DynamicMesh( Renderer* graphics )
	{
		_graphics = graphics;
		_maxVerts = _maxIndices = _numIndices = _numVerts = 0;
		_topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		_wireframe = false;
	}

	// Fill the vertex buffer
	bool DynamicMesh::FillVerts( const Vector3* verts, uint32 count )
	{
		// Create a new buffer if needed
		if ( count > _maxVerts )
		{
			_vertexBuffer = _graphics->CreateVertexBuffer<Vector3>( count, D3D11_USAGE_DYNAMIC, verts );
			_maxVerts = _numVerts = count;
			return (_vertexBuffer != nullptr);
		}

		// Copy data
		auto mappedVerts = _vertexBuffer->Map( D3D11_MAP_WRITE_DISCARD );
		if ( mappedVerts )
		{
			memcpy( mappedVerts, verts, sizeof(Vector3)*count );
			_vertexBuffer->Unmap();
			_numVerts = count;
			return true;
		}

		return false;
	}

	// Fill the index buffer
	bool DynamicMesh::FillIndices( const uint32* indices, uint32 count )
	{
		// Create a new buffer if needed
		if ( count > _maxVerts )
		{
			_indexBuffer = _graphics->CreateIndexBuffer<uint32>( count, D3D11_USAGE_DYNAMIC, indices );
			_maxIndices = _numIndices = count;
			return (_indexBuffer != nullptr);
		}

		// Otherwise just copy data
		auto mappedIndices = _indexBuffer->Map( D3D11_MAP_WRITE_DISCARD );
		if ( mappedIndices )
		{
			memcpy( mappedIndices, indices, sizeof(uint32)*count );
			_indexBuffer->Unmap();
			_numIndices = count;
			return true;
		}

		return false;
	}

	// Set the max number of verts
	void DynamicMesh::SetMaxVerts( uint32 value )
	{
		_vertexBuffer = _graphics->CreateVertexBuffer<Vector3>( value, D3D11_USAGE_DYNAMIC );
		_maxVerts = value;
	}

	// Clear the vertex data
	void DynamicMesh::Clear()
	{
		if ( _vertexBuffer )
		{
			auto mappedVerts = _vertexBuffer->Map( D3D11_MAP_WRITE_DISCARD );
			if ( mappedVerts )
			{
				memset( mappedVerts, 0, sizeof(Vector3)*_maxVerts );
				_vertexBuffer->Unmap();
				_numVerts = 0;
			}
		}

		if ( _indexBuffer )
		{
			auto mappedIndices = _indexBuffer->Map( D3D11_MAP_WRITE_DISCARD );
			if ( mappedIndices )
			{
				memset( mappedIndices, 0, sizeof(uint32)*_maxIndices );
				_indexBuffer->Unmap();
				_numIndices = 0;
			}
		}
	}

}

//--------------------------------------------------------------------------------------
// File: DepthStencil.cpp
//
// Depth stencil texture class
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma once

#include "Maoli.h"
#include "Graphics/DepthStencil.h"

namespace Maoli
{

	// Ctor
	DepthStencil::DepthStencil( Renderer* parent ) : Resource( parent ),
		_tex( nullptr ), _dsv( nullptr ), _dsvReadOnly(nullptr)
	{
		_srv[0] = nullptr;
	}


	// Dtor
	DepthStencil::~DepthStencil()
	{
		Release();
	}

	// Create the depth targets
	HRESULT DepthStencil::Create( uint32 width, uint32 height, DXGI_SAMPLE_DESC* pMSAA, bool asTexture )
	{
		// MSAA
		uint32 samples = 1;
		uint32 quality = 0;
		if ( pMSAA )
		{
			samples = pMSAA->Count;
			quality = pMSAA->Quality;
		}

		// Format
		DXGI_FORMAT format[3];
		format[0] = DXGI_FORMAT_R32_TYPELESS;
		format[1] = DXGI_FORMAT_D32_FLOAT;
		format[2] = DXGI_FORMAT_R32_FLOAT;

		// Create depth stencil texture
		D3D11_TEXTURE2D_DESC dstex;
		dstex.ArraySize = 1;
		dstex.Width = width;
		dstex.Height = height;
		dstex.MipLevels = 1;
		dstex.Format = format[0];
		dstex.SampleDesc.Count = samples;
		dstex.SampleDesc.Quality = quality;
		dstex.Usage = D3D11_USAGE_DEFAULT;
		if ( !asTexture )
			dstex.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		else
			dstex.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		dstex.CPUAccessFlags = 0;
		dstex.MiscFlags = 0;
		D_RETURN( _parent->GetDevice()->CreateTexture2D( &dstex, nullptr, &_tex ) );

		// Create the depth stencil view
		D3D11_DEPTH_STENCIL_VIEW_DESC DescDS;
		DescDS.Format = format[1];
		DescDS.Flags = 0;
		if ( pMSAA )
			DescDS.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
		else
		{
			DescDS.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
			DescDS.Texture2D.MipSlice = 0;
		}
		D_RETURN( _parent->GetDevice()->CreateDepthStencilView( _tex, &DescDS, &_dsv ) );

		// Create the shader resource view
		if ( asTexture )
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC dssrv;
			dssrv.Format = format[2];
			if ( pMSAA )
				dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
			else
			{
				dssrv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				dssrv.Texture2D.MipLevels = 1;
				dssrv.Texture2D.MostDetailedMip = 0;
			}
			D_RETURN( _parent->GetDevice()->CreateShaderResourceView( _tex, &dssrv, &_srv[0] ) );
		}

		return S_OK;
	}

	// Create a cube-map depth stencil
	HRESULT DepthStencil::CreateCube( uint32 width, uint32 height )
	{
		// Create cubic depth stencil texture.
		D3D11_TEXTURE2D_DESC dstex;
		ZeroMemory( &dstex, sizeof(dstex) );
		dstex.Width = width;
		dstex.Height = height;
		dstex.MipLevels = 1;
		dstex.ArraySize = 6;
		dstex.SampleDesc.Count = 1;
		dstex.SampleDesc.Quality = 0;
		dstex.Format = DXGI_FORMAT_R32_TYPELESS;
		dstex.Usage = D3D11_USAGE_DEFAULT;
		dstex.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		dstex.CPUAccessFlags = 0;
		dstex.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;
		D_RETURN( _parent->GetDevice()->CreateTexture2D( &dstex, nullptr, &_tex ) );

		// Create the depth stencil view for single face rendering
		D3D11_DEPTH_STENCIL_VIEW_DESC DescDS;
		ZeroMemory( &DescDS, sizeof(DescDS) );
		DescDS.Format = DXGI_FORMAT_D32_FLOAT;
		DescDS.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
		DescDS.Texture2DArray.FirstArraySlice = 0;
		DescDS.Texture2DArray.ArraySize = 1;
		DescDS.Texture2DArray.MipSlice = 0;
		D_RETURN( _parent->GetDevice()->CreateDepthStencilView( _tex, &DescDS, &_dsv ) );

		return S_OK;
	}


	// Free resources
	void DepthStencil::Release()
	{
		SafeRelease( _tex );
		SafeRelease( _srv[0] );
		SafeRelease( _dsv );
		SafeRelease( _dsvReadOnly );
	}


	// Clear the depth stencil
	void DepthStencil::Clear()
	{
		_parent->GetContext()->ClearDepthStencilView( _dsv, D3D11_CLEAR_DEPTH, 1.0f, 0 );
	}

	// Create a read only dsv
	bool DepthStencil::CreateReadOnlyView()
	{
		D3D11_DEPTH_STENCIL_VIEW_DESC DescDS;
		DescDS.Format = DXGI_FORMAT_D32_FLOAT;
		DescDS.Flags = D3D11_DSV_READ_ONLY_DEPTH;
		DescDS.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		DescDS.Texture2D.MipSlice = 0;
		return SUCCEEDED( _parent->GetDevice()->CreateDepthStencilView( _tex, &DescDS, &_dsvReadOnly ) );
	}


}

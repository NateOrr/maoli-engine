//--------------------------------------------------------------------------------------
// File: InstanceSet.cpp
//
// A set of instances of a single mesh
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "InstanceSet.h"

namespace Maoli
{

	// Ctor
	InstanceSet::InstanceSet( RenderNode* node )
	{
		_node = node;
		_refCount = 0;
	}

}
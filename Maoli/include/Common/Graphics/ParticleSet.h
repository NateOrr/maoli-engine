//--------------------------------------------------------------------------------------
// File: ParticleSet.h
//
// A self contained particle system, requires an emitter to function
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Sprite.h"

namespace Maoli
{
	// Forward decl
	class Renderer;
	class ParticleEmitter;

	// Single partcle
	struct Particle : LinkedListNode
	{
		Vector3 position;
		Vector3 velocity;
		Vector3 size;
		Vector3 dSize;
		Color color;
		Color dColor;
		float life;
		float frameTime;		// Timer for animations
		uint32 frame;				// Sprite sheet frame index
	};

	// Vertex intance format
	struct ParticleVertex
	{
		Vector4 position;		// World-space position
		Color color;			// Color
		Vector3 size;			// Scale
		uint32 frame;				// Sprite sheet frame index
	};


	// A particle set contains a group of identical particles
	class ParticleSet : public Sprite
	{
		friend class ParticleEmitter;
	public:

		// Binary serialize
		void Export( std::ofstream& fout );

		// Binary file import
		void Import( std::ifstream& fin );

		// Deep copy clone with a new emitter
		ParticleSet* Clone(ParticleEmitter* newEmitter) const;

		// Update this set
		void Update( float dt );

		// Set the max number of active particles
		void SetMaxParticles( uint32 max );

		// Get the max number of particles
		inline uint32 GetMaxParticles() const { return _pool.Size(); }

		// Set the emmision rate
		void SetEmissionRate( uint32 particlesPerSecond );

		// Get the emission rate
		inline uint32 GetEmissionRate() const { return _emissionRate; }

		// Get the particle life span
		inline float GetLifetime() const { return _lifetime; }

		// Set the particle life span
		inline void SetLifetime( float val ) { _lifetime = val; }

		// Get the local position
		inline const Vector3& GetPosition() const { return _position; }

		// Set the local position
		inline void SetPosition( const Vector3& val ) { _position = val; }

		// Get the time it takes for a particle to fade out
		inline float GetFadeOutTime() const { return _fadeOutTime; }

		// Set the time it takes for a particle to fade out
		inline void SetFadeOutTime( float val ) { _fadeOutTime = val; }

		// Get the time it takes for a particle to fade in
		inline float GetFadeInTime() const { return _fadeInTime; }

		// Set the time it takes for a particle to fade in
		inline void SetFadeInTime( float val ) { _fadeInTime = val; }

		// Get the min spawn Velocity
		inline const Vector3& GetMinVelocity() const { return _minVelocity; }

		// Set the min spawn Velocity
		inline void SetMinVelocity( const Vector3& val ) { _minVelocity = val; }

		// Get the max spawn Velocity
		inline const Vector3& GetMaxVelocity() const { return _maxVelocity; }

		// Set the max spawn Velocity
		inline void SetMaxVelocity( const Vector3& val ) { _maxVelocity = val; }

		// Get the min spawn size
		inline const Vector3& GetMinSize() const { return _minSize; }

		// Set the min spawn size
		inline void SetMinSize( const Vector3& val ) { _minSize = val; }

		// Get the max spawn size
		inline const Vector3& GetMaxSize() const { return _maxSize; }

		// Set the max spawn size
		inline void SetMaxSize( const Vector3& val ) { _maxSize = val; }

		// Get the ending size
		inline const Vector3& GetEndSize() const { return _endSize; }

		// Set the ending size
		inline void SetEndSize( const Vector3& val ) { _endSize = val; }

		// Get the min starting color
		inline const Color& GetMinColor() const { return _minColor; }

		// Set the min starting color
		inline void SetMinColor( const Color& val ) { _minColor = val; }

		// Get the max starting color
		inline const Color& GetMaxColor() const { return _maxColor; }

		// Set the max starting color
		inline void SetMaxColor( const Color& val ) { _maxColor = val; }

		// Get the ending color
		inline const Color& GetEndColor() const { return _endColor; }

		// Set the ending color
		inline void SetEndColor( const Color& val ) { _endColor = val; }

		// Get the spawn radius
		inline float GetSpawnRadius() const { return _spawnRadius; }

		// Set the spawn radius
		inline void SetSpawnRadius( float val ) { _spawnRadius = val; }

		// Get the gravity force
		inline const Vector3& GetGravity() const { return _gravity; }

		// Set the gravity force
		inline void SetGravity( const Vector3& val ) { _gravity = val; }

		// Set the name of the state
		inline void SetName( const char* name ) { _name = name; }

		// Get the name
		inline const String& GetName() const { return _name; }

		// Clear all active particles
		void Clear();

		// Enable this set
		inline void Enable(){ _enabled = true; }

		// Disable this set
		inline void Disable(){ _enabled = false; }

		// Get the number of active particles
		inline uint32 GetNumActiveParticles() const { return _active.Size(); }

	private:

		// Ctor
		ParticleSet( Renderer* graphics, ParticleEmitter* emitter );

		// Emit a particle into the scene
		void EmitParticle();

		// Reset particle data
		void ResetParticle( Particle* particle ) const;

		Renderer*						_graphics;			// Renderer that owns the system
		ParticleEmitter*				_emitter;			// Emitter that owns this set
		UserAllocatedList				_active;			// Active particles in the scene
		UserAllocatedList				_dead;				// Recycle bin
		Array<Particle>					_pool;				// Contiguous particle pool
		Array<ParticleVertex>			_vbData;			// Contiguous particle pool
		Buffer<ParticleVertex>::pointer	_instanceBuffer;	// Particle instance buffer
		bool							_enabled;			// True if active

		uint32			 _emissionRate;		// Time between particle emits
		float			 _emissionPeriod;	// Time between particle emits
		float			 _emissionTimer;	// Used for timing when to emit particles
		float			 _lifetime;			// Particle life span
		float			 _fadeOutTime;		// Lifetime - FadeDelay
		float			 _fadeInTime;		// Lifetime - FadeDelay
		float			 _spawnRadius;		// Spawn radius
		Vector3			 _gravity;			// Gravity force
		Vector3			 _minVelocity;		// Min starting velocity
		Vector3			 _maxVelocity;		// Max starting velocity
		Vector3			 _minSize;			// Minimum spawn size
		Vector3			 _maxSize;			// Maximum spawn size
		Vector3			 _endSize;			// Ending size
		Color			 _minColor;			// Minimum spawn color
		Color			 _maxColor;			// Maximum spawn color
		Color			 _endColor;			// Ending color
		Vector3			 _position;			// Local position
		String			 _name;				// Name of this set
	};
}
//--------------------------------------------------------------------------------------
// File: Probe.h
//
// Environment mapping and planar reflections
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Texture.h"

namespace Maoli
{
	// Forward decl
	class Renderer;

	// Support for environment cube maps and planar reflections.
	class EnvironmentProbe
	{
	public:

		// Ctor
		EnvironmentProbe( Renderer* graphics );

		// Dtor
		~EnvironmentProbe();

		// Check if an update is needed
		inline bool IsDirty() const { return _dirty; }

		// Flag as rendered
		inline void Update() { _dirty = false; }

		// Set the region
		void SetBounds(const Vector3& position, float radius);

		// Get the bounding region
		inline const BoundingVolume& GetBounds() const { return _bounds; }

		// Get the location
		inline const Vector3& GetPosition() const { return _bounds.GetWorldPosition(); }

		// Get the radius
		inline float GetRange() const { return _bounds.GetRadius(); }
		

		// Create a new cubemap probe
		HRESULT CreateCubemap(uint32 size, DXGI_FORMAT format, uint32 miplevels, bool dynamic);

		// Load a cubemap from file
		HRESULT LoadCubemap(const char* file);

		// Dynamic
		bool IsDynamic(){ return _isDynamic; }

		// The render surface
		Texture::pointer GetTexture(){ return _surface; }

		// Free mem
		void Release();

	private:

		Renderer*			_graphics;		// Graphics engine
		Texture::pointer	_surface;		// Low res render target
		bool				_isDynamic;		// If true, updated every frame
		BoundingVolume		_bounds;		// Bounding volume
		bool				_dirty;			// True if an update is needed
	};
}

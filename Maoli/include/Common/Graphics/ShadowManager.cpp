//--------------------------------------------------------------------------------------
// File: ShadowManager.cpp
//
// Manages shadow map resources and rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/ShadowManager.h"

namespace Maoli
{

	// Ctor
	ShadowManager::ShadowManager( Renderer* graphics )
	{
		_graphics = graphics;
		_resolution = 0;
		_numSpotShadowMaps = 0;
		_numCubeShadowMaps = 0;

		_spotShadowResource = nullptr;
		_cubeShadowResource = nullptr;
		_msaaDepthVariable = nullptr;
		_shadowMatricesVariable = nullptr;

	}

	// Setup
	bool ShadowManager::Init( uint32 resolution, uint32 numSamples, uint32 maxSpotLights, uint32 maxPointLights )
	{
		// Settings
		_resolution = resolution;
		DXGI_SAMPLE_DESC sampleDesc = { numSamples, 0 };
		_numSpotShadowMaps = maxSpotLights;
		_numCubeShadowMaps = maxPointLights;
		_spotShadowMatrices.Allocate( maxSpotLights );
		_spotShadowRanges.Allocate( maxSpotLights );

		// Setup the viewport
		_viewport.Height = (float)_resolution;
		_viewport.Width = (float)_resolution;
		_viewport.MinDepth = 0;
		_viewport.MaxDepth = 1.0f;
		_viewport.TopLeftX = 0;
		_viewport.TopLeftY = 0;

		// Compute size info
		_texelSize.x = _texelSize.y = float( _resolution );
		_texelSize.z = _texelSize.w = 1.0f / float( _resolution );

		// Mutisampled depth buffer
		_depthStencilMSAA = _graphics->CreateDepthStencil( _resolution, _resolution, &sampleDesc, true );
		if ( !_depthStencilMSAA )
			return false;

		// Create a render target to blur shadow maps
		_blurTexture[0] = _graphics->CreateTexture( _resolution, _resolution, DXGI_FORMAT_R32G32_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_blurTexture[0] )
			return false;

		// Create a render target to blur shadow maps
		_blurTexture[1] = _graphics->CreateTexture( _resolution, _resolution, DXGI_FORMAT_R32G32_FLOAT, 1, nullptr, D3D11_USAGE_DEFAULT, true, true );
		if ( !_blurTexture[1] )
			return false;

		// Allocate the spot shadow map array
		_spotShadowMapArrayTexture = _graphics->CreateTextureArrayResource( _resolution, _resolution, _numSpotShadowMaps, DXGI_FORMAT_R32G32_FLOAT, 1, true );
		if ( !_spotShadowMapArrayTexture )
			return false;

		// Allocate the point shadow map array
		_cubeShadowMapArrayTexture = _graphics->CreateTextureCubeArray( _resolution, _resolution, _numCubeShadowMaps, DXGI_FORMAT_R32G32_FLOAT, 1 );
		if ( !_cubeShadowMapArrayTexture )
			return false;

		// Shadow matrix buffer
		_spotShadowMatricesBuffer = _graphics->CreateBuffer<Matrix>( maxSpotLights, D3D11_USAGE_DYNAMIC, false, true );
		if ( !_spotShadowMatricesBuffer )
		{
			M_ERROR( "Could not create _spotShadowMatricesBuffer" );
			return false;
		}

		// Depth ranges
		_spotShadowRangesBuffer = _graphics->CreateBuffer<Vector2>( maxSpotLights, D3D11_USAGE_DYNAMIC, false, true );
		if ( !_spotShadowRangesBuffer )
		{
			M_ERROR( "Could not create _spotShadowRangesBuffer" );
			return false;
		}

		return true;
	}

	// Enable support for sun shadows
	bool ShadowManager::EnableSunShadows( bool value )
	{
		// Clear resources
		_cascades = nullptr;
		_partitionBuffer = nullptr;
		_partitionReadbackBuffer = nullptr;
		_partitionBoundsBuffer = nullptr;
		_partitionBoundsReadbackBuffer = nullptr;

		// Allocate sun shadows
		if ( value )
		{
			_cascades = _graphics->CreateTextureArrayResource( _resolution, _resolution, _numPartitions, DXGI_FORMAT_R32G32_FLOAT, 1, true );
			if ( !_cascades )
				return false;

			// Create the partition buffers
			_partitionBuffer = _graphics->CreateBuffer<Partition>( _numPartitions, D3D11_USAGE_DEFAULT, true, true );
			_partitionReadbackBuffer = _graphics->CreateBuffer<Partition>( _numPartitions, D3D11_USAGE_STAGING, false, false );
			_partitionBoundsBuffer = _graphics->CreateBuffer<BoundsFloat>( _numPartitions, D3D11_USAGE_DEFAULT, true, true );
			_partitionBoundsReadbackBuffer = _graphics->CreateBuffer<BoundsFloat>( _numPartitions, D3D11_USAGE_STAGING, false, false );
			if ( !_partitionBuffer || !_partitionReadbackBuffer || !_partitionBoundsBuffer || !_partitionBoundsReadbackBuffer )
				return false;
		}

		// Rebind effect variables
		BindEffectVariables();

		return true;
	}


	// Update effect variables
	void ShadowManager::BindEffectVariables()
	{
		// Shader vars
		_spotShadowResource = _graphics->GetEffect()->GetVariableByName( "g_txSpotShadowMaps" )->AsShaderResource();
		_cubeShadowResource = _graphics->GetEffect()->GetVariableByName( "g_txCubeShadowMaps" )->AsShaderResource();
		_msaaDepthVariable = _graphics->GetEffect()->GetVariableByName( "g_txDepthMS" )->AsShaderResource();

		// Grab the variables and passes
		_filterKernelVariable = _graphics->GetEffect()->GetVariableByName( "g_FilterKernel" )->AsScalar();
		_textureSizeVariable = _graphics->GetEffect()->GetVariableByName( "g_TextureSize" )->AsVector();
		_postFxVariable = _graphics->GetEffect()->GetVariableByName( "g_txPostFX" )->AsShaderResource();
		_shadowMatrixVariable = _graphics->GetEffect()->GetVariableByName( "g_mShadow" )->AsMatrix();
		_shadowMatricesVariable = _graphics->GetEffect()->GetVariableByName( "g_ShadowMatrices" )->AsShaderResource();
		_bonePaletteVariable = _graphics->GetEffect()->GetVariableByName( "g_BonePalette" )->AsShaderResource();
		//_shadowRangesVariable = _graphics->GetEffect()->GetVariableByName( "g_ShadowRanges" )->AsShaderResource();
		//_projRatioVariable = _graphics->GetEffect()->GetVariableByName( "g_ShadowProjRatio" )->AsVector();

		// Passes
		_shadowmapPass = _graphics->GetEffect()->GetPassByName( "ShadowMap" );
		_skinnedShadowmapPass = _graphics->GetEffect()->GetPassByName( "ShadowMapSkinned" );
		_resolvePass = _graphics->GetEffect()->GetPassByName( "ResolveEVSM" );
		_boxBlurHorizontalPass = _graphics->GetEffect()->GetPassByName( "BoxBlurH" );
		_boxBlurVerticalPass = _graphics->GetEffect()->GetPassByName( "BoxBlurV" );

		// Grab the variables for sdsm
		_cascadeMapVariable = _graphics->GetEffect()->GetVariableByName( "g_txCascade" )->AsShaderResource();
		_lightSpaceBorderVariable = _graphics->GetEffect()->GetVariableByName( "g_LightSpaceBorder" )->AsVector();
		_maxScaleVariable = _graphics->GetEffect()->GetVariableByName( "g_MaxScale" )->AsVector();
		_dilationFactorVariable = _graphics->GetEffect()->GetVariableByName( "g_DilationFactor" )->AsScalar();
		_partitionsVariable = _graphics->GetEffect()->GetVariableByName( "g_PartitionsReadOnly" )->AsShaderResource();
		_partitionBoundsVariable = _graphics->GetEffect()->GetVariableByName( "g_PartitionBoundsReadOnly" )->AsShaderResource();
		_partitionsUintUAVVariable = _graphics->GetEffect()->GetVariableByName( "g_PartitionsUint" )->AsUnorderedAccessView();
		_partitionsFloatUAVVariable = _graphics->GetEffect()->GetVariableByName( "g_Partitions" )->AsUnorderedAccessView();
		_partitionBoundsUintUAVVariable = _graphics->GetEffect()->GetVariableByName( "g_PartitionBoundsUint" )->AsUnorderedAccessView();
		_partitionIDVariable = _graphics->GetEffect()->GetVariableByName( "g_PartitionID" )->AsScalar();
		_blurFilterSizeVariable = _graphics->GetEffect()->GetVariableByName( "g_BlurFilterSize" )->AsVector();
		_blurDirectionVariable = _graphics->GetEffect()->GetVariableByName( "g_BlurDirection" )->AsScalar();

		// SDSM Passes
		_shadowmapSDSMPass = _graphics->GetEffect()->GetPassByName( "ShadowMapSDSM" );
		_shadowmapTerrainSDSMPass = _graphics->GetEffect()->GetPassByName( "ShadowMapTerrainSDSM" );
		_shadowmapInstancedSDSMPass = _graphics->GetEffect()->GetPassByName( "ShadowMapInstancedSDSM" );
		_shadowmapAlphaSDSMPass = _graphics->GetEffect()->GetPassByName( "ShadowMapAlphaSDSM" );
		_shadowmapAlphaInstancedSDSMPass = _graphics->GetEffect()->GetPassByName( "ShadowMapAlphaInstancedSDSM" );
		_shadowmapSkinnedSDSMPass = _graphics->GetEffect()->GetPassByName( "ShadowMapSkinnedSDSM" );
		_msaaToEvsmPass = _graphics->GetEffect()->GetPassByName( "MSAAtoEVSM" );
		_clearPartitionsPass = _graphics->GetEffect()->GetPassByName( "ClearPartitions" );
		_partitionBoundsPass = _graphics->GetEffect()->GetPassByName( "PartitionBounds" );
		_partitionsPass = _graphics->GetEffect()->GetPassByName( "Partitions" );
		_buildPartitionsPass = _graphics->GetEffect()->GetPassByName( "BuildPartitions" );
		_reducePartitionBoundsPass = _graphics->GetEffect()->GetPassByName( "ReducePartitionBounds" );
		_clearPartionBoundsPass = _graphics->GetEffect()->GetPassByName( "ClearPartitionBounds" );
		_partitionShadingPass = _graphics->GetEffect()->GetPassByName( "ShadingSDSMSingle" );
		_blurSDSMPass = _graphics->GetEffect()->GetPassByName( "BoxBlurSDSM" );

		// Set values
		_textureSizeVariable->SetFloatVector( _texelSize );
		_dilationFactorVariable->SetFloat( 0.015f );

		// One time binds
		if ( _cascades )
		{
			_cascadeMapVariable->SetResource( *_cascades );
			_partitionsVariable->SetResource( *_partitionBuffer );
			_partitionsUintUAVVariable->SetResource( *_partitionBuffer );
			_partitionsFloatUAVVariable->SetResource( *_partitionBuffer );
			_partitionBoundsVariable->SetResource( *_partitionBoundsBuffer );
			_partitionBoundsUintUAVVariable->SetResource( *_partitionBoundsBuffer );
		}
		_msaaDepthVariable->SetResource( *_depthStencilMSAA );
		_cubeShadowResource->SetResource( *_cubeShadowMapArrayTexture );
		_spotShadowResource->SetResource( *_spotShadowMapArrayTexture );
		_shadowMatricesVariable->SetResource( *_spotShadowMatricesBuffer );
		//_shadowRangesVariable->SetResource( *_spotShadowRangesBuffer );

	}

	// Render all visible models for the sun shadow map
	void ShadowManager::RenderModelForSunShadows( QuadtreeElement* obj )
	{
		Model& model = *(Model*)obj;

		// Render the submeshes
		for ( uint32 k = 0; k < model.GetNumSubMesh(); k++ )
		{
			RenderNode& submesh = *model.GetSubMesh( k );
			if ( submesh.GetMaterialBinding()->GetMaterial()->IsTransparent() )
				continue;

			// Get the effect pass
			_shadowMatrixVariable->SetMatrix( *submesh.worldMatrix * _viewProjectionMatrix );
			if ( submesh.bonePalette )
			{
				_bonePaletteVariable->SetResource( *submesh.bonePalette );
				_shadowmapSkinnedSDSMPass->Apply();
				_graphics->RenderSubMeshPosSkinned( submesh );
			}
			else if ( submesh.GetMaterialBinding()->IsAlphaTested() )
			{
				_graphics->SetMaterial( submesh.GetMaterialBinding() );
				_shadowmapAlphaSDSMPass->Apply();
				_graphics->RenderSubMesh( submesh );
			}
			else
			{
				_shadowmapSDSMPass->Apply();
				_graphics->RenderSubMeshPos( submesh );
			}
		}
	}

	// Render directional light shadows
	void ShadowManager::RenderSunShadows( const Vector3& sunDirection, int32 shadowSoftness )
	{
		// Build the initial view and projection matrices
		Camera& camera = *_graphics->GetCamera();
		ComputeDirectionalLightMatrices( camera, sunDirection, _viewMatrix, _projectionMatrix );
		_viewProjectionMatrix = _viewMatrix * _projectionMatrix;
		_shadowMatrixVariable->SetMatrix( _viewProjectionMatrix );

		// Compute the partitions
		const float maxFloat = std::numeric_limits<float>::max();
		Vector3 maxPartitionScale( maxFloat, maxFloat, maxFloat );
		Vector2 blurSizeLightSpace;
		const float edgeSofteningAmount = 0.02f * shadowSoftness;
		const float maxEdgeSofteningFilter = 16.0f;
		blurSizeLightSpace = Vector2(
			edgeSofteningAmount * 0.5f * _projectionMatrix( 0, 0 ),
			edgeSofteningAmount * 0.5f * _projectionMatrix( 1, 1 )
			);
		float maxBlurLightSpace = maxEdgeSofteningFilter / float( _resolution );
		maxPartitionScale.x = maxBlurLightSpace / blurSizeLightSpace.x;
		maxPartitionScale.y = maxBlurLightSpace / blurSizeLightSpace.y;
		Vector3 partitionBorderLightSpace( 0, 0, _projectionMatrix( 2, 2 ) );
		ComputePartitions( partitionBorderLightSpace, maxPartitionScale );

		// Build the matrices for each partition
		BuildPartitionMatrices( _projectionMatrix );

		// Process each partition
		_graphics->SetViewport( _viewport );
		_blurFilterSizeVariable->SetFloatVector( blurSizeLightSpace );
		for ( uint32 i = 0; i < _numPartitions; ++i )
		{
			// Skip empty partitions
			if ( _partitions[i].scale.x < 0.0f )
				continue;

			// Build the frustum
			_frustum.Build( _viewMatrix, _partitionProjMatrices[i] );

			// Apply this partition to the scene
			_partitionIDVariable->SetInt( i );

			// Bind the MSAA targets
			_depthStencilMSAA->Clear();
			_graphics->ClearInputBindings( *_depthStencilMSAA );
			_graphics->BindDepthStencilWithoutRenderTarget( _depthStencilMSAA );

			// Terrain
			_shadowMatrixVariable->SetMatrix( _viewProjectionMatrix );
			_graphics->RenderTerrain( _shadowmapTerrainSDSMPass );

			// Render the scene into the shadow map		
// 			if ( _graphics->GetQuadtree() )
// 			{
// 				_graphics->GetQuadtree()->GetVisibleObjects( _frustum, &ShadowManager::RenderModelForSunShadows, this );
// 			}
// 			else
			{
				for ( uint32 e = 0; e < _graphics->GetNumModels(); ++e )
				{
					// Check the mesh against the light frustum
					Model& mesh = *_graphics->GetModel( e );
					//if ( !_frustum.CheckAABB( mesh.GetBounds() ) )
					//	continue;

					// Render the submeshes
					for ( uint32 k = 0; k < mesh.GetNumSubMesh(); k++ )
					{
						RenderNode& submesh = *mesh.GetSubMesh( k );
						if ( submesh.GetMaterialBinding()->GetMaterial()->IsTransparent() )
							continue;

						// Get the effect pass
						_shadowMatrixVariable->SetMatrix( *submesh.worldMatrix * _viewProjectionMatrix );
						if ( submesh.bonePalette )
						{
							_bonePaletteVariable->SetResource( *submesh.bonePalette );
							_shadowmapSkinnedSDSMPass->Apply();
							_graphics->RenderSubMeshPosSkinned( submesh );
						}
						else if ( submesh.GetMaterialBinding()->IsAlphaTested() )
						{
							_graphics->SetMaterial( submesh.GetMaterialBinding() );
							_shadowmapAlphaSDSMPass->Apply();
							_graphics->RenderSubMesh( submesh );
						}
						else
						{
							_shadowmapSDSMPass->Apply();
							_graphics->RenderSubMeshPos( submesh );
						}
					}
				}
			}

			// Skip partitions with subpixel blur sizes
			if ( blurSizeLightSpace.x * _partitions[i].scale.x * _resolution <= 1.0f && blurSizeLightSpace.y * _partitions[i].scale.y * _resolution <= 1.0f )
			{
				// Resolve the MSAA surface
				_graphics->BindRenderTargetWithoutDepth( *_cascades, i );
				_graphics->SetRenderToQuad();
				_msaaToEvsmPass->Apply();
				_graphics->GetContext()->Draw( 6, 0 );
			}
			else
			{
				// Resolve the MSAA surface
				_graphics->BindRenderTargetWithoutDepth( *_blurTexture[0] );
				_graphics->SetRenderToQuad();
				_msaaToEvsmPass->Apply();
				_graphics->GetContext()->Draw( 6, 0 );

				// Horizontal blur
				_graphics->BindRenderTargetWithoutDepth( *_blurTexture[1] );
				_blurDirectionVariable->SetInt( 0 );
				_postFxVariable->SetResource( *_blurTexture[0] );
				_blurSDSMPass->Apply();
				_graphics->GetContext()->Draw( 6, 0 );

				// Vertical blur
				_graphics->BindRenderTargetWithoutDepth( *_cascades, i );
				_blurDirectionVariable->SetInt( 1 );
				_postFxVariable->SetResource( *_blurTexture[1] );
				_blurSDSMPass->Apply();
				_graphics->GetContext()->Draw( 6, 0 );
			}
		}

		// Generate mip maps for the shadow map
		//_cascades->GenerateMips();

		_shadowMatrixVariable->SetMatrix( _viewProjectionMatrix );
	}

	// Compute the AABB for a frustum
	void ShadowManager::ComputeFrustumExtents( const Camera& camera, const Matrix& lightViewProj, Vector3& outMin, Vector3& outMax )
	{
		// Get a matrix to move from view space to light projection space, and transform the frustum corners
		Vector3 cornersLightView[8];
		Matrix cameraViewToLightProj = camera.GetOrientationMatrix() * lightViewProj;
		cameraViewToLightProj.TransformArray( camera.GetFrustumCorners(), 8, cornersLightView );

		// Compute AABB of the transformed frustum
		outMin = cornersLightView[0];
		outMax = cornersLightView[0];
		for ( uint32 i = 1; i < 8; ++i )
		{
			outMin = Vector3::Min( outMin, cornersLightView[i] );
			outMax = Vector3::Max( outMax, cornersLightView[i] );
		}
	}


	// Compute the view and projection matrices for a directional light
	void ShadowManager::ComputeDirectionalLightMatrices( const Camera& camera, const Vector3& direction, Matrix& outView, Matrix& outProj )
	{
		// Align light-space axes to frustum axes
		// This ensures that the top and bottom of the frustum projection in
		// light space will align with one axis of the shadow map, which is a reasonable
		// guess at the best oriented bounding box (From SDSM paper)
		outView = Matrix::CreateLookAtView( -direction, Vector3( 0.0f, 0.0f, 0.0f ), camera.GetRightVector() );

		// Compute the light space frustum AABB
		Vector3 min, max;
		ComputeFrustumExtents( camera, outView, min, max );

		// First adjust the light matrix to be centered on the extents in x/y and behind everything in z
		Vector3 center = 0.5f * (min + max);
		Matrix centerTransform = Matrix::CreateTranslation( -center.x, -center.y, -min.z );
		outView.Mult( centerTransform );

		// Now create a projection matrix that covers the extents when centered
		Vector3 dimensions = max - min;
		outProj.Orthographic( dimensions.x, dimensions.y, 0.0f, dimensions.z );
	}

	// Compute the partitions for sdsm, gbuffer textures must be bound first
	void ShadowManager::ComputePartitions( const Vector3& lightSpaceBorder, const Vector3& maxScale )
	{
		// Get the tile dispatch sizes
		int32 dispatchWidth = (_graphics->GetWidth() + _tileSize - 1) / _tileSize;
		int32 dispatchHeight = (_graphics->GetHeight() + _tileSize - 1) / _tileSize;

		// Make sure the depth buffer is not bound
		_graphics->ClearRenderTarget();

		{
			// Update constant buffer
			_lightSpaceBorderVariable->SetFloatVector( lightSpaceBorder );
			_maxScaleVariable->SetFloatVector( maxScale );

			// Clear out Z-bounds result for reduction
			_clearPartitionsPass->Apply();
			_graphics->GetContext()->Dispatch( 1, 1, 1 );

			// Reduce Z-bounds
			_partitionBoundsPass->Apply();
			_graphics->GetContext()->Dispatch( dispatchWidth, dispatchHeight, 1 );

			// Generate partition intervals from Z-bounds
			_partitionsPass->Apply();
			_graphics->GetContext()->Dispatch( 1, 1, 1 );
		}


		{
			// Clear partition bounds buffer
			_clearPartionBoundsPass->Apply();
			_graphics->GetContext()->Dispatch( 1, 1, 1 );

			// Reduce bounds
			_reducePartitionBoundsPass->Apply();
			_graphics->GetContext()->Dispatch( dispatchWidth, dispatchHeight, 1 );

			// Update partitions with new bounds
			_buildPartitionsPass->Apply();
			_graphics->GetContext()->Dispatch( 1, 1, 1 );
		}

	}


	// Build the partition matrices
	void ShadowManager::BuildPartitionMatrices( const Matrix& lightProj )
	{
		// Read back
		_graphics->GetContext()->CopyResource( _partitionReadbackBuffer->GetBuffer(), _partitionBuffer->GetBuffer() );

		// Map; block for now :S
		const Partition* data = _partitionReadbackBuffer->Map( D3D11_MAP_READ );
		memcpy( _partitions, data, sizeof(Partition)* _numPartitions );
		_partitionReadbackBuffer->Unmap();

		// For each partition, work out the composite matrix
		for ( uint32 i = 0; i < _numPartitions; ++i )
		{
			// See vertex shader math
			const Partition& partition = _partitions[i];
			Matrix scale = Matrix::CreateScaling( partition.scale );
			Matrix bias = Matrix::CreateTranslation( (2.0f * partition.bias.x + partition.scale.x - 1.0f),
				-(2.0f * partition.bias.y + partition.scale.y - 1.0f), partition.bias.z );
			_partitionProjMatrices[i] = lightProj * scale * bias;
		}

	}

	// Render spot light shadows
	void ShadowManager::RenderSpotLightShadows( const Array<Light*>& spotLights )
	{
		// Set the viewport for rendering to shadow maps
		_graphics->SetViewport( _viewport );

		// Move the camera frustum corners into world space
		Vector3 worldSpaceCorners[8];
		_graphics->GetCamera()->GetOrientationMatrix().TransformArray( _graphics->GetCamera()->GetFrustumCorners(), 8, worldSpaceCorners );

		// Process each spot light
		for ( uint32 lightIndex = 0; lightIndex < spotLights.Size(); ++lightIndex )
		{
			Light& light = *spotLights[lightIndex];

			// Build the view matrix
			_viewMatrix = Matrix::CreateLookAtView( light.GetPosition(), light.GetPosition() + light.GetDirection(), Vector3( 0, 1, 0 ) );

			// Compute the optimal depth range by projecting the view frustum range into light space
			// 			float min = light.GetRange(), max = 0.05f;
			// 			for ( int32 i = 0; i < 8; ++i )
			// 			{
			// 				Vector3 viewSpaceCorner = _viewMatrix.Transform( worldSpaceCorners[i] );
			// 				min = std::min( min, viewSpaceCorner.z );
			// 				max = std::min( max, viewSpaceCorner.z );
			// 			}
			// 			float invClipDelta = 1.0f / (light.GetRange() - 0.05f);
			// 			_spotShadowRanges[lightIndex].x = (std::max( min, 0.05f ) - 0.05f) * invClipDelta;
			// 			_spotShadowRanges[lightIndex].y = (std::max( max, light.GetRange() ) - 0.05f) * invClipDelta;

			// Setup the projection matrix and frustum
			_projectionMatrix.Perspective( Math::pi*0.5f, 1.0f, 0.05f, light.GetRange() );
			_spotShadowMatrices[lightIndex] = _viewMatrix * _projectionMatrix;
			light.frustum.Build( _viewMatrix, _projectionMatrix );

			// 			// Compute projection ratio
			// 			Vector2 projRatio;
			// 			projRatio.x = light.GetRange() * invClipDelta;
			// 			projRatio.y = -0.05f * invClipDelta;
			// 			_projRatioVariable->SetFloatVector( projRatio );
			// 
			// 			// Rescale range into device space
			// 			_spotShadowRanges[lightIndex].x = (projRatio.y / _spotShadowRanges[lightIndex].x) + projRatio.x;
			// 			_spotShadowRanges[lightIndex].y = (projRatio.y / _spotShadowRanges[lightIndex].y) + projRatio.x;

			// Bind the MSAA targets
			_depthStencilMSAA->Clear();
			_graphics->ClearInputBindings( *_depthStencilMSAA );
			_graphics->BindDepthStencilWithoutRenderTarget( _depthStencilMSAA );

			// Render the scene into the shadow map		
			for ( uint32 e = 0; e < _graphics->GetNumModels(); ++e )
			{
				// Check the mesh against the light frustum
				Model& mesh = *_graphics->GetModel( e );
				if ( !light.frustum.CheckAABB( mesh.GetBounds() ) )
					continue;

				// Render the submeshes
				_shadowMatrixVariable->SetMatrix( mesh.GetWorldMatrix() * _spotShadowMatrices[lightIndex] );
				for ( uint32 k = 0; k < mesh.GetNumSubMesh(); k++ )
				{
					RenderNode& submesh = *mesh.GetSubMesh( k );
					if ( submesh.GetMaterialBinding()->GetMaterial()->IsTransparent() )
						continue;

					// Get the effect pass
					if ( submesh.bonePalette )
					{
						_bonePaletteVariable->SetResource( *submesh.bonePalette );
						_skinnedShadowmapPass->Apply();
						_graphics->RenderSubMeshPosSkinned( submesh );
					}
					else
					{
						// Render the mesh				
						_shadowmapPass->Apply();
						_graphics->RenderSubMeshPos( submesh );
					}
				}
			}

			// Resolve the MSAA surface
			_graphics->BindRenderTarget( *_blurTexture[1] );
			_graphics->SetRenderToQuad();
			_resolvePass->Apply();
			_graphics->GetContext()->Draw( 6, 0 );

			// Now blur the shadow map with a separable box filter
			if ( light.ShadowSoftness > 0 )
			{
				// Horizontal
				_filterKernelVariable->SetInt( light.ShadowSoftness );
				_graphics->BindRenderTarget( *_blurTexture[0] );
				_postFxVariable->SetResource( *_blurTexture[1] );
				_boxBlurHorizontalPass->Apply();
				_graphics->GetContext()->Draw( 6, 0 );

				// Vertical
				_graphics->BindRenderTarget( *_spotShadowMapArrayTexture, lightIndex );
				_postFxVariable->SetResource( *_blurTexture[0] );
				_boxBlurVerticalPass->Apply();
				_graphics->GetContext()->Draw( 6, 0 );
			}

			// Transpose the matrix for hlsl
			_spotShadowMatrices[lightIndex].Transpose();
		}

		// Build the mip chain for all the shadow maps
		//_spotShadowMapArrayTexture->GenerateMips();

		// Send the matrix palette to the gpu
		auto gpuShadowMatrices = _spotShadowMatricesBuffer->Map( D3D11_MAP_WRITE_DISCARD );
		memcpy( gpuShadowMatrices, _spotShadowMatrices, spotLights.Size() * sizeof(Matrix) );
		_spotShadowMatricesBuffer->Unmap();

		// Send the range palette to the gpu
		// 		auto sputShadowRanges = _spotShadowRangesBuffer->Map( D3D11_MAP_WRITE_DISCARD );
		// 		memcpy( sputShadowRanges, _spotShadowRanges, spotLights.Size() * sizeof(Vector2) );
		// 		_spotShadowRangesBuffer->Unmap();
	}

	// Render point light shadows
	void ShadowManager::RenderPointLightShadows( const Array<Light*>& pointLights )
	{
		// Set the viewport for rendering to shadow maps
		_graphics->SetViewport( _viewport );

		// Process each point light
		for ( uint32 lightIndex = 0; lightIndex < pointLights.Size(); ++lightIndex )
		{
			// Per light constants
			Light& light = *pointLights[lightIndex];
			_filterKernelVariable->SetInt( light.ShadowSoftness );
			_projectionMatrix.Perspective( Math::pi*0.5f, 1.0f, 0.05f, light.GetRange() );

			// Render each face of the cube map for point lights
			Vector3 vLookDir;
			Vector3 vUpDir;
			for ( int32 i = 0; i < 6; i++ )
			{
				// Clear and set the render target/depth buffer
				_depthStencilMSAA->Clear();
				_graphics->ClearInputBindings( *_depthStencilMSAA );
				_graphics->BindDepthStencilWithoutRenderTarget( _depthStencilMSAA );

				// Set the view matrix for this face
				switch ( (CubemapFace)i )
				{
					case CubemapFace::PositiveX:
						vLookDir = Vector3( 1.0f, 0.0f, 0.0f );
						vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
						break;
					case CubemapFace::NegativeX:
						vLookDir = Vector3( -1.0f, 0.0f, 0.0f );
						vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
						break;
					case CubemapFace::PositiveY:
						vLookDir = Vector3( 0.0f, 1.0f, 0.0f );
						vUpDir = Vector3( 0.0f, 0.0f, -1.0f );
						break;
					case CubemapFace::NegativeY:
						vLookDir = Vector3( 0.0f, -1.0f, 0.0f );
						vUpDir = Vector3( 0.0f, 0.0f, 1.0f );
						break;
					case CubemapFace::PositiveZ:
						vLookDir = Vector3( 0.0f, 0.0f, 1.0f );
						vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
						break;
					case CubemapFace::NegativeZ:
						vLookDir = Vector3( 0.0f, 0.0f, -1.0f );
						vUpDir = Vector3( 0.0f, 1.0f, 0.0f );
						break;
				}

				// Set the view transform for this cubemap face
				_viewMatrix = Matrix::CreateLookAtView( light.GetPosition(), light.GetPosition() + vLookDir, vUpDir );
				light.frustum.Build( _viewMatrix, _projectionMatrix );
				Matrix lightViewProj = _viewMatrix * _projectionMatrix;

				// Render the scene into the shadow map	cube face	
				for ( uint32 e = 0; e < _graphics->GetNumModels(); e++ )
				{
					// Check the mesh against the light frustum
					Model& mesh = *_graphics->GetModel( e );
					if ( !light.frustum.CheckAABB( mesh.GetBounds() ) )
						continue;

					// Render the submeshes
					_shadowMatrixVariable->SetMatrix( mesh.GetWorldMatrix() * lightViewProj );
					for ( uint32 k = 0; k < mesh.GetNumSubMesh(); k++ )
					{
						RenderNode& submesh = *mesh.GetSubMesh( k );
						if ( submesh.GetMaterialBinding()->GetMaterial()->IsTransparent() )
							continue;

						// Get the effect pass
						if ( submesh.bonePalette )
						{
							_bonePaletteVariable->SetResource( *submesh.bonePalette );
							_skinnedShadowmapPass->Apply();
							_graphics->RenderSubMeshPosSkinned( submesh );
						}
						else
						{
							// Render the mesh				
							_shadowmapPass->Apply();
							_graphics->RenderSubMeshPos( submesh );
						}
					}
				}

				// Now blur the shadow map with a separable box filter
				if ( light.ShadowSoftness > 0 )
				{
					// Resolve the MSAA surface
					_graphics->BindRenderTargetWithoutDepth( *_blurTexture[1] );
					_graphics->SetRenderToQuad();
					_resolvePass->Apply();
					_graphics->GetContext()->Draw( 6, 0 );

					// Horizontal
					_filterKernelVariable->SetInt( light.ShadowSoftness );
					_graphics->BindRenderTargetWithoutDepth( *_blurTexture[0] );
					_postFxVariable->SetResource( *_blurTexture[1] );
					_boxBlurHorizontalPass->Apply();
					_graphics->GetContext()->Draw( 6, 0 );

					// Vertical
					_graphics->BindRenderTargetWithoutDepth( *_cubeShadowMapArrayTexture, lightIndex * 6 + i );
					_postFxVariable->SetResource( *_blurTexture[0] );
					_boxBlurVerticalPass->Apply();
					_graphics->GetContext()->Draw( 6, 0 );
				}
				else
				{
					// Resolve the MSAA surface
					_graphics->BindRenderTargetWithoutDepth( *_cubeShadowMapArrayTexture, lightIndex * 6 + i );
					_graphics->SetRenderToQuad();
					_resolvePass->Apply();
					_graphics->GetContext()->Draw( 6, 0 );
				}
			}
		}

		// Build the mip chain for all the shadow maps
		//_cubeShadowMapArrayTexture->GenerateMips();
	}




}
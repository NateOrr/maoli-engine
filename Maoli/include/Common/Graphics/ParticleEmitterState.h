//--------------------------------------------------------------------------------------
// File: ParticleEmitterState.h
//
// A state within a particle emitter, can contain multiple particle types
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class ParticleSet;

	// A state for the emitter
	class ParticleEmitterState
	{
		friend class ParticleEmitter;
	public:

		// Ctor
		ParticleEmitterState();

		// Set the name of the state
		inline void SetName( const char* name ) { _name = name; }

		// Get the name
		inline const String& GetName() const { return _name; }

		// Add a particle set
		void AddParticleSet(ParticleSet* set);

		// Remove a particle set
		void RemoveParticleSet(ParticleSet* set);
		void RemoveParticleSet(int32 index);

		// Get a particle set
		inline ParticleSet* GetParticleSet(int32 index){ return _sets[index]; }

		// Get the number of particle sets
		inline uint32 GetNumParticleSets() const { return _sets.Size(); }

		// Set the duration for this state
		inline void SetDuration(float value){ _duration = value; }

		// Get the duration for this state
		inline float GetDuration() const { return _duration; }

		// Set the next state to transition to
		inline void SetNextState(ParticleEmitterState* state){ _nextState = state; }

		// Get the next state
		inline ParticleEmitterState* GetNextState() const { return _nextState; }

	private:

		String					_name;		// Name of this state
		Array<ParticleSet*>		_sets;		// Active particle sets
		ParticleEmitterState*	_nextState;	// Next state to transition to, null for continuous
		float					_duration;	// Time this state should be active for
	};
}
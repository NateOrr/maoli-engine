//--------------------------------------------------------------------------------------
// File: Buffer.inl
//
// ID3D11Buffer wrapper template definition
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	template <class T>
	Buffer<T>::Buffer( Renderer* parent ) : Resource( parent )
	{
		_buffer = nullptr;
		_uav[0] = nullptr;
		_srv[0] = nullptr;
		_length = 0;
		_byteStride = sizeof(T);
		_mappedResource.pData = nullptr;
	}



	// Dtor
	template <class T>
	Buffer<T>::~Buffer()
	{
		Release();
	}


	// Release resources associated with this buffer.  The destructor will call this.
	template <class T>
	void Buffer<T>::Release()
	{
		SafeRelease( _buffer );
		SafeRelease( _uav[0] );
		SafeRelease( _srv[0] );
	}


	// Create a new buffer
	template <class T>
	HRESULT Buffer<T>::Create( int32 length, D3D11_USAGE usage, bool UAV, bool SRV, const T* data )
	{
		_length = length;

		// Create the page table
		D3D11_BUFFER_DESC sbDesc;
		ZeroMemory( &sbDesc, sizeof(sbDesc) );

		// Set bind flags
		if ( UAV&&SRV )
			sbDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
		else if ( UAV )
			sbDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
		else if ( SRV )
			sbDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

		// Set cpu usage
		if ( usage == D3D11_USAGE_DYNAMIC )
			sbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		else if ( usage == D3D11_USAGE_STAGING )
			sbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;

		// Create it
		sbDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
		sbDesc.StructureByteStride = _byteStride;
		sbDesc.ByteWidth = _byteStride * _length;
		sbDesc.Usage = usage;
		if ( data != nullptr )
		{
			D3D11_SUBRESOURCE_DATA initData;
			initData.pSysMem = data;
			H_RETURN( _parent->GetDevice()->CreateBuffer( &sbDesc, &initData, &_buffer ), "Failed to CreateBuffer" );
		}
		else
			H_RETURN( _parent->GetDevice()->CreateBuffer( &sbDesc, nullptr, &_buffer ), "Failed to CreateBuffer" );

		// Setup the UAV
		if ( UAV )
		{
			D3D11_UNORDERED_ACCESS_VIEW_DESC sbUAVDesc;
			sbUAVDesc.Buffer.FirstElement = 0;
			sbUAVDesc.Buffer.Flags = 0;
			sbUAVDesc.Buffer.NumElements = _length;
			sbUAVDesc.Format = DXGI_FORMAT_UNKNOWN;
			sbUAVDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
			H_RETURN( _parent->GetDevice()->CreateUnorderedAccessView( _buffer, &sbUAVDesc, &_uav[0] ), "Failed to CreateUnorderedAccessView" );
		}

		// Setup the SRV
		if ( SRV )
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC sbSRVDesc;
			sbSRVDesc.Buffer.ElementOffset = 0;
			sbSRVDesc.Buffer.ElementWidth = _byteStride;
			sbSRVDesc.Buffer.FirstElement = 0;
			sbSRVDesc.Buffer.NumElements = _length;
			sbSRVDesc.Format = DXGI_FORMAT_UNKNOWN;
			sbSRVDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
			H_RETURN( _parent->GetDevice()->CreateShaderResourceView( _buffer, &sbSRVDesc, &_srv[0] ), "Failed to CreateShaderResourceView" );
		}

		return S_OK;
	}


	// Create a vertex buffer
	template <class T>
	HRESULT Buffer<T>::CreateVertexBuffer( int32 length, D3D11_USAGE usage, const T* data )
	{
		D3D11_BUFFER_DESC bd;
		D3D11_SUBRESOURCE_DATA InitData;
		bd.Usage = usage;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = (usage == D3D11_USAGE_DYNAMIC) ? D3D11_CPU_ACCESS_WRITE : 0;
		bd.MiscFlags = 0;
		bd.ByteWidth = _byteStride * length;
		InitData.pSysMem = data;
		D3D11_SUBRESOURCE_DATA* dataPtr = data ? &InitData : nullptr;
		HRESULT hr = _parent->GetDevice()->CreateBuffer( &bd, dataPtr, &_buffer );
		if( FAILED(hr) )
		{ 
			//std::cout << "dat shit failed" << std::endl;
			Maoli::ErrorMessage("Failed to create vertex buffer");
			return hr;		
		}
		return S_OK;
	}

	// Create an index buffer
	template <class T>
	HRESULT Buffer<T>::CreateIndexBuffer( int32 length, D3D11_USAGE usage, const T* data )
	{
		D3D11_BUFFER_DESC bd;
		D3D11_SUBRESOURCE_DATA InitData;
		bd.Usage = usage;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = (usage == D3D11_USAGE_DYNAMIC) ? D3D11_CPU_ACCESS_WRITE : 0;
		bd.MiscFlags = 0;
		bd.ByteWidth = _byteStride * length;
		InitData.pSysMem = data;
		H_RETURN( _parent->GetDevice()->CreateBuffer( &bd, &InitData, &_buffer ), "Failed to create index buffer" );
		return S_OK;
	}



	// Clear to the given value
	template <class T>
	void Buffer<T>::ClearUAV( const uint32 values[4] )
	{
		if ( _uav[0] )
			_parent->GetContext()->ClearUnorderedAccessViewUint( _uav[0], values );
	}


	// Clear to the given value
	template <class T>
	void Buffer<T>::ClearUAV( const float values[4] )
	{
		if ( _uav[0] )
			g_pImmediateContext->ClearUnorderedAccessViewFloat( _uav[0], values );
	}


	// Maps the buffer memory (staging and dynamic buffers only).
	// Unmap() must be called before the buffer can be used again.
	template <class T>
	T* Buffer<T>::Map( D3D11_MAP mapping )
	{
		if ( SUCCEEDED( _parent->GetContext()->Map( _buffer, 0, mapping, 0, &_mappedResource ) ) )
			return static_cast<T*>(_mappedResource.pData);
		return nullptr;
	}


	// Completes the Map operation on the buffer
	template <class T>
	void Buffer<T>::Unmap()
	{
		if ( _mappedResource.pData )
		{
			_parent->GetContext()->Unmap( _buffer, 0 );
			_mappedResource.pData = nullptr;
		}
	}


	// Set the debugging name
	template <class T>
	void Buffer<T>::SetDebugName( const char* name )
	{
#if defined(_DEBUG) || defined(PROFILE)
		_buffer->SetPrivateData( WKPDID_D3DDebugObjectName, strlen( name ), name );
		PipeplineResource::SetDebugName( name );
#endif
	}

	// Update a portion of the buffer
	template <class T>
	void Buffer<T>::Update( const T* data, uint32 start, uint32 count )
	{
		D3D11_BOX region = { start*_byteStride, 0, 0, count*_byteStride, 1, 1 };
		_parent->GetContext()->UpdateSubresource( _buffer, 0, &region, data, _byteStride, 0 );
	}
}

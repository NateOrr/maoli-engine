//--------------------------------------------------------------------------------------
// File: MeshInstance.cpp
//
// Simple mesh instance
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "MeshInstance.h"

namespace Maoli
{

	// Build the world matrix for this instance
	void MeshInstance::Update()
	{
		Matrix rotationMatrix = Matrix::CreateRotationYawPitchRoll( _rotation.y, _rotation.x, _rotation.z );
		Matrix scaleMatrix = Matrix::CreateScaling( _scale );
		Matrix translationMatrix = Matrix::CreateTranslation( _position );
		_worldMatrix = scaleMatrix * rotationMatrix * translationMatrix;

		Matrix mat = scaleMatrix * rotationMatrix;
		Vector3 center = mat.Transform( _mesh->GetGeometry()->GetCenter() );
		_bounds.Set( _position + center, mat.TransformNormal( _mesh->GetGeometry()->GetBounds() ).Absolute() );
	}

}
//--------------------------------------------------------------------------------------
// File: Font.cpp
//
// Bitmap fonts
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Font.h"

namespace Maoli
{

	// Ctor
	Font::Font(Renderer* graphics) : Resource(graphics)
	{
		_sheet = nullptr;
		_adjusted = false;
	}

	// Dtor
	Font::~Font(void)
	{
		Release();
	}

	// Cleanup
	void Font::Release()
	{
		_sheet = nullptr;
	}


	// Load from a file
	bool Font::Load( const char* imagefile )
	{
		// Load in the texture
		_sheet = _parent->CreateTextureFromFile(imagefile);
		if(!_sheet)
			return false;

		// Load the data file
		std::string fileName = imagefile;
		fileName = fileName.substr(0, fileName.length()-4) + " subimages.txt";
		std::ifstream fin(fileName);
		if(!fin.is_open())
			return false;

		// Setup the characters
		Rect region;
		int32 currentChar;
		std::string line;
		while(!fin.eof())
		{
			// Read the data for this character
			getline(fin, line);
			if(fin.eof())
				break;

			// Parse the line
			sscanf_s(line.c_str(), "%d:%d:%d:%d:%d", &currentChar, &region.x, &region.y,
				&region.width, &region.height);
			_characters.insert(std::pair<char, Rect>((char)currentChar, region));
		}

		fin.close();
		return true;
	}

	// Set the width adjustment
	void Font::SetWidthAdjustment( int32 w )
	{
		if(!_adjusted)
		{
			_adjusted = true;
			for(auto i=_characters.begin(); i!=_characters.end(); ++i)
			{
				Rect& r = i->second;
				r.x += w;
				r.width -= 2*w;
			}
		}
	}

}

//--------------------------------------------------------------------------------------
// File: Mesh.cpp
//
// 3D Mesh
// A mesh consists of a submesh for each material.
// This is simply a data container, meshes are instantiated using Model
// or DynamicMesh.  This allow for the same mesh data to be used for multiple
// objects with little increase in required memory.
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "MeshBinding.h"
#include "Graphics/Model.h"

namespace Maoli
{
	// Constructor
	static uint32 g_MeshBindingID = 0;
	MeshBinding::MeshBinding( Renderer* graphics ) : Resource( graphics )
	{
		_id = g_MeshBindingID++;
	}


	// Destructor
	MeshBinding::~MeshBinding()
	{
		Release();
	}

	// Destructor
	void MeshBinding::Release()
	{
		_positionVertexBuffer = nullptr;
		_texCoordVertexBuffer = nullptr;
		_normalVertexBuffer = nullptr;
		_boneWeightVertexBuffer = nullptr;
		_indexBuffer = nullptr;
		_renderNodes.Release();
	}

	// Create from geometry
	HRESULT MeshBinding::Init( Geometry* geometry )
	{
		if ( _geometry == geometry )
			return S_OK;

		_geometry = geometry;
		_renderNodes.Allocate( geometry->GetNumChunks( ) );
		return Update();
	}

	// Updates the buffers
	HRESULT MeshBinding::Update()
	{
		// Release the old buffers
		_positionVertexBuffer = nullptr;
		_texCoordVertexBuffer = nullptr;
		_normalVertexBuffer = nullptr;
		_boneWeightVertexBuffer = nullptr;
		_indexBuffer = nullptr;

		// Create and fill the vertex buffers
		_positionVertexBuffer = _parent->CreateVertexBuffer<Vector3>( _geometry->GetNumVerts(), D3D11_USAGE_IMMUTABLE, _geometry->GetVerts() );
		if ( !_positionVertexBuffer )
		{
			M_ERROR( "Failed to create position vertex buffer" );
			return E_FAIL;
		}
		_texCoordVertexBuffer = _parent->CreateVertexBuffer<Vector2>( _geometry->GetNumVerts(), D3D11_USAGE_IMMUTABLE, _geometry->GetTexCoords() );
		if ( !_texCoordVertexBuffer )
		{
			M_ERROR( "Failed to create tex coord vertex buffer" );
			return E_FAIL;
		}
		_normalVertexBuffer = _parent->CreateVertexBuffer<Vector3>( _geometry->GetNumVerts(), D3D11_USAGE_IMMUTABLE, _geometry->GetNormals() );
		if ( !_normalVertexBuffer )
		{
			M_ERROR( "Failed to create normals vertex buffer" );
			return E_FAIL;
		}

		// Bone weights for animated meshes
		if ( _geometry->HasBoneWeights() )
		{
			_boneWeightVertexBuffer = _parent->CreateVertexBuffer<BoneWeight>( _geometry->GetNumVerts(), D3D11_USAGE_IMMUTABLE, _geometry->GetBoneWeights() );
			if ( !_boneWeightVertexBuffer )
			{
				M_ERROR( "Failed to create bone weight vertex buffer" );
				return E_FAIL;
			}
		}

		// Create and fill the index buffer
		_indexBuffer = _parent->CreateIndexBuffer<uint32>( _geometry->GetNumIndices(), D3D11_USAGE_IMMUTABLE, _geometry->GetIndices() );
		if ( !_indexBuffer )
		{
			M_ERROR( "Failed to create the index buffer" );
			return E_FAIL;
		}

		// Set the buffers for the submesh array
		for ( uint32 i = 0; i < _geometry->GetNumChunks(); i++ )
		{
			_renderNodes[i].SetGraphics( dynamic_cast<Renderer*>(_parent) );
			_renderNodes[i].indexCount = _geometry->GetChunk( i )->indexCount;
			_renderNodes[i].startIndex = _geometry->GetChunk( i )->startIndex;
			_renderNodes[i].SetMaterialBinding( _parent->CreateMaterialBinding( _geometry->GetChunk( i )->material ) );
			_renderNodes[i].vertexBuffers[VERTEX_STREAM_POS] = _positionVertexBuffer->GetBuffer();
			_renderNodes[i].vertexBuffers[VERTEX_STREAM_TEXCOORD] = _texCoordVertexBuffer->GetBuffer();
			_renderNodes[i].vertexBuffers[VERTEX_STREAM_NORMAL] = _normalVertexBuffer->GetBuffer();
			_renderNodes[i]._index = i;
			if ( _boneWeightVertexBuffer )
				_renderNodes[i].vertexBuffers[VERTEX_STREAM_BONE] = _boneWeightVertexBuffer->GetBuffer();
			_renderNodes[i].indexBuffer = _indexBuffer->GetBuffer();
		}

		// Update all models that use this mesh
		for ( uint32 i = 0; i < _models.Size(); ++i )
			_models[i]->UpdateSubMeshes();

		return S_OK;
	}

}

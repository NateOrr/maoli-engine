//--------------------------------------------------------------------------------------
// File: Water.h
//
// Ocean rendering
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/MaterialBinding.h"
#include "Texture.h"

namespace Maoli
{
	// Forward decl
	class Renderer;

	class Ocean
	{
	public:

		// Ctor
		Ocean(Renderer* graphics);

		// Dtor
		~Ocean();

		// Setup
		HRESULT Init();
		
		// Frame update
		void Update(float dt);

		// Cleanup
		void Release();

		// Setup effect variables
		void BindEffectVariables();

		// Binary serialize
		void Export( std::ofstream& fout );

		// Binary file import
		void Import( std::ifstream& fin );


		// Get the wave field
		inline Texture::pointer GetWaveField() const { return _waveField; }

		// Get the wave normals
		inline Texture::pointer GetWaveNormals() const { return _waveNormals; }

		// Get the material
		inline MaterialBinding::pointer GetMaterial() const { return _materialBinding; }

		// Get the vertex buffer
		inline ID3D11Buffer* GetVertexBuffer(){ return _vertexBuffer->GetBuffer(); }

		// Get the index buffer
		inline ID3D11Buffer* GetIndexBuffer(){ return _indexBuffer->GetBuffer(); }

		// Get the number of indices to render
		inline int32 GetNumIndices() const { return _numIndices; }


		// Set the wave height
		inline void SetWaveHeightScale(float f){ _waveHeightScale = f; }	

		// Get the wave height
		inline float GetWaveHeightScale() const { return _waveHeightScale; }

		// Set the wave scale
		inline void SetWaveDisplacementScale(float f){ _waveDisplacementScale = f; }

		// Get the wave scale
		inline float GetWaveDisplacementScale() const { return _waveDisplacementScale; }

		// Set the water level
		void SetWaterLevel(float height);	

		// Get the water level
		inline float GetWaterLevel() const { return _waterLevel; }

		// Set the wind direction
		void SetWindDirection( float x, float z );
		void SetWindDirection( const Vector2& value );

		// Get the wind direction
		inline const Vector2& GetWindDirection() const { return _windDirection; }

		// Set the wind velocity
		void SetWindVelocity(float v);

		// Get the wind velocity
		inline float GetWindVelocity() const { return _windVelocity; }

		// Set the wave facet size
		void SetWaveFacetSize(float f);

		// Get the wave facet size
		inline float GetWaveFacetSize() const { return _waveFacetSize; }

		// Set the wave constant
		void SetWaveConstant(float f);

		// Get the wave constant
		inline float GetWaveConstant() const { return _waveConstant; }

		// Set the wave gravity
		void SetWaveGravity(float f);

		// Get the wave gravity
		inline float GetWaveGravity() const { return _waveGravity; }

		// Set the wave length
		void SetWaveLength(float f);	

		// Get the wavelength
		inline float GetWaveLength() const { return _waveLength; }

		// Set the extinction color
		void SetExtinctionColor( float r, float g, float b );
		void SetExtinctionColor( const Color& value );

		// Get the extinction color
		inline const Color& GetExtinctionColor() const { return _extinctionColor; }

		// Set the backscatter color
		void SetBackscatterColor(float r, float g, float b);
		void SetBackscatterColor( const Color& value );

		// Get the backscatter color
		inline const Color& GetBackscatterColor() const { return _backScatterColor; }

		// Set the shader data
		void SetShaderData();

	private:

		// Setup the wavefield
		void CreateWaveField();

		// Rendering stuff
		Renderer*					_graphics;			// Graphics engine
		Texture::pointer			_normalMaps[2];		// Normal maps, animated based on time
		MaterialBinding::pointer	_materialBinding;	// Material for water rendering
		Buffer<Vertex>::pointer		_vertexBuffer;		// Vertex buffer for the water grid
		Buffer<uint32>::pointer		_indexBuffer;		// Index buffer for the water grid
		int32							_numIndices;		// Total number of grid indices
		Texture::pointer			_waveField;			// Wave height field texture (xz = displacement, y = height)
		Texture::pointer			_waveNormals;		// Normals for the wave field
		int32							_gridSize;			// Heightfield grid size

		// Properties (names are self-explainatory, see Tessendorf paper for details)
		float				_waterLevel;
		Vector2				_windDirection;
		float				_windVelocity;
		float				_waveFacetSize;		
		float				_waveConstant;
		float				_waveGravity;
		float				_waveLength;
		float				_waveHeightScale;
		float				_waveDisplacementScale;
		Color				_extinctionColor;
		Color				_backScatterColor;

		// FFT stuff		
		Math::Complex*		 _fftHoPos;				// Precomputed spectral input to the FFT, contains Ho(K)	
		Math::Complex*		 _fftHoNeg;				// Precomputed spectral input to the FFT, contains Ho(-K)	
		Math::Complex*       _fftIn;				// Input to the FFT
		Math::Complex*       _fftInDX;				// Input to the FFT
		Math::Complex*       _fftInDZ;				// Input to the FFT
		Math::Complex*       _fftHeight;			// FFT results, water height is the real component
		Math::Complex*       _fftDisplacementX;		// FFT results, displacement X component
		Math::Complex*       _fftDisplacementZ;		// FFT results, displacement Z component

		// Effect variables
		ShaderScalar*			WaterHeightVariable;
		ShaderVector*			WaterExtinctionColorVariable;
		ShaderVector*			WaterBackscatterColorVariable;
	};
}

//--------------------------------------------------------------------------------------
// File: Terrain.h
//
// Terrain using gpu geometry clipmaps
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/MaterialBinding.h"
#include "Texture.h"

namespace Maoli
{

	// Forward decl
	class Renderer;


#define CLIPMAP_LEVELS 8

	// Perform sculpting
	enum SCULPT_TYPE
	{
		SCULPT_RAISE = 0,
		SCULPT_LOWER,
		SCULPT_GRAB,
		SCULPT_SMOOTH,
		SCULPT_RAMP,
		SCULPT_NOISE,
		SCULPT_EROSION,
		SCULPT_PAINT,
	};

	// Terrain rendering modes
	enum class TerrainMode
	{
		Blendmap,		// Single blend texture map
	};

	// Painting modes
	enum PAINT_TYPE
	{
		PAINT_TILE = 0,
		PAINT_PROJECT,
	};


	class Terrain
	{
		friend class Renderer;
		friend class DebugVisualizer;
	public:

		// Ctor
		Terrain( Engine* engine );

		// Dtpr
		~Terrain();
	

		// Binary serialize
		void Export( std::ofstream& fout );

		// Binary file import
		void Import( std::ifstream& fin );

		// Creation
		bool CreateFromFile( const char* szFile );
		bool Create( UINT size );

		// Saving
		void ExportHeightmap( const char* file );
		void ExportBlendmap( const char* file );

		// Destroy the terrain
		void Release();

		// Set the rendering mode
		void SetRenderMode( TerrainMode mode );

		// Get the rendering mode
		TerrainMode GetRenderMode() const { return m_RenderMode; }

		// Create the blendmap
		bool CreateBlendmap();

		// Load a blendmap from file
		bool LoadBlendmap( const char* file );

		// Bind all effect variables
		void BindEffectVariables();

		// Vertex/index buffers
		inline ID3D11Buffer* GetVertexBuffer() { return m_VertexBuffer; }
		inline ID3D11Buffer* GetIndexBuffer() { return m_IndexBuffer; }

		// Gets the world matrix
		inline Matrix& GetWorldMatrix() { return m_WorldMatrix; }

		// Returns the name
		inline const String& GetName() { return m_szName; }

		// Is the terrain loaded?
		inline bool IsLoaded() { return m_bLoaded; }

		// Returns the heightfield
		inline float* GetHeightfield() { return m_Height; }

		// Blend map size
		inline int32 GetBlendMapSize() { return m_BlendMapSize; }


		// Sets the size scale (not working)
		inline void SetScale( float f )
		{
			/*float d = 2.0f * ((f-m_Scale) / (m_Scale+f));
			m_Material.SetDiffuse(m_Material.GetDiffuse() + m_Material.GetDiffuse()*d);
			m_Scale = f;
			UpdateWorldMatrix(); */
		}

		// Set the factor to scale the heightmap by
		inline void SetHeightScale( float f ) { m_HeightExtents /= m_HeightScale;  m_HeightScale = f; m_HeightExtents *= f; UpdateWorldMatrix(); }

		// Get scales
		inline int32 GetSize() { return m_Size; }
		inline float GetScale() { return m_Scale; }
		inline float GetHeightScale() { return m_HeightScale; }

		// Performs a ray intersection test with the terrain
		bool RayIntersection( const Vector3& rayPos, const Vector3& rayDir, float& dist );

		// Access the engine that owns the terrain
		Engine* GetEngine();


	private:

		// Create a terrain from a heightmap
		HRESULT LoadTerrain( const char* szFile, ID3D11DepthStencilView* pDSV );
		HRESULT CreateFromTexture( ID3D11Texture2D* pHeightmap );

		// Internal data loading
		UINT LoadHeightMap( ID3D11Texture2D* pHeightmap );

		Renderer*			_graphics;
		String				m_szName;			// File name
		bool				m_bLoaded;			// True when a terrain is loaded
		float				m_Scale;			// Scale factor for the terrain
		float				m_HeightScale;		// Height scale factor
		int32					m_Size;				// Heightmap dimensions
		Vector3				m_vPos;				// Center of the heightmap
		Array<float>		m_Height;			// Height list
		Matrix				m_WorldMatrix;		// World transform
		TerrainMode			m_RenderMode;		// Rendering mode


		// Effect Variables
		ShaderVector*			ClipmapOffsetVariable;
		ShaderScalar*			ClipmapScaleVariable;
		ShaderVector*			ClipmapFixVariable;
		ShaderScalar*			ClipmapSizeVariable;
		ShaderScalar*			HeightmapSizeVariable;
		ShaderScalar*			HeightmapScaleVariable;
		ShaderResource*			HeightmapVariable;
		ShaderResource*			BlendmapVariable;
		ShaderResource*			ClipmapVariable;
		ShaderResource*			TerrainDiffuseVariable;
		ShaderResource*			TerrainNormalVariable;
		ShaderResource*			TerrainRoughnessVariable;
		ConstantBuffer*         TerrainMaterialCB;

		// Effect passes
		EffectPass*				_clipmapUpdatePass;		// Updates all clipmaps


		// Updates and rendering

		// Draws the full terrain
		int32 Render( EffectPass* effectPass );

		// Draws the terrain in debug visualization
		void RenderDebug( Model& boxMesh, Effect& effect );

		// Update a clipmap
		// (must set quad vertex buffer first)
		void UpdateClipmaps( Effect& effect );

		// Updates the world matrix
		void UpdateWorldMatrix();

		// Asyncronous updating
		void UpdateBuffers();


		// Sculpting
	public:

		// Paint to the blend map
		void PaintTerrain( const Vector3& pos, float radius );

		// Terrain sculpting
		void Sculpt( Vector3 center, float radius, float hardness, float strength, float delta, int32 detail, SCULPT_TYPE type );

		// Cache the region about to get sculpted into the undo stack
		void CacheSculptRegion( Vector3 minPoint, float radius );

		// Reverse a sculpt/paint action
		void UndoSculpt();

		// Redo a sculpt/paint action
		void RedoSculpt();

		// Get the total memory usage of the sculpter
		inline double GetMemoryUsage() { return m_UndoMemUsage; }

	private:

		// Maximum memory usage by the undo system
		double m_UndoMaxMem;
		double m_UndoMemUsage;

		// Undo and redo is build in here to simplify things
		struct SculptRegion
		{
			ID3D11Texture2D* tex;
			D3D11_BOX region;
			double mem;
		};
		Stack<SculptRegion> m_UndoStack;
		Stack<SculptRegion> m_RedoStack;

		// Internal helper for undo
		void ApplyUndo( SculptRegion &r );

	public:

		// Get the number of supported layers
		inline uint32 GetNumLayers() const { return m_MaxLayers; }

		// Get a layer material
		inline MaterialBinding::pointer GetLayerMaterial( int32 i ) { return m_Layers[i]->materialBinding; }

		// Load a layer mask
		void LoadLayerMask( const char* file, int32 i );

		// Get a layer mask file name
		const String& GetLayerMaskName( int32 i );

		// Clear a mask texture
		void ClearLayerMask( int32 i );

		// Mask properties
		inline PAINT_TYPE GetPaintMode( int32 i )
		{
			return m_Layers[i]->PaintMode;
		}
		inline PAINT_TYPE GetMaskMode( int32 i )
		{
			return m_Layers[i]->MaskMode;
		}
		inline bool GetMaskInvert( int32 i )
		{
			return m_Layers[i]->MaskInvert;
		}
		inline bool GetMaskInvertColor( int32 i )
		{
			return m_Layers[i]->MaskInvertColor;
		}
		inline bool GetMaskAbsolute( int32 i )
		{
			return m_Layers[i]->MaskAbsolute;
		}
		inline bool GetMaskColor( int32 i )
		{
			return m_Layers[i]->MaskColor;
		}
		inline bool GetMaskModulate( int32 i )
		{
			return m_Layers[i]->MaskModulate;
		}
		inline void SetPaintMode( PAINT_TYPE mode, int32 i )
		{
			m_Layers[i]->PaintMode = mode;
		}
		inline void SetMaskMode( PAINT_TYPE mode, int32 i )
		{
			m_Layers[i]->MaskMode = mode;
			BindLayer( i );
		}
		inline void SetMaskInvert( bool b, int32 i )
		{
			m_Layers[i]->MaskInvert = b;
			BindLayer( i );
		}
		inline void SetMaskInvertColor( bool b, int32 i )
		{
			m_Layers[i]->MaskInvertColor = b;
			BindLayer( i );
		}
		inline void SetMaskAbsolute( bool b, int32 i )
		{
			m_Layers[i]->MaskAbsolute = b;
			BindLayer( i );
		}
		inline void SetMaskColor( bool b, int32 i )
		{
			m_Layers[i]->MaskColor = b;
			BindLayer( i );
		}
		inline void SetMaskModulate( bool b, int32 i )
		{
			m_Layers[i]->MaskModulate = b;
			BindLayer( i );
		}

		// Attaches a material to a layer
		// Note: matID is only used for gui
		void AttachMaterial( MaterialBinding::pointer material, int32 i );

		// Binds the textures and normal maps
		void BindLayer( int32 i, bool mask = false );

		// Binds all paint layer materials for terrain rendering
		void BindLayerMaterials();

		// Update the materials in the shader
		void UpdateLayerMaterials();

		// Get the current layer material
		MaterialBinding::pointer GetCurrentLayerMaterial() { return m_Layers[m_CurrentLayer]->materialBinding; }

		// Get the default material
		MaterialBinding::pointer GetDefaltMaterial() { return m_DefaultMaterial; }

		// Set the default material
		void SetDefaultMaterial( MaterialBinding::pointer m ) { m_DefaultMaterial = m; }

	private:

		// Texture layers for painting
		struct PaintLayer
		{
			PaintLayer( MaterialBinding::pointer material );

			// [0]=min, [1]=max
			float Elevation[2];
			float Slope[2];
			bool MaskInvert;
			bool MaskInvertColor;
			bool MaskAbsolute;
			bool MaskColor;
			bool MaskModulate;

			// Drawing modes
			PAINT_TYPE MaskMode;
			PAINT_TYPE PaintMode;

			// Material
			MaterialBinding::pointer materialBinding;

			// Masks the painting region
			Texture::pointer Mask;
		};


		static const uint32           m_MaxLayers = 4;

		// Layer properties
		struct TerrainLayer
		{
			Color	color;		// Diffuse color for each layer
			Vector2	texScale;	// Texture scale for each layer
			float	roughness;	// Specular color for each layer
			uint32	texFlag;	// Bitfield indicates which textures are loaded
		};

		// Material constant buffer for terrains
		struct
		{
			bool	coneStepMapping;	// True if csm is active
			int32		csmSamples;			// Max number of samples to use during cone step mapping
			float	csmScale;			// Scaling factor for cone step mapping displacement appearence
			int32		pad;				// 16 byte padding

			TerrainLayer layers[m_MaxLayers];	// Layer data
		} cbTerrainMaterial;


		Texture::pointer			m_TerrainDiffuse[m_MaxLayers];
		Texture::pointer			m_TerrainNormal[m_MaxLayers];
		Texture::pointer			m_TerrainSpecular[m_MaxLayers];
		PaintLayer*                 m_Layers[m_MaxLayers];
		int32					        m_CurrentLayer;

		ShaderResource*				PaintMaskVariable;
		ShaderScalar*				PaintMaskFlagVariable;
		ShaderScalar*				PaintMaskAbsoluteVariable;
		ShaderScalar*				PaintMaskInvertVariable;
		ShaderScalar*				PaintMaskInvertColorVariable;
		ShaderScalar*				PaintMaskModeVariable;
		ShaderScalar*				PaintModeVariable;
		ShaderScalar*				PaintMaskColorVariable;
		ShaderScalar*				PaintMaskModulateVariable;
		ShaderVector*				PaintLayerSizeVariable;
		ShaderScalar*				PaintLayerIDVariable;
		ShaderVector*				PaintRegionVariable;
		ShaderVector*				MouseIntersectVariable;
		ShaderUAV*					_paintingUAVVariable;
		EffectPass*					PaintTerrainPass;

		// For efficiency reasons, virtual textures are sparse.
		// Pages are only stored on disk if they are used.
		// This material is needed as a fall back to smoothly
		// transition between regions that have active VT pages
		// and those that are "empty".
		MaterialBinding::pointer			m_DefaultMaterial;

		// Clipmap system
	private:

		// Heightmaps
		Texture::pointer	m_Heightmap;
		ID3D11Texture2D*	m_StagingHeightmap;

		// Blend texture
		int32					m_BlendMapSize;
		Texture::pointer	m_Blendmap;

		// The min and max heights
		Vector2			m_HeightExtents;

		// Creates the clipmap building blocks
		void SetupClipmap( int32 size, int32 numLevels );

		// Clipmaps
		int32					m_ClipmapSize;		// N
		int32					m_BlockSize;		// M = (N+1)/4
		int32					m_ClipmapLevels;	// Max number of active levels
		Texture::pointer    m_Clipmaps;

		// Trim positioning
		BYTE*			m_TrimXSide;
		BYTE*			m_TrimZSide;
		Vector2*		m_ClipmapOffsets;

		// Updating
		bool			m_FlagForUpdate;

		// Block positioning
		Vector2 m_BlockOffsets[12];

		// Vertex buffer offsets
		enum VB_OFFSETS
		{
			VB_BLOCK = 0,
			VB_RING1,
			VB_RING2,
			VB_TRIM_TL,
			VB_TRIM_TR,
			VB_TRIM_BR,
			VB_TRIM_BL,
			VB_CENTER_TRIM,

			VB_OFFET_LENGTH,
		};

		// Index buffer offsets
		enum IB_OFFSETS
		{
			IB_BLOCK = 0,
			IB_RING1,
			IB_RING2,
			IB_TRIM,
			IB_CENTER_TRIM,

			IB_OFFET_LENGTH,
		};

		// Vertex/index buffers
		ID3D11Buffer*		m_VertexBuffer;
		int32					m_VBOffsets[VB_OFFET_LENGTH];
		ID3D11Buffer*		m_IndexBuffer;
		int32					m_IBOffsets[IB_OFFET_LENGTH];
		int32					m_IBCount[IB_OFFET_LENGTH];



		// Spatial optimization

		// This quadtree is purely for fast ray-intersection tests at this point
		struct QuadtreeNode
		{
			Vector3 pos;
			Vector3 size;
			QuadtreeNode* pChildNodes[4];
			bool isLeaf;
			bool gotHit;

			QuadtreeNode()
			{
				pChildNodes[0] = nullptr;
				pChildNodes[1] = nullptr;
				pChildNodes[2] = nullptr;
				pChildNodes[3] = nullptr;
				isLeaf = false;
				gotHit = false;
			}

			~QuadtreeNode()
			{
				SafeDelete( pChildNodes[0] );
				SafeDelete( pChildNodes[1] );
				SafeDelete( pChildNodes[2] );
				SafeDelete( pChildNodes[3] );
			}
		};
		QuadtreeNode* m_pRootNode;

		// Builds a very basic quadtree down to a minimal node size
		void BuildQuadtree( QuadtreeNode* pNode, float minNodeSize );

		// Returns the leaf node hit by the ray
		float RayNodeIntersect( QuadtreeNode* pNode, const Vector3& rayPos, const Vector3& rayDir );
		float RayLeafIntersection( QuadtreeNode*, const Vector3& rayPos, const Vector3& rayDir );


		// Quadtree visualization
		void RenderQuadtreeDebug( Model& boxMesh, Effect& effect );
		void RenderNodeDebug( QuadtreeNode* pNode, Model& boxMesh, Effect& effect );

		// Update the node heights
		inline void UpdateTreeHeights() { UpdateNodeHeight( m_pRootNode ); }
		void UpdateNodeHeight( QuadtreeNode* pNode );
	};

}

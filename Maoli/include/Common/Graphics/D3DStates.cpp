//--------------------------------------------------------------------------------------
// File: D3DStates.cpp
//
// State management
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/D3DStates.h"

namespace Maoli
{
	// Ctor
	FrameStats::FrameStats()
	{
		Reset();
	}

	// Reset the stats
	void FrameStats::Reset()
	{
		materialChanges = indexBufferChanges = vertexBufferChanges = drawCalls = polygonsProcessed = polygonsDrawn = processedMessages = resourceBinds = 0;
	}


	// Ctor
	RenderState::RenderState()
	{
		_context = nullptr;
		ResetBindings();
	}

	// Reset all bindings
	void RenderState::ResetBindings()
	{
		Reset();
		_currentStream = 0;
		_materialBinding = nullptr;
		_inputLayout = nullptr;
		_cubemap = nullptr;
		_topology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
		_vertexBuffer = nullptr;
		_indexBuffer = nullptr;
		_blendState = nullptr;
		_depthStencilState = nullptr;
		_rasterizerState = nullptr;

		// Clear shaders
		for ( uint32 i = 0; i < (int32)ShaderType::NumTypes; ++i )
			_shaders[i] = nullptr;

		// Zero out the resources
		memset( _resourceBindings, 0, int32( ShaderType::NumTypes ) * _maxResourceBindings * sizeof(PipeplineResource*) );
		memset( _resourceViews, 0, int32( ShaderType::NumTypes ) * _maxResourceBindings * sizeof(ID3D11ShaderResourceView*) );
		memset( _constantBuffers, 0, int32( ShaderType::NumTypes ) * _maxConstantBufferBindings * sizeof(ConstantBuffer*) );
		memset( _samplerStates, 0, int32( ShaderType::NumTypes ) * _maxSamplerBindings * sizeof(ID3D11SamplerState*) );
	}

	// Reset the state
	void RenderState::Reset()
	{
		_frameStats.Reset();
		_materialBinding = nullptr;
	}


	// Clear all output bindings from the graphics pipeline
	void RenderState::ClearOutputBindings( bool nullTarget )
	{
		if ( _outputTargets.Size() > 0 )
		{
			for ( uint32 i = 0; i<_outputTargets.Size(); ++i )
			{
				_outputTargets[i]->_outputBinding.boundToPipeline = false;
				_outputTargets[i]->_outputBinding.fullyBound = false;
			}
			_outputTargets.Clear();
			if ( nullTarget )
			{
				ID3D11RenderTargetView* nullRTV[1] = { nullptr };
				_context->OMSetRenderTargets( 1, nullRTV, nullptr );
			}
		}

		if ( _computeUAVs.Size()>0 )
		{
			for ( uint32 i = 0; i < _computeUAVs.Size(); ++i )
				_computeUAVs[i]->_outputBinding.boundToPipeline = false;
			_computeUAVs.Clear();
			ID3D11UnorderedAccessView* nulluav[8] = { nullptr };
			_context->CSSetUnorderedAccessViews( 0, 8, nulluav, nullptr );
		}
	}


	// Clear input bindings from a texture
	void RenderState::ClearInputBindings( PipeplineResource* resource )
	{
		static ID3D11ShaderResourceView* nullsrv[1] = { nullptr };
		for ( auto iter = resource->_inputBindings.Begin(); !iter.End(); ++iter )
		{
			InputBinding& binding = *iter;
			(_context->*Shader::VTable[binding.shaderType].pBindShaderResource)(binding.slot, 1, nullsrv);
			_resourceViews[binding.shaderType][binding.slot] = nullptr;
			_resourceBindings[binding.shaderType][binding.slot] = nullptr;
		}
		resource->_inputBindings.Clear();
	}


	// Bind a single UAV to the graphics pipeline
	void RenderState::BindUnorderedAccessView( uint32 slot, PipeplineResource* resource )
	{
		// Clear the other targets
		ClearOutputBindings( false );

		// Unbind the current render target if it's bound to input
		ClearInputBindings( resource );

		// Set the render target
		_context->OMSetRenderTargetsAndUnorderedAccessViews( 0, nullptr, nullptr, slot, 1, &resource->GetUAV()[0], nullptr );
		resource->_outputBinding.boundToPipeline = true;
		_outputTargets.Add( resource );
	}


	// Set render targets and uavs for the graphics pipeline
	void RenderState::BindRenderTargetsAndUnorderedAccessViews( PipeplineResource* rtvResource, uint32 numResources, PipeplineResource* uavResource, uint32 uavSlot, uint32 uavCount )
	{
		// Clear the other targets
		ClearOutputBindings( false );

		// Unbind the current render target if it's bound to input
		ClearInputBindings( rtvResource );
		if ( rtvResource->GetDSV() )
			ClearInputBindings( *rtvResource->GetDSV() );

		// Bind
		ID3D11DepthStencilView* dsv = rtvResource->GetDSV() ? rtvResource->GetDSV()->GetDSV() : nullptr;
		_context->OMSetRenderTargetsAndUnorderedAccessViews( numResources, rtvResource->GetRTV(), dsv, uavSlot, uavCount, uavResource->GetUAV(), nullptr );

		// Set this as bound
		_outputTargets.Add( rtvResource );
		_outputTargets.Add( uavResource );
		rtvResource->_outputBinding.boundToPipeline = true;
		uavResource->_outputBinding.boundToPipeline = true;
	}

	// Bind only a depth stencil to the graphics output
	void RenderState::BindDepthStencilWithoutRenderTarget( DepthStencil::pointer depthStencil )
	{
		ClearOutputBindings( false );
		_context->OMSetRenderTargets( 0, 0, depthStencil->GetDSV() );
	}


	// Clear the render target/dsv
	void RenderState::ClearRenderTarget()
	{
		ID3D11RenderTargetView* nullRTV[1] = { nullptr };
		_context->OMSetRenderTargets( 1, nullRTV, nullptr );
	}

	// 	Bind the render target to the d3d11 pipeline
	void RenderState::BindRenderTarget( PipeplineResource* resource, uint32 index )
	{
		// Clear the other targets
		ClearOutputBindings( false );

		// Unbind the current render target if it's bound to input
		ClearInputBindings( resource );
		if ( resource->GetDSV() )
			ClearInputBindings( *resource->GetDSV() );

		// Set the render target
		ID3D11DepthStencilView* dsv = resource->GetDSV() ? resource->GetDSV()->GetDSV() : nullptr;
		_context->OMSetRenderTargets( 1, &resource->GetRTV()[index], dsv );

		// Set this as bound
		_outputTargets.Add( resource );
		resource->_outputBinding.boundToPipeline = true;
		resource->_outputBinding.index = index;
	}

	// 	Bind the render target to the d3d11 pipeline using a read-only depth buffer
	void RenderState::BindRenderTargetWithReadOnlyDepth( PipeplineResource* resource, uint32 index )
	{
		// Clear the other targets
		ClearOutputBindings( false );

		// Unbind the current render target if it's bound to input
		ClearInputBindings( resource );

		// Set the render target
		ID3D11DepthStencilView* dsv = resource->GetDSV() ? resource->GetDSV()->GetReadOnlyDSV() : nullptr;
		_context->OMSetRenderTargets( 1, &resource->GetRTV()[index], dsv );

		// Set this as bound
		_outputTargets.Add( resource );
		resource->_outputBinding.boundToPipeline = true;
		resource->_outputBinding.index = index;
	}

	// 	Bind the render target to the d3d11 pipeline without a dsv
	void RenderState::BindRenderTargetWithoutDepth( PipeplineResource* resource, uint32 index )
	{
		// Clear the other targets
		ClearOutputBindings( false );

		// Unbind the current render target if it's bound to input
		ClearInputBindings( resource );

		// Set the render target
		_context->OMSetRenderTargets( 1, &resource->GetRTV()[index], nullptr );

		// Set this as bound
		_outputTargets.Add( resource );
		resource->_outputBinding.boundToPipeline = true;
		resource->_outputBinding.index = index;
	}

	// 	Bind render target array to the d3d11 pipeline
	void RenderState::BindRenderTargetArray( PipeplineResource* resource, uint32 numResources )
	{
		// Clear the other targets
		ClearOutputBindings( false );

		// Unbind the current render target if it's bound to input
		ClearInputBindings( resource );
		if ( resource->GetDSV() )
			ClearInputBindings( *resource->GetDSV() );

		// Set the render target
		ID3D11DepthStencilView* dsv = resource->GetDSV() ? resource->GetDSV()->GetDSV() : nullptr;
		if ( numResources == 0 )
			_context->OMSetRenderTargets( resource->GetArraySize(), resource->GetRTV(), dsv );
		else
			_context->OMSetRenderTargets( numResources, resource->GetRTV(), dsv );

		// Set this as bound
		resource->_outputBinding.boundToPipeline = true;
		resource->_outputBinding.fullyBound = true;
		_outputTargets.Add( resource );
	}


	// Add on a vertex stream
	void RenderState::AddVertexStream( ID3D11Buffer* pStream, uint32 stride )
	{
		_vertexStreams[_currentStream] = pStream;
		_vertexStrides[_currentStream] = stride;
		++_currentStream;
	}

	// Bind the available streams and reset them
	void RenderState::BindVertexStreams()
	{
		uint32 offsets[VERTEX_STREAM_MAX] = { 0 };
		_vertexBuffer = nullptr;
		_context->IASetVertexBuffers( 0, _currentStream, _vertexStreams, _vertexStrides, offsets );
		_currentStream = 0;
	}


	// Set the Blend state
	void RenderState::SetBlendState( ID3D11BlendState* state, FLOAT factor[4], uint32 mask )
	{
		if ( _blendState != state )
		{
			_blendState = state;
			_context->OMSetBlendState( state, factor, mask );
		}
	}

	// Set the DepthStencil state
	void RenderState::SetDepthStencilState( ID3D11DepthStencilState* state, uint32 stncilRef )
	{
		if ( _depthStencilState != state )
		{
			_depthStencilState = state;
			_context->OMSetDepthStencilState( state, stncilRef );
		}
	}

	// Set the Rasterizer state
	void RenderState::SetRasterizerState( ID3D11RasterizerState* state )
	{
		if ( _rasterizerState != state )
		{
			_rasterizerState = state;
			_context->RSSetState( state );
		}
	}

	// Set a shader resource
	void RenderState::SetShaderResource( uint32 shaderType, uint32 slot, PipeplineResource* resource, ID3D11ShaderResourceView* resourceView )
	{
		static ID3D11ShaderResourceView* srv[1] = { 0 };
		srv[0] = resourceView;

		// Skip redundant resource bindings
		if ( srv[0] == _resourceViews[shaderType][slot] )
			return;

		// Clear the current resource in this slot
		PipeplineResource* boundResource = _resourceBindings[shaderType][slot];
		if ( boundResource )
			boundResource->ClearInputBinding( shaderType, slot );

		// Process the new resource
		if ( resource )
		{
			// Check if this resource is bound to output
			// todo: this isnt fully accurate, CS and graphics should be separate
			if ( resource->_outputBinding.boundToPipeline )
				ClearOutputBindings( true );

			// Set the input binding
			resource->AddInputBinding( shaderType, slot );
		}

		// Set it
		_resourceBindings[shaderType][slot] = resource;
		_resourceViews[shaderType][slot] = srv[0];
		(_context->*Shader::VTable[shaderType].pBindShaderResource)(slot, 1, srv);
		++_frameStats.resourceBinds;
	}



	// Unbind a shader
	void RenderState::ClearShader( ShaderType shader )
	{
		_shaders[(int32)shader] = nullptr;
		(_context->*Shader::VTable[(int32)shader].pSetShader)(nullptr, nullptr, 0);
	}

	// Apply a shader to the pipeline
	void RenderState::SetShader( Shader* shader )
	{
		// Grab the vtable
		int32 type = int32( shader->type );
		ShaderVTable& vtable = Shader::VTable[(int32)shader->type];

		// Bind the constant buffers
		for ( uint32 i = 0; i < shader->constantBuffers.Size(); ++i )
		{
			ConstantBuffer* cb = shader->constantBuffers[i].variable;
			if ( cb->IsDirty() || cb != _constantBuffers[type][shader->constantBuffers[i].bindPoint] )
			{
				// Dirty buffers need to be re-uploaded to vram
				if ( cb->IsDirty() )
					cb->ApplyChanges( _context );

				// Set the buffer
				_constantBuffers[type][shader->constantBuffers[i].bindPoint] = cb;
				(_context->*vtable.pSetConstantBuffers)(shader->constantBuffers[i].bindPoint, shader->constantBuffers[i].bindCount, cb->GetBuffer());
			}
		}

		// Bind the shader resources
		for ( uint32 i = 0; i < shader->resources.Size(); ++i )
		{
			M_ASSERT( shader->resources[i].bindCount == 1, "System needs to be updated to support arrays here" );

			// Set the resource
			PipeplineResource* resource = shader->resources[i].variable->_resource[shader->resources[i].offset];
			ID3D11ShaderResourceView* srv = shader->resources[i].variable->_srv[shader->resources[i].offset];
			SetShaderResource( uint32( shader->type ), shader->resources[i].bindPoint, resource, srv );
		}

		// Bind compute shader UAVS
		if ( shader->type == ShaderType::Compute )
		{
			for ( uint32 i = 0; i < shader->unorderedAccessViews.Size(); ++i )
			{
				PipeplineResource* resource = shader->unorderedAccessViews[i].variable->_resource;
				ClearInputBindings( resource );
				if ( resource->_outputBinding.boundToPipeline )
					ClearOutputBindings( true );
				resource->_outputBinding.boundToPipeline = true;
				_context->CSSetUnorderedAccessViews( shader->unorderedAccessViews[i].bindPoint, 1, resource->GetUAV(), nullptr );
				_computeUAVs.Add( resource );
			}
		}

		// Bind the samplers
		for ( uint32 i = 0; i < shader->samplers.Size(); ++i )
		{
			if ( shader->samplers[i].variable->D3DObject != _samplerStates[type][shader->samplers[i].bindPoint] )
			{
				_samplerStates[type][shader->samplers[i].bindPoint] = shader->samplers[i].variable->D3DObject;
				(_context->*vtable.pSetSamplers)(shader->samplers[i].bindPoint, shader->samplers[i].bindCount, &shader->samplers[i].variable->D3DObject);
			}
		}

		// Set the shader
		if ( _shaders[type] != shader->d3dShader )
		{
			_shaders[type] = shader->d3dShader;
			(_context->*vtable.pSetShader)(shader->d3dShader, nullptr, 0);
		}
	}


	// Changes the input layout
	void RenderState::SetInputLayout( ID3D11InputLayout* pInputLayout )
	{
		// State check
		if ( pInputLayout == _inputLayout )
			return;
		_inputLayout = pInputLayout;

		_context->IASetInputLayout( pInputLayout );
	}


	// Sets the primitive topology
	void RenderState::SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY type )
	{
		// State check
		if ( type == _topology )
			return;
		_topology = type;

		_context->IASetPrimitiveTopology( type );
	}


	// Set a cubemap
	void RenderState::SetCubeMap( Texture::pointer pCubeMap )
	{
		// Check state
		if ( pCubeMap == _cubemap )
			return;
		_cubemap = pCubeMap;
		if ( _cubemap )
			_probeCubeVariable->SetResource( *pCubeMap );
	}


	// Sets the material properties for a shader
	void RenderState::SetMaterial( MaterialBinding::pointer newMaterial )
	{
		// State check
		if ( _materialBinding != newMaterial )
		{
			_materialBinding = newMaterial;

			// Setup material property effect variables
			if ( _materialBinding )
			{
				_materialCBVariable->Fill( _materialBinding->GetShaderData() );
				_materialTextureVariable->SetResourceArray( _materialBinding->GetTextures(), 0, TEX_SIZE );
			}
		}
	}

	// Bind a single material texture
	void RenderState::BindMaterialTexture( MaterialBinding::pointer mat, TEX_TYPE type )
	{
		_materialTextureVariable->SetResourceArray( _materialBinding->GetTextures(), type, 1 );
	}

	// Bind a single material texture
	void RenderState::BindMaterialTexture( Texture::pointer tex, TEX_TYPE type )
	{
		_materialTextureVariable->SetResource( *tex, type);
	}


	// Set only the textures for a mterial
	void RenderState::BindMaterialTextures( MaterialBinding::pointer mat )
	{
		_materialTextureVariable->SetResourceArray( _materialBinding->GetTextures(), 0, TEX_SIZE );
	}


	// Change the vertex buffer
	void RenderState::SetVertexBuffer( ID3D11Buffer* pVB, uint32 stride )
	{
		static const uint32 offset = 0;
		if ( _vertexBuffer != pVB )
		{
			_vertexBuffer = pVB;
			_context->IASetVertexBuffers( 0, 1, &pVB, &stride, &offset );

#ifdef PHASE_DEBUG
			_frameStats.vertexBufferChanges++;
#endif
		}
	}


	// Change the index buffer
	void RenderState::SetIndexBuffer( ID3D11Buffer* pIB, DXGI_FORMAT size )
	{
		if ( _indexBuffer != pIB )
		{
			_indexBuffer = pIB;
			_context->IASetIndexBuffer( pIB, size, 0 );
#ifdef PHASE_DEBUG
			_frameStats.indexBufferChanges++;
#endif
		}
	}


	// Setup effect variables
	void RenderState::BindEffectVariables( Effect* _effect )
	{
		// Material Properties
		_materialCBVariable = _effect->GetConstantBufferByName( "cbMaterial" );
		_materialTextureVariable = _effect->GetVariableByName( "g_txMaterial" )->AsShaderResource();
		_materialTextureFlagVariable = _effect->GetVariableByName( "g_MaterialTextureFlag" )->AsScalar();
		_probeCubeVariable = _effect->GetVariableByName( "g_txCubeProbe" )->AsShaderResource();


		// Light Properties
		_lightCBVariable = _effect->GetConstantBufferByName( "cbLight" );
	}

	// Set a null vertex buffer
	void RenderState::ClearVertexBuffer()
	{
		_context->IASetVertexBuffers( 0, 0, 0, 0, 0 );
	}

	

}

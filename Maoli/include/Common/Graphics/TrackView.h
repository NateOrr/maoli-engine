//--------------------------------------------------------------------------------------
// File: TrackView.h
//
// Trackview for cameras
// 
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class TrackView
	{
	public:

		// Add a new track location
		void AddTrackLocation(Camera& obj, const Vector3& location, const Vector3& lookAt, float moveTime, float waitTime);

		// Update all tracks
		void Update(float dt);

	private:

		// Track states
		enum class TrackState
		{
			Moving,
			Waiting,
		};

		// Trackview location
		struct TrackLocation
		{
			Vector3 position;			// Position for the destination
			Vector3 lookAt;				// Rotation for the destination
			Vector3 deltaPosition;		// Change in position for each time step
			float moveDuration;				// Time it will take to move to the destination
			float waitDuration;				// Time the object should wait at the destination
		};

		// Trackview list for an object
		struct Track
		{
			// Ctor
			Track();

			Array<TrackLocation> locations;			// Location list
			uint32 currentLocation;					// Current location
			float time;								// Timer for movement
			TrackState state;						// Current state
		};

		// Helper types
		typedef std::pair<Camera*, Track> TrackPair;
		
		std::map<Camera*, Track>	_tracks;		// Each object added has a track
	};
}

//--------------------------------------------------------------------------------------
// File: Vertex.h
//
// 3D Vertex formats
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	// Available vertex streams
	enum VERTEX_STREAM_TYPE
	{
		VERTEX_STREAM_POS=0,
		VERTEX_STREAM_TEXCOORD,
		VERTEX_STREAM_NORMAL,
		VERTEX_STREAM_BONE,

		VERTEX_STREAM_MAX,
	};

	// Maoli Engine standard vertex format
	struct Vertex
	{
		Vector3 pos;	// Position
		float tu,tv;	// Texture coords
		Vector3 normal;	// Normal
		
		// Vertex properties
		static ID3D11InputLayout* pInputLayout;
		static const D3D11_INPUT_ELEMENT_DESC Desc[3];
		static const uint32 size;

		// Basic stream
		static ID3D11InputLayout* pStreamInputLayout;
		static const uint32 pStrides[4];
		static const D3D11_INPUT_ELEMENT_DESC StreamDesc[3];

		// Instanced stream
		static ID3D11InputLayout* pInstancedInputLayout;
		static const D3D11_INPUT_ELEMENT_DESC InstancedStreamDesc[7];

		// Animation stream
		static ID3D11InputLayout* pAnimationInputLayout;
		static const D3D11_INPUT_ELEMENT_DESC AnimationDesc[5];
	};

	// Maoli Engine vertex with only position
	struct PosVertex
	{
		Vector3 pos;	// Position

		static ID3D11InputLayout* pInputLayout;
		static const D3D11_INPUT_ELEMENT_DESC Desc[1];
		static const uint32 size;

		// Animation stream
		static ID3D11InputLayout* pAnimationInputLayout;
		static const D3D11_INPUT_ELEMENT_DESC AnimationDesc[3];
		static const uint32 animationStride;
	};

	// Sprite vertex format
	struct SpriteVertex
	{
		Vector4 pos;	// Position

		static ID3D11InputLayout* pInputLayout;
		static const D3D11_INPUT_ELEMENT_DESC Desc[1];
		static const uint32 size;
	};
}

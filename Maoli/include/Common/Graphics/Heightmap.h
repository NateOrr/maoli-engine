//-----------------------------------------------------------------------------
// File: Heightmap.h
//
// Support for generating terrain heightmaps
//
// Nate Orr
//-----------------------------------------------------------------------------
#pragma once

namespace Maoli
{


	//-----------------------------------------------------------------------------
	// Name: GenerateLiquidHeightmap()
	// Desc: Genterates a heightmap based on fluid simulation
	// **MODIFIED FROM Francis Woodhouse's ARTICLE ON GAMEDEV.NET**
	// http://www.gamedev.net/reference/articles/article2001.asp
	//-----------------------------------------------------------------------------
	byte* GenerateLiquidHeightmap(int32 w, int32 h, int32 num,float d,float t,float mu,float c);


	//-----------------------------------------------------------------------------
	// Name: GenerateNormalMap()
	// Desc: Genterates a normal map based on a height map
	//-----------------------------------------------------------------------------
	Vector3* GenerateNormalMap(float* pHeight, int32 w, int32 h, float hs);

}

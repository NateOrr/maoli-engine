//--------------------------------------------------------------------------------------
// File: Light.cpp
//
// 3D Light
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Light.h"
#include "Graphics/Renderer.h"
#include "Game/Entity.h"

namespace Maoli
{
	// Contructor
	Light::Light( Engine* engine ) : Component( engine )
	{
		_engine = engine;
		_graphics = _engine->GetGraphics();

		// Setup the properties
		_type = LightType::Point;
		_properties.lightData.color = Color::White;
		_properties.direction = Vector3( 0.0f, -0.5f, 0.3f );
		_properties.lightData.position = Vector3( 0, 5, 0 );
		_properties.lightData.range = 25.0f;
		_properties.innerRadius = 0.92f;
		_properties.outerRadius = 0.88f;

		_projectiveTexture = nullptr;
		ShadowSoftness = 2;
		_isDirty = true;
		_entityMatrix = nullptr;
		_active = false;
		_hasShadows = false;
	}

	// Dtor
	Light::~Light()
	{
		Release();
	}


	// Free the texture
	void Light::Release()
	{
		_projectiveTexture = nullptr;
	}


	// Per-frame logic
	void Light::Update( float dt )
	{
		if ( _isDirty )
		{
			if ( _entityMatrix )
			{
				_properties.lightData.position = _entityMatrix->Transform( _position );
				_properties.direction = _entityMatrix->TransformNormal( _direction );
				_properties.direction.Normalize();
				_entityMatrix = nullptr;
			}
			else
			{
				_properties.lightData.position = _position;
				_properties.direction = _direction;
			}
			_isDirty = false;
		}
	}

	// Update position
	void Light::OnTransformChanged( Component* sender, void* userData )
	{
		_entityMatrix = &((Transform*)sender)->GetOrientationMatrix();
		_isDirty = true;
	}


	// Register for any events that need to be handled
	void Light::RegisterEventCallbacks()
	{
		REGISTER_EVENT_CALLBACK( ComponentEvent::TransformChanged, &Light::OnTransformChanged );
	}

	// Binary serialize
	void Light::Export( std::ofstream& file )
	{
		Maoli::Serialize( file, &ShadowSoftness );
		Maoli::Serialize( file, &worldMatrix );
		Maoli::Serialize( file, &frustum );
		Maoli::Serialize( file, &_position );
		Maoli::Serialize( file, &_direction );
		Maoli::Serialize( file, &_properties );

		// Mesh and tex
		String str = _projectiveTexture ? _projectiveTexture->GetName() : String::none;
		str.Serialize( file );
	}

	// Binary file import
	void Light::Import( std::ifstream& file )
	{
		Maoli::Deserialize( file, &ShadowSoftness );
		Maoli::Deserialize( file, &worldMatrix );
		Maoli::Deserialize( file, &frustum );
		Maoli::Deserialize( file, &_position );
		Maoli::Deserialize( file, &_direction );
		Maoli::Deserialize( file, &_properties );

		// Mesh and tex
		String str;
		str.Deserialize( file );
		if ( str == String::none )
			_projectiveTexture = nullptr;
		else
			_projectiveTexture = _graphics->CreateTextureFromFile( str );
	}

	// Deep copy clone is required for all components
	Component* Light::Clone( Component* dest ) const
	{
		Light* clone = dest ? (Light*)dest : new Light( _engine );

		clone->ShadowSoftness = ShadowSoftness;
		clone->worldMatrix = worldMatrix;
		clone->frustum = frustum;
		memcpy( &clone->_properties, &_properties, sizeof(SpotLightShaderData) );
		clone->_projectiveTexture = _projectiveTexture;
		clone->SetLocalPosition( _position );
		clone->SetLocalDirection( _direction );

		return clone;
	}



#pragma region Transforms


	// Set the position
	void Light::SetLocalPosition( const Vector3& pos )
	{
		_position = pos;
		_isDirty = true;
	}


	// Set the position
	void Light::SetLocalPosition( float x, float y, float z )
	{
		_position.x = x;
		_position.y = y;
		_position.z = z;
		_isDirty = true;
	}


	// Add to the position
	void Light::AddToLocalPosition( const Vector3& pos )
	{
		_position += pos;
		_isDirty = true;
	}


	// Set the direction
	void Light::SetLocalDirection( const Vector3& pos )
	{
		_direction = pos;
		_direction.Normalize();
		_isDirty = true;
	}


	// Set the direction
	void Light::SetLocalDirection( float x, float y, float z )
	{
		_direction = Vector3( x, y, z );
		_direction.Normalize();
		_isDirty = true;
	}


	// Add to the direction
	void Light::AddToLocalDirection( const Vector3& pos )
	{
		_direction += pos;
		_direction.Normalize();
		_isDirty = true;
	}


	// Set the range
	void Light::SetRange( float range )
	{
		_properties.lightData.range = range;
	}


	// Add to the range
	void Light::AddToRange( float pos )
	{
		_properties.lightData.range += pos;
	}


#pragma endregion



	// Clear t projective texture
	void Light::ClearProjectiveTexture()
	{
		if ( _projectiveTexture )
		{
			_projectiveTexture = nullptr;
		}
	}


	// Load a projective texture
	Texture::pointer Light::LoadProjectiveTexture( const char* file )
	{
		Texture::pointer tex = _graphics->CreateTextureFromFile( file );
		if ( tex )
		{
			ClearProjectiveTexture();
			_projectiveTexture = tex;
		}
		return tex;
	}

	// Set the projective texture
	void Light::SetProjectiveTexture( Texture::pointer tex )
	{
		_projectiveTexture = tex;
	}

	// Set the light type
	void Light::SetType( LightType type )
	{
		if ( _active )
		{
			_graphics->RemoveLight( this );
			UpdateType( type );
			_graphics->AddLight( this );
		}
		else
			UpdateType( type );
	}

	// Update the light after the type has been changed
	void Light::UpdateType( LightType newType )
	{
		_type = newType;
	}

	// Enable or disable shadow casting
	void Light::EnableShadows( bool b )
	{
		_hasShadows = b;
		if(_active)
		{
			_graphics->RemoveLight( this );
			UpdateType( _type );
			_graphics->AddLight( this );
		}
	}


}

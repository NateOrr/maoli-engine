//--------------------------------------------------------------------------------------
// File: RenderNode.h
//
// A RenderNode is defined as a set of renderable geometry with a single material.
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/MaterialBinding.h"
#include "Graphics/Texture.h"

namespace Maoli
{
	// Forward decl
	class Renderer;
	class EffectPass;
	class Model;

	// Chunk of geometry ready to be rendered
	class RenderNode : public LinkedListNode
	{
		friend class MeshBinding;
	public:

		// Render callback signature
		typedef std::function<void(EffectPass*, bool)> RenderCallback;

		// Ctor
		RenderNode( Renderer* graphics = nullptr );

		// Set the render callback
		template <typename T>
		void SetRenderCallback( void(T::*fn)(RenderNode&, EffectPass*, bool), T* caller, EffectPass* effectPass = nullptr )
		{
			_renderFn = std::bind( fn, caller, *this, std::placeholders::_1, std::placeholders::_2 );
			_effectPass = effectPass;
		}

		// Get the default effect pass for this mesh
		inline EffectPass* GetEffectPass() const { return _effectPass; }

		// Render the mesh data
		inline void Render() { _renderFn(_effectPass, true); }

		// Get the bounds
		inline const BoundingVolume& GetBounds() const { return bounds; }

		// Get the material binding
		inline MaterialBinding::pointer GetMaterialBinding() const { return _materialBinding; }

		// Set the material binding
		void SetMaterialBinding( MaterialBinding::pointer mat );

		// Set the renderer
		inline void SetGraphics( Renderer* graphics ) { _graphics = graphics; }

		// Set the parent model
		inline void SetModel( Model* model ) { _model = model; }

		// Get the parent model
		inline Model* GetModel() const { return _model; }

		// Compute the instance ID
		void ComputeInstanceID();

		// Get the instance ID
		inline uint32 GetInstanceID() const { return _instanceID; }

		String						name;									// Name
		ID3D11Buffer*				indexBuffer;							// Parent index buffer
		ID3D11Buffer*				vertexBuffers[VERTEX_STREAM_MAX];		// Parent vertex buffer
		int32							startIndex;								// Start index
		int32							indexCount;								// Number of indices
		Matrix*						worldMatrix;							// Ptr to world matrix
		Matrix*						worldMatrixPrev;						// Ptr to world matrix from the last frame
		BoundingVolume				bounds;									// Mesh bounding volume
		Texture::pointer			cubeMap;								// Pointer to the cubemap that affects this mesh
		Buffer<Matrix>::pointer		bonePalette;							// Bone matrix palette for animations
		float						depth;									// View space depth for sorting
		uint32						sortKey;								// Key to sort by

	private:

		Renderer*					_graphics;
		EffectPass*					_effectPass;
		RenderCallback				_renderFn;
		MaterialBinding::pointer	_materialBinding;		// Ptr to the material
		Model*						_model;					// Model that owns this node
		uint32						_index;					// Index within the model
		uint32						_instanceID;			// Instance set ID
	};

}

//--------------------------------------------------------------------------------------
// File: ParticleSet.cpp
//
// A self contained particle system, requires an emitter to function
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "ParticleSet.h"
#include "ParticleEmitter.h"

namespace Maoli
{

	// Ctor
	ParticleSet::ParticleSet( Renderer* graphics, ParticleEmitter* emitter )
	{
		_graphics = graphics;
		_emitter = emitter;

		SetMaxParticles( 500 );
		SetEmissionRate( 30 );
		SetTexture( _graphics->CreateTextureFromFile( "Resources\\Textures\\Particles\\particle.png" ) );
		_lifetime = 3.0f;
		_fadeOutTime = _fadeInTime = 0.5f;
		_minSize = _maxSize = _endSize = Vector3( 0.1f, 0.1f, 0.1f );
		_minColor = _maxColor = _endColor = Color::White;
		_maxVelocity = Vector3( 1, 1, 1 );
		_minVelocity = -_maxVelocity;
		_emissionTimer = 0;
		_spawnRadius = 0;
		_name = "NewSet";
		_enabled = true;
	}

	// Binary serialize
	void ParticleSet::Export( std::ofstream& fout )
	{
		Maoli::Serialize( fout, &_emissionRate );
		Maoli::Serialize( fout, &_lifetime );
		Maoli::Serialize( fout, &_fadeOutTime );
		Maoli::Serialize( fout, &_fadeInTime );
		Maoli::Serialize( fout, &_spawnRadius );
		Maoli::Serialize( fout, &_gravity );
		Maoli::Serialize( fout, &_minVelocity );
		Maoli::Serialize( fout, &_maxVelocity );
		Maoli::Serialize( fout, &_minSize );
		Maoli::Serialize( fout, &_maxSize );
		Maoli::Serialize( fout, &_endSize );
		Maoli::Serialize( fout, &_minColor );
		Maoli::Serialize( fout, &_maxColor );
		Maoli::Serialize( fout, &_endColor );
		Maoli::Serialize( fout, &_position );
		Sprite::Serialize( fout );
		_texture->GetName().Serialize( fout );
		_name.Serialize( fout );
	}

	// Binary file import
	void ParticleSet::Import( std::ifstream& fin )
	{
		Maoli::Deserialize( fin, &_emissionRate );
		Maoli::Deserialize( fin, &_lifetime );
		Maoli::Deserialize( fin, &_fadeOutTime );
		Maoli::Deserialize( fin, &_fadeInTime );
		Maoli::Deserialize( fin, &_spawnRadius );
		Maoli::Deserialize( fin, &_gravity );
		Maoli::Deserialize( fin, &_minVelocity );
		Maoli::Deserialize( fin, &_maxVelocity );
		Maoli::Deserialize( fin, &_minSize );
		Maoli::Deserialize( fin, &_maxSize );
		Maoli::Deserialize( fin, &_endSize );
		Maoli::Deserialize( fin, &_minColor );
		Maoli::Deserialize( fin, &_maxColor );
		Maoli::Deserialize( fin, &_endColor );

		Maoli::Deserialize( fin, &_position );
		Sprite::Deserialize( fin );
		String texName;
		texName.Deserialize( fin );
		SetTexture( _graphics->CreateTextureFromFile( texName ) );
		_name.Deserialize( fin );

		SetMaxParticles( 500 );
	}

	// Deep copy clone
	ParticleSet* ParticleSet::Clone( ParticleEmitter* newEmitter ) const
	{
		ParticleSet* clone = new ParticleSet( _graphics, newEmitter );
		clone->SetEmissionRate( _emissionRate );
		clone->SetLifetime( _lifetime );
		clone->SetFadeOutTime( _fadeOutTime );
		clone->SetFadeInTime( _fadeInTime );
		clone->SetSpawnRadius( _spawnRadius );
		clone->SetGravity( _gravity );
		clone->SetMinVelocity( _minVelocity );
		clone->SetMaxVelocity( _maxVelocity );
		clone->SetMinSize( _minSize );
		clone->SetMaxSize( _maxSize );
		clone->SetEndSize( _endSize );
		clone->SetMinColor( _minColor );
		clone->SetMaxColor( _maxColor );
		clone->SetEndColor( _endColor );
		clone->SetPosition( _position );
		clone->SetTexture( _texture );
		clone->SetName( _name );

		// Sprite
		clone->_animated = _animated;
		clone->_animationLength = _animationLength;
		clone->_frameLength = _frameLength;
		clone->_numFrames = _numFrames;
		clone->_frameWidth = _frameWidth;
		clone->_frameHeight = _frameHeight;
		clone->_spriteWidth = _spriteWidth;
		clone->_spriteHeight = _spriteHeight;
		clone->_frame = _frame;
		clone->_frameTimer = _frameTimer;

		return clone;
	}

	// Set the max number of active particles
	void ParticleSet::SetMaxParticles( uint32 max )
	{
		if ( _pool.Size() != max )
		{
			_active.Release();
			_dead.Release();
			_pool.Allocate( max );
			for ( uint32 i = 0; i < max; ++i )
				_dead.Add( &_pool[i] );
			_instanceBuffer = _graphics->CreateVertexBuffer<ParticleVertex>( max, D3D11_USAGE_DYNAMIC );

			_vbData.Allocate( max );
		}
	}

	// Per-frame logic
	void ParticleSet::Update( float dt )
	{
		// Emit new particles
		if ( _emissionRate > 0 && _enabled )
		{
			_emissionTimer += dt;
			while ( !_dead.IsEmpty() && _emissionTimer > _emissionPeriod )
			{
				EmitParticle();
				_emissionTimer -= _emissionPeriod;
			}
		}

		// Update active particles
		_vbData.Clear();
		Vector3 dv = _gravity*dt;
		for ( auto iter = _active.Begin(); !iter.End(); )
		{
			// Update lifetime, and remove if needed
			Particle& p = *(Particle*)*iter;
			p.life += dt;
			if ( p.life > _lifetime )
			{
				iter = _active.Remove( iter );
				_dead.Add( &p );
				continue;
			}

			// Particle update
			p.velocity += dv;
			p.position += p.velocity*dt;
			p.color += p.dColor * dt;
			p.size += p.dSize * dt;
			if ( p.life < _fadeInTime )
				p.color.a = Math::Lerp( 0.0f, 1.0f, p.life / _fadeInTime );
			else if ( p.life > _lifetime - _fadeOutTime )
				p.color.a = Math::Lerp( 0.0f, 1.0f, (_lifetime - p.life) / _fadeOutTime );
			else
				p.color.a = 1.0f;

			// Animation
			if ( IsAnimated() )
			{
				p.frameTime += dt;
				if ( p.frameTime > GetAnimationFrameLength() )
				{
					++p.frame;
					if ( p.frame == GetNumAnimationFrames() )
						p.frame = 0;
					p.frameTime = 0;
				}
			}

			// Send to the instance buffer
			ParticleVertex& vert = _vbData.Add();
			vert.position = p.position;
			vert.color = p.color;
			vert.size = p.size;
			vert.frame = p.frame;
			++iter;
		}

		// Update the vertex buffer
		auto verts = _instanceBuffer->Map( D3D11_MAP_WRITE_DISCARD );
		memcpy( verts, _vbData, sizeof(ParticleVertex)* _vbData.Size() );
		_instanceBuffer->Unmap();
	}

	// Set the emmision rate
	void ParticleSet::SetEmissionRate( uint32 particlesPerSecond )
	{
		_emissionRate = particlesPerSecond;
		if ( float( particlesPerSecond ) == 0 )
			_emissionPeriod = std::numeric_limits<float>::max();
		else
			_emissionPeriod = 1.0f / float( particlesPerSecond );
	}

	// Emit a particle into the scene
	void ParticleSet::EmitParticle()
	{
		auto particle = _dead.RemoveFirst();
		_active.Add( particle );
		ResetParticle( (Particle*)particle );
	}

	// Reset particle data
	void ParticleSet::ResetParticle( Particle* particle ) const
	{
		Vector3 startingPosition = _position + _emitter->GetPosition();
		Vector3 startingVelocity;
		if ( _emitter->GetTransform() )
		{
			startingPosition += _emitter->GetTransform()->GetPosition();
			startingVelocity = _emitter->GetTransform()->GetVelocity();
		}

		particle->position.x = Math::NormalRand() * _spawnRadius + startingPosition.x;
		particle->position.y = Math::NormalRand() * _spawnRadius + startingPosition.y;
		particle->position.z = Math::NormalRand() * _spawnRadius + startingPosition.z;

		particle->velocity.x = Math::UniformRand() * (_maxVelocity - _minVelocity).x + _minVelocity.x + startingVelocity.x;
		particle->velocity.y = Math::UniformRand() * (_maxVelocity - _minVelocity).y + _minVelocity.y + startingVelocity.y;
		particle->velocity.z = Math::UniformRand() * (_maxVelocity - _minVelocity).z + _minVelocity.z + startingVelocity.z;

		particle->size.x = Math::UniformRand() * (_maxSize - _minSize).x + _minSize.x;
		particle->size.y = Math::UniformRand() * (_maxSize - _minSize).y + _minSize.y;
		particle->size.z = Math::UniformRand() * (_maxSize - _minSize).z + _minSize.z;

		particle->color.r = Math::UniformRand() * (_maxColor - _minColor).r + _minColor.r;
		particle->color.g = Math::UniformRand() * (_maxColor - _minColor).g + _minColor.g;
		particle->color.b = Math::UniformRand() * (_maxColor - _minColor).b + _minColor.b;
		particle->color.a = 1.0f;

		particle->dSize = (_endSize - particle->size) / _lifetime;
		particle->dColor = (_endColor - particle->color) / _lifetime;
		particle->life = 0.0f;
		particle->frame = 0;
		particle->frameTime = 0;
	}


	// Clear all active particles
	void ParticleSet::Clear()
	{
		while ( !_active.IsEmpty() )
			_dead.Add( _active.RemoveFirst() );
	}


}
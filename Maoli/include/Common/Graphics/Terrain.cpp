//--------------------------------------------------------------------------------------
// File: Terrain.cpp
//
// 3D heightmap based terrain
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Graphics/Heightmap.h"
#include "Graphics/Terrain.h"

#include "Graphics/Renderer.h"

#include <iostream>
#include <fstream>
using std::ifstream;
using std::ofstream;
using std::endl;


namespace Maoli
{

	// Constructor
	Terrain::Terrain( Engine* engine )
	{
		_graphics = engine->GetGraphics();

		m_bLoaded = false;
		m_FlagForUpdate = false;
		m_vPos *= 0;
		m_HeightScale = 128.0f;
		m_Scale = 1.0f;
		m_BlendMapSize = 0;

		m_VertexBuffer = nullptr;
		m_IndexBuffer = nullptr;

		m_TrimXSide = nullptr;
		m_TrimZSide = nullptr;
		m_ClipmapOffsets = nullptr;
		m_StagingHeightmap = nullptr;
		m_DefaultMaterial = nullptr;
		m_Clipmaps = nullptr;
		m_Blendmap = nullptr;
		m_Heightmap = nullptr;

		m_pRootNode = nullptr;

		m_RenderMode = TerrainMode::Blendmap;

		m_CurrentLayer = 0;
		for ( int32 i = 0; i < m_MaxLayers; i++ )
		{
			m_Layers[i] = new PaintLayer( MaterialBinding::pointer( nullptr ) );
			m_TerrainDiffuse[i] = nullptr;
			m_TerrainNormal[i] = nullptr;
			m_TerrainSpecular[i] = nullptr;
		}




#ifdef PHASE_DEBUG
		m_UndoMaxMem = 50.0;
		m_UndoMemUsage = 0;
#endif


		m_HeightExtents.x = 9999;
		m_HeightExtents.y = -500;

	}

	// Dtor
	Terrain::~Terrain()
	{
		Release();
	}


	// Build the world matrix
	void Terrain::UpdateWorldMatrix()
	{
		m_vPos = Vector3( -(float)m_Size*0.5f, 0, -(float)m_Size*0.5f ) * m_Scale;
		m_WorldMatrix.Identity();
		m_WorldMatrix.Scale( m_Scale, m_HeightScale, m_Scale );
		m_WorldMatrix.Translate( m_vPos );
		m_FlagForUpdate = true;
	}


	// Saving
	void Terrain::ExportHeightmap( const char* fileName )
	{
		// Save the heightmap	
		D3DX11SaveTextureToFileA( _graphics->GetContext(), m_Heightmap->GetTex()[0], D3DX11_IFF_DDS, fileName );
	}

	// Saving
	void Terrain::ExportBlendmap( const char* fileName )
	{
		// Save the heightmap	
		auto blend = _graphics->CreateTexture( m_BlendMapSize, m_BlendMapSize, DXGI_FORMAT_R8G8B8A8_UNORM, 4, nullptr, D3D11_USAGE_DEFAULT, false, false );
		_graphics->CopyTextureToTexture( blend, m_Blendmap );
		D3DX11SaveTextureToFileA( _graphics->GetContext(), blend->GetTex()[0], D3DX11_IFF_PNG, fileName );
	}


	// Loads from an image heightmap
	uint32 Terrain::LoadHeightMap( ID3D11Texture2D* pHeightmap )
	{
		// Create a staging buffer to copy the texture into
		ID3D11Texture2D* pStage;
		D3D11_TEXTURE2D_DESC td;
		pHeightmap->GetDesc( &td );
		td.BindFlags = 0;
		td.ArraySize = 1;
		td.MiscFlags = 0;
		td.MipLevels = 1;
		td.CPUAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
		td.Usage = D3D11_USAGE_STAGING;
		_graphics->GetDevice()->CreateTexture2D( &td, nullptr, &pStage );

		// Copy the loaded texture into the staging texture
		D3D11_BOX sourceRegion;
		sourceRegion.left = 0;
		sourceRegion.right = td.Width;
		sourceRegion.top = 0;
		sourceRegion.bottom = td.Height;
		sourceRegion.front = 0;
		sourceRegion.back = 1;
		_graphics->GetContext()->CopySubresourceRegion( pStage, 0, 0, 0, 0, pHeightmap, 0, &sourceRegion );

		// Allocate space in the height array
		m_Height.Allocate( td.Width*td.Height );

		// Map the texture and copy the height data over
		D3D11_MAPPED_SUBRESOURCE mappedTexture;
		if ( SUCCEEDED( _graphics->GetContext()->Map( pStage, 0, D3D11_MAP_READ_WRITE, 0, &mappedTexture ) ) )
		{
			float* pTexels = (float*)mappedTexture.pData;
			for ( uint32 row = 0; row < td.Height; row++ )
			{
				uint32 rowStart = row * mappedTexture.RowPitch / 4;
				for ( uint32 col = 0; col < td.Width; col++ )
				{
					m_Height[row*m_Size + col] = pTexels[rowStart + col];

					// Track the min/max
					float h = m_Height[row*m_Size + col] * m_HeightScale;
					m_HeightExtents.x = std::min( m_HeightExtents.x, h );
					m_HeightExtents.y = std::max( m_HeightExtents.y, h );
				}
			}
			_graphics->GetContext()->Unmap( pStage, 0 );
		}

		m_StagingHeightmap = pStage;

		return td.Height;
	}



	// Create a terrain from a heightmap that is already loaded
	HRESULT Terrain::CreateFromTexture( ID3D11Texture2D* pHeightmap )
	{
		// Get the dimensions
		D3D11_TEXTURE2D_DESC hd;
		pHeightmap->GetDesc( &hd );
		m_Size = hd.Width;

		SetScale( 1.0f );
		SetHeightScale( 128.0f );

		// Setup clipmap
		SetupClipmap( 255, CLIPMAP_LEVELS );

		// Get the height info
		LoadHeightMap( pHeightmap );

		// Setup the quadtree
		m_pRootNode = new QuadtreeNode();
		m_pRootNode->pos = Vector3( 0, m_HeightScale*0.5f, 0 );
		m_pRootNode->size = Vector3( (float)m_Size / 2.0f, m_HeightScale*0.5f, (float)m_Size / 2.0f );
		BuildQuadtree( m_pRootNode, 32 );

		// Default blendmap sizem 8 pixels per meter, but the max is 8192
		m_BlendMapSize = std::min( m_Size * 8, 8192 );

		// Create the blend map if needed
		if ( !CreateBlendmap() )
			return E_FAIL;

		// Free resources and return
		m_bLoaded = true;
		m_FlagForUpdate = true;
		return S_OK;
	}

	// Create a terrain from a heightmap
	bool Terrain::CreateFromFile( const char* szFile )
	{
		// Load a heightmap
		m_szName = szFile;

		// Setup the heightmap load info
		D3DX11_IMAGE_LOAD_INFO info = D3DX11_IMAGE_LOAD_INFO();
		info.FirstMipLevel = 0;
		info.MipLevels = 1;
		info.Usage = D3D11_USAGE_DEFAULT;
		info.Format = DXGI_FORMAT_R32_FLOAT;
		info.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		ID3D11Texture2D* tTex;

		// Load the heightmap texture
		if ( FAILED( D3DX11CreateTextureFromFileA( _graphics->GetDevice(), szFile, &info, nullptr, (ID3D11Resource**)&tTex, nullptr ) ) )
		{
			ErrorMessage( "Terrain Heightmap failed to load> %s", szFile );
			return false;
		}

		// Setup the heightmap
		m_Heightmap = _graphics->CreateTextureFromTexture( tTex, false, true );
		if ( !m_Heightmap )
		{
			M_ERROR( "Failed to CreateFromTexture" );
			return false;
		}

		return SUCCEEDED( CreateFromTexture( tTex ) );
	}

	// Create a terrain from a heightmap
	bool Terrain::Create( uint32 size )
	{
		// Load a heightmap
		m_szName = "New Terrain";

		// Create the height texture
		ID3D11Texture2D* tTex = nullptr;
		D3D11_TEXTURE2D_DESC desc = D3D11_TEXTURE2D_DESC();
		desc.Width = size;
		desc.Height = size;
		desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.MipLevels = 1;
		desc.Format = DXGI_FORMAT_R32_FLOAT;
		desc.ArraySize = 1;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		if ( FAILED( _graphics->GetDevice()->CreateTexture2D( &desc, nullptr, &tTex ) ) )
		{
			M_ERROR( "Failed to CreateTexture2D" );
			return false;

		}

		// Setup the heightmap
		m_Heightmap = _graphics->CreateTextureFromTexture( tTex, false, true );
		if ( !m_Heightmap )
		{
			M_ERROR( "Failed to CreateFromTexture" );
			return false;
		}

		return SUCCEEDED( CreateFromTexture( tTex ) );
	}


	// Set the rendering mode
	void Terrain::SetRenderMode( TerrainMode mode )
	{
		m_RenderMode = mode;
		if ( mode != TerrainMode::Blendmap )
			m_Blendmap = nullptr;
		else
		{
			if ( !m_Blendmap->Exists() )
				CreateBlendmap();
		}
	}


	// Create the blendmap
	bool Terrain::CreateBlendmap()
	{
		if ( m_RenderMode == TerrainMode::Blendmap )
		{
			m_Blendmap = nullptr;
			m_Blendmap = _graphics->CreateTextureEx( m_BlendMapSize, m_BlendMapSize, DXGI_FORMAT_R8G8B8A8_TYPELESS, 4, nullptr,
				D3D11_USAGE_DEFAULT, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_FORMAT_R32_UINT );
			if ( !m_Blendmap )
			{
				M_ERROR( "Failed to create terrain blend texture" );
				return false;
			}

			// Fill the first layer
			m_Blendmap->SetClearColor( 1.0f, 0, 0, 0 );
			m_Blendmap->Clear();
			m_Blendmap->GenerateMips();
		}
		return true;
	}


	// Load a blendmap from file
	bool Terrain::LoadBlendmap( const char* file )
	{
		if ( m_RenderMode == TerrainMode::Blendmap )
		{
			// Setup the heightmap load info
			ID3D11Texture2D* blendmap;
			D3DX11_IMAGE_LOAD_INFO info = D3DX11_IMAGE_LOAD_INFO();
			info.FirstMipLevel = 0;
			info.MipLevels = 1;
			info.Usage = D3D11_USAGE_DEFAULT;
			info.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			info.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
			info.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

			// Load the heightmap texture
			HRESULT hr = D3DX11CreateTextureFromFileA( _graphics->GetDevice(), file, &info, nullptr, (ID3D11Resource**)&blendmap, nullptr );
			if ( FAILED( hr ) )
			{
				CheckD3DError( hr );
				ErrorMessage( "Terrain Blendmap failed to load> %s", file );
				return false;
			}

			// Create a staging buffer to copy the texture into
			ID3D11Texture2D* pStage;
			D3D11_TEXTURE2D_DESC td;
			blendmap->GetDesc( &td );
			m_BlendMapSize = td.Width;
			td.BindFlags = 0;
			td.ArraySize = 1;
			td.MiscFlags = 0;
			td.MipLevels = 1;
			td.CPUAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
			td.Usage = D3D11_USAGE_STAGING;
			_graphics->GetDevice()->CreateTexture2D( &td, nullptr, &pStage );

			// Copy the loaded texture into the staging texture
			D3D11_BOX sourceRegion;
			sourceRegion.left = 0;
			sourceRegion.right = td.Width;
			sourceRegion.top = 0;
			sourceRegion.bottom = td.Height;
			sourceRegion.front = 0;
			sourceRegion.back = 1;
			_graphics->GetContext()->CopySubresourceRegion( pStage, 0, 0, 0, 0, blendmap, 0, &sourceRegion );
			blendmap->Release();

			// Map the texture and ensure that the weights are properly scaled
			struct Pixel
			{
				byte r, g, b, a;
			};
			D3D11_MAPPED_SUBRESOURCE mappedTexture;
			if ( SUCCEEDED( _graphics->GetContext()->Map( pStage, 0, D3D11_MAP_READ_WRITE, 0, &mappedTexture ) ) )
			{
				Pixel* pTexels = (Pixel*)mappedTexture.pData;
				for ( uint32 row = 0; row < td.Height; row++ )
				{
					uint32 rowStart = row * mappedTexture.RowPitch / 4;
					for ( uint32 col = 0; col < td.Width; col++ )
					{
						// Get the sum of the blend weights
						float r = ((float)pTexels[rowStart + col].r) / 255.0f;
						float g = ((float)pTexels[rowStart + col].g) / 255.0f;
						float b = ((float)pTexels[rowStart + col].b) / 255.0f;
						float a = ((float)pTexels[rowStart + col].a) / 255.0f;
						float sum = r + g + b + a;

						// Rescale the weights so they sum into 1.0
						r = r / sum;
						g = g / sum;
						b = b / sum;
						a = a / sum;

						// Insert back into the texture
						pTexels[rowStart + col].r = (byte)(r * 255.0f);
						pTexels[rowStart + col].g = (byte)(g * 255.0f);
						pTexels[rowStart + col].b = (byte)(b * 255.0f);
						pTexels[rowStart + col].a = (byte)(a * 255.0f);
					}
				}

				// Unlock the staging texture
				_graphics->GetContext()->Unmap( pStage, 0 );

				// Create a new blend map from this data
				CreateBlendmap();
				_graphics->GetContext()->CopySubresourceRegion( m_Blendmap->GetTex()[0], 0, 0, 0, 0, pStage, 0, &sourceRegion );
				m_Blendmap->GenerateMips();
				pStage->Release();
			}


		}
		return true;
	}



	// Release the terrain
	void Terrain::Release()
	{

#ifdef PHASE_DEBUG
		// Clear the redo stack
		while(!m_RedoStack.IsEmpty())
			m_RedoStack.Pop().tex->Release();

		// Clear the undo stack
		while(!m_UndoStack.IsEmpty())
			m_UndoStack.Pop().tex->Release();
		m_UndoMemUsage = 0;
#endif

		// Release all the layers
		for ( int32 i = 0; i < m_MaxLayers; i++ )
		{
			if ( m_Layers[i] )
			{
				m_Layers[i]->Mask = nullptr;
				delete m_Layers[i];
				m_Layers[i] = nullptr;
			}
		}

		m_HeightExtents.x = 9999;
		m_HeightExtents.y = -500;

		m_Blendmap = nullptr;


		SafeDelete( m_pRootNode );

		m_Heightmap = nullptr;
		m_Clipmaps = nullptr;

		SafeRelease( m_StagingHeightmap );

		SafeRelease( m_VertexBuffer );
		SafeRelease( m_IndexBuffer );

		SafeDelete( m_TrimXSide );
		SafeDelete( m_TrimZSide );
		SafeDelete( m_ClipmapOffsets );

		m_Height.Release();
	}


	// Bind all effect variables
	void Terrain::BindEffectVariables()
	{
		// Get access to all the needed effect variables
		ClipmapOffsetVariable = _graphics->GetEffect()->GetVariableByName( "g_ClipmapOffset" )->AsVector();
		ClipmapFixVariable = _graphics->GetEffect()->GetVariableByName( "g_ClipmapFix" )->AsVector();
		ClipmapScaleVariable = _graphics->GetEffect()->GetVariableByName( "g_ClipmapScale" )->AsScalar();
		ClipmapSizeVariable = _graphics->GetEffect()->GetVariableByName( "g_ClipmapSize" )->AsScalar();
		HeightmapSizeVariable = _graphics->GetEffect()->GetVariableByName( "g_HeightmapSize" )->AsScalar();
		HeightmapScaleVariable = _graphics->GetEffect()->GetVariableByName( "g_HeightScale" )->AsScalar();
		HeightmapVariable = _graphics->GetEffect()->GetVariableByName( "g_txHeightmap" )->AsShaderResource();
		BlendmapVariable = _graphics->GetEffect()->GetVariableByName( "g_txBlend" )->AsShaderResource();
		TerrainMaterialCB = _graphics->GetEffect()->GetConstantBufferByName( "cbTerrainMaterial" );
		TerrainDiffuseVariable = _graphics->GetEffect()->GetVariableByName( "g_txTerrainDiffuse" )->AsShaderResource();
		TerrainNormalVariable = _graphics->GetEffect()->GetVariableByName( "g_txTerrainNormal" )->AsShaderResource();
		TerrainRoughnessVariable = _graphics->GetEffect()->GetVariableByName( "g_txTerrainRoughness" )->AsShaderResource();
		ClipmapVariable = _graphics->GetEffect()->GetVariableByName( "g_txClipmap" )->AsShaderResource();

		PaintMaskVariable = _graphics->GetEffect()->GetVariableByName( "g_txPaintMask" )->AsShaderResource();
		PaintMaskFlagVariable = _graphics->GetEffect()->GetVariableByName( "g_UsePaintMask" )->AsScalar();
		PaintMaskAbsoluteVariable = _graphics->GetEffect()->GetVariableByName( "g_LayerMaskAbsolute" )->AsScalar();
		PaintMaskInvertVariable = _graphics->GetEffect()->GetVariableByName( "g_LayerMaskInvert" )->AsScalar();
		PaintMaskInvertColorVariable = _graphics->GetEffect()->GetVariableByName( "g_LayerMaskInvertColor" )->AsScalar();
		PaintMaskModeVariable = _graphics->GetEffect()->GetVariableByName( "g_PaintMaskMode" )->AsScalar();
		PaintModeVariable = _graphics->GetEffect()->GetVariableByName( "g_PaintMode" )->AsScalar();
		PaintMaskColorVariable = _graphics->GetEffect()->GetVariableByName( "g_LayerMaskColor" )->AsScalar();
		PaintMaskModulateVariable = _graphics->GetEffect()->GetVariableByName( "g_LayerMaskModulate" )->AsScalar();
		PaintLayerIDVariable = _graphics->GetEffect()->GetVariableByName( "g_LayerID" )->AsScalar();
		PaintLayerSizeVariable = _graphics->GetEffect()->GetVariableByName( "g_PaintLayerSize" )->AsVector();
		PaintRegionVariable = _graphics->GetEffect()->GetVariableByName( "g_PaintRegion" )->AsVector();
		MouseIntersectVariable = _graphics->GetEffect()->GetVariableByName( "g_MouseSceneIntersect" )->AsVector();
		PaintTerrainPass = _graphics->GetEffect()->GetPassByName( "PaintTerrain" );
		_paintingUAVVariable = _graphics->GetEffect()->GetVariableByName( "g_PhysicalTextureUAV" )->AsUnorderedAccessView();

		// Get access to the needed effect passes
		_clipmapUpdatePass = _graphics->GetEffect()->GetPassByName( "ClipmapUpdate" );
	}

	// Binary serialize
	void Terrain::Export( std::ofstream& fout )
	{
		// Save each materal
		for ( int32 i = 0; i < m_MaxLayers; ++i )
		{
			if ( m_Layers[i]->materialBinding )
				m_Layers[i]->materialBinding->GetMaterial()->GetFilePath().Serialize( fout );
			else
				String::none.Serialize( fout );
		}

		// Properties
		Maoli::Serialize( fout, &m_HeightScale );
		Maoli::Serialize( fout, &m_Scale );
	}

	// Binary file import
	void Terrain::Import( std::ifstream& fin )
	{
		// Load each materal
		String matFile;
		for ( int32 i = 0; i < m_MaxLayers; ++i )
		{
			matFile.Deserialize( fin );
			if ( matFile != String::none )
			{
				auto mat = _graphics->CreateMaterialBindingFromFile( matFile );
				AttachMaterial( mat, i );
			}
		}

		// Properties
		Maoli::Deserialize( fin, &m_HeightScale );
		Maoli::Deserialize( fin, &m_Scale );
	}

	// Access the engine that owns the terrain
	Engine* Terrain::GetEngine()
	{
		return _graphics->GetEngine();
	}

}

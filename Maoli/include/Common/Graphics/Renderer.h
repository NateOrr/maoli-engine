//--------------------------------------------------------------------------------------
// File: Renderer.h
//
// Deferred rendering system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Graphics/Light.h"

namespace Maoli
{
	// Forward decl
	class RenderNode;
	class Effect;
	class EnvironmentProbe;
	class Sky;
	class HDRSystem;
	class Ocean;
	class Engine;
	class SpriteBatcher;
	class DebugVisualizer;
	class DynamicMesh;
	class ParticleEmitter;
	class ShadowManager;
	class InstanceSet;
	class Light;
	class ShaderUAV;
	class ShaderVector;

	// Cubemap faces
	enum class CubemapFace
	{
		PositiveX,
		NegativeX,
		PositiveY,
		NegativeY,
		PositiveZ,
		NegativeZ,
	};

	class Renderer : public System
	{
		friend class EffectPass;
		friend class DebugVisualizer;
		friend class Skybox;
		friend class ShadowManager;

	public:
		///////////////////////////////////////////////////
		// System interface

		// Setup the renderer
		virtual bool Init();

		// Register a component to the system
		virtual bool RegisterComponent( Component* component );

		// Remove a component (assumes the component exists already in the system!)
		virtual void UnregisterComponent( Component* component );

		// Cleanup
		virtual void Release();


		///////////////////////////////////////////////////
		// Renderer interface

		// Ctor
		Renderer( Engine* engine, WindowHandle window, bool windowed );

		// Create a debug visualizer for the renderer to use
		bool CreateDebugVisualizer();

		// Update scene objects
		void Update( float dt );

		// Render the scene
		void Render();



		///////////////////////////////////
		// Scene management
	public:

		// Clear the current scene
		void ClearScene();

		// Creates a new light.  This does not add it to the scene
		Light* CreateLight() const;


		///////////////////////////////////
		// Sprite rendering

		// Get the sprite batcher
		inline SpriteBatcher* GetSprite() { return _spriteBatcher; }


		///////////////////////////////////
		// Resource creation

	protected:
		// Device dependent variables created here
		virtual HRESULT OnCreateDevice();

		// Resolution dependent variables created here
		virtual HRESULT OnResetSwapChain();

		// Free resources created in OnResetSwapChain()
		virtual void OnReleasingSwapChain();

		// Free resources created in OnCreateDevice()
		virtual void OnDestroyDevice();

	public:

		// Set the render callback for a submesh
		virtual void SetRenderCallback( RenderNode& mesh );

	protected:

		// Binds effect variables
		virtual HRESULT BindEffectVariables();

		// Load the effect
		HRESULT LoadEffect( bool bind );

		// Bind the variables
		HRESULT InitVariables();

		// Update variables that depend on the camera
		void UpdateCameraVariables();


	public:

		// Clears the current frame.
		void Clear();

		// Set the clear color
		void SetClearColor( const float clearColor[4] );

		// Set the clear color
		void SetClearColor( float r, float g, float b, float a = 1.0f );

		// Presents to the back buffer.
		void Present();

		// Enable or disable vsync
		inline void EnableVSync( bool on ) { _vsync = on ? 1 : 0; }

		// Get the screen width
		inline int32 GetWidth() { return _width; }

		// Get the screen height
		inline int32 GetHeight() { return _height; }

		// Get the device
		inline ID3D11Device* GetDevice() { return _device; }

		// Get the immediate context
		inline ID3D11DeviceContext* GetContext() { return _context; }

		// Get the back buffer
		inline Texture::pointer GetBackBuffer() { return _backBuffer; }

		// Get the depth stencil
		inline Pointer<DepthStencil> GetDepthStencil() { return _depthStencil; }

		// Resize the swap chain.
		HRESULT OnResizeWindow();

		// Reload the effect
		void ReloadEffect();

		// Takes a screenshot and saves it to a file
		void TakeScreenShot();

		// Get the debug visualizer
		inline DebugVisualizer* GetDebugVisualizer() { return _visualizer; }

		// Get the effect
		inline Effect* GetEffect() { return _effect; }

		// Access the quadtree
		inline const Quadtree* GetQuadtree() const { return _quadtree; }

		// Sets the current time, and adjusts the sun position accordingly
		void SetTimeOfDay( int32 hours, int32 minutes, int32 seconds );

		// Get the time of day hours
		int32 GetTimeOfDayHours() const;

		// Sets the current date, and adjusts the sun position accordingly
		void SetDate( int32 month, int32 day, int32 year );

		// Get the postfx buffer
		inline Texture::pointer GetPostFXBuffer() { return _postFXBuffer[0]; }

		// Get the ocean
		inline Ocean* GetOcean() { return _ocean; }

		// Get the pick ray location
		inline const Vector3& GetPickRayPosition() const { return _pickRayPos; }

		// Get the pick ray direction
		inline const Vector3& GetPickRayDirection() const { return _pickRayDir; }

		// Get the mouse pick point on the terrain
		inline const Vector3& GetTerrainPickPoint() const { return _terrainPickPoint; }

		// Get the pick point in the scene
		Vector3 GetPickPoint(Model* modelToIgnore = nullptr) const;

		// Return true if the mouse is over the terrain
		inline bool MouseOnTerrain() const { return _mouseOnTerrain; }

		// Check if the mouse is on screen
		inline bool MouseOnScreen() const { return _mouseOnScreen; }

		// Set the world matrix
		inline void SetWorldMatrix( const Matrix& mat ) { _worldMatrixVariable->SetMatrix( mat ); }

		// Get the billboard matrix
		inline const Matrix& GetBillboardMatrix() const { return _billboardMatrix; }

		// Get a list of models that are contained by a frustum
		bool GetModelsInFrustum( const Frustum& frustum, Array<Model*>& models );



		/////////////////////////////////////
		// Factory methods

		// Depth stencil creation
		DepthStencil::pointer CreateDepthStencil( uint32 width, uint32 height, DXGI_SAMPLE_DESC* pMSAA, bool asTexture );
		DepthStencil::pointer CreateDepthStencilCube( uint32 width, uint32 height );

		// Load texture from file
		Texture::pointer CreateTextureFromFile( const char* file, bool cubemap = false );

		// Create a texture array from a sprite sheet
		Texture::pointer CreateTextureArrayFromSpriteSheet( const char* file, int32 frameWidth, int32 frameHeight, int32 width, int32 height );

		// 	Create from an existing 2d texture
		Texture::pointer CreateTextureFromTexture( ID3D11Texture2D* pTex, bool isRTV, bool isSRV );

		// Create a 2d texture
		Texture::pointer CreateTexture( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV );

		// Create a texture from some initial data
		Texture::pointer CreateTextureFromData( uint32 width, uint32 height, DXGI_FORMAT format, D3D11_USAGE usage, bool srv, void* data );

		// Create a 2d texture with more options.
		Texture::pointer CreateTextureEx( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage,
			DXGI_FORMAT rtvFormat, DXGI_FORMAT srvFormat, DXGI_FORMAT uavFormat );

		// 	Create a an array of 2d textures
		Texture::pointer CreateTextureArray( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT* formats, uint32 mipLevels, DXGI_SAMPLE_DESC* pMSAA, D3D11_USAGE usage, bool isRTV, bool isSRV );

		// Create a texture array, where the actual resource is bound as an array
		// Currently only SRV arrays are supported
		Texture::pointer CreateTextureArrayResource( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels, bool rtv );

		// 	Create a cubemap texture.
		Texture::pointer CreateTextureCube( uint32 width, uint32 height, DXGI_FORMAT format, uint32 mipLevels, bool asSingleCube );
		Texture::pointer CreateTextureCubeArray( uint32 width, uint32 height, uint32 arraySize, DXGI_FORMAT format, uint32 mipLevels );

		// Insert a texture into an array resource
		HRESULT InsertTextureIntoTextureArray( const char* file, Texture::pointer tex, uint32 index ) const;

		// Loading into staging textures
		bool StageDDS( const char* file, Texture::pointer tex );
		bool StageDDS( const char* file, byte* tex, uint32 width, uint32 height );

		// Texture saving
		void SaveTexture( const char* file, Texture::pointer tex, D3DX11_IMAGE_FILE_FORMAT format ) const;
		void SaveTexture( const char* file, Texture::pointer tex, uint32 index, D3DX11_IMAGE_FILE_FORMAT format ) const;
		void SaveDDS( const char* file, Texture::pointer tex, D3DX11_IMAGE_FILE_FORMAT format ) const;
		void SaveStagingDDS( const char* file, Texture::pointer tex ) const;
		void SaveStagingDDS( const char* file, byte* tex, uint32 width, uint32 height ) const;



		/////////////////////////////////////
		// Texture copying

		// 	Resolve an MSAA texture into a regular texture.
		void ResolveTexture( Texture::pointer dest, Texture::pointer source ) const;

		// 	Copy a another texture into this texture
		void CopyTextureToTexture( Texture::pointer dest, Texture::pointer source ) const;

		// 	Copy a another texture into this texture
		void CopyTextureToTexture( Texture::pointer dest, ID3D11Texture2D* source ) const;

		// Copy a region of a texture into a buffer
		HRESULT CopyTextureToBuffer( uint32 texelSize, BYTE* pDest, uint32 dx, uint32 dy, uint32 destWidth,
			Texture::pointer pSource, uint32 subresource, uint32 left, uint32 top, uint32 right, uint32 bottom ) const;

		// Copy a region of a buffer into a texture
		HRESULT CopyBufferToTexture( uint32 texelSize, Texture::pointer pDest, uint32 subresource, uint32 dx, uint32 dy,
			BYTE* pSource, uint32 left, uint32 top, uint32 right, uint32 bottom, uint32 sourceWidth ) const;

		// Copy a region of a buffer into another buffer
		HRESULT CopyBufferRegion( uint32 texelSize, BYTE* pDest, uint32 dx, uint32 dy, uint32 destWidth,
			BYTE* pSource, uint32 left, uint32 top, uint32 right, uint32 bottom, uint32 sourceWidth ) const;

		// 	Copy a render target into this texture.
		void CopyRenderTargetToTexture( Texture::pointer dest, ID3D11RenderTargetView* source ) const;

		// Filter a texture
		void FilterTexture( D3DX11_FILTER_FLAG filter, Texture::pointer tex, int32 mipSlot = 0 );



		/////////////////////////////////////
		// Buffers

		// Create a new buffer
		template <class T>
		typename Buffer<T>::pointer CreateBuffer( int32 length, D3D11_USAGE usage, bool UAV, bool SRV, const T* data = nullptr )
		{
			Buffer<T>::pointer buffer( new Buffer<T>( this ) );
			if ( FAILED( buffer->Create( length, usage, UAV, SRV, data ) ) )
				buffer = nullptr;
			return buffer;
		}


		// Create a vertex buffer
		template <class T>
		typename Buffer<T>::pointer CreateVertexBuffer( int32 length, D3D11_USAGE usage, const T* data = nullptr )
		{
			Buffer<T>::pointer buffer( new Buffer<T>( this ) );
			if ( FAILED( buffer->CreateVertexBuffer( length, usage, data ) ) )
				buffer = nullptr;
			return buffer;
		}


		// Create an index buffer
		template <class T>
		typename Buffer<T>::pointer CreateIndexBuffer( int32 length, D3D11_USAGE usage, const T* data = nullptr )
		{
			Buffer<T>::pointer buffer( new Buffer<T>( this ) );
			if ( FAILED( buffer->CreateIndexBuffer( length, usage, data ) ) )
				buffer = nullptr;
			return buffer;
		}


		/////////////////////////////////////
		// Font loading

		// Load a font from file
		Font::pointer CreateFontFromFile( const char* file );


		/////////////////////////////////////
		// Mesh creation

		// Creates a mesh binding from existing geometry
		MeshBinding::pointer GetMeshBinding( Geometry* geometry );

		// Create a new mesh binding from file
		MeshBinding::pointer GetMeshBindingFromFile( const char* file );


		/////////////////////////////////////
		// Materials

		// Create a material binding
		MaterialBinding::pointer CreateMaterialBinding( Material* material );

		// Create a new material binding by name
		MaterialBinding::pointer CreateMaterialBinding( const char* name );

		// Create a new material binding from file
		MaterialBinding::pointer CreateMaterialBindingFromFile( const char* file );



		////////////////////////////////////////////////
		// State management

		// Clear all output bindings from the pipeline
		inline void ClearOutputBindings() { _state.ClearOutputBindings( true ); }

		// Clear input bindings from a resource
		inline void ClearInputBindings( PipeplineResource* resource ) { _state.ClearInputBindings( resource ); }

		// Bind only a depth stencil to the graphics output
		inline void BindDepthStencilWithoutRenderTarget( DepthStencil::pointer depthStencil ) { _state.BindDepthStencilWithoutRenderTarget( depthStencil ); }

		// Bind the render target with a null dsv
		inline void BindRenderTargetWithoutDepth( PipeplineResource* resource, uint32 index = 0 ) { _state.BindRenderTargetWithoutDepth( resource, index ); }

		// 	Bind the render target to the d3d11 pipeline
		inline void BindRenderTarget( PipeplineResource* resource, uint32 index = 0 ) { _state.BindRenderTarget( resource, index ); }

		// 	Bind the render target to the d3d11 pipeline with a readonly depth buffer
		inline void BindRenderTargetWithReadOnlyDepth( PipeplineResource* resource, uint32 index = 0 ) { _state.BindRenderTargetWithReadOnlyDepth( resource, index ); }

		// 	Bind render target array to the d3d11 pipeline
		inline void BindRenderTargetArray( PipeplineResource* resource, uint32 numResources = 0 ) { _state.BindRenderTargetArray( resource, numResources ); }

		// Bind a single UAV to the graphics pipeline
		inline void BindUnorderedAccessView( uint32 slot, PipeplineResource* resource ) { _state.BindUnorderedAccessView( slot, resource ); }

		// Bind both render targets and a uav
		inline void BindRenderTargetsAndUnorderedAccessViews( PipeplineResource* rtvResource, uint32 numResources, PipeplineResource* uavResource, uint32 uavSlot, uint32 uavCount )
		{
			_state.BindRenderTargetsAndUnorderedAccessViews( rtvResource, numResources, uavResource, uavSlot, uavCount );
		}

		// Clear the render target and dsv
		inline void ClearRenderTarget() { _state.ClearRenderTarget(); }

		// 	Sets the viewport for the current d3d11 context
		void SetViewport( const D3D11_VIEWPORT& viewPort );

		// Set the input layout
		inline void SetInputLayout( ID3D11InputLayout* pInputLayout ) { _state.SetInputLayout( pInputLayout ); }

		// Set the material
		inline void SetMaterial( MaterialBinding::pointer mat ) { _state.SetMaterial( mat ); }

		// Bind a single material texture
		inline void BindMaterialTexture( MaterialBinding::pointer mat, TEX_TYPE type ) { _state.BindMaterialTexture( mat, type ); }
		inline void BindMaterialTexture( Texture::pointer tex, TEX_TYPE type ) { _state.BindMaterialTexture( tex, type ); }

		// Set only the textures for a mterial
		inline void BindMaterialTextures( MaterialBinding::pointer mat ) { _state.BindMaterialTextures( mat ); }

		// Set the vertex buffer
		inline void SetVertexBuffer( ID3D11Buffer* pVB, uint32 stride ) { _state.SetVertexBuffer( pVB, stride ); }

		// Set the index buffer
		inline void SetIndexBuffer( ID3D11Buffer* pIB, DXGI_FORMAT size ) { _state.SetIndexBuffer( pIB, size ); }

		// Set the cubemap
		inline void SetCubeMap( Texture::pointer pCubeMap ) { _state.SetCubeMap( pCubeMap ); }

		// Set the topology
		inline void SetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY type ) { _state.SetPrimitiveTopology( type ); }

		// Set the compute shader
		inline void SetComputeShader( ID3D11DeviceChild* shader ) { _state.SetComputeShader( shader ); }

		// Set the vertex
		inline void SetVertexShader( ID3D11DeviceChild* shader ) { _state.SetVertexShader( shader ); }

		// Set the hull shader
		inline void SetHullShader( ID3D11DeviceChild* shader ) { _state.SetHullShader( shader ); }

		// Set the domain shader
		inline void SetDomainShader( ID3D11DeviceChild* shader ) { _state.SetDomainShader( shader ); }

		// Set the geometry
		inline void SetGeometryShader( ID3D11DeviceChild* shader ) { _state.SetGeometryShader( shader ); }

		// Set the pixel shader
		inline void SetPixelShader( ID3D11DeviceChild* shader ) { _state.SetPixelShader( shader ); }

		// Set the blend state
		inline void SetBlendState( ID3D11BlendState* state, FLOAT factor[4], uint32 mask ) { _state.SetBlendState( state, factor, mask ); }

		// Set the rasterizer state
		inline void SetRasterizerState( ID3D11RasterizerState* state ) { _state.SetRasterizerState( state ); }

		// Set the depth stencil state
		inline void SetDepthStencilState( ID3D11DepthStencilState* state, uint32 stencilRef ) { _state.SetDepthStencilState( state, stencilRef ); }

		// Get the frame stats
		inline const FrameStats& GetFrameStats() const { return _state.GetFrameStats(); }

		// Apply a shader to the pipeline
		inline void SetShader( Shader* shader ) { _state.SetShader( shader ); }

		// Unbind a shader
		inline void ClearShader( ShaderType shader ) { _state.ClearShader( shader ); }

		// Set the quad vertex buffer
		void SetRenderToQuad();

		// Set a null vertex buffer
		inline void ClearVertexBuffer() { _state.ClearVertexBuffer(); }

		// Add on a vertex stream
		inline void AddVertexStream( ID3D11Buffer* pStream, uint32 stride ) { _state.AddVertexStream( pStream, stride ); }

		// Bind the available streams and reset them
		inline void BindVertexStreams() { _state.BindVertexStreams(); }


		// Checks if a mesh within the camera view.  This does not take occulsion into account
		bool IsMeshVisible( Model& mesh, Camera& camera );

		// Checks if a submesh within the camera view.  This does not take occulsion into account
		bool IsMeshVisible( RenderNode& mesh, Camera& camera );

		// Add a dynamic mesh to be rendered this frame
		inline void DrawMesh( DynamicMesh* mesh ) { _dynamicMeshes.Add( mesh ); }


		////////////////////////////////////////////////
		// Material blending

		// Linearly blend two materials
		void BlendMaterials( MaterialBinding::pointer base, MaterialBinding::pointer layer, Texture::pointer mask, MaterialBinding::pointer result );

		// Compute a normal map from a bump map
		Texture::pointer ComputeNormalMap( Texture::pointer bumpMap );

		// Store a height map in the alpha channel of a normal map
		Texture::pointer StoreHeightInNormalMap( Texture::pointer normalMap, Texture::pointer heightMap );


		///////////////////////////////////////////////
		// Scene management

		// Water rendering
		inline void EnableOcean( bool b ) { _hasOcean = b; }

		// True if this scene uses an ocean
		inline bool HasOcean() const { return _hasOcean; }

		// Total number of polygons in the current scene
		uint32 GetPolyCount() const;

		// Searches the material list, and deletes any materials that are not used by a mesh
		void RemoveUnusedMaterials();

		// Adds a light to the scene
		void AddLight( Light* pLight );

		// Removes a light from the scene, but does not delete it
		void RemoveLight( Light* pLight );

		// Get the number of lights contained in the scene
		inline uint32 GetNumLights() const { return _lights.Size(); }

		// Get a light using it's index in the scene list
		inline Light* GetLight( int32 i ) { return _lights[i]; }

		// Checks if a light is within the camera view.  This does not take occulsion into account
		bool IsLightVisible( Light& light );

		// Create an empty mesh
		Model* CreateMesh();

		// Adds a mesh object to the scene
		void AddModel( Model* pMesh );

		// Add a submesh to the scene
		void AddSubmesh( RenderNode* node );

		// Remove a submesh from the scene
		void RemoveSubmesh( RenderNode* node );

		// Insert a visible render node to be rendered
		void InsertVisibleRenderNode( RenderNode* node );
		void InsertVisibleTransparentRenderNode( RenderNode* node );

		// Get the number of meshes contained in the scene
		inline uint32 GetNumModels() const { return _models.Size(); }

		// Get a mesh using it's index in the scene list
		inline Model* GetModel( int32 i ) { return _models[i]; }

		// Removes a mesh from the scene, but does not delete it
		void RemoveModel( Model* pMesh );

		// Permanently deletes a mesh
		void DeleteMesh( Model* pMesh );

		// Permanently deletes a sub-mesh
		void DeleteSubMesh( Model* pMesh, int32 index );

		// Set the color of the ambient lighting
		void SetAmbientLighting( float r, float g, float b );

		// Get a the active camera
		inline Camera* GetCamera() { return _camera; }

		// Set the camera
		void SetCamera( Camera* camera );

		// Build a quadtree for the current scene
		void BuildQuadtree();

		// Render the quadtree
		void RenderQuadtree();

		// Update the quadtree
		void UpdateQuadtree( Model* model );

		// Create a new emitter
		ParticleEmitter* CreateParticleEmitter( char* texFile, int32 maxParticles, int32 freq, float lifetime );

		// Delete an emitter
		void DeleteParticleEmitter( ParticleEmitter* emitter );


		// Create a new environment probe
		EnvironmentProbe* AddProbe( bool dynamic );

		// Add an file-based probe
		EnvironmentProbe* AddProbe( const char* file );


		// Removes a probe from the scene and deletes it
		void RemoveProbe( EnvironmentProbe* probe );

		// Gets the number of environment probes that are active in the scene
		uint32 GetNumProbes() { return _probes.Size(); }

		// Gets an environment probe from it's index in the scene list
		EnvironmentProbe* GetProbe( int32 i ) { return _probes[i]; }

		// Show/hide terrain
		void ShowTerrain( bool show ) { _showTerrain = show; }

		// Creates a terrain from a heightmap file
		Terrain* ImportHeightmap( const char* file );

		// Saves the terrain heightmap to a png
		void ExportHeightmap( const char* file );

		// Creates a blank terrain
		Terrain* CreateHeightmap( uint32 size );

		// Delete the terrain
		void ClearTerrain();


		// Get the terrain.  Will return nullptr if no terrain exists
		inline Terrain* GetTerrain() { return _terrain; }

		// Get the skybox
		inline Skybox& GetSkybox() { return *_skybox; }

		// Load a skybox
		bool LoadSkyboxCubemap( const char* file );

		// Enable / disable sun shadows
		inline void EnableSunShadows( bool value ) { _sunShadows = value; }

		// Perform a pre-z fill pass
		void ZFill();

		// Render all instance sets
		void RenderInstanceSets();

		// Render all visible meshes
		void RenderVisibleMeshes();

		// Render all transparent meshes
		void RenderTransparentMeshes();

		// Perform frustum culling for all meshes
		void DetermineVisibleMeshes();

		// Process a visible model
		void ProcessVisibleMesh(QuadtreeElement* obj);

		// Get the total number of visible meshes
		inline uint32 GetNumVisibleMeshes() const { return _visibletransparentMeshes.Size() + _visibleMeshes.Size(); }

		// Get the number of visible lights
		inline uint32 GetNumVisibleLights() const { return _numVisibleLights; }

		// Get the number of render nodes
		inline uint32 GetNumSubMesh() const { return _renderList.Size(); }

		// Render a submesh
		void RenderSubMesh( RenderNode& mesh );
		void RenderSubMeshPos( RenderNode& mesh );
		void RenderSubMeshSkinned( RenderNode& mesh );
		void RenderSubMeshPosSkinned( RenderNode& mesh );

		// Render a submesh, automatically applying the material/world matrix
		void RenderSubMesh( RenderNode& mesh, EffectPass* effect, bool useMaterial = true );
		void RenderSubMeshPos( RenderNode& mesh, EffectPass* effect, bool useMaterial = true );
		void RenderSubMeshSkinned( RenderNode& mesh, EffectPass* effect, bool useMaterial = true );
		void RenderSubMeshPosSkinned( RenderNode& mesh, EffectPass* effect, bool useMaterial = true );

		// Render a two sided transparent mesh
		void RenderTwoSidedSubMesh( RenderNode& mesh, EffectPass* pass, bool useMaterial = true );
		void RenderTwoSidedSubMeshSkinned( RenderNode& mesh, EffectPass* pass, bool useMaterial = true );

		// Render a dynamic mesh
		void RenderDynamicMesh( DynamicMesh& mesh );

		// Draw a decal at a location
		void DrawDecal( uint32 hitID, const Vector3& location, const Vector3& size, const Vector3& rotation, Texture::pointer decal, float duration, const Color& color = Color::White );

	protected:

		// Fill the deferred shading gbuffers
		void FillGBuffers();

		// Performs the shading phase on the GBuffers
		void DeferredShading();

		// Render the decals
		void RenderDecals();

		// Render special effects
		virtual void RenderEffects() {}


		////////////////////////////////////////////////
		// Rendering methods

		// Renders all the probes
		void RenderProbes(bool forceRender = false);
		void RenderProbe( EnvironmentProbe& probe );

		// Draw a submesh a number of times
		void RenderSubMeshInstanced( RenderNode& mesh, EffectPass* pass, int32 numInstances );


		// Render the scene
		void RenderScene();

		// Renders the terrains
		void RenderTerrain( EffectPass* pPass );

		// Creates a planar reflection texture
		void RenderWaterReflection( Texture::pointer surface );

		// Draw the sky
		void RenderSky( const Vector3& pos, Texture::pointer target, uint32 targetIndex = 0 );

		// Draw water
		void RenderWater();

		// Create a new instance set if needed
		void CreateInstanceSet(RenderNode* node);

		// Remove an instance set if needed
		void RemoveInstanceSet(RenderNode* node);


		int32							_width, _height;	// Back buffer dimensions
		WindowHandle				_window;			// Window handle
		ID3D11Device*				_device;			// D3D11 device
		D3D_DRIVER_TYPE				_driverType;		// Should be hardware
		IDXGISwapChain*             _swapChain;			// Rendering swap chain
		ID3D11DeviceContext*		_context;			// Immediate context
		Texture::pointer			_backBuffer;		// Main render target
		DepthStencil::pointer		_depthStencil;		// Main depth buffer
		float						_clearColor[4];		// Background clear color
		bool						_lostDevice;		// True when the device is lost
		uint32						_vsync;				// Determines vsync

		// Resource managers
		ResourceManager<Renderer, Texture>				_textureManager;
		ResourceManager<Renderer, Font>					_fontManager;
		ResourceManager<Renderer, MaterialBinding>		_materialManager;
		ResourceManager<Renderer, MeshBinding>			_meshManager;

		HDRSystem*	_hdr;				// Manages the hdr pipeline
		Sky*		_sky;				// Handles sky rendering
		Ocean*		_ocean;				// Handles ocean rendering
		bool		_skyNeedsUpdate;	// True when the atmospheric scattering needs to be updated
		Effect*		_effect;			// Manages all shaders for the engine

		Vertex						_quadVerts[6];					// Verts for the quad
		Buffer<Vertex>::pointer		_quadVertexBuffer;				// Contains a quad to render for light passes
		Matrix						_viewProjMatrix;				// View/projection matrix
		Matrix						_oldViewProjMatrix;				// View/projection matrix from the previous frame
		Matrix						_billboardMatrix;				// Camera billboard matrix
		Texture::pointer			_velocityMap[2];				// Per-pixel velocity for motion blur
		Texture::pointer			_postFXBuffer[2];				// Misc post fx
		Texture::pointer			_halfResFrameBuffer[2];			// Half res buffer for transparency/reflection effects
		DepthStencil::pointer		_reflectionDepthStencil;		// Depth buffer for the reflection texture
		uint32						_currentVelocityIndex;			// Current velocity sampler
		uint32						_previousVelocityIndex;			// Old velocity sampler
		SpriteBatcher*				_spriteBatcher;					// Sprite rendering
		Array<DynamicMesh*>			_dynamicMeshes;					// Set of dynamic meshes to render
		Vector3						_pickRayPos;					// Ray in world space from mouse posisiton into the scene
		Vector3						_pickRayDir;					// Ray in world space from mouse posisiton into the scene
		bool						_mouseOnScreen;					// True if the mouse is on the screen
		Buffer<Vector2>::pointer	_hammersleyBuffer;				// Hammersley sequence for importance sampling integral
		Vector4						_screenToViewX;					// Screen-View space basis X vector
		Vector4						_screenToViewY;					// Screen-View space basis Y vector
		Vector4						_screenToViewZ;					// Screen-View space basis Z vector
		Texture::pointer			_frameBuffer;					// Copy of the frame buffer

		// Scene stuff
		Camera*						_camera;					// Default camera
		Array<ParticleEmitter*>		_emitters;						// Particle emitters
		Array<Light*>				_spotShadowLights;			// Spot lights that use shadows
		Array<Light*>				_pointShadowLights;			// Point lights that use shadows
		Array<Light*>				_spotLights;				// Spot lights
		Array<Light*>				_pointLights;				// Point lights
		Array<Light*>				_lights;					// ALL lights
		Array<Model*>				_models;					// Mesh objects
		Array<RenderNode*>			_renderList;				// List of submeshes to be rendered
		List<RenderNode*>			_visibleMeshes;				// Sorted list of submeshes to be rendered
		List<RenderNode*>			_visibletransparentMeshes;	// Objects with transparency enabled
		Terrain*					_terrain;					// The terrain
		bool						_showTerrain;				// True to show the terrain
		Vector4						_ambient;					// Ambient lighting
		uint32						_numPolygons;				// Total polygons in the scene
		String						_name;						// Name of this scene
		bool						_hasOcean;					// True if this scene has an ocean
		Skybox*						_skybox;					// The skybox
		Array<EnvironmentProbe*>	_probes;					// Environment probes
		DepthStencil::pointer		_probeDepthStencil;			// Depth stencil for environment map rendering
		Matrix						_probeProjMatrix;			// Projection matrix for environment map rendering
		uint32						_numVisibleLights;			// Number of visible lights
		bool						_sunShadows;				// True for sun shadows
		Quadtree*					_quadtree;					// Tree for the scene
		float						_timer;						// Game timer

		// Instance set map
		// Instance sets are automaticall created as needed
		static const uint32						_maxInstances = 1024;	// Max instance count per draw call
		Buffer<Matrix>::pointer					_instanceBuffer;		// GPU instance data
		std::unordered_map<uint32, InstanceSet*>	_instanceSets;			// Instance set for each active instance id			

		// Construct the hammersley sequence buffer and bind it to the shader
		void ComputeHammersleySequence( int32 n );

		// For the editor
		DebugVisualizer*	_visualizer;

		// Effect passes
		EffectPass* _boxBlurHorizontalPass;				// Seperable box blur
		EffectPass* _boxBlurVerticalPass;				// Seperable box blur
		EffectPass* _bilateralBlurHorizontalPass;		// Seperable box blur
		EffectPass* _bilateralBlurVerticalPass;			// Seperable box blur

		// Effect variables
		ShaderScalar*		_filterKernelVariable;		// Kernel for blur filters

		// Probe Quality settings
		static struct ProbeSettings
		{
			static uint32 mipLevels;
			static DXGI_FORMAT format;
			static uint32 miplevels;

			static inline void Init()
			{
				mipLevels = 256;
				format = DXGI_FORMAT_R8G8B8A8_UNORM;
				miplevels = 1;
			}
		};


		/////////////////////////////////////////////
		// State management

		RenderState				_state;


		/////////////////////////////////////////////
		// Clustered shading
	private:
		// Light index offsets/counts for a cluster
		struct LightIndexData
		{
			uint32 start;
			uint32 numShadowSpotLights;
			uint32 numShadowPointLights;
			uint32 numSpotLights;
			uint32 numPointLights;
		};

	public:

		// Types of lights for clusters
		enum class ClusterLightType
		{
			SpotShadow,
			PointShadow,
			Spot,
			Point,
		};

		// Very coarse cluster culling
		struct Cluster
		{
			uint32 id;
			Vector3 min;
			Vector3 max;
			bool active;
			Array<uint32> lights[4];
			Vector3 start[4];
			Vector3 end[4];
		};

		// Cluster storage
		typedef std::unordered_map<Cluster*, Cluster*> ClusterMap;

		// Get the total number of clusters
		inline uint32 GetNumClusters() const { return _numClusters; }

		// Add light to clusters it intersects with
		void ProcessLightClusters( const Matrix& viewMatrix, const Matrix& viewProjMatrix, const Light& light, uint32 lightIndex, ClusterLightType type, ClusterMap& activeClusters );

		// Get the sun color
		inline const Color& GetSunColor() const { return _sunColor; }

		// Get the sun direction
		inline const Vector3& GetSunDirection() const { return _sunDirection; }

		// Set the sun color
		void SetSunColor( const Color& color );

		// Sky rendering
		void EnableSky( bool b );

		// True if this scene uses a sky
		inline bool HasSky() const { return _hasSky; }

		// Get the sun shadow softness
		int32 GetSunShadowSoftness() const { return _sunShadowSoftness; }

		// Set the sun shadow softness
		void SetSunShadowSoftness( int32 val ) { _sunShadowSoftness = val; }

	protected:


		// Construct the clusters in view space
		void BuildClusters();

		// Build the light list each frame
		void BuildVisibleLightBuffer();
		void BuildVisibleLightBufferGPU();

		// Get a map containing the active clusters from this frame
		inline const ClusterMap& GetActiveClusters() const { return _activeClusters; }

		// Process the active clusters, building the final index buffer
		void ProcessClusters();

		// Properties to render a decal
		struct DecalInfo
		{
			uint32 hitID;
			Vector3 position;
			Vector3 scale;
			Vector3 rotation;
			Color color;
			Texture::pointer decal;
			float duration;
			float timer;
			Matrix matrix;
			Matrix viewProjMatrix;
		};

		// Graphics settings
		struct
		{
			bool motionBlur;
			bool fxaa;
			bool ssao;
			bool hdr;
		} _settings;

		uint32											_tileSize;					// Tile width/height for tile shading
		uint32											_clusterSlices;				// # of depth slices for each tile
		uint32											_maxVisibleLights;			// Max number of visible lights per cluster
		uint32											_maxVisibleLightsPerCluster;// Max number of visible lights per cluster
		uint32											_numClusters;				// Total cluster count
		uint32											_tileWidth;					// # of horizontal tiles
		uint32											_tileHeight;				// # of vertical tiles
		Array<Cluster>									_clusters;					// Clusters for grouping lights
		std::unordered_map<Cluster*, Cluster*>			_activeClusters;			// Clusters that contain lights
		Array<Light*>									_visibleSpotShadowLights;	// Visible spot lights that cast shadows for this frame
		Array<Light*>									_visibleSpotLights;			// Visible spot lights for this frame
		Array<Light*>									_visiblePointShadowLights;	// Visible point lights that cast shadows for this frame
		Array<Light*>									_visiblePointLights;		// Visible point lights for this frame
		Array<Light::PointLightShaderData>				_pointLightData;			// Visible point light data
		Array<Light::SpotLightShaderData>				_spotLightData;				// Visible spot light data
		Buffer<Light::PointLightShaderData>::pointer	_pointLightListBuffer;		// List of all visible point lights for each frame
		Buffer<Light::SpotLightShaderData>::pointer		_spotLightListBuffer;		// List of all visible spot lights for each frame
		Array<LightIndexData>							_lightIndexData;			// CPU double buffer for light index data
		Buffer<LightIndexData>::pointer					_lightIndexBuffer;			// Light indexing data for each cluster
		Buffer<uint32>::pointer							_clusterIndexBuffer;		// Raw light indices for each cluster
		Array<uint32>										_clusterIndices;			// CPU double buffer for cluster indices
		Buffer<uint32>::pointer							_clusterIndexOffsetBuffer;	// Offset into the indices buffer, for the CS
		Vector3											_sunDirection;				// Sun direction
		Color											_sunColor;					// Sun color
		bool											_hasSky;					// True if this scene has a sky
		ShadowManager*									_shadowManager;				// Shadow system
		int32												_sunShadowSoftness;			// Shadow softness
		List<DecalInfo>									_decals;					// Decals
		Vector3											_terrainPickPoint;			// Mouse pick point on the terrain
		bool											_mouseOnTerrain;			// True if the mouse is over the terrain
		bool											_fullscreen;				// True if the game is in fullscreen

		std::unordered_map<Material*, List<RenderNode*>>		_materialNodeMap;			// Maps render nodes to materials

		ShaderResource*		_pointLightListVariable;		// Visible light list for clustered shading
		ShaderResource*		_spotLightListVariable;			// Visible light list for clustered shading
		ShaderResource*		_lightIndexDataVariable;		// Index data offsets/lengths per cluster
		ShaderResource*		_clusterIndicesVariable;		// Raw index lists for each cluster
		ShaderUAV*			_clusterIndicesOffsetUAV;		// Offset into the index buffer
		ShaderUAV*			_clusterIndicesUAV;				// Cluster light indices
		ShaderUAV*			_lightIndexDataUAV;				// Per cluster light offset/counts
		ShaderScalar*		_tileWidthVariable;				// # of tiles across the screen horizontally
		ShaderScalar*		_tileHeightVariable;			// # of tiles across the screen vertically
		ShaderScalar*		_clusterSlicesVariable;			// # of cluster slices
		ShaderScalar*		_tileSizeVariable;				// Tile dimensions
		ShaderScalar*		_maxLightsPerClusterVariable;	// Max lights per cluster
		ShaderScalar*		_numClustersVariable;			// Total number of clusters
		ShaderScalar*		_numShadowPointLightsVariable;	// Total number of lights
		ShaderScalar*		_numShadowSpotLightsVariable;	// Total number of lights
		ShaderScalar*		_numPointLightsVariable;		// Total number of lights
		ShaderScalar*		_numSpotLightsVariable;			// Total number of lights
		ShaderVector*		_sunDirectionVariable;			// Sun direction
		ShaderVector*		_sunColorVariable;				// Sun color
		EffectPass*			_clusterLightCullPointCS;		// GPU light culling
		EffectPass*			_clusterLightCullPointShadowCS;	// GPU light culling
		EffectPass*			_clusterLightCullSpotCS;		// GPU light culling
		EffectPass*			_clusterLightCullSpotShadowCS;	// GPU light culling
		EffectPass*			_resetClustersCS;				// Reset cluster data before culling takes place



		// GBuffer types
		enum DS_TYPE
		{
			DS_COLOR = 0,
			DS_NORMAL,
			DS_FLAT_NORMAL,
			DS_MATERIAL,
			DS_EMISSIVE,
			DS_SIZE,
		};

		// Deferred shading
		Texture::pointer	_gbuffer;				// Deferred shading GBuffer
		Texture::pointer	_idTexture;				// Object ID gbuffer
		Texture::pointer	_gbufferHalfRes;		// Half res gbuffer for faster ao
		Texture::pointer	_aoTexture;				// Ambient occlusion texture
		Texture::pointer	_aoBlurTexture;			// Used for blurring the AO texture
		Texture::pointer	_emissiveBlurTexture;	// Used for blurring the emissive texture
		Model*				_box;					// Box mesh for decals

	protected:

		/////////////////////////////////////////////
		// Effect passes
		/////////////////////////////////////////////


		// States
		EffectPass* _noCullState;
		EffectPass* _wireframeState;
		EffectPass* _smoothTesselateState;

		EffectPass* _zfillPass;						// ZFill
		EffectPass* _zfillSkinnedPass;				// ZFill
		EffectPass* _forwardPass;					// Very basic forward render pass
		EffectPass* _forwardTerrainPass;			// Very basic forward render pass for terrain
		EffectPass* _forwardShadingPass;			// Primary shading pass
		EffectPass* _forwardShadingAlphaPass;		// Primary shading pass
		EffectPass* _forwardShadingInstancedPass;	// Primary shading pass with instancing
		EffectPass* _forwardShadingSkinnedPass;		// Primary shading pass for skinned meshes

		EffectPass* _reflectPass;					// Planar reflection
		EffectPass* _terrainReflectPass;			// Terrain planar reflection
		EffectPass* _screenReflectPass;				// Screen-space reflection
		EffectPass* _localReflectionPass;			// Cubemap reflection
		EffectPass*	_downscalePass;					// Downscale the back buffer
		EffectPass*	_bilateralUpsamplePass;			// Upscale a texture into another texture

		EffectPass* _positionOnlyWireframePass;		// Wireframe render
		EffectPass* _positionOnlyColorPass;			// Material color

		EffectPass* _skyPass;						// Draws the sky with atmospheric scattering
		EffectPass* _skyReflectPass;				// Draws the sky with atmospheric scattering as planar reflection

		EffectPass* _waterPass;						// Water rendering

		EffectPass* _velocityMapPass;				// Builds a velocity map
		EffectPass* _motionBlurPass;				// Performs motion blur
		EffectPass* _gaussianBlurHorizontalPass;	// Horizontal gaussian blur
		EffectPass* _gaussianBlurVerticalPass;		// Vertical gaussian blur
		EffectPass* _fxaaPass;						// FXAA (anti-aliasing)

		// Deferred shading passes
		EffectPass* _gbufferInstancedPass;			// GBuffer fill for instancing
		EffectPass* _gbufferInstancedAlphaPass;		// GBuffer fill for instancing
		EffectPass* _gbufferPass;					// GBuffer fill
		EffectPass* _gbufferAlphaPass;				// GBuffer fill
		EffectPass* _gbufferSkinnedPass;			// GBuffer fill
		EffectPass* _gbufferCubemapPass;			// GBuffer fill
		EffectPass* _gbufferRefractPass;			// GBuffer fill
		EffectPass* _gbufferTerrainPass;			// GBuffer fill for terrain
		EffectPass* _gbufferDownscalePass;			// GBuffer downscaling
		EffectPass* _deferredCubeIBLPass;			// Cube map, image based lighting
		EffectPass* _shadingPass;					// Primary shading pass
		EffectPass* _shadingPassNoSun;				// Primary shading pass
		EffectPass* _shadingPassNoSunShadows;		// Primary shading pass
		EffectPass* _transparentPass;				// Primary shading pass for transparent meshes
		EffectPass* _transparentAlphaPass;			// Primary shading pass for transparent meshes
		EffectPass* _transparentSkinnedPass;		// Primary shading pass for transparent skinned meshes
		EffectPass* _ambientOcclusionPass;			// Ambient occlusion
		EffectPass* _decalPass;						// Deferred decals

		// Effect variables
		ShaderResource*		_gbufferNormalVariable;		// Gbuffers
		ShaderResource*		_gbufferFlatNormalVariable;	// Gbuffers
		ShaderResource*		_gbufferColorVariable;		// Gbuffers
		ShaderResource*		_gbufferMaterialVariable;	// Gbuffers
		ShaderResource*		_gbufferEmissiveVariable;	// Gbuffers
		ShaderResource*		_gbufferIDVariable;			// ID texture
		ShaderResource*		_decalTextureVariable;		// Decal texture
		ShaderMatrix*		_decalMatrixVariable;		// Decal view projection matrix
		ShaderVector*		_decalColorVariable;		// Decal color


		/////////////////////////////////////////////
		// Effect vars

		ShaderResource*		_frameBufferVariable;		// Back buffer texture, used for other fullscreen buffers as well
		ShaderResource*		_bonePaletteVariable;		// Bone matrix palette for animation
		ShaderScalar*		_elapsedTimeVariable;		// Elapsed frame time
		ShaderScalar*		_timerVariable;				// Elapsed frame time
		ShaderResource*		_depthBufferVariable;		// Depth buffer
		ShaderScalar*		_farZVariable;				// Clip plane far z
		ShaderScalar*		_nearZVariable;				// Clip plane near z
		ShaderVector*		_ambientVariable;			// Ambient lighting
		ShaderScalar*		_objectIDVariable;			// Submesh ID

		// Post processing
		ShaderResource*			_postFxVariable;		// PostFX textures
		ShaderResource*			_postFxVariable2;		// PostFX textures

		// Water
		ShaderVector*			_waterUVVariable;		// Water tex coord scale factor
		ShaderResource*			_waveFieldVariable;		// Wave height texture
		ShaderResource*			_waterNormalVariable;	// Normal map for the water
		ShaderMatrix*			_mirrorMatrixVariable;	// Mirror matrix for reflection rendering
		ShaderResource*			_skyCubeVariable;		// Cube map for sky environment mapping

		// Material Properties
		ShaderResource*			_materialBufferVariable;		// Global material buffer
		ShaderResource*			_materialTextureVariable;		// Material texture array
		ShaderScalar*			_materialTextureFlagVariable;	// Array of booleans for material texture array flags
		ShaderResource*			_probeVariable;					// Env map


		// Transforms and camera variables
		ShaderMatrix*         _worldMatrixVariable;					// World matrix
		ShaderMatrix*         _viewProjectionVariable;				// View/Projection matrix
		ShaderMatrix*         _previousViewProjectionVariable;		// View/Projection matrix from the previous frame
		ShaderMatrix*         _inverseViewProjectionVariable;		// View/Projection matrix from the previous frame
		ShaderVector*		  _cameraPositionVariable;				// Camera position
		ShaderVector*		  _cameraDirectionVariable;				// Camera direction
		ShaderVector*		  _screenSizeVariable;					// Screen size (xy = size, yz = 1.0/size)
		ShaderMatrix*		  _projMatrixVariable;					// Projection matrix
		ShaderMatrix*		  _viewMatrixVariable;					// Projection matrix
		ShaderVector*		  _projRatioVariable;					// For getting linear depth from z buffer
		ShaderVector*		  _screenToWorldXVariable;				// Screen-World space basis X vector
		ShaderVector*		  _screenToWorldYVariable;				// Screen-World space basis Y vector
		ShaderVector*		  _screenToWorldZVariable;				// Screen-World space basis Z vector
		ShaderScalar*		  _fovVariable;							// Camera FoV

		// Material blending
		EffectPass*				_blendMaterialPass;				// Pass for blending 2 textures
		EffectPass*				_bumpToNormalPass;				// Pass for blending 2 textures
		EffectPass*				_heightInNormalPass;			// Pass for blending 2 textures
		ShaderResource*			_blendTextureBaseVariable;		// The base texture for the blending
		ShaderResource*			_blendTextureLayerVariable;		// The layer to blend into the base
		ShaderResource*			_blendTextureMaskVariable;		// The mask to control the blending

		// Render states
		RasterizerState*		_defaultRS;						// Default RS
		RasterizerState*		_frontCullRS;					// Render backfaces
		RasterizerState*		_wireframeRS;					// Wireframe mode

	private:
		// No copies
		Renderer( const Renderer& );
		Renderer& operator=(const Renderer&);


		// Event callbacks

		// Material update
		void OnMaterialChanged( void* userData );
	};


}

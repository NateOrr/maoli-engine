//--------------------------------------------------------------------------------------
// File: SystemEvents.h
//
// Reserved engine events
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace EngineEvent
{
	enum EVENTS
	{
		MaterialChanged=-10000,
		MouseWheelMoved,
		MouseMoved,
		MouseButtonClicked,
		MouseButtonDown,
		MouseButtonUp,
		KeyPress,
		KeyDown,
		KeyUp,
		WorldLoaded,
	};
}
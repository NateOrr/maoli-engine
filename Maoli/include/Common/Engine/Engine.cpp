//--------------------------------------------------------------------------------------
// File: Engine.h
//
// Game engine
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Engine\Engine.h"
#include "Platform/Windows.h"
#include "Graphics/Renderer.h"

namespace Maoli
{
	// Ctor
	Engine::Engine()
	{
		_platform = nullptr;
		_eventManager = nullptr;
		_audio = nullptr;
		_graphics = nullptr;
		_physics = nullptr;
		_animation = nullptr;
		_scriptManager = nullptr;
		_state = EngineState::Default;
		_fps = 60;
		_frameCounter = 0;
		_fpsTimer = 0;
		_timeScale = 1.0f;
		_windowed = true;
		_coreSystems.Allocate( (int32)SystemType::User );
		for ( uint32 i = 0; i < _coreSystems.Size(); ++i )
			_coreSystems[i] = nullptr;
		BuildComponentTable();
	}


	// Dtor
	Engine::~Engine()
	{
		// Cleanup all entities
		for ( uint32 i = 0; i < _entities.Size(); ++i )
			DeleteEntity( _entities[i] );
		RemovePendingEntities();
		DeletePendingEntities();
		_entities.Release();
		_entitiesToRemove.Release();
		_entitiesToDelete.Release();
		_entityMap.clear();
		_entityNameCount.clear();

		// Free all user systems
		for ( uint32 i = 0; i < _userSystems.Size(); ++i )
		{
			_userSystems[i]->Release();
			delete _userSystems[i];
		}
		_userSystems.Release();

		// Free all systems
		for ( uint32 i = 0; i < _coreSystems.Size(); ++i )
		{
			_coreSystems[i]->Release();
			delete _coreSystems[i];
		}
		_coreSystems.Release();

		// Free the thread pool
		SafeReleaseDelete( _threadPool );

		// Free the platform
		SafeReleaseDelete( _platform );

		// Events
		SafeDelete( _eventManager );

		// Shutdown havok and geometry systems
		Havok::Release();
		Geometry::Shutdown();
		Material::Shutdown();
	}

	// Setup using an existing window
	bool Engine::Init( WindowHandle mainWindow )
	{
		return Init( mainWindow, mainWindow );
	}

	// Setup using an existing window
	bool Engine::Init( WindowHandle mainWindow, WindowHandle renderWindow )
	{
		// Setup the platform
		_platform = new WindowsPlatform( this );
		_platform->SetWindow( mainWindow, renderWindow );
		return Init();
	}


	// Setup
	bool Engine::Init( int32 width, int32 height, bool windowed )
	{
		// Setup the platform
		_platform = new WindowsPlatform( this );
		_platform->InitWindow( width, height );
		_windowed = windowed;
		return Init();
	}

	// Setup helper
	bool Engine::Init()
	{
		// Threading
		_threadPool = new ThreadPool;
		_threadPool->Init( _platform->GetProcessorCount() - 1 );

		// Events
		_eventManager = new EventManager;

		// Start the timer
		_isRunning = true;
		_timer.Reset();

		return true;
	}

	// Per frame update
	void Engine::Update()
	{
		// OS update
		// Use false if windows messages are handled externally
		if ( _platform->Update( true ) )
		{
			// Game Update
			//if ( _platform->IsActiveWindow() )
			{
				// Remove entities
				RemovePendingEntities();

				// Delete entities
				DeletePendingEntities();

				// Compute the time for the last frame
				_frameTime = std::max( _timer.GetElapsedSeconds(), 0.00001f );
				_timer.Reset();

				// Compute fps
				_fpsTimer += _frameTime;
				++_frameCounter;
				if ( _fpsTimer > 1.0f )
				{
					_fps = _frameCounter;
					_fpsTimer = 0;
					_frameCounter = 0;
				}

				// Apply time scale for slow mode / fast mode
				_frameTime *= _timeScale;

				switch ( _state )
				{
					case EngineState::Default:
					{
						// Update all user systems
						for ( uint32 i = 0; i < _userSystems.Size(); ++i )
						{
							_userSystems[i]->UpdateComponents( _frameTime );
							_userSystems[i]->Update( _frameTime );
						}

						// Core systems
						for ( uint32 i = 0; i < _coreSystems.Size(); ++i )
						{
							_coreSystems[i]->UpdateComponents( _frameTime );
							_coreSystems[i]->Update( _frameTime );
						}

						break;
					}

					case EngineState::Editor:
					{
						_physics->UpdateTransforms( _frameTime );
						_physics->RenderDebugInfo();
						_animation->UpdateComponents( _frameTime );
						_animation->Update( _frameTime );
						_graphics->UpdateComponents( _frameTime );
						_graphics->Update( _frameTime );
						break;
					}

					case EngineState::Paused:
					{
						_audio->UpdateComponents( _frameTime );
						_audio->Update( _frameTime );
						_graphics->Update( _frameTime );
						break;
					}
				}
			}
		}
		else
		{
			_isRunning = false;
		}
	}


	// Rendering
	void Engine::Render()
	{
		if ( _graphics )
			_graphics->Render();
	}

	// Create an empty entity
	Entity* Engine::CreateEntity( const char* name )
	{
		// Names cannot contain spaces
		if ( String( name ).Contains( ' ' ) )
		{
			_platform->MessageBox( "Entities cannot have spaces in their name" );
			return nullptr;
		}

		// Validate name
		auto iter = _entityMap.find( name );
		if ( iter == _entityMap.end() )
		{
			auto entity = CreateEntityTemplate( name );
			_entityMap.emplace( name, entity );
			_entityNameCount.emplace( name, 1 );
			std::cout << "Creating Entity " << name << std::endl;
			return entity;
		}
		else
		{
			String entityName = name;
			int32 count = ++_entityNameCount[name];
			return CreateEntity( entityName + count );
		}

		_platform->MessageBox( "Entities cannot have duplicate names" );
		return nullptr;
	}

	// Create an empty entity
	Entity* Engine::CreateEntityTemplate( const char* name )
	{
		return new Entity( this, name );
	}

	// Delete an entity
	void Engine::DeleteEntity( Entity* entity )
	{
		if ( !entity || entity->_flaggedForDelete )
			return;

		RemoveEntity( entity );
		_entitiesToDelete.Add( entity );
		entity->_flaggedForDelete = true;
	}

	// Rename an entity
	bool Engine::RenameEntity( Entity* entity, const char* newName )
	{
		// Names cannot contain spaces
		if ( String( newName ).Contains( ' ' ) )
		{
			_platform->MessageBox( "Entities cannot have spaces in their name" );
			return false;
		}

		// Don't allow duplicate names
		auto iter = _entityMap.find( newName );
		if ( iter != _entityMap.end() )
		{
			_platform->MessageBox( "That name is already taken" );
			return false;
		}

		// Rename
		iter = _entityMap.find( entity->GetName() );
		if ( iter != _entityMap.end() )
		{
			_entityMap.erase( iter );
			_entityNameCount.erase( _entityNameCount.find( entity->GetName() ) );
		}
		_entityMap.emplace( newName, entity );
		_entityNameCount.emplace( newName, 1 );
		entity->_name = newName;

		return true;
	}

	// Add an entity to the game world
	void Engine::AddEntity( Entity* e )
	{
		if ( !e->_active )
		{
			_entities.Add( e );
			e->_active = true;
			for ( auto iter = e->_components.begin(); iter != e->_components.end(); ++iter )
				RegisterComponent( iter->second );
		}

		LinkComponents();
	}

	// Remove an entity
	void Engine::RemoveEntity( Entity* e )
	{
		if ( !e || e->_flaggedForRemove )
			return;
		_entitiesToRemove.Add( e );
		e->_flaggedForRemove = true;
	}

	// Register a component with all systems
	void Engine::RegisterComponent( Component* component )
	{
		// Register components with each system
		bool registered = false;
		for ( uint32 i = 0; i < _coreSystems.Size(); ++i )
		{
			if ( _coreSystems[i]->RegisterComponent( component ) )
			{
				component->SetSystem( _coreSystems[i] );
				registered = true;
				break;
			}
		}

		// User systems
		if ( !registered )
		{
			for ( uint32 i = 0; i < _userSystems.Size(); ++i )
			{
				if ( _userSystems[i]->RegisterComponent( component ) )
				{
					component->SetSystem( _userSystems[i] );
					break;
				}
			}
		}

		// Register component dependencies
		for ( uint32 i = 0; i < component->_links.Size(); ++i )
			_dependencies.Add( component->_links[i] );
	}

	// Register all component creation methods
	void Engine::BuildComponentTable()
	{
		ComponentEvent::RegisterEngineEvents();

		Component::RegisterType<Light>();
		Component::RegisterType<Transform>();
		Component::RegisterType<RigidBody>();
		Component::RegisterType<CharacterController>();
		Component::RegisterType<Phantom>();
		Component::RegisterType<Model>();
		Component::RegisterType<AnimationController>();
		Component::RegisterType<Camera>();
		Component::RegisterType<NavMesh>();
		Component::RegisterType<ParticleEmitter>();
	}

	// Load an entity from file
	Entity* Engine::LoadEntity( const char* file )
	{
		std::ifstream fin( file, std::ofstream::binary );
		if ( fin.is_open() )
		{
			Entity* entity = CreateEntity( String( file ).GetFile().RemoveFileExtension() );
			if ( !entity )
				return nullptr;
			LoadEntity( entity, fin );

			fin.close();

			return entity;
		}
		return nullptr;
	}

	// Load an entity from an open file
	void Engine::LoadEntity( Entity* entity, std::ifstream& fin )
	{
		// Template name
		entity->_templateName.Deserialize( fin );

		String typeName;
		uint32 count;
		Maoli::Deserialize( fin, &count );
		for ( uint32 i = 0; i < count; ++i )
		{
			// Component data
			typeName.Deserialize( fin );
			Component* component = Component::_creationMethods[typeName]( this );
			component->SetOwner( entity );
			component->Import( fin );
			entity->AddComponent( component );

			// Global dependency data
			component->ImportDependencies( fin );
		}
	}

	// Load an entity from file
	Entity* Engine::LoadEntityTemplate( const char* file )
	{
		std::ifstream fin( file, std::ofstream::binary );
		{
			Entity* entity = CreateEntityTemplate( String( file ).GetFile().RemoveFileExtension() );
			if ( !entity )
				return nullptr;
			LoadEntity( entity, fin );

			fin.close();

			return entity;
		}
		return nullptr;
	}

	// Save an entity to file
	void Engine::SaveEntity( Entity* entity, const char* file )
	{
		std::ofstream fout( file, std::ofstream::binary );
		SaveEntity( entity, fout );
		fout.close();
	}

	// Save an entity to file
	void Engine::SaveEntity( Entity* entity, std::ofstream& fout )
	{
		// Template name
		entity->GetTemplateName().Serialize( fout );

		// Export the components
		uint32 count = entity->_components.size();
		Maoli::Serialize( fout, &count );
		for ( auto iter = entity->_components.begin(); iter != entity->_components.end(); ++iter )
		{
			// Component data
			iter->second->GetTypeName().Serialize( fout );
			iter->second->Export( fout );

			// Gloabl dependency data
			iter->second->ExportDependencies( fout );
		}
	}

	// Create a dependency link
	void Engine::CreateComponentDependency( Component* component, Component* target )
	{
		component->AddDependency( target );
	}

	// Break a dependency link
	void Engine::BreakComponentDependency( Component* component, Component* target )
	{
		component->RemoveDependency( target );
	}

	// Access an entity by name
	Entity* Engine::GetEntity( const char* name )
	{
		for ( uint32 i = 0; i < _entities.Size(); ++i )
			if ( _entities[i]->GetName() == name )
				return _entities[i];
		return nullptr;
	}

	// Link component dependencies
	void Engine::LinkComponents()
	{
		for ( auto iter = _dependencies.Begin(); !iter.End(); )
		{
			if ( iter->Link() )
				iter = _dependencies.Remove( iter );
			else
				++iter;
		}
	}

	// Clear all entities from the world and reset it
	void Engine::ClearWorld()
	{
		for ( uint32 i = 0; i < _entities.Size(); ++i )
			RemoveEntity( _entities[i] );
	}

	// Deletes all entities from the world and reset it
	void Engine::DeleteWorld()
	{
		for ( uint32 i = 0; i < _entities.Size(); ++i )
			DeleteEntity( _entities[i] );
	}

	// Set the current game world
	void Engine::SetWorld( World* world )
	{
		ClearWorld();

		if ( world )
		{
			for ( uint32 i = 0; i < world->GetEntities().Size(); ++i )
				AddEntity( world->GetEntities()[i] );
		}
	}

	// Remove entities waiting to be removed
	void Engine::RemovePendingEntities()
	{
		for ( uint32 i = 0; i < _entitiesToRemove.Size(); ++i )
		{
			auto e = _entitiesToRemove[i];
			if ( e->_active )
			{
				_entities.Remove( e );
				e->_active = false;
				e->_flaggedForRemove = false;
				for ( auto iter = e->_components.begin(); iter != e->_components.end(); ++iter )
				{
					// Unregister from the systems
					if ( iter->second->GetSystem() )
						iter->second->GetSystem()->UnregisterComponent( iter->second );

					// Remove dependency link data
					for ( auto depIter = _dependencies.Begin(); !depIter.End(); )
					{
						if ( depIter->component == iter->second )
							depIter = _dependencies.Remove( depIter );
						else
							++depIter;
					}
				}
			}
		}
		_entitiesToRemove.Clear();
	}

	// Delete entities waiting to be removed
	void Engine::DeletePendingEntities()
	{
		for ( uint32 i = 0; i < _entitiesToDelete.Size(); ++i )
		{
			auto entity = _entitiesToDelete[i];
			auto iter = _entityMap.find( entity->GetName() );
			if ( iter != _entityMap.end() )
			{
				_entityMap.erase( iter );
				_entityNameCount.erase( _entityNameCount.find( entity->GetName() ) );
			}
			std::cout << "Deleting Entity " << entity->GetName().c_str() << std::endl;
			delete entity;
		}
		_entitiesToDelete.Clear();
	}

	// Unregister from an event
	void Engine::UnregisterEventCallback( const Event& event, void* caller )
	{
		_eventManager->Unregister( event, caller );
	}

	// Broadcast an event
	void Engine::BroadcastEvent( const Event& event, void* userData /*= nullptr */ )
	{
		_eventManager->BroadcastEvent( event, userData );
	}

	// Add a new system
	bool Engine::RegisterSystem( SystemType type, System* system )
	{
		// Register custom component types
		if ( system )
			system->RegisterComponentTypes();

		// Reset the timer
		_timer.Reset();

		// Add to the engine
		switch ( type )
		{
			// User created system type
			case Maoli::SystemType::User:
				if ( system )
				{
					if ( system->Init() )
					{
						_userSystems.Add( system );
						_systemMap.emplace( system->GetTypeID(), system );
					}
					else
					{
						delete system;
						return false;
					}
				}
				return true;

			case Maoli::SystemType::Audio:
				_audio = system ? (Audio*)system : new Audio( this );
				_coreSystems[(int32)type] = _audio;
				break;

			case Maoli::SystemType::Animation:
				_animation = system ? (AnimationManager*)system : new AnimationManager( this );
				_coreSystems[(int32)type] = _animation;
				break;

			case Maoli::SystemType::Physics:
				_physics = system ? (Physics*)system : new Physics( this );
				_coreSystems[(int32)type] = _physics;
				break;

			case Maoli::SystemType::Graphics:
				_graphics = system ? (Renderer*)system : new Renderer( this, _platform->GetRenderWindowHandle(), _windowed );
				_coreSystems[(int32)type] = _graphics;
				break;

			case Maoli::SystemType::Scripting:
				_scriptManager = system ? (ScriptManager*)system : new ScriptManager( this );
				_coreSystems[(int32)type] = _scriptManager;
				break;
		}

		// Setup new systems
		if ( !_coreSystems[(int32)type]->Init() )
		{
			DeleteSystem( _coreSystems[(int32)type] );
			return false;
		}

		_systemMap.emplace( _coreSystems[(int32)type]->GetTypeID(), _coreSystems[(int32)type] );

		return true;
	}

	// Remove a system from the engine
	void Engine::DeleteSystem( System* system )
	{
		UnregisterSystem( system );
		delete system;
	}

	// Remove a system from the engine
	void Engine::UnregisterSystem( System* system )
	{
		if ( _coreSystems.Remove( system ) )
		{
			if ( _audio == system )
				_audio = nullptr;
			else if ( _animation == system )
				_animation = nullptr;
			else if ( _physics == system )
				_physics = nullptr;
			else if ( _graphics == system )
				_graphics = nullptr;
			else if ( _scriptManager == system )
				_scriptManager = nullptr;
		}
		else
			_userSystems.Remove( system );

		system->Release();
	
		// Remove from the type map
		auto iter = _systemMap.find( system->GetTypeID() );
		if ( iter != _systemMap.end() )
			_systemMap.erase( iter );
	}

}

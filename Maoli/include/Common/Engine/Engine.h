//--------------------------------------------------------------------------------------
// File: Engine.h
//
// Game engine
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Platform/Platform.h"
#include "Util/EventManager.h"
#include "Engine\SystemEvents.h"

// Forward decl
class GameStateManager;

namespace Maoli
{
	// Forward decl
	class Renderer;
	class Audio;
	class MessageHandler;
	class ThreadPool;
	class Entity;
	class Physics;
	class AnimationManager;
	class World;
	class ScriptManager;
	class Platform;
	typedef Entity EntityDesc;

	// Engine system types
	enum class SystemType
	{
		Scripting,
		Audio,
		Animation,
		Physics,
		Graphics,
		User,
	};

	// Engine states
	enum class EngineState
	{
		Default,
		Editor,
		Paused,
	};

	class Engine
	{
	public:

		// Ctor
		Engine();

		// Dtor
		~Engine();

		// Get the platform
		inline Platform* GetPlatform() { return _platform; }

		// Get the task manager
		inline ThreadPool* GetTaskManager() { return _threadPool; }

		// Get the primary renderer
		inline Renderer* GetGraphics() { return _graphics; }

		// Get the physics system
		inline Physics* GetPhysics() { return _physics; }

		// Get the animation manager
		inline AnimationManager* GetAnimationManager() { return _animation; }

		// Get the audio system
		inline Audio* GetAudio() { return _audio; }

		// Get the scripting manager
		inline ScriptManager* GetScriptManager() { return _scriptManager; }

		// Get the frame time
		inline float GetFrameTime() const { return _frameTime; }

		// Get the frames per second
		inline int32 GetFPS() const { return _fps; }

		// Setup
		bool Init( int32 width, int32 height, bool windowed );

		// Setup using an existing window
		bool Init( WindowHandle mainWindow );
		bool Init( WindowHandle mainWindow, WindowHandle renderWindow );

		// Per frame update
		void Update();

		// Rendering
		void Render();

		// Add a new system
		bool RegisterSystem( SystemType type, System* system = nullptr );

		// Remove a system from the engine
		void UnregisterSystem( System* system );

		// Remove a system from the engine and delete it
		void DeleteSystem( System* system );

		//////////////////////////////////////////////
		// Game system

		// Create an empty entity
		Entity* CreateEntity( const char* name );
		Entity* CreateEntityTemplate( const char* name );

		// Delete an entity
		void DeleteEntity( Entity* entity );

		// Rename an entity
		bool RenameEntity( Entity* entity, const char* newName );

		// Load an entity from file
		Entity* LoadEntity( const char* file );
		void LoadEntity( Entity* entity, std::ifstream& fin );
		Entity* LoadEntityTemplate( const char* file );

		// Save an entity to file
		void SaveEntity( Entity* entity, const char* file );
		void SaveEntity( Entity* entity, std::ofstream& fout );

		// Add an entity to the game world
		void AddEntity( Entity* e );

		// Remove an entity
		void RemoveEntity( Entity* e );

		// Get the number of entities in the world
		inline uint32 GetNumEntities() const { return _entities.Size(); }

		// Access an entity
		inline Entity* GetEntity( uint32 i ) { return _entities[i]; }

		// Access an entity by name
		Entity* GetEntity( const char* name );

		// Get all components of a given type
		template< typename T>
		bool GetComponents( Array<T*>& components )
		{
			components.Clear();
			for ( uint32 i = 0; i < _entities.Size(); ++i )
			{
				auto component = _entities[i]->GetComponent<T>();
				if ( component )
					components.Add( component );
			}
			return !components.IsEmpty();
		}

		// Register a component with all systems
		void RegisterComponent( Component* component );

		// Create a dependency link
		void CreateComponentDependency( Component* component, Component* target );

		// Break a dependency link
		void BreakComponentDependency( Component* component, Component* target );

		// Set the current game world
		void SetWorld( World* world );

		// Clear all entities from the world and reset it
		void ClearWorld();

		// Deletes all entities from the world and reset it
		void DeleteWorld();

		// Set the engine state
		inline void SetState( EngineState value ) { _state = value; }

		// Get the engine state
		inline EngineState GetState() { return _state; };

		// Set time scale for slow or fast motion
		inline void SetTimeScale( float scale ) { _timeScale = scale; }

		// Register for an event
		template <typename T>
		void RegisterEventCallback( const Event& event, T* caller, void (T::*fn)(void*) )
		{
			_eventManager->Register( event, caller, fn );
		}

		// Unregister from an event
		void UnregisterEventCallback( const Event& event, void* caller );

		// Broadcast an event
		void BroadcastEvent( const Event& event, void* userData = nullptr );

		// Remove entities waiting to be removed
		void RemovePendingEntities();

		// Delete entities waiting to be removed
		void DeletePendingEntities();

		// Get a system by class type
		template <typename T>
		T* GetSystem()
		{
			auto iter = _systemMap.find( typeid(T) );
			return (iter != _systemMap.end()) ? (T*)iter->second : nullptr;
		}

	private:

		// Init helper
		bool Init();

		// Register all component creation methods
		void BuildComponentTable();

		// Link component dependencies
		void LinkComponents();

		// Managers
		Platform*			_platform;
		ThreadPool*			_threadPool;
		EventManager*		_eventManager;

		/////////////////////////////
		// Systems
		Audio*				_audio;
		Renderer*			_graphics;
		Physics*			_physics;
		ScriptManager*		_scriptManager;
		AnimationManager*	_animation;
		Array<System*>		_coreSystems;
		Array<System*>		_userSystems;

		// System map for easier access
		std::unordered_map<std::type_index, System*> _systemMap;

		// Properties
		bool			_isRunning;		// True while the app is running
		float			_frameTime;		// Time in seconds for the last frame
		Timer			_timer;			// Frame timing
		int32				_fps;			// Frames per second
		int32				_frameCounter;	// Frame tracking for fps
		float			_fpsTimer;		// Used to compute fps
		float			_timeScale;		// Multiplier for frame time
		EngineState		_state;			// Engine state
		bool			_windowed;		// True if the engine is in windowed mode

		// Game system
		Array<Entity*>							_entities;			// All active game entities
		std::map<const String, Entity*>			_entityMap;			// Maps names to the entities and ensures no duplicates
		std::map<const String, int32>				_entityNameCount;	// Maps names to the entities and ensures no duplicates
		List<Component::ComponentDependency*>	_dependencies;		// Global componenet dependency links
		Array<Entity*>							_entitiesToRemove;	// Entities that require delayed removal
		Array<Entity*>							_entitiesToDelete;	// Entities that require delayed deletion
	};

}

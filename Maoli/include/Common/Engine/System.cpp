//--------------------------------------------------------------------------------------
// File: System.cpp
//
// System base class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Engine\System.h"

namespace Maoli
{

	// Ctor
	System::System( Engine* parent ) : _engine(parent)
	{
		_messageHandler = new MessageHandler;
	}

	// Send a message
	void System::SendMessage( MessageType id, void* param, int32 flags )
	{
		_messageHandler->SendMessage(id, param, flags);
	}

	// Dtor
	System::~System()
	{
		delete _messageHandler;
	}

	// Update all components
	void System::UpdateComponents(float dt)
	{
		for(uint32 i=0; i<_components.Size(); ++i)
			_components[i]->Update(dt);
	}

}

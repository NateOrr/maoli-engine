//--------------------------------------------------------------------------------------
// File: System.h
//
// System base class
//
// Systems provide the functionality of the engine.
// The operate off of a main task, which can be composed of many independent subtasks.
// The main task is sent to the Scheduler, which distributes all of the
// Subtasks to the TaskManager.
// Each system also has a method that executes prior to scheduling, 
// which allows the System to organize the subtasks.
// A final method will execute after the TaskManager has executed
// all tasks.  This provides the chance for re-synchronization and 
// to execute code that cannot be parallelized.
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

// Undefine annoying windows macro names
#undef SendMessage

namespace Maoli
{

	// Forward decl
	class Engine;
	class Component;
	class MessageHandler;
	enum class MessageType;

	class System
	{
	public:
		// Ctor
		System(Engine* parent);

		// Setup
		virtual bool Init() = 0;

		// Process messages
		virtual void ProcessMessages() {}

		// Register a component to the system
		virtual bool RegisterComponent(Component* component) = 0;

		// Remove a component (assumes the component exists already in the system!)
		virtual void UnregisterComponent(Component* component) = 0;

		// Register component types with the engine
		virtual void RegisterComponentTypes() {}

		// Update the system internal state
		virtual void Update(float dt) = 0;

		// Cleanup
		virtual void Release() = 0;

		// Dtor
		virtual ~System();

		// Get the system typeid
		inline std::type_index GetTypeID() const { return typeid(*this); }

		// Get the parent framework
		inline Engine* GetEngine() { return _engine; }

		// Send a message
		void SendMessage(MessageType id, void* param=nullptr, int32 flags=0);
		
		// Update all components
		void UpdateComponents(float dt);


	protected:

		Engine*				_engine;
		MessageHandler*		_messageHandler;
		Array<Component*>	_components;

	private:
	
		// Prevent copying
		System(const System&);
		System& operator=(const System&);
	};

}

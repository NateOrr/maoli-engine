//--------------------------------------------------------------------------------------
// File: Audio.h
//
// FMOD Audio
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Engine\System.h"

// Forward decl
namespace FMOD
{
	class Sound;
	class System;
	class Channel;
	class DSP;
}

namespace Maoli
{
	// Forward decl
	class Engine;
	class Vector3;
	class Transform;

	// Sound object
	class Sound
	{
		friend class Audio;
	public:

		// Dtor
		~Sound();

		// Play the sound
		void Play();

		// Stop the sound
		void Stop();

		// True if playing
		bool IsPlaying();

		// Volume
		inline void SetVolume( float f ) { _volume = f; }
		inline float GetVolume() const { return _volume; }

		// Frequency
		void AddToFrequency( int32 f, bool pitch = true );
		void SetFrequency( float f, bool pitch = true );
		float GetFrequency();
		inline float GetDefaultFrequency() const { return _defaultFreq; };

		// 3D sound source
		inline Transform* GetSource() const { return _source; }
		void SetSource( Transform* val );

		// Set the min and max distance for the sound range
		void SetRange(float minDistance, float maxDistance);

		// Update this sound
		void Update(float dt);

		// Check if this sound was loaded with 3d support
		inline bool Is3D() const { return _is3D; }

		// Get the file path
		inline const String& GetFilePath() const { return _fileName; }

		// Clone a new instance of this sound
		Sound* Clone();

	private:

		// Ctor
		Sound();

		// Add a reference
		inline void AddRef(){ ++_refCount; }

		// Remove a reference
		inline void RemoveRef(){ ++_refCount; }

		// Get the current ref count
		inline int32 GetRefCount(){ return _refCount; }

		FMOD::Sound*		_sound;
		FMOD::System*		_fmod;
		FMOD::Channel*		_channel;
		FMOD::DSP*			_pitch;
		float				_volume;
		float				_defaultFreq;
		float				_minDistance;
		float				_maxDistance;
		Audio*				_parent;
		Transform*			_source;
		int32					_refCount;
		bool				_is3D;
		bool				_isLooping;
		String				_fileName;
	};

	// Audio manager
	class Audio : public System
	{
		friend class Sound;
	public:

		// Ctor
		Audio(Engine* engine);

		// Dtor
		~Audio();

		/////////////////////////////////////
		// System interface

		// Setup the d3d device
		virtual bool Init();

		// Register a component to the system
		virtual bool RegisterComponent(Component* component);

		// Remove a component (assumes the component exists already in the system!)
		virtual void UnregisterComponent(Component* component);

		// Per-frame update
		virtual void Update(float dt);

		// Cleanup
		virtual void Release();


		// Create a sound
		Sound* LoadSound( const char* file, bool isLooping, bool is3D );

		// Set Volume
		void SetVolume( float f );

		// Get volume
		float GetVolume(){ return _volume; }

		// Is it muted?
		bool IsMuted(){ return _isMuted; }

		// Toggle mute
		void Mute();

		// Stops all currently playing sounds
		void StopSounds();

		// (Un)Pauses all currently playing sounds
		void PauseSounds( bool pause );

		// Slows all sounds neccisary for the critical state of the player
		void CriticalSlow( bool slow );

		// 3D sound receiver
		inline Transform* GetReceiver() const { return _receiver; }
		void SetReceiver( Transform* val );

	private:

		// Release a sound object
		void ReleaseSound(Sound* sound);

		FMOD::System*			_fmod;
		FMOD::DSP*				_pitch;
		float					_volume;
		List<Sound*>			_sounds;		// only Currently Playing sounds
		bool					_isMuted;
		Transform*				_receiver;

		// Sound reference tracking
		struct SoundRef
		{
			FMOD::Sound* sound;
			int32 refCount;
		};

		// Resource management
		std::map<const String, SoundRef> _loaded2DSounds;
		std::map<const String, SoundRef> _loaded3DSounds;

		// Prevent copying
		Audio(const Audio&);
		Audio& operator=(const Audio&);
	};

}

//--------------------------------------------------------------------------------------
// File: Audio.cpp
//
// FMOD Audio
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Maoli.h"
#include "Audio.h"
#include <fmod.hpp>
#include "Game/Component.h"
#include "Engine\Engine.h"
#include "Game/Entity.h"

namespace Maoli
{

#pragma region Sound

	// Sound Ctor
	Sound::Sound() : _sound( nullptr ), _fmod( nullptr ), _parent( nullptr ), _pitch( nullptr )
	{
		_source = nullptr;
		_refCount = 0;
		_channel = nullptr;
		_minDistance = 20.0f;
		_maxDistance = 100.0f;
		_is3D = true;
		_isLooping = false;
	}

	// Dtor
	Sound::~Sound()
	{
		_parent->ReleaseSound( this );
	}

	// Play a sound
	void Sound::Play()
	{
		Stop();

		_fmod->playSound( FMOD_CHANNEL_FREE, _sound, false, &_channel );

		_channel->setVolume( _volume * _parent->GetVolume() );
		_channel->setMute( _parent->IsMuted() );

		if ( _is3D )
			_channel->set3DMinMaxDistance( _minDistance, _maxDistance );

		if ( _source )
			_channel->set3DAttributes( (FMOD_VECTOR*)&_source->GetPosition(), 0 );

		_parent->_sounds.Add( this );
	}

	// Stop the sound
	void Sound::Stop()
	{
		if ( _channel )
		{
			bool active;
			if ( _pitch->getActive( &active ) && active )				
				_pitch->setActive( false );
			_channel->stop();
			_channel = nullptr;
			_parent->_sounds.Remove( this );
		}
	}


	// True if playing
	bool Sound::IsPlaying()
	{
		return (_channel != nullptr);
	}


	// 3D sound source
	void Sound::SetSource( Transform* val )
	{
		_source = val;
	}

	// Set the min and max distance for the sound range
	void Sound::SetRange( float minDistance, float maxDistance )
	{
		_minDistance = minDistance;
		_maxDistance = maxDistance;
		if ( _channel )
			_channel->set3DMinMaxDistance( _minDistance, _maxDistance );
	}

	// Add To Frequency
	void Sound::AddToFrequency( int32 f, bool pitch )
	{
		if ( pitch )
		{
			_pitch->remove();
			_channel->setFrequency( _defaultFreq + f * 4096 );
			_pitch->setParameter( FMOD_DSP_PITCHSHIFT_PITCH, _defaultFreq / ( _defaultFreq + f * 4096 ) );
			_fmod->addDSP( _pitch, 0 );
		}
		else 
		{
			_pitch->remove();
			_channel->setFrequency( _defaultFreq + f * 4096 );
			_pitch->setParameter( FMOD_DSP_PITCHSHIFT_PITCH, _defaultFreq );
			_fmod->addDSP( _pitch, 0 );
		}
	}
	// Set Frequency
	void Sound::SetFrequency( float f, bool pitch )
	{
		if ( pitch )
		{
			_pitch->remove();
			_channel->setFrequency( f );
			_pitch->setParameter( FMOD_DSP_PITCHSHIFT_PITCH, _defaultFreq / f );
			_fmod->addDSP( _pitch, 0 );
		}
		else
		{
			_pitch->remove();
			_channel->setFrequency( f );
			_pitch->setParameter( FMOD_DSP_PITCHSHIFT_PITCH, _defaultFreq );
			_fmod->addDSP( _pitch, 0 );
		}
	}

	// Get frequency
	float Sound::GetFrequency()
	{
		if ( _channel )
		{
			float freq;
			_channel->getFrequency( &freq );
			return freq;
		}
		else
			return -1;
	}

	// Update this sound
	void Sound::Update( float dt )
	{
		if ( _source )
			_channel->set3DAttributes( (FMOD_VECTOR*)&_source->GetPosition(), 0 );
	}

	// Clone a new instance of this sound
	Sound* Sound::Clone()
	{
		Sound* clone = _parent->LoadSound( _fileName, _isLooping, _is3D );
		clone->SetVolume( _volume );
		return clone;
	}



#pragma endregion


#pragma region Audio

	// Ctor
	Audio::Audio( Engine* parent ) : System( parent )
	{
		_fmod = nullptr;
		_volume = 1.0f;
		_isMuted = false;
		_receiver = nullptr;
	}


	// Dtor
	Audio::~Audio()
	{
		Release();
	}


	// Setup
	bool Audio::Init()
	{
		FMOD_RESULT result;
		result = FMOD::System_Create( &_fmod );		// Create the main system object.
		if ( result != FMOD_OK )
		{
			return false;
		}

		result = _fmod->init( 100, FMOD_INIT_NORMAL, 0 );	// Initialize FMOD.
		if ( result != FMOD_OK )
		{
			return false;
		}

		result = _fmod->createDSPByType( FMOD_DSP_TYPE_PITCHSHIFT, &_pitch );
		if ( result != FMOD_OK )
		{
			return false;
		}

		return true;
	}

	// Set Volume
	void Audio::SetVolume( float f )
	{
		_volume = f;
		for ( auto i = _sounds.Begin(); !i.End(); ++i )
		{
			if ( (*i)->IsPlaying() )
			{
				(*i)->_channel->setVolume( _volume * (*i)->_volume );
			}
		}
	}

	// 3D sound target
	void Audio::SetReceiver( Transform* val )
	{
		_receiver = val;

		if ( _receiver )
		{
			FMOD_VECTOR* listenerpos = (FMOD_VECTOR*)&_receiver->GetPosition();
			FMOD_VECTOR* listenerForward = (FMOD_VECTOR*)&_receiver->GetForwardVector();
			FMOD_VECTOR* listenerUp = (FMOD_VECTOR*)&_receiver->GetUpVector();
			_fmod->set3DListenerAttributes( 0, listenerpos, 0, listenerForward, listenerUp );
		}
	}

	// Toggle mute
	void Audio::Mute()
	{
		_isMuted = !_isMuted;
		for ( auto i = _sounds.Begin(); !i.End(); ++i )
		{
			if ( (*i)->IsPlaying() )
			{
				(*i)->_channel->setMute( _isMuted );
			}
		}
	}


	// Per-frame update
	void Audio::Update( float dt )
	{
		if ( _fmod )
		{
			// Update the receiver location
			if ( _receiver )
			{
				FMOD_VECTOR* listenerpos = (FMOD_VECTOR*)&_receiver->GetPosition();
				FMOD_VECTOR* listenerForward = (FMOD_VECTOR*)&_receiver->GetForwardVector();
				FMOD_VECTOR* listenerUp = (FMOD_VECTOR*)&_receiver->GetUpVector();
				_fmod->set3DListenerAttributes( 0, listenerpos, 0, listenerForward, listenerUp );
			}

			// Update each sound
			for ( auto iter = _sounds.Begin(); !iter.End(); )
			{
				Sound* sound = *iter;
				bool isPlaying = false;
				if ( sound->_channel )
					sound->_channel->isPlaying( &isPlaying );
				if ( !isPlaying )
				{
					sound->_channel = nullptr;
					iter = _sounds.Remove( iter );
				}
				else
				{
					sound->Update( dt );
					++iter;
				}
			}
			_fmod->update();
		}
	}


	// Cleanup
	void Audio::Release()
	{
		if ( _fmod )
		{
			_fmod->release();
			_fmod = nullptr;
		}
	}


	// Create a sound
	Sound* Audio::LoadSound( const char* file, bool isLooping, bool is3D )
	{
		// See if this sound is already loaded
		std::map<const String, SoundRef>& resourceMap = is3D ? _loaded3DSounds : _loaded2DSounds;
		FMOD::Sound* sound = nullptr;
		auto iter = resourceMap.find( file );
		if ( iter != resourceMap.end() )
		{
			SoundRef& ref = iter->second;
			++ref.refCount;
			sound = ref.sound;
		}
		else
		{
			// Setup FMOD flags
			uint32 flags = isLooping ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
			flags |= is3D ? FMOD_3D : FMOD_2D;

			// Create the sound
			FMOD_RESULT result = _fmod->createStream( file, flags, 0, &sound );
			if ( result != FMOD_OK )
				return false;
			SoundRef ref;
			ref.sound = sound;
			ref.refCount = 1;
			resourceMap.emplace( file, ref );
		}

		// Setup the engine sound object
		Sound* s = new Sound;
		s->_fmod = _fmod;
		s->_volume = 1.0f;
		s->_sound = sound;
		s->_parent = this;
		s->_is3D = is3D;
		s->_isLooping = isLooping;
		s->_fileName = file;
		sound->getDefaults( &( s->_defaultFreq ), 0, 0, 0 );

		return s;
	}

	// Register a component to the system
	bool Audio::RegisterComponent( Component* component )
	{
		return false;
	}

	// Remove a component (assumes the component exists already in the system!)
	void Audio::UnregisterComponent( Component* component )
	{

	}

	// Stops all currently playing sounds
	void Audio::StopSounds()
	{
		for ( auto iter = _sounds.Begin(); !iter.End(); ++iter )
		{
			Sound* sound = *iter;
			if ( sound->_channel )
			{
				sound->_channel->stop();
				sound->_channel = nullptr;
			}
		}
		_sounds.Clear();
	}

	// (Un)Pauses all currently playing sounds
	void Audio::PauseSounds( bool pause )
	{
		for ( auto iter = _sounds.Begin(); !iter.End(); ++iter )
		{
			Sound* sound = *iter;

			// pause if playing
			if ( pause )
			{
				bool bPlaying;
				sound->_channel->isPlaying( &bPlaying );
				if ( bPlaying )
					sound->_channel->setPaused( pause );
			}
			// unpause if paused
			else
			{
				bool bPaused;
				sound->_channel->getPaused( &bPaused );
				if ( bPaused )
					sound->_channel->setPaused( pause );
			}
		}
	}

	// Slows all sounds neccisary for the critical state of the player
	void Audio::CriticalSlow( bool slow )
	{
		for ( auto iter = _sounds.Begin(); !iter.End(); ++iter )
		{
			Sound* sound = *iter;
			if ( sound->GetFilePath().Contains( "Heartbeat" ) || sound->GetFilePath().Contains( "ShellShockScreach" ) )
				continue;

			if ( sound->_channel )
			{
				if ( slow )
					sound->SetFrequency( sound->GetDefaultFrequency() * 0.6f, false );
				else
					sound->SetFrequency( sound->GetDefaultFrequency(), false );
			}
		}
	}

	// Release a sound object
	void Audio::ReleaseSound( Sound* sound )
	{
		sound->Stop();

		std::map<const String, SoundRef>& resourceMap = sound->Is3D() ? _loaded3DSounds : _loaded2DSounds;
		auto iter = resourceMap.find( sound->_fileName );
		if ( iter != resourceMap.end() )
		{
			SoundRef& ref = iter->second;
			--ref.refCount;
			if ( ref.refCount == 0 )
			{
				ref.sound->release();
				resourceMap.erase( iter );
			}
		}
	}


#pragma endregion

}

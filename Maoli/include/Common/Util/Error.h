//--------------------------------------------------------------------------------------
// File: Error.h
//
// Error handling
//
// For error macros:
//		H -> HRESULT
//		B -> BOOL
//		D -> D3D HRESULT
//		M -> Generic error, no return type
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Displays a message box
	void ErrorMessage(const char* msg, ...);

	// Asserts in debug mode only
#if defined(DEBUG) || defined(_DEBUG)
#define M_ASSERT(err, msg) assert(err && msg)
#else
#define M_ASSERT(err, msg)
#endif

	// General errors
#if defined(DEBUG) || defined(_DEBUG)
#define H_ERROR(hr, msg) if(FAILED(hr)) Maoli::ErrorMessage("%s> (function:%s, line:%d)", (msg), __FUNCTION__, __LINE__);
#define B_ERROR(b, msg) if(!b) Maoli::ErrorMessage("%s> (function:%s, line:%d)", (msg), __FUNCTION__, __LINE__);
#define M_ERROR(msg) Maoli::ErrorMessage("%s> (function:%s, line:%d)", (msg), __FUNCTION__, __LINE__);
#define D_ERROR(hr) if(FAILED(hr)) Maoli::ErrorMessage("%s> (function:%s, line:%d)", DXGetErrorDescriptionA(hr), __FUNCTION__, __LINE__);
#define H_RETURN(hr, msg) if(FAILED(hr)){ Maoli::ErrorMessage("%s> (function:%s, line:%d)", (msg), __FUNCTION__, __LINE__); return hr; }
#define B_RETURN(b, msg) if(!b){ Maoli::ErrorMessage("%s> (function:%s, line:%d)", (msg), __FUNCTION__, __LINE__); return b; }
#define M_RETURN(msg) Maoli::ErrorMessage("%s> (function:%s, line:%d)", (msg), __FUNCTION__, __LINE__); return;
#define D_RETURN(hr) if(FAILED(hr)){ Maoli::ErrorMessage("%s> (function:%s, line:%d)", DXGetErrorDescriptionA(hr), __FUNCTION__, __LINE__); return hr; }
#else
#define H_ERROR(hr, msg) if(FAILED(hr)) Maoli::ErrorMessage(msg);
#define B_ERROR(b, msg) if(!b) Maoli::ErrorMessage(msg);
#define M_ERROR(msg) Maoli::ErrorMessage(msg);
#define D_ERROR(hr) if(FAILED(hr)) Maoli::ErrorMessage(DXGetErrorDescriptionA(hr));
#define H_RETURN(hr, msg) if(FAILED(hr)){ Maoli::ErrorMessage(msg); return hr; }
#define B_RETURN(b, msg) if(!b){ Maoli::ErrorMessage(msg); return b; }
#define M_RETURN(msg) Maoli::ErrorMessage(msg); return;
#define D_RETURN(hr) if(FAILED(hr)){ Maoli::ErrorMessage(DXGetErrorDescriptionA(hr)); return hr; }
#endif

	// Validate a d3d error code
	void CheckD3DError(HRESULT hr);

}

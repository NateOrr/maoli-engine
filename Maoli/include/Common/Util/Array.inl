//--------------------------------------------------------------------------------------
// File: Array.inl
//
// Growable array class
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{
	// Constructors
	template <typename T>
	Array<T>::Array(int32 size)
	{ 
		_size=0; 
		_data = nullptr; 
		_capacity=0; 
		if(size>0)
			Allocate(size);
	} 


	// Copy ctor
	template <typename T>
	Array<T>::Array(const Array<T>& a)
	{
		_size=0; 
		_data = nullptr; 
		_capacity=0; 
		*this = a;
	}

	// Assignment op
	template <typename T>
	Array<T>& Array<T>::operator=(const Array<T>& a)
	{
		if(this != &a)
		{
			Release();
			_size = a._size;
			_capacity = a._capacity;
			_data = new T[_size];
			for(uint32 i=0; i<_size; ++i)
				_data[i] = a[i];
		}
		return *this;
	}

	// Destructor
	template <typename T>
	Array<T>::~Array()
	{ 
		Release();
	}


	// Bracket [] style access
	template <typename T>
	inline T& Array<T>::operator[]( int32 i )
	{
		M_ASSERT(i<(int32)_size, "Array access out of bounds");
		return _data[i];
	}

	// Const Bracket [] style access
	template <typename T>
	inline const T& Array<T>::operator[]( int32 i ) const
	{
		M_ASSERT(i<(int32)_size, "Array access out of bounds");
		return _data[i];
	}


	// Adds an object to the end of the array
	template <typename T>
	void Array<T>::Add( const T& t )
	{
		if(IsFull())
			Resize();
		_data[_size] = t;
		++_size;
	}

	// Adds an empty object to the end of the array
	template <typename T>
	T& Array<T>::Add()
	{
		if(IsFull())
			Resize();
		_data[_size] = T();
		++_size;
		return _data[_size-1];
	}

	// Copy a range of data to the end of the array
	template <typename T>
	void Maoli::Array<T>::AddRange( const T* data, uint32 count )
	{
		if ( _size + count > _capacity )
			Resize();
		memcpy( &_data[_size], data, sizeof(T)*count );
		_size += count;
	}


	// Write to binary file
	template <typename T>
	void Array<T>::Serialize(std::ofstream& file) const
	{
		Maoli::Serialize(file, &_size);
		for(uint32 i=0; i<_size; ++i)
			Maoli::Serialize(file, &_data[i]);
	}


	// Read from binary file
	template <typename T>
	void Array<T>::Deserialize(std::ifstream& file)
	{
		Release();
		Maoli::Deserialize(file, &_size);
		_data = new T[_size];
		for(uint32 i=0; i<_size; ++i)
			Maoli::Deserialize(file, &_data[i]);
		_capacity = _size;
	}


	// Inserts an object at [index]
	template <typename T>
	void Array<T>::Insert(const T& t, uint32 index)
	{
		M_ASSERT(index <= _size , "Insertion point outside array bounds");
		
		if(IsFull())
			Resize();
		uint32 i;
		for(i=_size; i>index; --i)
			_data[i] = _data[i-1];
		_data[i] = t;
		++_size;
	}

	// Removes the object at [index]
	template <typename T>
	T Array<T>::RemoveAt(uint32 index)
	{
		M_ASSERT(index < _size , "Remove point outside array bounds");

		T ret = _data[index];
		for(uint32 i=index; i<_size-1; ++i)
			_data[i] = _data[i+1];
		--_size;
		return ret;
	}

	// Removes the given object
	template <typename T>
	bool Array<T>::Remove(const T& t)
	{
		uint32 i;
		bool ret = false;
		for(i=0; i<_size; ++i)
		{
			if(_data[i] == t)
			{
				ret = true;
				--_size;
				break;
			}
		}

		for(; i<_size; ++i)
			_data[i] = _data[i+1];
		return ret;
	}


	// Removes the given object
	template <typename T>
	bool Array<T>::RemoveAll(const T& t)
	{
		bool ret = false;
		for(uint32 i=0; i<_size; ++i)
		{
			if(_data[i] == t)
			{
				ret = true;
				--_size;
				for(uint32 j=i; j<_size; ++j)
					_data[j] = _data[j+1];
			}
		}
		
		return ret;
	}

	// Grows the array
	template <typename T>
	void Array<T>::Resize()
	{ 
		if(_capacity==0)
			_capacity = 8;
		else
			_capacity*=2;
		T* pNew = new T[_capacity]; 
		for(uint32 i=0; i<_size; ++i)
			pNew[i] = _data[i];
		delete[] _data;
		_data = pNew;
	}

	// Frees all mem and makes an empty array
	template <typename T>
	void Array<T>::Release()
	{
		_size=0;
		_capacity=0;
		SafeDeleteArray(_data);
	}


	// Resets the array without freeing mem
	template <typename T>
	inline void Array<T>::Clear()
	{
		_size = 0;
	}


	// Create using an array, this creates a copy
	template <typename T>
	void Array<T>::FromArray(const T* pData, uint32 iLength)
	{
		Release();
		_data = new T[iLength];
		for(uint32 i=0; i<iLength; ++i)
			_data[i] = pData[i];
		_size = _capacity = iLength;
	}

	// Create using an array and manage the deletion
	template <typename T>
	void Array<T>::TakeArray(T* pData, uint32 iLength)
	{
		Release();
		_data = pData;
		_size = _capacity = iLength;
	}

	// Pre-allocates memory
	template <typename T>
	void Array<T>::Allocate(uint32 size, bool zero)
	{
		Release();
		_data = new T[size];
		_size = _capacity = size;
		if(zero)
			Zero();
	}	


	// Reserve a certain capacity, objects are NOT added here
	template <typename T>
	void Array<T>::Reserve(uint32 size)
	{
		Release();
		_data = new T[size];
		_capacity = size;
	}

	// Remove unused memory from the end of the array
	template <typename T>
	void Array<T>::Finalize()
	{
		if(!_data)
			return;
		T* pNewData = new T[_size];
		for(uint32 i=0; i<_size; ++i)
			pNewData[i] = _data[i];
		delete[] _data;
		_data = pNewData;
		_capacity = _size;
	}


	// Is the array full?
	template <typename T>
	inline bool Array<T>::IsFull() const
	{
		return (_size==_capacity);
	}

	// Is the array empty?
	template <typename T>
	inline bool Array<T>::IsEmpty() const
	{
		return (_size==0);
	}

	// Returns the size of the array
	template <typename T>
	inline uint32 Array<T>::Size() const
	{
		return _size;
	}

	// Returns the max capacity of the array
	template <typename T>
	inline uint32 Array<T>::Capacity() const
	{
		return _capacity;
	}

	// Convert to c-style array/pointer automatically
	template <typename T>
	inline Array<T>::operator T*()
	{
		return _data;
	}

	// Convert to const c-style array/pointer automatically
	template <typename T>
	inline Array<T>::operator const T*() const
	{
		return _data;
	}

	// Memset to 0
	template <typename T>
	void Array<T>::Zero()
	{
		memset(_data, 0, sizeof(T)*_capacity);
	}

	// Sort the elements in ascending order
	template <typename T>
	void Array<T>::Sort()
	{
		std::sort(_data, _data+_size);
	}

	// Get raw data pointer (use at own risk)
	template <typename T>
	inline void* Array<T>::GetRawData()
	{
		return reinterpret_cast<void*>(_data);
	}

	// Check if the array contains an element
	template <typename T>
	bool Maoli::Array<T>::Contains( const T& t ) const
	{
		for ( uint32 i = 0; i < _size; ++i )
		if ( _data[i] == t )
			return true;
		return false;
	}

}

// Print to a stream
template <typename T>
std::ostream& operator<<( std::ostream& os, const Maoli::Array<T>& a )
{
	for(uint32 i=0; i<a.Size(); ++i)
		os << a[i] << " ";
	os << std::endl;
	return os;
}

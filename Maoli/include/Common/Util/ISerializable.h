//--------------------------------------------------------------------------------------
// File: ISerializable.h
//
// Interface for serializable classes
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once
#include <fstream>

namespace Maoli
{
	class ISerializable
	{
	public:
		// Write to binary file
		virtual void Serialize(std::ofstream& file) const = 0;

		// Read from binary file
		virtual void Deserialize(std::ifstream& file) = 0;
	};
}

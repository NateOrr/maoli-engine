//--------------------------------------------------------------------------------------
// File: UserList.cpp
//
// Linked List where the user is responsible for node memory
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"

namespace Maoli
{
#pragma region List

	// Ctor
	List<LinkedListNode>::List()
	{
		_head = _tail = nullptr;
		_size = 0;
	}


	// Dtor
	List<LinkedListNode>::~List()
	{

	}

	// Adds an object to the front of the list
	void List<LinkedListNode>::AddToFront( LinkedListNode* node )
	{
		M_ASSERT( (!node->next && !node->prev && !node->list), "This node already exists in another list! Remove it first" );

		if ( !_head )
			_tail = _head = node;
		else
		{
			_head->prev = node;
			node->next = _head;
			_head = node;
		}
		node->list = this;
		++_size;
	}


	// Adds an object to the end of the list
	void List<LinkedListNode>::Add( LinkedListNode* node )
	{
		M_ASSERT( (!node->next && !node->prev && !node->list), "This node already exists in another list! Remove it first" );

		if ( !_head )
			_tail = _head = node;
		else
		{
			node->prev = _tail;
			_tail->next = node;
			_tail = node;
		}
		node->list = this;
		++_size;
	}

	// Insert at the specified iterator
	List<LinkedListNode>::Iterator List<LinkedListNode>::Insert( const List<LinkedListNode>::Iterator& iter, LinkedListNode* node )
	{
		M_ASSERT( (!node->next && !node->prev && !node->list), "This node already exists in another list! Remove it first" );
		M_ASSERT( !iter.End(), "Invalid iterator" );

		// Setup links
		LinkedListNode* at = iter._node;
		if ( at == _head )
		{
			node->next = _head;
			_head->prev = node;
			_head = node;
		}
		else
		{
			node->next = at;
			node->prev = at->prev;
			if ( at->prev )
				at->prev->next = node;
			at->prev = node;
		}

		// Update the iterator
		node->list = this;
		++_size;
		return Iterator( node );
	}

	// Removes the first object and returns it
	LinkedListNode* List<LinkedListNode>::RemoveFirst()
	{
		M_ASSERT( _head != nullptr, "Cannot remove from an empty list" );

		LinkedListNode* first = _head;
		LinkedListNode* node = _head;
		--_size;
		if ( _head == _tail )
			_head = _tail = nullptr;
		else if ( (_head = _head->next) )
			_head->prev = nullptr;

		first->list = nullptr;
		first->next = first->prev = nullptr;

		return first;
	}


	// If obj is in the list it is removed and returned
	bool List<LinkedListNode>::Remove( LinkedListNode* node )
	{
		M_ASSERT( _head != nullptr, "Cannot remove from an empty list" );

		// It isn't in the list...
		if ( node->list != this )
			return false;
		--_size;

		// Clear the nodes prev pointer
		if ( node->prev )
			node->prev->next = node->next;
		if ( node->next )
			node->next->prev = node->prev;
		if ( node == _head )
			_head = node->next;
		if ( node == _tail )
			_tail = node->prev;

		node->list = nullptr;
		node->next = node->prev = nullptr;

		return true;
	}


	// Remove at the specified iterator
	List<LinkedListNode>::Iterator List<LinkedListNode>::Remove( const List<LinkedListNode>::Iterator& iter )
	{
		M_ASSERT( _head != nullptr, "Cannot remove from an empty list" );
		M_ASSERT( !iter.End(), "Invalid iterator" );

		// Clear the nodes prev pointer
		LinkedListNode* node = iter._node;
		Iterator next( node->next );
		if ( node->prev )
			node->prev->next = node->next;
		if ( node->next )
			node->next->prev = node->prev;
		if ( node == _head )
			_head = node->next;
		if ( node == _tail )
			_tail = node->prev;

		node->list = nullptr;
		node->next = node->prev = nullptr;

		// Delete it and decrement the length
		--_size;
		return next;
	}


	// Removes the object at the end of the list and returns it
	LinkedListNode* List<LinkedListNode>::RemoveLast()
	{
		M_ASSERT( _tail != nullptr, "Cannot remove from an empty list" );
		LinkedListNode* last = _tail;
		LinkedListNode* node = _tail;
		--_size;
		if ( _head == _tail )
			_head = _tail = nullptr;
		else if ( (_tail = _tail->prev) )
			_tail->next = nullptr;

		node->list = nullptr;
		node->next = node->prev = nullptr;

		return last;
	}

	// Makes an empty list
	void List<LinkedListNode>::Clear()
	{
		LinkedListNode* node = _head;
		while ( node != nullptr )
		{
			node->list = nullptr;
			LinkedListNode* next = node->next;
			node->next = node->prev = nullptr;
			node = next;
		}
		_head = _tail = nullptr;
		_size = 0;
	}

	// Frees all mem and creates an empty list
	void List<LinkedListNode>::Release()
	{
		_head = _tail = nullptr;
		_size = 0;
	}

	// Check if the list contains an element
	bool Maoli::List<LinkedListNode>::Contains( LinkedListNode* node )
	{
		return (node->list == this);
	}


#pragma endregion


#pragma region Iterator


	// Ctor
	List<LinkedListNode>::Iterator::Iterator( LinkedListNode* n ) : _node( n )
	{

	}

	// Ctor
	List<LinkedListNode>::Iterator::Iterator() : _node( nullptr )
	{

	}

#pragma endregion


}

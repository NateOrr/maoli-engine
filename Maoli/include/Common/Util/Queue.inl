//---------------------------------------------------------------------------------------
// File: Queue.inl
//
// Queue ADT
//
// Nate Orr
//---------------------------------------------------------------------------------------

namespace Maoli
{
	// Enqueue
	template <typename T>
	inline void Queue<T>::Enqueue(const T& obj)
	{
		Add(obj);
	}

	// Dequeue
	template <typename T>
	inline T Queue<T>::Dequeue()
	{
		return List<T>::RemoveFirst();
	}

	// Peek the front
	template <typename T>
	inline T& Queue<T>::Front() const
	{
		return List<T>::GetFirst();
	}

	// True if empty
	template <typename T>
	inline bool Queue<T>::IsEmpty() const
	{
		return List<T>::IsEmpty();
	}

	// Get the number of elements
	template <typename T>
	inline uint32 Queue<T>::Size() const
	{
		return List<T>::Size();
	}

	// Clear the queue
	template <typename T>
	void Maoli::Queue<T>::Clear()
	{
		List<T>::Clear();
	}

	// Free memory
	template <typename T>
	inline void Queue<T>::Release()
	{
		List<T>::Release();
	}

	// Dtor
	template <typename T>
	Queue<T>::~Queue()
	{
		Release();
	}
}

//--------------------------------------------------------------------------------------
// File: ThreadPool.cpp
//
// Worker thread pooling
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "ThreadPool.h"

#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>

// Mutex lock
typedef std::unique_lock<std::mutex> unique_lock;

namespace Maoli
{
	// Starts the threads
	void ThreadPool::Init(int32 numThreads, ShutdownComponent component, uint32 memoryPerThread)
	{
		_isActive = new std::atomic_bool;
		_isActive->store(true);
		_numThreads = numThreads;
		_component = component;
		_memPerThread = memoryPerThread;
		_jobMutex = new std::mutex;

		// Allocate thread specific memory
		_threadID.Allocate(_numThreads);		
		_threads.Allocate(_numThreads);		

		// Set off the threads
		for(int32 i=0; i<_numThreads; ++i)
		{
			_threadID[i].id = i;
			_threadID[i].parent = this;
			_threads[i] = new std::thread(&ThreadPool::JobProc, this, &_threadID[i]);
		}
	}


	// Get the number of active threads
	inline int32 ThreadPool::GetNumThreads() const 
	{ 
		return _numThreads; 
	}


	// Queues up a job
	void ThreadPool::SubmitJob(ThreadJob jobFn, int32 priority)
	{
		unique_lock lock(*_jobMutex);
		_jobs.Enqueue( jobFn, priority );
		_jobSemaphore.Post();
	}


	// Thread callback
	void ThreadPool::JobProc(ThreadData* thread)
	{
		// Wait for work, and do the work as it comes in
		//uint32 heapID = MemoryManager::GetInstance()->CreateHeap(_memPerThread);
		while(_isActive->load())
		{
			_jobSemaphore.Wait();
			unique_lock lock(*_jobMutex);
			if(!_jobs.IsEmpty())
			{
				ThreadJob job = _jobs.Dequeue();
				lock.unlock();
				job();
			}
		}
		//MemoryManager::GetInstance()->DestroyHeap(heapID);
	}


	// Frees memory
	void ThreadPool::Release()
	{
		// Force all jobs to finish if needed
		if(_component == ShutdownComponent::FinishAllJobs)
		{
			bool jobsLeft = true;
			while(jobsLeft)
			{
				_jobSemaphore.Post();
				unique_lock lock(*_jobMutex);
				jobsLeft = !_jobs.IsEmpty();
				lock.unlock();
			}
		}
		
		// Wait for the threads to close
		_isActive->store(false);
		for(int32 i=0; i<_numThreads; ++i)
		{
			for(int32 j=0; j<_numThreads; ++j)
				_jobSemaphore.Post();
			_threads[i]->join();
			delete _threads[i];
		}

		// Clear out the arrays
		_jobs.Release();	
		_threads.Release();
		_threadID.Release();	
		_numThreads = 0;
		SafeDelete(_jobMutex);
		SafeDelete(_isActive);
	}

}

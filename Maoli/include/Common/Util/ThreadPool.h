//--------------------------------------------------------------------------------------
// File: ThreadPool.h
//
// Worker threads and async job queuing
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Semaphore.h"

namespace Maoli
{

	// Shutdown components
	enum class ShutdownComponent
	{
		FinishCurrentJobs,
		FinishAllJobs,
	};

	// Thread job function def
	typedef std::function<void()> ThreadJob;

	// Manages worker threads for generic jobs
	class ThreadPool
	{
	public:

		// Starts the threads
		void Init(int32 numTheads, ShutdownComponent component = ShutdownComponent::FinishCurrentJobs, uint32 memoryPerThread = 1024*1024);

		// Queues up a job
		void SubmitJob(ThreadJob jobFn, int32 priority = 0);

		// Frees memory and closes all threads
		void Release();

		// Get the number of active threads
		int32 GetNumThreads() const;
	
	private:

		// Parameter for the thread callbacks
		struct ThreadData 
		{
			ThreadPool* parent;
			int32			id;
		};

		
		// Thread callback
		void JobProc(ThreadData* thread);
		
		// Thread specific
		Array<ThreadData>		_threadID;			// ID for each thread
		Array<std::thread*>		_threads;			// The threads

		// Global
		static const int32		_priorityLevels=3;  // Number of priority levels
		std::atomic_bool*		_isActive;			// True while the app is running
		int32						_numThreads;		// Total number of threads
		uint32					_memPerThread;		// Heap size for each thread
		std::mutex*				_jobMutex;			// Mutex for the job queue
		Semaphore				_jobSemaphore;		// Semaphore for available work
		ShutdownComponent		_component;			// Defines how the pool should shutdown

		// The job priority queue
		DiscretePriorityQueue<ThreadJob, _priorityLevels, int32>		_jobs;

	};

}

//--------------------------------------------------------------------------------------
// File: ResourceManager.h
//
// Template resource manager class
// Handles file-based resource objects
//
// Any class to be used with a ResourceManager MUST inherit from class Resource! 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	template <typename ManagerType, typename ResourceType>
	class ResourceManager
	{
	public:
		// Ctor
		ResourceManager( ManagerType* parent ) : _parent( parent ) {}

		// Create an new, empty resource.  up to the app to initialize
		Pointer<ResourceType> Create( const char* name = nullptr );

		// Retrieves an object pointer from the list, otherwise returns nullptr
		Pointer<ResourceType> Get( const char* sName ) const;

		// Loads an object from a file into the manager if it's not already in it
		Pointer<ResourceType> Load( const char* sFile );

		// Adds an object to the manager if it's not already in it
		void Add( Pointer<ResourceType>& pNew );

		// Reloads the resource
		void Reload( Pointer<ResourceType>& resource );

		// Releases all resources
		void Release();

	private:

		// Removes a resource
		void Unload( Pointer<ResourceType>& resource );

		// Behind the scenes smart pointer management
		void OnResourceRelease( Pointer<ResourceType>& resource );

		List<Pointer<ResourceType>>	_resources;		// Linked list of resource objects
		ManagerType*				_parent;		// Graphics engine
	};
}

#include "ResourceManager.inl"

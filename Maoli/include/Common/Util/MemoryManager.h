//--------------------------------------------------------------------------------------
// File: MemoryManager.h
//
// Custom memory management
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	// Forward decl
	class Heap;

	class MemoryManager
	{
	public:

		// Get singleton instance
		static MemoryManager* GetInstance();

		// Create a heap
		uint32 CreateHeap(uint32 size = 32*1024*1024, bool threadShared = false, const char* name = "HEAP");

		// Free heap memory
		void DestroyHeap(uint32 id);

		// Allocate memory
		void* Allocate(uint32 size, uint32 heapID = -1);

		// Free memory
		void Free(void* mem);

		// Shows stats
		void DisplayStats();

		// Free all memory
		void Release();

	private:

		// Singleton protection
		MemoryManager();
		~MemoryManager();
		MemoryManager(const MemoryManager&);
		MemoryManager(const MemoryManager&&);
		MemoryManager& operator=(const MemoryManager&);

		static const uint32	_maxHeaps = 4;		// Max number of heaps
		uint32				_numHeaps;			// Number of heaps
		Heap*				_heaps[_maxHeaps];	// Available heaps
		uint32				_numAllocations;	// Total number of allocations
		uint32				_numDeallocations;	// Total number of deallocations
		uint32				_totalAllocated;	// Total size of allocated memory
		std::mutex*			_mutex;				// Thread safe heap creation/destruction
		
	};

}
//
//// Global new
//inline void* operator new(size_t size)
//{
//	return Maoli::MemoryManager::GetInstance()->Allocate(size);
//}
//
//// Global delete
//inline void operator delete(void *p)
//{
//	Maoli::MemoryManager::GetInstance()->Free(p);
//}
//
//// Global new for arrays
//inline void* operator new[](size_t size)
//{
//	return Maoli::MemoryManager::GetInstance()->Allocate(static_cast<uint32>(size));
//}
//
//// Global delete for arrays
//inline void operator delete[](void *p)
//{
//	Maoli::MemoryManager::GetInstance()->Free(p);
//}
//

//--------------------------------------------------------------------------------------
// File: Timer.h
//
// Timer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	class Timer
	{
	public:

		// Ctor
		Timer();

		// Reset the timer
		void Reset();

		// Get the elapsed time in seconds
		float GetElapsedSeconds();

		// Get the elapsed time in milliseconds
		uint32 GetElapsedMilliseconds();


	private:

		// Type helpers
		typedef std::chrono::high_resolution_clock Clock;

		Clock::time_point _startTime;	// The starting time
	};
}


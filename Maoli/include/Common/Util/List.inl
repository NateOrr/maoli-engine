//--------------------------------------------------------------------------------------
// File: List.inl
//
// Linked List class
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{


#pragma region List

	// Node ctor
	template <typename T>
	List<T>::Node::Node()
	{
		next = prev = nullptr;
	}

	// Delete node data
	template <typename T>
	void List<T>::Node::Delete()
	{
		SafeDelete( data );
	}


	// Ctor
	template <typename T>
	List<T>::List()
	{
		_head = _tail = nullptr;
		_size = 0;
	}


	// Copy ctor
	template <typename T>
	List<T>::List( const List& l )
	{
		_head = _tail = nullptr;
		_size = 0;
		*this = l;
	}

	// Assignment op
	template <typename T>
	List<T>& List<T>::operator=(const List<T>& l)
	{
		if ( this != &l )
		{
			Release();
			for ( auto i = l.Begin(); !i.End(); ++i )
				Add( *i );
		}
		return *this;
	}



	// Dtor
	template <typename T>
	List<T>::~List()
	{
		Release();
	}


	// Get an iterator at the start of the list
	template <typename T>
	inline typename List<T>::Iterator List<T>::Begin() const
	{
		return Iterator( _head );
	}

	// Get an iterator at the end of the list (empty iterator)
	template <typename T>
	inline typename List<T>::Iterator List<T>::End() const
	{
		return Iterator( nullptr );
	}


	// Is the list empty?
	template <typename T>
	inline bool List<T>::IsEmpty() const
	{
		return (_size == 0);
	}

	// Adds an object to the front of the list
	template <typename T>
	void List<T>::AddToFront( const T& data )
	{
		Node* node = GetNewNode();
		node->data = data;
		if ( !_head )
			_tail = _head = node;
		else
		{
			_head->prev = node;
			node->next = _head;
			_head = node;
		}
		++_size;
	}


	// Adds an object to the end of the list
	template <typename T>
	void List<T>::Add( const T& data )
	{
		Node* node = GetNewNode();
		node->data = data;
		if ( !_head )
			_tail = _head = node;
		else
		{
			node->prev = _tail;
			_tail->next = node;
			_tail = node;
		}
		++_size;
	}

	// Adds an object to the end of the list
	template <typename T>
	T& List<T>::Add()
	{
		Node* node = GetNewNode();
		if ( !_head )
			_tail = _head = node;
		else
		{
			node->prev = _tail;
			_tail->next = node;
			_tail = node;
		}
		++_size;
		return node->data;
	}

	// Insert at the specified iterator
	template <typename T>
	typename List<T>::Iterator List<T>::Insert( typename List<T>::Iterator const& iter, const T& data )
	{
		M_ASSERT( !iter.End(), "Invalid iterator" );

		// Make the new node
		Node* n = GetNewNode();
		n->data = data;

		// Setup links
		Node* at = iter._node;
		if ( at == _head )
		{
			n->next = _head;
			_head->prev = n;
			_head = n;
		}
		else
		{
			n->next = at;
			n->prev = at->prev;
			if ( at->prev )
				at->prev->next = n;
			at->prev = n;
		}

		// Update the iterator
		++_size;
		return Iterator( n );
	}


	// Gets objects at the end of the list
	template <typename T>
	inline T& List<T>::GetFirst() const
	{
		M_ASSERT( _head != nullptr, "Cannot read from an empty list" );
		return _head->data;
	}


	// Gets objects at the end of the list
	template <typename T>
	inline T& List<T>::GetLast() const
	{
		M_ASSERT( _tail != nullptr, "Cannot read from an empty list" );
		return _tail->data;
	}

	// Gets the number of objects in the list
	template <typename T>
	inline uint32 List<T>::Size() const
	{
		return _size;
	}


	// Removes the first object and returns it
	template <typename T>
	T List<T>::RemoveFirst()
	{
		T data = _head->data;
		Node* node = _head;
		--_size;
		if ( _head == _tail )
			_head = _tail = nullptr;
		else
		if ( (_head = _head->next) )
			_head->prev = nullptr;
		RecycleNode( node );
		return data;
	}


	// If obj is in the list it is removed and returned
	template <typename T>
	bool List<T>::Remove( const T& obj )
	{
		M_ASSERT( _head != nullptr, "Cannot remove from an empty list" );


		// Find the object
		Node* node = _head;
		while ( node&&node->data != obj )
			node = node->next;

		// It isn't in the list...
		if ( !node )
			return false;
		--_size;

		// Clear the nodes prev pointer
		if ( node->prev )
			node->prev->next = node->next;
		if ( node->next )
			node->next->prev = node->prev;
		if ( node == _head )
			_head = node->next;
		if ( node == _tail )
			_tail = node->prev;

		// Delete it and decrement the length
		RecycleNode( node );

		return true;
	}


	// Remove at the specified iterator
	template <typename T>
	typename List<T>::Iterator List<T>::Remove( typename List<T>::Iterator const& iter )
	{
		M_ASSERT( _head != nullptr, "Cannot remove from an empty list" );
		M_ASSERT( !iter.End(), "Invalid iterator" );

		// Clear the nodes prev pointer
		Node* node = iter._node;
		Iterator next( node->next );
		if ( node->prev )
			node->prev->next = node->next;
		if ( node->next )
			node->next->prev = node->prev;
		if ( node == _head )
			_head = node->next;
		if ( node == _tail )
			_tail = node->prev;

		// Delete it and decrement the length
		--_size;
		RecycleNode( node );
		return next;
	}


	// Removes the object at the end of the list and returns it
	template <typename T>
	T List<T>::RemoveLast()
	{
		M_ASSERT( _tail != nullptr, "Cannot remove from an empty list" );
		T& data = _tail->data;
		Node* node = _tail;
		--_size;
		if ( _head == _tail )
			_head = _tail = nullptr;
		else
		if ( (_tail = _tail->prev) )
			_tail->next = nullptr;
		RecycleNode( node );
		return data;
	}

	// Makes an empty list
	template <typename T>
	void List<T>::Clear()
	{
		Node* temp = _head;
		while ( temp != nullptr )
		{
			Node* n = temp->next;
			RecycleNode( temp );
			temp = n;
		}
		_head = _tail = nullptr;
		_size = 0;
	}

	// Frees all mem and creates an empty list
	template <typename T>
	void List<T>::Release()
	{
		while ( _head )
		{
			Node* t = _head->next;
			delete _head;
			_head = t;
		}
		_tail = nullptr;
		_head = nullptr;
		_size = 0;
		while ( !_recycled.IsEmpty() )
			delete _recycled.Pop();
		_recycled.Release();
	}

	// Clears the list
	template <typename T>
	void List<T>::ReleaseAndDelete()
	{
		Node* temp = _head;
		while ( temp )
		{
			temp->Delete();
			temp = temp->next;
		}
		SafeDelete( _head );
		while ( !_recycled.IsEmpty() )
		{
			delete _recycled.Top()->Delete();
			delete _recycled.Pop();
		}
		_recycled.Release();
		_tail = nullptr;
		_head = nullptr;
		_size = 0;
		List();
	}

	// Recycle a node
	template <typename T>
	void List<T>::RecycleNode( Node* node )
	{
		//delete node;
		node->next = nullptr;
		node->prev = nullptr;
		node->data = T();
		_recycled.Push( node );
	}


	// Get a free node
	template <typename T>
	typename List<T>::Node* List<T>::GetNewNode()
	{
		//return new Node;
		if ( _recycled.IsEmpty() )
			return new Node;
		return _recycled.Pop();
	}

	// Check if the list contains an element
	template <typename T>
	bool Maoli::List<T>::Contains( const T& value )
	{
		for ( auto i = Begin(); !i.End(); ++i )
			if ( *i == value )
				return true;
		return false;
	}


#pragma endregion


#pragma region Iterator


	// Ctor
	template <typename T>
	List<T>::Iterator::Iterator( Node* n ) : _node( n )
	{

	}

	// Ctor
	template <typename T>
	List<T>::Iterator::Iterator() : _node( nullptr )
	{

	}

	// Move forward
	template <typename T>
	inline typename List<T>::Iterator& List<T>::Iterator::operator++()
	{
		if ( _node )
			_node = _node->next;
		return *this;
	}

	// Move backward
	template <typename T>
	inline typename List<T>::Iterator& List<T>::Iterator::operator--()
	{
		if ( _node )
			_node = _node->prev;
		return *this;
	}

	// True if the iterator has reached the end in either direction
	template <typename T>
	inline bool List<T>::Iterator::End() const
	{
		return (_node == nullptr);
	}

	// Comparison
	template <typename T>
	inline bool List<T>::Iterator::operator==(const Iterator& iter) const
	{
		return _node == iter._node;
	}

	// Comparison
	template <typename T>
	inline bool List<T>::Iterator::operator!=(const Iterator& iter) const
	{
		return _node != iter._node;
	}

	// Data access
	template <typename T>
	inline T& List<T>::Iterator::operator*() const
	{
		M_ASSERT( _node != nullptr, "Invalid iterator dereferenced" );
		return _node->data;
	}

	// Data access
	template <typename T>
	inline T& List<T>::Iterator::operator->() const
	{
		M_ASSERT( _node != nullptr, "Invalid iterator dereferenced" );
		return _node->data;
	}

#pragma endregion

}


// Print to a stream
template <typename T>
std::ostream& operator<<(std::ostream& os, const Maoli::List<T>& list)
{
	for ( auto i = list.Begin(); i != list.End(); ++i )
		os << *i << " ";
	os << std::endl;
	return os;
}

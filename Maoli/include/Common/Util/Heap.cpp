//--------------------------------------------------------------------------------------
// File: Heap.cpp
//
// A block of memory
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Heap.h"

// 16-byte aligned malloc
static void* malloc16( size_t s )
{
	byte* p;
	byte* porig = (byte*)malloc( s + 0x10 );
	if ( !porig )
		return 0;
	p = (byte*)(int32( porig + 16 ) & (~0xf));
	*(p - 1) = p - porig;
	return p;
}

// 16-byte aligned free
static void free16( void *p )
{
	byte *porig = (byte*)p;
	porig = porig - *(porig - 1);
	free( porig );
}


namespace Maoli
{
	// Ctor
	Heap::Heap() : _name( "HEAP" )
	{
		_pool = nullptr;
		_size = 0;
		_threadShared = false;
		_freeListHead = nullptr;
		_firstHeader = nullptr;
		_lastFooter = nullptr;
	}

	// Free mem
	void Heap::Release()
	{
		if ( _pool )
			free16( _pool );
	}


	// Setup
	bool Heap::Init( uint32 poolSize, bool threadShared /*= false*/, const String& name /*= "HEAP"*/ )
	{
		M_ASSERT( poolSize % 16 == 0, "Heaps must be allocated in multiples of 16 bytes" );

		// Thread sync
		_threadShared = threadShared;
		if ( _threadShared )
			_mutex.lock();

		// Allocate the ppol
		_pool = (char*)malloc16( poolSize );
		if ( !_pool )
		{
			if ( _threadShared )
				_mutex.unlock();
			return false;
		}

		// Setup the header
		Header* header = (Header*)_pool;
		header->size = poolSize - sizeof( Header ) - sizeof( Footer );
		header->next = header;
		header->prev = header;

		// Setup the footer
		Footer* footer = (Footer*)(_pool + sizeof( Header ) + header->size);
		footer->size = header->size;

		// Lists
		_freeListHead = header;
		_firstHeader = header;
		_lastFooter = footer;

		// Misc
		_heapID = -1;
		_threadID = std::this_thread::get_id();
		_name = name;

		// Thread sync
		if ( _threadShared )
			_mutex.unlock();

		return true;
	}

	// Find a free block of memory that can store the requested size
	Heap::Header* Heap::FindBlock( uint32 size )
	{
		// Find the first block that's big enough
		Header* header = _freeListHead;
		do
		{
			if ( header->size >= size )
				return header;
			header = header->next;
		} while ( header != _freeListHead );

		// Otherwise return null, since nothing is available
		return nullptr;
	}

	// Allocate memory from the pool, returns null if not enough memory is available
	void* Heap::Allocate( uint32 size )
	{
		// Thread sync
		if ( _threadShared )
			_mutex.lock();

		// Grab the first available block
		Header* blockHeader = FindBlock( size );
		if ( blockHeader == nullptr )
			return nullptr;

		// Check if we should split
		void* dataPtr = nullptr;
		int32 netAllocSize = size + sizeof( Header ) + sizeof( Footer );
		int32 diff = blockHeader->size - netAllocSize;
		if ( diff > 0 )
		{
			// Split the block
			char* poolBlock = ((char*)blockHeader) + sizeof( Header );
			Footer* blockFooter = (Footer*)(poolBlock + diff);

			Header* newHeader = (Header*)(((char*)blockFooter) + sizeof( Footer ));
			char* newBlock = ((char*)newHeader) + sizeof( Header );
			Footer* newFooter = (Footer*)(newBlock + size);

			// Update the original block's header/footer
			blockHeader->size = diff;
			blockFooter->size = blockHeader->size;

			// Setup the new block's header/footer
			newHeader->size = size;
			newHeader->size |= (1 << 31); // set as in use
			newHeader->next = newHeader->prev = nullptr;
			newFooter->size = newHeader->size;

			// Grab the data
			dataPtr = (void*)newBlock;
		}

		// Otherwise just return this block
		else
		{
			// Get the memory and the footer
			char* poolBlock = ((char*)blockHeader) + sizeof( Header );
			Footer* blockFooter = (Footer*)(poolBlock + blockHeader->size);

			// Handle case that this is the first block
			if ( _freeListHead == blockHeader )
				_freeListHead = blockHeader->next;

			// Remove from the list
			blockHeader->next->prev = blockHeader->prev;
			blockHeader->prev->next = blockHeader->next;

			// Mark as in use
			blockHeader->size |= (1 << 31);
			blockFooter->size |= (1 << 31);
			blockHeader->next = blockHeader->prev = nullptr;

			// Grab the data
			dataPtr = (void*)poolBlock;
		}

		// Thread sync
		if ( _threadShared )
			_mutex.unlock();

		return dataPtr;
	}

	// Free a block of memory that belongs to this heap
	void Heap::Free( void* data )
	{
		// Thread sync
		if ( _threadShared )
			_mutex.lock();

		// Get the header and footer
		Header* header = (Header*)((char*)data - sizeof( Header ));
		header->size &= ~(1 << 31);
		Footer* footer = (Footer*)((char*)data + header->size);
		footer->size &= ~(1 << 31);

		// Get the next and prev blocks
		Footer* prevFooter = (Footer*)((char*)header - sizeof( Footer ));
		Header* nextHeader = (Header*)((char*)footer + sizeof( Footer ));

		// Check right
		if ( !(nextHeader->size & (1 << 31)) && header->next )
		{
			// Remove the block
			Header* temp = header->next;
			temp->next->prev = temp->prev;
			temp->prev->next = temp->next;

			// Merge the blocks
			temp->size &= ~(1 << 31);
			header->size += temp->size;
			footer->size = header->size;
		}

		// Check left
		else if ( !(prevFooter->size & (1 << 31)) && header->prev )
		{
			// Merge
			Header* leftHeader = header->prev;
			leftHeader->size &= ~(1 << 31);
			leftHeader->size += header->size;
			footer->size = header->size;
			return;
		}

		// Add to the free list
		Header* lastHeader = _freeListHead->prev;
		header->next = lastHeader->next;
		header->prev = lastHeader;
		lastHeader->next->prev = header;
		lastHeader->next = header;

		// Thread sync
		if ( _threadShared )
			_mutex.unlock();
	}

}

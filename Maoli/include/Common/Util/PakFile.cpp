//--------------------------------------------------------------------------------------
// File: PakFile.h
//
// PAK File system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "PakFile.h"

#ifdef MAOLI_WINDOWS

#define NOMINMAX
#include <windows.h>

namespace Maoli
{
	// Ctor
	PakFile::PakFile()
	{

	}


	// Dtor
	PakFile::~PakFile()
	{
		Close();
	}


	// Generate a pakfile recursively
	void PakFile::CreatePAK( const char* directory, const char* outFile )
	{
		std::ofstream fout(outFile, std::ios_base::binary);
		if(fout.is_open())
		{
			// Create the header
			MainHeader header;
			header.signature[0] = 'p';
			header.signature[1] = 'a';
			header.signature[2] = 'c';
			header.signature[3] = 'k';


			// Get all the files contained in this folder
			WIN32_FIND_DATAA data;
			String dir = directory;
			dir += '\\';
			HANDLE fh = FindFirstFileA((dir + "*.*").c_str(), &data);
			std::map<String, FileHeader> files;
			int32 offset = sizeof(MainHeader);
			if(fh != INVALID_HANDLE_VALUE)
			{
				do 
				{
					// Force lowercase names
					String validFileName = String(data.cFileName).ToLower();
					if(validFileName.Length() > _maxFileNameLength)
						validFileName = validFileName.Substring(0, _maxFileNameLength-1);
					if(String(validFileName) == "thumbs.db")
						continue;

					// Get the file size
					std::ifstream fin(dir + validFileName, std::ifstream::in | std::ifstream::binary);
					if(fin.is_open())
					{
						fin.seekg(0, std::ifstream::end);

						// Setup the file header
						FileHeader fileHeader;
						strcpy_s(fileHeader.fileName, validFileName);
						fileHeader.fileLength = (int32)fin.tellg();
						fileHeader.fileOffset = offset;
						offset += fileHeader.fileLength;

						// Put into the map
						files.insert( std::pair<String, FileHeader>(validFileName, fileHeader) );
						fin.close();
					}

				} while(FindNextFileA(fh, &data)!=0);
			}

			// Adjust the main header
			header.directoryOffset = offset;
			header.directoryLength = files.size() * sizeof(FileHeader);

			// Write out the header
			fout.write((char*)&header, sizeof(MainHeader));

			// Write each file
			const int32 bufferSize = 1024;
			char buffer[bufferSize];
			for(auto i=files.begin(); i!=files.end(); ++i)
			{
				std::ifstream fin(dir + i->first, std::ifstream::in | std::ifstream::binary);
				int32 bytesWritten = 0;
				while(bytesWritten != i->second.fileLength)
				{
					int32 bytesToRead = std::min(bufferSize, i->second.fileLength-bytesWritten);
					fin.read(buffer, bytesToRead);
					fout.write(buffer, bytesToRead);
					bytesWritten += bytesToRead;
				}
			}

			// Write each directory entry
			for(auto i=files.begin(); i!=files.end(); ++i)
				fout.write((char*)&i->second, sizeof(FileHeader));

			fout.close();
		}
	}

	// Import a pak file
	void PakFile::Load( const char* file )
	{
		_file.open(file, std::ifstream::binary);
		if(_file.is_open())
		{
			// Get access to the directory
			MainHeader header;
			_file.read((char*)&header, sizeof(MainHeader));
			_file.seekg(header.directoryOffset, std::ifstream::beg);

			// Read in each file header
			int32 numFiles = header.directoryLength / sizeof(FileHeader);
			for(int32 i=0; i<numFiles; ++i)
			{
				FileHeader fileHeader;
				_file.read((char*)&fileHeader, sizeof(FileHeader));
				_directory.insert( std::pair<String, FileHeader>(fileHeader.fileName, fileHeader) );
			}
		}
	}

	// Close the pak file
	void PakFile::Close()
	{
		_file.close();
		_directory.clear();
	}


	// Extract a file from the pak
	void PakFile::ExportFile( const char* fileName, const char* outFile )
	{
		auto iter = _directory.find(String(fileName).ToLower());
		if(iter == _directory.end())
			return;

		const FileHeader& header = iter->second;
		std::ofstream fout(outFile, std::ofstream::binary);
		if(fout.is_open())
		{
			// Seek to the file
			_file.seekg(header.fileOffset);

			// Write to file
			const int32 bufferSize = 1024;
			char buffer[bufferSize];
			int32 bytesWritten = 0;
			while(bytesWritten != header.fileLength)
			{
				int32 bytesToRead = std::min(bufferSize, header.fileLength-bytesWritten);
				_file.read(buffer, bytesToRead);
				fout.write(buffer, bytesToRead);
				bytesWritten += bytesToRead;
			}

			fout.close();
		}
	}

	// Extract all files
	void PakFile::ExportAll( const char* location )
	{
		String dir = location ? location : "";
		if(location)
			dir += '\\';
		for(auto i = _directory.begin(); i!=_directory.end(); ++i)
			ExportFile(i->first.c_str(), (dir+i->first).c_str());
	}

	// Load a file into memory
	bool PakFile::GetFile( const char* fileName, void** fileData, int32* fileSize )
	{
		*fileData = nullptr;
		*fileSize = 0;
		auto iter = _directory.find(String(fileName).ToLower());
		if(iter == _directory.end())
			return false;

		// Seek to the file
		const FileHeader& header = iter->second;
		_file.seekg(header.fileOffset);

		// Read in the data
		char* data = new char[header.fileLength];
		_file.read(data, header.fileLength);
		*fileData = (void*)data;
		*fileSize = header.fileLength;

		return true;
	}

}

#endif
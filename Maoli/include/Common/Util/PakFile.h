//--------------------------------------------------------------------------------------
// File: PakFile.h
//
// PAK File system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#ifdef MAOLI_WINDOWS

namespace Maoli
{

	class PakFile
	{
	public:
		PakFile();
		~PakFile();

		// Generate a pakfile recursively
		void CreatePAK(const char* directory, const char* outFile);

		// Import a pak file
		void Load(const char* file);

		// Close the pak file
		void Close();

		// Extract a file from the pak
		void ExportFile(const char* fileName, const char* outFile);

		// Extract all files
		void ExportAll(const char* location);

		// Load a file into memory
		bool GetFile(const char* fileName, void** fileData, int32* fileSize);

	private:

		// Header data
		struct MainHeader
		{
			char signature[4];
			int32 directoryOffset;
			int32 directoryLength;
		};

		// File data
		static const uint32 _maxFileNameLength = 56;
		struct FileHeader
		{
			char fileName[_maxFileNameLength];
			int32 fileOffset;
			int32 fileLength;
		};

		std::ifstream					_file;			// The actual file
		std::map<String, FileHeader>	_directory;		// File headers

	};

}

#endif
//--------------------------------------------------------------------------------------
// File: Timer.cpp
//
// Timer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Timer.h"


namespace Maoli
{

	// Ctor
	Timer::Timer()
	{
		Reset();
	}

	// Start the timer
	void Timer::Reset()
	{
		_startTime = Clock::now();
	}

	// Get the elapsed time in seconds
	float Timer::GetElapsedSeconds()
	{
		auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - _startTime).count();
		return float(ms * 0.001f);
	}

	// Get the elapsed time in milliseconds
	uint32 Timer::GetElapsedMilliseconds()
	{
		return (uint32)std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - _startTime).count();
	}

}

//--------------------------------------------------------------------------------------
// File: DataType.h
//
// File based resource with self management
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Engine;

	template <typename T, typename ParentType>
	class DataType
	{
	public:

		// Get the data ID
		inline uint32 GetID() const { return _id; }

		// Get the name
		inline const String& GetName() const { return _name; }

		// Get the file path
		inline const String& GetFilePath() const { return _filePath; }

		// Get the ref count of data instance
		inline uint32 GetRefCount() const { return _refCount; }

		// Check if the resource was loaded from file
		inline bool FileBased() const { return _fileBased; }

		// Get the number of data resources allocated
		static inline uint32 GetCount() { return _resources.size(); }

		// Access a resource by index
		static inline T* Get(int32 i)
		{ 
			int32 count = 0;
			for ( auto iter = _resources.begin(); iter != _resources.end(); ++iter, ++count )
			{
				if ( count == i )
					return iter->second;
			}
			return nullptr;
		}


		// Create a new type by name
		static T* Create( const char* name, ParentType* parent )
		{
			auto iter = _resources.find( name );
			T* resource = nullptr;
			if ( iter == _resources.end() )
			{
				resource = new T( parent );
				resource->_id = _idCounter++;
				resource->_name = name;
				resource->_filePath = String::none;
				_resources.emplace( resource->_name, resource );
				//std::cout << "Creating: " << resource->GetName() << std::endl;
			}
			else
			{
				resource = iter->second;
			}
			resource->_fileBased = false;
			++resource->_refCount;
			return resource;
		}

		// Create from file
		static T* CreateFromFile( const char* fileName, ParentType* parent )
		{
			String name = String( fileName ).GetFile().RemoveFileExtension();
			auto iter = _resources.find( name );
			T* resource = nullptr;
			if ( iter == _resources.end() )
			{
				resource = new T( parent );
				if ( !resource->Load( fileName ) )
				{
					delete resource;
					return nullptr;
				}
				resource->_id = _idCounter++;
				resource->_name = name;
				resource->_filePath = fileName;
				_resources.emplace( resource->_name, resource );
				//std::cout << "Loading: " << resource->GetName() << std::endl;
			}
			else
			{
				resource = iter->second;
			}
			resource->_fileBased = true;
			++resource->_refCount;
			return resource;
		}

		// Rename the data
		bool Rename(const char* newName)
		{
			// Make sure the name is not in use
			auto iter = _resources.find( newName );
			if ( iter != _resources.end() )
				return false;

			// Re-insert the data
			iter = _resources.find( _name );
			if(iter != _resources.end())
				_resources.erase( iter );
			_name = newName;
			_resources.emplace( _name, (T*)this );

			return true;
		}

		// Update the file path
		bool UpdateFilePath(const char* filePath)
		{
			String newName = String( filePath ).GetFile().RemoveFileExtension();
			if ( newName != _name )
			{
				if ( !Rename( newName ) )
					return false;
			}
			_filePath = filePath;
			_fileBased = true;

			return true;
		}

		// Dereference a type
		static bool Delete( T*& type )
		{
			if ( type && --type->_refCount == 0 )
			{
				//std::cout << "Deleting: " << type->GetName() << std::endl;
				auto iter = _resources.find( type->GetName() );
				_resources.erase( iter );
				delete type;
				type = nullptr;
				return true;
			}
			return false;
		}

		// Delete all resources
		static void Shutdown()
		{
			if ( _resources.size() > 0 )
			{
				for ( auto iter = _resources.begin(); iter != _resources.end(); ++iter )
				{
					//std::cout << "Resource not freed: " << iter->second->GetName() << std::endl;
					delete iter->second;
				}
				_resources.clear();
				_idCounter = 0;
			}
		}

		// Update the file path
		inline void ChangeFilePath( const char* file ) { _filePath = file; }

	protected:

		// Ctor
		DataType( ParentType* parent ) : _parent( parent ), _refCount( 0 )
		{
			_filePath = String::none;
			_fileBased = false;
		}

		// Dtor
		virtual ~DataType()
		{

		}

		ParentType*							_parent;		// Parent object

	private:

		String								_name;			// Data name
		String								_filePath;		// Full file path
		uint32								_refCount;		// Reference tracking
		uint32								_id;			// Unique id
		bool								_fileBased;		// True if loaded from file
		static std::map<const String, T*>	_resources;		// Shared map of all active data
		static uint32							_idCounter;		// ID counting for unique ID's
	};

	// Shared map of all active data
	template <typename T, typename ParentType>
	std::map<const String, T*> Maoli::DataType<T, ParentType>::_resources;

	// ID counting for unique ID's
	template <typename T, typename ParentType>
	uint32 Maoli::DataType<T, ParentType>::_idCounter = 0;

}




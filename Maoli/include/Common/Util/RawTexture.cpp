//--------------------------------------------------------------------------------------
// File: RawTexture.cpp
//
// Filed based byte array texture
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Util/RawTexture.h"
#include "Graphics/Texture.h"
#include "Graphics/Renderer.h"

namespace Maoli
{
	// Reads the entire contents of the file
	// --------------------------------------------------------------------------------------
	bool RawTexture::Load(const char* fileName)
	{
		return BinaryFile::Load(fileName);
	}

	// Load dds texture
	// --------------------------------------------------------------------------------------
	bool RawTexture::LoadDDS(const char* fileName, uint32 width, uint32 height)
	{
		if(_data==nullptr)
		{
			_fileSize = width*height*4;
			_data = new byte[_fileSize];
		}
		return _graphics->StageDDS(fileName, _data, width, height);
	}

	// Save to file
	// --------------------------------------------------------------------------------------
	void RawTexture::Save(const char* fileName)
	{
		std::ofstream file(fileName, std::ios_base::binary);
		if(!file.is_open())
			return;

		// Write the data
		file.write(reinterpret_cast<char*>(_data), _fileSize);

		file.close();
	}


	// Ctor
	RawTexture::RawTexture( Engine* engine )
	{
		_engine = engine;
		_graphics = _engine->GetGraphics();
	}


	// Allocate a buffer ahead of time
	void RawTexture::Allocate( uint32 size )
	{
		_data = new byte[size]; _fileSize = size;
	}

}

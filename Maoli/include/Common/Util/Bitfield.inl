//--------------------------------------------------------------------------------------
// File: Bitfield.inl
//
// Variable sized bitfields
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	template <typename T>
	Bitfield<T>::Bitfield() : _bits(0)
	{

	}


	// Turn a bit on
	template <typename T>
	inline void Bitfield<T>::Set( int32 index, bool on /*= true*/ )
	{
		if(on)
			_bits |= (1<<index);
		else
			_bits &= ~(1<<index);
	}
	
	// Turn a bit off
	template <typename T>
	inline void Bitfield<T>::Unset( int32 index )
	{
		_bits &= ~(1<<index);
	}

	// Toggle
	template <typename T>
	inline void Bitfield<T>::Toggle( int32 index )
	{
		_bits ^= (1<<index);
	}

	// Check if a bit is set
	template <typename T>
	bool Bitfield<T>::Check( int32 index )
	{
		return (_bits&(1<<index)) != 0;
	}

	// Array style access for bit checking
	template <typename T>
	bool Bitfield<T>::operator[]( int32 index )
	{
		return (_bits&(1<<index)) != 0;
	}

	// Directly set bits
	template <typename T>
	T& Bitfield<T>::operator|=( int32 bitValue )
	{
		return _bits|=bitValue;
	}

	// Directly check bits
	template <typename T>
	bool Bitfield<T>::operator&( int32 bitValue )
	{
		return (_bits&bitValue)!=0;
	}

	// Directly Toggle bits
	template <typename T>
	T& Bitfield<T>::operator^=( int32 bitValue )
	{
		return _bits^=bitValue;
	}

	// Get the raw bits
	template <typename T>
	const T& Bitfield<T>::GetBits() const
	{
		return _bits;
	}

}

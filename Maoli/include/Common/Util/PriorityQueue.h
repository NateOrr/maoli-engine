//---------------------------------------------------------------------------------------
// File: PriorityQueue.h
//
// PriorityQueue ADT
//
// Nate Orr
//---------------------------------------------------------------------------------------
#pragma once

#include "List.h"

namespace Maoli
{
	template <typename T>
	class PriorityQueue : protected List<T>
	{
		// Type compare function
		typedef bool( *GreaterFn )(T const&, T const&);

	public:

		// Ctor
		PriorityQueue( GreaterFn compareFn );

		// Dtor
		~PriorityQueue();

		// Enqueue
		void Enqueue( const T& obj );

		// Dequeue
		T Dequeue();

		// Peek the front
		T& Front() const;

		// True if empty
		bool IsEmpty() const;

		// Get the number of elements
		uint32 Size() const;

		// Clear the queue
		void Clear();

		// Free memory
		void Release();

	private:
		GreaterFn	_compareFn;
	};


}

#include "PriorityQueue.inl"

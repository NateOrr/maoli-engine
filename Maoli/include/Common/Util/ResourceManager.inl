//--------------------------------------------------------------------------------------
// File: ResourceManager.inl
//
// Template resource manager class
// Handles file-based resource objects
//
// Any class to be used with a ResourceManager MUST inherit from class EngineResource! 
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{
	// Create an new, empty resource.  up to the app to initialize
	template <typename ManagerType, typename ResourceType>
	Pointer<ResourceType> ResourceManager<ManagerType, ResourceType>::Create( const char* name )
	{
		// Handle already loaded resources
		auto loadedRes = Get( name );
		if ( loadedRes )
			return loadedRes;

		// Create a uniquely named resource
		Pointer<ResourceType> res( new ResourceType( _parent ) );
		res->_name = name ? name : "NewResource";
		Add( res );
		return res;
	}


	// Retrieves an object pointer from the list, otherwise returns nullptr
	template <typename ManagerType, typename ResourceType>
	Pointer<ResourceType> ResourceManager<ManagerType, ResourceType>::Get( const char* name ) const
	{
		if ( !name )
			return Pointer<ResourceType>( nullptr );

		// Iterate through the list
		for ( auto i = _resources.Begin(); !i.End(); ++i )
		{
			// Check if the filenames match
			Pointer<ResourceType>& resource = *i;
			if ( resource->GetName() == name )
				return resource;
		}

		// ResourceType is not in the list so return nullptr
		return Pointer<ResourceType>( nullptr );
	}


	// Adds an object to the manager if it's not already in it
	template <typename ManagerType, typename ResourceType>
	void ResourceManager<ManagerType, ResourceType>::Add( Pointer<ResourceType>& resource )
	{
		if ( !resource )
			return;

		// Make sure it's not already in the list
		if ( !Get( resource->GetName() ) )
		{
			// Add it to the list
			_resources.Add( resource );
			resource._managed = true;
			resource._releaseCallback = std::bind(&ResourceManager<ManagerType, ResourceType>::Unload, this, std::placeholders::_1);
		}
	}


	// Removes a resource
	template <typename ManagerType, typename ResourceType>
	void ResourceManager<ManagerType, ResourceType>::Unload( Pointer<ResourceType>& resource )
	{
		_resources.Remove( resource );
	}


	// Loads an object from a file into the manager if it's not already in it
	template <typename ManagerType, typename ResourceType>
	Pointer<ResourceType> ResourceManager<ManagerType, ResourceType>::Load( const char* file )
	{
		if ( !file )
			return Pointer<ResourceType>( nullptr );

		// Make sure it's not already in the manager
		Pointer<ResourceType> pObjPtr = Get( file );
		if ( !pObjPtr )
		{
			// Validate the file path
			if ( !FileExists( file ) )
				return Pointer<ResourceType>( nullptr );

			// Make a new object
			Pointer<ResourceType> res( new ResourceType( _parent ) );

			// Load the file
			if ( !res->Load( file ) )
			{
				res = nullptr;
				return res;
			}

			// Add it to the list
			Add( res );
			return res;
		}

		return pObjPtr;
	}

	// Reloads the resource
	template <typename ManagerType, typename ResourceType>
	void ResourceManager<ManagerType, ResourceType>::Reload( Pointer<ResourceType>& resource )
	{
		String szName = resource->GetName();
		resource->Release();
		resource->Load( szName );
	}


	// Releases all resources
	template <typename ManagerType, typename ResourceType>
	void ResourceManager<ManagerType, ResourceType>::Release()
	{
		// Itterate through the list
		for ( auto i = _resources.Begin(); !i.End(); ++i )
		{
			Pointer<ResourceType>& resource = *i;
			resource._managed = false;
			resource = nullptr;
		}

		// Release the linked list
		_resources.Release();
	}
}

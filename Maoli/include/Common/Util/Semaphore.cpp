//--------------------------------------------------------------------------------------
// File: Semaphore.cpp
//
// Semaphore threading primitive
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Semaphore.h"

#include <condition_variable>


// Ctor
Semaphore::Semaphore() : _count(0)
{
	_mutex = new std::mutex;
	_conVar = new std::condition_variable;
}

// Dtor
Semaphore::~Semaphore()
{
	delete _mutex;
	delete _conVar;
}

// Wait for resources
void Semaphore::Wait()
{
	std::unique_lock<std::mutex> lock(*_mutex);
	_conVar->wait(lock, [&](){ return _count != 0; });
	--_count;
}

// Let waiting threads know that resources are free
void Semaphore::Post()
{
	std::unique_lock<std::mutex> lock(*_mutex);
	++_count;
	_conVar->notify_all();
}



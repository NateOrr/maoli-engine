//--------------------------------------------------------------------------------------
// File: Resource.h
//
// Managed resource (see note below)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// An object that can be handled by the resource manager.
	// Classes that inherit from this should never be instantiated directly,
	// they should be obtained from the managing app object.
	//
	//
	// Derived classes should define a public typedef Pointer<T> pointer; for their type.
	// Derived classes MUST have private or protected constructors and destructors.
	// This will enforce the use of Pointer<>, and allow the resource managers
	// to do their job correctly.
	template <typename T>
	class Resource
	{
		template <typename F, typename G>
		friend class ResourceManager;
		
		template <typename G>
		friend class Pointer;

	public:
		
		// Gets the name
		inline const String& GetName() const { return _name; }

		// Get the id
		inline int32 GetID() const { return _id; }

		// Get the parent object
		inline T* GetParent() const { return _parent; }

		// Loads from a file
		virtual bool Load(const char* file){ return true; }

	protected:

		// Ctor
		Resource( T* parent ) : _name( "EngineResource" ), _parent( parent ), _refCount( 0 ) {}
		
		// Dtor
		virtual ~Resource(){}

		// Cleanup
		virtual void Release() = 0;

		String	_name;			// Name
		T*		_parent;		// D3D object that created the resource
		int32		_refCount;		// Ref counter for smart pointers
		int32		_id;			// Resource id
	};

}

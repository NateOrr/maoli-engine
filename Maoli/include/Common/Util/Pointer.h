//---------------------------------------------------------------------------------------
// File: Pointer.h
//
// Reference tracked, shared smart pointer
//
// This is designed specifically with the Maoli resource system in mind,
// but should be generic enough to use in any circumstance.
//
// Nate Orr
//---------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	// Standard smart pointer
	template <typename T>
	class Pointer
	{
		// Callback form
		template <typename F, typename G>
		friend class ResourceManager;
		typedef std::function<void( Pointer<T>& )> Callback;

	public:

		// Empty pointer
		Pointer();

		// Managed pointer
		explicit Pointer( T* obj );

		// Copy ctor
		Pointer( const Pointer& p );

		// Dtor
		~Pointer();


		// Assignment to another pointer
		Pointer& operator=(const Pointer& p);

		// Assignment to nullptr
		Pointer& operator=(std::nullptr_t null);

		// Comparison with nullptr
		bool operator==(std::nullptr_t null) const;

		// Comparison with nullptr
		bool operator!=(std::nullptr_t null) const;

		// Comparison
		bool operator==(const Pointer& p) const;

		// Comparison
		bool operator!=(const Pointer& p) const;

		// Auto boolean
		operator bool() const;

		// Logical not
		bool operator!() const;

		// Object access
		T* operator*();

		// Object access
		T* operator->();

		// const Object access
		const T* operator->() const;

		// Get the reference count
		int32 GetReferenceCount() const;

	private:

		// Increase ref count
		void AddRef();

		// Decrease ref cout, delete object when it reaches 0
		void Release();


		// This needs to be dynamically allocated in order for sharing to work
		struct Tracker
		{
			int32		refCount;		// Current reference count
			T*		object;			// The managed object
		};

		// Prevent bracket access
		T& operator[]( int32 );

		// Assignment to a raw pointer is not allowed.
		Pointer& operator=(T*);

		T*		_object;		// Managed object

		// Callback for reference decrements
		Callback _releaseCallback;
		bool	 _managed;
	};

}

#include "Pointer.inl"

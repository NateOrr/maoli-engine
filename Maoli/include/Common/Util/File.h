//--------------------------------------------------------------------------------------
// File: File.h
//
// Binary file IO
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	class File
	{
	public:

		// Ctor
		File( const String& fileName );
		File( char* buffer );

		// Dtor
		~File();

		// Open the file
		bool Open( const String& fileName );

		// Set the IO buffer
		void SetBuffer( char* buffer );

		// Close the fole
		void Close();

	private:

		std::fstream	_file;
		char*			_buffer;
	};
}
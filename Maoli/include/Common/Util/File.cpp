//--------------------------------------------------------------------------------------
// File: File.cpp
//
// Binary file IO
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "File.h"

namespace Maoli
{


	// Ctor
	File::File( const String& fileName ) : _file( fileName )
	{
		_buffer = nullptr;
	}

	// Ctor
	File::File( char* buffer )
	{
		_buffer = buffer;
	}

	// Dtor
	File::~File()
	{

	}

	// Open the file
	bool File::Open( const String& fileName )
	{
		_file.open( fileName );
		_buffer = nullptr;
		return _file.is_open();
	}

	// Close the fole
	void File::Close()
	{
		_file.close();
		_buffer = nullptr;
	}

	// Set the IO buffer
	void File::SetBuffer( char* buffer )
	{
		_buffer = buffer;
	}

}
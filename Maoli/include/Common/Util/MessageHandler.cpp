//--------------------------------------------------------------------------------------
// File: MessageHandler.cpp
//
// For passing global messages
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "MessageHandler.h"

namespace Maoli
{

	// Ctor
	MessageHandler::MessageHandler()
	{
		_messageIndex = 0;
	}

	// Gets the next available message
	Message MessageHandler::GetNextMessage()
	{
		if(_messageIndex>=_messages.Size())
		{
			if(_messages.Size()>0)
				_messages.Clear();
			_messageIndex = 0;

			Message m;
			m.msg = 0;
			return m;
		}

		return _messages[_messageIndex++];
	}


	// Clear all messages
	void MessageHandler::Flush()
	{
		_messages.Release();
	}

}

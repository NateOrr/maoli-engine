//---------------------------------------------------------------------------------------
// File: PriorityQueue.inl
//
// PriorityQueue with discrete priority levels
//
// Nate Orr
//---------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	template <typename T, int32 _priorityLevels, typename PriorityType>
	Maoli::DiscretePriorityQueue<T, _priorityLevels, PriorityType>::DiscretePriorityQueue() : _topQueue(&_queues[0]), _totalElements(0)
	{
		for(int32 i=0; i<_priorityLevels; ++i)
			_queues[i].priority = i;
	}

	// Dtor
	template <typename T, int32 _priorityLevels, typename PriorityType>
	DiscretePriorityQueue<T, _priorityLevels, PriorityType>::~DiscretePriorityQueue()
	{
		Release();
	}


	// Assigns _topQueue to the highest available level
	template <typename T, int32 _priorityLevels, typename PriorityType>
	inline void Maoli::DiscretePriorityQueue<T, _priorityLevels, PriorityType>::ValidateTopQueue()
	{
		if(_topQueue->list.IsEmpty())
		{
			for(int32 i=_topQueue->priority-1; i>=0; --i)
			{
				if(!_queues[i].list.IsEmpty())
				{
					_topQueue = &_queues[i];
					return;
				}
			}
			_topQueue = &_queues[0];
		}
	}


	// Enqueue
	template <typename T, int32 _priorityLevels, typename PriorityType>
	void DiscretePriorityQueue<T, _priorityLevels, PriorityType>::Enqueue(const T& obj, PriorityType priority)
	{
		M_ASSERT((int32)priority < _priorityLevels, "priority must be less than the number of levels");
		_queues[(int32)priority].list.Add(obj);
		if(_topQueue->priority < (int32)priority)
			_topQueue = &_queues[(int32)priority];
		++_totalElements;
	}

	// Dequeue
	template <typename T, int32 _priorityLevels, typename PriorityType>
	T DiscretePriorityQueue<T, _priorityLevels, PriorityType>::Dequeue()
	{
		M_ASSERT(_totalElements, "Cannot Dequeue from an emtpty PriorityQueue!");
		--_totalElements;
		T&& data = _topQueue->list.RemoveFirst();
		ValidateTopQueue();
		return data;
	}

	// Peek the front
	template <typename T, int32 _priorityLevels, typename PriorityType>
	inline T& DiscretePriorityQueue<T, _priorityLevels, PriorityType>::Front() const
	{
		M_ASSERT(_totalElements, "Cannot peek from an emtpty PriorityQueue!");
		return _topQueue->list.GetFirst();
	}

	// True if empty
	template <typename T, int32 _priorityLevels, typename PriorityType>
	inline bool DiscretePriorityQueue<T, _priorityLevels, PriorityType>::IsEmpty() const
	{
		return (_totalElements==0);
	}

	// Get the number of elements
	template <typename T, int32 _priorityLevels, typename PriorityType>
	inline uint32 DiscretePriorityQueue<T, _priorityLevels, PriorityType>::Size() const
	{
		return _totalElements;
	}

	// Free memory
	template <typename T, int32 _priorityLevels, typename PriorityType>
	void DiscretePriorityQueue<T, _priorityLevels, PriorityType>::Release()
	{
		for(int32 i=0; i<_priorityLevels; ++i)
			_queues[i].list.Release();
	}

	
}

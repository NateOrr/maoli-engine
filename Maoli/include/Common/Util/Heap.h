//--------------------------------------------------------------------------------------
// File: Heap.h
//
// A block of memory
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "String.h"
#include <thread>
#include <mutex>

namespace Maoli
{
	// Custom memory heap.
	// Blocks are stored in the free list using a cyclic linked list
	class Heap
	{
		friend class MemoryManager;

	public:

		// Ctor
		Heap();

		// Setup
		bool Init(uint32 poolSize, bool threadShared = false, const String& name = "HEAP");

		// Free mem
		void Release();

		// Allocate memory from the pool, returns null if not enough memory is available
		void* Allocate(uint32 size);

		// Free a block of memory that belongs to this heap
		void Free(void* data);

		// Get the size of the memory pool
		inline uint32 GetSize() const { return _size; }

		// Get the thread id
		inline const std::thread::id& GetThreadID() const { return _threadID; }

		// Get the heap id
		inline uint32 GetID() const { return _heapID; }

		// Get the name
		inline const String& GetName() const { return _name; }


	private:

		// Block header
		struct Header
		{
			uint32	size;	// Size of the block, high order bit determines if the block is in use
			Header* prev;	// Previous header in the free list
			Header* next;	// Next header in the free list
		};

		// Block footer
		struct Footer
		{
			uint32	size;	// Size of the block, high order bit determines if the block is in use
		};

		// Find a free block of memory that can store the requested size
		Header* FindBlock(uint32 size);

		
		char*				_pool;				// Memory block for the heap
		uint32				_size;				// Size of the pool
		Header*				_freeListHead;		// First node in the free list
		Header*				_firstHeader;		// First Header within the heap
		Footer*				_lastFooter;		// Last Footer within the heap
		bool				_threadShared;		// True for thread safety (used for heaps shared between threads)
		std::mutex			_mutex;				// Mutex for thread sync
		String				_name;				// Name ID
		std::thread::id		_threadID;			// ID of the thread that created this heap
		uint32				_heapID;			// Global ID of this heap
	};

}

//--------------------------------------------------------------------------------------
// File: Stack.inl
//
// Stack class
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{

	// Ctor
	template <typename T>
	Stack<T>::Stack()
	{ 
		_data = nullptr; 
		_capacity = 0; 
		_size=-1;
	}


	// Copy ctor
	template <typename T>
	Stack<T>::Stack(const Stack<T>& s)
	{
		_data = nullptr; 
		_capacity = 0; 
		_size=-1;
		*this = s;
	}


	// Assignment op
	template <typename T>
	Stack<T>& Stack<T>::operator=(const Stack<T>& s)
	{
		if(this != &s)
		{
			Release();
			_size = s._size;
			_capacity = s._capacity;
			_data = new T[_capacity];
			for(int32 i=0; i<=_size; ++i)
				_data[i] = s._data[i];
		}
		return *this;
	}


	// Dtor
	template <typename T>
	Stack<T>::~Stack()
	{ 
		Release();
	}

	// Push data onto the stack
	template <typename T>
	void Stack<T>::Push(const T& t)
	{ 
		++_size;
		if(_size==_capacity)
			Resize();
		_data[_size] = t;
	}


	// Pop data from the stack
	template <typename T>
	inline T& Stack<T>::Pop()
	{ 
		M_ASSERT(_size>=0 , "Cannot pop from an empty stack");
		return _data[_size--]; 
	}

	// Peek the top element
	template <typename T>
	inline T& Stack<T>::Top()
	{
		return _data[_size];
	}

	// Is the stack empty?
	template <typename T>
	inline bool Stack<T>::IsEmpty() const 
	{ 
		return (_size<0); 
	}

	// Return the size of the stack
	template <typename T>
	inline int32 Stack<T>::Size() const
	{ 
		return _size+1; 
	}

	// Free all mem and create an empty stack
	template <typename T>
	void Stack<T>::Release()
	{ 
		SafeDeleteArray(_data); 
		_capacity = 0; 
		_size=-1; 
	}

	// Free all mem, delete all data and create an empty stack
	template <typename T>
	void Stack<T>::ReleaseAndDelete()
	{ 
		for(int32 i=0; i<=_size; ++i) 
			delete _data[i]; 
		Release();
	}

	// Resize the stack to double its current capacity
	template <typename T>
	void Stack<T>::Resize()
	{
		if(_capacity==0)
		{
			_capacity = 10; 
			_data = new T[_capacity]; 
			 return;
		}
		T* pTemp = new T[_capacity*2];
		memcpy(pTemp,_data,_capacity*sizeof(T));
		delete[] _data;
		_data = pTemp;
		_capacity*=2;
	}

}

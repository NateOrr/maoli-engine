//---------------------------------------------------------------------------------------
// File: Queue.h
//
// Queue ADT
//
// Nate Orr
//---------------------------------------------------------------------------------------
#pragma once

#include "List.h"

namespace Maoli
{
	template <typename T>
	class Queue : protected List<T>
	{
	public:

		// Enqueue
		void Enqueue(const T& obj);

		// Dequeue
		T Dequeue();

		// Peek the front
		T& Front() const;

		// True if empty
		bool IsEmpty() const;

		// Get the number of elements
		uint32 Size() const;

		// Clear the queue
		void Clear();

		// Free memory
		void Release();

		// Dtor
		~Queue();
	};

}

#include "Queue.inl"

//--------------------------------------------------------------------------------------
// File: Array.h
//
// Growable array class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	template <typename T>
	class Array : public ISerializable
	{
	public:
		// Ctor
		Array(int32 size=0);

		// Copy ctor
		// todo: move ctor
		Array(const Array& a);

		// Assignment op
		Array& operator=(const Array& a);

		// Dtor
		~Array();


		// Write to binary file
		void Serialize(std::ofstream& file) const;

		// Read from binary file
		void Deserialize(std::ifstream& file);

		// Frees all mem and makes an empty array
		void Release();

		// Resets the array without freeing mem
		void Clear();

		// Memset to 0
		void Zero();


		// Bracket [] style access
		T& operator[](int32 i);

		// Const Bracket [] style access
		const T& operator[](int32 i) const;


		// Adds an object to the end of the array
		void Add( const T& t );

		// Adds an empty object to the end of the array
		T& Add();

		// Copy a range of data to the end of the array
		void AddRange( const T* data, uint32 count );

		// Inserts an object at [index]
		void Insert(const T& t, uint32 index);


		// Removes the object at [index]
		T RemoveAt(uint32 index);

		// Removes the first occurance of the given object
		bool Remove(const T& t);

		// Removes all occurances of the given object
		bool RemoveAll(const T& t);


		// Check if the array contains an element
		bool Contains( const T& t ) const;

		// Is the array full?
		bool IsFull() const;

		// Is the array empty?
		bool IsEmpty() const;

		// Returns the size of the array
		uint32 Size() const;

		// Returns the max capacity of the array
		uint32 Capacity() const;

		// Convert to c-style array/pointer automatically
		operator T*(); 

		// Convert to const c-style array/pointer automatically
		operator const T*() const; 


		// Create using an array
		void FromArray(const T* pData, uint32 iLength);

		// Create from a heap array and handle the deletion
		void TakeArray(T* pData, uint32 iLength);

		// Pre-allocates memory, objects are actually "added" to the array
		void Allocate(uint32 size, bool zero = false);

		// Reserve a certain capacity, objects are NOT added here
		void Reserve(uint32 size);

		// Remove unused memory from the end of the array
		void Finalize();


		// Sort the elements in ascending order
		// (NOTE: uses the < operator so overload if nessecary)
		void Sort();

		// Get raw data pointer (use at own risk)
		void* GetRawData();

	private:

		// Grows the array		
		void Resize();

		T*		_data;		// Array data
		uint32	_size;		// Size of current array
		uint32	_capacity;	// Capacity
	};

}

// Print to a stream
template <typename T>
std::ostream& operator<<( std::ostream& os, const Maoli::Array<T>& a );

#include "Array.inl"

//--------------------------------------------------------------------------------------
// File: QString.h
//
// FixedString class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	// Todo:
	// < and > operators

	// Stores a string
	template <typename CharType, int32 _maxLength>
	class FixedString : public ISerializable
	{
	public:
		FixedString();
		FixedString(const CharType* str);	
		FixedString(const CharType c);
		FixedString(const FixedString& str);
		~FixedString();

		// Copy constructor
		FixedString& operator=( const FixedString& str );
		FixedString& operator=( const CharType* str );
		FixedString& operator=( const CharType c);

		// Comparison
		bool operator==( const CharType* str ) const;
		bool operator==( const FixedString& str ) const;
		bool operator!=( const CharType* str ) const;
		bool operator!=( const FixedString& str ) const;
		bool operator<(const FixedString& str) const;
		bool operator<=(const FixedString& str) const;
		bool operator>(const FixedString& str) const;
		bool operator>=(const FixedString& str) const;


		// Gets a CharType at location i
		inline CharType& operator[](int32 i){ return _str[i]; }

		// Concatenations
		FixedString& operator+=( const FixedString& str );
		FixedString& operator+=( const CharType* str );
		FixedString& operator+=( int32 i );
		FixedString& operator+=( uint32 i );
		FixedString& operator+=( float f );
		FixedString& operator+=( double d );
		FixedString& operator+=( CharType c );	

		// Addition
		template <typename T>
		FixedString operator+( const T& v ) const;

		// Stream style concatenation
		template <typename T>
		FixedString& operator<<( const T& v );

		// Get the CharType array
		const CharType* c_str() const;

		// Gets the size of the string
		int32 Length() const;

		// Sets the floating point precision
		void SetFloatPrecision(byte i);

		// Automatic type casting
		operator const CharType* () const;

		// Returns a substring
		FixedString Substring(int32 s, int32 e) const;

		// Removes array tags
		FixedString RemoveArrayTags(int32* pIndex = nullptr) const;

		// Chops off the main directory
		FixedString ChopDirectory(const FixedString& dir) const;

		// Removes the .* extension from a string
		FixedString RemoveFileExtension() const;

		// Returns the containing directory
		FixedString GetDirectory() const;
		FixedString GetPreviousDirectory() const;
		FixedString GetFile() const;

		// Get the file extension
		FixedString GetFileExtension() const;

		// Write to binary file
		void Serialize(std::ofstream& file) const;

		// Read in from binary file
		void Deserialize(std::ifstream& file);

		// Make lowercase
		FixedString& MakeLowercase();

		// Get a lowercase version of this string
		FixedString ToLower() const;

		// Fix slashes in the file path
		FixedString& FixFilePath();

		// Check if a string contains a character
		bool Contains( CharType c ) const;

		// Check if a string contains a substring
		bool Contains( const FixedString& str ) const;

		// Replace a character with another character
		FixedString& Replace( CharType character, CharType newCharacter );

		// Replace a character with a string
		FixedString& Replace( CharType character, const FixedString& str );

		// Replace a portion of a string with a string
		FixedString& Replace( const FixedString& str, const FixedString& newStr );


	protected:
		CharType		 _str[_maxLength];	// String data
		int32				 _length;			// Number of characters + nullptr terminator
		byte			 _precision;		// Number of decimal places floats contain

	public:
		// Default strings used by the engine
		static FixedString none;
	};


	// Default string types
	typedef FixedString<char, 256> String;
	typedef FixedString<wchar_t, 256> WString;

}

#include "String.inl"

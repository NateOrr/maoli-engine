//--------------------------------------------------------------------------------------
// File: FixedString.cpp
//
// FixedString class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "String.h"
#include <stdarg.h>

namespace Maoli
{
	// Default strings used by the engine
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::none( "None" );


	// Default constructor
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>::FixedString()
	{
		_str[0] = nullchar;
		_length = 0;
		_precision = 4;
	}

	// Constructor from a CharType
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>::FixedString( const CharType c )
	{
		_str[0] = nullchar;
		_length = 0;
		_precision = 4;
		*this = c;
	}


	// Constructor from a CharType array
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>::FixedString( const CharType* str )
	{
		_str[0] = nullchar;
		_length = 0;
		_precision = 4;
		*this = str;
	}

	// Constructor from another FixedString
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>::FixedString( const FixedString<CharType, _maxLength>& str )
	{
		_str[0] = nullchar;
		_length = 0;
		_precision = 4;
		*this = str;
	}

	// Copy constructor from a CharType array
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator=(const CharType* str)
	{
		_length = (int32)strlen( str );
		memcpy( _str, str, _length );
		_str[_length] = nullchar;
		return *this;
	}

	// Copy constructor from a CharType
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator=(const CharType c)
	{
		_length = 1;
		_str[0] = c;
		_str[1] = nullchar;
		return *this;
	}

	// Copy constructor from another FixedString
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator=(const FixedString<CharType, _maxLength>& str)
	{
		if ( this != &str )
		{
			*this = str.c_str();
			_precision = str._precision;
		}
		return *this;
	}

	// Destructor
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>::~FixedString()
	{
		_str[0] = nullchar;
	}



	// Get the CharType array
	template <typename CharType, int32 _maxLength>
	inline const CharType* FixedString<CharType, _maxLength>::c_str() const
	{
		return _str;
	}

	// Gets the size of the string
	template <typename CharType, int32 _maxLength>
	inline int32 FixedString<CharType, _maxLength>::Length() const
	{
		return _length;
	}

	// Sets the floating point precision
	template <typename CharType, int32 _maxLength>
	inline void FixedString<CharType, _maxLength>::SetFloatPrecision( byte i )
	{
		_precision = i;
	}

	// Automatic type casting
	template <typename CharType, int32 _maxLength>
	inline FixedString<CharType, _maxLength>::operator const CharType* () const
	{
		return _str;
	}



	// Returns a substring
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::Substring( int32 s, int32 e ) const
	{
		// Make a string object and copy the CharType array into it
		FixedString sz;
		sz._precision = _precision;
		sz._length = e - s;
		memcpy( sz._str, _str + s, e - s );
		sz._str[sz._length] = nullchar;
		return sz;
	}

	// Removes array tags
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::RemoveArrayTags( int32* pIndex ) const
	{
		for ( int32 i = 0; i < _length; ++i )
		{
			// Check for a bracket
			if ( _str[i] == '[' )
			{
				// Grab the integer index if needed
				if ( pIndex )
				{
					CharType digits[16];
					int32 di = 0;
					int32 si = di + i;
					while ( _str[++si] != ']' && di < 15 && si < _length )
						digits[di++] = _str[si];
					digits[di] = nullchar;
					*pIndex = atoi( digits );
				}

				// Chop off the array tag
				return Substring( 0, i );
			}
		}
		if ( pIndex )
			*pIndex = -1;
		return *this;
	}

	// Write to binary file
	template <typename CharType, int32 _maxLength>
	void FixedString<CharType, _maxLength>::Serialize( std::ofstream& file ) const
	{
		// Send the string length, then the string
		file.write( reinterpret_cast<const CharType*>(&_length), sizeof(int32) );
		file.write( _str, _length + 1 );
	}


	// Read in from binary file
	template <typename CharType, int32 _maxLength>
	void FixedString<CharType, _maxLength>::Deserialize( std::ifstream& file )
	{
		// Read the length, then the string
		file.read( reinterpret_cast<CharType*>(&_length), sizeof(int32) );
		file.read( _str, _length + 1 );
	}


	// Comparison with a c string
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator==(const CharType* str) const
	{
		int32 len = strlen( str );
		if ( _length != len )
			return false;
		for ( int32 i = 0; i <= len; ++i )
		{
			if ( _str[i] != str[i] )
				return false;
		}
		return true;
	}

	// Comparison with a string
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator==(const FixedString<CharType, _maxLength>& str) const
	{
		if ( _length != str._length )
			return false;
		for ( int32 i = 0; i < _length; ++i )
		{
			if ( _str[i] != str[i] )
				return false;
		}
		return true;
	}

	// Comparison with a c string
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator!=(const CharType* str) const
	{
		return !(*this == str);
	}

	// Comparison with a string
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator!=(const FixedString<CharType, _maxLength>& str) const
	{
		return !(*this == str);
	}


	// Concats with a CharType array
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator+=(const CharType* str)
	{
		int32 len = (int32)strlen( str );
		int32 len2 = _length;
		_length += len;
		memcpy( _str + len2, str, len );
		_str[_length] = nullchar;
		return *this;
	}

	// Concats with another string
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator+=(const FixedString<CharType, _maxLength>& anotherStr)
	{
		return *this += anotherStr.c_str();
	}

	// Concats with an int32
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator+=(int32 i)
	{
		int32 numDigits = GetNumDigits( i );
		_length += numDigits;
		snprintf( _str + _length - numDigits, _maxLength - _length - numDigits, "%d", i );
		_str[_length] = nullchar;
		return *this;
	}

	// Concats with a uint32
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator+=(uint32 i)
	{
		return *this += ((int32)i);
	}

	// Concats with a float
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator+=(float f)
	{
		int32 numDigits = GetNumDigits( f, _precision );
		_length += numDigits;
		snprintf( _str + _length - numDigits, _maxLength - _length - numDigits, "%f", f );
		_str[_length] = nullchar;
		return *this;
	}

	// Concats with a double
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator+=(double f)
	{
		return *this += (float)f;
	}


	// Concats with an int32
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator+=(CharType c)
	{
		++_length;
		_str[_length - 1] = c;
		_str[_length] = nullchar;
		return *this;
	}


	// Addition
	template <typename CharType, int32 _maxLength>
	template <typename T>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::operator+(const T& v) const
	{
		FixedString newStr( *this );
		return newStr += v;
	}

	// Stream style concatenation
	template <typename CharType, int32 _maxLength>
	template <typename T>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::operator<<(const T& v)
	{
		return *this += v;
	}



	// Gets one directory up from the file
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::GetPreviousDirectory() const
	{
		int32 len = strlen( _str );
		int32 i = len - 1;
		int32 count = 0;
		while ( count<2 && i>0 )
		{
			if ( _str[i] == '\\' || _str[i] == '/' )
				++count;
			--i;
		}
		return Substring( 0, i + 1 );
	}


	// Chops off the directory
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::ChopDirectory( const FixedString<CharType, _maxLength>& dir ) const
	{
		int32 i = 0;
		while ( i < _length && dir[i] == _str[i] )
			++i;
		return Substring( i, _length );
	}


	// Gets the file portion of a path
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::GetFile() const
	{
		// Get the first /
		for ( int32 i = _length; i >= 0; --i )
		if ( _str[i] == '\\' || _str[i] == '/' )
			return Substring( i + 1, _length );
		return *this;
	}


	// Gets the directory portion of a path
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::GetDirectory() const
	{
		// Get the first /
		for ( int32 i = _length; i >= 0; --i )
		if ( _str[i] == '\\' || _str[i] == '/' )
			return Substring( 0, i + 1 );
		return *this;
	}

	// Removes the .* extension from a string
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::RemoveFileExtension() const
	{
		// Get the first .
		for ( int32 i = _length; i >= 0; --i )
		if ( _str[i] == '.' )
			return Substring( 0, i );
		return *this;
	}

	// Get the file extension
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::GetFileExtension() const
	{
		// Get the first .
		for ( int32 i = _length; i >= 0; --i )
		if ( _str[i] == '.' )
			return Substring( i + 1, _length );
		return *this;
	}


	// Less than
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator<(const FixedString<CharType, _maxLength>& str) const
	{
		return strcmp( _str, str._str ) < 0;
	}

	// Less than or equal
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator<=(const FixedString<CharType, _maxLength>& str) const
	{
		return (*this == str || *this < str);
	}

	// Greater than than
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator>(const FixedString<CharType, _maxLength>& str) const
	{
		return strcmp( _str, str._str ) > 0;
	}

	// Greater than or equal
	template <typename CharType, int32 _maxLength>
	bool FixedString<CharType, _maxLength>::operator>=(const FixedString<CharType, _maxLength>& str) const
	{
		return (*this == str || *this > str);
	}

	// Make lowercase
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& FixedString<CharType, _maxLength>::MakeLowercase()
	{
		for ( int32 i = 0; i < _length; ++i )
		{
			if ( _str[i] >= 'A' && _str[i] <= 'Z' )
				_str[i] -= ('Z' - 'z');
		}
		return *this;
	}

	// Get a lowercase version of this string
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength> FixedString<CharType, _maxLength>::ToLower() const
	{
		String lowerStr( _str );
		for ( int32 i = 0; i < _length; ++i )
		{
			if ( lowerStr._str[i] >= 'A' && lowerStr._str[i] <= 'Z' )
				lowerStr._str[i] -= ('Z' - 'z');
		}
		return lowerStr;
	}


	// Fix slashes in the file path
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& Maoli::FixedString<CharType, _maxLength>::FixFilePath()
	{
		for ( int32 i = 0; i < _length; ++i )
		if ( _str[i] == '/' )
			_str[i] = '\\';
		return *this;
	}

	// Check if a string contains a character
	template <typename CharType, int32 _maxLength>
	bool Maoli::FixedString<CharType, _maxLength>::Contains( CharType c ) const
	{
		for ( int32 i = 0; i < _length; ++i )
		{
			if ( _str[i] == c )
				return true;
		}
		return false;
	}

	// Check if a string contains a substring
	template <typename CharType, int32 _maxLength>
	bool Maoli::FixedString<CharType, _maxLength>::Contains( const FixedString& str ) const
	{
		int32 index = 0;
		for ( int32 i = 0; i < _length; ++i )
		{
			// Check length
			if ( str.Length() + i > _length )
				return false;

			// Search for the string
			bool match = true;
			for ( int32 j = 0; j < str.Length(); ++j )
			{
				if ( _str[i + j] != str[j] )
				{
					match = false;
					break;
				}
			}

			if ( match )
			{
				return true;
			}
		}

		return false;
	}

	// Replace a character with another character
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& Maoli::FixedString<CharType, _maxLength>::Replace( CharType character, CharType newCharacter )
	{
		for ( int32 i = 0; i < _length; ++i )
		{
			if ( _str[i] == character )
				_str[i] = newCharacter;
		}
		return *this;
	}

	// Replace a character with a string
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& Maoli::FixedString<CharType, _maxLength>::Replace( CharType character, const FixedString& str )
	{
		FixedString temp = *this;
		int32 index = 0;
		for ( int32 i = 0; i < _length; ++i )
		{
			if ( temp[i] == character )
			{
				for ( int32 j = 0; j < str.Length(); ++j )
					_str[index++] = str[j];
			}
			else
				_str[index++] = temp[i];
		}
		_length = index;
		_str[index] = nullchar;

		return *this;
	}

	// Replace a portion of a string with a string
	template <typename CharType, int32 _maxLength>
	FixedString<CharType, _maxLength>& Maoli::FixedString<CharType, _maxLength>::Replace( const FixedString& str, const FixedString& newStr )
	{
		FixedString temp = *this;
		int32 index = 0;
		for ( int32 i = 0; i < _length; ++i )
		{
			// Search for the string
			bool match = true;
			for ( int32 j = 0; j < str.Length(); ++j )
			{
				if ( temp[i + j] != str[j] )
				{
					match = false;
					break;
				}
			}

			if ( match )
			{
				// Copy the new string in
				for ( int32 j = 0; j < newStr.Length(); ++j )
				{
					_str[index++] = newStr[j];
				}

				// Update the offset
				i += str.Length() - 1;
			}
			else
				_str[index++] = temp[i];
		}
		_length = index;
		_str[index] = nullchar;

		return *this;
	}

}

//---------------------------------------------------------------------------------------
// File: PriorityQueue.h
//
// PriorityQueue with discrete priority levels
//
// Nate Orr
//---------------------------------------------------------------------------------------
#pragma once

#include "List.h"

namespace Maoli
{
	template <typename T, int32 _priorityLevels = 1, typename PriorityType = int32>
	class DiscretePriorityQueue
	{
	public:

		// Ctor
		DiscretePriorityQueue();

		// Dtor
		~DiscretePriorityQueue();

		// Enqueue
		void Enqueue(const T& obj, PriorityType priority = (PriorityType)0);

		// Dequeue the highest priority data
		T Dequeue();

		// Peek the front
		T& Front() const;

		// True if empty
		bool IsEmpty() const;

		// Get the number of elements
		uint32 Size() const;

		// Free memory
		void Release();

	private:

		// A list with a priotity attached
		struct PriorityList
		{
			List<T> list;
			int32 priority;
		};

		// Assigns _topQueue to the highest available level
		void ValidateTopQueue();

		PriorityList	_queues[_priorityLevels];	// A queue for each priotity level
		PriorityList*	_topQueue;					// Highest priority list that has elements
		uint32			_totalElements;				// The net size of the queue
	};


}

#include "DiscretePriorityQueue.inl"

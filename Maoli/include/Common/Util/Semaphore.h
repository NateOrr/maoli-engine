//--------------------------------------------------------------------------------------
// File: Semaphore.h
//
// Semaphore threading primitive
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once


class Semaphore
{
public:

	// Ctor
	Semaphore();

	// Dtor
	~Semaphore();

	// Wait for resources
	void Wait();

	// Let waiting threads know that resources are free
	void Post();

private:

	std::mutex*					_mutex;		// Mutex to protect the count
	std::condition_variable*	_conVar;	// Waiting for count
	int32							_count;		// Counter
};


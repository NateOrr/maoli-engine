// ---------------------------------------------------------------------------------------
// File: Pointer.inl
// 
// Reference tracked, shared smart pointer
// 
// Nate Orr
// ---------------------------------------------------------------------------------------

namespace Maoli
{
	// Empty pointer
	template <typename T>
	Pointer<T>::Pointer() : _object( nullptr ), _managed( false )
	{

	}

	// Managed pointer
	template <typename T>
	Pointer<T>::Pointer( T* obj ) : _object( nullptr ), _managed( false )
	{
		if ( obj )
		{
			_object = obj;
			_object->_refCount = 1;
		}
	}


	// Copy ctor
	template <typename T>
	Pointer<T>::Pointer( const Pointer<T>& p ) : _object( nullptr ), _managed( false )
	{
		*this = p;
	}

	// Dtor
	template <typename T>
	Pointer<T>::~Pointer()
	{
		// Decrement ref count when it passes out of scope
		Release();
	}

	// Assignment to another pointer
	template <typename T>
	Pointer<T>& Pointer<T>::operator=(const Pointer<T>& p)
	{
		if ( this != &p )
		{
			// Decrease ref count of whatever was previously pointed to
			Release();

			// Share the tracker and addref
			_object = p._object;
			_managed = p._managed;
			if ( _managed )
				_releaseCallback = p._releaseCallback;
			AddRef();
		}
		return *this;
	}

	// Assignment to nullptr
	template <typename T>
	Pointer<T>& Pointer<T>::operator=(std::nullptr_t null)
	{
		Release();
		_object = nullptr;
		_managed = false;
		return *this;
	}

	// Comparison with nullptr
	template <typename T>
	inline bool Pointer<T>::operator==(std::nullptr_t null) const
	{
		return _object == nullptr;
	}

	// Comparison with nullptr
	template <typename T>
	inline bool Pointer<T>::operator!=(std::nullptr_t null) const
	{
		return _object != nullptr;
	}

	// Comparison
	template <typename T>
	inline bool Pointer<T>::operator==(const Pointer<T>& p) const
	{
		return _object == p._object;
	}

	// Comparison
	template <typename T>
	inline bool Pointer<T>::operator!=(const Pointer<T>& p) const
	{
		return _object != p._object;
	}

	// Auto boolean
	template <typename T>
	inline Pointer<T>::operator bool() const
	{
		return _object != nullptr;
	}

	// Logical not
	template <typename T>
	inline bool Pointer<T>::operator!() const
	{
		return _object == nullptr;
	}

	// Object access
	template <typename T>
	inline T* Pointer<T>::operator*()
	{
		M_ASSERT( _object != nullptr, "Pointer<T><> dereference without a managed object" );
		return _object;
	}

	// Object access
	template <typename T>
	inline T* Pointer<T>::operator->()
	{
		M_ASSERT( _object != nullptr, "Pointer<T><> dereference without a managed object" );
		return _object;
	}

	// const Object access
	template <typename T>
	inline const T* Pointer<T>::operator->() const
	{
		M_ASSERT( _object != nullptr, "Pointer<T><> dereference without a managed object" );
		return _object;
	}


	// Get the reference count
	template <typename T>
	inline int32 Pointer<T>::GetReferenceCount() const
	{
		return (_object != nullptr) ? _object->_refCount : 0;
	}

	// Increase ref count
	template <typename T>
	inline void Pointer<T>::AddRef()
	{
		if ( _object )
			++_object->_refCount;
	}

	// Decrease ref cout, delete object when it reaches 0
	template <typename T>
	void Pointer<T>::Release()
	{
		if ( _object )
		{
			// Decrement the ref count
			--_object->_refCount;

			// If the ref count is 0, there should be no other Pointer<T>
			// objects managing the object.  Therefor it is safe to
			// delete both the object and the tracker
			if ( _object->_refCount == 0 )
			{
				delete _object;
				_managed = false;
				_object = nullptr;
			}

			// When the ref count is 1, that means the only copy is 
			// inside the resource manager, so it must be released
			else if ( _managed && _object->_refCount == 1 )
				_releaseCallback( *this );
		}
	}
}

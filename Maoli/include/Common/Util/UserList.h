//--------------------------------------------------------------------------------------
// File: UserList.h
//
// Linked List where the user is responsible for node memory
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	struct LinkedListNode
	{
		// Ctor
		LinkedListNode()
		{
			prev = next = nullptr;
			list = nullptr;
		}

		LinkedListNode*			next;
		LinkedListNode*			prev;
		List<LinkedListNode>*	list;
	};

	template <>
	class List<LinkedListNode>
	{
	public:

		// Forward decl to keep things clean
		class Iterator;

		// Ctor
		List();

		// Dtor
		~List();


		// Get an iterator at the start of the list
		Iterator Begin() const;

		// Get an iterator at the end of the list (empty iterator)
		Iterator End() const;

		// Adds an object to the end of the list
		void Add( LinkedListNode* data );

		// Adds an object to the front of the list
		void AddToFront( LinkedListNode* data );


		// Returns the first object
		LinkedListNode* GetFirst() const;

		// Returns the last object
		LinkedListNode* GetLast() const;


		// Insert at the specified iterator
		Iterator Insert( const Iterator& iter, LinkedListNode* data );

		// Removes the object
		bool Remove( LinkedListNode* data );

		// Remove at the specified iterator
		Iterator Remove( const Iterator& iter );

		// Removes the first object
		LinkedListNode* RemoveFirst();

		// Removes the last object from the list
		LinkedListNode* RemoveLast();


		// Returns the number of elements
		uint32 Size() const;

		// Clears the list
		void Clear();
		void Release();

		// Is the list empty?
		bool IsEmpty() const;

		// Check if the list contains an element
		bool Contains( LinkedListNode* value );

	private:

		LinkedListNode*	_head;		// Pointer to first node
		LinkedListNode*	_tail;		// Pointer to last node
		uint32			_size;		// Length of the list

		// Copy and assignment not allowed
		List( const List& l );
		List& operator=(const List& l);

	public:

		// Iterator
		class Iterator
		{
			friend class List;
		public:

			// Ctor
			Iterator();

			// Move forward
			Iterator& operator++();

			// Move backward
			Iterator& operator--();

			// True if the iterator has reached the end in either direction
			bool End() const;

			// Comparison
			bool operator==(const Iterator& iter) const;

			// Comparison
			bool operator!=(const Iterator& iter) const;

			// Data access
			LinkedListNode* operator*() const;

			// Data access
			LinkedListNode* operator->() const;

		private:

			// Ctor
			Iterator( LinkedListNode* n );

			LinkedListNode*	_node;
		};
	};

	// Access to this type
	typedef List<LinkedListNode> UserAllocatedList;

}


#include "UserList.inl"

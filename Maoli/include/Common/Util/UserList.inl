//--------------------------------------------------------------------------------------
// File: UserList.inl
//
// Linked List where the user is responsible for node memory
//
// Nate Orr
//--------------------------------------------------------------------------------------

namespace Maoli
{


#pragma region List


	// Get an iterator at the start of the list
	inline List<LinkedListNode>::Iterator List<LinkedListNode>::Begin() const
	{
		return Iterator( _head );
	}

	// Get an iterator at the end of the list (empty iterator)
	inline List<LinkedListNode>::Iterator List<LinkedListNode>::End() const
	{
		return Iterator( nullptr );
	}


	// Is the list empty?
	inline bool List<LinkedListNode>::IsEmpty() const
	{
		return (_size == 0);
	}


	// Gets objects at the end of the list
	inline LinkedListNode* List<LinkedListNode>::GetFirst() const
	{
		M_ASSERT( _head != nullptr, "Cannot read from an empty list" );
		return _head;
	}


	// Gets objects at the end of the list
	inline LinkedListNode* List<LinkedListNode>::GetLast() const
	{
		M_ASSERT( _tail != nullptr, "Cannot read from an empty list" );
		return _tail;
	}

	// Gets the number of objects in the list
	inline uint32 List<LinkedListNode>::Size() const
	{
		return _size;
	}
	


#pragma endregion


#pragma region Iterator

	// Move forward
	inline List<LinkedListNode>::Iterator& List<LinkedListNode>::Iterator::operator++()
	{
		if ( _node )
			_node = _node->next;
		return *this;
	}

	// Move backward
	inline List<LinkedListNode>::Iterator& List<LinkedListNode>::Iterator::operator--()
	{
		if ( _node )
			_node = _node->prev;
		return *this;
	}

	// True if the iterator has reached the end in either direction
	inline bool List<LinkedListNode>::Iterator::End() const
	{
		return (_node == nullptr);
	}

	// Comparison
	inline bool List<LinkedListNode>::Iterator::operator==(const Iterator& iter) const
	{
		return _node == iter._node;
	}

	// Comparison
	inline bool List<LinkedListNode>::Iterator::operator!=(const Iterator& iter) const
	{
		return _node != iter._node;
	}

	// Data access
	inline LinkedListNode* List<LinkedListNode>::Iterator::operator*() const
	{
		M_ASSERT( _node != nullptr, "Invalid iterator dereferenced" );
		return _node;
	}

	// Data access
	inline LinkedListNode* List<LinkedListNode>::Iterator::operator->() const
	{
		M_ASSERT( _node != nullptr, "Invalid iterator dereferenced" );
		return _node;
	}

#pragma endregion

}


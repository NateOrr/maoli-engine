//--------------------------------------------------------------------------------------
// File: Util.cpp
//
// Helper functions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Util.h"

namespace Maoli
{

	// Checks if a file exists
	bool FileExists( const char *filename )
	{
		std::ifstream ifile( filename );
		return ifile.is_open();
	}

	// Gets the number or characters needed to put an int32 in a char array
	int32 GetNumDigits( int32 i )
	{
		int32 digits = 0;
		if ( i <= 0 )
			digits = 1;
		while ( i )
		{
			i /= 10;
			++digits;
		}
		return digits;
	}

	// Gets the number or characters needed to put an float in a char array
	int32 GetNumDigits( float f, int32 p )
	{
		float num = fabs( f );
		int32 digits = 0;
		if ( f <= 0.0f )
			digits = 1;
		float iTen = 1.0f / 10.0f;
		while ( num > 1.0f )
		{
			num *= iTen;
			++digits;
		}
		++digits;
		while ( num != 0.000000f && p )
		{
			num *= iTen;
			++digits;
			--p;
		}
		return digits;
	}



}
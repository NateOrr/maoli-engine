//--------------------------------------------------------------------------------------
// File: EventManager.cpp
//
// Event system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "EventManager.h"

namespace Maoli
{
	// Unregister from an event
	void EventManager::Unregister( const Event& event, void* caller )
	{
		// Get the list of callbacks
		auto iter = _callbacks.find( event );
		if(iter == _callbacks.end())
			return;

		// Remove the callback
		List<CallbackSet>& callbacks = iter->second;
		for ( auto i = callbacks.Begin(); !i.End(); ++i )
		{
			CallbackSet& cb = *i;
			if ( cb.caller == caller )
			{
				callbacks.Remove( i );
				break;
			}
		}

		// Remove empty events
		if ( callbacks.IsEmpty() )
		{
			auto eventIter = _callbacks.find( event );
			_callbacks.erase( eventIter );
		}
	}

	// Broadcast an event
	void EventManager::BroadcastEvent( const Event& event, void* userData )
	{
		// Get the list of callbacks
		auto iter = _callbacks.find( event );
		if(iter == _callbacks.end())
			return;

		// Execute the callback
		List<CallbackSet>& callbacks = iter->second;
		for ( auto i = callbacks.Begin(); !i.End(); ++i )
		{
			CallbackSet& cb = *i;
			cb.callback( userData );
		}
	}

	// Remove all active events
	void EventManager::Clear()
	{
		_callbacks.clear();
	}




}
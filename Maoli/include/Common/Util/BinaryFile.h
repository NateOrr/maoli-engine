//--------------------------------------------------------------------------------------
// File: BinaryFile.h
//
// Load a binary file into a byte array
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{

	class BinaryFile
	{
	public:
		// Ctor
		BinaryFile();
		
		// Dtor
		virtual ~BinaryFile();	

		// Cleanup
		void Release();

		// Reads the entire contents of the file
		bool Load(const char* file);

		// Fill with pre-existing data
		void FromData(byte* data, uint32 size);

		// Checks if a file is loaded
		inline bool IsLoaded() const { return _data!=nullptr; }

		// Size in bytes
		inline uint32 GetSize() const { return _fileSize; }

		// Get the bytes
		inline byte* GetData(){ return _data; }


	protected:
		byte*			_data;
		uint32	_fileSize;
	};
}

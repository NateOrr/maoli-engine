//---------------------------------------------------------------------------------------
// File: PriorityQueue.inl
//
// PriorityQueue ADT
//
// Nate Orr
//---------------------------------------------------------------------------------------

namespace Maoli
{
	// Ctor
	template <typename T>
	PriorityQueue<T>::PriorityQueue( typename PriorityQueue<T>::GreaterFn compareFn )
	{
		_compareFn = compareFn;
	}

	// Enqueue
	template <typename T>
	inline void PriorityQueue<T>::Enqueue( const T& obj )
	{
		for ( auto iter = List<T>::Begin(); !iter.End(); ++iter )
		{
			if ( _compareFn( *iter, obj ) )
			{
				Insert( iter, obj );
				return;
			}
		}

		Add( obj );
	}

	// Dequeue
	template <typename T>
	inline T PriorityQueue<T>::Dequeue()
	{
		return List<T>::RemoveFirst();
	}

	// Peek the front
	template <typename T>
	inline T& PriorityQueue<T>::Front() const
	{
		return List<T>::GetFirst();
	}

	// True if empty
	template <typename T>
	inline bool PriorityQueue<T>::IsEmpty() const
	{
		return List<T>::IsEmpty();
	}

	// Get the number of elements
	template <typename T>
	inline uint32 PriorityQueue<T>::Size() const
	{
		return List<T>::Size();
	}

	// Clear the queue
	template <typename T>
	void Maoli::PriorityQueue<T>::Clear()
	{
		List<T>::Clear();
	}

	// Free memory
	template <typename T>
	inline void PriorityQueue<T>::Release()
	{
		List<T>::Release();
	}

	// Dtor
	template <typename T>
	PriorityQueue<T>::~PriorityQueue()
	{
		Release();
	}
}

//--------------------------------------------------------------------------------------
// File: RawTexture.h
//
// Filed based byte array texture
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Forward decl
	class Engine;
	class Renderer;

	// Filed based byte array texture
	// --------------------------------------------------------------------------------------
	class RawTexture : public BinaryFile
	{
	public:
		// Ctor
		RawTexture(Engine* engine);

		// Reads the entire contents of the file
		bool Load(const char* fileName);

		// Load dds texture
		bool LoadDDS(const char* fileName, uint32 width, uint32 height);

		// Save to file, no image compression is used to preserve data integrity
		void Save(const char* fileName);

		// Allocate a buffer ahead of time
		void Allocate(uint32 size);

		// Automatic conversion to byte array
		inline operator byte*(){ return GetData(); }

	private:

		Engine*		_engine;
		Renderer*	_graphics;

	};
}

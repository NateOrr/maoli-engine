//--------------------------------------------------------------------------------------
// File: Util.h
//
// Helper functions
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Checks if a file exists
	bool FileExists( const char *filename );

	// Swaps two objects
	template<class T>
	inline void Swap( T& a, T& b )
	{
		T temp = a;
		a = b;
		b = temp;;
	}

	// Safe COM release
	template <typename T>
	void SafeRelease( T*& obj )
	{
		if ( obj )
		{
			obj->Release();
			obj = nullptr;
		}
	}


	// Safe pointer delete
	template <typename T>
	void SafeDelete( T*& obj )
	{
		if ( obj )
		{
			delete obj;
			obj = nullptr;
		}
	}

	// Safe interface release
	template <typename T>
	void SafeReleaseDelete( T*& obj )
	{
		if ( obj )
		{
			obj->Release();
			delete obj;
			obj = nullptr;
		}
	}


	// Safe array delete
	template <typename T>
	void SafeDeleteArray( T*& obj )
	{
		if ( obj )
		{
			delete[] obj;
			obj = nullptr;
		}
	}


	// Serialize a struct that does not contain pointers
	template <typename T>
	inline void Serialize( std::ofstream& file, const T* data )
	{
		file.write( reinterpret_cast<const char*>(data), sizeof( T ) );
	}


	// De-Serialize a struct that does not contain pointers
	template <typename T>
	inline void Deserialize( std::ifstream& file, T* data )
	{
		file.read( reinterpret_cast<char*>(data), sizeof( T ) );
	}

	// Gets the number or characters needed to put an int32 in a char array
	int32 GetNumDigits( int32 i );

	// Gets the number or characters needed to put an float in a char array
	int32 GetNumDigits( float f, int32 p );

}
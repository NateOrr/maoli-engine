//--------------------------------------------------------------------------------------
// File: Bitfield.h
//
// Variable sized bitfields
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Generic bitfield
	template <typename T>
	class Bitfield
	{
	public:

		// Ctor
		Bitfield();
		
		// Turn a bit on
		void Set(int32 index, bool on = true);

		// Turn a bit off
		void Unset(int32 index);

		// Toggle
		void Toggle(int32 index);

		// Check if a bit is set
		bool Check(int32 index);

		// Array style access for bit checking
		bool operator[] (int32 index);

		// Directly set bits
		T& operator|= (int32 bitValue);

		// Directly check bits
		bool operator& (int32 bitValue);

		// Directly Toggle bits
		T& operator^= (int32 bitValue);

		// Get the raw bits
		const T& GetBits() const;
		

	protected:

		T		_bits;
	};

	// Pre-defined types
	typedef Bitfield<byte>		Bitfield8;
	typedef Bitfield<uint16>	Bitfield16;
	typedef Bitfield<uint32>	Bitfield32;
	typedef Bitfield<uint64>	Bitfield64;
}

#include "Bitfield.inl"

//--------------------------------------------------------------------------------------
// File: List.h
//
// Linked List
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma once

#include "Stack.h"

namespace Maoli
{
	template <typename T>
	class List
	{
	public:

		// Forward decl to keep things clean
		class Iterator;

		// Ctor
		List();

		// Copy ctor
		List(const List& l);

		// Assignment op
		List& operator=(const List& l);

		// Dtor
		~List();


		// Get an iterator at the start of the list
		Iterator Begin() const;

		// Get an iterator at the end of the list (empty iterator)
		Iterator End() const;

		// Adds an object to the end of the list
		void Add( const T& data );
		T& Add();
			
		// Adds an object to the front of the list
		void AddToFront(const T& data);


		// Returns the first object
		T& GetFirst() const;

		// Returns the last object
		T& GetLast() const;


		// Insert at the specified iterator
		Iterator Insert(const Iterator& iter, const T& data);

		// Removes the object
		bool Remove(const T& data);

		// Remove at the specified iterator
		Iterator Remove(const Iterator& iter);

		// Removes the first object
		T RemoveFirst();

		// Removes the last object from the list
		T RemoveLast();


		// Returns the number of elements
		uint32 Size() const;

		// Clears the list
		void Clear();

		// Releases dynamic memory
		void Release();
		void ReleaseAndDelete();

		// Is the list empty?
		bool IsEmpty() const;

		// Check if the list contains an element
		bool Contains( const T& value );

	private:

		// Linked list node
		struct Node
		{
			T data;
			Node* next;
			Node* prev;

			// Ctor
			Node();

			// Delete node data
			void Delete();
		};

		// Recycle a node
		void RecycleNode(Node* node);

		// Get a free node
		Node* GetNewNode();
			
		Node*	_head;		// Pointer to first node
		Node*	_tail;		// Pointer to last node
		uint32	_size;		// Length of the list
			
		Stack<Node*> _recycled;		// Recycled node memory


	public:

		// Iterator
		class Iterator
		{
			friend class List;
		public:

			// Ctor
			Iterator();

			// Move forward
			Iterator& operator++();

			// Move backward
			Iterator& operator--();

			// True if the iterator has reached the end in either direction
			bool End() const;

			// Comparison
			bool operator==(const Iterator& iter) const;

			// Comparison
			bool operator!=(const Iterator& iter) const;

			// Data access
			T& operator*() const;

			// Data access
			T& operator->() const;

		private:
			
			// Ctor
			Iterator(Node* n);

			Node*	_node;
		};
	};

}

// Print to a stream
template <typename T>
std::ostream& operator<<( std::ostream& os, const Maoli::List<T>& list );

#include "List.inl"

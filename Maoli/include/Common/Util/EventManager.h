//--------------------------------------------------------------------------------------
// File: EventManager.h
//
// Event system
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Event type
	typedef int32 Event;

	class EventManager
	{
		// Event callback format
		typedef std::function<void( void* )> EventCallback;

	public:

		// Register to an event
		template <typename T>
		void Register( const Event& event, T* caller, void (T::*fn)(void*)  )
		{
			// Get the list of callbacks
			auto iter = _callbacks.find( event );
			List<CallbackSet>& callbacks = (iter == _callbacks.end()) ? _callbacks.emplace( event, List<CallbackSet>() ).first->second : iter->second;

			// Make sure this caller has not already registered
			for ( auto i = callbacks.Begin(); !i.End(); ++i )
			{
				CallbackSet& cb = *i;
				if ( cb.caller == caller )
					return;
			}

			// Register the callback
			CallbackSet cb = { caller, std::bind( fn, caller, std::placeholders::_1 ) };
			callbacks.Add( cb );
		}

		// Unregister from an event
		void Unregister( const Event& event, void* caller );

		// Broadcast an event
		void BroadcastEvent( const Event& event, void* userData = nullptr );

		// Remove all active events
		void Clear();

	private:

		struct CallbackSet
		{
			void* caller;
			EventCallback callback;
		};

		std::unordered_map<Event, List<CallbackSet>>	_callbacks;		// Callbacks for each event
	};
}
//---------------------------------------------------------------------------------------
// File: Console.cpp
//
// Console window support for win32 apps
//
// Nate Orr
//---------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Console.h"

#ifdef MAOLI_WINDOWS

namespace Maoli
{
	// Open the console
	void Console::Open()
	{
		AllocConsole();
		AttachConsole(GetCurrentProcessId());
		this->_cinbuf = std::cin.rdbuf();
		this->_console_cin.open("CONIN$");
		std::cin.rdbuf(this->_console_cin.rdbuf());
		this->_coutbuf = std::cout.rdbuf();
		this->_console_cout.open("CONOUT$");
		std::cout.rdbuf(this->_console_cout.rdbuf());
		this->_cerrbuf = std::cerr.rdbuf();
		this->_console_cerr.open("CONOUT$");
		std::cerr.rdbuf(this->_console_cerr.rdbuf());
	}

	// Close the console
	void Console::Close()
	{
		this->_console_cout.close();
		std::cout.rdbuf(this->_coutbuf);
		this->_console_cin.close();
		std::cin.rdbuf(this->_cinbuf);
		this->_console_cerr.close();
		std::cerr.rdbuf(this->_cerrbuf);
		FreeConsole();
	}
}

#endif
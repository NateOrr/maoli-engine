//--------------------------------------------------------------------------------------
// File: MemoryManager.cpp
//
// Custom memory pool
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "MemoryManager.h"
#include <thread>
#include <mutex>
#include "Heap.h"

namespace Maoli
{
	// Get singleton instance
	MemoryManager* MemoryManager::GetInstance()
	{
		static MemoryManager instance;
		return &instance;
	}
	
	// Ctor
	MemoryManager::MemoryManager() : _numHeaps(0), _numAllocations(0), _numDeallocations(0), _totalAllocated(0)
	{
		_mutex = new std::mutex;
		for(uint32 i=0; i<_maxHeaps; ++i)
			_heaps[i] = nullptr;
	}

	// Dtor
	MemoryManager::~MemoryManager()
	{
		Release();
		delete _mutex;
	}
	
	// Create a heap
	uint32 MemoryManager::CreateHeap(uint32 size, bool threadShared, const char* name)
	{
		M_ASSERT(_numHeaps < _maxHeaps, "Max number of heaps already created");

		std::unique_lock<std::mutex> lock( *_mutex );
		_heaps[_numHeaps] = (Heap*)malloc(sizeof(Heap));
		_heaps[_numHeaps]->Init(size, threadShared, name);
		_heaps[_numHeaps]->_heapID = _numHeaps;
		++_numHeaps;
		return _heaps[_numHeaps-1]->_heapID;
	}

	// Free heap memory
	void MemoryManager::DestroyHeap( uint32 id )
	{
		M_ASSERT(id < _maxHeaps, "Heap ID out of range");
		std::unique_lock<std::mutex> lock( *_mutex );
		if ( _heaps[id] )
		{
			_heaps[id]->Release();
			free(_heaps[id]);
			_heaps[id] = nullptr;
		}
	}

	// Allocate memory
	void* MemoryManager::Allocate( uint32 size, uint32 heapID /*= -1*/ )
	{
#ifdef MAOLI_DEBUG
		++_numAllocations;
		_totalAllocated += size;
#endif

		M_ASSERT(_numHeaps, "No heap created, use MemoryManager::CreateHeap");

		// For valid ID's, just allocate
		if(heapID != -1)
		{
			M_ASSERT(heapID < _numHeaps, "Invalid heap id");
			return _heaps[heapID]->Allocate(size);
		}

		// Otherwise use the first heap that shares this thread id
		std::thread::id threadID = std::this_thread::get_id();
		for(uint32 i=0; i<_numHeaps; ++i)
		{
			if(_heaps[i]->_threadID == threadID)
				return _heaps[i]->Allocate(size);
		}

		return nullptr;
	}

	// Free memory
	void MemoryManager::Free( void* mem )
	{
#ifdef MAOLI_DEBUG
		++_numDeallocations;
#endif

		// Ignore null memory
		if(!mem)
			return;

		// Find the heap that created this memory
		for(uint32 i=0; i<_numHeaps; ++i)
		{
			if( mem>=_heaps[i]->_firstHeader && mem<=_heaps[i]->_lastFooter)
			{
				_heaps[i]->Free(mem);
				return;
			}
		}
	}

	// Shows stats
	void MemoryManager::DisplayStats()
	{
		using std::cout;
		using std::endl;

#ifndef MAOLI_DEBUG
		cout << "Memory stats are disabled in release mode\nplease run in debug mode to view stats\n";
		return;
#endif

		// Process the allocation size
		String allocStride(" bytes");
		float total = (float)_totalAllocated;
		if(total >= 1024)
		{
			total /= 1024;
			allocStride = "kb";
		}
		if(total >= 1024)
		{
			total /= 1024;
			allocStride = "mb";
		}
		String totalValue;
		totalValue.SetFloatPrecision(2);
		totalValue << total;

		cout << "Memory Manager Stats\n";
		cout << "\tNumHeaps = " << _numHeaps << endl;
		cout << "\tNumAllocations = " << _numAllocations << endl;
		cout << "\tNumDeletes = " << _numDeallocations << endl;
		cout << "\tTotalAllocated = " << totalValue << allocStride << endl;
		cout << endl << endl;
	}

	// Free all memory
	void MemoryManager::Release()
	{
		for(uint32 i=0; i<_maxHeaps; ++i)
		{
			DestroyHeap(i);
		}
	}

}

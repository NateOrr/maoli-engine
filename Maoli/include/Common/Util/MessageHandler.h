//--------------------------------------------------------------------------------------
// File: MessageHandler.h
//
// For passing system messages
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

// Stupid windows macros...
#undef SendMessage

namespace Maoli
{

	// Message IDs
	enum class MessageType
	{
		None = 0, 
		ProcessCommandList,
	};

	// Basic message
	struct Message
	{
		int32 msg;
		void* param;
		int32 flags;
	};

	class MessageHandler
	{
	public:

		// Ctor
		MessageHandler();

		// Clear all messages
		void Flush();

		// Posts a message
		// Message id of zero is reserved
		template <class T>
		void SendMessage(const T& id, void* param=nullptr, int32 flags=0)
		{
			Message m;
			m.msg = (int32)id;
			m.flags = flags;
			m.param = param;
			_messages.Add(m);
		}

		// Gets the next available message
		Message GetNextMessage();

	private:

		Array<Message>		_messages;			// Message container
		uint32				_messageIndex;		// Current message in processing
	};

}

//--------------------------------------------------------------------------------------
// File: BinaryFile.cpp
//
// Load a binary file into a byte array
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "BinaryFile.h"


namespace Maoli
{
	// Ctor
	BinaryFile::BinaryFile()
	{
		_data = nullptr;
	}


	// Dtor
	BinaryFile::~BinaryFile()
	{
		Release();
	}


	// Cleanup
	void BinaryFile::Release()
	{
		delete[] _data;
		_data = nullptr;
	}


	// Reads the contents of a binary file
	bool BinaryFile::Load(const char* fileName)
	{
		std::ifstream file(fileName, std::ios_base::binary);
		if(!file.is_open())
			return false;

		_data = nullptr;

		// Read the data
		file.seekg(0, std::ios_base::end);
		_fileSize = (uint32)file.tellg();
		file.seekg(0, std::ios_base::beg);
		byte* data = new byte[_fileSize];
		file.read(reinterpret_cast<char*>(data), _fileSize);
		_data = data;

		file.close();
		return true;
	}


	// Fill with pre-existing data
	void BinaryFile::FromData( byte* data, uint32 size )
	{
		_fileSize = size;
		_data = new byte[_fileSize];
		memcpy(_data, data, _fileSize);
	}



}

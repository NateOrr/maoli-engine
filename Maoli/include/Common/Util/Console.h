//---------------------------------------------------------------------------------------
// File: Console.h
//
// Console window support for win32 apps
//
// Nate Orr
//---------------------------------------------------------------------------------------
#pragma once

#ifdef MAOLI_WINDOWS

namespace Maoli
{
	class Console
	{
	public:

		// Open the console
		void Open();

		// Close the console
		void Close();

	private:
		std::streambuf*	_cinbuf;
		std::streambuf*	_coutbuf;
		std::streambuf*	_cerrbuf;
		std::ifstream	_console_cin;
		std::ofstream	_console_cout;
		std::ofstream	_console_cerr;
	};
}

#endif
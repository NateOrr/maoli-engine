//--------------------------------------------------------------------------------------
// File: Error.cpp
//
// Error handling
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Util/Error.h"

namespace Maoli
{
	// Displays a message box
	void ErrorMessage(const char* msg, ...)
	{
		static char outStr[128];
		va_list arg_list;
		va_start(arg_list, msg);
		vsprintf_s(outStr,msg,arg_list);
		va_end(arg_list);
		::MessageBoxA(nullptr, outStr, "Maoli Engine", MB_OK);
	}

	// Validate a d3d error code
	void CheckD3DError( HRESULT hr )
	{
		std::cout << "D3D Function call returned: ";
		switch ( hr )
		{
			case D3D11_ERROR_FILE_NOT_FOUND:
				std::cout << "D3D11_ERROR_FILE_NOT_FOUND" << std::endl;
				break;
			
			case D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS:
				std::cout << "D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS" << std::endl;
				break;
			
			case D3D11_ERROR_TOO_MANY_UNIQUE_VIEW_OBJECTS:
				std::cout << "D3D11_ERROR_TOO_MANY_UNIQUE_VIEW_OBJECTS" << std::endl;
				break;
			
			case D3D11_ERROR_DEFERRED_CONTEXT_MAP_WITHOUT_INITIAL_DISCARD:
				std::cout << "D3D11_ERROR_DEFERRED_CONTEXT_MAP_WITHOUT_INITIAL_DISCARD" << std::endl;
				break;
			
			case D3DERR_INVALIDCALL:
				std::cout << "D3DERR_INVALIDCALL" << std::endl;
				break;
			
			case DXGI_ERROR_INVALID_CALL:
				std::cout << "DXGI_ERROR_INVALID_CALL" << std::endl;
				break;
			
			case D3DERR_WASSTILLDRAWING:
				std::cout << "D3DERR_WASSTILLDRAWING" << std::endl;
				break;
			
			case DXGI_ERROR_WAS_STILL_DRAWING:
				std::cout << "DXGI_ERROR_WAS_STILL_DRAWING" << std::endl;
				break;
			
			case E_FAIL:
				std::cout << "E_FAIL" << std::endl;
				break;
			
			case E_INVALIDARG:
				std::cout << "E_INVALIDARG" << std::endl;
				break;
			
			case E_OUTOFMEMORY:
				std::cout << "E_OUTOFMEMORY" << std::endl;
				break;
			
			case E_NOTIMPL:
				std::cout << "E_NOTIMPL" << std::endl;
				break;
			
			case S_FALSE:
				std::cout << "S_FALSE" << std::endl;
				break;
			
			case S_OK:
				std::cout << "S_OK" << std::endl;
				break;
		}
	}

}

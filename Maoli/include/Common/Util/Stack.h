//--------------------------------------------------------------------------------------
// File: Stack.h
//
// Stack class
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma once


namespace Maoli
{

	template <typename T>
	class Stack
	{
	public:
		// Ctor
		Stack();

		// Copy ctor
		Stack(const Stack& s);

		// Assignment op
		Stack& operator=(const Stack& s);

		// Dtor
		~Stack();

		// Push data onto the stack
		void Push(const T& data);

		// Pop data from the stack
		T& Pop();

		// Peek the top element
		T& Top();

		// Is the stack empty?
		bool IsEmpty() const;

		// Return the stack size
		int32 Size() const;

		// Free all mem and create an empty stack
		void Release();

		// Free all mem, delete all data and create an empty stack
		void ReleaseAndDelete();

	private:
		T*		_data;		// Stack data
		int32		_size;		// Current stack size
		int32		_capacity;	// Stack capacity

		// Resize the stack to double its current capacity
		void Resize();
	};

}

#include "Stack.inl"

//--------------------------------------------------------------------------------------
// File: ConstantBuffer.cpp
//
// Constant buffer wrapper
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ConstantBuffer.h"

namespace Maoli
{

	// Ctor
	ConstantBuffer::ConstantBuffer()
	{
		_buffer = nullptr;
	}


	// Dtor
	ConstantBuffer::~ConstantBuffer()
	{
		SafeRelease(_buffer);
		_variables.Release();
	}


	// Copy the buffer to video memory
	void ConstantBuffer::ApplyChanges(ID3D11DeviceContext* dc)
	{
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		dc->Map(_buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		CopyMemory(mappedResource.pData, _data, _size);
		dc->Unmap(_buffer, 0);
		isDirty = false;
	}


	// Builds the variable reflection data
	HRESULT ConstantBuffer::Create(ID3D11Device* device, ID3D11ShaderReflectionConstantBuffer* cb)
	{
		// Get a desc for this cb
		D3D11_SHADER_BUFFER_DESC desc;
		cb->GetDesc(&desc);

		// Create the buffer
		Allocate(desc.Size);
		D3D11_BUFFER_DESC bd;
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		bd.MiscFlags = 0;  
		bd.ByteWidth = static_cast<uint32>(_size + 
			(16 - (_size % 16)));
		bd.StructureByteStride = 0;
		H_RETURN( device->CreateBuffer(&bd, 0, &_buffer), "failed to CreateBuffer");

		this->_size = desc.Size;
		_type = ShaderVariableType::Buffer;
		_name = desc.Name;

		// Process each variable
		_variables.Allocate(desc.Variables);
		for(uint32 i=0; i<desc.Variables; i++)
		{
			// Get the desc for the var
			D3D11_SHADER_VARIABLE_DESC varDesc;
			cb->GetVariableByIndex(i)->GetDesc(&varDesc);

			// Get the type, and create the appropriate shader variable
			D3D11_SHADER_TYPE_DESC typeDesc;
			cb->GetVariableByIndex(i)->GetType()->GetDesc(&typeDesc);

			switch(typeDesc.Class)
			{
				// Scalars
			case D3D10_SVC_SCALAR:
				_variables[i] = new ShaderScalar(varDesc.Name, this, varDesc.StartOffset, typeDesc.Elements);
				break;

				// Vectors
			case D3D10_SVC_VECTOR:
				_variables[i] = new ShaderVector(varDesc.Name, this, varDesc.StartOffset, varDesc.Size, typeDesc.Columns);
				break;

				// Matrices
			case D3D10_SVC_MATRIX_COLUMNS:
			case D3D10_SVC_MATRIX_ROWS: 
				_variables[i] = new ShaderMatrix(varDesc.Name, this, varDesc.StartOffset, varDesc.Size);
				break;

				// Structs
			case D3D10_SVC_STRUCT:
				_variables[i] = new ShaderStruct(varDesc.Name, this, varDesc.StartOffset, varDesc.Size);
				break;

			default:
				break;

			}
		}

		return S_OK;
	}


	// Get a variable by name
	inline ConstantBufferVariable* ConstantBuffer::GetVariableByName(const char* name)
	{
		for(uint32 i=0; i<_variables.Size(); i++)
		{
			if(strcmp(name, _variables[i]->GetName()) == 0)
				return _variables[i];
		}
		return nullptr;
	}


	

}

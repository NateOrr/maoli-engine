//--------------------------------------------------------------------------------------
// File: ShaderBuffer.cpp
//
// A shader buffer object that requires updating
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ShaderBuffer.h"

namespace Maoli
{
	// Ctor
	ShaderBuffer::ShaderBuffer( ) : ShaderVariable( String::none, ShaderVariableType::Buffer, 0 )
	{
		isDirty = true;
		_data = nullptr;
	}


	// Dtor
	ShaderBuffer::~ShaderBuffer()
	{
		SafeDeleteArray(_data);
	}


	// Allocate the memory for the buffer
	void ShaderBuffer::Allocate( uint32 size )
	{
		_size = size; _data = new BYTE[size];
	}


	// Copy memory into the buffer
	void ShaderBuffer::Fill( const void* data )
	{
		CopyMemory(_data, data, _size);
		FlagDirty();
	}

}

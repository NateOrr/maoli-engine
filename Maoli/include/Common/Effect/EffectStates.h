//--------------------------------------------------------------------------------------
// File: EffectStates.h
//
// Blend, Rasterizer, and Depth Stencil states
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Effect/ShaderVariable.h"

namespace Maoli
{
	// Blend state manager
	struct BlendState : public ShaderVariable
	{
		// Ctor
		BlendState();

		// Ctor
		BlendState(const char* name, const D3D11_BLEND_DESC& d);

		// Dtor
		~BlendState();

		ID3D11BlendState*	D3DObject;		// D3D state
		D3D11_BLEND_DESC	Desc;			// State description
	};


	// Rasterizer state manager
	struct RasterizerState : public ShaderVariable
	{
		// Ctor
		RasterizerState();
		
		// Ctor
		RasterizerState(const char* name, D3D11_RASTERIZER_DESC& d);

		// Dtor
		~RasterizerState();

		ID3D11RasterizerState*	D3DObject;		// D3D state
		D3D11_RASTERIZER_DESC	Desc;			// State description
	};


	// Depth stencil state manager
	struct DepthStencilState : public ShaderVariable
	{
		// Ctor
		DepthStencilState();
		
		// Ctor
		DepthStencilState(const char* name, D3D11_DEPTH_STENCIL_DESC& d);

		// Dtor
		~DepthStencilState();

		ID3D11DepthStencilState*	D3DObject;		// D3D state
		D3D11_DEPTH_STENCIL_DESC	Desc;			// State description
	};


	// Texture sampler
	struct SamplerState : public ShaderVariable
	{
		// Ctor
		SamplerState();
		
		// Ctor
		SamplerState(const char* name, D3D11_SAMPLER_DESC& d);

		// Dtor
		~SamplerState();

		ID3D11SamplerState*		D3DObject;		// D3D state
		D3D11_SAMPLER_DESC		Desc;			// State description
	};
}

//--------------------------------------------------------------------------------------
// File: ShaderMatrix.h
//
// Matrix inside a constant buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ConstantBufferVariable.h"

namespace Maoli
{
	class ShaderMatrix : public ConstantBufferVariable
	{
	public:

		// Ctor
		ShaderMatrix(const char* name, ShaderBuffer* parent, uint32 offset, uint32 size);

		// Set a matrix
		void SetMatrix(const Matrix& matrix);

		// Set a matrix array
		void SetMatrixArray(const Matrix* matrices, uint32 count);
	};
}

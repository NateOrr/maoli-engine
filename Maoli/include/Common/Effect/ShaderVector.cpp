//--------------------------------------------------------------------------------------
// File: ShaderVector.cpp
//
// Vector inside a constant buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ShaderVector.h"
#include "Effect/ShaderBuffer.h"

namespace Maoli
{
	// Ctor
	ShaderVector::ShaderVector( const char* name, ShaderBuffer* parent, uint32 offset, uint32 size, uint32 columns ) : 
		ConstantBufferVariable(name, parent, offset, ShaderVariableType::Vector, size), _length(columns), _elementSize(size/columns)
	{

	}

	// Set an integer vector
	void ShaderVector::SetIntVector( const uint32 v[] )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v, _size);
	}

	// Set an integer vector
	void ShaderVector::SetIntVector( const int32 v[] )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v, _size);
	}

	// Set an float vector
	void ShaderVector::SetFloatVector( const float v[] )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v, _size);
	}

	// Set a double vector
	void ShaderVector::SetFloatVector( const double v[] )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v, _size);
	}

	// Set a float vector
	void ShaderVector::SetFloatVector( const Vector4& v )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, &v, _size);
	}

	// Set an array of vectors
	void ShaderVector::SetFloatVectorArray( const float* v, uint32 numBytes )
	{
		_parent->FlagDirty();
		CopyMemory( _parent->_data + _start, v, numBytes );
	}

}

//--------------------------------------------------------------------------------------
// File: Effect.h
//
// Shader effects framework
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "EffectTypes.h"

namespace Maoli
{
	// Forward decl
	class Renderer;
	class EffectLoader;
	class ConstantBuffer;
	class ShaderVariable;
	struct BlendState;
	struct RasterizerState;
	struct DepthStencilState;
	class Shader;

	// Contains everything needed to render
	class EffectPass
	{
		friend class EffectLoader;
		friend class Effect;
	public:

		// Apply all of the shaders and variables for this effect
		void Apply();
		void Apply(DepthStencilState* ds, BlendState* bs, RasterizerState* rs);

		// Create an input layout from the vertex shader
		HRESULT CreateInputLayout(const D3D11_INPUT_ELEMENT_DESC desc[], uint32 numElements, ID3D11InputLayout** pLayout);

		// Get the name
		inline const String& Name() const { return _name; }

		// Get the depth stencil state
		inline DepthStencilState* GetDepthStencilState() { return _depthStencilState; }

		// Get the blend state
		inline BlendState* GetBlendState() { return _blendState; }

		// Get the rasterizer state
		inline RasterizerState* GetRasterizerState() { return _rasterizerState; }

		// Get the ID
		inline uint32 GetID() const { return _id; }

	private:

		// Ctor
		EffectPass(Renderer* graphics);

		String				_name;										// Name for this pass
		Renderer*			_graphics;									// Graphics engine
		Shader*				_shaders[(uint32)ShaderType::NumTypes];		// Shaders
		BlendState*			_blendState;								// BlendState
		RasterizerState*	_rasterizerState;							// RasterizerState
		DepthStencilState*	_depthStencilState;							// DepthStencilState
		FLOAT				_blendFactor[4];							// Blend factors
		uint32				_sampleMask;								// Stencil mask
		uint32				_stencilRef;								// Stencil ref
		uint32				_id;										// Identifier for mesh sorting
	};


	// Effect
	// Manages an effect, which consists of shaders and render states
	class Effect
	{
		friend class EffectLoader;
		friend class Renderer;

	public:

		// Load in a file
		bool AddMFX(const char* file);

		// Recompile all shaders
		bool Reload();

		// Optimizes memory usage, clears some reflection and file data
		void Optimize();

		// Return a pass by name
		EffectPass* GetPassByName(const char* name);

		// Return a pass by index
		inline EffectPass* GetPassByIndex(uint32 i){ return _passes[i]; }

		// Get the name
		inline const String& GetName() const { return _name; }

		// Number of constant buffers
		inline uint32 NumConstantBuffers() const {return _constantBuffers.Size(); }

		// Returns a constant buffer, or nullptr if it does not exist
		ConstantBuffer* GetConstantBufferByName(const char* name);

		// Returns a constant buffer, or nullptr if it does not exist
		inline ConstantBuffer* GetConstantBufferByIndex(uint32 i){ return (i<_constantBuffers.Size()) ? _constantBuffers[i] : nullptr; }

		// Number of variables
		inline uint32 GetNumVariables() const { return _variables.Size(); }

		// Number of shaders
		inline uint32 GetNumShaders()const { return _shaders.Size(); }

		// Get the variable by index
		inline ShaderVariable* GetVariableByIndex(uint32 i){ return (i<_variables.Size()) ? _variables[i] : nullptr; }

		// Get a variable by name
		ShaderVariable* GetVariableByName(const char* name);

		// Get a shader by index 
		inline Shader* GetShaderByIndex(uint32 i){ return _shaders[i]; }

		// Get a shader by name
		Shader* GetShaderByName(const char* name);

		// Dump all shader data
		void DumpShaderData(const char* file);


	private:

		// Ctor
		Effect(Renderer* graphics);

		// Dtor
		~Effect();

		// Free the data
		virtual void Release();

		// Create the shader from a file
		Shader* CreateShader(const char* fileName, ShaderType shaderType);		

		// Builds a combined list of all variables from all shaders
		HRESULT ProcessVariables();

		// Constructs the constant buffers from the reflection data
		HRESULT ProcessConstantBuffers(uint32 shaderID, D3D11_SHADER_DESC& shaderDesc);

		// Builds a combined list of resource variables from all shaders
		HRESULT ProcessResources(uint32 shaderID, D3D11_SHADER_DESC& shaderDesc);

		// Fills in bind variables that were not loaded yet during ProcessResources
		void FinalizeBindPoints();

		// Remove a variable from this effect
		void RemoveVariable(ShaderVariable* s);

		
		Renderer*				_graphics;			// Graphics engine
		Array<Shader*>			_shaders;			// Shaders
		Array<ConstantBuffer*>	_constantBuffers;	// Constant buffers
		Array<ShaderVariable*>  _variables;			// Shader variables
		Array<EffectPass*>		_passes;			// Effect passes
		String					_name;				// Name
		EffectLoader*			_loader;			// Used for loading in files
	};

}

//--------------------------------------------------------------------------------------
// File: Shader.h
//
// Interface to hlsl shaders
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "EffectTypes.h"

namespace Maoli
{
	// Forward decl
	class RenderState;
	class PipeplineResource;
	class ShaderUAV;
	struct SamplerState;

	// Forward decl
	template <class T>
	struct ShaderVariableBindPoint;

	// Shader function VTable
	struct ShaderVTable
	{
		void ( __stdcall ID3D11DeviceContext::*pSetShader)(ID3D11DeviceChild* pShader, ID3D11ClassInstance*const* ppClassInstances, uint32 NumClassInstances);
		void ( __stdcall ID3D11DeviceContext::*pSetConstantBuffers)(uint32 StartConstantSlot, uint32 NumBuffers, ID3D11Buffer *const *pBuffers);
		void ( __stdcall ID3D11DeviceContext::*pSetSamplers)(uint32 Offset, uint32 NumSamplers, ID3D11SamplerState*const* pSamplers);
		void ( __stdcall ID3D11DeviceContext::*pBindShaderResource )(UINT, UINT, ID3D11ShaderResourceView* const*);
		void ( __stdcall ID3D11DeviceContext::*pSetUnorderedAccessViews)(uint32 StartSlot, uint32 NumUAVs, ID3D11UnorderedAccessView *const *ppUnorderedAccessViews, const UINT *pUAVInitialCounts);
		HRESULT (ID3D11Device::*pCreateShader)(const void *pShaderBlob, SIZE_T ShaderBlobSize, ID3D11ClassLinkage* pClassLinkage, ID3D11DeviceChild **ppShader);
	};


	// Generic d3d11 shader, all data is ready to be passed to the video card
	class Shader
	{
	public:

		// Shader VTables for each shader type
		static ShaderVTable VTable[(uint32)ShaderType::NumTypes];

		// Ctor
		Shader();

		// Dtor
		~Shader();

		String											name;
		Array<ShaderVariableBindPoint<ConstantBuffer>> 	constantBuffers;		// Constant buffers
		Array<ShaderVariableBindPoint<ShaderResource>>	resources;				// Shader resources
		Array<ShaderVariableBindPoint<ShaderUAV>>		unorderedAccessViews;	// UAVs
		Array<ShaderVariableBindPoint<SamplerState>>	samplers;				// Shader samplers
		ID3D11DeviceChild*								d3dShader;				// D3D shader object
		ShaderType										type;					// Type of shader
		BinaryFile*										byteCode;				// Shader byte code
		ID3D11ShaderReflection*							shaderReflection;		// Shader reflection data
	};

}

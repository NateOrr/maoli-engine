//--------------------------------------------------------------------------------------
// File: EffectTypes.h
//
// Common effect types and decl
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace Maoli
{
	// Shader types
	enum class ShaderType
	{
		Vertex,
		Pixel,
		Geometry,
		Hull,
		Domain,
		Compute,

		NumTypes,
	};

	// Shader variable types
	enum class ShaderVariableType
	{
		BlendState,
		RasterizerState,
		DepthStencilState,
		Sampler,

		Scalar,
		Vector,
		Matrix,
		Struct,
		Buffer,
		ShaderResource,
		UnorderedAccessView,

		Unknown,

		NumTypes,
	};

	// Render state types
	enum class RenderStateType
	{
		BlendState,
		RasterizerState,
		DepthStencilState,

		NumTypes,
	};
}
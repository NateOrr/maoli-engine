//--------------------------------------------------------------------------------------
// File: ConstantBufferVariable.h
//
// Variable that is stored inside a buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ShaderVariable.h"

namespace Maoli
{
	// Forward decl
	class ShaderBuffer;

	class ConstantBufferVariable : public ShaderVariable
	{
	public:

		// Ctor
		ConstantBufferVariable();

		// Ctor
		ConstantBufferVariable(const char* name, ShaderBuffer* parent, uint32 offset, ShaderVariableType type, uint32 size);

		// Get the offset
		inline uint32 GetOffset(){ return _start; }

	protected:
		ShaderBuffer*		_parent;	// The constant buffer
		uint32				_start;		// Starting memory location
	};
}

//--------------------------------------------------------------------------------------
// File: EffectLoader.h
//
// Custom effect file parser
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/EffectLoader.h"

namespace Maoli
{
	// Ctor
	EffectLoader::EffectLoader()
	{
		BuildStringTables();
	}
	
	// Dtor
	EffectLoader::~EffectLoader()
	{
		Release();
	}

	// Cleanup
	void EffectLoader::Release()
	{
		_lines.Release();
		_commands.Release();
		for(int32 i=0; i<ET_MAX; i++)
			_enumTables[i].Release();
		for(int32 i=0; i<SBT_MAX; i++)
			_variableTable[i].Release();
		_specialCommands.Release();
		_filesToParse.Release();
	}


	// Splits a line up into words
	bool EffectLoader::Split(String& line, Array<String>& out)
	{
		// Split the line into parts
		out.Release();
		char command[64];
		int32 ci = 0;
		for(int32 i=0; i<line.Length(); i++)
		{
			// Ignore spaces
			if(line[i]==' ' || line[i]=='\t' || line[i]=='\n')
			{
				// If there is a command stored, add it to the list
				if(ci>0)
				{
					command[ci] = nullchar;
					out.Add(command);		
					ci=0;
				}
				continue;
			}

			// Process these single character commands
			if( line[i]==';' || line[i]=='=' || line[i]=='{' || line[i]=='}' ||
				line[i]=='<' || line[i]=='>' || line[i]=='\"')
			{
				if(ci>0)
				{
					command[ci] = nullchar;
					out.Add(command);	
				}
				out.Add(line[i]);
				ci=0;
				continue;
			}

			// Comments
			if(line[i]=='/' && line[i+1]=='/')
			{
				// If there is a command stored, add it to the list
				if(ci>0)
				{
					command[ci] = nullchar;
					out.Add(command);	
					ci=0;
				}
				return true;
			}
			
			// Continue to store the command
			command[ci++] = line[i];
		}

		// Add the leftover command
		if(ci>0)
		{
			command[ci] = nullchar;
			out.Add(command);	
			ci=0;
		}

		return true;
	}

	// Parses the file
	bool EffectLoader::Parse(String& fileName)
	{
		// Open the file
		std::ifstream file(fileName.c_str());
		if(!file.is_open())
			return nullptr;

		// Load in each line, skipping commented lines
		uint32 numLines = (uint32)std::count(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(), '\n')+1;
		file.seekg(0, std::ios_base::beg);
		_lines.Allocate(numLines);
		char line[128];
		for(uint32 i=0; i<_lines.Size(); i++)
		{	
			line[0] = nullchar;
			file.getline(line, 128);
			if(strlen(line)>0)
				_lines[i] = line;
		}
		file.close();

		// Process each line
		for(_lineIndex=0; _lineIndex<_lines.Size(); _lineIndex++)
		{
			// Get the command list
			Split(_lines[_lineIndex], _commands);

			// Skip commented lines
			if(_commands.Size()==0)
				continue;

			// Skip preprocessor commands
			if(CheckPreprossor())
				continue;

			// Validate the state block
			StateBlockType type = ValidateStateBlock(_commands[0]);
			if(type==SBT_MAX)
			{
				String err = "Syntax Error: \'";
				err += _commands[0];
				err += "\' is unrecognized, object keyword expected";
				MessageBoxA(nullptr, err, "MFX File Error", MB_OK);
				return false;
			}

			// Read the state block
			if(!ReadStateBlock(type))
				return false;
		}

		
		return true;
	}

	// Processes special commands and directives
	bool EffectLoader::Preprocessor(String& fileName)
	{
		// Open the file
		std::ifstream file(fileName.c_str());
		if(!file.is_open())
			return nullptr;

		// Load in each line, skipping commented lines
		uint32 numLines = (uint32)std::count(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(), '\n')+1;
		file.seekg(0, std::ios_base::beg);
		_lines.Allocate(numLines);
		char line[128];
		for(uint32 i=0; i<_lines.Size(); i++)
		{	
			line[0] = nullchar;
			file.getline(line, 128);
			if(strlen(line)>0)
				_lines[i] = line;
		}
		file.close();

		// Check the file for preprocessor commands
		for(_lineIndex=0; _lineIndex<_lines.Size(); _lineIndex++)
		{
			// Get the command list
			Split(_lines[_lineIndex], _commands);

			// Skip commented and empty lines
			if(_commands.Size()==0)
				continue;

			// Check for commands
			for(uint32 i=0; i<_specialCommands.Size(); i++)
			{
				if(_commands[0]==_specialCommands[i].string)
				{
					if((this->*_specialCommands[i].command)())
						break;
					else
						return false;
				}
			}
		}
		return true;
	}
	
	// Used to skip preprocessor commands during the actual parse
	bool EffectLoader::CheckPreprossor()
	{
		// Check for commands
		for(uint32 i=0; i<_specialCommands.Size(); i++)
		{
			if(_commands[0]==_specialCommands[i].string)
				return true;
		}
		return false;
	}

	// Includes another file to parse
	bool EffectLoader::IncludeFile()
	{
		// Check the syntax
		if( _commands.Size()==4 &&
			(_commands[1]=='\"' && _commands[3]=='\"') ||
			(_commands[1]=='<' && _commands[3]=='>') )
		{
			_filesToParse.Insert(_commands[2], _filesToParse.Size()-1);
			return true;
		}
		MessageBoxA(nullptr, "Invalid #include syntax", "MFX File Error", MB_OK);
		return false;
	}

	// Validates a stateblock keyword
	EffectLoader::StateBlockType EffectLoader::ValidateStateBlock(String& str)
	{
		for(int32 i=0; i<SBT_MAX; i++)
			if(str==_blockTable[i].String)
				return (StateBlockType)i;
		return SBT_MAX;
	}

	// Makes sure that a duplicate name is not used
	bool EffectLoader::ValidateName(String& str, StateBlockType type)
	{
		// Check the states in the effect
		if(type==SBT_EFFECT)
			return (_effect->GetPassByName(str)==nullptr);

		ShaderVariable* s = _effect->GetVariableByName(str);
		return !(s && (int32)s->GetType()==(int32)type);
	}


	// Process a new state variable
	bool EffectLoader::ReadStateBlock(StateBlockType blockType)
	{
		// Verify the block name is unique
		if(!ValidateName(_commands[1], blockType))
		{
			String err = "Identifier already defined: \'";
			MessageBoxA(nullptr, err+_commands[1]+"\'", "MFX File Error", MB_OK);
			return false;
		}

		// Clear the desc
		(this->*_blockTable[blockType].Clear)();

		// Verify syntax
		String name = _commands[1];
		Split(_lines[++_lineIndex], _commands);
		if(_commands[0][0]!='{')
		{
			MessageBoxA(nullptr, "Syntax Error: { Expected", "MFX File Error", MB_OK);
			return false;
		}

		// Fill the state desc
		while(1)
		{
			// Parse the next line
			Split(_lines[++_lineIndex], _commands);
			
			// Skip commented or empty lines
			if(_commands.Size()==0)
				continue;

			// Check if this block has ended
			if(_commands[0][0]=='}')
			{
				if(_commands.Size()!=2)
				{
					MessageBoxA(nullptr, "Syntax Error: Text after } is not valid", "MFX File Error", MB_OK);
					return false;
				}
				else if(_commands[1][0]!=';')
				{
					MessageBoxA(nullptr, "Syntax Error: ; Expected", "MFX File Error", MB_OK);
					return false;
				}
				break;
			}


			// Verify syntax
			if(_commands.Size()==4)
			{
				if(_commands[1][0]!='=')
				{
					MessageBoxA(nullptr, "Syntax Error: Assignment Operator Expected", "MFX File Error", MB_OK);
					return false;
				}

				if(_commands[3][0]!=';')
				{
					MessageBoxA(nullptr, "Syntax Error: ; Expected", "MFX File Error", MB_OK);
					return false;
				}
			}
			else
			{
				MessageBoxA(nullptr, "Syntax Error: Property Assignment Expected", "MFX File Error", MB_OK);
				return false;
			}
			

			// Process the assignment
			if(!AssignVariable(blockType))
				return false;
		}

		// Create the state
		return (this->*_blockTable[blockType].Create)(name);
	}

	// Create a Blend state
	bool EffectLoader::CreateBlendState(String& name)
	{
		BlendState* s = new BlendState(name, _blendDesc);
		if(FAILED(_effect->_graphics->GetDevice()->CreateBlendState(&_blendDesc, &s->D3DObject)))
		{
			MessageBoxA(nullptr, "Unable to create Blend state", "MFX File Error", MB_OK);
			return false;
		}
		_effect->_variables.Add(s);
		return true;
	}

	// Create a Rasterizer state
	bool EffectLoader::CreateRasterizerState(String& name)
	{
		RasterizerState* s = new RasterizerState(name, _rasterizerDesc);
		if(FAILED(_effect->_graphics->GetDevice()->CreateRasterizerState(&_rasterizerDesc, &s->D3DObject)))
		{
			MessageBoxA(nullptr, "Unable to create Rasterizer state", "MFX File Error", MB_OK);
			return false;
		}
		_effect->_variables.Add(s);
		return true;
	}

	// Create a DepthStencil state
	bool EffectLoader::CreateDepthStencilState(String& name)
	{
		DepthStencilState* s = new DepthStencilState(name, _depthStencilDesc);
		if(FAILED(_effect->_graphics->GetDevice()->CreateDepthStencilState(&_depthStencilDesc, &s->D3DObject)))
		{
			MessageBoxA(nullptr, "Unable to create DepthStencil state", "MFX File Error", MB_OK);
			return false;
		}
		_effect->_variables.Add(s);
		return true;
	}

	// Create a Sampler state
	bool EffectLoader::CreateSamplerState(String& name)
	{
		// Create the sampler state
		SamplerState* s = new SamplerState(name, _samplerDesc);
		if(FAILED(_effect->_graphics->GetDevice()->CreateSamplerState(&_samplerDesc, &s->D3DObject)))
		{
			MessageBoxA(nullptr, "Unable to create SamplerState", "MFX File Error", MB_OK);
			return false;
		}
		_effect->_variables.Add(s);
		return true;
	}

	
	// Create an effect pass
	bool EffectLoader::CreateEffectPass(String& name)
	{
		// Validate each shader
		EffectPass* p = new EffectPass(_effect->_graphics);
		p->_name = name;
		p->_graphics = _effect->_graphics;
		for(int32 i=0; i<(int32)ShaderType::NumTypes; i++)
		{
			// Skip empty shaders
			if(_effectDesc.shaders[i]==String::none)
				continue;

			// Check if this shader is already loaded
			Shader* s = _effect->GetShaderByName(_effectDesc.shaders[i]);
			if(s==nullptr)
			{
				// This shader does not exist, so we must create it
				s = _effect->CreateShader( _effectDesc.shaders[i], (ShaderType)i);
				if(!s)
				{
					ErrorMessage("Unable to create effect pass %s", _effectDesc.shaders[i].c_str());
					delete p;
					return false;
				}
			}

			// Add this to this effect pass
			p->_shaders[i] = s;
		}

		// Validate the render states
		for(int32 i=0; i<(int32)RenderStateType::NumTypes; i++)
		{
			// Skip empty states
			if(_effectDesc.renderStates[i]==String::none)
				_effectDesc.renderStates[i] = _defaultRenderStates[i];

			// Check if this state exists
			ShaderVariable* s = _effect->GetVariableByName(_effectDesc.renderStates[i]);
			if(!s)
			{
				String err = _blockTable[i].String + " '" + _effectDesc.renderStates[i];
				MessageBoxA(nullptr, err + "' not found.", "MFX File Error", MB_OK);
				delete p;
				return false;
			}

			// Make sure the state type is correct
			if(_blockTable[i].VarType != s->GetType())
			{
				String err = "\'";
				err += _effectDesc.renderStates[i];
				err += "\' is not a valid ";
				err += _blockTable[i].String;
				MessageBoxA(nullptr, err, "MFX File Error", MB_OK);
				delete p;
				return false;
			}

			// Add to this effect pass
			switch (s->GetType())
			{
			case ShaderVariableType::BlendState:
				p->_blendState = s->AsBlendState();
				break;

			case ShaderVariableType::RasterizerState:
				p->_rasterizerState = s->AsRasterizerState();
				break;

			case ShaderVariableType::DepthStencilState:
				p->_depthStencilState = s->AsDepthStencilState();
				break;
			}
		}
		
		// Add all of these shaders to the effect, along with the pass
		p->_id = _effect->_passes.Size();
		_effect->_passes.Add(p);
		for(int32 i=0; i<(int32)ShaderType::NumTypes; i++)
		{
			if(p->_shaders[i] && !_effect->GetShaderByName(p->_shaders[i]->name))
				_effect->_shaders.Add(p->_shaders[i]);
		}

		return true;
	}

	// Process a variable assignment
	bool EffectLoader::AssignVariable(StateBlockType type)
	{
		// Locate the variable in the string table
		for(uint32 i=0; i<_variableTable[type].Size(); i++)
		{
			// If there is a match, process this assignment
			if(_commands[0] == _variableTable[type][i].string)
			{
				// Handle the different variable types
				switch((AssignmentType)_variableTable[type][i].value)
				{
				case AT_BOOL:
					return AssignBool(*(bool*)(_variableTable[type][i].variable), _commands[2]);

				case AT_INT:
					return AssignInt(*(int32*)(_variableTable[type][i].variable), _commands[2]);

				case AT_BYTE:
					return AssignByte(*(UINT8*)(_variableTable[type][i].variable), _commands[2]);

				case AT_FLOAT:
					return AssignFloat(*(float*)(_variableTable[type][i].variable), _commands[2]);

				case AT_ENUM:
					return AssignEnum(*(int32*)(_variableTable[type][i].variable), _commands[2], _variableTable[type][i].type);

				case AT_STRING:
					return AssignString(*(String*)(_variableTable[type][i].variable), _commands[2]);

				default:
					MessageBoxA(nullptr, "Internal Error: AssignVariable() Failure", "MFX File Error", MB_OK);
					return false;
				}
			}
		}
		String error = "Syntax Error: Unrecognized Variable Name: ";
		error += _commands[0];
		MessageBoxA(nullptr, error, "MFX File Error", MB_OK);
		return false;
	}


	// Assigns a boolean var
	bool EffectLoader::AssignBool(bool& var, String& str)
	{
		// Parse the string for the boolean value
		if(str=="TRUE" || str=="true")
			var = true;
		else if(str=="FALSE" || str=="false")
			var = false;
		else
		{
			MessageBoxA(nullptr, "Syntax Error: Boolean value expected", "MFX File Error", MB_OK);
			return false;
		}
		return true;
	}

	// Assign a float value
	bool EffectLoader::AssignFloat(float& var, String& str)
	{
		var = (float)atof(str);
		return true;
	}

	// Assign a int32 value
	bool EffectLoader::AssignInt(int32& var, String& str)
	{
		var = atoi(str);
		return true;
	}

	// Assign a byte value
	bool EffectLoader::AssignByte(UINT8& var, String& str)
	{
		// Search the table for a matching value
		for(uint32 i=0; i<_enumTables[ET_WRITE_ENABLE].Size(); i++)
		{
			if(str==_enumTables[ET_WRITE_ENABLE][i].string)
			{
				var = (UINT8)_enumTables[ET_WRITE_ENABLE][i].value;
				return true;
			}
		}
		MessageBoxA(nullptr, "Syntax Error: Invalid Assignment Value", "MFX File Error", MB_OK);
		return false;
	}

	// Assign a string value
	bool EffectLoader::AssignString(String& var, String& str)
	{
		var = str;
		return true;
	}

	// Assign an enum
	bool EffectLoader::AssignEnum(int32& var, String& str, EnumType et)
	{
		// Search the table for a matching value
		for(uint32 i=0; i<_enumTables[et].Size(); i++)
		{
			if(str==_enumTables[et][i].string)
			{
				var = _enumTables[et][i].value;
				return true;
			}
		}
		String err = "Syntax Error: Invalid Assignment Value: ";
		err += str;
		MessageBoxA(nullptr, err, "MFX File Error", MB_OK);
		return false;
	}

	// Reset the state desc
	void EffectLoader::ClearBlendState()
	{
		_blendDesc.AlphaToCoverageEnable = false;
		_blendDesc.IndependentBlendEnable = false;
		for(int32 i=0; i<8; i++)
		{
			_blendDesc.RenderTarget[i].BlendEnable = false;
			_blendDesc.RenderTarget[i].BlendOp = D3D11_BLEND_OP_ADD;
			_blendDesc.RenderTarget[i].SrcBlend = D3D11_BLEND_ONE;
			_blendDesc.RenderTarget[i].DestBlend = D3D11_BLEND_ONE;
			_blendDesc.RenderTarget[i].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			_blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;
			_blendDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ONE;
			_blendDesc.RenderTarget[i].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		}
	}

	// Reset the state desc
	void EffectLoader::ClearRasterizerState()
	{
		_rasterizerDesc.FillMode = D3D11_FILL_SOLID;
		_rasterizerDesc.CullMode = D3D11_CULL_BACK;
		_rasterizerDesc.FrontCounterClockwise = false;
		_rasterizerDesc.DepthBias = 0;
		_rasterizerDesc.SlopeScaledDepthBias = 0.0f;
		_rasterizerDesc.DepthBiasClamp = 0.0f;
		_rasterizerDesc.DepthClipEnable = true;
		_rasterizerDesc.ScissorEnable = false;
		_rasterizerDesc.MultisampleEnable = false;
		_rasterizerDesc.AntialiasedLineEnable = false;
	}

	// Reset the state desc
	void EffectLoader::ClearDepthStencilState()
	{
		_depthStencilDesc.DepthEnable = true;
		_depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		_depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		_depthStencilDesc.StencilEnable = false;
		_depthStencilDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
		_depthStencilDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
		_depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		_depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		_depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		_depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
		_depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		_depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		_depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		_depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	}

	// Reset the state desc
	void EffectLoader::ClearSamplerState()
	{
		_samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		_samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		_samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		_samplerDesc.BorderColor[3] = D3D11_DEFAULT_BORDER_COLOR_COMPONENT;
		_samplerDesc.BorderColor[2] = D3D11_DEFAULT_BORDER_COLOR_COMPONENT;
		_samplerDesc.BorderColor[1] = D3D11_DEFAULT_BORDER_COLOR_COMPONENT;
		_samplerDesc.BorderColor[0] = D3D11_DEFAULT_BORDER_COLOR_COMPONENT;
		_samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		_samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		_samplerDesc.MaxAnisotropy = (UINT32) D3D11_DEFAULT_MAX_ANISOTROPY;
		_samplerDesc.MipLODBias = D3D11_DEFAULT_MIP_LOD_BIAS;
		_samplerDesc.MinLOD = -FLT_MAX;
		_samplerDesc.MaxLOD = FLT_MAX;
	}

	// Reset the state desc
	void EffectLoader::ClearEffect()
	{
		for(int32 i=0; i<(int32)ShaderType::NumTypes; i++)
			_effectDesc.shaders[i] = String::none;
		for(int32 i=0; i<(int32)RenderStateType::NumTypes; i++)
			_effectDesc.renderStates[i] = String::none;
	}

	// Init the enum string tables
	void EffectLoader::BuildStringTables()
	{
		// D3D11_BLEND
		int32 i=0;
		_enumTables[ET_BLEND].Allocate(17);
		_enumTables[ET_BLEND][i++] = StringKey( "ZERO", D3D11_BLEND_ZERO );
		_enumTables[ET_BLEND][i++] = StringKey( "ONE", D3D11_BLEND_ONE );
		_enumTables[ET_BLEND][i++] = StringKey( "SRC_COLOR", D3D11_BLEND_SRC_COLOR );
		_enumTables[ET_BLEND][i++] = StringKey( "INV_SRC_COLOR", D3D11_BLEND_INV_SRC_COLOR );
		_enumTables[ET_BLEND][i++] = StringKey( "SRC_ALPHA", D3D11_BLEND_SRC_ALPHA );
		_enumTables[ET_BLEND][i++] = StringKey( "INV_SRC_ALPHA", D3D11_BLEND_INV_SRC_ALPHA );
		_enumTables[ET_BLEND][i++] = StringKey( "DEST_ALPHA", D3D11_BLEND_DEST_ALPHA );
		_enumTables[ET_BLEND][i++] = StringKey( "INV_DEST_ALPHA", D3D11_BLEND_INV_DEST_ALPHA );
		_enumTables[ET_BLEND][i++] = StringKey( "DEST_COLOR", D3D11_BLEND_DEST_COLOR );
		_enumTables[ET_BLEND][i++] = StringKey( "INV_DEST_COLOR", D3D11_BLEND_INV_DEST_COLOR );
		_enumTables[ET_BLEND][i++] = StringKey( "SRC_ALPHA_SAT", D3D11_BLEND_SRC_ALPHA_SAT );
		_enumTables[ET_BLEND][i++] = StringKey( "BLEND_FACTOR", D3D11_BLEND_BLEND_FACTOR );
		_enumTables[ET_BLEND][i++] = StringKey( "INV_BLEND_FACTOR", D3D11_BLEND_INV_BLEND_FACTOR );
		_enumTables[ET_BLEND][i++] = StringKey( "SRC1_COLOR", D3D11_BLEND_SRC1_COLOR );
		_enumTables[ET_BLEND][i++] = StringKey( "INV_SRC1_COLOR", D3D11_BLEND_INV_SRC1_COLOR );
		_enumTables[ET_BLEND][i++] = StringKey( "SRC1_ALPHA", D3D11_BLEND_SRC1_ALPHA );
		_enumTables[ET_BLEND][i++] = StringKey( "INV_SRC1_ALPHA", D3D11_BLEND_INV_SRC1_ALPHA );

		// D3D11_BLEND_OP
		i=0;
		_enumTables[ET_BLEND_OP].Allocate(5);
		_enumTables[ET_BLEND_OP][i++] = StringKey( "ADD", D3D11_BLEND_OP_ADD );
		_enumTables[ET_BLEND_OP][i++] = StringKey( "SUBTRACT", D3D11_BLEND_OP_SUBTRACT );
		_enumTables[ET_BLEND_OP][i++] = StringKey( "REV_SUBTRACT", D3D11_BLEND_OP_REV_SUBTRACT );
		_enumTables[ET_BLEND_OP][i++] = StringKey( "MIN", D3D11_BLEND_OP_MIN );
		_enumTables[ET_BLEND_OP][i++] = StringKey( "MAX", D3D11_BLEND_OP_MAX );

		// D3D11_FILL_MODE
		i=0;
		_enumTables[ET_FILL_MODE].Allocate(2);
		_enumTables[ET_FILL_MODE][i++] = StringKey( "WIREFRAME", D3D11_FILL_WIREFRAME );
		_enumTables[ET_FILL_MODE][i++] = StringKey( "SOLID", D3D11_FILL_SOLID );

		// D3D11_CULL_MODE
		i=0;
		_enumTables[ET_CULL_MODE].Allocate(3);
		_enumTables[ET_CULL_MODE][i++] = StringKey( "NONE", D3D11_CULL_NONE );
		_enumTables[ET_CULL_MODE][i++] = StringKey( "FRONT", D3D11_CULL_FRONT );
		_enumTables[ET_CULL_MODE][i++] = StringKey( "BACK", D3D11_CULL_BACK );

		// D3D11_DEPTH_WRITE_MASK
		i=0;
		_enumTables[ET_DEPTH_WRITE_MASK].Allocate(2);
		_enumTables[ET_DEPTH_WRITE_MASK][i++] = StringKey( "ZERO", D3D11_DEPTH_WRITE_MASK_ZERO );
		_enumTables[ET_DEPTH_WRITE_MASK][i++] = StringKey( "ALL", D3D11_DEPTH_WRITE_MASK_ALL );

		// D3D11_COMPARISON_FUNC
		i=0;
		_enumTables[ET_COMPARISON_FUNC].Allocate(8);
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "NEVER", D3D11_COMPARISON_NEVER );
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "LESS", D3D11_COMPARISON_LESS );
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "EQUAL", D3D11_COMPARISON_EQUAL );
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "LESS_EQUAL", D3D11_COMPARISON_LESS_EQUAL );
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "GREATER", D3D11_COMPARISON_GREATER );
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "NOT_EQUAL", D3D11_COMPARISON_NOT_EQUAL );
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "GREATER_EQUAL", D3D11_COMPARISON_GREATER_EQUAL );
		_enumTables[ET_COMPARISON_FUNC][i++] = StringKey( "ALWAYS", D3D11_COMPARISON_ALWAYS );

		// D3D11_STENCIL_OP
		i=0;
		_enumTables[ET_STENCIL_OP].Allocate(8);
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "KEEP", D3D11_STENCIL_OP_KEEP );
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "ZERO", D3D11_STENCIL_OP_ZERO );
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "REPLACE", D3D11_STENCIL_OP_REPLACE );
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "INCR_SAT", D3D11_STENCIL_OP_INCR_SAT );
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "DECR_SAT", D3D11_STENCIL_OP_DECR_SAT );
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "INVERT", D3D11_STENCIL_OP_INVERT );
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "INCR", D3D11_STENCIL_OP_INCR );
		_enumTables[ET_STENCIL_OP][i++] = StringKey( "DECR", D3D11_STENCIL_OP_DECR );

		// D3D11_COLOR_WRITE_ENABLE
		i=0;
		_enumTables[ET_WRITE_ENABLE].Allocate(7);
		_enumTables[ET_WRITE_ENABLE][i++] = StringKey( "RED", D3D11_COLOR_WRITE_ENABLE_RED );
		_enumTables[ET_WRITE_ENABLE][i++] = StringKey( "GREEN", D3D11_COLOR_WRITE_ENABLE_GREEN );
		_enumTables[ET_WRITE_ENABLE][i++] = StringKey( "BLUE", D3D11_COLOR_WRITE_ENABLE_BLUE );
		_enumTables[ET_WRITE_ENABLE][i++] = StringKey( "ALPHA", D3D11_COLOR_WRITE_ENABLE_ALPHA );
		_enumTables[ET_WRITE_ENABLE][i++] = StringKey( "ALL", D3D11_COLOR_WRITE_ENABLE_ALL );
		_enumTables[ET_WRITE_ENABLE][i++] = StringKey( "0", 0 );
		_enumTables[ET_WRITE_ENABLE][i++] = StringKey( "0x00", 0 );

		// D3D11_FILTER
		i=0;
		_enumTables[ET_FILTER].Allocate(18);
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_MAG_MIP_POINT", D3D11_FILTER_MIN_MAG_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_MAG_POINT_MIP_LINEAR", D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_POINT_MAG_LINEAR_MIP_POINT", D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_POINT_MAG_MIP_LINEAR", D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_LINEAR_MAG_MIP_POINT", D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_LINEAR_MAG_POINT_MIP_LINEAR", D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_MAG_LINEAR_MIP_POINT", D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "MIN_MAG_MIP_LINEAR", D3D11_FILTER_MIN_MAG_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "ANISOTROPIC", D3D11_FILTER_ANISOTROPIC );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_MAG_MIP_POINT", D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_MAG_POINT_MIP_LINEAR", D3D11_FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT", D3D11_FILTER_COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_POINT_MAG_MIP_LINEAR", D3D11_FILTER_COMPARISON_MIN_POINT_MAG_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_LINEAR_MAG_MIP_POINT", D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR", D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_MAG_LINEAR_MIP_POINT", D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_MIN_MAG_MIP_LINEAR", D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR );
		_enumTables[ET_FILTER][i++] = StringKey( "COMPARISON_ANISOTROPIC", D3D11_FILTER_COMPARISON_ANISOTROPIC );

		// D3D11_TEXTURE_ADDRESS_MODE
		i=0;
		_enumTables[ET_ADDRESS_MODE].Allocate(5);
		_enumTables[ET_ADDRESS_MODE][i++] = StringKey( "Wrap", D3D11_TEXTURE_ADDRESS_WRAP );
		_enumTables[ET_ADDRESS_MODE][i++] = StringKey( "Mirror", D3D11_TEXTURE_ADDRESS_MIRROR );
		_enumTables[ET_ADDRESS_MODE][i++] = StringKey( "Clamp", D3D11_TEXTURE_ADDRESS_CLAMP );
		_enumTables[ET_ADDRESS_MODE][i++] = StringKey( "Border", D3D11_TEXTURE_ADDRESS_BORDER );
		_enumTables[ET_ADDRESS_MODE][i++] = StringKey( "MirrorOnce", D3D11_TEXTURE_ADDRESS_MIRROR_ONCE );


		// Map variable strings to their variables and assignment types

		// BlendState variables
		i=0;
		_variableTable[SBT_BLEND].Allocate(74);
		_variableTable[SBT_BLEND][i++] = StringKey( "AlphaToCoverageEnable", AT_BOOL, &_blendDesc.AlphaToCoverageEnable );
		_variableTable[SBT_BLEND][i++] = StringKey( "IndependentBlendEnable", AT_BOOL, &_blendDesc.IndependentBlendEnable);
		_variableTable[SBT_BLEND][i++] = StringKey( String("BlendEnable"), AT_BOOL, &_blendDesc.RenderTarget[0].BlendEnable );
		_variableTable[SBT_BLEND][i++] = StringKey( String("SrcBlend"), AT_ENUM, &_blendDesc.RenderTarget[0].SrcBlend, ET_BLEND );
		_variableTable[SBT_BLEND][i++] = StringKey( String("DestBlend"), AT_ENUM, &_blendDesc.RenderTarget[0].DestBlend, ET_BLEND );
		_variableTable[SBT_BLEND][i++] = StringKey( String("BlendOp"), AT_ENUM, &_blendDesc.RenderTarget[0].BlendOp, ET_BLEND_OP );
		_variableTable[SBT_BLEND][i++] = StringKey( String("SrcBlendAlpha"), AT_ENUM, &_blendDesc.RenderTarget[0].SrcBlendAlpha, ET_BLEND );
		_variableTable[SBT_BLEND][i++] = StringKey( String("DestBlendAlpha"), AT_ENUM, &_blendDesc.RenderTarget[0].DestBlendAlpha, ET_BLEND );
		_variableTable[SBT_BLEND][i++] = StringKey( String("BlendOpAlpha"), AT_ENUM, &_blendDesc.RenderTarget[0].BlendOpAlpha, ET_BLEND_OP );
		_variableTable[SBT_BLEND][i++] = StringKey( String("RenderTargetWriteMask"), AT_BYTE, &_blendDesc.RenderTarget[0].RenderTargetWriteMask );
		for(int32 aI=0; aI<8; aI++)
		{
			_variableTable[SBT_BLEND][i++] = StringKey( String("BlendEnable")+"["+aI+"]", AT_BOOL, &_blendDesc.RenderTarget[aI].BlendEnable );
			_variableTable[SBT_BLEND][i++] = StringKey( String("SrcBlend")+"["+aI+"]", AT_ENUM, &_blendDesc.RenderTarget[aI].SrcBlend, ET_BLEND );
			_variableTable[SBT_BLEND][i++] = StringKey( String("DestBlend")+"["+aI+"]", AT_ENUM, &_blendDesc.RenderTarget[aI].DestBlend, ET_BLEND );
			_variableTable[SBT_BLEND][i++] = StringKey( String("BlendOp")+"["+aI+"]", AT_ENUM, &_blendDesc.RenderTarget[aI].BlendOp, ET_BLEND_OP );
			_variableTable[SBT_BLEND][i++] = StringKey( String("SrcBlendAlpha")+"["+aI+"]", AT_ENUM, &_blendDesc.RenderTarget[aI].SrcBlendAlpha, ET_BLEND );
			_variableTable[SBT_BLEND][i++] = StringKey( String("DestBlendAlpha")+"["+aI+"]", AT_ENUM, &_blendDesc.RenderTarget[aI].DestBlendAlpha, ET_BLEND );
			_variableTable[SBT_BLEND][i++] = StringKey( String("BlendOpAlpha")+"["+aI+"]", AT_ENUM, &_blendDesc.RenderTarget[aI].BlendOpAlpha, ET_BLEND_OP );
			_variableTable[SBT_BLEND][i++] = StringKey( String("RenderTargetWriteMask")+"["+aI+"]", AT_BYTE, &_blendDesc.RenderTarget[aI].RenderTargetWriteMask );
		}

		// RasterizerState variables
		i=0;
		_variableTable[SBT_RASTERIZER].Allocate(10);
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "FillMode", AT_ENUM, &_rasterizerDesc.FillMode, ET_FILL_MODE );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "CullMode", AT_ENUM, &_rasterizerDesc.CullMode, ET_CULL_MODE );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "FrontCounterClockwise", AT_BOOL, &_rasterizerDesc.FrontCounterClockwise );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "DepthBias", AT_INT, &_rasterizerDesc.DepthBias );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "DepthBiasClamp", AT_FLOAT, &_rasterizerDesc.DepthBiasClamp );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "SlopeScaledDepthBias", AT_FLOAT, &_rasterizerDesc.SlopeScaledDepthBias );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "DepthClipEnable", AT_BOOL, &_rasterizerDesc.DepthClipEnable );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "ScissorEnable", AT_BOOL, &_rasterizerDesc.ScissorEnable );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "MultisampleEnable", AT_BOOL, &_rasterizerDesc.MultisampleEnable );
		_variableTable[SBT_RASTERIZER][i++] = StringKey( "AntialiasedLineEnable", AT_BOOL, &_rasterizerDesc.AntialiasedLineEnable );

		// DepthStencilState variables
		i=0;
		_variableTable[SBT_DEPTH_STENCIL].Allocate(14);
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "DepthEnable", AT_BOOL, &_depthStencilDesc.DepthEnable );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "DepthWriteMask", AT_ENUM, &_depthStencilDesc.DepthWriteMask, ET_DEPTH_WRITE_MASK );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "DepthFunc", AT_ENUM, &_depthStencilDesc.DepthFunc, ET_COMPARISON_FUNC );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "StencilEnable", AT_BOOL, &_depthStencilDesc.StencilEnable );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "StencilReadMask", AT_BYTE, &_depthStencilDesc.StencilReadMask );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "StencilWriteMask", AT_BYTE, &_depthStencilDesc.StencilWriteMask );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "FrontFaceStencilFail", AT_ENUM, &_depthStencilDesc.FrontFace.StencilFailOp, ET_STENCIL_OP );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "FrontFaceStencilDepthFail", AT_ENUM, &_depthStencilDesc.FrontFace.StencilDepthFailOp, ET_STENCIL_OP );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "FrontFaceStencilPass", AT_ENUM, &_depthStencilDesc.FrontFace.StencilPassOp, ET_STENCIL_OP );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "FrontFaceStencilFunc", AT_ENUM, &_depthStencilDesc.FrontFace.StencilFunc, ET_COMPARISON_FUNC );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "BackFaceStencilFail", AT_ENUM, &_depthStencilDesc.BackFace.StencilFailOp, ET_STENCIL_OP );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "BackFaceStencilDepthFail", AT_ENUM, &_depthStencilDesc.BackFace.StencilDepthFailOp, ET_STENCIL_OP );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "BackFaceStencilPass", AT_ENUM, &_depthStencilDesc.BackFace.StencilPassOp, ET_STENCIL_OP );
		_variableTable[SBT_DEPTH_STENCIL][i++] = StringKey( "BackFaceStencilFunc", AT_ENUM, &_depthStencilDesc.BackFace.StencilFunc, ET_COMPARISON_FUNC );

		// SamplerState variables
		i=0;
		_variableTable[SBT_SAMPLER].Allocate(13);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "Filter", AT_ENUM, &_samplerDesc.Filter, ET_FILTER);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "AddressU", AT_ENUM, &_samplerDesc.AddressU, ET_ADDRESS_MODE);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "AddressV", AT_ENUM, &_samplerDesc.AddressV, ET_ADDRESS_MODE);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "AddressW", AT_ENUM, &_samplerDesc.AddressW, ET_ADDRESS_MODE);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "MipLODBias", AT_FLOAT, &_samplerDesc.MipLODBias);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "MaxAnisotropy", AT_INT, &_samplerDesc.MaxAnisotropy);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "ComparisonFunc", AT_ENUM, &_samplerDesc.ComparisonFunc, ET_COMPARISON_FUNC);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "BorderColor[0]", AT_FLOAT, &_samplerDesc.BorderColor[0]);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "BorderColor[1]", AT_FLOAT, &_samplerDesc.BorderColor[1]);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "BorderColor[2]", AT_FLOAT, &_samplerDesc.BorderColor[2]);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "BorderColor[3]", AT_FLOAT, &_samplerDesc.BorderColor[3]);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "MinLOD", AT_FLOAT, &_samplerDesc.MinLOD);
		_variableTable[SBT_SAMPLER][i++] = StringKey( "MaxLOD", AT_FLOAT, &_samplerDesc.MaxLOD);


		// Effect variables
		i=0;
		_variableTable[SBT_EFFECT].Allocate(9);
		_variableTable[SBT_EFFECT][i++] = StringKey( "VertexShader", AT_STRING, &_effectDesc.shaders[(int32)ShaderType::Vertex] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "PixelShader", AT_STRING, &_effectDesc.shaders[(int32)ShaderType::Pixel] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "GeometryShader", AT_STRING, &_effectDesc.shaders[(int32)ShaderType::Geometry] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "HullShader", AT_STRING, &_effectDesc.shaders[(int32)ShaderType::Hull] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "DomainShader", AT_STRING, &_effectDesc.shaders[(int32)ShaderType::Domain] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "ComputeShader", AT_STRING, &_effectDesc.shaders[(int32)ShaderType::Compute] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "BlendState", AT_STRING, &_effectDesc.renderStates[(int32)RenderStateType::BlendState] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "RasterizerState", AT_STRING, &_effectDesc.renderStates[(int32)RenderStateType::RasterizerState] );
		_variableTable[SBT_EFFECT][i++] = StringKey( "DepthStencilState", AT_STRING, &_effectDesc.renderStates[(int32)RenderStateType::DepthStencilState] );

		// Block types
		i=0;
		_blockTable[i++] = BlockStringKey("BlendState", SBT_BLEND, ShaderVariableType::BlendState, &EffectLoader::CreateBlendState, &EffectLoader::ClearBlendState);
		_blockTable[i++] = BlockStringKey("RasterizerState", SBT_RASTERIZER, ShaderVariableType::RasterizerState, &EffectLoader::CreateRasterizerState, &EffectLoader::ClearRasterizerState);
		_blockTable[i++] = BlockStringKey("DepthStencilState", SBT_DEPTH_STENCIL, ShaderVariableType::DepthStencilState, &EffectLoader::CreateDepthStencilState, &EffectLoader::ClearDepthStencilState);
		_blockTable[i++] = BlockStringKey("SamplerState", SBT_SAMPLER, ShaderVariableType::Sampler, &EffectLoader::CreateSamplerState, &EffectLoader::ClearSamplerState);
		_blockTable[i++] = BlockStringKey("Effect", SBT_EFFECT, ShaderVariableType::Unknown, &EffectLoader::CreateEffectPass, &EffectLoader::ClearEffect);

		// Special commands
		i=0;
		_specialCommands.Allocate(1);
		_specialCommands[i++] = CommandKey("#include", &EffectLoader::IncludeFile);

		// Render state defaults
		_defaultRenderStates[SBT_BLEND] = "DefaultBS";
		_defaultRenderStates[SBT_RASTERIZER] = "DefaultRS";
		_defaultRenderStates[SBT_DEPTH_STENCIL] = "DefaultDS";

	}

	// Loads a .mfx file
	bool EffectLoader::LoadMFX( const char* name, Effect* effect)
	{
		// Setup for parsing
		_filesToParse.Release();
		_filesToParse.Add(name);

		// Run the preprocessor only on the main file
		if(!Preprocessor(_filesToParse[0]))
			return nullptr;

		// Now parse the files
		_effect = effect;
		for(uint32 i=0; i<_filesToParse.Size(); i++)
		{
			if(!Parse(_filesToParse[i]))
			{
				return false;
			}
		}

		// Finalize the effect setup
		_commands.Release();
		if(FAILED(_effect->ProcessVariables()))
		{
			MessageBoxA(nullptr, "Effect Error: ProcessVariables() Failed", "MFX File Error", MB_OK);
			return false;
		}
		return true;
	}

}

//--------------------------------------------------------------------------------------
// File: Shader.cpp
//
// Interface to hlsl shaders
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "Maoli.h"
#include "Effect/Shader.h"

namespace Maoli
{
	// Shader VTables for each shader type
	ShaderVTable Shader::VTable[(int32)ShaderType::NumTypes] =
	{
		// Vertex Shaders
		{
				(void (__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChild*, ID3D11ClassInstance*const*, uint32)) &ID3D11DeviceContext::VSSetShader,
				&ID3D11DeviceContext::VSSetConstantBuffers,
				&ID3D11DeviceContext::VSSetSamplers,
				&ID3D11DeviceContext::VSSetShaderResources,
				nullptr,
				(HRESULT (ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreateVertexShader
		},

		// Pixel Shaders
		{
				(void (__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChild*, ID3D11ClassInstance*const*, uint32)) &ID3D11DeviceContext::PSSetShader,
				&ID3D11DeviceContext::PSSetConstantBuffers,
				&ID3D11DeviceContext::PSSetSamplers,
				&ID3D11DeviceContext::PSSetShaderResources,
				nullptr,
				(HRESULT (ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreatePixelShader
		},

		// Geometry Shaders
		{
				(void (__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChild*, ID3D11ClassInstance*const*, uint32)) &ID3D11DeviceContext::GSSetShader,
				&ID3D11DeviceContext::GSSetConstantBuffers,
				&ID3D11DeviceContext::GSSetSamplers,
				&ID3D11DeviceContext::GSSetShaderResources,
				nullptr,
				(HRESULT (ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreateGeometryShader
		},

		// Hull Shaders
		{
				(void (__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChild*, ID3D11ClassInstance*const*, uint32)) &ID3D11DeviceContext::HSSetShader,
				&ID3D11DeviceContext::HSSetConstantBuffers,
				&ID3D11DeviceContext::HSSetSamplers,
				&ID3D11DeviceContext::HSSetShaderResources,
				nullptr,
				(HRESULT (ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreateHullShader
		},

		// Domain Shaders
		{
				(void (__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChild*, ID3D11ClassInstance*const*, uint32)) &ID3D11DeviceContext::DSSetShader,
				&ID3D11DeviceContext::DSSetConstantBuffers,
				&ID3D11DeviceContext::DSSetSamplers,
				&ID3D11DeviceContext::DSSetShaderResources,
				nullptr,
				(HRESULT (ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreateDomainShader
		},

		// Compute Shaders
		{
				(void (__stdcall ID3D11DeviceContext::*)(ID3D11DeviceChild*, ID3D11ClassInstance*const*, uint32)) &ID3D11DeviceContext::CSSetShader,
				&ID3D11DeviceContext::CSSetConstantBuffers,
				&ID3D11DeviceContext::CSSetSamplers,
				&ID3D11DeviceContext::CSSetShaderResources,
				&ID3D11DeviceContext::CSSetUnorderedAccessViews,
				(HRESULT (ID3D11Device::*)(const void *, SIZE_T, ID3D11ClassLinkage*, ID3D11DeviceChild **)) &ID3D11Device::CreateComputeShader
		},
				
	};


	// Ctor
	Shader::Shader()
	{
		d3dShader = nullptr;
		byteCode = nullptr;
		shaderReflection = nullptr;
		name = String::none;
	}


	// Dtor
	Shader::~Shader()
	{
		constantBuffers.Release();
		resources.Release();
		samplers.Release();
		SafeRelease(d3dShader);
		SafeDelete(byteCode);
		SafeRelease(shaderReflection);
	}



}

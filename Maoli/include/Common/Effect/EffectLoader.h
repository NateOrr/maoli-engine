//--------------------------------------------------------------------------------------
// File: EffectLoader.h
//
// Custom effect file parser
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "EffectTypes.h"

namespace Maoli
{

	// Forward decl
	class Engine;
	class Effect;

	// Handles the loading and processing of Maoli Effect Files (.mfx)
	class EffectLoader
	{
		// Only the effect class can use this
		friend class Effect;

	public:

		// Loads a .mfx file
		bool LoadMFX(const char* name, Effect* effect);

		// Cleanup
		void Release();


	private:

		// Types of state blocks supported
		enum StateBlockType
		{
			SBT_BLEND,
			SBT_RASTERIZER,
			SBT_DEPTH_STENCIL,
			SBT_SAMPLER,
			SBT_EFFECT,

			SBT_MAX
		};

		// Ctor
		EffectLoader();

		// Dtor
		~EffectLoader();

		// Parses the file
		bool Parse(String& fileName);

		// Splits a line up into words
		bool Split(String& line, Array<String>& out);

		// Process a new state variable
		bool ReadStateBlock(StateBlockType blockType);

		// Makes sure that a duplicate name is not used
		bool ValidateName(String& str, StateBlockType type);

		// Validates a stateblock keyword
		StateBlockType ValidateStateBlock(String& str);

		// Create a new state variable
		bool CreateBlendState(String& name);
		bool CreateRasterizerState(String& name);
		bool CreateDepthStencilState(String& name);
		bool CreateSamplerState(String& name);
		bool CreateEffectPass(String& name);

		// Reset the state desc
		void ClearBlendState();
		void ClearRasterizerState();
		void ClearDepthStencilState();
		void ClearSamplerState();
		void ClearEffect();

		// Assignment types
		enum AssignmentType
		{
			AT_BOOL,
			AT_FLOAT,
			AT_INT,
			AT_BYTE,
			AT_STRING,
			AT_ENUM
		};

		// Assign a boolean value
		bool AssignBool(bool& var, String& str);

		// Assign a float value
		bool AssignFloat(float& var, String& str);

		// Assign a int32 value
		bool AssignInt(int32& var, String& str);

		// Assign a byte value
		bool AssignByte(UINT8& var, String& str);

		// Assign a string value
		bool AssignString(String& var, String& str);

		// Assign the actual variable
		bool AssignVariable(StateBlockType type);

		// Includes another file to parse
		bool IncludeFile();

		// Processes special commands and directives
		// Only available to the main file
		bool Preprocessor(String& fileName);

		// Used to skip preprocessor commands during the actual parse
		bool CheckPreprossor();

		Array<String>	_lines;			// The lines of the 
		uint32			_lineIndex;		// Current line in the parse
		Array<String>	_commands;		// Command list
		Effect*			_effect;		// The effect to load into
		Array<String>	_filesToParse;	// The files that need to be loaded
		
		// Working descs
		D3D11_BLEND_DESC			_blendDesc;
		D3D11_RASTERIZER_DESC		_rasterizerDesc;
		D3D11_DEPTH_STENCIL_DESC	_depthStencilDesc;
		D3D11_SAMPLER_DESC			_samplerDesc;

		// Init the enum string tables
		void BuildStringTables();


		// Effect pass loading
		struct EffectDesc
		{
			String name;
			String shaders[ShaderType::NumTypes];
			String renderStates[RenderStateType::NumTypes];
		} _effectDesc;


		// Enum table ids
		enum EnumType
		{
			ET_BLEND,
			ET_BLEND_OP,
			ET_FILL_MODE,
			ET_CULL_MODE,
			ET_DEPTH_WRITE_MASK,
			ET_COMPARISON_FUNC,
			ET_STENCIL_OP,
			ET_WRITE_ENABLE,
			ET_FILTER,
			ET_ADDRESS_MODE,

			ET_MAX
		};

		// Assign an enum
		bool AssignEnum(int32& var, String& str, EnumType et);

		// Enum/string/variable key pair
		struct StringKey
		{
			StringKey(){};

			template <class T>
			StringKey(const char* s, T v, void* p=nullptr, EnumType e=ET_MAX){
				string = s;
				value = (int32)v;
				variable = p;
				type=e;
			}

			String string;
			int32 value;		
			void* variable;
			EnumType type;
		};

		// Format for a block creation and read functions
		typedef bool(EffectLoader::*BlockNameFn)(String&);
		typedef void(EffectLoader::*BlockClearFn)();

		// Block keys
		struct BlockStringKey
		{
			// Ctor
			BlockStringKey(){}
			BlockStringKey(const char* s, StateBlockType t, ShaderVariableType v, BlockNameFn f1, BlockClearFn f3){
				String = s;
				Type = t;
				Create = f1;
				Clear = f3;
				VarType = v;
			}

			String String;
			StateBlockType Type;		
			BlockNameFn Create;
			BlockClearFn Clear;
			ShaderVariableType VarType;
		};

		// A single command
		typedef bool(EffectLoader::*CommandFn)();
		struct CommandKey
		{
			// Ctor
			CommandKey(){}
			CommandKey(const char* s, CommandFn f) : string(s), command(f) {}
			
			String		string;		// Command string
			CommandFn	command;	// Function to execute
		};

		// String tables
		Array<StringKey>	_enumTables[ET_MAX];
		Array<StringKey>	_variableTable[SBT_MAX];
		BlockStringKey		_blockTable[SBT_MAX];			// Maps block type names to the enum and a pointer to the create funtion
		Array<CommandKey>	_specialCommands;				// Single character and special commands
		String				_defaultRenderStates[3];
	};
}

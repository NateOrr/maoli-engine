//--------------------------------------------------------------------------------------
// File: Effect.h
//
// Shader framework
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/Effect.h"
#include "Effect/EffectStates.h"
#include "Effect/ShaderVariable.h"

#include "Graphics/Renderer.h"
#include "Effect/EffectLoader.h"

#include <iostream>
#include <fstream>
using std::ofstream;
using std::endl;

namespace Maoli
{
	// Init
	Effect::Effect( Renderer* graphics )
	{
		_graphics = graphics;
		_loader = new EffectLoader;
	}

	// Dtor
	Effect::~Effect()
	{
		Release();

		// Delete the loader
		delete _loader;
		_loader = nullptr;
	}

	// Free the data
	void Effect::Release()
	{
		_loader->Release();

		// Shaders
		for ( uint32 i = 0; i < _shaders.Size(); i++ )
			delete _shaders[i];
		_shaders.Release();

		// Constant buffers
		for ( uint32 i = 0; i < _constantBuffers.Size(); i++ )
			delete _constantBuffers[i];
		_constantBuffers.Release();

		// Variables
		for ( uint32 i = 0; i < _variables.Size(); i++ )
			delete _variables[i];
		_variables.Release();

		// Passes
		for ( uint32 i = 0; i < _passes.Size(); i++ )
			delete _passes[i];
		_passes.Release();
	}

	// Load in a file
	bool Effect::AddMFX( const char* file )
	{
		if ( !_loader->LoadMFX( file, this ) )
			return false;

		DumpShaderData( "shader_debug.txt" );
		return true;
	}


	// Optimizes memory usage, clears some reflection and file data
	void Effect::Optimize()
	{
		// Reflection objects and files
		for ( uint32 i = 0; i < _shaders.Size(); i++ )
		{
			SafeDelete( _shaders[i]->byteCode );
			SafeRelease( _shaders[i]->shaderReflection );
		}

		// Optimize arrays
		_shaders.Finalize();
		_constantBuffers.Finalize();
		_variables.Finalize();
		_passes.Finalize();
	}




	// Create the shader from a file
	Shader* Effect::CreateShader( const char* name, ShaderType shaderType )
	{
		// Make sure this shader is not already loaded
		Shader* s = GetShaderByName( name );
		if ( s )
			return s;

		// Shader data
		BinaryFile* file = new BinaryFile();

		// Load the file
		String fileName = String( "Resources\\Compiled Shaders\\" ) + name + ".cso";
		if ( !file->Load( fileName ) )
		{
			delete file;
			MessageBoxA( nullptr, fileName, "Failed to load shader file", MB_OK );
			return nullptr;
		}

		// Create a shader
		s = new Shader();
		s->name = name;
		s->byteCode = file;
		s->type = shaderType;
		HRESULT hr = E_FAIL;
		switch ( shaderType )
		{
			case ShaderType::Vertex:
			{
									   hr = _graphics->GetDevice()->CreateVertexShader( file->GetData(), file->GetSize(), nullptr, (ID3D11VertexShader**)&s->d3dShader );
									   break;
			}

			case ShaderType::Pixel:
			{
									  hr = _graphics->GetDevice()->CreatePixelShader( file->GetData(), file->GetSize(), nullptr, (ID3D11PixelShader**)&s->d3dShader );
									  break;
			}

			case ShaderType::Geometry:
			{
										 hr = _graphics->GetDevice()->CreateGeometryShader( file->GetData(), file->GetSize(), nullptr, (ID3D11GeometryShader**)&s->d3dShader );
										 break;
			}

			case ShaderType::Hull:
			{
									 hr = _graphics->GetDevice()->CreateHullShader( file->GetData(), file->GetSize(), nullptr, (ID3D11HullShader**)&s->d3dShader );
									 break;
			}

			case ShaderType::Domain:
			{
									   hr = _graphics->GetDevice()->CreateDomainShader( file->GetData(), file->GetSize(), nullptr, (ID3D11DomainShader**)&s->d3dShader );
									   break;
			}

			case ShaderType::Compute:
			{
										hr = _graphics->GetDevice()->CreateComputeShader( file->GetData(), file->GetSize(), nullptr, (ID3D11ComputeShader**)&s->d3dShader );
										break;
			}
		}

		if ( FAILED( hr ) )
		{
			delete s;
			return nullptr;
		}

		return s;
	}


	// Builds a combined list of all variables from all shaders
	HRESULT Effect::ProcessVariables()
	{
		// Load the reflection data for each shader
		for ( uint32 shaderID = 0; shaderID < _shaders.Size(); shaderID++ )
		{
			// Create the reflection data for this shader
			BinaryFile* f = _shaders[shaderID]->byteCode;
			if ( !f->IsLoaded() )
				return E_FAIL;
			H_RETURN( D3DReflect( f->GetData(), f->GetSize(), IID_ID3D11ShaderReflection, (void**)&_shaders[shaderID]->shaderReflection ), "Failed to reflect shader" );
		}

		// Process the variables
		D3D11_SHADER_DESC shaderDesc;
		for ( uint32 shaderID = 0; shaderID < _shaders.Size(); shaderID++ )
		{
			_shaders[shaderID]->shaderReflection->GetDesc( &shaderDesc );

			// Handle the constant buffer creation
			H_RETURN( ProcessConstantBuffers( shaderID, shaderDesc ), "Failed to ProcessConstantBuffers" );

			// Handle resource variable creation, as well as binding info
			H_RETURN( ProcessResources( shaderID, shaderDesc ), "Failed to ProcessResources" );

		}

		// Add all of the constant buffer variables to the list
		for ( uint32 i = 0; i < _constantBuffers.Size(); i++ )
		{
			for ( uint32 j = 0; j < _constantBuffers[i]->GetNumVariables(); j++ )
				_variables.Add( _constantBuffers[i]->GetVariableByIndex( j ) );
		}

		// Update any bind points that were missed earlier
		FinalizeBindPoints();

		return S_OK;
	}


	// Constructs the constant buffers from the reflection data
	HRESULT Effect::ProcessConstantBuffers( uint32 shaderID, D3D11_SHADER_DESC& shaderDesc )
	{
		// Process each constant buffer			
		D3D11_SHADER_BUFFER_DESC bufferDesc, testBufferDesc;
		ID3D11ShaderReflectionConstantBuffer* testBuffer;
		Shader* s = _shaders[shaderID];
		for ( uint32 cID = 0; cID < shaderDesc.ConstantBuffers; cID++ )
		{
			// Get the buffer desc, and make sure the same buffer is not already loaded from another shader
			s->shaderReflection->GetConstantBufferByIndex( cID )->GetDesc( &bufferDesc );
			bool skip = false;
			for ( uint32 testShaderID = 0; testShaderID < shaderID; testShaderID++ )
			{
				if ( shaderID == testShaderID || _shaders[testShaderID]->shaderReflection == nullptr )
					continue;
				D3D11_SHADER_DESC tds;
				_shaders[testShaderID]->shaderReflection->GetDesc( &tds );
				for ( uint32 tcID = 0; tcID < tds.ConstantBuffers; tcID++ )
				{
					testBuffer = _shaders[testShaderID]->shaderReflection->GetConstantBufferByIndex( tcID );
					testBuffer->GetDesc( &testBufferDesc );
					if ( strcmp( testBufferDesc.Name, bufferDesc.Name ) == 0 )
					{
						// Make sure two different buffers of the same name are not being used
						if ( testBufferDesc.Size != bufferDesc.Size )
						{
							String msg = "Two different constant buffers of the same name cannot be used within the same effect: ";
							msg += bufferDesc.Name;
							ErrorMessage( msg );
							return E_FAIL;
						}
						skip = true;
						break;
					}
				}
			}
			if ( skip )
				continue;

			// At this point, we can safely add the data for this cb
			ConstantBuffer* cb = new ConstantBuffer();
			cb->Create( _graphics->GetDevice(), s->shaderReflection->GetConstantBufferByIndex( cID ) );
			_constantBuffers.Add( cb );
		}

		return S_OK;
	}



	// Builds a combined list of resource variables from all shaders
	HRESULT Effect::ProcessResources( uint32 shaderID, D3D11_SHADER_DESC& shaderDesc )
	{
		// Process resource binding information, as well as shader resource variable and samplers
		D3D11_SHADER_INPUT_BIND_DESC bindDesc;
		Shader* s = _shaders[shaderID];
		for ( uint32 resourceID = 0; resourceID < shaderDesc.BoundResources; resourceID++ )
		{
			s->shaderReflection->GetResourceBindingDesc( resourceID, &bindDesc );

			switch ( bindDesc.Type )
			{
				// UAVs
				case D3D11_SIT_UAV_RWSTRUCTURED:
				case D3D11_SIT_UAV_APPEND_STRUCTURED:
				case D3D11_SIT_UAV_CONSUME_STRUCTURED:
				case D3D11_SIT_UAV_RWSTRUCTURED_WITH_COUNTER:
				case D3D_SIT_UAV_RWTYPED:
				{
											// Setup the variable
											ShaderVariable* uavVariable = GetVariableByName( bindDesc.Name );

											// Setup the UAV if needed
											if ( !uavVariable )
											{
												uavVariable = new ShaderUAV( bindDesc.Name );
												_variables.Add( uavVariable );
											}

											// Setup the bind point in the shader
											int32 id = s->unorderedAccessViews.Size();
											s->unorderedAccessViews.Add();
											s->unorderedAccessViews[id].bindPoint = bindDesc.BindPoint;
											s->unorderedAccessViews[id].bindCount = bindDesc.BindCount;
											s->unorderedAccessViews[id].variable = uavVariable->AsUnorderedAccessView();
											s->unorderedAccessViews[id].bindingName = bindDesc.Name;
				}
					break;

					// Add the constant buffer binding info to the shader
				case D3D10_SIT_CBUFFER:
				{
										  int32 id = s->constantBuffers.Size();
										  s->constantBuffers.Add();
										  s->constantBuffers[id].bindPoint = bindDesc.BindPoint;
										  s->constantBuffers[id].bindCount = 1;
										  s->constantBuffers[id].variable = GetConstantBufferByName( bindDesc.Name );
				}
					break;

					// Create a variable for this texture, and add the binding info to the shader
				case D3D10_SIT_TEXTURE:
				case D3D11_SIT_STRUCTURED:
				{
											 // If this is an array, we need some extra steps
											 int32 arrayIndex;
											 String baseName = String( bindDesc.Name ).RemoveArrayTags( &arrayIndex );

											 // Need to figure out the number of elements
											 if ( arrayIndex <= 0 )
											 {
												 int32 bindCount = std::max( arrayIndex, 0 );
												 if ( arrayIndex == 0 )
												 {
													 int32 testIndex;
													 String testName;
													 D3D11_SHADER_INPUT_BIND_DESC testBindDesc;
													 D3D11_SHADER_DESC testShaderDesc;
													 for ( uint32 i = 0; i < _shaders.Size(); i++ )
													 {
														 H_RETURN( _shaders[i]->shaderReflection->GetDesc( &testShaderDesc ), "failed to GetDesc" );
														 for ( uint32 j = 0; j < testShaderDesc.BoundResources; j++ )
														 {
															 H_RETURN( _shaders[i]->shaderReflection->GetResourceBindingDesc( j, &testBindDesc ), "failed to GetResourceBindingDesc" );
															 testName = String( testBindDesc.Name ).RemoveArrayTags( &testIndex );
															 if ( baseName == testName )
																 bindCount = std::max( testIndex, bindCount );
														 }
													 }
												 }

												 // Make sure we didn't already add this texture from another shader, and that the name isn't duplicate
												 ShaderVariable* testVar = GetVariableByName( baseName );
												 bool skip = false;
												 if ( testVar )
												 {
													 if ( (testVar->GetType() != ShaderVariableType::ShaderResource) ||
														 (testVar->AsShaderResource()->GetResourceCount() != (uint32)bindCount + 1) )
													 {
														 String err( "Resource \'" );
														 err += baseName;
														 err += "' already defined.";
														 MessageBoxA( nullptr, err, "Texture Variable Error", MB_OK );
														 return E_FAIL;
													 }
													 skip = true;
												 }
												 if ( !skip )
												 {
													 _variables.Add( new ShaderResource( baseName, bindCount + 1 ) );
												 }
											 }

											 // Add the binding info to the shader
											 if ( arrayIndex == -1 )
												 arrayIndex = 0;
											 int32 id = s->resources.Size();
											 s->resources.Add();
											 s->resources[id].bindPoint = bindDesc.BindPoint;
											 s->resources[id].bindCount = bindDesc.BindCount;
											 auto var = GetVariableByName( baseName );
											 s->resources[id].variable = var ? var->AsShaderResource() : nullptr;
											 s->resources[id].offset = (uint32)arrayIndex;
											 s->resources[id].bindingName = baseName;
				}
					break;

					// Create a sampler variable and add the binding info to the shader
				case D3D10_SIT_SAMPLER:
				{
										  // Locate this sampler
										  ShaderVariable* var = GetVariableByName( bindDesc.Name );
										  if ( !var || !var->AsSamplerState() )
										  {
											  String err = "SamplerStates must map between a shader and an effect, \'";
											  err += bindDesc.Name;
											  err += "\' was not defined in any available effect files";
											  MessageBoxA( nullptr, err, "Effect Error", MB_OK );
											  return E_FAIL;
										  }

										  // Add the binding info to the shader
										  int32 id = s->samplers.Size();
										  s->samplers.Add();
										  s->samplers[id].bindPoint = bindDesc.BindPoint;
										  s->samplers[id].bindCount = 1;
										  s->samplers[id].variable = var->AsSamplerState();
				}
					break;
			}
		}
		return S_OK;
	}


	// Fills in bind variables that were not loaded yet during ProcessResources
	void Effect::FinalizeBindPoints()
	{
		for ( uint32 i = 0; i < _shaders.Size(); i++ )
		{
			for ( uint32 j = 0; j < _shaders[i]->resources.Size(); j++ )
			{
				if ( _shaders[i]->resources[j].variable == nullptr )
				{
					_shaders[i]->resources[j].variable = GetVariableByName( _shaders[i]->resources[j].bindingName )->AsShaderResource();
					if ( _shaders[i]->resources[j].variable == nullptr )
						ErrorMessage( "Binding %s failed to load.", _shaders[i]->resources[j].bindingName );
				}
			}
		}
	}



	// Remove a variable from this effect
	void Effect::RemoveVariable( ShaderVariable* s )
	{
		// First remove this from all shaders
		for ( uint32 i = 0; i < _shaders.Size(); i++ )
		{
			for ( uint32 j = 0; j < _shaders[i]->resources.Size(); j++ )
			if ( _shaders[i]->resources[j].variable == s )
				_shaders[i]->resources.RemoveAt( j );
		}

		// Now remove from the effect, and delete it
		_variables.Remove( s );
		delete s;
	}


	// Returns a constant buffer, or nullptr if it does not exist
	ConstantBuffer* Effect::GetConstantBufferByName( const char* name )
	{
		for ( uint32 i = 0; i < _constantBuffers.Size(); i++ )
		{
			if ( strcmp( name, _constantBuffers[i]->GetName() ) == 0 )
				return _constantBuffers[i];
		}
		return nullptr;
	}


	// Get a variable by name
	ShaderVariable* Effect::GetVariableByName( const char* name )
	{
		for ( uint32 i = 0; i < _variables.Size(); i++ )
		{
			if ( strcmp( name, _variables[i]->GetName() ) == 0 )
				return _variables[i];
		}
		return nullptr;
	}

	// Return a pass by name
	EffectPass* Effect::GetPassByName( const char* name )
	{
		for ( uint32 i = 0; i < _passes.Size(); i++ )
		{
			if ( strcmp( name, _passes[i]->Name() ) == 0 )
				return _passes[i];
		}
		return nullptr;
	}

	// Return a shader by name
	Shader* Effect::GetShaderByName( const char* name )
	{
		for ( uint32 i = 0; i < _shaders.Size(); i++ )
		{
			if ( strcmp( name, _shaders[i]->name ) == 0 )
				return _shaders[i];
		}
		return nullptr;
	}

	// Dump all shader data
	void Effect::DumpShaderData( const char* file )
	{
		std::ofstream fout( file );
		if ( fout.is_open() )
		{
			// Constant buffer data
			fout << "Constant Buffers:" << std::endl;
			for ( uint32 j = 0; j < _constantBuffers.Size(); ++j )
			{
				fout << "\t";
				fout << _constantBuffers[j]->GetName() << "-> ";
				fout << _constantBuffers[j]->GetSize() << std::endl;

				for ( uint32 i = 0; i < _constantBuffers[j]->GetNumVariables(); ++i )
				{
					ShaderVariable* var = _constantBuffers[j]->GetVariableByIndex( i );
					fout << "\t\t";
					fout << var->GetName() << "-> ";
					fout << var->GetSize() << std::endl;
				}
			}
			fout << std::endl;

			// Process each shader
			for ( uint32 i = 0; i < _shaders.Size(); ++i )
			{
				// Name
				fout << _shaders[i]->name.c_str() << std::endl;

				// Constant buffers
				fout << "Constant Buffers:" << std::endl;
				for ( uint32 j = 0; j < _shaders[i]->constantBuffers.Size(); ++j )
				{
					fout << "\t";
					fout << _shaders[i]->constantBuffers[j].variable->GetName() << "-> @";
					fout << _shaders[i]->constantBuffers[j].bindPoint << " for ";
					fout << _shaders[i]->constantBuffers[j].bindCount << std::endl;
				}

				// Resources
				fout << "Resources:" << std::endl;
				for ( uint32 j = 0; j < _shaders[i]->resources.Size(); ++j )
				{
					fout << "\t";
					fout << _shaders[i]->resources[j].bindingName << "-> @";
					fout << _shaders[i]->resources[j].bindPoint << " for ";
					fout << _shaders[i]->resources[j].bindCount << std::endl;
				}

				// UAVS
				fout << "UAV:" << std::endl;
				for ( uint32 j = 0; j < _shaders[i]->unorderedAccessViews.Size(); ++j )
				{
					fout << "\t";
					fout << _shaders[i]->unorderedAccessViews[j].bindingName << "-> @";
					fout << _shaders[i]->unorderedAccessViews[j].bindPoint << " for ";
					fout << _shaders[i]->unorderedAccessViews[j].bindCount << std::endl;
				}
				fout << std::endl;
			}

			fout.close();
		}
	}


	// Recompile all shaders
	bool Effect::Reload()
	{
		BinaryFile file;
		for ( uint32 i = 0; i < _shaders.Size(); ++i )
		{
			String fileName = String( "Resources\\Compiled Shaders\\" ) + _shaders[i]->name + ".cso";
			if ( !file.Load( fileName ) )
			{
				ErrorMessage( "Could not reload shader %s", _shaders[i]->name );
			}
			_shaders[i]->byteCode->Release();
			_shaders[i]->byteCode->FromData( file.GetData(), file.GetSize() );
			file.Release();

			// Clear the old shader
			_shaders[i]->d3dShader->Release();

			// Create a shader
			HRESULT hr = E_FAIL;
			switch ( _shaders[i]->type )
			{
				case ShaderType::Vertex:
				{
										   hr = _graphics->GetDevice()->CreateVertexShader( _shaders[i]->byteCode->GetData(), _shaders[i]->byteCode->GetSize(), nullptr, (ID3D11VertexShader**)&_shaders[i]->d3dShader );
										   break;
				}

				case ShaderType::Pixel:
				{
										  hr = _graphics->GetDevice()->CreatePixelShader( _shaders[i]->byteCode->GetData(), _shaders[i]->byteCode->GetSize(), nullptr, (ID3D11PixelShader**)&_shaders[i]->d3dShader );
										  break;
				}

				case ShaderType::Geometry:
				{
											 hr = _graphics->GetDevice()->CreateGeometryShader( _shaders[i]->byteCode->GetData(), _shaders[i]->byteCode->GetSize(), nullptr, (ID3D11GeometryShader**)&_shaders[i]->d3dShader );
											 break;
				}

				case ShaderType::Hull:
				{
										 hr = _graphics->GetDevice()->CreateHullShader( _shaders[i]->byteCode->GetData(), _shaders[i]->byteCode->GetSize(), nullptr, (ID3D11HullShader**)&_shaders[i]->d3dShader );
										 break;
				}

				case ShaderType::Domain:
				{
										   hr = _graphics->GetDevice()->CreateDomainShader( _shaders[i]->byteCode->GetData(), _shaders[i]->byteCode->GetSize(), nullptr, (ID3D11DomainShader**)&_shaders[i]->d3dShader );
										   break;
				}

				case ShaderType::Compute:
				{
											hr = _graphics->GetDevice()->CreateComputeShader( _shaders[i]->byteCode->GetData(), _shaders[i]->byteCode->GetSize(), nullptr, (ID3D11ComputeShader**)&_shaders[i]->d3dShader );
											break;
				}
			}

			if ( FAILED( hr ) )
			{
				ErrorMessage( "Something bad happened" );
				exit( -1 );
				return false;
			}
		}

		return false;
	}


	// Init
	EffectPass::EffectPass( Renderer* graphics )
	{
		_graphics = graphics;

		for ( int32 i = 0; i < (int32)ShaderType::NumTypes; i++ )
			_shaders[i] = nullptr;

		_blendState = nullptr;
		_rasterizerState = nullptr;
		_depthStencilState = nullptr;

		_blendFactor[0] = _blendFactor[1] =
			_blendFactor[2] = _blendFactor[3] = 0.0f;
		_sampleMask = 0xffffffff;
		_stencilRef = 0;
	}

	// Apply all of the shaders for this effect
	void EffectPass::Apply()
	{
		// Set shaders
		for ( int32 i = 0; i < (int32)ShaderType::NumTypes; i++ )
		{
			if ( _shaders[i] )
				_graphics->SetShader( _shaders[i] );
			else
				_graphics->ClearShader( (ShaderType)i );
		}

		// Set blend state
		_graphics->SetBlendState( _blendState->D3DObject, _blendFactor, _sampleMask );

		// Set rasterizer state
		_graphics->SetRasterizerState( _rasterizerState->D3DObject );

		// Set depth stencil state
		_graphics->SetDepthStencilState( _depthStencilState->D3DObject, _stencilRef );
	}

	// Apply all of the shaders for this effect
	void EffectPass::Apply( DepthStencilState* ds, BlendState* bs, RasterizerState* rs )
	{
		// Set shaders
		for ( int32 i = 0; i < (int32)ShaderType::NumTypes; i++ )
		{
			if ( _shaders[i] )
				_graphics->SetShader( _shaders[i] );
			else
				_graphics->ClearShader( (ShaderType)i );
		}

		// Set blend state
		_graphics->SetBlendState( bs ? bs->D3DObject : _blendState->D3DObject, _blendFactor, _sampleMask );

		// Set rasterizer state
		_graphics->SetRasterizerState( rs ? rs->D3DObject : _rasterizerState->D3DObject);

		// Set depth stencil state
		_graphics->SetDepthStencilState( ds ? ds->D3DObject : _depthStencilState->D3DObject, _stencilRef );
	}

	// Create an input layout from the vertex shader
	HRESULT EffectPass::CreateInputLayout( const D3D11_INPUT_ELEMENT_DESC* desc, uint32 numElements, ID3D11InputLayout** pLayout )
	{
		if ( !_shaders[(int32)ShaderType::Vertex]->byteCode || !_graphics )
			return E_FAIL;

		return _graphics->GetDevice()->CreateInputLayout( desc, numElements, _shaders[(int32)ShaderType::Vertex]->byteCode->GetData(), _shaders[(int32)ShaderType::Vertex]->byteCode->GetSize(), pLayout );
	}

	

}

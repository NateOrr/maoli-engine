//--------------------------------------------------------------------------------------
// File: ShaderUAV.h
//
// Maps a UAV to a shader
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ShaderVariable.h"

namespace Maoli
{
	// UnorderedAccessView
	class ShaderUAV : public ShaderVariable
	{
		friend class RenderState;
	public:

		// Ctor
		ShaderUAV(const char* name);

		// Sets the uav
		inline void SetResource(PipeplineResource* resource){ _resource = resource; }

		// Get a pointer to the uav holding resource
		inline PipeplineResource* GetResource(){ return _resource; }

	protected:

		PipeplineResource*	_resource;
	};
}

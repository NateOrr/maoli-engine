//--------------------------------------------------------------------------------------
// File: ShaderResource.h
//
// Maps SRV's to resources inside shaders
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ShaderVariable.h"
#include "Graphics/Texture.h"

namespace Maoli
{
	// Texture or other shader resource
	class ShaderResource : public ShaderVariable
	{
		friend class RenderState;

	public:

		// Ctor
		ShaderResource(const char* name, uint32 count);

		// Set a single texture resource
		inline void SetResource(PipeplineResource* resource, int32 index = 0){ _resource[0] = resource; _texIndex = index; _srv[0] = resource ? resource->GetSRV()[index] : nullptr; }

		// Sets a texture resource array
		void SetResourceArray(PipeplineResource* resource, uint32 offset, uint32 count);
		void SetResourceArray( PipeplineResource* resource[], uint32 offset, uint32 count );

		// Sets a resource array
		void SetResourceArray( Texture::pointer resource[], uint32 offset, uint32 count );

		// Get a pointer to the resource
		inline ID3D11ShaderResourceView* GetSRV(uint32 i=0){ return _srv[i]; }

		// Number of resources
		inline uint32 GetResourceCount() const {	return _resource.Size(); }

	protected:

		Array<PipeplineResource*>			_resource;
		int32									_texIndex;
		Array<ID3D11ShaderResourceView*>	_srv;		// SRV's for the shader
	};
}

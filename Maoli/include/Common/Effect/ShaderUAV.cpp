//--------------------------------------------------------------------------------------
// File: ShaderUAV.cpp
//
// Maps a UAV to a shader
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ShaderUAV.h"

namespace Maoli
{
	// Ctor
	ShaderUAV::ShaderUAV( const char* name ) : ShaderVariable(name, ShaderVariableType::UnorderedAccessView, 0)
	{
		_resource = nullptr;
	}

}

//--------------------------------------------------------------------------------------
// File: ConstantBuffer.h
//
// Constant buffer wrapper
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma once

#include "Effect/ShaderBuffer.h"

namespace Maoli
{


	// Constant Buffers
	class ConstantBuffer : public ShaderBuffer
	{
	public:

		// Ctor
		ConstantBuffer();

		// Dtor
		~ConstantBuffer();

		// Access to the buffer object to be passed to d3d11 functions
		inline ID3D11Buffer** GetBuffer(){ return &_buffer; }

		// Creates the constant buffer, and fills the variable list
		HRESULT Create(ID3D11Device* device, ID3D11ShaderReflectionConstantBuffer* cb);

		// Copy the buffer to video memory
		void ApplyChanges(ID3D11DeviceContext* dc);
		
		// Number of variables
		inline uint32 GetNumVariables(){ return _variables.Size(); }

		// Get the variable by index
		inline ConstantBufferVariable* GetVariableByIndex(uint32 i){ return _variables[i]; }

		// Get a variable by name
		ConstantBufferVariable* GetVariableByName(const char* name);

	private:
		ID3D11Buffer*					_buffer;		
		Array<ConstantBufferVariable*>	_variables;		
	};

}

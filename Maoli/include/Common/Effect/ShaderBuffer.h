//--------------------------------------------------------------------------------------
// File: ShaderBuffer.h
//
// A shader buffer object that requires updating
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ShaderVariable.h"

namespace Maoli
{

	class ShaderBuffer : public ShaderVariable
	{
		friend class ShaderScalar;
		friend class ShaderVector;
		friend class ShaderMatrix;
		friend class ShaderStruct;
	public:

		// Ctor
		ShaderBuffer();

		// Dtor
		~ShaderBuffer();

		// Flag this buffer as needing update
		inline void FlagDirty(){ isDirty = true; }

		// Check if this buffer needs to update
		inline bool IsDirty(){ return isDirty; }

		// Allocate the data
		void Allocate(uint32 size);

		// Copy memory into the buffer
		void Fill(const void* data);

	protected:
		bool			isDirty;	// True if the buffer needs to be synced
		BYTE*			_data;		// Raw data		
	};
}

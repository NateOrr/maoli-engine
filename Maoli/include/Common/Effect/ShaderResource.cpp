//--------------------------------------------------------------------------------------
// File: ShaderResource.cpp
//
// Maps SRV's to resources inside shaders
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ShaderResource.h"

namespace Maoli
{
	// Ctor
	ShaderResource::ShaderResource( const char* name, uint32 count ) : ShaderVariable(name, ShaderVariableType::ShaderResource, 0)
	{
		_resource.Allocate(count, true);
		_srv.Allocate(count, true);
		_texIndex = 0;
	}


	// Sets a resource array
	void ShaderResource::SetResourceArray( PipeplineResource* resource, uint32 offset, uint32 count )
	{
		_texIndex = 0;
		for(uint32 i=0; i<count; i++)
		{
			_srv[i+offset] = resource->GetSRV()[i];
			_resource[i+offset] = resource;
		}
	}

	// Sets a texture resource array
	void ShaderResource::SetResourceArray( PipeplineResource* resource[], uint32 offset, uint32 count )
	{
		_texIndex = 0;
		for ( uint32 i = 0; i < count; i++ )
		{
			if ( resource[i] )
			{
				_srv[i + offset] = resource[i]->GetSRV()[0];
				_resource[i + offset] = resource[i];
			}
			else
			{
				_srv[i + offset] = nullptr;
				_resource[i + offset] = nullptr;
			}
		}
	}

	// Sets a resource array
	void ShaderResource::SetResourceArray( Texture::pointer resource[], uint32 offset, uint32 count )
	{
		_texIndex = 0;
		for(uint32 i=0; i<count; i++)
		{
			if(resource[i])
			{
				_srv[i+offset] = resource[i]->GetSRV()[0];
				_resource[i+offset] = *resource[i];
			}
			else
			{
				_srv[i+offset] = nullptr;
				_resource[i+offset] = nullptr;
			}
		}
	}

	

}

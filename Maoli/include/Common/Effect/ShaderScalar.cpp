//--------------------------------------------------------------------------------------
// File: ShaderScalar.cpp
//
// Scalar inside a constant buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ShaderScalar.h"
#include "Effect/ShaderBuffer.h"

namespace Maoli
{
	// Ctor
	ShaderScalar::ShaderScalar( const char* name, ShaderBuffer* parent, uint32 offset, uint32 count ) :
		ConstantBufferVariable(name, parent, offset, ShaderVariableType::Scalar, count==0 ? sizeof(int32) : count*sizeof(int32))
	{

	}


	// Set a float
	void ShaderScalar::SetFloat( float v )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, &v, _size);
	}


	// Set an int32
	void ShaderScalar::SetInt( int32 v )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, &v, _size);
	}


	// Set an int32
	void ShaderScalar::SetInt( uint32 v )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, &v, _size);
	}


	// Set a bool
	void ShaderScalar::SetBool( BOOL v )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, &v, _size);
	}


	// Set a float array
	void ShaderScalar::SetFloatArray( float* v, uint32 offset, uint32 count )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v+offset, sizeof(int32)*count);
	}


	// Set an int32 array
	void ShaderScalar::SetIntArray( int32* v, uint32 offset, uint32 count )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v+offset, sizeof(int32)*count);
	}


	// Set a bool array
	void ShaderScalar::SetBoolArray( BOOL* v, uint32 offset, uint32 count )
	{
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v+offset, sizeof(int32)*count);
	}

}

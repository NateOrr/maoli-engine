//--------------------------------------------------------------------------------------
// File: ShaderStruct.cpp
//
// Struct inside a constant buffer (or an entire constant buffer)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ShaderStruct.h"

namespace Maoli
{
	// Ctor
	ShaderStruct::ShaderStruct( const char* name, ShaderBuffer* parent, uint32 offset, uint32 size ) :
		ConstantBufferVariable(name, parent, offset, ShaderVariableType::Struct, size)
	{

	}
}

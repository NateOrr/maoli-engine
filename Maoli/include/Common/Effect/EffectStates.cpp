//--------------------------------------------------------------------------------------
// File: EffectStates.cpp
//
// Blend, Rasterizer, and Depth Stencil states
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/EffectStates.h"

namespace Maoli
{


#pragma region BlendState

	// Ctor
	BlendState::BlendState()
	{
		D3DObject = nullptr;
	}


	// Ctor
	BlendState::BlendState( const char* name, const D3D11_BLEND_DESC& d ) : ShaderVariable(name, ShaderVariableType::BlendState, 0)
	{
		D3DObject = nullptr;
		CopyMemory(&Desc, &d, sizeof(d));
	}


	// Dtor
	BlendState::~BlendState()
	{
		SafeRelease(D3DObject);
	}

#pragma endregion


#pragma region RasterizerState

	// Ctor
	RasterizerState::RasterizerState()
	{
		D3DObject = nullptr;
	}


	// Ctor
	RasterizerState::RasterizerState( const char* name, D3D11_RASTERIZER_DESC& d ) : ShaderVariable(name, ShaderVariableType::RasterizerState, 0)
	{
		D3DObject = nullptr;
		CopyMemory(&Desc, &d, sizeof(d));
	}


	// Dtor
	RasterizerState::~RasterizerState()
	{
		SafeRelease(D3DObject);
	}

#pragma endregion


#pragma region DepthStencilState

	// Ctor
	DepthStencilState::DepthStencilState()
	{
		D3DObject = nullptr;
	}


	// Ctor
	DepthStencilState::DepthStencilState( const char* name, D3D11_DEPTH_STENCIL_DESC& d ) : ShaderVariable(name, ShaderVariableType::DepthStencilState, 0)
	{
		D3DObject = nullptr;
		CopyMemory(&Desc, &d, sizeof(d));
	}

	// Dtor
	DepthStencilState::~DepthStencilState()
	{
		SafeRelease(D3DObject);
	}

#pragma endregion


#pragma region SamplerState


	// Ctor
	SamplerState::SamplerState()
	{
		D3DObject = nullptr;
	}


	// Ctor
	SamplerState::SamplerState( const char* name, D3D11_SAMPLER_DESC& d ) : ShaderVariable(name, ShaderVariableType::Sampler, 0)
	{
		D3DObject = nullptr;
		CopyMemory(&Desc, &d, sizeof(d));
	}


	// Dtor
	SamplerState::~SamplerState()
	{
		SafeRelease(D3DObject);
	}

#pragma endregion


}

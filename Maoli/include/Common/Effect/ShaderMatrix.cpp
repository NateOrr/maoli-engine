//--------------------------------------------------------------------------------------
// File: ShaderMatrix.cpp
//
// Matrix inside a constant buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ShaderMatrix.h"
#include "Effect/ShaderBuffer.h"

namespace Maoli
{
	// Ctor
	ShaderMatrix::ShaderMatrix( const char* name, ShaderBuffer* parent, uint32 offset, uint32 size ) :
		ConstantBufferVariable(name, parent, offset, ShaderVariableType::Matrix, size)
	{

	}


	// Set a matrix
	void ShaderMatrix::SetMatrix( const Matrix& v )
	{
		// todo: OMGWTFBBQ
		_parent->FlagDirty();
		CopyMemory(_parent->_data+_start, v.GetTranspose(), _size);
	}


	// Set a matrix array
	void ShaderMatrix::SetMatrixArray( const Matrix* matrices, uint32 count )
	{
		// todo: maybe define row major in hlsl
		_parent->FlagDirty();
		for(uint32 i=0; i<count; ++i)
			CopyMemory(_parent->_data+_start+sizeof(Matrix)*i, matrices[i].GetTranspose(), sizeof(Matrix));
	}

}

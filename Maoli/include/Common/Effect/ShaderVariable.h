//--------------------------------------------------------------------------------------
// File: ShaderVariable.h
//
// Interface to variables inside shaders
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "EffectTypes.h"

namespace Maoli
{
	// Forward declarations
	class ShaderVariable;
	class ShaderScalar;
	class ShaderVector;
	class ShaderMatrix;
	class ShaderStruct;
	class ShaderResource;
	class ShaderUAV;
	class ConstantBuffer;
	class ConstantBufferVariable;
	struct SamplerState;
	struct BlendState;
	struct RasterizerState;
	struct DepthStencilState;
	class Shader;


	// Bind point for shader resource within a specific shader
	template <class T>
	struct ShaderVariableBindPoint
	{
		// Ctor
		ShaderVariableBindPoint()
		{
			variable = nullptr;
			offset = 0;
			bindPoint = 0;
			bindCount = 1;
		}

		T*			variable;		// The variable to be used
		uint32  		bindPoint;		// Starting register inside the shader
		uint32  		bindCount;		// Number of variables to bind
		uint32  		offset;			// Register in the shader
		String		bindingName;	// Name of the resource to bind (required to fill in the variable in some cases)
	};


	// Generic shader variable
	class ShaderVariable
	{
	public:

		// Ctor
		ShaderVariable();

		// Ctor
		ShaderVariable(const char* name, ShaderVariableType vartype, uint32 size);

		// Dtor
		virtual ~ShaderVariable();

		// Name of the variable
		inline const String& GetName(){ return _name; }

		// Type of the variable
		inline const ShaderVariableType GetType(){ return _type; }

		// Size in bytes of the data this variable represents
		inline const uint32 GetSize(){ return _size; }

		// Converts to a ShaderScalar
		inline ShaderScalar* AsScalar(){ return (_type == ShaderVariableType::Scalar) ? (ShaderScalar*)this : nullptr; }

		// Converts to a ShaderVector
		inline ShaderVector* AsVector(){ return (_type == ShaderVariableType::Vector) ? (ShaderVector*)this : nullptr; }

		// Converts to a ShaderMatrix
		inline ShaderMatrix* AsMatrix(){ return (_type == ShaderVariableType::Matrix) ? (ShaderMatrix*)this : nullptr; }

		// Converts to a ShaderStruct
		inline ShaderStruct* AsStruct(){ return (_type == ShaderVariableType::Matrix) ? (ShaderStruct*)this : nullptr; }

		// Converts to a ConstantBuffer
		inline ConstantBuffer* AsConstantBuffer(){ return (_type == ShaderVariableType::Buffer) ? (ConstantBuffer*)this : nullptr; }

		// Converts to a ShaderResource
		inline ShaderResource* AsShaderResource(){ return (_type == ShaderVariableType::ShaderResource) ? (ShaderResource*)this : nullptr; }

		// Converts to a ShaderUAV
		inline ShaderUAV* AsUnorderedAccessView(){ return (_type == ShaderVariableType::UnorderedAccessView) ? (ShaderUAV*)this : nullptr; }

		// Converts to a SamplerState
		inline SamplerState* AsSamplerState(){ return (_type == ShaderVariableType::Sampler) ? (SamplerState*)this : nullptr; }

		// Converts to a BlendState
		inline BlendState* AsBlendState(){ return (_type == ShaderVariableType::BlendState) ? (BlendState*)this : nullptr; }

		// Converts to a RasterizerState
		inline RasterizerState* AsRasterizerState(){ return (_type == ShaderVariableType::RasterizerState) ? (RasterizerState*)this : nullptr; }

		// Converts to a DepthStencilState
		inline DepthStencilState* AsDepthStencilState(){ return (_type == ShaderVariableType::DepthStencilState) ? (DepthStencilState*)this : nullptr; }


	protected:
		String				_name;			// Name as it appears in the shader
		ShaderVariableType	_type;			// Type of variable
		uint32				_size;			// Size of the data in bytes
	};

}


// Include all the variable types
#include "ShaderVariable.h"
#include "ShaderScalar.h"
#include "ShaderVector.h"
#include "ShaderMatrix.h"
#include "ShaderStruct.h"
#include "ShaderResource.h"
#include "ShaderUAV.h"
#include "ShaderBuffer.h"
#include "ConstantBufferVariable.h"

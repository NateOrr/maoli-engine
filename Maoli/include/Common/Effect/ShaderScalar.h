//--------------------------------------------------------------------------------------
// File: ShaderScalar.h
//
// Scalar inside a constant buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ConstantBufferVariable.h"

namespace Maoli
{
	class ShaderScalar : public ConstantBufferVariable
	{
	public:

		// Ctor
		ShaderScalar(const char* name, ShaderBuffer* parent, uint32 offset, uint32 count);

		// Set a float
		void SetFloat(float v);

		// Set an int32
		void SetInt(int32 v);

		// Set an int32
		void SetInt(uint32 v);

		// Set a bool
		void SetBool(BOOL v);

		// Set a float array
		void SetFloatArray(float* v, uint32 offset, uint32 count);

		// Set an int32 array
		void SetIntArray(int32* v, uint32 offset, uint32 count);

		// Set a bool array
		void SetBoolArray(BOOL* v, uint32 offset, uint32 count);

	};
}

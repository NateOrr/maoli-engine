//--------------------------------------------------------------------------------------
// File: ConstantBufferVariable.cpp
//
// Variable that is stored inside a buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#include "Maoli.h"
#include "Effect/ConstantBufferVariable.h"

namespace Maoli
{

	// Ctor
	ConstantBufferVariable::ConstantBufferVariable()
	{
		_start = 0;
		_parent = nullptr;
	}


	// Ctor
	ConstantBufferVariable::ConstantBufferVariable( const char* name, ShaderBuffer* parent, uint32 offset, ShaderVariableType type, uint32 size ) : 
		ShaderVariable(name, type, size)
	{
		_parent = parent;
		_start = offset;
	}

}

//--------------------------------------------------------------------------------------
// File: ShaderStruct.h
//
// Struct inside a constant buffer (or an entire constant buffer)
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ConstantBufferVariable.h"

namespace Maoli
{
	class ShaderStruct : public ConstantBufferVariable
	{
	public:

		// Ctor
		ShaderStruct(const char* name, ShaderBuffer* parent, uint32 offset, uint32 size);

		// Assign data to this variable
		template<class T>
		void Set(const T& data)
		{
			_parent->FlagDirty();
			CopyMemory(_parent->_data+_start, &data, _size);
		}
	};
}

//--------------------------------------------------------------------------------------
// File: ShaderVector.h
//
// Vector inside a constant buffer
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#include "ConstantBufferVariable.h"

namespace Maoli
{

	class ShaderVector : public ConstantBufferVariable
	{
	public:

		// Ctor
		ShaderVector(const char* name, ShaderBuffer* parent, uint32 offset, uint32 size, uint32 columns);

		// Get the length
		inline uint32 GetLength() const { return _length; }

		// Get the element size
		inline uint32 GetElementSize() const { return _elementSize; }
		
		// Set an integer vector
		void SetIntVector(const int32 v[]);

		// Set an integer vector
		void SetIntVector(const uint32 v[]);

		// Set an float vector
		void SetFloatVector(const float v[]);

		// Set a float vector
		void SetFloatVector(const Vector4& v);

		// Set a double vector
		void SetFloatVector(const double v[]);

		// Set an array of vectors
		void SetFloatVectorArray( const float* v, uint32 numBytes );
		

	private:

		uint32 _length;			// Vector length
		uint32 _elementSize;		// Size of each element
	};
}

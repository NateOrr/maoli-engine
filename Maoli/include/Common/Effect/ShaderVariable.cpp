//--------------------------------------------------------------------------------------
// File: ShaderVariable.cpp
//
// Interface to variables inside shaders
//
// Nate Orr
//--------------------------------------------------------------------------------------

#include "Maoli.h"
#include "Effect/ShaderVariable.h"

namespace Maoli
{

	// Ctor
	ShaderVariable::ShaderVariable( ) : _name( String::none )
	{
		_type = ShaderVariableType::Unknown;
		_size = 0;
	}

	// Ctor
	ShaderVariable::ShaderVariable( const char* name, ShaderVariableType vartype, uint32 size ) : _name(name)
	{
		_type = vartype;
		_size = size;
	}

	// Dtor
	ShaderVariable::~ShaderVariable()
	{

	}

	
}

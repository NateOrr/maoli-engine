//---------------------------------------------------------------------------------------
// File: Common.cpp
//
// Common includes and functions
//
// Nate Orr
//---------------------------------------------------------------------------------------
#include "Maoli.h"

namespace Maoli
{
	// Pre-defined colors
	const Color Color::White = Color( 1, 1, 1, 1 );
	const Color Color::Red = Color( 1, 0, 0, 1 );
	const Color Color::Green = Color( 0, 1, 0, 1 );
	const Color Color::Blue = Color( 0, 0, 1, 1 );
	const Color Color::Black = Color( 0, 0, 0, 1 );

	namespace Math
	{
		// Computes a gaussian sampling value
		float ComputeGaussianValue( float x, float mean, float std_deviation )
		{
			// The gaussian equation is defined as such:
			/*
			-(x - mean)^2
			-------------
			1.0               2*std_dev^2
			f(x,mean,std_dev) = -------------------- * e^
			sqrt(2*pi*std_dev^2)

			*/

			return (1.0f / sqrt( 2.0f * (float)pi * std_deviation * std_deviation ))
				* expf( (-((x - mean) * (x - mean))) / (2.0f * std_deviation * std_deviation) );
		}

		// Compute a random number in the range of 0..1
		float UniformRand()
		{
			return static_cast<float>(rand() % 10000) / 9999.0f;
		}

		// Computes a random gaussian value
		float ComputeRandomGauss( float mean, float deviation )
		{
			float x1, x2, y1, y2, w;

			do
			{
				float r1 = UniformRand();
				float r2 = UniformRand();
				x1 = 2.0f * r1 - 1.0f;
				x2 = 2.0f * r2 - 1.0f;
				w = x1 * x1 + x2 * x2;
			} while ( w >= 1.0f );

			w = sqrtf( (-2.0f * logf( w )) / w );
			y1 = x1 * w;
			y2 = x2 * w;

			y2 = (y2 * deviation) + mean;

			return y2;
		}

		// Gets the ray for a given pixel to trace
		void GetPickRay( int32 x, int32 y, Camera& cam, Vector3& oPos, Vector3& oDir )
		{
			// Get the ray in screen space
			Matrix matProj = cam.GetProjectionMatrix();
			Vector3 v;
			v.x = (((2.0f * x) / cam.GetViewBufferWidth()) - 1) / matProj( 0, 0 );
			v.y = -(((2.0f * y) / cam.GetViewBufferHeight()) - 1) / matProj( 1, 1 );
			v.z = 1.0f;

			// Get the inverse view matrix
			Matrix m = cam.GetViewMatrix().GetInverse();

			// Transform the screen space ray into 3D space
			oDir.x = v.x*m( 0, 0 ) + v.y*m( 1, 0 ) + v.z*m( 2, 0 );
			oDir.y = v.x*m( 0, 1 ) + v.y*m( 1, 1 ) + v.z*m( 2, 1 );
			oDir.z = v.x*m( 0, 2 ) + v.y*m( 1, 2 ) + v.z*m( 2, 2 );
			oDir.Normalize();
			oPos = m.GetPosition();
		}

		// Ray-Plane intersection test
		bool RayPlaneIntersect( Vector3 &origin, Vector3 &direction, Plane& plane, float *t )
		{
			Vector3 normal( plane.a, plane.b, plane.c );
			float rayD = normal.Dot( direction );
			if ( abs( rayD ) < Math::epsilon ) return false;
			float originD = -(normal.Dot( origin ) + plane.d);
			*t = originD / rayD;
			return true;
		}

		// Check if a sphere intersects an AABB
		bool SphereIntersectAABB( const Vector3& posSphere, float radius, const Vector3& posBox, const Vector3& bounds )
		{
			// Check the 8 points of the box against the sphere volume
			Vector3 r;
			Vector3 points[4];
			points[0] = posBox + Vector3( bounds.x, bounds.y, bounds.z );
			points[1] = posBox + Vector3( -bounds.x, bounds.y, bounds.z );
			points[2] = posBox + Vector3( -bounds.x, bounds.y, -bounds.z );
			points[3] = posBox + Vector3( bounds.x, bounds.y, -bounds.z );

			r = posSphere + (posBox + points[0]);
			float radiusSquared = radius*radius;
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			r = posSphere - (posBox + points[0]);
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			r = posSphere + (posBox + points[1]);
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			r = posSphere - (posBox + points[1]);
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			r = posSphere + (posBox + points[2]);
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			r = posSphere - (posBox + points[2]);
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			r = posSphere + (posBox + points[3]);
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			r = posSphere - (posBox + points[3]);
			if ( r.LengthSquared() <= radiusSquared )
				return true;
			return false;
		}

		// Clamp a vector
		void Clamp( Vector2& vec, float min, float max )
		{
			if ( vec.x<min ) vec.x = min;
			if ( vec.y<min ) vec.y = min;
			if ( vec.x>max ) vec.x = max;
			if ( vec.y>max ) vec.y = max;
		}

		// Get the Max value
		Vector3 MaxVector( const Vector3& v1, const Vector3& v2 )
		{
			return Vector3( std::max( v1.x, v2.x ), std::max( v1.y, v2.y ), std::max( v1.z, v2.z ) );
		}

		// Get the Min value
		Vector3 MinVector( const Vector3& v1, const Vector3& v2 )
		{
			return Vector3( std::min( v1.x, v2.x ), std::min( v1.y, v2.y ), std::min( v1.z, v2.z ) );
		}

		// Return the absolute value of a vector
		Vector3 Absolute( const Vector3& v )
		{
			return Vector3( fabs( v.x ), fabs( v.y ), fabs( v.z ) );
		}

		// Checks for a box-sphere intersection
		bool BoxSphereIntersect( const Vector3& boxCenter, const Vector3& boxSize, const Vector3& sphereCenter, const float radius )
		{
			return AABBSphereIntersect( boxCenter - boxSize, boxCenter + boxSize, sphereCenter, radius );
		}

		// Checks for a box-sphere intersection
		bool AABBSphereIntersect( const Vector3& boxMin, const Vector3& boxMax, const Vector3& sphereCenter, const float radius )
		{
			float dist_squared = radius * radius;
			if ( sphereCenter.x < boxMin.x )
				dist_squared -= Math::Square( sphereCenter.x - boxMin.x );
			else if ( sphereCenter.x > boxMax.x )
				dist_squared -= Math::Square( sphereCenter.x - boxMax.x );
			if ( sphereCenter.y < boxMin.y )
				dist_squared -= Math::Square( sphereCenter.y - boxMin.y );
			else if ( sphereCenter.y > boxMax.y )
				dist_squared -= Math::Square( sphereCenter.y - boxMax.y );
			if ( sphereCenter.z < boxMin.z )
				dist_squared -= Math::Square( sphereCenter.z - boxMin.z );
			else if ( sphereCenter.z > boxMax.z )
				dist_squared -= Math::Square( sphereCenter.z - boxMax.z );
			return dist_squared > 0;
		}

		// Checks if a point is on a plane
		bool PointOnPlane( Vector3& point, Plane& plane )
		{
			Vector3 p( plane.a, plane.b, plane.c );
			return point.Dot( p ) == plane.d;
		}

		// Checks if a point is in a box
		bool PointInBox( Vector3& point, Vector3& min, Vector3& max )
		{
			return ((point.x >= min.x && point.x <= max.x) &&
				(point.y >= min.y && point.y <= max.y) &&
				(point.z >= min.z && point.z <= max.z));
		}

		// Returns the normal of a polygon
		void GetNormalFromTri( const Vector3 vPolygon[], Vector3& normal )
		{
			Vector3 vVector1 = vPolygon[2] - vPolygon[0];
			Vector3 vVector2 = vPolygon[1] - vPolygon[0];
			normal = -vVector1.Cross( vVector2 );
			normal.Normalize();
		}

		// Convert Euler angles to angle-axis format
		void AngleAxis( const Vector3& theta, float& angle, Vector3& axis )
		{
			AngleAxis( theta.x, theta.y, theta.z, angle, axis );
		}

		// Convert Euler angles to angle-axis format
		void AngleAxis( float thetaX, float thetaY, float thetaZ, float& angle, Vector3& axis )
		{
			thetaX /= 2;
			thetaY /= 2;
			thetaZ /= 2;
			float c1 = cosf( thetaX );
			float s1 = sinf( thetaX );
			float c2 = cosf( thetaY );
			float s2 = sinf( thetaY );
			float c3 = cosf( thetaZ );
			float s3 = sinf( thetaZ );
			float c1c2 = c1*c2;
			float s1s2 = s1*s2;

			float w = c1c2*c3 - s1s2*s3;
			axis.x = c1c2*s3 + s1s2*c3;
			axis.y = s1*c2*c3 + c1*s2*s3;
			axis.z = c1*s2*c3 - s1*c2*s3;
			angle = 2 * acosf( w );

			float norm = axis.LengthSquared();
			if ( norm > 0.001f )
			{
				norm = 1.0f / sqrtf( norm );
				axis.x *= norm;
				axis.y *= norm;
				axis.z *= norm;
			}
			else
			{
				axis.x = 1.0f;
				axis.y = axis.z = 0;
			}
		}

		// Checks if a point is in a triangle
		bool PointInTriangle( const Vector3& p, const Vector3& v1, const Vector3& v2, const Vector3& v3 )
		{
			// Move triangle to origin
			Vector3 a = v1 - p;
			Vector3 b = v2 - p;
			Vector3 c = v3 - p;

			float ab = a.Dot( b );
			float ac = a.Dot( c );
			float bc = b.Dot( c );
			float cc = c.Dot( c );

			// Make sure plane normals for pab and pbc point in the same direction
			if ( bc*ac - cc*ab < 0.0f )
				return false;

			// Make sure plane normals for pab and pca point in the same direction
			float bb = b.Dot( b );
			if ( ab*bc - ac*bb < 0.0f )
				return false;

			return true;
		}

		// Get the closest triangle vertex relative to a point in the triangle and a goal
		uint32 ClosestVertexToPath( const Vector3& start, const Vector3& end, const Vector3& v1, const Vector3& v2, const Vector3& v3 )
		{
			float d1 = (start - v1).LengthSquared() + (end - v1).LengthSquared();
			float d2 = (start - v2).LengthSquared() + (end - v2).LengthSquared();
			float d3 = (start - v3).LengthSquared() + (end - v3).LengthSquared();
			float d = std::min( std::min( d1, d2 ), d3 );
			if ( d == d1 )
				return 0u;
			if ( d == d2 )
				return 1u;
			return 2u;
		}

		// Distance from a point to a line segment
		float PointToEdgeDistance( const Vector3& c, const Vector3& a, const Vector3& b )
		{
			Vector3 ab = b - a, ac = c - a, bc = c - b;
			float e = ac.Dot( ab );

			// Handle cases where c projects outside ab
			if ( e > 0 )
				return ac.Length();
			float f = ab.LengthSquared();
			if ( e >= f )
				return bc.Length();

			return sqrtf( ac.LengthSquared() - e*e / f );
		}

		// Distance between two line segments
		float EdgeToEdgeDistance( const Vector3& p1, const Vector3& q1, const Vector3& p2, const Vector3& q2 )
		{
			Vector3 d1 = q1 - p1; // Direction vector of segment S1
			Vector3 d2 = q2 - p2; // Direction vector of segment S2
			Vector3 r = p1 - p2;
			float a = d1.LengthSquared(); // Squared length of segment S1, always nonnegative
			float e = d2.LengthSquared(); // Squared length of segment S2, always nonnegative
			float f = d2.Dot( r );
			Vector3 c1, c2;
			float s, t;

			// Check if either or both segments degenerate into points
			if ( a <= Math::epsilon && e <= Math::epsilon )
			{
				// Both segments degenerate into points
				s = t = 0.0f;
				c1 = p1;
				c2 = p2;
				return (c1 - c2).Length();
			}
			if ( a <= Math::epsilon )
			{
				// First segment degenerates into a point
				s = 0.0f;
				t = f / e; // s = 0 => t = (b*s + f) / e = f / e
				t = Clamp( t, 0.0f, 1.0f );
			}
			else
			{
				float c = d1.Dot( r );
				if ( e <= Math::epsilon )
				{
					// Second segment degenerates into a point
					t = 0.0f;
					s = -c / a;
					Math::Clamp( s, 0.0f, 1.0f ); // t = 0 => s = (b*t - c) / a = -c / a
				}
				else
				{
					// The general nondegenerate case starts here
					float b = d1.Dot( d2 );
					float denom = a*e - b*b; // Always nonnegative

					// If segments not parallel, compute closest point on L1 to L2, and
					// clamp to segment S1. Else pick arbitrary s (here 0)
					if ( denom != 0.0f )
					{
						s = (b*f - c*e) / denom;
						Math::Clamp( s, 0.0f, 1.0f );
					}
					else s = 0.0f;

					// Compute point on L2 closest to S1(s) using
					// t = Dot((P1+D1*s)-P2,D2) / Dot(D2,D2) = (b*s + f) / e
					t = (b*s + f) / e;

					// If t in [0,1] done. Else clamp t, recompute s for the new value
					// of t using s = Dot((P2+D2*t)-P1,D1) / Dot(D1,D1)= (t*b - c) / a
					// and clamp s to [0, 1]
					float tnom = b*s + f;
					if ( tnom < 0.0f )
					{
						t = 0.0f;
						s = -c / a;
						Math::Clamp( s, 0.0f, 1.0f );
					}
					else if ( tnom > e )
					{
						t = 1.0f;
						s = (b - c) / a;
						Math::Clamp( s, 0.0f, 1.0f );
					}
					else
					{
						t = tnom / e;
					}
				}
			}

			c1 = p1 + d1 * s;
			c2 = p2 + d2 * t;
			return (c1 - c2).Length();
		}

		// Ray-Triangle intersection test
		bool RayTriangleIntersect( const Vector3& origin, const Vector3& direction, const Vector3& v0, const Vector3& v1, const Vector3& v2 )
		{
			Vector3 e1, e2, h, s, q;
			float a, f, u, v;
			e1 = v1 - v0;
			e2 = v2 - v0;

			h = direction.Cross( e2 );
			a = e1.Dot( h );

			if ( a > -0.00001f && a < 0.00001f )
				return(false);

			f = 1 / a;
			s = origin - v0;
			u = f * s.Dot( h );

			if ( u < 0.0f || u > 1.0f )
				return(false);

			q = s.Cross( e1 );
			v = f * direction.Dot( q );

			if ( v < 0.0 || u + v > 1.0 )
				return(false);

			// at this stage we can compute t to find out where
			// the intersection point is on the line
			float t = f * e2.Dot( q );

			if ( t > 0.00001 ) // ray intersection
				return(true);

			else // this means that there is a line intersection
				// but not a ray intersection
				return (false);
		}

		// Ray-Triangle intersection test
		bool RayTriangleIntersect( const Vector3& origin, const Vector3& direction, const Vector3& v0, const Vector3& v1, const Vector3& v2, float& dist )
		{
			Vector3 e1, e2, h, s, q;
			float a, f, u, v;
			e1 = v1 - v0;
			e2 = v2 - v0;

			h = direction.Cross( e2 );
			a = e1.Dot( h );

			if ( a > -0.00001f && a < 0.00001f )
				return(false);

			f = 1 / a;
			s = origin - v0;
			u = f * s.Dot( h );

			if ( u < 0.0f || u > 1.0f )
				return(false);

			q = s.Cross( e1 );
			v = f * direction.Dot( q );

			if ( v < 0.0 || u + v > 1.0 )
				return(false);

			// at this stage we can compute t to find out where
			// the intersection point is on the line
			dist = f * e2.Dot( q );

			if ( dist > 0.00001 ) // ray intersection
				return(true);

			else // this means that there is a line intersection
				// but not a ray intersection
				return (false);
		}

		// Ray-AABB intersection test
		bool RayBoxIntersect( const Vector3& origin, const Vector3& direction, const Vector3& position, const Vector3& size, float& tmin )
		{
			tmin = 0.0f;				// set to -FLT_MAX to get first hit on line
			float tmax = FLT_MAX;		// set to max distance ray can travel (for segment)
			const Vector3 min = position - size;
			const Vector3 max = position + size;

			// For all three slabs
			for ( int32 i = 0; i < 3; i++ )
			{
				if ( fabs( direction[i] ) < Math::epsilon )
				{
					// Ray is parallel to slab. No hit if origin not within slab
					if ( origin[i] < min[i] || origin[i] > max[i] )
						return false;
				}
				else
				{
					// Compute intersection t value of ray with near and far plane of slab
					float ood = 1.0f / direction[i];
					float t1 = (min[i] - origin[i]) * ood;
					float t2 = (max[i] - origin[i]) * ood;

					// Make t1 be intersection with near plane, t2 with far plane
					if ( t1 > t2 ) Swap( t1, t2 );

					// Compute the intersection of slab intersections intervals
					tmin = std::max( tmin, t1 );
					tmax = std::min( tmax, t2 );

					// Exit with no collision as soon as slab intersection becomes empty
					if ( tmin > tmax )
						return false;
				}
			}

			return true;
		}

		// Compute the AABB of a sphere in screen space
		void GetSphereScreenSpaceAABB( const Vector3& viewPos, const float radius, const Camera& camera, int32 width, int32 height, Point& min, Point& max )
		{
			// Get view space bounds
			Vector3 top = viewPos + Vector3( 0, radius, 0 );
			Vector3 bottom = viewPos + Vector3( 0, -radius, 0 );
			Vector3 left = viewPos + Vector3( -radius, 0, 0 );
			Vector3 right = viewPos + Vector3( radius, 0, 0 );

			// Figure out which diagonal to use
			left.z = left.x < 0.0f ? left.z - radius : left.z + radius;
			right.z = right.x < 0.0f ? right.z + radius : right.z - radius;
			top.z = top.y < 0.0f ? top.z + radius : top.z - radius;
			bottom.z = bottom.y < 0.0f ? bottom.z - radius : bottom.z + radius;

			// Clamp z to the clip planes
			Math::Clamp( left.z, camera.GetNearZ(), camera.GetFarZ() );
			Math::Clamp( right.z, camera.GetNearZ(), camera.GetFarZ() );
			Math::Clamp( top.z, camera.GetNearZ(), camera.GetFarZ() );
			Math::Clamp( bottom.z, camera.GetNearZ(), camera.GetFarZ() );

			// Apply perspective transform
			float rLeft = left.x * camera.GetProjectionMatrix()(0, 0) / left.z;
			float rRight = right.x * camera.GetProjectionMatrix()(0, 0) / right.z;
			float rTop = top.y * camera.GetProjectionMatrix()(1, 1) / top.z;
			float rBottom = bottom.y * camera.GetProjectionMatrix()(1, 1) / bottom.z;

			// Clamp to viewport
			Math::Clamp( rLeft, -1.0f, 1.0f );
			Math::Clamp( rRight, -1.0f, 1.0f );
			Math::Clamp( rTop, -1.0f, 1.0f );
			Math::Clamp( rBottom, -1.0f, 1.0f );

			// Convert to screen coords
			rLeft = rLeft * 0.5f + 0.5f;
			rRight = rRight * 0.5f + 0.5f;
			rTop = rTop * 0.5f + 0.5f;
			rBottom = rBottom * 0.5f + 0.5f;
			rTop = 1.0f - rTop;
			rBottom = 1.0f - rBottom;

			// Output
			min.x = int32(rLeft * width);
			min.y = int32(rTop * height);
			max.x = int32(rRight * width);
			max.y = int32(rBottom * height);
		}

		// Get the number of mip levels for a texture
		int32 GetNumMipLevels( int32 width, int32 height )
		{
			int32 numLevels = 1;
			while ( (width > 1) || (height > 1) )
			{
				width = std::max( width / 2, 1 );
				height = std::max( height / 2, 1 );
				++numLevels;
			}

			return numLevels;
		}

	}
}

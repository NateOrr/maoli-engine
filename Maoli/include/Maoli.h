//---------------------------------------------------------------------------------------
// File: Common.h
//
// Common includes and functions
//
// Nate Orr
//---------------------------------------------------------------------------------------
#pragma once

// Debug mode
#if defined(DEBUG) || defined(_DEBUG)
#define MAOLI_DEBUG
#endif

// Platform compatibility
#ifdef MAOLI_WINDOWS
#define snprintf sprintf_s
#endif

// SSE on windows
#ifdef MAOLI_WINDOWS
#include <smmintrin.h>
#include <mmintrin.h>
#endif

// This is just for readability. There is no reason for the overhead of virtual
// function calls in the cross platform classes since the required version
// is known at compile time. This macro is used to flag functions that are
// required for derived classes to override. Since these functions are not
// defined by the base class there will always be a linker errors to
// ensure they are implemented, this macro is just used to make those functions
// more visible.
#define REQUIRED

//Standard c++ stuff
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <ctime>
#include <cstring>
#include <cmath>
#include <cstdio>
#include <chrono>
#include <cassert>
#include <memory>
#include <functional>
#include <map>
#include <unordered_map>
#include <typeindex>
#include <stdlib.h>

// Datatypes
typedef uint64_t uint64;
typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t uint8;
typedef int64_t int64;
typedef int32_t int32;
typedef int16_t int16;
typedef int8_t int8;
typedef uint8_t byte;
#define nullchar 0

// Forward declare std::thread stuff, so this lib is clr compatible
namespace std
{
	class thread;
	class mutex;
	class condition_variable;
	struct atomic_bool;
}

// Asserts in debug mode only
#ifdef MAOLI_DEBUG
#define M_ASSERT(err, msg) assert(err && msg)
#else
#define M_ASSERT(err, msg)
#endif

// Common Maoli stuff
#include "Util/Util.h"
#include "Util/ISerializable.h"
#include "Util/Pointer.h"
#include "Util/Array.h"
#include "Util/Stack.h"
#include "Util/List.h"
#include "Util/UserList.h"
#include "Util/Queue.h"
#include "Util/PriorityQueue.h"
#include "Util/DiscretePriorityQueue.h"
#include "Util/Bitfield.h"
#include "Util/String.h"
#include "Util/Timer.h"
#include "Util/MemoryManager.h"
#include "Util/ThreadPool.h"
#include "Util/PakFile.h"
#include "Util/BinaryFile.h"
#include "Util/MessageHandler.h"
#include "Util/DataType.h"
#include "Util/EventManager.h"
#include "Util/Resource.h"
#include "Util/ResourceManager.h"
#include "Util/Console.h"


// Constants and helpers
namespace Maoli
{
	// Common math
	namespace Math
	{
		const float pi = 3.1415926f;
		const float twopi = 2 * pi;
		const float epsilon = 0.001f;
		const float epsilon2 = epsilon*epsilon;

		// Clamp a value
		template <class T>
		inline T& Clamp( T& val, const T& min, const T& max )
		{
			if ( val < min ) val = min;
			if ( val > max ) val = max;
			return val;
		}

		// Square a value
		template <typename T>
		inline T Square( const T& val ) { return val*val; }

		// Linear interpolation
		template <typename T>
		inline T Lerp( const T& a, const T& b, float s ) { return a + s*(b - a); }
	}
}

// Math data types
#include "Math/Vector2.h"
#include "Math/Vector3.h"
#include "Math/Vector4.h"
#include "Math/Vector4SSE.h"
#include "Math/Color.h"
#include "Math/ColorSSE.h"
#include "Math/Matrix2.h"
#include "Math/Matrix.h"
#include "Math/MatrixSSE.h"
#include "Math/Plane.h"
#include "Math/BoundingVolume.h"
#include "Math/Frustum.h"
#include "Math/Transform.h"
#include "Math/Camera.h"
#include "Math/Point.h"
#include "Math/Rect.h"
#include "Math/Material.h"
#include "Math/Geometry.h"
#include "Math/NavMesh.h"
#include "Math/Quadtree.h"

// Math helper functions
namespace Maoli
{
	namespace Math
	{
		// Complex number
		struct Complex
		{
			double real, imaginary;
		};

		// Get the number of mip levels for a texture
		int32 GetNumMipLevels( int32 width, int32 height );

		// Normalize a depth value
		inline float NormalizeDepth( float depth, const Camera& camera )
		{
			return (depth - camera.GetNearZ()) / (camera.GetFarZ() - camera.GetNearZ());
		}

		// Project a point from world space to screen space
		inline Point Project( const Vector3& p, const Matrix& viewProjectionMatrix, int32 width, int32 height )
		{
			Vector4 screenPos = viewProjectionMatrix.Transform( Vector4( p, 1.0f ) );
			return Point( int32( ((screenPos.x / screenPos.w) + 1.0f) * (width >> 1) ),
				height - int32( ((screenPos.y / screenPos.w) + 1.0f) * (height >> 1) ) );
		}

		// Project into clip space
		inline Vector3 Project( const Matrix& projMatrix, const Vector3& p )
		{
			const Vector4& v = projMatrix.Transform( Vector4( p, 1.0f ) );
			return v.AsVector3() / v.w;
		}

		// Computes a gaussian sampling value
		float ComputeGaussianValue( float x, float mean, float std_deviation );

		// Computes a random gaussian value
		float ComputeRandomGauss( float mean, float deviation );

		// Compute a random number in the range of 0..1
		float UniformRand();

		// Compute a random number in the range of -1..1
		inline float NormalRand() { return UniformRand()*2.0f - 1.0f; }

		// Checks if the number is a power of 2
		inline bool IsPowerOf2( int32 i ) { return i > 0 && (i & (i - 1)) == 0; }

		// Clamp a vector
		void Clamp( Vector2& vec, float min, float max );

		// Converts a float to a uint32
		inline uint32 F2I( float f ) { return *((uint32*)&f); }

		// Return the absolute value of a vector
		Vector3 Absolute( const Vector3& v );

		// Checks for a box-sphere intersection
		bool BoxSphereIntersect( const Vector3& boxCenter, const Vector3& boxSize, const Vector3& sphereCenter, const float radius );
		bool AABBSphereIntersect( const Vector3& boxMin, const Vector3& boxMax, const Vector3& sphereCenter, const float radius );

		// Intersection of two spheres
		inline bool SphereSphereIntersect( const Vector3& p1, float r1, const Vector3& p2, float r2 )
		{
			return ((p2 - p1).LengthSquared() < Math::Square( r1 ) + Math::Square( r2 ));
		}

		// Gets the ray for a given pixel to trace
		void GetPickRay( int32 x, int32 y, Camera& cam, Vector3& oPos, Vector3& oDir );

		// Get the scalar triple product
		inline float ScalarTriple( const Vector3& a, const Vector3& b, const Vector3& c )
		{
			return a.Cross( b ).Dot( c );
		}

		// Ray-Plane intersection test
		bool RayPlaneIntersect( const Vector3& origin, const Vector3& direction, Plane& plane, float *t );

		// Ray-Triangle intersection test
		bool RayTriangleIntersect( const Vector3& origin, const Vector3& direction, const Vector3& a, const Vector3& b, const Vector3& c );
		bool RayTriangleIntersect( const Vector3& origin, const Vector3& direction, const Vector3& a, const Vector3& b, const Vector3& c, float& dist );

		// Ray-AABB intersection test
		bool RayBoxIntersect( const Vector3& origin, const Vector3& direction, const Vector3& position, const Vector3& size, float& dist );

		// Checks if a point is on a plane
		bool PointOnPlane( Vector3& point, Plane& plane );

		// Checks if a point is in a box
		bool PointInBox( Vector3& point, Vector3& min, Vector3& max );

		// Checks if a point is in a triangle
		bool PointInTriangle( const Vector3& point, const Vector3& v1, const Vector3& v2, const Vector3& v3 );

		// Get the closest triangle vertex relative to a point in the triangle and a goal
		uint32 ClosestVertexToPath( const Vector3& start, const Vector3& end, const Vector3& v1, const Vector3& v2, const Vector3& v3 );

		// Distance from a point to a line segment
		float PointToEdgeDistance( const Vector3& p, const Vector3& e1, const Vector3& e2 );

		// Distance between two line segments
		float EdgeToEdgeDistance( const Vector3& p1, const Vector3& q1, const Vector3& p2, const Vector3& q2 );

		// Returns the normal of a polygon
		void GetNormalFromTri( const Vector3 vPolygon[], Vector3& normal );

		// Get the area of a triangle
		inline float TriangleArea( const Vector3& a, const Vector3& b, const Vector3& c )
		{
			return (b - a).Cross( c - a ).Length() * 0.5f;
		}

		// Get the squared area of a triangle
		inline float TriangleArea2( const Vector3& a, const Vector3& b, const Vector3& c )
		{
			return (b - a).Cross( c - a ).LengthSquared() * 0.25f;
		}

		// Determine the winding order of a triangle
		// positive = clockwise, negative = counterclockwise
		inline float TriangleWinding( const Vector3& a, const Vector3& b, const Vector3& c )
		{
			return (b - a).Cross( c - a ).Dot( Vector3( 0, -1, 0 ) );
		}

		// Check if a sphere intersects an AABB
		bool SphereIntersectAABB( const Vector3& posSphere, float radius, const Vector3& posBox, const Vector3& bounds );

		// Convert Euler angles to angle-axis format
		void AngleAxis( const Vector3& theta, float& angle, Vector3& axis );
		void AngleAxis( float thetaX, float thetaY, float thetaZ, float& angle, Vector3& axis );

		// Compute the AABB of a sphere in screen space
		void GetSphereScreenSpaceAABB( const Vector3& viewPos, const float radius, const Camera& camera, int32 width, int32 height, Point& min, Point& max );
	}
}

#include "PlatformCommon.h"

// Animation
#include "Animation/AnimationTrack.h"
#include "Animation/AnimationController.h"
#include "Animation/Animation.h"
#include "Animation/AnimationManager.h"

// Physics
#include "Physics/Phantom.h"
#include "Physics/RigidBody.h"
#include "Physics/CharacterController.h"
#include "Physics/Physics.h"

// Audio
#include "Audio/Audio.h"

// OS wrappers, MUST be included before the rest of the engine
#include "Platform/Platform.h"

// DirectX headers
#pragma warning( disable : 4005 )
#include <d3d11.h>
#include <dxgi.h>
#include <d3dx11.h>
#include <DXErr.h>
#include <d3dCompiler.h>

// Common Maoli stuff
#include "Util/Error.h"
#include "Engine/System.h"
#include "Engine/Engine.h"

// Graphics engine core
#include "Graphics/Vertex.h"
#include "Graphics/PipelineResource.h"
#include "Graphics/DepthStencil.h"
#include "Graphics/Texture.h"
#include "Graphics/Font.h"
#include "Graphics/Buffer.h"
#include "Graphics/Buffer.h"

// Effects
#include "Effect/ShaderUAV.h"
#include "Effect/ShaderVector.h"
#include "Effect/ShaderScalar.h"
#include "Effect/ShaderResource.h"
#include "Effect/ShaderMatrix.h"
#include "Effect/ConstantBuffer.h"
#include "Effect/EffectStates.h"
#include "Effect/Shader.h"
#include "Effect/Effect.h"

// Renderer
#include "Graphics/Light.h"
#include "Graphics/Model.h"
#include "Graphics/DynamicMesh.h"
#include "Graphics/InstanceSet.h"
#include "Graphics/ParticleEmitterState.h"
#include "Graphics/ParticleSet.h"
#include "Graphics/ParticleEmitter.h"
#include "Graphics/Probe.h"
#include "Graphics/Terrain.h"
#include "Graphics/D3DStates.h"
#include "Graphics/SpriteBatcher.h"
#include "Graphics/Renderer.h"
#include "Graphics/Water.h"
#include "Graphics/DebugVisualizer.h"

// Game
#include "Game/Event.h"
#include "Game/Component.h"
#include "Game/Entity.h"
#include "Game/World.h"
#include "Game/ScriptManager.h"
#include "Game/ScriptObject.h"

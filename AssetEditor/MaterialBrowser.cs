﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Honua
{
    public partial class MaterialBrowser : Form
    {
        // Renderer
        MaoliSharp.Renderer _graphics;
        public MaoliSharp.Material Material { get; set; }

        // File to load
        string _selectedFile = null;
        Dictionary<String, PictureBox> _loadedMaterials;
        PictureBox _selectedBox = null;
        List<String> _pendingChanges;   // Materials that need their previews re-loaded

        // Ctor
        public MaterialBrowser(MaoliSharp.Renderer graphics)
        {
            InitializeComponent();
            _pendingChanges = new List<string>();
            _graphics = graphics;
            this.MouseWheel += new MouseEventHandler(this.OnMouseWheel);
            _loadedMaterials = new Dictionary<string, PictureBox>();
            ImportMaterials("Resources\\Materials");
        }

        // Open the browser
        public bool OpenBrowser()
        {
            // Update pending material changes
            foreach (var file in _pendingChanges)
            {
                // Check if this material already exists in the browser
                string previewFile = Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file) + "_preview.jpg";
                if (_loadedMaterials.ContainsKey(file))
                {
                    var box = _loadedMaterials[file];
                    box.BackgroundImage = Util.LoadBitmap(previewFile);

                    // Add the label and border to the image
                    using (Graphics g = Graphics.FromImage(box.BackgroundImage))
                    {
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        g.DrawString(Path.GetFileNameWithoutExtension(file),
                                     new Font("Tahoma", 15),
                                     Brushes.White,
                                     new PointF(0, 0));
                        Pen pen = new Pen(Color.Black, 2);
                        g.DrawRectangle(pen, 0, 0, box.BackgroundImage.Width - 2, box.BackgroundImage.Height - 2);
                    }
                }
                else
                {
                    AddPreviewBox(file);
                }
            }

            this.Material = null;
            ShowDialog();
            return this.Material != null;
        }

        // Add a preview box
        private void AddPreviewBox(String file)
        {
            string previewFile = Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file) + "_preview.jpg";

            // Setup the preview box
            PictureBox box = new PictureBox();
            box.BackgroundImage = Util.LoadBitmap(previewFile);
            box.BackgroundImageLayout = ImageLayout.Stretch;
            box.BorderStyle = BorderStyle.Fixed3D;
            box.Size = new System.Drawing.Size(128, 128);
            box.Tag = file;
            box.Click += new EventHandler(this.OnClick);
            box.DoubleClick += new EventHandler(this.OnDoubleClick);
            _materials.Controls.Add(box);
            _loadedMaterials.Add(file, box);

            // Add the label and border to the image
            using (Graphics g = Graphics.FromImage(box.BackgroundImage))
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                g.DrawString(Path.GetFileNameWithoutExtension(file),
                             new Font("Tahoma", 15),
                             Brushes.White,
                             new PointF(0, 0));
                Pen pen = new Pen(Color.Black, 2);
                g.DrawRectangle(pen, 0, 0, box.BackgroundImage.Width - 2, box.BackgroundImage.Height - 2);
            }
        }

        // Import all materials from the root folder
        void ImportMaterials(string directory)
        {
            // Grab the files and folders
            try
            {
                var folders = Directory.GetDirectories(directory);
                var files = Directory.GetFiles(directory);

                // Import each material preview
                foreach (string file in files)
                {
                    // Get the file path for the preview
                    if (Path.GetExtension(file) == ".mat")
                    {
                        AddPreviewBox(file);
                    }
                }

                // Process the other folders
                foreach (string folder in folders)
                    ImportMaterials(folder);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
                return;
            }
        }

        // Add a new material to the browser, or update an existing one
        public void AddMaterial(String file)
        {
            _pendingChanges.Add(file);
        }

        // Mouse wheel scrolling
        int _scroll = 0;
        private void OnMouseWheel(object sender, MouseEventArgs e)
        {
            _scroll = Math.Max(_scroll - e.Delta, 0);
            _scroll = Math.Min(_scroll, _materials.Height);
            _materials.AutoScrollPosition = new Point(0, _scroll);
        }


        // Select a preview
        private void OnClick(object sender, EventArgs e)
        {
            // De-select
            if (_selectedBox != null)
            {
                using (Graphics g = Graphics.FromImage(_selectedBox.BackgroundImage))
                {
                    Pen pen = new Pen(Color.Black, 2);
                    g.DrawRectangle(pen, 0, 0, _selectedBox.BackgroundImage.Width - 2, _selectedBox.BackgroundImage.Height - 2);
                }
                _selectedBox.Refresh();
            }

            // Select
            _selectedBox = sender as PictureBox;
            _selectedFile = _selectedBox.Tag as string;
            using (Graphics g = Graphics.FromImage(_selectedBox.BackgroundImage))
            {
                Pen pen = new Pen(Color.Blue, 2);
                g.DrawRectangle(pen, 0, 0, _selectedBox.BackgroundImage.Width - 2, _selectedBox.BackgroundImage.Height - 2);
            }
            _selectedBox.Refresh();
        }

        // Select a preview
        private void OnDoubleClick(object sender, EventArgs e)
        {
            PictureBox box = sender as PictureBox;
            _selectedFile = box.Tag as string;
            OnLoad(sender, e);
            this.Close();
        }

        // Load the selected material
        private void OnLoad(object sender, EventArgs e)
        {
            if (_selectedFile == null)
            {
                MessageBox.Show("You must select a material to do that.");
                return;
            }
            this.Material = _graphics.CreateMaterial(_selectedFile);
            this.Material.Import(_selectedFile);
            this.Close();
        }

        // Exit the dialog
        private void OnCancel(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honua
{
    // A generic list of disposable resources
    class ResourceList<T> : IDisposable where T : IDisposable
    {
        // Entity list
        List<T> _resources;

        // Entity count
        public int Count { get { return _resources.Count; } }

        // Set to true if the list should be cleaned up
        public bool NeedsDispose { get; set; }

        // Bracket access
        public T this[int i]
        {
            get { return _resources[i]; }
            set { _resources[i] = value; }
        }

        // Ctor
        public ResourceList(List<T> list)
        {
            _resources = new List<T>(list);
            NeedsDispose = false;
        }

        // Cleanup
        public void Dispose()
        {
            if(NeedsDispose)
            {
                for (int i = 0; i < _resources.Count; ++i)
                    _resources[i].Dispose();
            }
            _resources = null;
        }

    }
}

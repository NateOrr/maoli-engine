﻿namespace Honua
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this._renderPanel = new System.Windows.Forms.Panel();
            this._splitContainer = new System.Windows.Forms.SplitContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._undoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._redoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.computeNormalMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fixedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.freeRoamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorToAlphaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._gridMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripProbeButton = new System.Windows.Forms.ToolStripButton();
            this._undoButton = new System.Windows.Forms.ToolStripButton();
            this._redoButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this._materialModeButton = new System.Windows.Forms.ToolStripButton();
            this._layerModeButton = new System.Windows.Forms.ToolStripButton();
            this._entityModeButton = new System.Windows.Forms.ToolStripButton();
            this._terrainModeButton = new System.Windows.Forms.ToolStripButton();
            this._worldModeButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripBRDFButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._translateAxisButton = new System.Windows.Forms.ToolStripButton();
            this._rotateAxisButton = new System.Windows.Forms.ToolStripButton();
            this._scaleAxisButton = new System.Windows.Forms.ToolStripButton();
            this._gridSnap = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._splitContainer)).BeginInit();
            this._splitContainer.Panel1.SuspendLayout();
            this._splitContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _renderPanel
            // 
            this._renderPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._renderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._renderPanel.Location = new System.Drawing.Point(0, 0);
            this._renderPanel.Name = "_renderPanel";
            this._renderPanel.Size = new System.Drawing.Size(681, 693);
            this._renderPanel.TabIndex = 0;
            // 
            // _splitContainer
            // 
            this._splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._splitContainer.Location = new System.Drawing.Point(0, 70);
            this._splitContainer.Name = "_splitContainer";
            // 
            // _splitContainer.Panel1
            // 
            this._splitContainer.Panel1.Controls.Add(this._renderPanel);
            // 
            // _splitContainer.Panel2
            // 
            this._splitContainer.Panel2.AutoScroll = true;
            this._splitContainer.Size = new System.Drawing.Size(1008, 693);
            this._splitContainer.SplitterDistance = 681;
            this._splitContainer.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.cameraToolStripMenuItem,
            this.materialToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.OnNew);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OnLoad);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.OnSave);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.saveAsToolStripMenuItem.Text = "Save As";
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.OnQuit);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._undoMenuItem,
            this._redoMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // _undoMenuItem
            // 
            this._undoMenuItem.Name = "_undoMenuItem";
            this._undoMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this._undoMenuItem.Size = new System.Drawing.Size(165, 24);
            this._undoMenuItem.Text = "Undo";
            this._undoMenuItem.Click += new System.EventHandler(this.OnUndo);
            // 
            // _redoMenuItem
            // 
            this._redoMenuItem.Name = "_redoMenuItem";
            this._redoMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this._redoMenuItem.Size = new System.Drawing.Size(165, 24);
            this._redoMenuItem.Text = "Redo";
            this._redoMenuItem.Click += new System.EventHandler(this.OnRedo);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buildDatabaseToolStripMenuItem,
            this.computeNormalMapToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // buildDatabaseToolStripMenuItem
            // 
            this.buildDatabaseToolStripMenuItem.Name = "buildDatabaseToolStripMenuItem";
            this.buildDatabaseToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.buildDatabaseToolStripMenuItem.Text = "Build Database";
            this.buildDatabaseToolStripMenuItem.Click += new System.EventHandler(this.OnBuildDatabase);
            // 
            // computeNormalMapToolStripMenuItem
            // 
            this.computeNormalMapToolStripMenuItem.Name = "computeNormalMapToolStripMenuItem";
            this.computeNormalMapToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.computeNormalMapToolStripMenuItem.Text = "Compute Normal Map";
            this.computeNormalMapToolStripMenuItem.Click += new System.EventHandler(this.OnComputeNormalMap);
            // 
            // cameraToolStripMenuItem
            // 
            this.cameraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fixedToolStripMenuItem,
            this.freeRoamToolStripMenuItem});
            this.cameraToolStripMenuItem.Name = "cameraToolStripMenuItem";
            this.cameraToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.cameraToolStripMenuItem.Text = "Camera";
            // 
            // fixedToolStripMenuItem
            // 
            this.fixedToolStripMenuItem.Checked = true;
            this.fixedToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fixedToolStripMenuItem.Name = "fixedToolStripMenuItem";
            this.fixedToolStripMenuItem.Size = new System.Drawing.Size(149, 24);
            this.fixedToolStripMenuItem.Text = "Fixed";
            // 
            // freeRoamToolStripMenuItem
            // 
            this.freeRoamToolStripMenuItem.Name = "freeRoamToolStripMenuItem";
            this.freeRoamToolStripMenuItem.Size = new System.Drawing.Size(149, 24);
            this.freeRoamToolStripMenuItem.Text = "Free Roam";
            // 
            // materialToolStripMenuItem
            // 
            this.materialToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.colorToAlphaToolStripMenuItem});
            this.materialToolStripMenuItem.Name = "materialToolStripMenuItem";
            this.materialToolStripMenuItem.Size = new System.Drawing.Size(76, 24);
            this.materialToolStripMenuItem.Text = "Material";
            // 
            // colorToAlphaToolStripMenuItem
            // 
            this.colorToAlphaToolStripMenuItem.Name = "colorToAlphaToolStripMenuItem";
            this.colorToAlphaToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.colorToAlphaToolStripMenuItem.Text = "Color To Alpha";
            this.colorToAlphaToolStripMenuItem.Click += new System.EventHandler(this.OnColorToAlpha);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._gridMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // _gridMenuItem
            // 
            this._gridMenuItem.Name = "_gridMenuItem";
            this._gridMenuItem.Size = new System.Drawing.Size(146, 24);
            this._gridMenuItem.Text = "Show Grid";
            this._gridMenuItem.Click += new System.EventHandler(this.OnShowGrid);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 763);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(35, 35);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripProbeButton,
            this._undoButton,
            this._redoButton,
            this.toolStripSeparator2,
            this._materialModeButton,
            this._layerModeButton,
            this._entityModeButton,
            this._terrainModeButton,
            this._worldModeButton,
            this.toolStripButton1,
            this.toolStripSeparator3,
            this.toolStripBRDFButton,
            this.toolStripSeparator1,
            this._translateAxisButton,
            this._rotateAxisButton,
            this._scaleAxisButton});
            this.toolStrip2.Location = new System.Drawing.Point(0, 28);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(1008, 42);
            this.toolStrip2.TabIndex = 5;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(39, 39);
            this.toolStripButton3.Text = "&New";
            this.toolStripButton3.Click += new System.EventHandler(this.OnNew);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(39, 39);
            this.toolStripButton4.Text = "&Open";
            this.toolStripButton4.Click += new System.EventHandler(this.OnLoad);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(39, 39);
            this.toolStripButton5.Text = "&Save";
            this.toolStripButton5.Click += new System.EventHandler(this.OnSave);
            // 
            // toolStripProbeButton
            // 
            this.toolStripProbeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripProbeButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripProbeButton.Image")));
            this.toolStripProbeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripProbeButton.Name = "toolStripProbeButton";
            this.toolStripProbeButton.Size = new System.Drawing.Size(39, 39);
            this.toolStripProbeButton.ToolTipText = "Change Light Probe";
            this.toolStripProbeButton.Click += new System.EventHandler(this.OnLoadProbe);
            // 
            // _undoButton
            // 
            this._undoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._undoButton.Enabled = false;
            this._undoButton.Image = ((System.Drawing.Image)(resources.GetObject("_undoButton.Image")));
            this._undoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._undoButton.Name = "_undoButton";
            this._undoButton.Size = new System.Drawing.Size(39, 39);
            this._undoButton.ToolTipText = "Undo";
            this._undoButton.Click += new System.EventHandler(this.OnUndo);
            // 
            // _redoButton
            // 
            this._redoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._redoButton.Enabled = false;
            this._redoButton.Image = ((System.Drawing.Image)(resources.GetObject("_redoButton.Image")));
            this._redoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._redoButton.Name = "_redoButton";
            this._redoButton.Size = new System.Drawing.Size(39, 39);
            this._redoButton.ToolTipText = "Redo";
            this._redoButton.Click += new System.EventHandler(this.OnRedo);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 42);
            // 
            // _materialModeButton
            // 
            this._materialModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._materialModeButton.Image = ((System.Drawing.Image)(resources.GetObject("_materialModeButton.Image")));
            this._materialModeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._materialModeButton.Name = "_materialModeButton";
            this._materialModeButton.Size = new System.Drawing.Size(39, 39);
            this._materialModeButton.ToolTipText = "View Material";
            this._materialModeButton.Click += new System.EventHandler(this.OnSetEditorMode);
            // 
            // _layerModeButton
            // 
            this._layerModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._layerModeButton.Image = ((System.Drawing.Image)(resources.GetObject("_layerModeButton.Image")));
            this._layerModeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._layerModeButton.Name = "_layerModeButton";
            this._layerModeButton.Size = new System.Drawing.Size(39, 39);
            this._layerModeButton.ToolTipText = "Edit Layers";
            this._layerModeButton.Click += new System.EventHandler(this.OnSetEditorMode);
            // 
            // _entityModeButton
            // 
            this._entityModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._entityModeButton.Image = ((System.Drawing.Image)(resources.GetObject("_entityModeButton.Image")));
            this._entityModeButton.ImageTransparentColor = System.Drawing.Color.White;
            this._entityModeButton.Name = "_entityModeButton";
            this._entityModeButton.Size = new System.Drawing.Size(39, 39);
            this._entityModeButton.Text = "toolStripButton2";
            this._entityModeButton.ToolTipText = "Edit Entity";
            this._entityModeButton.Click += new System.EventHandler(this.OnSetEditorMode);
            // 
            // _terrainModeButton
            // 
            this._terrainModeButton.Checked = true;
            this._terrainModeButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this._terrainModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._terrainModeButton.Image = global::Honua.Properties.Resources.TerrainButton;
            this._terrainModeButton.ImageTransparentColor = System.Drawing.Color.White;
            this._terrainModeButton.Name = "_terrainModeButton";
            this._terrainModeButton.Size = new System.Drawing.Size(39, 39);
            this._terrainModeButton.Text = "toolStripButton2";
            this._terrainModeButton.ToolTipText = "Edit Terrain";
            this._terrainModeButton.Click += new System.EventHandler(this.OnSetEditorMode);
            // 
            // _worldModeButton
            // 
            this._worldModeButton.Checked = true;
            this._worldModeButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this._worldModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._worldModeButton.Image = global::Honua.Properties.Resources.world;
            this._worldModeButton.ImageTransparentColor = System.Drawing.Color.White;
            this._worldModeButton.Name = "_worldModeButton";
            this._worldModeButton.Size = new System.Drawing.Size(39, 39);
            this._worldModeButton.Text = "toolStripButton2";
            this._worldModeButton.ToolTipText = "Edit World";
            this._worldModeButton.Click += new System.EventHandler(this.OnSetEditorMode);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(39, 39);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.OnPlayGame);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 42);
            // 
            // toolStripBRDFButton
            // 
            this.toolStripBRDFButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripBRDFButton.Enabled = false;
            this.toolStripBRDFButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBRDFButton.Image")));
            this.toolStripBRDFButton.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripBRDFButton.Name = "toolStripBRDFButton";
            this.toolStripBRDFButton.Size = new System.Drawing.Size(39, 39);
            this.toolStripBRDFButton.ToolTipText = "Edit BRDF";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 42);
            // 
            // _translateAxisButton
            // 
            this._translateAxisButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._translateAxisButton.Image = ((System.Drawing.Image)(resources.GetObject("_translateAxisButton.Image")));
            this._translateAxisButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._translateAxisButton.Name = "_translateAxisButton";
            this._translateAxisButton.Size = new System.Drawing.Size(39, 39);
            this._translateAxisButton.ToolTipText = "Move Object";
            this._translateAxisButton.Click += new System.EventHandler(this.OnSetTransformAxis);
            // 
            // _rotateAxisButton
            // 
            this._rotateAxisButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._rotateAxisButton.Image = ((System.Drawing.Image)(resources.GetObject("_rotateAxisButton.Image")));
            this._rotateAxisButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._rotateAxisButton.Name = "_rotateAxisButton";
            this._rotateAxisButton.Size = new System.Drawing.Size(39, 39);
            this._rotateAxisButton.ToolTipText = "Rotate Object";
            this._rotateAxisButton.Click += new System.EventHandler(this.OnSetTransformAxis);
            // 
            // _scaleAxisButton
            // 
            this._scaleAxisButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._scaleAxisButton.Image = ((System.Drawing.Image)(resources.GetObject("_scaleAxisButton.Image")));
            this._scaleAxisButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._scaleAxisButton.Name = "_scaleAxisButton";
            this._scaleAxisButton.Size = new System.Drawing.Size(39, 39);
            this._scaleAxisButton.ToolTipText = "Scale Object";
            this._scaleAxisButton.Click += new System.EventHandler(this.OnSetTransformAxis);
            // 
            // _gridSnap
            // 
            this._gridSnap.Font = new System.Drawing.Font("Candara", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._gridSnap.Location = new System.Drawing.Point(622, 34);
            this._gridSnap.Name = "_gridSnap";
            this._gridSnap.Size = new System.Drawing.Size(32, 30);
            this._gridSnap.TabIndex = 6;
            this._gridSnap.Text = "0.0";
            this._gridSnap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnGridSnapKeyPress);
            this._gridSnap.Leave += new System.EventHandler(this.SetGridSnap);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(613, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "GridSnap";
            // 
            // MainWindow
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 785);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._gridSnap);
            this.Controls.Add(this._splitContainer);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "Maoli Asset Editor";
            this._splitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._splitContainer)).EndInit();
            this._splitContainer.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel _renderPanel;
        private System.Windows.Forms.SplitContainer _splitContainer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _undoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _redoMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton _undoButton;
        private System.Windows.Forms.ToolStripButton _redoButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton _layerModeButton;
        private System.Windows.Forms.ToolStripButton _materialModeButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripBRDFButton;
        private System.Windows.Forms.ToolStripButton toolStripProbeButton;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem computeNormalMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton _worldModeButton;
        private System.Windows.Forms.ToolStripMenuItem cameraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fixedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem freeRoamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorToAlphaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton _translateAxisButton;
        private System.Windows.Forms.ToolStripButton _rotateAxisButton;
        private System.Windows.Forms.ToolStripButton _scaleAxisButton;
        private System.Windows.Forms.TextBox _gridSnap;
        private System.Windows.Forms.ToolStripButton _entityModeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton _terrainModeButton;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _gridMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}


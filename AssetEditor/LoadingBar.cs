﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Honua
{
    public partial class LoadingBar : Form
    {
        public LoadingBar(string message)
        {
            this.DesktopLocation = this.PointToClient(
                new Point((SystemInformation.PrimaryMonitorSize.Width - this.Width) / 2,
                          (SystemInformation.PrimaryMonitorSize.Height + this.Height/2) / 2));
            InitializeComponent();
            this._messageLabel.SelectedText = null;
            this.MouseClick += new MouseEventHandler(MouseClickEvent);
            _messageLabel.Text = message;
        }

        void MouseClickEvent(object sender, MouseEventArgs e)
        {

        }

        // Increment the loading bar progress
        float progress = 0.0f;
        public void DoProgress()
        {
            this.progress += 0.15f;
            if (this.progress > this.progressBar1.Maximum - 1)
                this.progress = 0;
            this.progressBar1.Value = (int)this.progress;
        }
    }
}

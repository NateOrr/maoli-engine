﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaoliSharp
{
    public class MessageFilter : IMessageFilter
    {
        // Engine that will process messages
        MaoliSharp.Engine _engine;

        // Ctor
        public MessageFilter(MaoliSharp.Engine engine)
        {
            _engine = engine;
        }

        // Process messages before they are dispatched
        public bool PreFilterMessage(ref Message m)
        {
            _engine.Platform.ProcessMessage((uint)m.Msg, m.WParam.ToInt32(), m.LParam.ToInt32());
            return false;
        }
    }
}

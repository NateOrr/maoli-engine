﻿namespace Honua.Panels
{
    partial class ObjectPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._properties = new Honua.PropertyPage();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this._properties);
            this._groupBox.Margin = new System.Windows.Forms.Padding(4);
            this._groupBox.Padding = new System.Windows.Forms.Padding(4);
            this._groupBox.Size = new System.Drawing.Size(597, 344);
            this._groupBox.Text = "Component";
            // 
            // _properties
            // 
            this._properties.AutoScroll = true;
            this._properties.Dock = System.Windows.Forms.DockStyle.Fill;
            this._properties.Location = new System.Drawing.Point(4, 23);
            this._properties.Name = "_properties";
            this._properties.SelectedObject = null;
            this._properties.Size = new System.Drawing.Size(589, 317);
            this._properties.TabIndex = 0;
            this._properties.TextAlignment = 150;
            // 
            // ObjectPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 381);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ObjectPanel";
            this.Text = "LightPanel";
            this._groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PropertyPage _properties;

    }
}
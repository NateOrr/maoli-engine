﻿namespace Honua.Panels
{
    partial class TexturePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._groupBox = new Honua.CollapsibleGroupBox();
            this._emissive = new System.Windows.Forms.Panel();
            this._metallic = new System.Windows.Forms.Panel();
            this._normal = new System.Windows.Forms.Panel();
            this._alpha = new System.Windows.Forms.Panel();
            this._roughness = new System.Windows.Forms.Panel();
            this._color = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.AllowDrop = true;
            this._groupBox.Controls.Add(this._emissive);
            this._groupBox.Controls.Add(this._metallic);
            this._groupBox.Controls.Add(this._normal);
            this._groupBox.Controls.Add(this._alpha);
            this._groupBox.Controls.Add(this._roughness);
            this._groupBox.Controls.Add(this._color);
            this._groupBox.Controls.Add(this.label9);
            this._groupBox.Controls.Add(this.label10);
            this._groupBox.Controls.Add(this.label7);
            this._groupBox.Controls.Add(this.label8);
            this._groupBox.Controls.Add(this.label6);
            this._groupBox.Controls.Add(this.label5);
            this._groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this._groupBox.Location = new System.Drawing.Point(0, 0);
            this._groupBox.Margin = new System.Windows.Forms.Padding(4);
            this._groupBox.Name = "_groupBox";
            this._groupBox.Padding = new System.Windows.Forms.Padding(4);
            this._groupBox.Size = new System.Drawing.Size(436, 565);
            this._groupBox.TabIndex = 12;
            this._groupBox.TabStop = false;
            this._groupBox.Text = "Textures";
            this._groupBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDrop);
            this._groupBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            // 
            // _emissive
            // 
            this._emissive.AllowDrop = true;
            this._emissive.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._emissive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._emissive.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._emissive.Location = new System.Drawing.Point(220, 377);
            this._emissive.Name = "_emissive";
            this._emissive.Size = new System.Drawing.Size(141, 121);
            this._emissive.TabIndex = 31;
            this._emissive.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDrop);
            this._emissive.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            // 
            // _metallic
            // 
            this._metallic.AllowDrop = true;
            this._metallic.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._metallic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._metallic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._metallic.Location = new System.Drawing.Point(220, 200);
            this._metallic.Name = "_metallic";
            this._metallic.Size = new System.Drawing.Size(141, 121);
            this._metallic.TabIndex = 31;
            this._metallic.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDrop);
            this._metallic.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            // 
            // _normal
            // 
            this._normal.AllowDrop = true;
            this._normal.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._normal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._normal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._normal.Location = new System.Drawing.Point(220, 40);
            this._normal.Name = "_normal";
            this._normal.Size = new System.Drawing.Size(141, 121);
            this._normal.TabIndex = 31;
            this._normal.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDrop);
            this._normal.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            // 
            // _alpha
            // 
            this._alpha.AllowDrop = true;
            this._alpha.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._alpha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._alpha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._alpha.Location = new System.Drawing.Point(37, 377);
            this._alpha.Name = "_alpha";
            this._alpha.Size = new System.Drawing.Size(141, 121);
            this._alpha.TabIndex = 31;
            this._alpha.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDrop);
            this._alpha.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            // 
            // _roughness
            // 
            this._roughness.AllowDrop = true;
            this._roughness.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._roughness.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._roughness.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._roughness.Location = new System.Drawing.Point(37, 200);
            this._roughness.Name = "_roughness";
            this._roughness.Size = new System.Drawing.Size(141, 121);
            this._roughness.TabIndex = 31;
            this._roughness.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDrop);
            this._roughness.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            // 
            // _color
            // 
            this._color.AllowDrop = true;
            this._color.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._color.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._color.Location = new System.Drawing.Point(37, 40);
            this._color.Name = "_color";
            this._color.Size = new System.Drawing.Size(141, 121);
            this._color.TabIndex = 31;
            this._color.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnDrop);
            this._color.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnDragEnter);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(257, 501);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "Emissive";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(84, 501);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 28;
            this.label10.Text = "Alpha";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(256, 324);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 17);
            this.label7.TabIndex = 26;
            this.label7.Text = "Metallic";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(67, 324);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "Roughness";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(257, 164);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 17);
            this.label6.TabIndex = 22;
            this.label6.Text = "Normal";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 164);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Base Color";
            // 
            // TexturePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 660);
            this.Controls.Add(this._groupBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TexturePanel";
            this.Text = "TexturePanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Honua.CollapsibleGroupBox _groupBox;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Panel _emissive;
        public System.Windows.Forms.Panel _metallic;
        public System.Windows.Forms.Panel _normal;
        public System.Windows.Forms.Panel _alpha;
        public System.Windows.Forms.Panel _roughness;
        public System.Windows.Forms.Panel _color;
    }
}
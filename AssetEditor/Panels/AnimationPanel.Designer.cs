﻿namespace Honua.Panels
{
    partial class AnimationPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._animations = new System.Windows.Forms.ListBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this._properties = new Honua.PropertyPage();
            this._time = new Honua.FloatBar();
            this.label1 = new System.Windows.Forms.Label();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this.label1);
            this._groupBox.Controls.Add(this._time);
            this._groupBox.Controls.Add(this._properties);
            this._groupBox.Controls.Add(this._animations);
            this._groupBox.Controls.Add(this.button4);
            this._groupBox.Controls.Add(this.button5);
            this._groupBox.Controls.Add(this.button6);
            this._groupBox.Controls.Add(this.button7);
            this._groupBox.Margin = new System.Windows.Forms.Padding(4);
            this._groupBox.Padding = new System.Windows.Forms.Padding(4);
            this._groupBox.Size = new System.Drawing.Size(324, 474);
            this._groupBox.Text = "Animation Set";
            // 
            // _animations
            // 
            this._animations.FormattingEnabled = true;
            this._animations.ItemHeight = 18;
            this._animations.Location = new System.Drawing.Point(29, 68);
            this._animations.Margin = new System.Windows.Forms.Padding(4);
            this._animations.Name = "_animations";
            this._animations.Size = new System.Drawing.Size(253, 148);
            this._animations.TabIndex = 5;
            this._animations.Click += new System.EventHandler(this.OnSelectAnimation);
            this._animations.SelectedIndexChanged += new System.EventHandler(this.OnSelectAnimation);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(44, 28);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(108, 32);
            this.button4.TabIndex = 6;
            this.button4.Text = "Add Track";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.OnAddTrack);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(160, 28);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(105, 32);
            this.button5.TabIndex = 7;
            this.button5.Text = "Delete Track";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.OnDeleteTrack);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(77, 224);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(61, 26);
            this.button6.TabIndex = 8;
            this.button6.Text = "Play";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.OnPlayTrack);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(183, 224);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(61, 26);
            this.button7.TabIndex = 9;
            this.button7.Text = "Stop";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.OnStopTrack);
            // 
            // _properties
            // 
            this._properties.AutoScroll = true;
            this._properties.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._properties.Location = new System.Drawing.Point(4, 338);
            this._properties.Name = "_properties";
            this._properties.SelectedObject = null;
            this._properties.Size = new System.Drawing.Size(316, 132);
            this._properties.TabIndex = 10;
            this._properties.TextAlignment = 150;
            // 
            // _time
            // 
            this._time.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._time.Location = new System.Drawing.Point(44, 257);
            this._time.Maximum = 100F;
            this._time.Minimum = 0.01F;
            this._time.Name = "_time";
            this._time.NumTicks = 1000;
            this._time.Size = new System.Drawing.Size(237, 38);
            this._time.TabIndex = 11;
            this._time.TextAlignment = 100;
            this._time.Value = 1F;
            this._time.ValueChanged = null;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 317);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "Track Properties";
            // 
            // AnimationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 487);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AnimationPanel";
            this.Text = "AnimationPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ListBox _animations;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private PropertyPage _properties;
        private FloatBar _time;
        private System.Windows.Forms.Label label1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using EntityList = Honua.ResourceList<MaoliSharp.Entity>;

namespace Honua.Panels
{
    public partial class WorldPanel : Form
    {

        MaoliSharp.Engine _engine;                  // Renderer
        Panels.EntityPanel _entityPanel;            // Entity editor
        HonuaEditor _parent;                        // Parent editor
        ToolTip _toolTip;                           // Tooltip for entity types
        bool _isEntityListOpen = false;             // True if the component type list is open
        MaoliSharp.World _world;                    // Game world for editing
        MaoliSharp.World _demoWorld;               // Cached world during demo mode
        MaoliSharp.Camera _camera;                  // Game camera
        List<MaoliSharp.Entity> _selectedEntities;  // Selected entity list
        Point _initialMousePos;                     // Selection box start position
        MaoliSharp.Frustum _selectionFrustum;       // Selection box frustum
        bool _ignoreControlSelection = false;

        // States
        public enum State
        {
            Default,
            SelectionBox,
            Demo,
        }
        State _state = State.Default;

        // Access the entity
        public MaoliSharp.Entity SelectedEntity
        {
            get { return _selectedEntities.Count > 0 ? _selectedEntities[0] : null; }
            set { SelectEntity(value, false); }
        }

        // Access the entity panel
        public Panels.EntityPanel EntityPanel { get { return _entityPanel; } }

        public WorldPanel(HonuaEditor parent, MaoliSharp.World world)
        {
            InitializeComponent();
            _parent = parent;
            _engine = parent.Engine;
            _toolTip = new ToolTip();
            _world = world;
            _camera = _engine.Graphics.GetCamera();
            _tod.SelectedObject = _engine.Graphics;
            _tod.Property = _engine.Graphics.GetType().GetProperty("TimeOfDay");
            _softness.SelectedObject = _engine.Graphics;
            _softness.Property = _engine.Graphics.GetType().GetProperty("SunShadowSoftness");
            _selectedEntities = new List<MaoliSharp.Entity>();
            _selectionFrustum = new MaoliSharp.Frustum();
            _entityPanel = new Panels.EntityPanel(parent);
            _entityPanel._groupBox.Hide();
        }


        // Cleanup
        public void Shutdown()
        {
            if (_entityPanel.SelectedEntity != null)
                _entityPanel.SelectedEntity.Dispose();
        }


        #region State machine

        // Rendering
        public void Render()
        {
            switch (_state)
            {
                case State.Default:
                    DefaultUpdate();
                    break;

                case State.Demo:
                    DemoUpdate();
                    break;

                case State.SelectionBox:
                    SelectionUpdate();
                    break;
            }
        }

        // Default state
        void DefaultUpdate()
        {
            // Process input
            {
                // Delete
                if (_engine.Platform.KeyPressed(MaoliSharp.Keys.Delete))
                    OnDeleteEntity(null, null);

                // Ctrl commands
                if (_engine.Platform.KeyDown(MaoliSharp.Keys.LeftControl))
                {
                    // Duplicate
                    if (_engine.Platform.KeyPressed(MaoliSharp.Keys.D))
                    {
                        Command.PerformAction(new EntityList(_selectedEntities), null,
                            new Action(this.Duplicate), new Action(this.DeleteEntities));
                    }

                    // Game test
                    if (_engine.Platform.KeyPressed(MaoliSharp.Keys.G))
                        SetState(State.Demo);
                }
            }

            // Entity picking
            if (Util.MouseInControl(_parent.RenderPanel) && !_parent.AxisWidget.IsSelected)
            {
                if (_engine.Platform.MouseLeftClick())
                {
                    // Single select on click
                    _initialMousePos = _parent.RenderPanel.PointToClient(Cursor.Position);
                    PickEntity();
                }

                // Selection box is the mouse has moved
                else if (_engine.Platform.MouseLeftDown())
                {
                    var mousePos = _parent.RenderPanel.PointToClient(Cursor.Position);
                    int minx = Math.Min(_initialMousePos.X, mousePos.X);
                    int miny = Math.Min(_initialMousePos.Y, mousePos.Y);
                    int maxx = Math.Max(_initialMousePos.X, mousePos.X);
                    int maxy = Math.Max(_initialMousePos.Y, mousePos.Y);
                    if (maxx - minx > 0 && maxy - miny > 0)
                        SetState(State.SelectionBox);
                }
            }

            // Terrain clamping
            if (_selectedEntities.Count == 1 && _engine.Platform.KeyDown(MaoliSharp.Keys.T))
            {
                if (_engine.Graphics.MouseOnTerrain())
                {
                    var transform = _selectedEntities[0].GetComponent<MaoliSharp.Transform>();
                    if (transform != null)
                    {
                        MaoliSharp.Vector3 terrainPoint = new MaoliSharp.Vector3();
                        var model = _selectedEntities[0].GetComponent<MaoliSharp.Model>();
                        if (model != null)
                            terrainPoint = _engine.Graphics.GetPickPoint(model);
                        else
                            terrainPoint = _engine.Graphics.GetPickPoint();
                        MaoliSharp.Vector3 center, size;
                        _selectedEntities[0].GetBounds(out center, out size);
                        transform.Position = terrainPoint + new MaoliSharp.Vector3(0, size.y, 0);
                    }
                }
            }

            // Render bounding boxes of selected entities and update the axis widgets
            RenderSelectedEntityBounds();
        }

        // Render bounding boxes of selected entities and update the axis widgets
        private void RenderSelectedEntityBounds()
        {
            foreach (var entity in _selectedEntities)
            {
                MaoliSharp.Vector3 pos, size;
                entity.GetBounds(out pos, out size);
                _engine.Graphics.DebugRenderBox(pos, size, new MaoliSharp.Vector3(1, 1, 1));
            }
        }
        // Select a single entity with the mouse
        private void PickEntity()
        {
            MaoliSharp.Vector3 rayPos;
            MaoliSharp.Vector3 rayDir;
            _engine.Graphics.GetPickRay(out rayPos, out rayDir);
            float closestDist = 9999;
            float dist = 0;
            bool found = false;
            MaoliSharp.Entity closestEntity = null;
            foreach (var entity in _engine.Entities)
            {
                if (entity.GetName() == "Terrain")
                    continue;
                if (entity.GetComponent<MaoliSharp.Transform>() == null)
                    continue;

                MaoliSharp.Vector3 pos, size;
                entity.GetBounds(out pos, out size);
                if (MaoliSharp.Math.RayBoxIntersect(rayPos, rayDir, pos, size, ref dist) && dist < closestDist)
                {
                    closestDist = dist;
                    found = true;
                    closestEntity = entity;
                }
            }
            if (found || !_engine.Platform.KeyDown(MaoliSharp.Keys.LeftControl))
                SelectEntity(closestEntity, _engine.Platform.KeyDown(MaoliSharp.Keys.LeftControl));
        }

        // Selection box update
        void SelectionUpdate()
        {
            var mousePos = _parent.RenderPanel.PointToClient(Cursor.Position);
            if (_engine.Platform.MouseLeftDown())
            {
                // Draw the selection box
                _engine.Graphics.RenderSelectionBox(_initialMousePos.X, _initialMousePos.Y, mousePos.X, mousePos.Y);
            }
            else
            {
                // Get the selection box frustum
                int minx = Math.Min(_initialMousePos.X, mousePos.X);
                int miny = Math.Min(_initialMousePos.Y, mousePos.Y);
                int maxx = Math.Max(_initialMousePos.X, mousePos.X);
                int maxy = Math.Max(_initialMousePos.Y, mousePos.Y);
                if (maxx - minx > 0 && maxy - miny > 0)
                {
                    MaoliSharp.Vector3[] corners = new MaoliSharp.Vector3[4];
                    MaoliSharp.Vector3 p;
                    _engine.Graphics.GetPickRay(minx, miny, out p, out corners[0]);
                    _engine.Graphics.GetPickRay(maxx, miny, out p, out corners[1]);
                    _engine.Graphics.GetPickRay(maxx, maxy, out p, out corners[2]);
                    _engine.Graphics.GetPickRay(minx, maxy, out p, out corners[3]);
                    _selectionFrustum.Create(_parent.Camera.Position, corners, 0.05f, 100.0f);

                    // Get selected entities
                    if (!_engine.Platform.KeyDown(MaoliSharp.Keys.LeftControl))
                    {
                        _selectedEntities.Clear();
                        _entities.SelectedItem = null;
                        _parent.AxisWidget.Clear();
                    }
                    for (int i = 0; i < _entities.Items.Count; ++i)
                    {
                        MaoliSharp.Vector3 pos, size;
                        var entity = _entities.Items[i] as MaoliSharp.Entity;
                        entity.GetBounds(out pos, out size);
                        if (_selectionFrustum.BoxIntersect(pos, size))
                            SelectEntity(entity, true);
                    }
                }
                else if (Util.MouseInControl(_parent.RenderPanel) && !_parent.AxisWidget.IsSelected)
                {
                    PickEntity();
                }

                SetState(State.Default);
            }

            // Render bounding boxes of selected entities and update the axis widgets
            RenderSelectedEntityBounds();
        }

        // Demo mode update
        void DemoUpdate()
        {
            // Game test
            if (_engine.Platform.KeyPressed(MaoliSharp.Keys.G))
                SetState(State.Default);
        }

        // State change
        public void SetState(State state)
        {
            if (_state != state)
            {
                // State cleanup
                switch (_state)
                {
                    case State.Default:
                        break;

                    case State.SelectionBox:
                        break;

                    case State.Demo:
                        _engine.DeleteWorld();
                        _demoWorld.Dispose();
                        _demoWorld = null;
                        _engine.SetWorld(_world);
                        _parent.RestoreCamera();
                        _groupBox.Show();
                        _engine.SetState(MaoliSharp.EngineState.Editor);
                        break;
                }

                _state = state;

                // State setup
                switch (_state)
                {
                    case State.Default:
                        break;

                    case State.SelectionBox:
                        break;

                    case State.Demo:
                        this.label1.Focus();
                        _parent.CacheCamera(false);
                        _parent.AxisWidget.Clear();
                        _groupBox.Hide();
                        _entityPanel._groupBox.Hide();
                        _parent.SelectComponent(null);
                        _demoWorld = _world.Clone();
                        _engine.SetWorld(_demoWorld);
                        _engine.SetState(MaoliSharp.EngineState.Default);
                        break;
                }
            }
        }

        #endregion

        // Clear all entities
        public void Clear()
        {
            _engine.ClearWorld();
            _engine.Graphics.ClearTerrain();
            _world.DeleteEntities();
            _selectedEntities.Clear();
            _entities.Items.Clear();
            _parent.AxisWidget.Clear();
            _parent.SelectComponent(null);
            _entityPanel.SelectedEntity = null;
        }

        // Handle a load
        public void OnLoad()
        {
            foreach (var entity in _engine.Entities)
                _entities.Items.Add(entity);
        }

        // Component list has opened
        private void OnEntityListOpen(object sender, EventArgs e)
        {
            _isEntityListOpen = true;
        }

        // Hide the tool tip when the component type box closes
        private void OnEntityListClosed(object sender, EventArgs e)
        {
            _toolTip.Hide(_entityTypes);
            _isEntityListOpen = false;
        }

        // Display tool tip for the component type
        private void OnEntityListDrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0)
                return;

            // Draw the test box
            String text = _entityTypes.GetItemText(_entityTypes.Items[e.Index]);
            e.DrawBackground();
            using (SolidBrush br = new SolidBrush(e.ForeColor))
            {
                e.Graphics.DrawString(text, e.Font, br, e.Bounds);
            }

            // Show the tool tip over the selected item
            if (_isEntityListOpen && (e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                var entityType = _entityTypes.SelectedItem as String;
                _toolTip.Show(entityType, _entityTypes, e.Bounds.Right, e.Bounds.Bottom);
            }
            e.DrawFocusRectangle();
        }

        // Add a new component to the entity
        private void OnAddEntity(object sender, EventArgs e)
        {
            if (_entityTypes.SelectedIndex != -1)
            {
                var entityFile = _entityTypes.SelectedItem as String;
                var entity = _engine.LoadEntity("Resources\\Entities\\" + entityFile + ".entity");
                if (entity != null)
                {
                    _entities.Items.Add(entity);
                    _engine.AddEntity(entity);
                    _world.AddEntity(entity);
                    SelectEntity(entity, false);
                    var transform = entity.GetComponent<MaoliSharp.Transform>();
                    if (transform != null)
                        transform.Position = _camera.Position + _camera.Direction * 5.0f;
                }
            }
        }

        // Remove a component from the entity
        private void OnDeleteEntity(object sender, EventArgs e)
        {
            if (_entities.SelectedItems.Count > 0)
            {
                Command.PerformAction(new EntityList(_selectedEntities), null,
                new Action(this.DeleteEntities), new Action(this.UndoDeleteEntities));
            }
        }

        // Delete all selected entities
        void DeleteEntities(object sender, object param)
        {
            EntityList entityList = sender as EntityList;
            entityList.NeedsDispose = true;
            for (int i = 0; i < entityList.Count; ++i)
            {
                _world.RemoveEntity(entityList[i]);
                _engine.RemoveEntity(entityList[i]);
                _entities.Items.Remove(entityList[i]);
            }
            _selectedEntities.Clear();
            _parent.AxisWidget.Clear();
            this.SelectedEntity = null;
            _parent.SelectComponent(null);
            _entities.SelectedItem = null;
        }

        // Undo an entity delete
        void UndoDeleteEntities(object sender, object param)
        {
            _selectedEntities.Clear();
            EntityList entityList = sender as EntityList;
            entityList.NeedsDispose = false;
            for (int i = 0; i < entityList.Count; ++i)
            {
                _world.AddEntity(entityList[i]);
                _engine.AddEntity(entityList[i]);
                _entities.Items.Add(entityList[i]);
                _selectedEntities.Add(entityList[i]);
                _parent.AxisWidget.SelectObject(entityList[i]);
            }
        }

        // A component was selected in the list box
        private void OnSelectEntity(object sender, EventArgs e)
        {
            if (_entities.SelectedIndex != -1 && !_ignoreControlSelection)
            {
                _selectedEntities.Clear();
                _parent.AxisWidget.Clear();
                foreach (var item in _entities.SelectedItems)
                    SelectEntity(item as MaoliSharp.Entity, true);
            }
        }

        // Select an entity
        void SelectEntity(MaoliSharp.Entity entity, bool multiSelect)
        {
            _parent.SelectComponent(null);
            if (!multiSelect)
            {
                _selectedEntities.Clear();
                _ignoreControlSelection = true;
                _entities.SelectedItem = null;
                _ignoreControlSelection = false;
                _parent.AxisWidget.Clear();
            }
            if (entity != null)
            {
                if (multiSelect && _selectedEntities.Contains(entity))
                {
                    _selectedEntities.Remove(entity);
                    _ignoreControlSelection = true;
                    _entities.SelectedItems.Remove(entity);
                    _ignoreControlSelection = false;
                    _parent.AxisWidget.RemoveObject(entity.GetComponent<MaoliSharp.Transform>());
                }
                else
                {
                    _selectedEntities.Add(entity);
                    _ignoreControlSelection = true;
                    _entities.SelectedItems.Add(entity);
                    _ignoreControlSelection = false;
                    _parent.AxisWidget.SelectObject(entity.GetComponent<MaoliSharp.Transform>());
                }
            }

            // Show/hide the entity editor
            if (_selectedEntities.Count == 1)
            {
                _entityPanel.SelectedEntity = _selectedEntities[0];
                _entityPanel._groupBox.Show();
            }
            else
            {
                _entityPanel.SelectedEntity = null;
                _entityPanel._groupBox.Hide();
            }
        }

        void Duplicate(object sender, object param)
        {
            // Clone all the selected entities
            EntityList entityList = sender as EntityList;
            entityList.NeedsDispose = false;
            List<MaoliSharp.Entity> newEntities = new List<MaoliSharp.Entity>(entityList.Count);
            for (int i = 0; i < entityList.Count; ++i)
                newEntities.Add(entityList[i].Clone());

            // Add them to the world
            _selectedEntities = newEntities;
            _parent.AxisWidget.Clear();
            foreach (var entity in newEntities)
            {
                _engine.AddEntity(entity);
                _world.AddEntity(entity);
                _entities.Items.Add(entity);
                _parent.AxisWidget.SelectObject(entity.GetComponent<MaoliSharp.Transform>());
            }
        }


        // Called when the panel becomes visible
        public void UpdateEntityList()
        {
            // Fill the entity list
            _entityTypes.Items.Clear();
            var files = Directory.GetFiles("Resources\\Entities");
            foreach (string file in files)
                _entityTypes.Items.Add(Path.GetFileNameWithoutExtension(file));
            _entityTypes.SelectedIndex = 0;

            // Re-select
            foreach (var entity in _selectedEntities)
                _parent.AxisWidget.SelectObject(entity.GetComponent<MaoliSharp.Transform>());
        }

        // Enable/disable the sky
        private void OnEnableSky(object sender, EventArgs e)
        {
            _engine.Graphics.EnableSky(_sky.Checked);
            _softness.Visible = _tod.Visible = _sky.Checked;
        }

        // Set the ambient light color
        private void OnSetAmbientColor(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _engine.Graphics.SetAmbient((float)dlg.Color.R / 255.0f, (float)dlg.Color.G / 255.0f, (float)dlg.Color.B / 255.0f);
                _ambient.BackColor = dlg.Color;
            }
        }

        // Ignore keyboard input to the control
        private void IgnoreKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        private void IgnoreKeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void OnEnableOcean(object sender, EventArgs e)
        {
            _parent.EnableOceanPanel(_ocean.Checked);
        }

    }
}

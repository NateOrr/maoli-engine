﻿namespace Honua.Panels
{
    partial class ModelPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.meshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reverseFaceWindingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recomputeNormalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertYTexCoordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swaxXYTexCoordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapCoordinateSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._submeshes = new System.Windows.Forms.ListBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this._unitBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this._xDimension = new Honua.FloatBox();
            this._yDimension = new Honua.FloatBox();
            this._zDimension = new Honua.FloatBox();
            this._groupBox.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this._zDimension);
            this._groupBox.Controls.Add(this._yDimension);
            this._groupBox.Controls.Add(this._xDimension);
            this._groupBox.Controls.Add(this.toolStrip1);
            this._groupBox.Controls.Add(this._submeshes);
            this._groupBox.Controls.Add(this.button3);
            this._groupBox.Controls.Add(this.button1);
            this._groupBox.Controls.Add(this._unitBox);
            this._groupBox.Controls.Add(this.label1);
            this._groupBox.Controls.Add(this.button2);
            this._groupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._groupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._groupBox.Size = new System.Drawing.Size(387, 404);
            this._groupBox.Text = "Model";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolStripSeparator1,
            this.meshToolStripMenuItem});
            this.toolStrip1.Location = new System.Drawing.Point(24, 23);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(105, 25);
            this.toolStrip1.TabIndex = 14;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(43, 25);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.OnSaveMesh);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // meshToolStripMenuItem
            // 
            this.meshToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.meshToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reverseFaceWindingToolStripMenuItem,
            this.recomputeNormalsToolStripMenuItem,
            this.invertYTexCoordToolStripMenuItem,
            this.swaxXYTexCoordsToolStripMenuItem,
            this.swapCoordinateSystemToolStripMenuItem});
            this.meshToolStripMenuItem.Name = "meshToolStripMenuItem";
            this.meshToolStripMenuItem.Size = new System.Drawing.Size(53, 25);
            this.meshToolStripMenuItem.Text = "Tools";
            // 
            // reverseFaceWindingToolStripMenuItem
            // 
            this.reverseFaceWindingToolStripMenuItem.Name = "reverseFaceWindingToolStripMenuItem";
            this.reverseFaceWindingToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.reverseFaceWindingToolStripMenuItem.Text = "Reverse Face Winding";
            this.reverseFaceWindingToolStripMenuItem.Click += new System.EventHandler(this.OnReverseFaceWinding);
            // 
            // recomputeNormalsToolStripMenuItem
            // 
            this.recomputeNormalsToolStripMenuItem.Name = "recomputeNormalsToolStripMenuItem";
            this.recomputeNormalsToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.recomputeNormalsToolStripMenuItem.Text = "Recompute Normals";
            this.recomputeNormalsToolStripMenuItem.Click += new System.EventHandler(this.OnComputeNormals);
            // 
            // invertYTexCoordToolStripMenuItem
            // 
            this.invertYTexCoordToolStripMenuItem.Name = "invertYTexCoordToolStripMenuItem";
            this.invertYTexCoordToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.invertYTexCoordToolStripMenuItem.Text = "Invert Y Tex Coord";
            this.invertYTexCoordToolStripMenuItem.Click += new System.EventHandler(this.OnInvertUV);
            // 
            // swaxXYTexCoordsToolStripMenuItem
            // 
            this.swaxXYTexCoordsToolStripMenuItem.Name = "swaxXYTexCoordsToolStripMenuItem";
            this.swaxXYTexCoordsToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.swaxXYTexCoordsToolStripMenuItem.Text = "Swap X/Y Tex Coords";
            this.swaxXYTexCoordsToolStripMenuItem.Click += new System.EventHandler(this.OnFlipUV);
            // 
            // swapCoordinateSystemToolStripMenuItem
            // 
            this.swapCoordinateSystemToolStripMenuItem.Name = "swapCoordinateSystemToolStripMenuItem";
            this.swapCoordinateSystemToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.swapCoordinateSystemToolStripMenuItem.Text = "Swap Coordinate System";
            this.swapCoordinateSystemToolStripMenuItem.Click += new System.EventHandler(this.OnSwapCoordinates);
            // 
            // _submeshes
            // 
            this._submeshes.FormattingEnabled = true;
            this._submeshes.ItemHeight = 18;
            this._submeshes.Location = new System.Drawing.Point(24, 72);
            this._submeshes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._submeshes.Name = "_submeshes";
            this._submeshes.Size = new System.Drawing.Size(191, 112);
            this._submeshes.TabIndex = 6;
            this._submeshes.SelectedIndexChanged += new System.EventHandler(this.OnSelectSubmesh);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(235, 147);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(117, 27);
            this.button3.TabIndex = 10;
            this.button3.Text = "Delete Submesh";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 204);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 31);
            this.button1.TabIndex = 11;
            this.button1.Text = "Set Material";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnSetMaterial);
            // 
            // _unitBox
            // 
            this._unitBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._unitBox.FormattingEnabled = true;
            this._unitBox.Items.AddRange(new object[] {
            "Millimeters",
            "Centimeters",
            "Meters"});
            this._unitBox.Location = new System.Drawing.Point(243, 93);
            this._unitBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._unitBox.Name = "_unitBox";
            this._unitBox.Size = new System.Drawing.Size(95, 26);
            this._unitBox.TabIndex = 7;
            this._unitBox.SelectedIndexChanged += new System.EventHandler(this.OnChangeUnits);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(269, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Units";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(125, 204);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 31);
            this.button2.TabIndex = 12;
            this.button2.Text = "Edit Material";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnEditMaterial);
            // 
            // _xDimension
            // 
            this._xDimension.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._xDimension.Location = new System.Drawing.Point(24, 255);
            this._xDimension.Maximum = 100F;
            this._xDimension.Minimum = 0.01F;
            this._xDimension.Name = "_xDimension";
            this._xDimension.Size = new System.Drawing.Size(197, 38);
            this._xDimension.TabIndex = 16;
            this._xDimension.TextAlignment = 110;
            this._xDimension.Value = 1F;
            this._xDimension.ValueChanged = null;
            // 
            // _yDimension
            // 
            this._yDimension.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._yDimension.Location = new System.Drawing.Point(24, 299);
            this._yDimension.Maximum = 100F;
            this._yDimension.Minimum = 0.01F;
            this._yDimension.Name = "_yDimension";
            this._yDimension.Size = new System.Drawing.Size(197, 38);
            this._yDimension.TabIndex = 16;
            this._yDimension.TextAlignment = 110;
            this._yDimension.Value = 1F;
            this._yDimension.ValueChanged = null;
            // 
            // _zDimension
            // 
            this._zDimension.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._zDimension.Location = new System.Drawing.Point(24, 343);
            this._zDimension.Maximum = 100F;
            this._zDimension.Minimum = 0.01F;
            this._zDimension.Name = "_zDimension";
            this._zDimension.Size = new System.Drawing.Size(197, 38);
            this._zDimension.TabIndex = 16;
            this._zDimension.TextAlignment = 110;
            this._zDimension.Value = 1F;
            this._zDimension.ValueChanged = null;
            // 
            // ModelPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 537);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ModelPanel";
            this.Text = "ModelPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ListBox _submeshes;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox _unitBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripMenuItem meshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reverseFaceWindingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recomputeNormalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertYTexCoordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swaxXYTexCoordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapCoordinateSystemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private FloatBox _zDimension;
        private FloatBox _yDimension;
        private FloatBox _xDimension;
    }
}
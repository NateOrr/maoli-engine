﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace Honua.Panels
{
    public partial class EntityPanel : Form
    {

        MaoliSharp.Engine _engine;          // Renderer
        HonuaEditor _parent;                // Parent editor
        MaoliSharp.Entity _entity;          // Entity to work with
        ToolTip _toolTip;                   // Tooltip for component types
        bool _isComponentListOpen = false;  // True if the component type list is open
        bool _isLocal = false;              // True if the entity was allocated locally

        // Access the entity
        public MaoliSharp.Entity SelectedEntity
        {
            get { return _entity; }
            set
            {
                if (_isLocal && _entity != null)
                    _entity.Dispose();

                _entity = value;
                if (_entity != null)
                    _entityName.Text = _entity.GetName();
                _isLocal = false;

                // Components
                _components.Items.Clear();
                if (_entity != null)
                {
                    foreach (var c in _entity.Components)
                        _components.Items.Add(c);
                }
            }
        }


        // Get the selected component
        public MaoliSharp.Component SelectedComponent
        {
            get
            {
                return (_components.SelectedIndex != -1) ? _components.SelectedItem as MaoliSharp.Component : null;
            }
        }


        public EntityPanel(HonuaEditor parent)
        {
            InitializeComponent();
            _parent = parent;
            _engine = parent.Engine;
            _toolTip = new ToolTip();

            // Fill the component list
            var assembly = Assembly.GetAssembly(_engine.GetType());
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                // Check the full hierarchy for Component base class
                var baseType = type;
                while (baseType.Namespace == "MaoliSharp")
                {
                    if (baseType.BaseType.FullName == "MaoliSharp.Component")
                    {
                        _componentTypes.Items.Add(type.FullName.Replace("MaoliSharp.", ""));
                        break;
                    }
                    baseType = baseType.BaseType;
                }
            }
            _componentTypes.SelectedIndex = 0;

            OnNewEntity(null, null);
        }

        // Save the entity to file
        public void SaveEntity()
        {
            String file = "Resources\\Entities\\" + _entity.GetName() + ".entity";
            if (File.Exists(file))
            {
                if (MessageBox.Show(_entity.GetName() + ".entity already exists. Overwrite?", "Honua Editor", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;
            }
            var model = _entity.GetComponent<MaoliSharp.Model>();
            if (model != null)
               model.Save();
            _engine.SaveEntity(_entity, file);
        }

        // Load the entity from file
        public bool LoadEntity(String file)
        {
            MaoliSharp.Entity newEntity = _engine.LoadEntityTemplate(file);
            if (newEntity != null)
            {
                _parent.SelectComponent(null);
                this.SelectedEntity = newEntity;
                _engine.AddEntity(_entity);
                _isLocal = true;
                return true;
            }
            return false;
        }


        // Component list has opened
        private void OnComponentListOpen(object sender, EventArgs e)
        {
            _isComponentListOpen = true;
        }

        // Hide the tool tip when the component type box closes
        private void OnComponentListClosed(object sender, EventArgs e)
        {
            _toolTip.Hide(_componentTypes);
            _isComponentListOpen = false;
        }

        // Display tool tip for the component type
        private void OnComponentListDrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0)
                return;

            // Draw the test box
            String text = _componentTypes.GetItemText(_componentTypes.Items[e.Index]);
            e.DrawBackground();
            using (SolidBrush br = new SolidBrush(e.ForeColor))
            {
                e.Graphics.DrawString(text, e.Font, br, e.Bounds);
            }

            // Show the tool tip over the selected item
            if (_isComponentListOpen && (e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                String typeName = "MaoliSharp." + _componentTypes.SelectedItem as String;
                Type componentType = Type.GetType(typeName + ", MaoliSharp");
                MethodInfo inf = componentType.GetMethod("GetDescription", BindingFlags.Static | BindingFlags.Public);
                if(inf != null)
                {
                    String desc = inf.Invoke(null, null) as String;
                    _toolTip.Show(desc, _componentTypes, e.Bounds.Right, e.Bounds.Bottom);
                }
            }
            e.DrawFocusRectangle();
        }

        // Add a new component to the entity
        private void OnAddComponent(object sender, EventArgs e)
        {
            if (_componentTypes.SelectedIndex != -1)
            {
                // Create the component based on the type
                String typeName = "MaoliSharp." + _componentTypes.SelectedItem as String;
                Type componentType = Type.GetType(typeName + ", MaoliSharp");
                var methods = _engine.GetType().GetMethods();
                MaoliSharp.Component component = null;
                foreach (var m in methods)
                {
                    if (m.Name == "CreateComponent" && m.IsGenericMethod)
                    {
                        MethodInfo generic = m.MakeGenericMethod(componentType);
                        component = generic.Invoke(_engine, null) as MaoliSharp.Component;
                    }
                }
                if (component == null)
                    return;

                // Load the file if needed
                if (component.FileBased())
                {
                    String fileName = Util.GetOpenFileName("Resources", component.GetFileFilter());
                    if (fileName != null)
                    {
                        if (component.Load(fileName))
                        {
                            if (_entity.AddComponent(component))
                            {
                                _components.SelectedIndex = _components.Items.Add(component);
                                return;
                            }
                        }
                        MessageBox.Show("Failed to load component");
                    }
                    return;
                }

                if (_entity.AddComponent(component))
                    _components.SelectedIndex = _components.Items.Add(component);
            }
        }

        // Remove a component from the entity
        private void OnDeleteComponent(object sender, EventArgs e)
        {
            if (_componentTypes.SelectedIndex != -1)
            {
                var component = _components.SelectedItem as MaoliSharp.Component;
                _entity.RemoveComponent(component);
                _components.Items.Remove(component);
                _parent.SelectComponent(null);
            }
        }

        // Clear all components from the entity
        public void OnNewEntity(object sender, EventArgs e)
        {
            if (_entity != null)
            {
                _components.Items.Clear();
                _engine.RemoveEntity(_entity);
                if (_isLocal)
                    _entity.Dispose();
            }
            _isLocal = true;
            _entity = _engine.CreateEntityTemplate("NewEntity");
            var transform = _engine.CreateComponent<MaoliSharp.Transform>();
            _components.Items.Add(transform);
            _entity.AddComponent(transform);
            _engine.AddEntity(_entity);
            _entityName.Text = _entity.GetName();
            _parent.SelectComponent(null);
            _engine.Graphics.ClearDebugRenderList();
        }

        // Handle enter press for rename
        private void OnEntityNameBoxKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.label1.Focus();
                e.Handled = true;
            }
        }

        // Handle enter press for rename
        private void OnComponentNameBoxKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.label1.Focus();
                e.Handled = true;
            }
        }

        // Rename entity
        private void OnRenameEntity(object sender, EventArgs e)
        {
            if (!_engine.RenameEntity(_entity, _entityName.Text))
                _entityName.Text = _entity.GetName();
        }


        // A component was selected in the list box
        private void OnSelectComponent(object sender, EventArgs e)
        {
            if (_components.SelectedIndex != -1)
            {
                var component = _components.SelectedItem as MaoliSharp.Component;
                _parent.SelectComponent(component);
            }
        }

        private void IgnoreKeydown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
        }

        private void IgnoreKeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

    }
}

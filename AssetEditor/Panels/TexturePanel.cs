﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class TexturePanel : Form
    {
        // Renderer
        MaoliSharp.Renderer _graphics;

        // Parent window
        HonuaEditor _parent;
        
        // Ctor
        public TexturePanel(HonuaEditor parent, MaoliSharp.Renderer g)
        {
            InitializeComponent();
            _parent = parent;
            _graphics = g;
        }

        // Drag and drop enter, to show that it is supported
        void OnDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) 
                e.Effect = DragDropEffects.Copy;
        }

        // Drag and drop file
        void OnDrop(object sender, DragEventArgs e)
        {
            Panel box = sender as Panel;
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            _parent.LoadMaterialTexture(files[0], (MaoliSharp.TextureType)box.Tag);
        }

    }
}

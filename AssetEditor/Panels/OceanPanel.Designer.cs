﻿namespace Honua.Panels
{
    partial class OceanPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._properties = new Honua.PropertyPage();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.AutoSize = true;
            this._groupBox.Controls.Add(this._properties);
            this._groupBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._groupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._groupBox.Size = new System.Drawing.Size(387, 27);
            this._groupBox.Text = "Ocean";
            // 
            // _properties
            // 
            this._properties.AutoScroll = true;
            this._properties.AutoSize = true;
            this._properties.Dock = System.Windows.Forms.DockStyle.Fill;
            this._properties.Location = new System.Drawing.Point(3, 23);
            this._properties.Margin = new System.Windows.Forms.Padding(4);
            this._properties.Name = "_properties";
            this._properties.PropertyChanged = null;
            this._properties.SelectedObject = null;
            this._properties.Size = new System.Drawing.Size(381, 0);
            this._properties.TabIndex = 10;
            this._properties.TextAlignment = 170;
            // 
            // OceanPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 537);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "OceanPanel";
            this.Text = "OceanPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PropertyPage _properties;

    }
}
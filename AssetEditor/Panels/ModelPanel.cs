﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class ModelPanel : Honua.ComponentPanel
    {
        // Mesh
        public MaoliSharp.Model _model;
        MaoliSharp.SubMesh _selectedSubmesh;
        float _unitScale = 1.0f;
        HonuaEditor _editor;

        // Ctor
        public ModelPanel(HonuaEditor parent)
        {
            InitializeComponent();
            _unitBox.SelectedIndex = 2;
            _editor = parent;
        }

        // Save a mesh to file
        void OnSaveMesh(object sender, EventArgs e)
        {
            if (_model == null)
            {
                MessageBox.Show("You need to load a mesh first");
            }
            else
            {
                String fileName = Util.GetSaveFileName("Resources\\Models", Util.MeshFilter);
                if (fileName != null)
                    _model.Save(fileName);
            }
        }

        // Fill the mesh box
        public override void SetComponent(object component)
        {
            _unitBox.SelectedIndex = 2;
            _model = component as MaoliSharp.Model;
            _submeshes.Items.Clear();
            if (_model != null)
            {
                // Sub meshes
                foreach (MaoliSharp.SubMesh submesh in _model.SubMeshes)
                    _submeshes.Items.Add(submesh);
                if (_submeshes.Items.Count>0)
                    _submeshes.SelectedIndex = 0;

                _xDimension.SelectedObject = _model;
                _yDimension.SelectedObject = _model;
                _zDimension.SelectedObject = _model;

                _xDimension.Property = typeof(MaoliSharp.Model).GetProperty("XDimension");
                _yDimension.Property = typeof(MaoliSharp.Model).GetProperty("YDimension");
                _zDimension.Property = typeof(MaoliSharp.Model).GetProperty("ZDimension");

                _xDimension.Text = "X Dimension";
                _yDimension.Text = "Y Dimension";
                _zDimension.Text = "Z Dimension";
            }
        }

        // Handle addition of new components to the entity
        public override void OnAddComponent(object component)
        {

        }

        // Handle removal of components to the entity
        public override void OnRemoveComponent(object component)
        {

        }

        // Select a submesh from the list box
        private void OnSelectSubmesh(object sender, EventArgs e)
        {
            _selectedSubmesh = _submeshes.SelectedItem as MaoliSharp.SubMesh;
        }

        // Change the mesh units
        private void OnChangeUnits(object sender, EventArgs e)
        {
            if (_model == null)
                return;

            // Compute the scaling factor
            if ((string)_unitBox.SelectedItem == "Centimeters")
                _unitScale = 0.1f;
            else if ((string)_unitBox.SelectedItem == "Millimeters")
                _unitScale = 0.01f;
            else if ((string)_unitBox.SelectedItem == "Meters")
                _unitScale = 1.0f;
            _model.UniformScale = _unitScale;
        }

        // Change a submesh material
        private void OnSetMaterial(object sender, EventArgs e)
        {
            MaoliSharp.Material mat = _editor.ChooseMaterial();
            if (mat != null)
                _selectedSubmesh.Material = mat;
        }

        // Edit the material for the selected submesh
        private void OnEditMaterial(object sender, EventArgs e)
        {
            _editor.SetMaterial(_selectedSubmesh.Material);
            _editor.SetState(HonuaEditor.EditorState.Material);
        }

        // Delete a submesh
        private void OnDelete(object sender, EventArgs e)
        {
            if (_model != null)
            {
                if (_submeshes.SelectedIndex == -1)
                {
                    MessageBox.Show("You need to select a submesh first");
                    return;
                }

                if (_submeshes.Items.Count == 1)
                {
                    MessageBox.Show("You cannot delete the only submesh");
                    return;
                }

                _model.DeleteSubmesh(_submeshes.SelectedIndex);
                _submeshes.Items.RemoveAt(_submeshes.SelectedIndex);
            }
        }

        // Reverse mesh face winding
        private void OnReverseFaceWinding(object sender, EventArgs e)
        {
            if (_model != null)
                _model.ReverseFaceWinding();
            else
                MessageBox.Show("You need to load a mesh first");
        }

        // Compute smooth mesh normals
        private void OnComputeNormals(object sender, EventArgs e)
        {
            if (_model != null)
                _model.RecomputeNormals();
            else
                MessageBox.Show("You need to load a mesh first");
        }

        // Invert y uv coords
        private void OnInvertUV(object sender, EventArgs e)
        {
            if (_model != null)
                _model.InvertUV();
            else
                MessageBox.Show("You need to load a mesh first");
        }

        // Swap x/y tex coords
        private void OnFlipUV(object sender, EventArgs e)
        {
            if (_model != null)
                _model.FlipUV();
            else
                MessageBox.Show("You need to load a mesh first");
        }

        // Swap x/y coords
        private void OnSwapCoordinates(object sender, EventArgs e)
        {
            if (_model != null)
                _model.FlipCoordinateSystem();
            else
                MessageBox.Show("You need to load a mesh first");
        }

        // Submesh rendering
        public override void OnRender()
        {
            // Selected submesh
            if (_submeshes.SelectedIndex != -1)
                _editor.Graphics.DebugRenderMesh(_model, _submeshes.SelectedIndex, true, true, true);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class ParticlePanel : Honua.ComponentPanel
    {
        MaoliSharp.Renderer _graphics;
        HonuaEditor _editor;
        MaoliSharp.ParticleEmitter _emitter;
        MaoliSharp.ParticleEmitterState _selectedState;
        int _selectedStateIndex;

        // For tracking sets in the undo/redo pool
        class SetTracker
        {
            public SetTracker(MaoliSharp.ParticleEmitterState state, MaoliSharp.ParticleSet set)
            {
                State = state;
                Set = set;
            }

            public MaoliSharp.ParticleEmitterState State;
            public MaoliSharp.ParticleSet Set;
        }

        // Ctor
        public ParticlePanel(HonuaEditor parent)
        {
            InitializeComponent();
            _editor = parent;
            _graphics = parent.Graphics;
            _stateName.ValueChanged += new EventHandler(this.OnStateNameChange);
        }

        // Fill the mesh box
        public override void SetComponent(object component)
        {
            if (component != null)
            {
                _emitter = component as MaoliSharp.ParticleEmitter;
                _states.Items.Clear();

                // Update the combo box for the next state
                UpdateNextStateBox();
                
                // States
                for (int i = 0; i < _emitter.GetNumStates(); ++i)
                    _states.Items.Add(_emitter.GetState(i));
                if (_states.Items.Count > 0)
                    _states.SelectedIndex = 0;
            }
        }

        // Update the combo box for the next state
        void UpdateNextStateBox()
        {
            _nextState.ComboBox.Items.Clear();
            _nextState.ComboBox.Items.Add("None");
            int numStates = _emitter.GetNumStates();
            for (int i = 0; i < numStates; ++i)
                _nextState.ComboBox.Items.Add(_emitter.GetState(i));
        }

        // Update the set box when a state is selected or changed
        void OnChangeState()
        {
            _sets.Items.Clear();
            if (_selectedState != null)
            {
                for (int i = 0; i < _selectedState.GetNumSets(); ++i)
                {
                    _sets.Items.Add(_selectedState.GetSet(i));
                    if (_sets.Items.Count > 0)
                        _sets.SelectedIndex = 0;
                }
            }
        }

        // Name change for states
        void OnStateNameChange(object sender, object param)
        {
            if (_selectedState != null)
            {
                var state = _selectedState;
                _states.Items.Remove(state);
                _states.Items.Insert(_selectedStateIndex, state);
            }
        }


        // Add an new state
        private void OnAddState(object sender, EventArgs e)
        {
            var state = _emitter.CreateState("NewState");
            Command.PerformAction(_states.Items.Count, state,
                new Action(this.AddState), new Action(this.DeleteState));
        }

        // Delete a state
        private void OnDeleteState(object sender, EventArgs e)
        {
            if (_states.SelectedItem != null)
            {
                if (_states.Items.Count > 1)
                {
                    Command.PerformAction(_states.SelectedIndex, _selectedState,
                        new Action(this.DeleteState), new Action(this.AddState));
                }
                else
                    MessageBox.Show("You cannot remove the only state from an emitter");
            }
        }

        // Delete a state action
        void DeleteState(object sender, object param)
        {
            var state = param as MaoliSharp.ParticleEmitterState;
            _emitter.RemoveState(state);
            _states.Items.Remove(state);
            _states.SelectedIndex = 0;
        }

        // Add a state action
        void AddState(object sender, object param)
        {
            var state = param as MaoliSharp.ParticleEmitterState;
            int index = (int)sender;
            _states.Items.Insert(index, state);
            _emitter.AddState(state, index);
            _states.SelectedIndex = index;
        }

        // Select state
        private void OnSelectState(object sender, EventArgs e)
        {
            if (_states.SelectedItem != null)
            {
                _selectedState = _states.SelectedItem as MaoliSharp.ParticleEmitterState;
                _selectedStateIndex = _states.SelectedIndex;
                _emitter.SetActiveState(_selectedState);
                OnChangeState();

                // Setup the next state box
                UpdateNextStateBox();
                _nextState.ComboBox.Items.Remove(_selectedState);

                _nextState.SelectedObject = _selectedState;
                _nextState.Property = typeof(MaoliSharp.ParticleEmitterState).GetProperty("NextState");
                _duration.SelectedObject = _selectedState;
                _duration.Property = typeof(MaoliSharp.ParticleEmitterState).GetProperty("Duration");
                _stateName.SelectedObject = _selectedState;
                _stateName.Property = typeof(MaoliSharp.ParticleEmitterState).GetProperty("Name");
            }
        }




        // Add an new set
        private void OnAddSet(object sender, EventArgs e)
        {
            if (_selectedState != null)
            {
                var set = _emitter.CreateParticleSet();
                var tracker = new SetTracker(_selectedState, set);
                Command.PerformAction(_sets.Items.Count, tracker,
                    new Action(this.AddSet), new Action(this.DeleteSet));
            }
        }

        // Delete a set
        private void OnDeleteSet(object sender, EventArgs e)
        {
            if (_sets.SelectedItem != null)
            {
                if (_sets.Items.Count > 1)
                {
                    var set = _sets.SelectedItem as MaoliSharp.ParticleSet;
                    var tracker = new SetTracker(_selectedState, set);
                    Command.PerformAction(_sets.SelectedIndex, tracker,
                        new Action(this.DeleteSet), new Action(this.AddSet));
                }
                else
                    MessageBox.Show("You cannot remove the only set from an emitter state");
            }
        }

        // Delete a set action
        void DeleteSet(object sender, object param)
        {
            var tracker = param as SetTracker;
            tracker.State.RemoveSet(tracker.Set);
            _sets.Items.Remove(tracker.Set);
            _sets.SelectedIndex = 0;
        }

        // Add a set action
        void AddSet(object sender, object param)
        {
            var tracker = param as SetTracker;
            int index = (int)sender;
            _sets.Items.Insert(index, tracker.Set);
            tracker.State.AddSet(tracker.Set);
            _sets.SelectedIndex = index;
        }

        // Select an set
        private void OnSelectSet(object sender, EventArgs e)
        {
            if (_sets.SelectedItem != null)
            {
                var set = _sets.SelectedItem as MaoliSharp.ParticleSet;
                _setProperties.SelectedObject = set;
            }
        }

        // Rendering / frame update
        public override void OnRender()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class ObjectPanel : Honua.ComponentPanel
    {
        // Ctor
        public ObjectPanel()
        {
            InitializeComponent();
        }

        // Fill the mesh box
        public override void SetComponent(object component)
        {
            _properties.SelectedObject = component;
        }

    }
}

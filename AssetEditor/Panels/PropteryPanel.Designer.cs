﻿namespace Honua.Panels
{
    partial class PropteryPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._groupBox = new Honua.CollapsibleGroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this._emissiveColorBox = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this._baseColorBox = new System.Windows.Forms.PictureBox();
            this._transparencyBar = new System.Windows.Forms.TrackBar();
            this._metallicBar = new System.Windows.Forms.TrackBar();
            this._roughnessBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._nameBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._alphaTesting = new System.Windows.Forms.CheckBox();
            this._groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._emissiveColorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._baseColorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._transparencyBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._metallicBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._roughnessBar)).BeginInit();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this._alphaTesting);
            this._groupBox.Controls.Add(this.label12);
            this._groupBox.Controls.Add(this._emissiveColorBox);
            this._groupBox.Controls.Add(this.label11);
            this._groupBox.Controls.Add(this._baseColorBox);
            this._groupBox.Controls.Add(this._transparencyBar);
            this._groupBox.Controls.Add(this._metallicBar);
            this._groupBox.Controls.Add(this._roughnessBar);
            this._groupBox.Controls.Add(this.label2);
            this._groupBox.Controls.Add(this.label4);
            this._groupBox.Controls.Add(this.label3);
            this._groupBox.Controls.Add(this._nameBox);
            this._groupBox.Controls.Add(this.label1);
            this._groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this._groupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._groupBox.Location = new System.Drawing.Point(0, 0);
            this._groupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._groupBox.Name = "_groupBox";
            this._groupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._groupBox.Size = new System.Drawing.Size(401, 409);
            this._groupBox.TabIndex = 11;
            this._groupBox.TabStop = false;
            this._groupBox.Text = "Properties";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(226, 317);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 17);
            this.label12.TabIndex = 53;
            this.label12.Text = "Emissive";
            // 
            // _emissiveColorBox
            // 
            this._emissiveColorBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._emissiveColorBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._emissiveColorBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._emissiveColorBox.Location = new System.Drawing.Point(230, 274);
            this._emissiveColorBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._emissiveColorBox.Name = "_emissiveColorBox";
            this._emissiveColorBox.Size = new System.Drawing.Size(53, 39);
            this._emissiveColorBox.TabIndex = 52;
            this._emissiveColorBox.TabStop = false;
            this._emissiveColorBox.Click += new System.EventHandler(this.OnChangeEmissiveColor);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(83, 317);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 17);
            this.label11.TabIndex = 51;
            this.label11.Text = "Base Color";
            // 
            // _baseColorBox
            // 
            this._baseColorBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this._baseColorBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._baseColorBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._baseColorBox.Location = new System.Drawing.Point(94, 274);
            this._baseColorBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._baseColorBox.Name = "_baseColorBox";
            this._baseColorBox.Size = new System.Drawing.Size(53, 39);
            this._baseColorBox.TabIndex = 50;
            this._baseColorBox.TabStop = false;
            this._baseColorBox.Click += new System.EventHandler(this.OnChangeBaseColor);
            // 
            // _transparencyBar
            // 
            this._transparencyBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._transparencyBar.Location = new System.Drawing.Point(148, 175);
            this._transparencyBar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._transparencyBar.Maximum = 100;
            this._transparencyBar.Name = "_transparencyBar";
            this._transparencyBar.Size = new System.Drawing.Size(163, 56);
            this._transparencyBar.TabIndex = 49;
            this._transparencyBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._transparencyBar.ValueChanged += new System.EventHandler(this.UpdateTransparency);
            // 
            // _metallicBar
            // 
            this._metallicBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._metallicBar.Location = new System.Drawing.Point(148, 135);
            this._metallicBar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._metallicBar.Maximum = 100;
            this._metallicBar.Name = "_metallicBar";
            this._metallicBar.Size = new System.Drawing.Size(163, 56);
            this._metallicBar.TabIndex = 49;
            this._metallicBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._metallicBar.ValueChanged += new System.EventHandler(this.UpdateMetallic);
            // 
            // _roughnessBar
            // 
            this._roughnessBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._roughnessBar.Location = new System.Drawing.Point(148, 92);
            this._roughnessBar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._roughnessBar.Maximum = 100;
            this._roughnessBar.Name = "_roughnessBar";
            this._roughnessBar.Size = new System.Drawing.Size(163, 56);
            this._roughnessBar.TabIndex = 48;
            this._roughnessBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._roughnessBar.ValueChanged += new System.EventHandler(this.UpdateRoughness);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 175);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 47;
            this.label2.Text = "Transparency";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 17);
            this.label4.TabIndex = 47;
            this.label4.Text = "Metallic";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 92);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 46;
            this.label3.Text = "Roughness";
            // 
            // _nameBox
            // 
            this._nameBox.Location = new System.Drawing.Point(149, 47);
            this._nameBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._nameBox.Name = "_nameBox";
            this._nameBox.ReadOnly = true;
            this._nameBox.Size = new System.Drawing.Size(132, 22);
            this._nameBox.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 44;
            this.label1.Text = "Name";
            // 
            // _alphaTesting
            // 
            this._alphaTesting.AutoSize = true;
            this._alphaTesting.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._alphaTesting.Location = new System.Drawing.Point(122, 223);
            this._alphaTesting.Name = "_alphaTesting";
            this._alphaTesting.Size = new System.Drawing.Size(117, 21);
            this._alphaTesting.TabIndex = 54;
            this._alphaTesting.Text = "Alpha Testing";
            this._alphaTesting.UseVisualStyleBackColor = true;
            this._alphaTesting.Click += new System.EventHandler(this.OnSetAlphaTesting);
            // 
            // PropteryPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 482);
            this.Controls.Add(this._groupBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PropteryPanel";
            this.Text = "PropteryPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._emissiveColorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._baseColorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._transparencyBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._metallicBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._roughnessBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Honua.CollapsibleGroupBox _groupBox;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.PictureBox _emissiveColorBox;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.PictureBox _baseColorBox;
        public System.Windows.Forms.TrackBar _metallicBar;
        public System.Windows.Forms.TrackBar _roughnessBar;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox _nameBox;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TrackBar _transparencyBar;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.CheckBox _alphaTesting;
    }
}
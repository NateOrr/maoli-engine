﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class AnimationPanel : Honua.ComponentPanel
    {
        MaoliSharp.Renderer _graphics;
        HonuaEditor _editor;
        int _selectedTrackIndex;
        MaoliSharp.AnimationTrack _selectedTrack;

        // Track speed update helper class
        class FloatChange
        {
            public FloatChange(float value, float prevValue)
            {
                this.value = value;
                this.prevValue = prevValue;
            }
            public float value, prevValue;
        }

        // Animation track data
        class AnimationTrackRef
        {
            public AnimationTrackRef(MaoliSharp.AnimationTrack track, String fileName)
            {
                this.track = track;
                this.fileName = fileName;
                if (track != null)
                {
                    this.speed = track.Speed;
                    this.loop = track.Loop;
                    this.name = track.Name;
                }
            }

            public MaoliSharp.AnimationTrack track;
            public String fileName;
            public float speed;
            public bool loop;
            public String name;
        }


        // Mesh
        public MaoliSharp.Animation _animation;

        // Ctor
        public AnimationPanel(HonuaEditor parent)
        {
            InitializeComponent();
            _editor = parent;
            _graphics = parent.Graphics;
            _time.Text = "Time";
            _time.TextAlignment = 85;
            _time.Width = 250;
            _properties.PropertyChanged += new EventHandler(this.OnPropertyChanged);
        }

        // Fill the mesh box
        public override void SetComponent(object component)
        {
            if (component != null)
            {
                _animation = component as MaoliSharp.Animation;
                _animations.Items.Clear();

                // Animations
                foreach (var track in _animation.Tracks)
                    _animations.Items.Add(track);
                if (_animations.Items.Count > 0)
                    _animations.SelectedIndex = 0;
            }
        }

        // Handle track name change
        void OnPropertyChanged(object sender, EventArgs e)
        {
            var control = sender as HonuaControl;
            if (control != null)
            {
                if (control.Property.Name == "Name")
                {
                    var track = _selectedTrack;
                    _animations.Items.Remove(track);
                    _animations.Items.Insert(_selectedTrackIndex, track);
                    _animations.Refresh();
                }
            }
        }


        // Add an animation track
        private void OnAddTrack(object sender, EventArgs e)
        {
            String fileName = Util.GetOpenFileName("Resources\\Animations", Util.AnimationFilter);
            if (fileName != null)
            {
                if (!_animation.IsTrackLoaded(fileName))
                {
                    var track = new AnimationTrackRef(null, fileName);
                    Command.PerformAction(_animations.Items.Count, track,
                        new Action(this.AddTrack), new Action(this.DeleteTrack));
                }
                else
                {
                    MessageBox.Show("Track is already loaded");
                }
            }
        }

        // Delete an animation track
        private void OnDeleteTrack(object sender, EventArgs e)
        {
            if (_selectedTrack != null)
            {
                if (_animations.Items.Count > 1)
                {
                    AnimationTrackRef trackRef = new AnimationTrackRef(_selectedTrack, _selectedTrack.FileName);
                    Command.PerformAction(_selectedTrackIndex, trackRef,
                        new Action(this.DeleteTrack), new Action(this.AddTrack));
                }
                else
                    MessageBox.Show("You cannot remove the only track from an animation");
            }
        }

        // Delete a track
        void DeleteTrack(object sender, object param)
        {
            AnimationTrackRef track = (AnimationTrackRef)param;
            _animation.DeleteTrack(track.track.Index);
            _animations.Items.Remove(track.track);
            _animations.SelectedIndex = 0;
            _animations.Refresh();
        }

        // Add a track
        void AddTrack(object sender, object param)
        {
            int index = (int)sender;
            var trackRef = (AnimationTrackRef)param;
            bool existingTrack = (trackRef.track != null);
            var track = _animation.AddTrack(trackRef.fileName, index);
            trackRef.track = track;
            if (existingTrack)
            {
                track.Name = trackRef.name;
                track.Speed = trackRef.speed;
                track.Loop = trackRef.loop;
            }
            _animations.Items.Insert(index, track);
            _animations.SelectedIndex = index;
        }

        // Play an animation track
        private void OnPlayTrack(object sender, EventArgs e)
        {
            if (_animations.SelectedItem != null)
                _animation.PlayTrack(_animations.SelectedIndex);
        }

        // Pause animations
        private void OnStopTrack(object sender, EventArgs e)
        {
            _animation.Pause();
        }


        // Select an animation track in the list box
        private void OnSelectAnimation(object sender, EventArgs e)
        {
            if (_animations.SelectedItem != null)
            {
                var track = _animations.SelectedItem as MaoliSharp.AnimationTrack;
                _selectedTrack = track;
                _selectedTrackIndex = _animations.SelectedIndex;
                _properties.SelectedObject = track;
                _time.Maximum = track.Duration;
                _time.SelectedObject = track;
                _time.Property = track.GetType().GetProperty("Time");
                track.Play();
            }
        }

        // Rendering / frame update
        public override void OnRender()
        {
            if (_animations.SelectedItem != null)
            {
                var track = _animations.SelectedItem as MaoliSharp.AnimationTrack;
                _time.Value = track.Time;
            }
        }
    }
}

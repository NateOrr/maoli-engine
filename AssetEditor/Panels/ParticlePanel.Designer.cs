﻿namespace Honua.Panels
{
    partial class ParticlePanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._states = new System.Windows.Forms.ListBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.collapsibleGroupBox1 = new Honua.CollapsibleGroupBox();
            this._setProperties = new Honua.PropertyPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this._sets = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._nextState = new Honua.SelectionBox();
            this._duration = new Honua.FloatBox();
            this._stateName = new Honua.StringBox();
            this._groupBox.SuspendLayout();
            this.collapsibleGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.AutoSize = true;
            this._groupBox.Controls.Add(this._stateName);
            this._groupBox.Controls.Add(this._duration);
            this._groupBox.Controls.Add(this._nextState);
            this._groupBox.Controls.Add(this.label2);
            this._groupBox.Controls.Add(this.label1);
            this._groupBox.Controls.Add(this.collapsibleGroupBox1);
            this._groupBox.Controls.Add(this._sets);
            this._groupBox.Controls.Add(this._states);
            this._groupBox.Controls.Add(this.button2);
            this._groupBox.Controls.Add(this.button1);
            this._groupBox.Controls.Add(this.button4);
            this._groupBox.Controls.Add(this.button5);
            this._groupBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._groupBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._groupBox.Size = new System.Drawing.Size(515, 896);
            this._groupBox.Text = "Particle Emitter";
            // 
            // _states
            // 
            this._states.FormattingEnabled = true;
            this._states.ItemHeight = 18;
            this._states.Location = new System.Drawing.Point(29, 118);
            this._states.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._states.Name = "_states";
            this._states.Size = new System.Drawing.Size(253, 112);
            this._states.TabIndex = 5;
            this._states.Click += new System.EventHandler(this.OnSelectState);
            this._states.SelectedIndexChanged += new System.EventHandler(this.OnSelectState);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(44, 78);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(108, 32);
            this.button4.TabIndex = 6;
            this.button4.Text = "Add State";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.OnAddState);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(160, 78);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(105, 32);
            this.button5.TabIndex = 7;
            this.button5.Text = "Delete State";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.OnDeleteState);
            // 
            // collapsibleGroupBox1
            // 
            this.collapsibleGroupBox1.AutoSize = true;
            this.collapsibleGroupBox1.Controls.Add(this._setProperties);
            this.collapsibleGroupBox1.Location = new System.Drawing.Point(29, 611);
            this.collapsibleGroupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.collapsibleGroupBox1.Name = "collapsibleGroupBox1";
            this.collapsibleGroupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.collapsibleGroupBox1.Size = new System.Drawing.Size(267, 258);
            this.collapsibleGroupBox1.TabIndex = 8;
            this.collapsibleGroupBox1.TabStop = false;
            this.collapsibleGroupBox1.Text = "Particle Set Properties";
            // 
            // _setProperties
            // 
            this._setProperties.AutoScroll = true;
            this._setProperties.AutoSize = true;
            this._setProperties.Location = new System.Drawing.Point(15, 33);
            this._setProperties.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._setProperties.Name = "_setProperties";
            this._setProperties.PropertyChanged = null;
            this._setProperties.SelectedObject = null;
            this._setProperties.Size = new System.Drawing.Size(200, 193);
            this._setProperties.TabIndex = 0;
            this._setProperties.TextAlignment = 170;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(156, 437);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 32);
            this.button1.TabIndex = 7;
            this.button1.Text = "Delete Set";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnDeleteSet);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(40, 437);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 32);
            this.button2.TabIndex = 6;
            this.button2.Text = "Add Set";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnAddSet);
            // 
            // _sets
            // 
            this._sets.FormattingEnabled = true;
            this._sets.ItemHeight = 18;
            this._sets.Location = new System.Drawing.Point(25, 477);
            this._sets.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._sets.Name = "_sets";
            this._sets.Size = new System.Drawing.Size(253, 112);
            this._sets.TabIndex = 5;
            this._sets.Click += new System.EventHandler(this.OnSelectSet);
            this._sets.SelectedIndexChanged += new System.EventHandler(this.OnSelectSet);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Candara", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(75, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 29);
            this.label1.TabIndex = 9;
            this.label1.Text = "Emitter States";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Candara", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(75, 404);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 29);
            this.label2.TabIndex = 9;
            this.label2.Text = "Particle Sets";
            // 
            // _nextState
            // 
            this._nextState.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._nextState.IgnoreEvents = true;
            this._nextState.Location = new System.Drawing.Point(25, 289);
            this._nextState.Name = "_nextState";
            this._nextState.NullIndex = 0;
            this._nextState.Property = null;
            this._nextState.PropertyPage = null;
            this._nextState.Size = new System.Drawing.Size(349, 48);
            this._nextState.TabIndex = 10;
            this._nextState.TextAlignment = 100;
            this._nextState.ValueChanged = null;
            // 
            // _duration
            // 
            this._duration.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._duration.Location = new System.Drawing.Point(25, 330);
            this._duration.Maximum = 1000F;
            this._duration.Minimum = 0F;
            this._duration.Name = "_duration";
            this._duration.Property = null;
            this._duration.PropertyPage = null;
            this._duration.Size = new System.Drawing.Size(263, 49);
            this._duration.TabIndex = 11;
            this._duration.TextAlignment = 100;
            this._duration.Value = 1F;
            this._duration.ValueChanged = null;
            // 
            // _stateName
            // 
            this._stateName.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._stateName.Location = new System.Drawing.Point(29, 239);
            this._stateName.Name = "_stateName";
            this._stateName.Property = null;
            this._stateName.PropertyPage = null;
            this._stateName.Size = new System.Drawing.Size(263, 49);
            this._stateName.TabIndex = 12;
            this._stateName.TextAlignment = 100;
            this._stateName.Value = "1.0";
            this._stateName.ValueChanged = null;
            // 
            // ParticlePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 852);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ParticlePanel";
            this.Text = "ParticlePanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            this.collapsibleGroupBox1.ResumeLayout(false);
            this.collapsibleGroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ListBox _states;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private CollapsibleGroupBox collapsibleGroupBox1;
        private PropertyPage _setProperties;
        public System.Windows.Forms.ListBox _sets;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private SelectionBox _nextState;
        private FloatBox _duration;
        private StringBox _stateName;
    }
}
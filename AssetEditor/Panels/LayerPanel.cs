﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Honua.Panels
{
    public partial class LayerPanel : Form
    {
        // Renderer
        MaoliSharp.Renderer _graphics;

        // Parent window
        HonuaEditor _parent;

        // Blend masks for each layer
        List<MaoliSharp.Texture> _blendMasks = new List<MaoliSharp.Texture>();
        List<Bitmap> _blendMaskImages = new List<Bitmap>();
        
        // The selected material
        public MaoliSharp.Material _material;

        // The primary material
        public MaoliSharp.Material _finalMaterial;
        public MaoliSharp.Material _blendStageMaterial;

        // Default blend mask
        Bitmap _defaultLayerMaskImage;
        MaoliSharp.Texture _defaultLayerMask;

        // Ctor
        public LayerPanel(HonuaEditor parent, MaoliSharp.Renderer g)
        {
            InitializeComponent();
            _parent = parent;
            _graphics = g;

            // Setup the context menu for the texture boxes
            // Shortcut menus for loading/clearing textures
            ContextMenu menu = new ContextMenu();
            MenuItem loadTex = new MenuItem("Load", new EventHandler(this.LoadBlendMask));
            MenuItem clearTex = new MenuItem("Clear", new EventHandler(this.ClearBlendMask));
            loadTex.ShowShortcut = false;
            clearTex.ShowShortcut = false;
            menu.MenuItems.Add(loadTex);
            menu.MenuItems.Add(clearTex);
            _blendMaskBox.ContextMenu = menu;

            // Initial images
            _defaultLayerMask = _graphics.LoadTexture("Resources\\Textures\\Masks\\gray.jpg");
            _defaultLayerMaskImage = Util.LoadBitmap("Resources\\Textures\\Masks\\gray.jpg");
            _blendMaskImages.Add(_defaultLayerMaskImage);
            _blendMasks.Add(_defaultLayerMask);
        }

        // Move a layer up
        private void OnMoveUp(object sender, EventArgs e)
        {
            int index = _layers.SelectedIndex;
            if (index != -1) 
            {
                if(index > 0)
                {
                    MaoliSharp.Material item = _layers.SelectedItem as MaoliSharp.Material;
                    _layers.Items.RemoveAt(index);
                    _layers.Items.Insert(index - 1, item);
                    _layers.SelectedIndex = index - 1;
                }
            }
        }

        // Move a layer down
        private void OnMoveDown(object sender, EventArgs e)
        {
            int index = _layers.SelectedIndex;
            if (index != -1) 
            {
                if (index < _layers.Items.Count-1)
                {
                    MaoliSharp.Material item = _layers.SelectedItem as MaoliSharp.Material;
                    _layers.Items.RemoveAt(index);
                    _layers.Items.Insert(index+1, item);
                    _layers.SelectedIndex = index + 1;
                }
            }
        }


        // Remove a layer
        private void OnDeleteLayer(object sender, EventArgs e)
        {
            int index = _layers.SelectedIndex;
            if (index != -1) 
                _layers.Items.RemoveAt(index);
        }

        // Add a new layer
        private void OnAddLayer(object sender, EventArgs e)
        {
            if(_parent.Browser.OpenBrowser())
            {
                _material = _parent.Browser.Material;
                _blendMaskImages.Add(_defaultLayerMaskImage);
                _blendMasks.Add(_defaultLayerMask);
                _layers.Items.Add(_parent.Browser.Material);
                _layers.SelectedIndex = _layers.Items.Count - 1;
                _parent.SetSelectedMaterial();
            }
        }

        // Load a layer material, replacing the current one
        private void OnLoadLayer(object sender, EventArgs e)
        {
            if(_layers.SelectedItem == null)
            {
                MessageBox.Show("Select the layer that you want to replace");
                return;
            }

            if (_parent.Browser.OpenBrowser())
            {
                _material = _parent.Browser.Material;
                _blendMaskImages.Add(_parent._noTexImage);
                _blendMasks.Add(null);
                _layers.Items[_layers.SelectedIndex] = _parent.Browser.Material;
                _parent.SetSelectedMaterial();
            }
        }


        // Select a layer
        private void OnSelectLayer(object sender, EventArgs e)
        {
            if (_layers.SelectedIndex != -1)
                _material = _layers.SelectedItem as MaoliSharp.Material;
            else
            {
                _material = _layers.Items[0] as MaoliSharp.Material;
                _layers.SelectedIndex = 0;
            }
            _parent.SetSelectedMaterial();
            _blendMaskBox.BackgroundImage = _blendMaskImages[_layers.SelectedIndex];
        }


        // Load a texture
        void LoadBlendMask(object sender, EventArgs e)
        {
            int index = _layers.SelectedIndex;
            String fileName = Util.GetOpenFileName("Resources\\Textures", Util.TextureFilter);
            if (fileName != null)
            {
                _blendMasks[index] = _graphics.LoadTexture(fileName);
                if (_blendMasks[index] == null)
                    MessageBox.Show("Texture Failed To Load");
                else
                {
                    _blendMaskImages[index] = Util.LoadBitmap(fileName);
                    _blendMaskBox.BackgroundImage = _blendMaskImages[index];
                }
            }
        }

        // Clear a texture
        void ClearBlendMask(object sender, EventArgs e)
        {
            int index = _layers.SelectedIndex;
            if (_blendMasks[index] != null)
            {
                _blendMasks[index] = null;
                _blendMaskImages[index].Dispose();
                _blendMaskImages[index] = _parent._noTexImage;
            }
        }


        // Blend two layers together
        public void BlendLayers()
        {
            if (_layers.Items.Count > 1)
            {
                // Blend the first two layers
                int numLayers = _layers.Items.Count;
                MaoliSharp.Material baseMaterial = _layers.Items[numLayers-2] as MaoliSharp.Material;
                MaoliSharp.Material layerMaterial = _layers.Items[numLayers-1] as MaoliSharp.Material;
                _graphics.BlendMaterials(baseMaterial, layerMaterial, _blendMasks[numLayers-1], _finalMaterial);

                // Continue blending
                bool final = true;
                for (int i = numLayers-3; i >= 0; --i)
                {
                    final = !final;
                    if (final)
                    {
                        baseMaterial = _blendStageMaterial;
                        layerMaterial = _layers.Items[i] as MaoliSharp.Material;
                        _graphics.BlendMaterials(baseMaterial, layerMaterial, _blendMasks[i], _finalMaterial);
                    }
                    else
                    {
                        baseMaterial = _finalMaterial;
                        layerMaterial = _layers.Items[i] as MaoliSharp.Material;
                        _graphics.BlendMaterials(baseMaterial, layerMaterial, _blendMasks[i], _blendStageMaterial);
                    }
                }

                // Bind the final materal to the mesh
                if (!final)
                    _finalMaterial = _blendStageMaterial;
                _material = _finalMaterial;
                _parent.SetSelectedMaterial();
            }
        }

    }
}

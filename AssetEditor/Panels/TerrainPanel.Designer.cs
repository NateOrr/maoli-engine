﻿namespace Honua.Panels
{
    partial class TerrainPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TerrainPanel));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this._sizeBox = new System.Windows.Forms.ComboBox();
            this._layer = new System.Windows.Forms.ComboBox();
            this._layerLabel = new System.Windows.Forms.Label();
            this.buttonTerrainRamp = new System.Windows.Forms.Button();
            this.buttonTerrainNoise = new System.Windows.Forms.Button();
            this.buttonTerrainSmooth = new System.Windows.Forms.Button();
            this.buttonTerrainErosion = new System.Windows.Forms.Button();
            this.buttonTerrainGrab = new System.Windows.Forms.Button();
            this.buttonTerrainLower = new System.Windows.Forms.Button();
            this.buttonTerrainRaise = new System.Windows.Forms.Button();
            this.buttonTerrainPaint = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this._radius = new Honua.FloatBar();
            this._hardness = new Honua.FloatBar();
            this._strength = new Honua.FloatBar();
            this._heightScale = new Honua.FloatBar();
            this._sizeScale = new Honua.FloatBar();
            this._layerMaterial = new System.Windows.Forms.PictureBox();
            this._layerMask = new System.Windows.Forms.PictureBox();
            this._maskLabel = new System.Windows.Forms.Label();
            this._materialLabel = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this._groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._layerMaterial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._layerMask)).BeginInit();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this._layerMask);
            this._groupBox.Controls.Add(this._layerMaterial);
            this._groupBox.Controls.Add(this._strength);
            this._groupBox.Controls.Add(this._hardness);
            this._groupBox.Controls.Add(this._sizeScale);
            this._groupBox.Controls.Add(this._heightScale);
            this._groupBox.Controls.Add(this._radius);
            this._groupBox.Controls.Add(this.buttonTerrainRamp);
            this._groupBox.Controls.Add(this.buttonTerrainNoise);
            this._groupBox.Controls.Add(this.buttonTerrainSmooth);
            this._groupBox.Controls.Add(this.buttonTerrainErosion);
            this._groupBox.Controls.Add(this.buttonTerrainGrab);
            this._groupBox.Controls.Add(this.buttonTerrainLower);
            this._groupBox.Controls.Add(this.buttonTerrainRaise);
            this._groupBox.Controls.Add(this.buttonTerrainPaint);
            this._groupBox.Controls.Add(this.label3);
            this._groupBox.Controls.Add(this._maskLabel);
            this._groupBox.Controls.Add(this._materialLabel);
            this._groupBox.Controls.Add(this._layerLabel);
            this._groupBox.Controls.Add(this._layer);
            this._groupBox.Controls.Add(this._sizeBox);
            this._groupBox.Controls.Add(this.button2);
            this._groupBox.Controls.Add(this.button4);
            this._groupBox.Controls.Add(this.button3);
            this._groupBox.Controls.Add(this.button5);
            this._groupBox.Controls.Add(this.button1);
            this._groupBox.Margin = new System.Windows.Forms.Padding(4);
            this._groupBox.Padding = new System.Windows.Forms.Padding(4);
            this._groupBox.Size = new System.Drawing.Size(524, 901);
            this._groupBox.Text = "Terrain";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(47, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 28);
            this.button1.TabIndex = 13;
            this.button1.Text = "Load Heightmap";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnLoadHeightmap);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(47, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 28);
            this.button2.TabIndex = 13;
            this.button2.Text = "Create Heightmap";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnCreateHeightmap);
            // 
            // _sizeBox
            // 
            this._sizeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sizeBox.FormattingEnabled = true;
            this._sizeBox.Items.AddRange(new object[] {
            "64",
            "128",
            "256",
            "512",
            "1024",
            "2048",
            "4096",
            "8192"});
            this._sizeBox.Location = new System.Drawing.Point(213, 50);
            this._sizeBox.Margin = new System.Windows.Forms.Padding(4);
            this._sizeBox.Name = "_sizeBox";
            this._sizeBox.Size = new System.Drawing.Size(99, 26);
            this._sizeBox.TabIndex = 14;
            // 
            // _layer
            // 
            this._layer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._layer.FormattingEnabled = true;
            this._layer.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this._layer.Location = new System.Drawing.Point(18, 709);
            this._layer.Margin = new System.Windows.Forms.Padding(4);
            this._layer.Name = "_layer";
            this._layer.Size = new System.Drawing.Size(99, 26);
            this._layer.TabIndex = 14;
            this._layer.SelectedIndexChanged += new System.EventHandler(this.OnChangeLayer);
            // 
            // _layerLabel
            // 
            this._layerLabel.AutoSize = true;
            this._layerLabel.Location = new System.Drawing.Point(42, 684);
            this._layerLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._layerLabel.Name = "_layerLabel";
            this._layerLabel.Size = new System.Drawing.Size(43, 18);
            this._layerLabel.TabIndex = 15;
            this._layerLabel.Text = "Layer";
            // 
            // buttonTerrainRamp
            // 
            this.buttonTerrainRamp.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainRamp.Enabled = false;
            this.buttonTerrainRamp.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainRamp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainRamp.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainRamp.Image")));
            this.buttonTerrainRamp.Location = new System.Drawing.Point(74, 436);
            this.buttonTerrainRamp.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainRamp.Name = "buttonTerrainRamp";
            this.buttonTerrainRamp.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainRamp.TabIndex = 67;
            this.buttonTerrainRamp.UseVisualStyleBackColor = false;
            this.buttonTerrainRamp.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // buttonTerrainNoise
            // 
            this.buttonTerrainNoise.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainNoise.Enabled = false;
            this.buttonTerrainNoise.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainNoise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainNoise.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainNoise.Image")));
            this.buttonTerrainNoise.Location = new System.Drawing.Point(134, 436);
            this.buttonTerrainNoise.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainNoise.Name = "buttonTerrainNoise";
            this.buttonTerrainNoise.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainNoise.TabIndex = 68;
            this.buttonTerrainNoise.UseVisualStyleBackColor = false;
            this.buttonTerrainNoise.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // buttonTerrainSmooth
            // 
            this.buttonTerrainSmooth.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainSmooth.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainSmooth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainSmooth.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainSmooth.Image")));
            this.buttonTerrainSmooth.Location = new System.Drawing.Point(254, 377);
            this.buttonTerrainSmooth.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainSmooth.Name = "buttonTerrainSmooth";
            this.buttonTerrainSmooth.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainSmooth.TabIndex = 66;
            this.buttonTerrainSmooth.UseVisualStyleBackColor = false;
            this.buttonTerrainSmooth.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // buttonTerrainErosion
            // 
            this.buttonTerrainErosion.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainErosion.Enabled = false;
            this.buttonTerrainErosion.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainErosion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainErosion.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainErosion.Image")));
            this.buttonTerrainErosion.Location = new System.Drawing.Point(194, 436);
            this.buttonTerrainErosion.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainErosion.Name = "buttonTerrainErosion";
            this.buttonTerrainErosion.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainErosion.TabIndex = 69;
            this.buttonTerrainErosion.UseVisualStyleBackColor = false;
            this.buttonTerrainErosion.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // buttonTerrainGrab
            // 
            this.buttonTerrainGrab.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainGrab.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainGrab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainGrab.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainGrab.Image")));
            this.buttonTerrainGrab.Location = new System.Drawing.Point(194, 377);
            this.buttonTerrainGrab.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainGrab.Name = "buttonTerrainGrab";
            this.buttonTerrainGrab.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainGrab.TabIndex = 65;
            this.buttonTerrainGrab.UseVisualStyleBackColor = false;
            this.buttonTerrainGrab.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // buttonTerrainLower
            // 
            this.buttonTerrainLower.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainLower.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainLower.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainLower.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainLower.Image")));
            this.buttonTerrainLower.Location = new System.Drawing.Point(134, 377);
            this.buttonTerrainLower.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainLower.Name = "buttonTerrainLower";
            this.buttonTerrainLower.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainLower.TabIndex = 64;
            this.buttonTerrainLower.UseVisualStyleBackColor = false;
            this.buttonTerrainLower.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // buttonTerrainRaise
            // 
            this.buttonTerrainRaise.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainRaise.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainRaise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainRaise.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainRaise.Image")));
            this.buttonTerrainRaise.Location = new System.Drawing.Point(74, 377);
            this.buttonTerrainRaise.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainRaise.Name = "buttonTerrainRaise";
            this.buttonTerrainRaise.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainRaise.TabIndex = 63;
            this.buttonTerrainRaise.UseVisualStyleBackColor = false;
            this.buttonTerrainRaise.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // buttonTerrainPaint
            // 
            this.buttonTerrainPaint.BackColor = System.Drawing.SystemColors.Control;
            this.buttonTerrainPaint.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonTerrainPaint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTerrainPaint.Image = ((System.Drawing.Image)(resources.GetObject("buttonTerrainPaint.Image")));
            this.buttonTerrainPaint.Location = new System.Drawing.Point(254, 436);
            this.buttonTerrainPaint.Margin = new System.Windows.Forms.Padding(4);
            this.buttonTerrainPaint.Name = "buttonTerrainPaint";
            this.buttonTerrainPaint.Size = new System.Drawing.Size(53, 51);
            this.buttonTerrainPaint.TabIndex = 70;
            this.buttonTerrainPaint.UseVisualStyleBackColor = false;
            this.buttonTerrainPaint.Click += new System.EventHandler(this.OnSetSculptMode);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Candara", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(128, 325);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 33);
            this.label3.TabIndex = 15;
            this.label3.Text = "Sculpting";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(189, 145);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(99, 28);
            this.button3.TabIndex = 13;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.OnSave);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(69, 145);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(99, 28);
            this.button4.TabIndex = 13;
            this.button4.Text = "Load";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.OnLoad);
            // 
            // _radius
            // 
            this._radius.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._radius.Location = new System.Drawing.Point(70, 513);
            this._radius.Maximum = 100F;
            this._radius.Minimum = 0.01F;
            this._radius.Name = "_radius";
            this._radius.NumTicks = 1000;
            this._radius.PropertyPage = null;
            this._radius.Size = new System.Drawing.Size(237, 38);
            this._radius.TabIndex = 71;
            this._radius.TextAlignment = 100;
            this._radius.Value = 1F;
            this._radius.ValueChanged = null;
            // 
            // _hardness
            // 
            this._hardness.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._hardness.Location = new System.Drawing.Point(70, 557);
            this._hardness.Maximum = 100F;
            this._hardness.Minimum = 0.01F;
            this._hardness.Name = "_hardness";
            this._hardness.NumTicks = 1000;
            this._hardness.Property = null;
            this._hardness.PropertyPage = null;
            this._hardness.Size = new System.Drawing.Size(237, 38);
            this._hardness.TabIndex = 71;
            this._hardness.TextAlignment = 100;
            this._hardness.Value = 1F;
            this._hardness.ValueChanged = null;
            // 
            // _strength
            // 
            this._strength.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._strength.Location = new System.Drawing.Point(70, 601);
            this._strength.Maximum = 5F;
            this._strength.Minimum = 0.01F;
            this._strength.Name = "_strength";
            this._strength.NumTicks = 1000;
            this._strength.Property = null;
            this._strength.PropertyPage = null;
            this._strength.Size = new System.Drawing.Size(237, 38);
            this._strength.TabIndex = 71;
            this._strength.TextAlignment = 100;
            this._strength.Value = 1F;
            this._strength.ValueChanged = null;
            // 
            // _heightScale
            // 
            this._heightScale.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._heightScale.Location = new System.Drawing.Point(51, 213);
            this._heightScale.Maximum = 500F;
            this._heightScale.Minimum = 0.01F;
            this._heightScale.Name = "_heightScale";
            this._heightScale.NumTicks = 1000;
            this._heightScale.Property = null;
            this._heightScale.PropertyPage = null;
            this._heightScale.Size = new System.Drawing.Size(237, 38);
            this._heightScale.TabIndex = 71;
            this._heightScale.TextAlignment = 100;
            this._heightScale.Value = 1F;
            this._heightScale.ValueChanged = null;
            // 
            // _sizeScale
            // 
            this._sizeScale.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._sizeScale.Location = new System.Drawing.Point(51, 257);
            this._sizeScale.Maximum = 20F;
            this._sizeScale.Minimum = 0.01F;
            this._sizeScale.Name = "_sizeScale";
            this._sizeScale.NumTicks = 1000;
            this._sizeScale.Property = null;
            this._sizeScale.PropertyPage = null;
            this._sizeScale.Size = new System.Drawing.Size(237, 38);
            this._sizeScale.TabIndex = 71;
            this._sizeScale.TextAlignment = 100;
            this._sizeScale.Value = 1F;
            this._sizeScale.ValueChanged = null;
            // 
            // _layerMaterial
            // 
            this._layerMaterial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._layerMaterial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._layerMaterial.Location = new System.Drawing.Point(152, 709);
            this._layerMaterial.Name = "_layerMaterial";
            this._layerMaterial.Size = new System.Drawing.Size(128, 128);
            this._layerMaterial.TabIndex = 73;
            this._layerMaterial.TabStop = false;
            this._layerMaterial.Click += new System.EventHandler(this.OnSetMaterial);
            // 
            // _layerMask
            // 
            this._layerMask.BackColor = System.Drawing.Color.White;
            this._layerMask.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._layerMask.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._layerMask.Location = new System.Drawing.Point(317, 709);
            this._layerMask.Name = "_layerMask";
            this._layerMask.Size = new System.Drawing.Size(128, 128);
            this._layerMask.TabIndex = 73;
            this._layerMask.TabStop = false;
            // 
            // _maskLabel
            // 
            this._maskLabel.AutoSize = true;
            this._maskLabel.Location = new System.Drawing.Point(339, 686);
            this._maskLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._maskLabel.Name = "_maskLabel";
            this._maskLabel.Size = new System.Drawing.Size(80, 18);
            this._maskLabel.TabIndex = 15;
            this._maskLabel.Text = "Layer Mask";
            // 
            // _materialLabel
            // 
            this._materialLabel.AutoSize = true;
            this._materialLabel.Location = new System.Drawing.Point(188, 686);
            this._materialLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._materialLabel.Name = "_materialLabel";
            this._materialLabel.Size = new System.Drawing.Size(62, 18);
            this._materialLabel.TabIndex = 15;
            this._materialLabel.Text = "Material";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(227, 95);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(160, 28);
            this.button5.TabIndex = 13;
            this.button5.Text = "Load Blendmap";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.OnLoadBlendmap);
            // 
            // TerrainPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 914);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TerrainPanel";
            this.Text = "TerrainPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._layerMaterial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._layerMask)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox _sizeBox;
        private System.Windows.Forms.Label _layerLabel;
        private System.Windows.Forms.ComboBox _layer;
        private System.Windows.Forms.Button buttonTerrainRamp;
        private System.Windows.Forms.Button buttonTerrainNoise;
        private System.Windows.Forms.Button buttonTerrainSmooth;
        private System.Windows.Forms.Button buttonTerrainErosion;
        private System.Windows.Forms.Button buttonTerrainGrab;
        private System.Windows.Forms.Button buttonTerrainLower;
        private System.Windows.Forms.Button buttonTerrainRaise;
        private System.Windows.Forms.Button buttonTerrainPaint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private FloatBar _strength;
        private FloatBar _hardness;
        private FloatBar _radius;
        private FloatBar _heightScale;
        private FloatBar _sizeScale;
        private System.Windows.Forms.PictureBox _layerMaterial;
        private System.Windows.Forms.PictureBox _layerMask;
        private System.Windows.Forms.Label _maskLabel;
        private System.Windows.Forms.Label _materialLabel;
        private System.Windows.Forms.Button button5;
    }
}
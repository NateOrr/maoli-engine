﻿namespace Honua.Panels
{
    partial class EntityPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._groupBox = new Honua.CollapsibleGroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this._entityName = new System.Windows.Forms.TextBox();
            this._components = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._componentTypes = new System.Windows.Forms.ComboBox();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._groupBox.Controls.Add(this.button3);
            this._groupBox.Controls.Add(this._entityName);
            this._groupBox.Controls.Add(this._components);
            this._groupBox.Controls.Add(this.button2);
            this._groupBox.Controls.Add(this.button1);
            this._groupBox.Controls.Add(this.label2);
            this._groupBox.Controls.Add(this.label1);
            this._groupBox.Controls.Add(this._componentTypes);
            this._groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this._groupBox.Location = new System.Drawing.Point(0, 0);
            this._groupBox.Name = "_groupBox";
            this._groupBox.Size = new System.Drawing.Size(334, 231);
            this._groupBox.TabIndex = 1;
            this._groupBox.TabStop = false;
            this._groupBox.Text = "Entity";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(205, 36);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(97, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "New";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.OnNewEntity);
            // 
            // _entityName
            // 
            this._entityName.Location = new System.Drawing.Point(98, 35);
            this._entityName.Name = "_entityName";
            this._entityName.Size = new System.Drawing.Size(100, 26);
            this._entityName.TabIndex = 4;
            this._entityName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnEntityNameBoxKeyPress);
            this._entityName.Leave += new System.EventHandler(this.OnRenameEntity);
            // 
            // _components
            // 
            this._components.FormattingEnabled = true;
            this._components.ItemHeight = 18;
            this._components.Location = new System.Drawing.Point(20, 131);
            this._components.Name = "_components";
            this._components.Size = new System.Drawing.Size(178, 58);
            this._components.TabIndex = 3;
            this._components.SelectedIndexChanged += new System.EventHandler(this.OnSelectComponent);
            this._components.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IgnoreKeydown);
            this._components.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IgnoreKeyPress);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(204, 153);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Delete Selected";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnDeleteComponent);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(204, 82);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Add Component";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnAddComponent);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Entity Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Component Types";
            // 
            // _componentTypes
            // 
            this._componentTypes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._componentTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._componentTypes.FormattingEnabled = true;
            this._componentTypes.Location = new System.Drawing.Point(20, 84);
            this._componentTypes.Name = "_componentTypes";
            this._componentTypes.Size = new System.Drawing.Size(178, 27);
            this._componentTypes.TabIndex = 0;
            this._componentTypes.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnComponentListDrawItem);
            this._componentTypes.DropDown += new System.EventHandler(this.OnComponentListOpen);
            this._componentTypes.DropDownClosed += new System.EventHandler(this.OnComponentListClosed);
            // 
            // EntityPanel
            // 
            this.ClientSize = new System.Drawing.Size(334, 523);
            this.Controls.Add(this._groupBox);
            this.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "EntityPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Honua.CollapsibleGroupBox _groupBox;
        private System.Windows.Forms.ComboBox _componentTypes;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox _components;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox _entityName;
        private System.Windows.Forms.Label label2;

    }
}
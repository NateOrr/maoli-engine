﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class OceanPanel : Honua.ComponentPanel
    {
        // Mesh
        HonuaEditor _editor;
        MaoliSharp.Ocean _ocean;

        // Ctor
        public OceanPanel(HonuaEditor parent)
        {
            InitializeComponent();
            _editor = parent;
        }

        // Set the ocean
        public void SetOcean(MaoliSharp.Ocean ocean)
        {
            _ocean = ocean;
            _properties.SelectedObject = _ocean;
        }

    }
}

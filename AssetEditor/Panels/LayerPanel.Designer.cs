﻿namespace Honua.Panels
{
    partial class LayerPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._groupBox = new Honua.CollapsibleGroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this._blendMaskBox = new System.Windows.Forms.PictureBox();
            this._moveDownButton = new System.Windows.Forms.Button();
            this._moveUpButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this._addLayerButton = new System.Windows.Forms.Button();
            this._deleteButton = new System.Windows.Forms.Button();
            this._layers = new System.Windows.Forms.ListBox();
            this._groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._blendMaskBox)).BeginInit();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.Controls.Add(this.label1);
            this._groupBox.Controls.Add(this._blendMaskBox);
            this._groupBox.Controls.Add(this._moveDownButton);
            this._groupBox.Controls.Add(this._moveUpButton);
            this._groupBox.Controls.Add(this.button2);
            this._groupBox.Controls.Add(this._addLayerButton);
            this._groupBox.Controls.Add(this._deleteButton);
            this._groupBox.Controls.Add(this._layers);
            this._groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this._groupBox.Location = new System.Drawing.Point(0, 0);
            this._groupBox.Margin = new System.Windows.Forms.Padding(4);
            this._groupBox.Name = "_groupBox";
            this._groupBox.Padding = new System.Windows.Forms.Padding(4);
            this._groupBox.Size = new System.Drawing.Size(648, 368);
            this._groupBox.TabIndex = 0;
            this._groupBox.TabStop = false;
            this._groupBox.Text = "Layers";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 231);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Blend Mask";
            // 
            // _blendMaskBox
            // 
            this._blendMaskBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._blendMaskBox.Location = new System.Drawing.Point(81, 251);
            this._blendMaskBox.Margin = new System.Windows.Forms.Padding(4);
            this._blendMaskBox.Name = "_blendMaskBox";
            this._blendMaskBox.Size = new System.Drawing.Size(129, 102);
            this._blendMaskBox.TabIndex = 5;
            this._blendMaskBox.TabStop = false;
            // 
            // _moveDownButton
            // 
            this._moveDownButton.Location = new System.Drawing.Point(287, 133);
            this._moveDownButton.Margin = new System.Windows.Forms.Padding(4);
            this._moveDownButton.Name = "_moveDownButton";
            this._moveDownButton.Size = new System.Drawing.Size(100, 28);
            this._moveDownButton.TabIndex = 4;
            this._moveDownButton.Text = "Move Down";
            this._moveDownButton.UseVisualStyleBackColor = true;
            this._moveDownButton.Click += new System.EventHandler(this.OnMoveDown);
            // 
            // _moveUpButton
            // 
            this._moveUpButton.Location = new System.Drawing.Point(287, 97);
            this._moveUpButton.Margin = new System.Windows.Forms.Padding(4);
            this._moveUpButton.Name = "_moveUpButton";
            this._moveUpButton.Size = new System.Drawing.Size(100, 28);
            this._moveUpButton.TabIndex = 3;
            this._moveUpButton.Text = "Move Up";
            this._moveUpButton.UseVisualStyleBackColor = true;
            this._moveUpButton.Click += new System.EventHandler(this.OnMoveUp);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(287, 59);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 1;
            this.button2.Text = "Load Layer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnLoadLayer);
            // 
            // _addLayerButton
            // 
            this._addLayerButton.Location = new System.Drawing.Point(287, 23);
            this._addLayerButton.Margin = new System.Windows.Forms.Padding(4);
            this._addLayerButton.Name = "_addLayerButton";
            this._addLayerButton.Size = new System.Drawing.Size(100, 28);
            this._addLayerButton.TabIndex = 1;
            this._addLayerButton.Text = "Add Layer";
            this._addLayerButton.UseVisualStyleBackColor = true;
            this._addLayerButton.Click += new System.EventHandler(this.OnAddLayer);
            // 
            // _deleteButton
            // 
            this._deleteButton.Location = new System.Drawing.Point(287, 169);
            this._deleteButton.Margin = new System.Windows.Forms.Padding(4);
            this._deleteButton.Name = "_deleteButton";
            this._deleteButton.Size = new System.Drawing.Size(100, 28);
            this._deleteButton.TabIndex = 1;
            this._deleteButton.Text = "Delete Layer";
            this._deleteButton.UseVisualStyleBackColor = true;
            this._deleteButton.Click += new System.EventHandler(this.OnDeleteLayer);
            // 
            // _layers
            // 
            this._layers.FormattingEnabled = true;
            this._layers.ItemHeight = 16;
            this._layers.Location = new System.Drawing.Point(16, 23);
            this._layers.Margin = new System.Windows.Forms.Padding(4);
            this._layers.Name = "_layers";
            this._layers.Size = new System.Drawing.Size(261, 196);
            this._layers.TabIndex = 0;
            this._layers.SelectedIndexChanged += new System.EventHandler(this.OnSelectLayer);
            // 
            // LayerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 380);
            this.Controls.Add(this._groupBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LayerPanel";
            this.Text = "LayerPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._blendMaskBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Honua.CollapsibleGroupBox _groupBox;
        public System.Windows.Forms.Button _moveDownButton;
        public System.Windows.Forms.Button _moveUpButton;
        public System.Windows.Forms.Button _addLayerButton;
        public System.Windows.Forms.Button _deleteButton;
        public System.Windows.Forms.ListBox _layers;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.PictureBox _blendMaskBox;
        public System.Windows.Forms.Button button2;

    }
}
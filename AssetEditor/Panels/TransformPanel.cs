﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua.Panels
{
    public partial class TransformPanel : Honua.ComponentPanel
    {
        MaoliSharp.Renderer _graphics;
        HonuaEditor _editor;
        VectorControl _position;
        VectorControl _rotation;
        VectorControl _scale;

        // Mesh
        public object _component;

        // Ctor
        public TransformPanel(HonuaEditor parent)
        {
            InitializeComponent();
            _editor = parent;
            _graphics = parent.Graphics;

            _position = new VectorControl();
            _rotation = new VectorControl();
            _scale = new VectorControl();

            _position.TextAlignment = 100;
            _rotation.TextAlignment = 100;
            _scale.TextAlignment = 100;

            _groupBox.Controls.Add(_position);
            _groupBox.Controls.Add(_rotation);
            _groupBox.Controls.Add(_scale);

            _rotation.Location = new Point(_rotation.Location.X, _position.Location.Y + _position.Height);
            _scale.Location = new Point(_scale.Location.X, _rotation.Location.Y + _rotation.Height);
        }


        // Fill the mesh box
        public override void SetComponent(object component)
        {
            // Validate the object
            _component = component;
            if (component == null)
            {
                _groupBox.Visible = false;
                return;
            }

            var type = _component.GetType();
            var props = type.GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(MaoliSharp.AxisWidget))).ToList();
            if (props.Count == 0)
            {
                _groupBox.Visible = false;
                return;
            }

            // Hide controls
            _position.Enabled = false;
            _rotation.Enabled = false;
            _scale.Enabled = false;

            // Grab the properies with the widget attribute
            _groupBox.Visible = true;
            _groupBox.BringToFront();
            foreach (var prop in props)
            {
                var attr = prop.GetCustomAttribute(typeof(MaoliSharp.AxisWidget)) as MaoliSharp.AxisWidget;
                switch (attr.Axis)
                {
                    case MaoliSharp.AxisMode.Translate:
                        if (prop.PropertyType == typeof(MaoliSharp.Vector3))
                        {
                            _position.Enabled = true;
                            _position.SelectedObject = _component;
                            _position.Property = prop;
                        }
                        break;

                    case MaoliSharp.AxisMode.Rotate:
                        if (prop.PropertyType == typeof(MaoliSharp.Vector3))
                        {
                            _rotation.Enabled = true;
                            _rotation.SelectedObject = _component;
                            _rotation.Property = prop;
                        }
                        break;

                    case MaoliSharp.AxisMode.Scale:
                        if (prop.PropertyType == typeof(MaoliSharp.Vector3))
                        {
                            _scale.Enabled = true;
                            _scale.SelectedObject = _component;
                            _scale.Property = prop;
                        }
                        break;
                }
            }

            // Update the controls
            UpdateControls();
        }

        // Update controls with new object values
        public void UpdateControls()
        {
            if (_component != null)
            {
                if (_scale.Enabled && _scale.Property != null)
                    _scale.Value = (MaoliSharp.Vector3)_scale.Property.GetValue(_component);

                if (_position.Enabled && _position.Property != null)
                    _position.Value = (MaoliSharp.Vector3)_position.Property.GetValue(_component);

                if (_rotation.Enabled && _rotation.Property != null)
                    _rotation.Value = (MaoliSharp.Vector3)_rotation.Property.GetValue(_component);
            }
        }

    }
}

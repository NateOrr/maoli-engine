﻿namespace Honua.Panels
{
    partial class WorldPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._groupBox = new Honua.CollapsibleGroupBox();
            this._softness = new Honua.FloatBar();
            this._tod = new Honua.FloatBar();
            this._sky = new System.Windows.Forms.CheckBox();
            this._ambient = new System.Windows.Forms.Panel();
            this._entities = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._entityTypes = new System.Windows.Forms.ComboBox();
            this._ocean = new System.Windows.Forms.CheckBox();
            this._groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupBox
            // 
            this._groupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._groupBox.Controls.Add(this._softness);
            this._groupBox.Controls.Add(this._tod);
            this._groupBox.Controls.Add(this._ocean);
            this._groupBox.Controls.Add(this._sky);
            this._groupBox.Controls.Add(this._ambient);
            this._groupBox.Controls.Add(this._entities);
            this._groupBox.Controls.Add(this.button2);
            this._groupBox.Controls.Add(this.button1);
            this._groupBox.Controls.Add(this.label2);
            this._groupBox.Controls.Add(this.label3);
            this._groupBox.Controls.Add(this.label1);
            this._groupBox.Controls.Add(this._entityTypes);
            this._groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this._groupBox.Location = new System.Drawing.Point(0, 0);
            this._groupBox.Name = "_groupBox";
            this._groupBox.Size = new System.Drawing.Size(381, 435);
            this._groupBox.TabIndex = 1;
            this._groupBox.TabStop = false;
            this._groupBox.Text = "Game World";
            // 
            // _softness
            // 
            this._softness.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._softness.Location = new System.Drawing.Point(97, 112);
            this._softness.Margin = new System.Windows.Forms.Padding(2);
            this._softness.Maximum = 24F;
            this._softness.Minimum = 0F;
            this._softness.Name = "_softness";
            this._softness.NumTicks = 1000;
            this._softness.Property = null;
            this._softness.PropertyPage = null;
            this._softness.Size = new System.Drawing.Size(237, 38);
            this._softness.TabIndex = 127;
            this._softness.TextAlignment = 100;
            this._softness.Value = 2F;
            this._softness.ValueChanged = null;
            // 
            // _tod
            // 
            this._tod.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._tod.Location = new System.Drawing.Point(97, 73);
            this._tod.Margin = new System.Windows.Forms.Padding(2);
            this._tod.Maximum = 24F;
            this._tod.Minimum = 0F;
            this._tod.Name = "_tod";
            this._tod.NumTicks = 24;
            this._tod.Property = null;
            this._tod.PropertyPage = null;
            this._tod.Size = new System.Drawing.Size(237, 38);
            this._tod.TabIndex = 127;
            this._tod.TextAlignment = 100;
            this._tod.Value = 6F;
            this._tod.ValueChanged = null;
            // 
            // _sky
            // 
            this._sky.AutoSize = true;
            this._sky.Location = new System.Drawing.Point(28, 82);
            this._sky.Name = "_sky";
            this._sky.Size = new System.Drawing.Size(53, 22);
            this._sky.TabIndex = 126;
            this._sky.Text = "Sky";
            this._sky.UseVisualStyleBackColor = true;
            this._sky.CheckedChanged += new System.EventHandler(this.OnEnableSky);
            this._sky.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IgnoreKeyDown);
            // 
            // _ambient
            // 
            this._ambient.BackColor = System.Drawing.SystemColors.Control;
            this._ambient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ambient.Location = new System.Drawing.Point(174, 39);
            this._ambient.Name = "_ambient";
            this._ambient.Size = new System.Drawing.Size(54, 21);
            this._ambient.TabIndex = 125;
            this._ambient.Click += new System.EventHandler(this.OnSetAmbientColor);
            // 
            // _entities
            // 
            this._entities.FormattingEnabled = true;
            this._entities.ItemHeight = 18;
            this._entities.Location = new System.Drawing.Point(28, 288);
            this._entities.Name = "_entities";
            this._entities.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._entities.Size = new System.Drawing.Size(178, 130);
            this._entities.TabIndex = 3;
            this._entities.SelectedIndexChanged += new System.EventHandler(this.OnSelectEntity);
            this._entities.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IgnoreKeyDown);
            this._entities.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IgnoreKeyPress);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(212, 310);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Delete Selected";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnDeleteEntity);
            this.button2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IgnoreKeyDown);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 225);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnAddEntity);
            this.button1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IgnoreKeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "World Entities";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Ambient Lighting";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 208);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Entity Templates";
            // 
            // _entityTypes
            // 
            this._entityTypes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._entityTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._entityTypes.FormattingEnabled = true;
            this._entityTypes.Location = new System.Drawing.Point(28, 227);
            this._entityTypes.Name = "_entityTypes";
            this._entityTypes.Size = new System.Drawing.Size(178, 27);
            this._entityTypes.TabIndex = 0;
            this._entityTypes.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnEntityListDrawItem);
            this._entityTypes.DropDown += new System.EventHandler(this.OnEntityListOpen);
            this._entityTypes.DropDownClosed += new System.EventHandler(this.OnEntityListClosed);
            this._entityTypes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IgnoreKeyDown);
            this._entityTypes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IgnoreKeyPress);
            // 
            // _ocean
            // 
            this._ocean.AutoSize = true;
            this._ocean.Location = new System.Drawing.Point(28, 167);
            this._ocean.Name = "_ocean";
            this._ocean.Size = new System.Drawing.Size(70, 22);
            this._ocean.TabIndex = 126;
            this._ocean.Text = "Ocean";
            this._ocean.UseVisualStyleBackColor = true;
            this._ocean.CheckedChanged += new System.EventHandler(this.OnEnableOcean);
            this._ocean.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IgnoreKeyDown);
            // 
            // WorldPanel
            // 
            this.ClientSize = new System.Drawing.Size(381, 493);
            this.Controls.Add(this._groupBox);
            this.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "WorldPanel";
            this._groupBox.ResumeLayout(false);
            this._groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public Honua.CollapsibleGroupBox _groupBox;
        private System.Windows.Forms.ComboBox _entityTypes;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox _entities;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private Honua.FloatBar _tod;
        private System.Windows.Forms.CheckBox _sky;
        private System.Windows.Forms.Panel _ambient;
        private System.Windows.Forms.Label label3;
        private Honua.FloatBar _softness;
        private System.Windows.Forms.CheckBox _ocean;

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class TerrainPanel : Honua.ComponentPanel
    {
        MaoliSharp.Renderer _graphics;
        HonuaEditor _editor;

        MaoliSharp.Terrain _terrain;    // The terrain

        // Ctor
        public TerrainPanel(HonuaEditor parent)
        {
            InitializeComponent();
            _editor = parent;
            _graphics = parent.Graphics;
            _sizeBox.SelectedIndex = 0;
            _layer.SelectedIndex = 0;

            // Value callbacks
            _radius.ValueChanged += new EventHandler(this.OnSetRadius);
            _hardness.ValueChanged += new EventHandler(this.OnSetHardness);

            // Context menu for layer mask
            ContextMenu menu = new ContextMenu();
            MenuItem loadTex = new MenuItem("Load", new EventHandler(this.OnLoadLayerMask));
            MenuItem clearTex = new MenuItem("Clear", new EventHandler(this.OnClearLayerMask));
            loadTex.ShowShortcut = false;
            clearTex.ShowShortcut = false;
            menu.MenuItems.Add(loadTex);
            menu.MenuItems.Add(clearTex);
            _layerMask.ContextMenu = menu;

            // Initial mode
            OnSetSculptMode(this.buttonTerrainRaise, new EventArgs());
        }

        // Load the layer mask
        void OnLoadLayerMask(object sender, EventArgs e)
        {
            if (_layer.SelectedIndex != -1)
            {
                String file = Util.GetOpenFileName("Resources\\Textures", Util.TextureFilter);
                if (file != null)
                {
                    _layerMask.BackgroundImage = Util.LoadBitmap(file);
                    _editor.Graphics.LoadLayerMask(file, _layer.SelectedIndex);
                }
            }
        }

        // Load the layer mask
        void OnClearLayerMask(object sender, EventArgs e)
        {
            if (_layer.SelectedIndex != -1)
            {
                _editor.Graphics.ClearLayerMask(_layer.SelectedIndex);
            }
        }

        // Radius / hardness relationship
        void OnSetRadius(object sender, EventArgs e)
        {
            if (_terrain != null)
            {
                if (_radius.Value < _terrain.Hardness)
                    _radius.Value = _terrain.Hardness;
            }
        }

        // Radius / hardness relationship
        void OnSetHardness(object sender, EventArgs e)
        {
            if (_terrain != null)
            {
                if (_hardness.Value > _terrain.Radius)
                    _hardness.Value = _terrain.Radius;
            }
        }

        // Fill the mesh box
        public void SetTerrain(MaoliSharp.Terrain terrain)
        {
            _terrain = terrain;
            _layer.SelectedIndex = -1;
            if (_terrain != null)
            {
                _radius.SelectedObject = terrain;
                _hardness.SelectedObject = terrain;
                _strength.SelectedObject = terrain;
                _heightScale.SelectedObject = terrain;
                _sizeScale.SelectedObject = terrain;

                _radius.Property = typeof(MaoliSharp.Terrain).GetProperty("Radius");
                _hardness.Property = typeof(MaoliSharp.Terrain).GetProperty("Hardness");
                _strength.Property = typeof(MaoliSharp.Terrain).GetProperty("Strength");
                _heightScale.Property = typeof(MaoliSharp.Terrain).GetProperty("HeightScale");
                _sizeScale.Property = typeof(MaoliSharp.Terrain).GetProperty("SizeScale");

                _radius.Text = "Radius";
                _hardness.Text = "Hardness";
                _strength.Text = "Strength";
                _heightScale.Text = "HeightScale";
                _sizeScale.Text = "SizeScale";

                _layer.SelectedIndex = 0;
            }

            bool active = (_terrain != null);
            _radius.Enabled = active;
            _hardness.Enabled = active;
            _strength.Enabled = active;
            _heightScale.Enabled = active;
            _sizeScale.Enabled = active;
        }

        // Load a terrain heightmap
        private void OnLoadHeightmap(object sender, EventArgs e)
        {
            String file = Util.GetOpenFileName("Resources\\Textures\\Heightmaps", MaoliSharp.Terrain.GetFileFilter());
            if (file != null)
            {
                _terrain = _editor.Graphics.LoadHeightmap(file);
                if (_terrain != null)
                {
                    MaoliSharp.Material mat = _editor.Graphics.LoadMaterial("Resources\\Materials\\terrain_default.mat");
                    _terrain.SetMaterial(0, mat);
                }
                SetTerrain(_terrain);
            }
        }

        // Create a new heightmap
        private void OnCreateHeightmap(object sender, EventArgs e)
        {
            _terrain = _editor.Graphics.CreateTerrain(uint.Parse(_sizeBox.SelectedItem as String));
            if (_terrain != null)
            {
                MaoliSharp.Material mat = _editor.Graphics.LoadMaterial("Resources\\Materials\\terrain_default.mat");
                _terrain.SetMaterial(0, mat);
            }
            SetTerrain(_terrain);
        }

        // Editor update
        public override void OnRender()
        {
            if (_terrain == null)
                return;

            _editor.Graphics.DrawTerrainWidget();
            if (_editor.Engine.Platform.MouseLeftDown())
            {
                _editor.Graphics.SculptTerrain();
            }
        }

        // Set the sculpting mode
        private void OnSetSculptMode(object sender, EventArgs e)
        {
            buttonTerrainLower.FlatAppearance.BorderColor = Color.Black;
            buttonTerrainRaise.FlatAppearance.BorderColor = Color.Black;
            buttonTerrainSmooth.FlatAppearance.BorderColor = Color.Black;
            buttonTerrainErosion.FlatAppearance.BorderColor = Color.Black;
            buttonTerrainRamp.FlatAppearance.BorderColor = Color.Black;
            buttonTerrainPaint.FlatAppearance.BorderColor = Color.Black;
            buttonTerrainGrab.FlatAppearance.BorderColor = Color.Black;
            buttonTerrainNoise.FlatAppearance.BorderColor = Color.Black;
            ((Button)sender).FlatAppearance.BorderColor = Color.Blue;
            if (sender == buttonTerrainLower)
                _editor.Graphics.SetTerrainSculptMode(MaoliSharp.SculptMode.Lower);
            else if (sender == buttonTerrainRaise)
                _editor.Graphics.SetTerrainSculptMode(MaoliSharp.SculptMode.Raise);
            else if (sender == buttonTerrainSmooth)
                _editor.Graphics.SetTerrainSculptMode(MaoliSharp.SculptMode.Smooth);
            else if (sender == buttonTerrainGrab)
                _editor.Graphics.SetTerrainSculptMode(MaoliSharp.SculptMode.Grab);
            else if (sender == buttonTerrainPaint)
                _editor.Graphics.SetTerrainSculptMode(MaoliSharp.SculptMode.Paint);

            // Hide painting tools as needed
            _layerMask.Visible = (sender == buttonTerrainPaint);
            _layerMaterial.Visible = (sender == buttonTerrainPaint);
            _layer.Visible = (sender == buttonTerrainPaint);
            _layerLabel.Visible = (sender == buttonTerrainPaint);
            _materialLabel.Visible = (sender == buttonTerrainPaint);
            _maskLabel.Visible = (sender == buttonTerrainPaint);
        }

        // Save the terrain
        private void OnSave(object sender, EventArgs e)
        {
            string file = Util.GetSaveFileName("Resources\\Textures\\Heightmaps\\", "Terrain Files|*.png");
            if(file != null)
            {
                _terrain.ExportHeightmap(file);
                _terrain.ExportBlendmap(Path.GetDirectoryName(file) + "\\" + Path.GetFileNameWithoutExtension(file) + "_blend.png");
            }
        }

        private void OnLoad(object sender, EventArgs e)
        {

        }

        // Cleanup
        public void Release()
        {
            _terrain = null;
        }

        // Set the layer material
        private void OnSetMaterial(object sender, EventArgs e)
        {
            uint layer = (uint)_layer.SelectedIndex;
            MaoliSharp.Material mat = _editor.ChooseMaterial();
            if (mat != null)
            {
                _terrain.SetMaterial(layer, mat);
                _layerMaterial.BackgroundImage = Util.LoadBitmap(mat.GetPreviewFile());
            }
        }

        // Change the active layer for painting
        private void OnChangeLayer(object sender, EventArgs e)
        {
            if (_layer.SelectedIndex != -1)
            {
                // Mask texture preview
                String maskFile = _editor.Graphics.GetLayerMask(_layer.SelectedIndex);
                if (maskFile != null)
                    _layerMask.BackgroundImage = Util.LoadBitmap(maskFile);
                else
                {
                    if (_layerMask.BackgroundImage != null)
                    {
                        _layerMask.BackgroundImage.Dispose();
                        _layerMask.BackgroundImage = null;
                    }
                }

                // Material preview
                _editor.Graphics.SetActiveLayer(_layer.SelectedIndex);
                if (_terrain != null)
                {
                    var mat = _terrain.GetMaterial((uint)_layer.SelectedIndex);
                    if (mat != null)
                    {
                        try
                        {
                            _layerMaterial.BackgroundImage = Util.LoadBitmap(mat.GetPreviewFile());
                        }
                        catch
                        {
                            if (_layerMaterial.BackgroundImage != null)
                            {
                                _layerMaterial.BackgroundImage.Dispose();
                                _layerMaterial.BackgroundImage = null;
                            }
                        }
                    }
                    else
                    {
                        if (_layerMaterial.BackgroundImage != null)
                        {
                            _layerMaterial.BackgroundImage.Dispose();
                            _layerMaterial.BackgroundImage = null;
                        }
                    }
                }
            }
            else
            {
                // Clear the material preview
                if (_layerMaterial.BackgroundImage != null)
                {
                    _layerMaterial.BackgroundImage.Dispose();
                    _layerMaterial.BackgroundImage = null;
                }

                // Clear the mask preview
                if (_layerMask.BackgroundImage != null)
                {
                    _layerMask.BackgroundImage.Dispose();
                    _layerMask.BackgroundImage = null;
                }
            }
        }

        // Load a blendmap for the terrain
        private void OnLoadBlendmap(object sender, EventArgs e)
        {
            if (_terrain != null)
            {
                String file = Util.GetOpenFileName("Resources\\Textures\\Heightmaps", MaoliSharp.Terrain.GetFileFilter());
                if (file != null)
                    _terrain.ImportBlendmap(file);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua.Panels
{
    public partial class PropteryPanel : Form
    {

        // Renderer
        MaoliSharp.Renderer _graphics;

        // Parent window
        HonuaEditor _parent;
        
        // Ctor
        public PropteryPanel(HonuaEditor parent, MaoliSharp.Renderer g)
        {
            InitializeComponent();
            _parent = parent;
            _graphics = g;
        }

        // Modify the base color
        private void OnChangeBaseColor(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.AnyColor = true;
            cd.SolidColorOnly = true;
            if (cd.ShowDialog().Equals(DialogResult.OK))
            {
                _parent.SelectedMaterial.BaseColor = cd.Color;
                _baseColorBox.BackColor = cd.Color;
            }
        }

        // Modify the emissve color
        private void OnChangeEmissiveColor(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.AnyColor = true;
            cd.SolidColorOnly = true;
            if (cd.ShowDialog().Equals(DialogResult.OK))
            {
                _parent.SelectedMaterial.Emissive = cd.Color;
                _emissiveColorBox.BackColor = cd.Color;
            }
        }


        // Update the material roughness from the slider
        private void UpdateRoughness(object sender, EventArgs e)
        {
            _parent.SelectedMaterial.Roughness = (float)_roughnessBar.Value / 100.0f;
        }

        // Update the material metalic from the slider
        private void UpdateMetallic(object sender, EventArgs e)
        {
            _parent.SelectedMaterial.Metallic = (float)_metallicBar.Value / 100.0f;
        }

        // Update the material transparency from the slider
        private void UpdateTransparency(object sender, EventArgs e)
        {
            _parent.SelectedMaterial.Transparency = (float)_transparencyBar.Value / 100.0f;
        }

        private void OnSetAlphaTesting(object sender, EventArgs e)
        {
            _parent.SelectedMaterial.AlphaTesting = _alphaTesting.Checked;
        }

    }
}

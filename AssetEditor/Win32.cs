﻿using System;
using System.Security;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;

namespace MaoliSharp
{
    [SuppressUnmanagedCodeSecurity]
    public class Win32
    {
        // Win32 Message class
        [StructLayout(LayoutKind.Sequential)]
        public struct Message
        {
            public IntPtr handle;
            public uint msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }

        // Import the PeekMessage function
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("User32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool PeekMessage(out Message message, IntPtr handle, uint filterMin, uint filterMax, uint flags);

        // Translate message
        [DllImport("user32.dll")]
        public static extern bool TranslateMessage([In] ref Message message);

        // Dispatch message
        [DllImport("user32.dll")]
        public static extern IntPtr DispatchMessage([In] ref Message message);

        // SendMessage
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, IntPtr lParam);

        // Bring a window to the front
        [DllImport("User32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        // Message types
        public const int WM_SETREDRAW = 0x000B;
        public const int WM_USER = 0x400;
        public const int EM_GETEVENTMASK = (WM_USER + 59);
        public const int EM_SETEVENTMASK = (WM_USER + 69);
    }
}

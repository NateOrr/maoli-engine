﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Honua
{
    // Constructs materials from textures in a folder
    class MaterialBuilder
    {
        // Working lists of each texture type
        List<string> _diffuse = new List<string>();
        List<string> _normal = new List<string>();
        List<string> _roughness = new List<string>();
        List<string> _metallic = new List<string>();
        List<string> _alpha = new List<string>();
        List<string> _emissive = new List<string>();
        List<string> _height = new List<string>();
        Dictionary<string, bool> _processed = new Dictionary<string, bool>();

        // Temp data
        MaoliSharp.MaterialBuilder _material;

        // Renderer
        MaoliSharp.Renderer _graphics;

        // Current working directory
        string _currentDirectory;


        // Ctor
        public MaterialBuilder(MaoliSharp.Renderer graphics)
        {
            _graphics = graphics;
            _material = new MaoliSharp.MaterialBuilder(_graphics);
        }

        // Process a folder of textures
        public void BuildMaterialDatabase(string directory)
        {
            // Get a list of all folders, and all files
            var folders = Directory.GetDirectories(directory);
            var files = Directory.GetFiles(directory);

            // Construct the texture lists for this folder
            BuildTextureLists(files);

            // Process the immediate files first
            _currentDirectory = directory;
            _material.SetDirectory(_currentDirectory);
            _processed.Clear();
            foreach (string file in files)
            {
                BuildMaterialFromFile(file, files);
            }

            // Now process each folder recursively
            foreach (string folder in folders)
            {
                BuildMaterialDatabase(folder);
            }
        }


        // Build a material from a texture file
        void BuildMaterialFromFile(string textureFile, string[] files)
        {
            // Only process valid texture files
            if (!IsFileATexture(textureFile))
                return;

            // Get the name portion of the file path
            var fileName = Path.GetFileNameWithoutExtension(textureFile);

            // Get the specific name of this texture
            var name = GetTextureName(fileName);
            if (name == null || _processed.ContainsKey(name))
                return;

            // Create a new material
            _material.Create(name);

            // Get all the matching texture types in the folder
            _material.AddTexture(FindTexture(name, MaoliSharp.TextureType.BaseColor), MaoliSharp.TextureType.BaseColor);
            _material.AddTexture(FindTexture(name, MaoliSharp.TextureType.NormalMap), MaoliSharp.TextureType.NormalMap);
            _material.AddTexture(FindTexture(name, MaoliSharp.TextureType.Roughness), MaoliSharp.TextureType.Roughness);
            _material.AddTexture(FindTexture(name, MaoliSharp.TextureType.Metallic), MaoliSharp.TextureType.Metallic);
            _material.AddTexture(FindTexture(name, MaoliSharp.TextureType.Emissive), MaoliSharp.TextureType.Emissive);
            _material.AddTexture(FindTexture(name, MaoliSharp.TextureType.Alpha), MaoliSharp.TextureType.Alpha);

            // Generate normal maps from height maps
            string heightmap = FindHeightmap(name);
            string normalmap = _material.GetTexture(MaoliSharp.TextureType.NormalMap);
            if (heightmap != null)
            {
                // If no normal map exists, compute one from the bump map
                MaoliSharp.Texture finalMap = null;
                MaoliSharp.Texture bumpMap = _graphics.LoadTexture(_currentDirectory + "\\" + heightmap);
                if (normalmap == null)
                {
                    finalMap = _graphics.ComputeNormalMap(bumpMap);
                }

                // Otherwise, merge the heightmap into the normal map alpha
                else
                {
                    MaoliSharp.Texture normal = _graphics.LoadTexture(normalmap);
                    finalMap = _graphics.StoreHeightInNormalMap(normal, bumpMap);
                    normal.Dispose();
                }

                // Save it to file
                string finalFile = name + "_normal.dds";
                finalMap.Save(_currentDirectory + "\\" + finalFile);
                _material.AddTexture(finalFile, MaoliSharp.TextureType.NormalMap);
                finalMap.Dispose();
                bumpMap.Dispose();
            }

            // Export the material preview
            MaoliSharp.Material tempMat = _graphics.LoadMaterial(_currentDirectory + "\\" + name + ".mat");
            _graphics.MakeMaterialPreview(tempMat);
            _material.Export(_currentDirectory + "\\" + name + ".mat");
            _processed.Add(name, true);
        }

        // Get the specific name of a texture
        string GetTextureName(string textureFile)
        {
            int index = textureFile.LastIndexOf('_');
            if (index == -1)
                return null;
            return textureFile.Substring(0, index);
        }

        // Find the corresponding texture type for a given texture name
        string FindTexture(string name, MaoliSharp.TextureType type)
        {
            // Grab the correct list
            List<string> textureList = null;
            switch (type)
            {
                case MaoliSharp.TextureType.BaseColor:
                    textureList = _diffuse;
                    break;

                case MaoliSharp.TextureType.NormalMap:
                    textureList = _normal;
                    break;

                case MaoliSharp.TextureType.Roughness:
                    textureList = _roughness;
                    break;

                case MaoliSharp.TextureType.Metallic:
                    textureList = _metallic;
                    break;

                case MaoliSharp.TextureType.Emissive:
                    textureList = _emissive;
                    break;

                case MaoliSharp.TextureType.Alpha:
                    textureList = _alpha;
                    break;

                default:
                    return null;
            }

            // Find the corresponding texture
            foreach (string texFile in textureList)
            {
                if (GetTextureName(texFile) == name)
                    return texFile;
            }

            return null;
        }

        // Find a height map
        string FindHeightmap(string name)
        {
            // Find the corresponding texture
            foreach (string texFile in _height)
            {
                if (GetTextureName(texFile) == name)
                    return texFile;
            }
            return null;
        }

        // Construct the texture lists for this folder
        void BuildTextureLists(string[] files)
        {
            _diffuse.Clear();
            _normal.Clear();
            _roughness.Clear();
            _metallic.Clear();
            _alpha.Clear();
            _emissive.Clear();
            _height.Clear();
            foreach (string file in files)
            {
                var fileName = Path.GetFileName(file);
                switch (GetTextureType(fileName))
                {
                    case MaoliSharp.TextureType.BaseColor:
                        _diffuse.Add(fileName);
                        break;

                    case MaoliSharp.TextureType.NormalMap:
                        _normal.Add(fileName);
                        break;

                    case MaoliSharp.TextureType.Roughness:
                        _roughness.Add(fileName);
                        break;

                    case MaoliSharp.TextureType.Metallic:
                        _metallic.Add(fileName);
                        break;

                    case MaoliSharp.TextureType.Emissive:
                        _emissive.Add(fileName);
                        break;

                    case MaoliSharp.TextureType.Alpha:
                        _alpha.Add(fileName);
                        break;

                    default:
                        {
                            if (IsTextureHeight(fileName))
                                _height.Add(fileName);
                        }
                        break;
                }
            }
        }



        #region Validation

        // Validate that a file is a texture
        bool IsFileATexture(string textureFile)
        {
            // Make sure this is a supported extension
            // *.jpg;*.bmp;*.jpeg;*.dds;*.png;*.tif"
            string ext = Path.GetExtension(textureFile);
            return (ext == ".jpg" ||
                    ext == ".bmp" ||
                    ext == ".jpeg" ||
                    ext == ".dds" ||
                    ext == ".png" ||
                    ext == ".tif");
        }

        // Find the type of a texture
        MaoliSharp.TextureType GetTextureType(string textureFile)
        {
            // Diffuse
            if (IsTextureDiffuse(textureFile))
                return MaoliSharp.TextureType.BaseColor;

            // NormalMap
            if (IsTextureNormal(textureFile))
                return MaoliSharp.TextureType.NormalMap;

            // Roughness
            if (IsTextureRoughness(textureFile))
                return MaoliSharp.TextureType.Roughness;

            // Metallic
            if (IsTextureMetallic(textureFile))
                return MaoliSharp.TextureType.Metallic;

            // Emissive
            if (IsTextureEmissive(textureFile))
                return MaoliSharp.TextureType.Emissive;

            // Alpha
            if (IsTextureAlpha(textureFile))
                return MaoliSharp.TextureType.Alpha;

            return MaoliSharp.TextureType.None;
        }

        // Check if a texture is a color map
        bool IsTextureDiffuse(string textureFile)
        {
            return textureFile.IndexOf("_diffuse", System.StringComparison.OrdinalIgnoreCase) != -1;
        }

        // Check if a texture is a normal map
        bool IsTextureNormal(string textureFile)
        {
            return textureFile.IndexOf("_normal", System.StringComparison.OrdinalIgnoreCase) != -1;
        }

        // Check if a texture is a roughness map
        bool IsTextureRoughness(string textureFile)
        {
            return textureFile.IndexOf("_roughness", System.StringComparison.OrdinalIgnoreCase) != -1;
        }

        // Check if a texture is a metalic map
        bool IsTextureMetallic(string textureFile)
        {
            return textureFile.IndexOf("_metallic", System.StringComparison.OrdinalIgnoreCase) != -1;
        }

        // Check if a texture is an emissive map
        bool IsTextureEmissive(string textureFile)
        {
            return textureFile.IndexOf("_emissive", System.StringComparison.OrdinalIgnoreCase) != -1;
        }

        // Check if a texture is an alpha map
        bool IsTextureAlpha(string textureFile)
        {
            return textureFile.IndexOf("_alpha", System.StringComparison.OrdinalIgnoreCase) != -1;
        }

        // Check if a texture is a height map
        bool IsTextureHeight(string textureFile)
        {
            return textureFile.IndexOf("_disp", System.StringComparison.OrdinalIgnoreCase) != -1;
        }

        #endregion


    }
}

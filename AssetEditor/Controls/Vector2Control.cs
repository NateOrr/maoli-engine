﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class Vector2Control : HonuaControl
    {
        MaoliSharp.Vector2 _value = new MaoliSharp.Vector2();
        float _minimum = -100f;
        float _maximum = 100.0f;

        // Ctor
        public Vector2Control()
        {
            InitializeComponent();
            TextAlignment = 15;
        }

        // Set the property to auto update
        public override PropertyInfo Property
        {
            set
            {
                if (value.PropertyType == typeof(MaoliSharp.Vector2))
                {
                    _property = value;
                    if (_object != null)
                    {
                        Value = (MaoliSharp.Vector2)_property.GetValue(_object);
                        Text = _property.Name;
                    }
                    else
                        throw new Exception("SelectedObject must be set before Property");
                }
                else
                    throw new Exception("PropertyType must be a MaoliSharp.Vector2");
            }
        }

        // Get the value
        public MaoliSharp.Vector2 Value 
        { 
            get { return _value; }
            set
            {
                if (value.x > Maximum) value.x = Maximum;
                if (value.x < Minimum) value.x = Minimum;
                if (value.y > Maximum) value.y = Maximum;
                if (value.y < Minimum) value.y = Minimum;
                _x.Text = value.x.ToString("0.00");
                _y.Text = value.y.ToString("0.00");
                _value = value;
                if (_property != null)
                {
                    _property.SetValue(_object, _value);
                    OnValueChanged();
                }
            }
        }

        // Get/set the minimum allowed value
        public float Minimum 
        { 
            get{return _minimum;}
            set
            {
                _minimum = value;
                Value = _value;
            }
        }

        // Get/set the maximum allowed value
        public float Maximum
        {
            get { return _maximum; }
            set
            {
                _maximum = value;
                Value = _value;
            }
        }

        // Set the label text
        public override String Text 
        { 
            get { return _label.Text; } 
            set 
            { 
                _label.Text = value;

                // Proper offset
                using (var g = this.CreateGraphics())
                {
                    var size = g.MeasureString(_label.Text, _label.Font);
                    _label.Location = new Point(TextAlignment - (int)size.Width - 5, _label.Location.Y);
                    _x.Location = new Point(TextAlignment, _x.Location.Y);
                    _y.Location = new Point(_x.Location.X + _x.Width + 5, _x.Location.Y);
                    this.Size = new Size(_y.Location.X + _y.Width, this.Height);
                }
            } 
        }

        // Update playback speed when the control loses focus
        private void OnLoseFocus(object sender, EventArgs e)
        {
            try
            {
                var newValue = new MaoliSharp.Vector2();
                newValue.x = float.Parse(_x.Text);
                newValue.y = float.Parse(_y.Text);
                if ( (_value - newValue).Length() > 0.00001f )
                {
                    Honua.Command.PerformAction(_value, newValue, new Honua.Action(this.Redo),
                                       new Honua.Action(this.Undo));
                }
            }
            catch
            {
                MessageBox.Show("Please enter a valid floating point value");
            }
        }

        // Update the playback speed on enter
        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                _label.Focus();
                e.Handled = true;
            }
        }

        // Function for undo/redo
        void Undo(object sender, object param)
        {
            Value = (MaoliSharp.Vector2)sender;
        }
        void Redo(object sender, object param)
        {
            Value = (MaoliSharp.Vector2)param;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class TextureBox : HonuaControl
    {
        MaoliSharp.Texture _value;
        MaoliSharp.Texture _prevValue;

        // Game engine
        public MaoliSharp.Engine Engine { get; set; }

        // Ctor
        public TextureBox()
        {
            InitializeComponent();
            TextAlignment = 100;
        }

        // Set the property to auto update
        public override PropertyInfo Property
        {
            set
            {
                if (value.PropertyType == typeof(MaoliSharp.Texture))
                {
                    _property = value;
                    if (_object != null)
                        Value = (MaoliSharp.Texture)_property.GetValue(_object);
                    else
                        throw new Exception("SelectedObject must be set before Property");
                }
                else
                    throw new Exception("PropertyType must be a Texture");
            }
        }

        // Get the value
        public MaoliSharp.Texture Value 
        { 
            get { return _value; }
            set
            {
                if (_box.BackgroundImage != null)
                {
                    _box.BackgroundImage.Dispose();
                    _box.BackgroundImage = null;
                }
                _prevValue = _value;
                _value = value;
                if (_value != null)
                {
                    try
                    {
                        _box.BackgroundImage = Util.LoadBitmap(_value.FileName);
                    }
                    catch
                    {

                    }
                }
                if (_property != null)
                {
                    _property.SetValue(_object, _value);
                    OnValueChanged();
                }
            }
        }

        // Set the label text
        public override String Text 
        { 
            get { return _label.Text; } 
            set 
            { 
                _label.Text = value;

                // Proper offset
                using (var g = this.CreateGraphics())
                {
                    var size = g.MeasureString(_label.Text, _label.Font);
                    _label.Location = new Point(TextAlignment - (int)size.Width - 5, _label.Location.Y);
                    _box.Location = new Point(TextAlignment, _box.Location.Y);
                    this.Size = new Size(_box.Location.X + _box.Width, this.Height);
                }
            } 
        }

        // Function for undo/redo
        void Undo(object sender, object param)
        {
            Value = (MaoliSharp.Texture)sender;
        }
        void Redo(object sender, object param)
        {
            Value = (MaoliSharp.Texture)param;
        }

        private void OnClick(object sender, EventArgs e)
        {
            String fileName = Util.GetOpenFileName("Resources\\Textures", Util.TextureFilter);
            if (fileName != null)
            {
                var tex = Engine.Graphics.LoadTexture(fileName);
                if (tex != null && _prevValue != tex)
                {
                    Command.PerformAction(Value, tex, new Action(Redo), new Action(Undo) );
                }
            }
        }
    }
}

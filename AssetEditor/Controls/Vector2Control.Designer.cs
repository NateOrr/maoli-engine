﻿namespace Honua
{
    partial class Vector2Control
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._label = new System.Windows.Forms.Label();
            this._x = new System.Windows.Forms.TextBox();
            this._y = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _label
            // 
            this._label.AutoSize = true;
            this._label.Location = new System.Drawing.Point(5, 11);
            this._label.Name = "_label";
            this._label.Size = new System.Drawing.Size(44, 18);
            this._label.TabIndex = 131;
            this._label.Text = "Value";
            // 
            // _x
            // 
            this._x.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._x.Location = new System.Drawing.Point(80, 3);
            this._x.Name = "_x";
            this._x.Size = new System.Drawing.Size(52, 32);
            this._x.TabIndex = 130;
            this._x.Text = "1.0";
            this._x.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            this._x.Leave += new System.EventHandler(this.OnLoseFocus);
            // 
            // _y
            // 
            this._y.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._y.Location = new System.Drawing.Point(138, 3);
            this._y.Name = "_y";
            this._y.Size = new System.Drawing.Size(52, 32);
            this._y.TabIndex = 130;
            this._y.Text = "1.0";
            this._y.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            this._y.Leave += new System.EventHandler(this.OnLoseFocus);
            // 
            // Vector2Control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._label);
            this.Controls.Add(this._y);
            this.Controls.Add(this._x);
            this.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Vector2Control";
            this.Size = new System.Drawing.Size(209, 51);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _label;
        private System.Windows.Forms.TextBox _x;
        private System.Windows.Forms.TextBox _y;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using Honua;

namespace Honua
{
    public partial class MainWindow : Honua.UndoRedoForm
    {
        // Threading
        Thread _loadThread;
        Thread _exitThread;

        // Editor
        Honua.HonuaEditor _editor;

        #region Init

        // Ctor
        public MainWindow()
        {
            InitializeComponent();
            Command.SetWindow(this);
            this.WindowState = FormWindowState.Maximized;

            // Display a loading bar in another thread while the engine loads
            _loadThread = new Thread(new ThreadStart(Util.DisplayLoadingBar));
            _loadThread.Start();

            // Setup the editor
            _editor = new Honua.HonuaEditor();
            _editor.AddComponentPanel(typeof(MaoliSharp.Model), typeof(Honua.Panels.ModelPanel));
            _editor.AddComponentPanel(typeof(MaoliSharp.Animation), typeof(Honua.Panels.AnimationPanel));
            _editor.AddComponentPanel(typeof(MaoliSharp.ParticleEmitter), typeof(Honua.Panels.ParticlePanel));
            _editor.Init(this, _renderPanel, _splitContainer.Panel2);

            _terrainModeButton.Checked = _materialModeButton.Checked = _layerModeButton.Checked = _entityModeButton.Checked = false;
            _worldModeButton.Checked = true;

            // Setup GUI
            PropertyPage.Engine = _editor.Engine;
            PropertyPage.MakeControl = Util.MakePropertyControl;

            // Close the loading bar
            _loadThread.Abort();
            MaoliSharp.Win32.SetForegroundWindow(this.Handle);
        }


        // Shutdown
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            // Display a loading bar in another thread while the engine loads
            _exitThread = new Thread(new ThreadStart(Util.DisplayExitBar));
            _exitThread.Start();
            _editor.Shutdown();
            base.OnFormClosing(e);
        }

        // Final shutdown
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            _exitThread.Abort();
        }


        // Close the program
        private void OnQuit(object sender, EventArgs e)
        {
            this.Close();
        }



        #endregion



        // Save the material
        private void OnSave(object sender, EventArgs e)
        {
            String fileName;

            if (_editor.State == Honua.HonuaEditor.EditorState.Entity)
            {
                _editor.SaveEntity();
                return;
            }

            if (_editor.State == Honua.HonuaEditor.EditorState.World)
            {
                fileName = Util.GetSaveFileName("Resources\\Maps", "Maoli Map Files|*.map;");
                if (fileName != null)
                    _editor.SaveWorld(fileName);
                return;
            }

            String filter = (_editor.State == Honua.HonuaEditor.EditorState.Entity) ? "Entity Files|*.entity;" : "Material Files|*.mat;";
            String initialDirectory = (_editor.State == Honua.HonuaEditor.EditorState.Entity) ? "Resources\\Entities" : "Resources\\Materials";
            fileName = Util.GetSaveFileName(initialDirectory, filter);
            if (fileName != null)
                _editor.SaveMaterial(fileName);
        }

        // Load a material
        private void OnLoad(object sender, EventArgs e)
        {
            if (_editor.State == Honua.HonuaEditor.EditorState.Entity)
            {
                String fileName = Util.GetOpenFileName("Resources\\Entities", "Entity Files|*.entity;");
                if (fileName != null)
                {
                    if (!_editor.LoadEntity(fileName))
                        MessageBox.Show("Failed to load entity");
                }
            }
            else if (_editor.State == Honua.HonuaEditor.EditorState.World)
            {
                String fileName = Util.GetOpenFileName("Resources\\Maps", "Maoli Map Files|*.map;");
                if (fileName != null)
                {
                    if (!_editor.LoadWorld(fileName))
                        MessageBox.Show("Failed to load map");
                }
            }
            else
            {
                _editor.LoadMaterial();
            }
        }

        // Make a new material
        private void OnNew(object sender, EventArgs e)
        {
            _editor.Clear();
        }

        // Load a light probe
        private void OnLoadProbe(object sender, EventArgs e)
        {
            String fileName = Util.GetOpenFileName("Resources\\Textures\\Cubemaps", "Cubemap Textures|*.dds;");
            if (fileName != null)
            {
                _editor.Graphics.LoadSkybox(fileName);
            }
        }

        // Construct a material database from all textures contained in a folder
        private void OnBuildDatabase(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.RootFolder = Environment.SpecialFolder.Desktop;
            dlg.SelectedPath = "Resources\\Materials\\";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Thread thread = new Thread(Util.DisplayWorkingBar);
                thread.Start();
                _editor.BuildMaterialDatabase(dlg.SelectedPath);
                thread.Abort();
            }
        }

        // Build a normal map from a bump map
        private void OnComputeNormalMap(object sender, EventArgs e)
        {
            String fileName = Util.GetOpenFileName("Resources\\Textures", Util.TextureFilter);
            if (fileName != null)
            {
                _editor.ComputeNormalMap(fileName);
            }
        }

        // Set the editor mode
        private void OnSetEditorMode(object sender, EventArgs e)
        {
            _terrainModeButton.Checked = _materialModeButton.Checked = _layerModeButton.Checked = _entityModeButton.Checked = _worldModeButton.Checked = false;
            if (_materialModeButton == sender)
            {
                _editor.SetState(Honua.HonuaEditor.EditorState.Material);
                _materialModeButton.Checked = true;
            }
            else if (_layerModeButton == sender)
            {
                _editor.SetState(Honua.HonuaEditor.EditorState.Layer);
                _layerModeButton.Checked = true;
            }
            else if (_entityModeButton == sender)
            {
                _editor.SetState(Honua.HonuaEditor.EditorState.Entity);
                _entityModeButton.Checked = true;
            }
            else if (_worldModeButton == sender)
            {
                _editor.SetState(Honua.HonuaEditor.EditorState.World);
                _worldModeButton.Checked = true;
            }
            else if (_terrainModeButton == sender)
            {
                _editor.SetState(Honua.HonuaEditor.EditorState.Terrain);
                _terrainModeButton.Checked = true;
            }
        }

        // Put the color map as the alpha map
        private void OnColorToAlpha(object sender, EventArgs e)
        {
            _editor.MaterialColorToAlpha();
        }

        // Set the axis widget transform
        private void OnSetTransformAxis(object sender, EventArgs e)
        {
            _translateAxisButton.Checked = false;
            _rotateAxisButton.Checked = false;
            _scaleAxisButton.Checked = false;
            ToolStripButton button = sender as ToolStripButton;
            button.Checked = true;
            if (button == _translateAxisButton)
                _editor.AxisWidget.Mode = MaoliSharp.AxisMode.Translate;
            else if (button == _rotateAxisButton)
                _editor.AxisWidget.Mode = MaoliSharp.AxisMode.Rotate;
            else if (button == _scaleAxisButton)
                _editor.AxisWidget.Mode = MaoliSharp.AxisMode.Scale;
        }

        private void OnGridSnapKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                _renderPanel.Focus();
                SetGridSnap(sender, null);
                e.Handled = true;
            }
        }

        private void SetGridSnap(object sender, EventArgs e)
        {
            float f;
            if (float.TryParse(_gridSnap.Text, out f) && f >= 0)
                _editor.AxisWidget.GridSnap = f;
            else
            {
                MessageBox.Show("Please enter a valid positive floating point value");
            }
        }

        // Undo the last action
        private void OnUndo(object sender, EventArgs e)
        {
            Command.Undo();
        }

        // Redo the last action that was undone
        private void OnRedo(object sender, EventArgs e)
        {
            Command.Redo();
        }

        // Set the status of undo controls
        public override bool UndoAvailable { set { _undoButton.Enabled = _undoMenuItem.Enabled = value; } }

        // Set the status of redo controls
        public override bool RedoAvailable { set { _redoButton.Enabled = _redoMenuItem.Enabled = value; } }

        private void OnShowGrid(object sender, EventArgs e)
        {
            _editor.Graphics.DrawGrid = !_editor.Graphics.DrawGrid;
            _gridMenuItem.Checked = _editor.Graphics.DrawGrid;
        }

        private void OnPlayGame(object sender, EventArgs e)
        {
            _editor.PlayGame();
        }

    }
}

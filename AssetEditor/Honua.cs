﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Honua.Panels;
using System.Drawing;
using System.Reflection;
using System.IO;
using System.Threading;
using Honua;

namespace Honua
{
    public class HonuaEditor : Honua.BaseEditor
    {
        // Game engine
        MaoliSharp.Engine _engine = null;

        MaoliSharp.Renderer _graphics;              // Renderer
        MaoliSharp.FirstPersonCamera _camera;       // User controlled camera
        MaoliSharp.Entity _materialEntity;          // Entity used for materia editing
        MaoliSharp.Entity _cameraEntity;            // Entity used for materia editing
        MaoliSharp.Entity _lightEntity;             // Entity used for materia editing
        MaoliSharp.Model _materialMesh;             // Mesh used for material editing
        MaoliSharp.MessageFilter _messageFilter;    // Access to win32 messages
        Form _form;                                 // Rendering surface
        Panel _editorPanel;                         // The panel where all editor forms go
        ComponentPanel _currentComponentPanel;      // Selected component form
        AxisWidget _axisWidget;                     // Axis transform tool
        Point _mousePos;                            // Mouse position in the render panel
        Panel _renderPanel;                         // Panel where graphics is output
        MaoliSharp.World _world;                    // Game world

        // Base panels
        EntityPanel _entityPanel;
        WorldPanel _worldPanel;
        TransformPanel _transformPanel;
        PropteryPanel _propertyPanel;
        TexturePanel _texturePanel;
        LayerPanel _layerPanel;
        ObjectPanel _componentPanel;
        TerrainPanel _terrainPanel;
        OceanPanel _oceanPanel;

        // For caching camera/selected entity
        class CameraCache
        {
            public MaoliSharp.Vector3 CameraPosition;
            public MaoliSharp.Vector3 CameraRotation;
            public MaoliSharp.Vector3 EntityPosition;
            public MaoliSharp.Vector3 EntityRotation;
        }
        CameraCache _cache = new CameraCache();

        // Access the engine
        public MaoliSharp.Engine Engine { get { return _engine; } }

        // Access the camera
        public MaoliSharp.Camera Camera { get { return _camera; } }

        // Access the render panel
        public Panel RenderPanel { get { return _renderPanel; } }

        // Access the renderer
        public MaoliSharp.Renderer Graphics { get { return _graphics; } }

        // Access the entity
        public MaoliSharp.Entity Entity { get { return _entityPanel.SelectedEntity; } }

        // Mouse position inside the render panel
        public Point MousePosition { get { return _mousePos; } }

        // Misc
        bool _shuttingDown = false;

        // States
        public enum EditorState
        {
            Material,
            Layer,
            Entity,
            World,
            Terrain,
        }
        EditorState _state = EditorState.Material;

        // Images
        public Bitmap _noTexImage;

        // Get the state
        public EditorState State { get { return _state; } }

        // Map component type to their editor panel
        Dictionary<Type, ComponentPanel> _panelMap = new Dictionary<Type, ComponentPanel>();

        // File browser
        MaterialBrowser _browser;
        public MaterialBrowser Browser
        {
            get { return _browser; }
        }

        // Selected material
        public MaoliSharp.Material SelectedMaterial
        {
            get { return _layerPanel._material; }
        }

        // Axis widget
        public AxisWidget AxisWidget
        {
            get { return _axisWidget; }
        }


        #region Init

        // Initialize
        public bool Init(Form window, Panel renderPanel, Panel editorPanel)
        {
            // Setup the game engine
            _engine = new MaoliSharp.Engine();
            _engine.Init(window.Handle, renderPanel.Handle);
            _engine.SetState(MaoliSharp.EngineState.Editor);
            _graphics = _engine.Graphics;
            _graphics.EnableVSync(true);
            _graphics.EnableSky(true);
            //_graphics.LoadSkybox("Resources\\Textures\\Cubemaps\\OceanSkybox.dds");
            _graphics.SetTimeOfDay(4, 0, 0);
            _graphics.EnableSunShadows(false);
            _engine.EnablePhysicsVisualization(true);
            _form = window;
            _editorPanel = editorPanel;
            _renderPanel = renderPanel;
            _world = new MaoliSharp.World(_engine);

            // Add the camera
            _camera = _engine.CreateComponent<MaoliSharp.FirstPersonCamera>();
            _camera.Position = new MaoliSharp.Vector3(0, 0, -5);
            _cameraEntity = _engine.CreateEntity("Camera");
            _cameraEntity.AddComponent(_camera);
            _engine.AddEntity(_cameraEntity);

            // Load a sphere for previews
            _materialEntity = _engine.CreateEntity("MaterialDummy");
            _materialMesh = _graphics.LoadMesh("Resources\\Models\\sphere.mesh");
            var transform = _engine.CreateComponent<MaoliSharp.Transform>();
            _materialEntity.AddComponent(_materialMesh);
            _materialEntity.AddComponent(transform);

            // Setup the default lighting
            _lightEntity = _engine.CreateEntity("SceneLight");
            var light = _engine.CreateComponent<MaoliSharp.Light>();
            light.Position = new MaoliSharp.Vector3(0, 8, 0);
            light.Range = 14.0f;
            _lightEntity.AddComponent(light);
            _engine.AddEntity(_lightEntity);

            // Skybox
            _graphics.LoadSkybox("Resources\\Textures\\Cubemaps\\uffizi_cross.dds");

            // Load up the browser
            _browser = new MaterialBrowser(_graphics);

            // Hook the message filter, so the c++ side can process win32 messages
            _messageFilter = new MaoliSharp.MessageFilter(_engine);
            Application.AddMessageFilter(_messageFilter);

            // Custom panels
            foreach (var panel in _panelMap)
            {
                _editorPanel.Controls.Add(panel.Value.EditorBox);
            }

            // Editor panels
            _propertyPanel = new PropteryPanel(this, _graphics);
            _texturePanel = new TexturePanel(this, _graphics);
            _layerPanel = new LayerPanel(this, _graphics);
            _componentPanel = new ObjectPanel();
            _entityPanel = new EntityPanel(this);
            _worldPanel = new WorldPanel(this, _world);
            _transformPanel = new TransformPanel(this);
            _terrainPanel = new TerrainPanel(this);
            _oceanPanel = new OceanPanel(this);
            _editorPanel.Controls.Add(_texturePanel._groupBox);
            _editorPanel.Controls.Add(_propertyPanel._groupBox);
            _editorPanel.Controls.Add(_layerPanel._groupBox);
            _editorPanel.Controls.Add(_transformPanel.EditorBox);
            _editorPanel.Controls.Add(_componentPanel.EditorBox);
            _editorPanel.Controls.Add(_worldPanel.EntityPanel._groupBox);
            _editorPanel.Controls.Add(_entityPanel._groupBox);
            _editorPanel.Controls.Add(_worldPanel._groupBox);
            _editorPanel.Controls.Add(_terrainPanel.EditorBox);
            _editorPanel.Controls.Add(_oceanPanel.EditorBox);

            // Widgets
            _axisWidget = new AxisWidget(_engine, this, _transformPanel);

            // Load editor images
            _noTexImage = Util.LoadBitmap("Resources\\Textures\\Editor\\NoTex.jpg");

            // Setup the context menu for the texture boxes
            // Shortcut menus for loading/clearing textures
            ContextMenu menu = new ContextMenu();
            MenuItem loadTex = new MenuItem("Load", new EventHandler(this.LoadMaterialTexture));
            MenuItem clearTex = new MenuItem("Clear", new EventHandler(this.ClearMaterialTexture));
            loadTex.ShowShortcut = false;
            clearTex.ShowShortcut = false;
            menu.MenuItems.Add(loadTex);
            menu.MenuItems.Add(clearTex);
            _texturePanel._color.ContextMenu = menu;
            _texturePanel._normal.ContextMenu = menu;
            _texturePanel._roughness.ContextMenu = menu;
            _texturePanel._metallic.ContextMenu = menu;
            _texturePanel._alpha.ContextMenu = menu;
            _texturePanel._emissive.ContextMenu = menu;

            // Setup texture boxes
            _texturePanel._color.Tag = MaoliSharp.TextureType.BaseColor;
            _texturePanel._normal.Tag = MaoliSharp.TextureType.NormalMap;
            _texturePanel._roughness.Tag = MaoliSharp.TextureType.Roughness;
            _texturePanel._metallic.Tag = MaoliSharp.TextureType.Metallic;
            _texturePanel._alpha.Tag = MaoliSharp.TextureType.Alpha;
            _texturePanel._emissive.Tag = MaoliSharp.TextureType.Emissive;

            // Setup the default material
            ResetMaterial();
            _layerPanel._material.LoadTextureSet("Resources\\Textures\\Game Pack\\Textures\\tex_201.jpg");
            _layerPanel._material.Roughness = 0.5f;
            UpdateGUI();
            SetSelectedMaterial();

            // Set default state to material
            _propertyPanel._groupBox.Hide();
            _texturePanel._groupBox.Hide();
            _layerPanel._groupBox.Hide();
            _entityPanel._groupBox.Hide();
            _worldPanel._groupBox.Hide();
            _transformPanel.EditorBox.Hide();
            _componentPanel.EditorBox.Hide();
            _terrainPanel.EditorBox.Hide();
            _oceanPanel.EditorBox.Hide();
            SetState(EditorState.World);

            // Update callback
            Application.Idle += new EventHandler(Run);

            return true;
        }

        // Process waiting win32 messages
        bool AppStillIdle
        {
            get
            {
                MaoliSharp.Win32.Message msg;
                return !MaoliSharp.Win32.PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }

        // Main loop
        void Run(object sender, EventArgs e)
        {
            // Update and render the engine/editor
            while (AppStillIdle)
            {
                if (!_shuttingDown && !_browser.Visible && _form.ContainsFocus)
                {
                    // Get the mouse location
                    _mousePos = _renderPanel.PointToClient(Cursor.Position);

                    if (_state == EditorState.World)
                        _worldPanel.Render();
                    _axisWidget.Update();

                    if (_currentComponentPanel != null)
                        _currentComponentPanel.OnRender();

                    // Terrain
                    if (_state == EditorState.Terrain)
                        _terrainPanel.OnRender();

                    _engine.Update();
                    _engine.Render();
                }
            }
        }

        // Shutdown the editor
        public void Shutdown()
        {
            _shuttingDown = true;
            Application.RemoveMessageFilter(_messageFilter);
            _axisWidget.Release();
            _engine.ClearWorld();
            _world.DeleteEntities();
            _lightEntity.Dispose();
            _materialEntity.Dispose();
            _cameraEntity.Dispose();
            _terrainPanel.Release();
            if (_entityPanel.SelectedEntity != null)
                _entityPanel.SelectedEntity.Dispose();
            _worldPanel.Shutdown();
            Command.Clear();
            GC.Collect();
            _engine.Release();
        }

        #endregion


        #region Editor

        // Cache camera data on state change
        public void CacheCamera(bool zero)
        {
            _cache.CameraPosition = _camera.Position;
            _cache.CameraRotation = _camera.Rotation;

            if (zero)
            {
                _camera.Position = new MaoliSharp.Vector3(0, 0, -5);
                _camera.Rotation = new MaoliSharp.Vector3(0, 0, 0);
            }

            if (_entityPanel.SelectedEntity != null)
            {
                var transform = _entityPanel.SelectedEntity.GetComponent<MaoliSharp.Transform>();
                if (transform != null)
                {
                    _cache.EntityPosition = transform.Position;
                    _cache.EntityRotation = transform.Rotation;

                    if (zero)
                        transform.Position = transform.Rotation = new MaoliSharp.Vector3(0, 0, 0);
                }
            }
        }

        // Restore camera transform
        public void RestoreCamera()
        {
            _camera.Position = _cache.CameraPosition;
            _camera.Rotation = _cache.CameraRotation;
            if (_entityPanel.SelectedEntity != null)
            {
                var transform = _entityPanel.SelectedEntity.GetComponent<MaoliSharp.Transform>();
                if (transform != null)
                {
                    transform.Position = _cache.EntityPosition;
                    transform.Rotation = _cache.EntityRotation;
                }
            }
            _engine.AddEntity(_cameraEntity);
        }

        // New item
        public void Clear()
        {
            Command.Clear();
            if (_state == EditorState.World)
            {
                _worldPanel.Clear();
                _engine.AddEntity(_cameraEntity);
            }
            if (_state == EditorState.Entity)
                _entityPanel.OnNewEntity(null, null);
            else
                ResetMaterial();
        }


        // Register a component panel
        public void AddComponentPanel(Type componentType, Type panelType)
        {
            var panel = Activator.CreateInstance(panelType, new object[] { this }) as ComponentPanel;
            panel.Hide();
            _panelMap[componentType] = panel;
            panel.EditorBox.Hide();
        }

        // Select a component type
        public void SelectComponent(MaoliSharp.Component component)
        {
            if (_currentComponentPanel != null)
                _currentComponentPanel.EditorBox.Hide();

            // Clear panels
            if (component == null)
            {
                _currentComponentPanel = null;
            }

            // Apply the axis widget
            if (_state == EditorState.World)
            {
                _axisWidget.Clear();
                _transformPanel.SetComponent(null);
                if (component != null && component.GetType() == typeof(MaoliSharp.Transform))
                {
                    _transformPanel.SetComponent(component);
                    _axisWidget.SelectObject(component);
                }
            }
            else if (_state == EditorState.Entity)
            {
                _transformPanel.SetComponent(null);
                _axisWidget.Clear();
                if (component != null && component.GetType() != typeof(MaoliSharp.Transform))
                {
                    _axisWidget.SelectObject(component);
                    _transformPanel.SetComponent(component);
                }
            }

            // Show the appropriate panel
            if (component != null && _panelMap.ContainsKey(component.GetType()))
            {
                _currentComponentPanel = _panelMap[component.GetType()];
                _currentComponentPanel.SetComponent(component);
                _currentComponentPanel.EditorBox.BringToFront();
                _currentComponentPanel.EditorBox.Show();
            }
            else if (component != null && component.GetType() != typeof(MaoliSharp.Transform))
            {
                _currentComponentPanel = _componentPanel;
                _currentComponentPanel.SetComponent(component);
                _currentComponentPanel.EditorBox.BringToFront();
                _currentComponentPanel.EditorBox.Show();
            }
        }

        // Save the entity
        public void SaveEntity()
        {
            _entityPanel.SaveEntity();
        }

        // Load the entity
        public bool LoadEntity(String file)
        {
            return _entityPanel.LoadEntity(file);
        }

        // Save the game world
        public void SaveWorld(String file)
        {
            _world.Save(file);
        }

        // Load the game world
        public bool LoadWorld(String file)
        {
            Thread thread = new Thread(Util.DisplayLoadingBar);
            thread.Start();
            _worldPanel.Clear();
            if (_world.Load(file))
            {
                _engine.SetWorld(_world);
                // Always keep the camera
                _engine.AddEntity(_cameraEntity);
                _worldPanel.OnLoad();
                thread.Abort();
                return true;
            }
            thread.Abort();
            return false;
        }

        // Enable / disable the ocean panel
        public void EnableOceanPanel(bool value)
        {
            _oceanPanel.EditorBox.Visible = value;
            _graphics.EnableOcean(value);
            _oceanPanel.SetOcean(_graphics.GetOcean());
        }

        // Set the editor mode
        public void SetState(EditorState state)
        {
            if (state == _state)
                return;

            // Cleanup the previous state
            _engine.ClearWorld();
            _axisWidget.Clear();
            SelectComponent(null);
            switch (_state)
            {
                case EditorState.Material:
                    _texturePanel._groupBox.Hide();
                    _propertyPanel._groupBox.Hide();
                    break;

                case EditorState.Layer:
                    _layerPanel._groupBox.Hide();
                    break;

                case EditorState.Entity:
                    _entityPanel._groupBox.Hide();
                    break;

                case EditorState.World:
                    _worldPanel._groupBox.Hide();
                    _oceanPanel.EditorBox.Hide();
                    break;

                case EditorState.Terrain:
                    _terrainPanel.EditorBox.Hide();
                    break;
            }

            // Setup the new state
            _state = state;
            switch (_state)
            {
                case EditorState.Material:
                    _engine.Graphics.ShowTerrain(false);
                    _engine.AddEntity(_lightEntity);
                    _engine.AddEntity(_materialEntity);
                    _texturePanel._groupBox.Show();
                    _propertyPanel._groupBox.Show();
                    _layerPanel.BlendLayers();
                    _camera.Mode = MaoliSharp.CameraMode.ArcBall;
                    break;

                case EditorState.Layer:
                    _engine.Graphics.ShowTerrain(false);
                    _engine.AddEntity(_lightEntity);
                    _engine.AddEntity(_materialEntity);
                    _layerPanel._groupBox.Show();
                    _camera.Mode = MaoliSharp.CameraMode.ArcBall;
                    break;

                case EditorState.Entity:
                    _engine.Graphics.ShowTerrain(false);
                    _engine.AddEntity(_lightEntity);
                    if (_entityPanel.SelectedEntity != null)
                        _engine.AddEntity(_entityPanel.SelectedEntity);
                    _entityPanel._groupBox.Show();
                    _camera.Mode = MaoliSharp.CameraMode.ArcBall;
                    break;

                case EditorState.World:
                    _engine.Graphics.ShowTerrain(true);
                    _engine.SetWorld(_world);
                    _worldPanel.UpdateEntityList();
                    _worldPanel._groupBox.Show();
                    _camera.Mode = MaoliSharp.CameraMode.FirstPerson;
                    break;

                case EditorState.Terrain:
                    _engine.Graphics.ShowTerrain(true);
                    _engine.SetWorld(_world);
                    _terrainPanel.SetTerrain(_engine.Graphics.GetTerrain());
                    _terrainPanel.EditorBox.Show();
                    _camera.Mode = MaoliSharp.CameraMode.FirstPerson;
                    break;
            }

            // Always keep the camera
            _engine.AddEntity(_cameraEntity);
        }

        #endregion


        #region Material

        // Get a material from the browser
        public MaoliSharp.Material ChooseMaterial()
        {
            if (_browser.OpenBrowser())
            {
                return _browser.Material;
            }
            return null;
        }

        // Load the material
        public void LoadMaterial()
        {
            if (_browser.OpenBrowser())
            {
                _layerPanel._layers.Items.Clear();
                _layerPanel._layers.Items.Add(_browser.Material);
                _layerPanel._material = _browser.Material;
                SetSelectedMaterial();
            }
        }

        // Load a material
        public void LoadMaterial(string file)
        {
            _layerPanel._material.Import(file);
            SetSelectedMaterial();
        }

        // Save the material
        public void SaveMaterial(String file)
        {
            _layerPanel._material.Export(file);
            _graphics.MakeMaterialPreview(_layerPanel._material);
            _browser.AddMaterial(file);
        }

        // Set the selected material
        public void SetSelectedMaterial()
        {
            foreach (MaoliSharp.SubMesh subMesh in _materialMesh.SubMeshes)
                subMesh.Material = _layerPanel._material;
            UpdateGUI();
        }

        // Set the material
        public void SetMaterial(MaoliSharp.Material mat)
        {
            _layerPanel._material = mat;
            SetSelectedMaterial();
        }

        // Update the gui
        void UpdateGUI()
        {
            // Name
            _propertyPanel._nameBox.Text = _layerPanel._material.Name;

            // Props
            _propertyPanel._roughnessBar.Value = (int)(_layerPanel._material.Roughness * 100.0f);
            _propertyPanel._metallicBar.Value = (int)(_layerPanel._material.Metallic * 100.0f);
            _propertyPanel._alphaTesting.Checked = _layerPanel._material.AlphaTesting;


            // Colors
            _propertyPanel._baseColorBox.BackColor = _layerPanel._material.BaseColor;
            _propertyPanel._emissiveColorBox.BackColor = _layerPanel._material.Emissive;


            // Textures
            {
                // Base color
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.BaseColor);
                    _texturePanel._color.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._color.BackgroundImage = _noTexImage;
                }


                // Normal
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.NormalMap);
                    _texturePanel._normal.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._normal.BackgroundImage = _noTexImage;
                }

                // Roughness
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Roughness);
                    _texturePanel._roughness.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._roughness.BackgroundImage = _noTexImage;
                }

                // Metallic
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Metallic);
                    _texturePanel._metallic.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._metallic.BackgroundImage = _noTexImage;
                }

                // Alpha
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Alpha);
                    _texturePanel._alpha.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._alpha.BackgroundImage = _noTexImage;
                }

                // Emissive
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Emissive);
                    _texturePanel._emissive.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._emissive.BackgroundImage = _noTexImage;
                }
            }

        }

        // Reset the material to default
        void ResetMaterial()
        {
            // Delete all materials
            for (int i = 1; i < _graphics.GetNumMaterials(); ++i)
                _graphics.RemoveMaterial(_graphics.GetMaterial(i));

            // Setup the default material
            _layerPanel._material = _graphics.CreateMaterial("NewMaterial");
            _layerPanel._material.Roughness = 0.5f;
            _layerPanel._material.Metallic = 0.0f;
            _layerPanel._material.BaseColor = Color.FromArgb(255, 255, 255, 255);
            _layerPanel._material.Emissive = Color.FromArgb(0, 0, 0, 0);
            _layerPanel._material.Transparency = 0.0f;
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.BaseColor);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.NormalMap);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Roughness);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Metallic);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Alpha);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Emissive);
            UpdateGUI();

            // Add the default material to the layers
            _layerPanel._layers.Items.Clear();
            _layerPanel._layers.Items.Add(_layerPanel._material);
            _layerPanel._layers.SelectedIndex = 0;

            // Create the base material
            _layerPanel._finalMaterial = _graphics.CreateMaterial("FinalMaterial");
            _layerPanel._blendStageMaterial = _graphics.CreateMaterial("StagingMaterial");
        }

        // Get the selected material
        public MaoliSharp.Material GetSelectedLayer()
        {
            return _layerPanel._material;
        }

        // Load a texture set
        public void LoadMaterialTextureSet()
        {
            String fileName = Util.GetOpenFileName("Resources\\Textures\\Game Pack\\Textures", Util.TextureFilter);
            if (fileName != null)
            {
                if (_layerPanel._material.LoadTextureSet(fileName) == false)
                    MessageBox.Show("Texture Failed To Load");
                else
                {
                    UpdateGUI();
                }
            }
        }

        // Load a texture
        void LoadMaterialTexture(object sender, EventArgs e)
        {
            // Get the texture box that called this command
            MenuItem item = sender as MenuItem;
            Panel textureBox = item.GetContextMenu().SourceControl as Panel;

            // Load the texture
            String fileName = Util.GetOpenFileName("Resources\\Textures", Util.TextureFilter);
            if (fileName != null)
            {
                if (_layerPanel._material.LoadTexture(fileName, (MaoliSharp.TextureType)textureBox.Tag) == false)
                    MessageBox.Show("Texture Failed To Load");
                else
                {
                    UpdateGUI();
                }
            }
        }

        // Load the current material texture
        public void LoadMaterialTexture(string fileName, MaoliSharp.TextureType type)
        {
            if (_layerPanel._material.LoadTexture(fileName, type) == false)
                MessageBox.Show("Texture Failed To Load");
            else
            {
                UpdateGUI();
            }
        }

        // Clear a texture
        void ClearMaterialTexture(object sender, EventArgs e)
        {
            // Get the texture box that called this command
            MenuItem item = sender as MenuItem;
            Panel textureBox = item.GetContextMenu().SourceControl as Panel;

            _layerPanel._material.ClearTexture((MaoliSharp.TextureType)textureBox.Tag);
            textureBox.BackgroundImage = _noTexImage;
        }

        // Build the material database
        public void BuildMaterialDatabase(String directory)
        {
            MaterialBuilder builder = new MaterialBuilder(_graphics);
            builder.BuildMaterialDatabase(directory);
            _browser = new MaterialBrowser(_graphics);
        }

        // Build a normal map from a bump map
        public void ComputeNormalMap(String file)
        {
            MaoliSharp.Texture bumpMap = _graphics.LoadTexture(file);
            MaoliSharp.Texture normalMap = _graphics.ComputeNormalMap(bumpMap);
            normalMap.Save("c:\\users\\nathaniel\\desktop\\normalmap.dds");
            SelectedMaterial.SetTexture(normalMap, MaoliSharp.TextureType.NormalMap);
        }

        // Set material color texture to alpha channel
        public void MaterialColorToAlpha()
        {
            if (_layerPanel._material != null)
                _layerPanel._material.SetTexture(_layerPanel._material.GetTexture(MaoliSharp.TextureType.BaseColor), MaoliSharp.TextureType.Alpha);
        }

        // Enter the game mode
        public void PlayGame()
        {
            _worldPanel.SetState(WorldPanel.State.Demo);
        }

        #endregion

    }
}

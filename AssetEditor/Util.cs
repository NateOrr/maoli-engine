﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.Threading;

namespace Honua
{
    // Utility functions
    public class Util
    {
        // File filters
        public static readonly string TextureFilter = "Image Files|*.jpg;*.bmp;*.jpeg;*.dds;*.png;*.tif;*.tga";
        public static readonly string MeshFilter = "3D Mesh Files|*.pbm;*.x;*.3ds;*.obj;*.lwo;*.lxo;*.dxf;*.ac;*.ms3d;*.ase;*.ply;*.smd;*.vta;*.dae;*.cob;*.scn;*.mdl;*.md2;*.md3;*.mdc;*.md5;*.bhv;*.csm;*.b3d;*.q3d;*.q3s;*.mesh;*.irrmesh;*.irr;*.nff;*.off;*.raw;*.ter;*.hmp;*.bsp";
        public static readonly string AnimationFilter = "Animation Files|*.hkx;";

        // Dialogs
        static OpenFileDialog _openFileDlg = new OpenFileDialog();
        static SaveFileDialog _saveFileDlg = new SaveFileDialog();

        // Get a file name from an open file dialog
        public static String GetOpenFileName(String initialDir, String filter)
        {
            _openFileDlg.CheckFileExists = true;
            _openFileDlg.AddExtension = true;
            _openFileDlg.CheckPathExists = true;
            _openFileDlg.RestoreDirectory = true;
            _openFileDlg.AutoUpgradeEnabled = true;
            _openFileDlg.Filter = filter;
            String currentDir = Directory.GetCurrentDirectory();
            _openFileDlg.InitialDirectory = Path.Combine(currentDir, initialDir);
            if (_openFileDlg.ShowDialog().Equals(DialogResult.OK))
                return _openFileDlg.FileName.Replace(currentDir + "\\", "");
            return null;
        }

        // Get a file name from a save file dialog
        public static String GetSaveFileName(String initialDir, String filter)
        {
            _saveFileDlg.CheckFileExists = false;
            _saveFileDlg.AddExtension = true;
            _saveFileDlg.CheckPathExists = false;
            _saveFileDlg.RestoreDirectory = true;
            _saveFileDlg.AutoUpgradeEnabled = true;
            _saveFileDlg.Filter = filter;
            String currentDir = Directory.GetCurrentDirectory();
            _saveFileDlg.InitialDirectory = Path.Combine(currentDir, initialDir);
            if (_saveFileDlg.ShowDialog().Equals(DialogResult.OK))
                return _saveFileDlg.FileName.Replace(currentDir + "\\", "");
            return null;
        }

        // Convert a struct to a byte array for file io
        public static byte[] StructToBytes<T>(T str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        // Convert a byte array to struct
        public static T BytesToStruct<T>(byte[] arr) where T : new()
        {
            T str = new T();
            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(arr, 0, ptr, size);
            str = (T)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);
            return str;
        }

        // Write a string to binary file
        public static void WriteBinaryString(BinaryWriter writer, string str)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            writer.Write(str.Length);
            writer.Write(encoding.GetBytes(str));
            writer.Write((byte)0);
        }

        // Write a string to binary file
        public static string ReadBinaryString(BinaryReader reader)
        {
            int length = reader.ReadInt32() + 1;
            byte[] buffer = new byte[length];
            reader.Read(buffer, 0, length);
            return System.Text.Encoding.Default.GetString(buffer, 0, length - 1);
        }

        // Load a bitmap from file, preserving file access
        public static Bitmap LoadBitmap(String file)
        {
            using (var bmpTemp = new Bitmap(file))
            {
                return new Bitmap(bmpTemp);
            }
        }

        // Fill a combo box with entities
        public static void FillComboBox(ComboBox box, MaoliSharp.Engine engine)
        {
            box.Items.Clear();
            box.Items.Add("None");
            foreach (var entity in engine.Entities)
                box.Items.Add(entity);
            box.SelectedIndex = 0;
        }

        // Fill a combo box with Comonents
        public static void FillComboBox<T>(ComboBox box, MaoliSharp.Entity entity) where T : MaoliSharp.Component
        {
            box.Items.Clear();
            box.Items.Add("None");
            foreach (var component in entity.Components)
            {
                if (component.GetType().Equals(typeof(T)))
                    box.Items.Add(component);
            }
            box.SelectedIndex = 0;
        }

        // Fill a combo box with Comonents
        public static void FillComboBox<T>(ComboBox box, MaoliSharp.Engine engine) where T : MaoliSharp.Component
        {
            box.Items.Clear();
            box.Items.Add("None");
            foreach (var entity in engine.Entities)
            {
                foreach (var component in entity.Components)
                {
                    if (component.GetType().Equals(typeof(T)))
                        box.Items.Add(entity);
                }
            }
            box.SelectedIndex = 0;
        }

        // Fill a combo box with Comonents
        public static void FillComboBox(ComboBox box, MaoliSharp.Engine engine, Type componentType)
        {
            box.Items.Clear();
            box.Items.Add("None");
            foreach (var entity in engine.Entities)
            {
                foreach (var component in entity.Components)
                {
                    if (component.GetType().Equals(componentType))
                        box.Items.Add(entity);
                }
            }
            box.SelectedIndex = 0;
        }

        // Convert degrees to radians
        public static float DegreeToRadian(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;
            return (float)Math.PI * angle / 180.0f;
        }

        // Convert radians to degrees
        public static float RadianToDegree(float angle)
        {
            float twopi = (float)Math.PI * 2.0f;
            while (angle > twopi)
                angle -= twopi;
            while (angle < 0)
                angle += twopi;
            return angle * (180.0f / (float)Math.PI);
        }

        // Clamp a value to a range
        public static void Clamp(ref float range, float min, float max)
        {
            if (range < min) range = min;
            if (range > max) range = max;
        }

        // Check if the mouse is inside a control
        public static bool MouseInControl(Control control)
        {
            return control.ClientRectangle.Contains(control.PointToClient(Control.MousePosition));
        }

        // Show a loading bar
        public static void DisplayLoadingBar()
        {
            LoadingBar loadBar = new LoadingBar("Loading");
            loadBar.Show();
            while (true)
            {
                loadBar.DoProgress();
                loadBar.Update();
                Thread.Sleep(34);
            }
        }

        // Show an exit bar
        public static void DisplayExitBar()
        {
            LoadingBar loadBar = new LoadingBar("Shutdown");
            loadBar.Show();
            while (true)
            {
                loadBar.DoProgress();
                loadBar.Update();
                Thread.Sleep(34);
            }
        }

        // Show an exit bar
        public static void DisplayWorkingBar()
        {
            LoadingBar loadBar = new LoadingBar("Working");
            loadBar.Show();
            while (true)
            {
                loadBar.DoProgress();
                loadBar.Update();
                Thread.Sleep(34);
            }
        }

        // Fill the property page
        public static HonuaControl MakePropertyControl(PropertyInfo prop, object obj)
        {
            // Texture box
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaTexture)) as HonuaTexture;
                if (attr != null)
                {
                    var control = new TextureBox();
                    control.Engine = PropertyPage.Engine as MaoliSharp.Engine;
                    return control;
                }
            }

            // Vector
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaVector)) as HonuaVector;
                if (attr != null)
                {
                    if (prop.PropertyType == typeof(MaoliSharp.Vector3))
                    {

                        var control = new VectorControl();
                        control.Minimum = attr.Minimum;
                        control.Maximum = attr.Maximum;
                        return control;
                    }
                    else if (prop.PropertyType == typeof(MaoliSharp.Vector2))
                    {

                        var control = new Vector2Control();
                        control.Minimum = attr.Minimum;
                        control.Maximum = attr.Maximum;
                        return control;
                    }
                }
            }

            // Color box
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaColor)) as HonuaColor;
                if (attr != null)
                {
                    var control = new ColorBox();
                    return control;
                }
            }

            // Float controls
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaFloat)) as HonuaFloat;
                if (attr != null)
                {
                    // Boxes
                    if (attr.Type == ValueControls.Box)
                    {
                        var control = new FloatBox();
                        control.Minimum = attr.Minimum;
                        control.Maximum = attr.Maximum;
                        return control;
                    }

                    // Sliders
                    if (attr.Type == ValueControls.Slider)
                    {
                        var control = new FloatBar();
                        control.Minimum = attr.Minimum;
                        control.Maximum = attr.Maximum;
                        return control;
                    }
                }
            }


            // Int controls
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaInt)) as HonuaInt;
                if (attr != null)
                {
                    // Boxes
                    if (attr.Type == ValueControls.Box)
                    {
                        var control = new IntBox();
                        control.Minimum = attr.Minimum;
                        control.Maximum = attr.Maximum;
                        return control;
                    }

                    // Sliders
                    if (attr.Type == ValueControls.Slider)
                    {
                        var control = new IntBar();
                        control.Minimum = attr.Minimum;
                        control.Maximum = attr.Maximum;
                        return control;
                    }
                }
            }

            // Check box
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaBool)) as HonuaBool;
                if (attr != null)
                {
                    var control = new BoolBox();
                    return control;
                }
            }

            // Selection box
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaSelectionBox)) as HonuaSelectionBox;
                if (attr != null)
                {
                    var control = new SelectionBox();

                    // Fill based on the type
                    if (attr.Target == SelectionBoxTarget.Enum)
                    {
                        control.SetEnum(prop.PropertyType);
                    }
                    else if (attr.Target == SelectionBoxTarget.Entity)
                    {
                        Util.FillComboBox(control.ComboBox, PropertyPage.Engine as MaoliSharp.Engine);
                    }
                    else if (attr.Target == SelectionBoxTarget.Component)
                    {
                        Util.FillComboBox(control.ComboBox, PropertyPage.Engine as MaoliSharp.Engine, attr.ItemType);
                    }

                    control.NullIndex = attr.NullIndex;
                    return control;
                }
            }


            // Text controls
            {
                var attr = prop.GetCustomAttribute(typeof(HonuaText)) as HonuaText;
                if (attr != null)
                {
                    var control = new StringBox();
                    return control;
                }
            }

            return null;
        }
    }
}

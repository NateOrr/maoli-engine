﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows.Forms;
using Honua.Panels;

namespace Honua
{
    // Tri axis widget for translation, scaling, and rotation
    public class AxisWidget
    {
        // 0=x, 1=y, 2=z
        MaoliSharp.Model[] _translationAxis = new MaoliSharp.Model[3];          // Models for the translation axes
        MaoliSharp.Model[] _rotationAxis = new MaoliSharp.Model[3];             // Models for the rotation axes
        MaoliSharp.Model[] _scaleAxis = new MaoliSharp.Model[3];                // Models for the scaling axes
        MaoliSharp.Vector3[] _axisColors = new MaoliSharp.Vector3[3];           // Axis colors
        MaoliSharp.Vector3[] _axisVectors = new MaoliSharp.Vector3[3];          // Axis delta directions
        MaoliSharp.Vector3[] _halfTestVectors = new MaoliSharp.Vector3[3];      // Vectors used for a half space test to determine delta direction
        MaoliSharp.Model[] _currentTransform;                                   // Current axis set
        MaoliSharp.Camera _camera;                                              // Camera from the engine
        MaoliSharp.Vector3 _gridDelta = new MaoliSharp.Vector3();               // Distance traveled along the grid (For snap)
        float _gridSnap = 0;                                                    // Grid snap value
        int _selectedAxis = -1;                                                 // Axis that is selected by the user
        TransformPanel _transformPanel;                                         // Editor panel that maps to this object
        object _object;                                                         // The primary selected object
        MaoliSharp.Vector3 _delta;                                              // Net change since the mouse was clicked
        Dictionary<object, PropertyInfo> _currentProperties;                    // Property set for the current mode

        // Selected entities
        Dictionary<object, PropertyInfo> _positionProperties = new Dictionary<object, PropertyInfo>();
        Dictionary<object, PropertyInfo> _rotationProperties = new Dictionary<object, PropertyInfo>();
        Dictionary<object, PropertyInfo> _scaleProperties = new Dictionary<object, PropertyInfo>();
        Dictionary<object, PropertyInfo> _uniformScaleProperties = new Dictionary<object, PropertyInfo>();

        // Game engine
        MaoliSharp.Engine _engine;
        MaoliSharp.Renderer _graphics;
        HonuaEditor _editor;

        MaoliSharp.AxisMode _mode;

        // States
        enum State
        {
            Selecting,
            Applying,
        }
        State _state = State.Selecting;

        // Ctor
        public AxisWidget(MaoliSharp.Engine engine, HonuaEditor editor, TransformPanel panel)
        {
            _engine = engine;
            _graphics = engine.Graphics;
            _currentTransform = _translationAxis;
            _editor = editor;
            _camera = _graphics.GetCamera();
            _transformPanel = panel;

            // Load the axis models
            for (int i = 0; i < 3; ++i)
            {
                _translationAxis[i] = _graphics.LoadMesh("Resources\\Models\\Editor\\Axis Cone.mesh");
                _scaleAxis[i] = _graphics.LoadMesh("Resources\\Models\\Editor\\Axis Pyre.mesh");
                _rotationAxis[i] = _graphics.LoadMesh("Resources\\Models\\Editor\\Axis Ball.mesh");
            }

            // Colors
            _axisColors[0] = new MaoliSharp.Vector3(1, 0, 0);
            _axisColors[1] = new MaoliSharp.Vector3(0, 0, 1);
            _axisColors[2] = new MaoliSharp.Vector3(0, 1, 0);

            // Directions
            _axisVectors[0] = new MaoliSharp.Vector3(1, 0, 0);
            _axisVectors[1] = new MaoliSharp.Vector3(0, 1, 0);
            _axisVectors[2] = new MaoliSharp.Vector3(0, 0, 1);
            _halfTestVectors[0] = new MaoliSharp.Vector3(0, 0, -1);
            _halfTestVectors[1] = new MaoliSharp.Vector3(0, 0, 0);
            _halfTestVectors[2] = new MaoliSharp.Vector3(1, 0, 0);

            // X-axis
            _translationAxis[0].Rotation = new MaoliSharp.Vector3(0, MaoliSharp.Math.PI * 0.5f, 0);
            _scaleAxis[0].Rotation = new MaoliSharp.Vector3(0, MaoliSharp.Math.PI * 0.5f, 0);
            _rotationAxis[0].Rotation = new MaoliSharp.Vector3(0, MaoliSharp.Math.PI * 0.5f, 0);

            // Y-axis
            _translationAxis[1].Rotation = new MaoliSharp.Vector3(-MaoliSharp.Math.PI * 0.5f, 0, 0);
            _scaleAxis[1].Rotation = new MaoliSharp.Vector3(-MaoliSharp.Math.PI * 0.5f, 0, 0);
            _rotationAxis[1].Rotation = new MaoliSharp.Vector3(-MaoliSharp.Math.PI * 0.5f, 0, 0);

            // Z-axis
            _translationAxis[2].Rotation = new MaoliSharp.Vector3(0, MaoliSharp.Math.PI, 0);
            _scaleAxis[2].Rotation = new MaoliSharp.Vector3(0, MaoliSharp.Math.PI, 0);
            _rotationAxis[2].Rotation = new MaoliSharp.Vector3(0, MaoliSharp.Math.PI, 0);

            // Properties
            GridSnap = 0;
            Mode = MaoliSharp.AxisMode.Translate;
            Visible = false;
        }

        // Cleanup
        public void Release()
        {
            for (int i = 0; i < 3; ++i)
            {
                _engine.Graphics.DeleteMesh(_translationAxis[i]);
                _engine.Graphics.DeleteMesh(_rotationAxis[i]);
                _engine.Graphics.DeleteMesh(_scaleAxis[i]);
            }
        }

        // Check if an axis is highlighted by the mouse
        public bool IsSelected { get { return _selectedAxis != -1; } }

        // The current axis mode
        public MaoliSharp.AxisMode Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                switch (_mode)
                {
                    case MaoliSharp.AxisMode.Translate:
                        _currentTransform = _translationAxis;
                        _currentProperties = _positionProperties;
                        break;

                    case MaoliSharp.AxisMode.Rotate:
                        _currentTransform = _rotationAxis;
                        _currentProperties = _rotationProperties;
                        break;

                    case MaoliSharp.AxisMode.Scale:
                        _currentProperties = _scaleProperties;
                        _currentTransform = _scaleAxis;
                        break;

                    case MaoliSharp.AxisMode.UniformScale:
                        _currentProperties = _uniformScaleProperties;
                        _currentTransform = _scaleAxis;
                        break;
                }
            }
        }

        // Grid snap size
        public float GridSnap
        {
            get { return _gridSnap; }
            set
            {
                if (value >= 0)
                {
                    _gridSnap = value;
                    if (value > 0)
                    {
                        // Update the selected objects
                        foreach (var obj in _positionProperties)
                        {
                            MaoliSharp.Vector3 position = (MaoliSharp.Vector3)obj.Value.GetValue(obj.Key);
                            float dx = position.x - (float)Math.Round(position.x / _gridSnap) * _gridSnap;
                            float dy = position.y - (float)Math.Round(position.y / _gridSnap) * _gridSnap;
                            float dz = position.z - (float)Math.Round(position.z / _gridSnap) * _gridSnap;
                            position.x -= dx;
                            position.y -= dy;
                            position.z -= dz;
                            obj.Value.SetValue(obj.Key, position);
                        }
                        _transformPanel.UpdateControls();
                    }
                }
            }
        }

        // Visibility
        public bool Visible { get; set; }

        // Select an object
        public void SelectObject(object obj)
        {
            // Reset properties
            Visible = false;
            if (obj == null)
                return;

            // Validate the object
            var type = obj.GetType();
            var props = type.GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(MaoliSharp.AxisWidget))).ToList();
            if (props.Count == 0)
                return;

            // Grab the properies with the widget attribute
            Visible = true;
            if (_object == null)
                _object = obj;
            foreach (var prop in props)
            {
                var attr = prop.GetCustomAttribute(typeof(MaoliSharp.AxisWidget)) as MaoliSharp.AxisWidget;
                switch (attr.Axis)
                {
                    case MaoliSharp.AxisMode.Translate:
                        if (prop.PropertyType == typeof(MaoliSharp.Vector3))
                        {
                            _positionProperties.Add(obj, prop);
                        }
                        else
                            MessageBox.Show("Axis Widget Translate property must be of type MaoliSharp.Vector3");
                        break;

                    case MaoliSharp.AxisMode.Rotate:
                        if (prop.PropertyType == typeof(MaoliSharp.Vector3))
                            _rotationProperties.Add(obj, prop);
                        else
                            MessageBox.Show("Axis Widget Rotate property must be of type MaoliSharp.Vector3");
                        break;

                    case MaoliSharp.AxisMode.Scale:
                        if (prop.PropertyType == typeof(MaoliSharp.Vector3))
                            _scaleProperties.Add(obj, prop);
                        else
                            MessageBox.Show("Axis Widget Scale property must be of type MaoliSharp.Vector3");
                        break;

                    case MaoliSharp.AxisMode.UniformScale:
                        if (prop.PropertyType == typeof(float))
                            _uniformScaleProperties.Add(obj, prop);
                        else
                            MessageBox.Show("Axis Widget UniformScale property must be of type float");
                        break;
                }
            }
        }

        // Remove an object
        public void RemoveObject(object obj)
        {
            _positionProperties.Remove(obj);
            _rotationProperties.Remove(obj);
            _scaleProperties.Remove(obj);
            _uniformScaleProperties.Remove(obj);
            if (_object == obj)
            {
                if (_positionProperties.Count == 0)
                    _object = null;
                else
                    _object = _positionProperties.First().Key;
            }
            Visible = _positionProperties.Count > 0;
        }

        // Remove all object
        public void Clear()
        {
            _positionProperties.Clear();
            _rotationProperties.Clear();
            _scaleProperties.Clear();
            _uniformScaleProperties.Clear();
            Visible = false;
            _object = null;
        }

        // Handle translations
        void TranslationUpdate(float strength)
        {
            // Apply more translation based on distance
            MaoliSharp.Vector3 axisPosition = (MaoliSharp.Vector3)_positionProperties[_object].GetValue(_object);
            float distFactor = ((_camera.Position - axisPosition).Length() + 10) / 12.0f;
            strength *= distFactor;

            // Handle grid snap
            if (GridSnap > 0)
            {
                MaoliSharp.Vector3 gridMove = new MaoliSharp.Vector3();
                _gridDelta += _axisVectors[_selectedAxis] * strength;
                if (Math.Abs(_gridDelta.x) >= GridSnap)
                {
                    gridMove.x += GridSnap * Math.Sign(_gridDelta.x);
                    _gridDelta.x -= GridSnap * Math.Sign(_gridDelta.x);
                }
                if (Math.Abs(_gridDelta.y) >= GridSnap)
                {
                    gridMove.y += GridSnap * Math.Sign(_gridDelta.y);
                    _gridDelta.y -= GridSnap * Math.Sign(_gridDelta.y);
                }
                if (Math.Abs(_gridDelta.z) >= GridSnap)
                {
                    gridMove.z += GridSnap * Math.Sign(_gridDelta.z);
                    _gridDelta.z -= GridSnap * Math.Sign(_gridDelta.z);
                }

                _delta += gridMove;
                foreach (var obj in _positionProperties)
                {
                    MaoliSharp.Vector3 position = (MaoliSharp.Vector3)obj.Value.GetValue(obj.Key);
                    position += gridMove;
                    obj.Value.SetValue(obj.Key, position);
                }
            }
            else
            {
                _delta += _axisVectors[_selectedAxis] * strength;
                foreach (var obj in _positionProperties)
                {
                    MaoliSharp.Vector3 position = (MaoliSharp.Vector3)obj.Value.GetValue(obj.Key);
                    position += _axisVectors[_selectedAxis] * strength;
                    obj.Value.SetValue(obj.Key, position);
                }
            }

            _transformPanel.UpdateControls();
        }

        // Handle rotations
        void RotationUpdate(float strength)
        {
            _delta += _axisVectors[_selectedAxis] * strength;
            foreach (var obj in _rotationProperties)
            {
                MaoliSharp.Vector3 rot = (MaoliSharp.Vector3)obj.Value.GetValue(obj.Key);
                rot += _axisVectors[_selectedAxis] * strength;
                obj.Value.SetValue(obj.Key, rot);
            }
            _transformPanel.UpdateControls();
        }

        // Handle scaling
        void ScaleUpdate(float strength)
        {
            const float minScale = 0.01f;
            _delta += _axisVectors[_selectedAxis] * strength;
            foreach (var obj in _scaleProperties)
            {
                MaoliSharp.Vector3 scale = (MaoliSharp.Vector3)obj.Value.GetValue(obj.Key);
                scale += _axisVectors[_selectedAxis] * strength;
                scale.x = Math.Max(scale.x, minScale);
                scale.y = Math.Max(scale.y, minScale);
                scale.z = Math.Max(scale.z, minScale);
                obj.Value.SetValue(obj.Key, scale);
            }
            _transformPanel.UpdateControls();
        }

        // Handle uniform scaling
        void UniformScaleUpdate(float strength)
        {
            const float minScale = 0.01f;
            foreach (var obj in _uniformScaleProperties)
            {
                float scale = (float)obj.Value.GetValue(obj.Key);
                scale += strength;
                _delta.x += strength;
                scale = Math.Max(scale, minScale);
                obj.Value.SetValue(obj.Key, scale);
            }
            _transformPanel.UpdateControls();
        }

        // Undo an operation
        void Undo(object sender, object param)
        {
            Dictionary<object, PropertyInfo> properties = param as Dictionary<object, PropertyInfo>;

            // Vector transforms
            const float minScale = 0.01f;
            var attr = (MaoliSharp.AxisWidget)properties.First().Value.GetCustomAttribute(typeof(MaoliSharp.AxisWidget));
            bool isScale = (attr.Axis == MaoliSharp.AxisMode.Scale);
            if (properties.First().Value.PropertyType == typeof(MaoliSharp.Vector3))
            {
                MaoliSharp.Vector3 delta = (MaoliSharp.Vector3)sender;
                foreach (var pair in properties)
                {
                    MaoliSharp.Vector3 pos = (MaoliSharp.Vector3)pair.Value.GetValue(pair.Key);
                    pos -= delta;

                    // Handle scale
                    if (isScale)
                    {
                        pos.x = Math.Max(pos.x, minScale);
                        pos.y = Math.Max(pos.y, minScale);
                        pos.z = Math.Max(pos.z, minScale);
                    }

                    pair.Value.SetValue(pair.Key, pos);
                }
            }
            else
            {
                // Uniform scale
                float delta = (float)sender;
                foreach (var pair in properties)
                {
                    float pos = (float)pair.Value.GetValue(pair.Key);
                    pos -= delta;
                    pos = Math.Max(pos, minScale);
                    pair.Value.SetValue(pair.Key, pos);
                }
            }
        }
        void Redo(object sender, object param)
        {
            Dictionary<object, PropertyInfo> properties = param as Dictionary<object, PropertyInfo>;

            // Vector transforms
            const float minScale = 0.01f;
            var attr = (MaoliSharp.AxisWidget)properties.First().Value.GetCustomAttribute(typeof(MaoliSharp.AxisWidget));
            bool isScale = (attr.Axis == MaoliSharp.AxisMode.Scale);
            if (properties.First().Value.PropertyType == typeof(MaoliSharp.Vector3))
            {
                MaoliSharp.Vector3 delta = (MaoliSharp.Vector3)sender;
                foreach (var pair in properties)
                {
                    MaoliSharp.Vector3 pos = (MaoliSharp.Vector3)pair.Value.GetValue(pair.Key);
                    pos += delta;

                    // Handle scale
                    if (isScale)
                    {
                        pos.x = Math.Max(pos.x, minScale);
                        pos.y = Math.Max(pos.y, minScale);
                        pos.z = Math.Max(pos.z, minScale);
                    }

                    pair.Value.SetValue(pair.Key, pos);
                }
            }
            else
            {
                // Uniform scale
                float delta = (float)sender;
                foreach (var pair in properties)
                {
                    float pos = (float)pair.Value.GetValue(pair.Key);
                    pos += delta;
                    pos = Math.Max(pos, minScale);
                    pair.Value.SetValue(pair.Key, pos);
                }
            }
        }

        // Commit an operation
        void Apply()
        {
            if (_delta.Length() > 0.0001f)
            {
                object deltaParam = (Mode == MaoliSharp.AxisMode.UniformScale) ? (object)_delta.x : (object)_delta;
                this.Undo(deltaParam, _currentProperties);
                var props = new Dictionary<object, PropertyInfo>(_currentProperties);
                Command.PerformAction(deltaParam, props, new Action(this.Redo),
                    new Action(this.Undo));
            }
        }

        // Render and update the current axes
        public void Update()
        {
            if (!Visible || _currentProperties.Count == 0)
                return;

            // Test which axis is highlighted
            if (_state == State.Selecting && Util.MouseInControl(_editor.RenderPanel))
            {
                _delta = new MaoliSharp.Vector3(0, 0, 0);
                MaoliSharp.Vector3 rayPos;
                MaoliSharp.Vector3 rayDir;
                _graphics.GetPickRay(out rayPos, out rayDir);
                _selectedAxis = -1;
                for (int i = 0; i < 3; ++i)
                {
                    _currentTransform[i].Update();
                    if (_currentTransform[i].RayIntersect(rayPos, rayDir, false))
                    {
                        _selectedAxis = i;
                        break;
                    }
                }
            }

            // Handle axis movement
            if (_engine.Platform.MouseLeftDown() && _selectedAxis != -1)
            {
                if (_state != State.Applying)
                    _gridDelta.x = _gridDelta.y = _gridDelta.z = 0;
                _state = State.Applying;

                // Move the widget
                float dir = _camera.Direction.Dot(_halfTestVectors[_selectedAxis]) > 0 ? -1 : 1;
                float strength = dir * 0.01f * ((_selectedAxis == 1) ? -_engine.Platform.MouseDeltaY() : _engine.Platform.MouseDeltaX());

                // Update based on transform type
                switch (Mode)
                {
                    case MaoliSharp.AxisMode.Translate:
                        TranslationUpdate(strength);
                        break;

                    case MaoliSharp.AxisMode.Rotate:
                        RotationUpdate(strength);
                        break;

                    case MaoliSharp.AxisMode.Scale:
                        ScaleUpdate(strength);
                        break;

                    case MaoliSharp.AxisMode.UniformScale:
                        UniformScaleUpdate(strength);
                        break;
                }
            }

            // Revert state
            if (_state == State.Applying && !_engine.Platform.MouseLeftDown())
            {
                _state = State.Selecting;
                Apply();
            }

            // Render the axes
            if (_object != null)
            {
                MaoliSharp.Vector3 position = (MaoliSharp.Vector3)_positionProperties[_object].GetValue(_object);
                float distFactor = ((_camera.Position - position).Length() + 10) / 12.0f;
                var scale = new MaoliSharp.Vector3(distFactor, distFactor, distFactor);
                for (int i = 0; i < 3; ++i)
                {
                    // Scale based on distance
                    MaoliSharp.Vector3 color = (_selectedAxis == i || (_selectedAxis != -1 && Mode == MaoliSharp.AxisMode.UniformScale)) ? new MaoliSharp.Vector3(1, 1, 0) : _axisColors[i];
                    _graphics.DebugRenderMesh(_currentTransform[i], color, position, _currentTransform[i].Rotation, scale, true);
                    _graphics.DebugRenderLine(position, _currentTransform[i].Rotation, scale, color, true);
                }
            }
        }
    }
}

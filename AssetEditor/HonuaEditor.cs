﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Honua.Panels;
using System.Drawing;
using System.Reflection;
using BaseGUI;

namespace Honua
{
    public class HonuaEditor : BaseGUI.BaseEditor
    {
        // Game engine
        MaoliSharp.Engine _engine = null;

        MaoliSharp.Renderer _graphics;              // Renderer
        MaoliSharp.Camera _camera;                  // User controlled camera
        MaoliSharp.Entity _materialEntity;          // Entity used for materia editing
        MaoliSharp.Entity _lightEntity;             // Light for the entity/material editor
        MaoliSharp.Model _materialMesh;             // Mesh used for material editing
        MaoliSharp.MessageFilter _messageFilter;    // Access to win32 messages
        Form _form;                                 // Rendering surface
        Panel _editorPanel;                         // The panel where all editor forms go
        ComponentPanel _componentPanel;             // Selected component form
        AxisWidget _axisWidget;                     // Axis transform tool
        Point _mousePos;                            // Mouse position in the render panel
        Panel _renderPanel;                         // Panel where graphics is output
        MaoliSharp.World _world;                    // Primary game world

        // Cache camera and selected entity when changing editor modes
        class ModeCache
        {
            public MaoliSharp.Vector3 EntityPosition;
            public MaoliSharp.Vector3 EntityRotation;
            public MaoliSharp.Vector3 CameraPosition;
            public MaoliSharp.Vector3 CameraRotation;
        }
        ModeCache _cache = new ModeCache();

        // Base panels
        EntityPanel _entityPanel;
        PropteryPanel _propertyPanel;
        TexturePanel _texturePanel;
        LayerPanel _layerPanel;
        WorldPanel _worldPanel;
        TransformPanel _transformPanel;

        // Access the engine
        public MaoliSharp.Engine Engine { get { return _engine; } }

        // Access the renderer
        public MaoliSharp.Renderer Graphics { get { return _graphics; } }

        // Access the entity
        public MaoliSharp.Entity Entity { get { return _entityPanel.SelectedEntity; } }

        // Get the transform panel
        public TransformPanel TransformPanel { get { return _transformPanel; } }

        // Get the render panel
        public Panel RenderPanel { get { return _renderPanel; } }


        // Mouse position inside the render panel
        public Point MousePosition { get { return _mousePos; } }

        // Misc
        bool _shuttingDown = false;

        // States
        public enum EditorState
        {
            Material,
            Layer,
            Entity,
            World,
        }
        EditorState _state = EditorState.Material;

        // Images
        public Bitmap _noTexImage;

        // Get the state
        public EditorState State { get { return _state; } }

        // Map component type to their editor panel
        Dictionary<Type, ComponentPanel> _panelMap = new Dictionary<Type, ComponentPanel>();

        // File browser
        MaterialBrowser _browser;
        public MaterialBrowser Browser
        {
            get { return _browser; }
        }

        // Selected material
        public MaoliSharp.Material SelectedMaterial
        {
            get { return _layerPanel._material; }
        }

        // Axis widget
        public AxisWidget AxisWidget
        {
            get { return _axisWidget; }
        }


        #region Init

        // Initialize
        public bool Init(Form window, Panel renderPanel, Panel editorPanel)
        {
            // Setup the game engine
            _engine = new MaoliSharp.Engine();
            _engine.Init(window.Handle, renderPanel.Handle);
            _engine.EnablePhysics(false);
            _graphics = _engine.Graphics;
            _form = window;
            _editorPanel = editorPanel;
            _renderPanel = renderPanel;

            // Setup the game world
            _world = new MaoliSharp.World(_engine);

            // Add the camera
            _camera = _engine.CreateComponent<MaoliSharp.FirstPersonCamera>();
            _camera.Position = new MaoliSharp.Vector3(0, 0, -5);
            MaoliSharp.Entity camEntity = _engine.CreateEntity("Camera");
            camEntity.AddComponent(_camera);
            _engine.AddEntity(camEntity);
            _camera.Type = MaoliSharp.CameraType.FreeRoam;

            // Load a sphere for previews
            _materialEntity = _engine.CreateEntity("MaterialDummy");
            _materialMesh = _graphics.LoadMesh("Resources\\Models\\sphere.mesh");
            var transform = _engine.CreateComponent<MaoliSharp.Transform>();
            _materialEntity.AddComponent(_materialMesh);
            _materialEntity.AddComponent(transform);
            _engine.AddEntity(_materialEntity);

            // Setup the default lighting
            _lightEntity = _engine.CreateEntity("SceneLight");
            var light = _engine.CreateComponent<MaoliSharp.Light>();
            light.Position = new MaoliSharp.Vector3(0, 8, 0);
            light.Range = 14.0f;
            _lightEntity.AddComponent(light);
            _engine.AddEntity(_lightEntity);

            // Skybox
            _graphics.LoadSkybox("Resources\\Textures\\Cubemaps\\uffizi_cross.dds");

            // Load up the browser
            _browser = new MaterialBrowser(_graphics);

            // Hook the message filter, so the c++ side can process win32 messages
            _messageFilter = new MaoliSharp.MessageFilter(_engine);
            Application.AddMessageFilter(_messageFilter);

            // Custom panels
            foreach (var panel in _panelMap)
            {
                _editorPanel.Controls.Add(panel.Value.EditorBox);
            }

            // Editor panels
            _propertyPanel = new PropteryPanel(this, _graphics);
            _texturePanel = new TexturePanel(this, _graphics);
            _layerPanel = new LayerPanel(this, _graphics);
            _entityPanel = new EntityPanel(this);
            _worldPanel = new WorldPanel(this, _world);
            _transformPanel = new TransformPanel(this);
            _editorPanel.Controls.Add(_texturePanel._groupBox);
            _editorPanel.Controls.Add(_propertyPanel._groupBox);
            _editorPanel.Controls.Add(_layerPanel._groupBox);
            _editorPanel.Controls.Add(_transformPanel.EditorBox);
            _editorPanel.Controls.Add(_entityPanel._groupBox);
            _editorPanel.Controls.Add(_worldPanel._groupBox);

            // Widgets
            _axisWidget = new AxisWidget(_engine, this, _transformPanel);

            // Load editor images
            _noTexImage = Util.LoadBitmap("Resources\\Textures\\Editor\\NoTex.jpg");

            // Setup the context menu for the texture boxes
            // Shortcut menus for loading/clearing textures
            ContextMenu menu = new ContextMenu();
            MenuItem loadTex = new MenuItem("Load", new EventHandler(this.LoadMaterialTexture));
            MenuItem clearTex = new MenuItem("Clear", new EventHandler(this.ClearMaterialTexture));
            loadTex.ShowShortcut = false;
            clearTex.ShowShortcut = false;
            menu.MenuItems.Add(loadTex);
            menu.MenuItems.Add(clearTex);
            _texturePanel._baseTextureBox.ContextMenu = menu;
            _texturePanel._normalTextureBox.ContextMenu = menu;
            _texturePanel._roughnessTextureBox.ContextMenu = menu;
            _texturePanel._metallicTextureBox.ContextMenu = menu;
            _texturePanel._alphaTextureBox.ContextMenu = menu;
            _texturePanel._emissiveTextureBox.ContextMenu = menu;

            // Setup texture boxes
            _texturePanel._baseTextureBox.Tag = MaoliSharp.TextureType.BaseColor;
            _texturePanel._normalTextureBox.Tag = MaoliSharp.TextureType.NormalMap;
            _texturePanel._roughnessTextureBox.Tag = MaoliSharp.TextureType.Roughness;
            _texturePanel._metallicTextureBox.Tag = MaoliSharp.TextureType.Metallic;
            _texturePanel._alphaTextureBox.Tag = MaoliSharp.TextureType.Alpha;
            _texturePanel._emissiveTextureBox.Tag = MaoliSharp.TextureType.Emissive;

            // Setup the default material
             ResetMaterial();
             _layerPanel._material.LoadTextureSet("Resources\\Textures\\Game Pack\\Textures\\tex_201.jpg");
             _layerPanel._material.Roughness = 0.5f;
             UpdateGUI();
             SetSelectedMaterial();
 
             // Create the default entity
             _entityPanel.OnNewEntity(null, null);
 
            // Set default state to material
            _propertyPanel._groupBox.Hide();
            _texturePanel._groupBox.Hide();
            _layerPanel._groupBox.Hide();
            _transformPanel.EditorBox.Hide();
            _entityPanel._groupBox.Hide();
            _worldPanel._groupBox.Hide();
             SetState(EditorState.World);

            // Update callback
            Application.Idle += new EventHandler(Run);

            return true;
        }

        // Process waiting win32 messages
        bool AppStillIdle
        {
            get
            {
                MaoliSharp.Win32.Message msg;
                return !MaoliSharp.Win32.PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }

        // Main loop
        void Run(object sender, EventArgs e)
        {
            // Update and render the engine/editor
            while (AppStillIdle)
            {
                if (!_shuttingDown && !_browser.Visible && _form.ContainsFocus)
                {
                    // Get the mouse location
                    _mousePos = _renderPanel.PointToClient(Cursor.Position);

                    if (_state == EditorState.World)
                        _worldPanel.Render();
                    _axisWidget.Update();
                    

                    _engine.Update();

                    _engine.Render();
                }
            }
        }

        // Shutdown the editor
        public void Shutdown()
        {
            _shuttingDown = true;
            Application.RemoveMessageFilter(_messageFilter);
            _axisWidget.Release();
            _engine.ClearWorld();
            _entityPanel.SelectedEntity = null;
            _engine.DeleteEntity(_lightEntity);
            _engine.DeleteEntity(_materialEntity);
            _world.DeleteEntities();
            _engine.Release();
        }

        #endregion


        #region Editor

        // New item
        public void Clear()
        {
            if (_state == EditorState.Entity)
                _entityPanel.OnNewEntity(null, null);
            else if (_state == EditorState.World)
                _worldPanel.Clear();
            else
                ResetMaterial();
        }

        // Cache camera and selected entity transforms
        public void CacheCamera(bool reset)
        {
            _cache.CameraPosition = _camera.Position;
            _cache.CameraRotation = _camera.Rotation;
            if (reset)
            {
                _camera.Position = new MaoliSharp.Vector3(0, 0, -5);
                _camera.Rotation = new MaoliSharp.Vector3(0, 0, 0);
            }
            if(_entityPanel.SelectedEntity != null)
            {
                var transform = _entityPanel.SelectedEntity.GetComponent<MaoliSharp.Transform>();
                if (transform != null)
                {
                    _cache.EntityPosition = transform.Position;
                    _cache.EntityRotation = transform.Rotation;
                    transform.Position = new MaoliSharp.Vector3(0, 0, 0);
                    transform.Rotation = new MaoliSharp.Vector3(0, 0, 0);
                }
                else
                {
                    _cache.EntityPosition = new MaoliSharp.Vector3(0, 0, 0);
                    _cache.EntityRotation = new MaoliSharp.Vector3(0, 0, 0);
                }
            }
        }

        // Restore the camera and entity transforms
        public void RestoreCamera()
        {
            _camera.Position = _cache.CameraPosition;
            _camera.Rotation = _cache.CameraRotation;
            if(_entityPanel.SelectedEntity != null)
            {
                var transform = _entityPanel.SelectedEntity.GetComponent<MaoliSharp.Transform>();
                if (transform != null)
                {
                    transform.Position = _cache.EntityPosition;
                    transform.Rotation = _cache.EntityRotation;
                }
            }
        }


        // Register a component panel
        public void AddComponentPanel(Type componentType, Type panelType)
        {
            var panel = Activator.CreateInstance(panelType, new object[] { this }) as ComponentPanel;
            panel.Hide();
            _panelMap[componentType] = panel;
            panel.EditorBox.Hide();
        }

        // Select a component type
        public void SelectComponent(MaoliSharp.Component component)
        {
            if (_componentPanel != null)
                _componentPanel.EditorBox.Hide();

            // Apply the axis widget
            if (_state == EditorState.World)
            {
                //_axisWidget.SelectedObject = component;
                _transformPanel.SetComponent(component);
            }
            else
            {
                _axisWidget.Clear();
                if (component != null && component.GetType() != typeof(MaoliSharp.Transform))
                    _axisWidget.SelectObject(component);
                _transformPanel.SetComponent(null);
            }

            // Show the appropriate panel
            if (component != null && _panelMap.ContainsKey(component.GetType()))
            {
                _componentPanel = _panelMap[component.GetType()];
                _componentPanel.SetComponent(component);
                _componentPanel.EditorBox.Show();
            }
            else
                _componentPanel = null;
        }

        // Select the entity to work with
        public void SelectEntity(MaoliSharp.Entity entity)
        {
            if (entity != null)
                _entityPanel.SelectedEntity = entity;
            _entityPanel._groupBox.Visible = (entity != null);
        }

        // Save the entity
        public void SaveEntity()
        {
            _entityPanel.SaveEntity();
        }

        // Load the entity
        public bool LoadEntity(String file)
        {
            return _entityPanel.LoadEntity(file);
        }

        // Load the game world
        public bool LoadWorld(String file)
        {
            _worldPanel.Clear();
            if (_world.Load(file))
            {
                _engine.SetWorld(_world);
                _worldPanel.OnLoad();
                return true;
            }
            return false;
        }

        // Save the game world
        public void SaveWorld(String file)
        {
            _world.Save(file);
        }

        // Set the editor mode
        public void SetState(EditorState state)
        {
            if (state == _state)
                return;

            // Cleanup the previous state
            _engine.ClearWorld();
            switch (_state)
            {
                case EditorState.Material:
                    RestoreCamera();
                    _texturePanel._groupBox.Hide();
                    _propertyPanel._groupBox.Hide();
                    break;

                case EditorState.Layer:
                    RestoreCamera();
                    _layerPanel._groupBox.Hide();
                    break;

                case EditorState.Entity:
                    RestoreCamera();
                    _entityPanel._groupBox.Hide();
                    break;

                case EditorState.World:
                    _worldPanel.SelectedEntity = null;
                    _worldPanel._groupBox.Hide();
                    break;
            }

            // Setup the new state
            _state = state;
            switch (_state)
            {
                case EditorState.Material:
                    CacheCamera(true);
                    _engine.AddEntity(_materialEntity);
                    _texturePanel._groupBox.Show();
                    _propertyPanel._groupBox.Show();
                    _layerPanel.BlendLayers();
                    break;

                case EditorState.Layer:
                    CacheCamera(true);
                    _engine.AddEntity(_materialEntity);
                    _layerPanel._groupBox.Show();
                    break;

                case EditorState.Entity:
                    CacheCamera(true);
                    _engine.AddEntity(_entityPanel.SelectedEntity);
                    _engine.AddEntity(_lightEntity);
                    _entityPanel._groupBox.Show();
                    break;

                case EditorState.World:
                    _engine.SetWorld(_world);
                    _worldPanel.SelectedEntity = _entityPanel.SelectedEntity;
                    _worldPanel.UpdateEntityList();
                    _worldPanel._groupBox.Show();
                    break;
            }

            // Update component panels
            if (_state != EditorState.World)
                SelectComponent(_entityPanel.SelectedComponent);
        }

        #endregion


        #region Material

        // Get a material from the browser
        public MaoliSharp.Material ChooseMaterial()
        {
            if (_browser.OpenBrowser())
            {
                return _browser.Material;
            }
            return null;
        }

        // Load the material
        public void LoadMaterial()
        {
            if (_browser.OpenBrowser())
            {
                _layerPanel._layers.Items.Clear();
                _layerPanel._layers.Items.Add(_browser.Material);
                _layerPanel._material = _browser.Material;
                SetSelectedMaterial();
            }
        }

        // Load a material
        public void LoadMaterial(string file)
        {
            _layerPanel._material.Import(file);
            SetSelectedMaterial();
        }

        // Save the material
        public void SaveMaterial(String file)
        {
            _layerPanel._material.Export(file);
            _graphics.MakeMaterialPreview(_layerPanel._material);
        }

        // Set the selected material
        public void SetSelectedMaterial()
        {
            foreach (MaoliSharp.SubMesh subMesh in _materialMesh.SubMeshes)
                subMesh.Material = _layerPanel._material;
            UpdateGUI();
        }

        // Set the material
        public void SetMaterial(MaoliSharp.Material mat)
        {
            _layerPanel._material = mat;
            SetSelectedMaterial();
        }

        // Update the gui
        void UpdateGUI()
        {
            // Name
            _propertyPanel._nameBox.Text = _layerPanel._material.Name;

            // Props
            _propertyPanel._roughnessBar.Value = (int)(_layerPanel._material.Roughness * 100.0f);
            _propertyPanel._metallicBar.Value = (int)(_layerPanel._material.Metallic * 100.0f);

            // Colors
            _propertyPanel._baseColorBox.BackColor = _layerPanel._material.BaseColor;
            _propertyPanel._emissiveColorBox.BackColor = _layerPanel._material.Emissive;


            // Textures
            {
                // Base color
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.BaseColor);
                    _texturePanel._baseTextureBox.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._baseTextureBox.BackgroundImage = _noTexImage;
                }


                // Normal
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.NormalMap);
                    _texturePanel._normalTextureBox.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._normalTextureBox.BackgroundImage = _noTexImage;
                }

                // Roughness
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Roughness);
                    _texturePanel._roughnessTextureBox.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._roughnessTextureBox.BackgroundImage = _noTexImage;
                }

                // Metallic
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Metallic);
                    _texturePanel._metallicTextureBox.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._metallicTextureBox.BackgroundImage = _noTexImage;
                }

                // Alpha
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Alpha);
                    _texturePanel._alphaTextureBox.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._alphaTextureBox.BackgroundImage = _noTexImage;
                }

                // Emissive
                try
                {
                    string texName = _layerPanel._material.GetTextureName(MaoliSharp.TextureType.Emissive);
                    _texturePanel._emissiveTextureBox.BackgroundImage = (texName != null && !texName.Equals("NewResource")) ? Util.LoadBitmap(texName) : _noTexImage;
                }
                catch
                {
                    _texturePanel._emissiveTextureBox.BackgroundImage = _noTexImage;
                }
            }

        }

        // Reset the material to default
        void ResetMaterial()
        {
            // Delete all materials
            for (int i = 1; i < _graphics.GetNumMaterials(); ++i)
                _graphics.RemoveMaterial(_graphics.GetMaterial(i));

            // Setup the default material
            _layerPanel._material = _graphics.CreateMaterial("NewMaterial");
            _layerPanel._material.Roughness = 0.5f;
            _layerPanel._material.Metallic = 0.0f;
            _layerPanel._material.BaseColor = Color.FromArgb(255, 255, 255, 255);
            _layerPanel._material.Emissive = Color.FromArgb(0, 0, 0, 0);
            _layerPanel._material.Transparency = 0.0f;
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.BaseColor);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.NormalMap);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Roughness);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Metallic);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Alpha);
            _layerPanel._material.ClearTexture(MaoliSharp.TextureType.Emissive);
            UpdateGUI();

            // Add the default material to the layers
            _layerPanel._layers.Items.Clear();
            _layerPanel._layers.Items.Add(_layerPanel._material);
            _layerPanel._layers.SelectedIndex = 0;

            // Create the base material
            _layerPanel._finalMaterial = _graphics.CreateMaterial("FinalMaterial");
            _layerPanel._blendStageMaterial = _graphics.CreateMaterial("StagingMaterial");
        }

        // Get the selected material
        public MaoliSharp.Material GetSelectedLayer()
        {
            return _layerPanel._material;
        }

        // Load a texture set
        public void LoadMaterialTextureSet()
        {
            String fileName = Util.GetOpenFileName("Resources\\Textures\\Game Pack\\Textures", Util.TextureFilter);
            if (fileName != null)
            {
                if (_layerPanel._material.LoadTextureSet(fileName) == false)
                    MessageBox.Show("Texture Failed To Load");
                else
                {
                    UpdateGUI();
                }
            }
        }

        // Load a texture
        void LoadMaterialTexture(object sender, EventArgs e)
        {
            // Get the texture box that called this command
            MenuItem item = sender as MenuItem;
            PictureBox textureBox = item.GetContextMenu().SourceControl as PictureBox;

            // Load the texture
            String fileName = Util.GetOpenFileName("Resources\\Textures", Util.TextureFilter);
            if (fileName != null)
            {
                if (_layerPanel._material.LoadTexture(fileName, (MaoliSharp.TextureType)textureBox.Tag) == false)
                    MessageBox.Show("Texture Failed To Load");
                else
                {
                    UpdateGUI();
                }
            }
        }

        // Clear a texture
        void ClearMaterialTexture(object sender, EventArgs e)
        {
            // Get the texture box that called this command
            MenuItem item = sender as MenuItem;
            PictureBox textureBox = item.GetContextMenu().SourceControl as PictureBox;

            _layerPanel._material.ClearTexture((MaoliSharp.TextureType)textureBox.Tag);
            textureBox.BackgroundImage = _noTexImage;
        }

        // Build the material database
        public void BuildMaterialDatabase(String directory)
        {
            MaterialBuilder builder = new MaterialBuilder(_graphics);
            builder.BuildMaterialDatabase(directory);
            _browser = new MaterialBrowser(_graphics);
        }

        // Build a normal map from a bump map
        public void ComputeNormalMap(String file)
        {
            MaoliSharp.Texture bumpMap = _graphics.LoadTexture(file);
            MaoliSharp.Texture normalMap = _graphics.ComputeNormalMap(bumpMap);
            normalMap.Save("c:\\users\\nathaniel\\desktop\\normalmap.dds");
            SelectedMaterial.SetTexture(normalMap, MaoliSharp.TextureType.NormalMap);
        }

        // Set material color texture to alpha channel
        public void MaterialColorToAlpha()
        {
            if (_layerPanel._material != null)
                _layerPanel._material.SetTexture(_layerPanel._material.GetTexture(MaoliSharp.TextureType.BaseColor), MaoliSharp.TextureType.Alpha);
        }



        #endregion

    }
}

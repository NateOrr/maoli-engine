//--------------------------------------------------------------------------------------
// File: FirstPersonCamera.h
//
// Exposes the camera class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"
#include "Components/FirstPersonCamera.h"

#pragma managed
#include "FirstPersonCamera.h"
#include "Engine.h"
#include "Interop.h"

namespace MaoliSharp
{
	// Ctor
	FirstPersonCamera::FirstPersonCamera( UnmanagedComponent^ component ) : Camera( component )
	{
		_firstPersonCamera = (Maoli::FirstPersonCamera*)component->component;
	}


	// Mode
	CameraMode FirstPersonCamera::Mode::get()
	{
		return (CameraMode)_firstPersonCamera->GetMode();
	}

	// Mode
	void FirstPersonCamera::Mode::set( CameraMode value )
	{
		_firstPersonCamera->SetMode( Maoli::CameraMode( value ) );
	}

	// Focal distance
	float FirstPersonCamera::FocusDistance::get()
	{
		return _firstPersonCamera->GetFocusDistance();
	}

	// Focal distance
	void FirstPersonCamera::FocusDistance::set( float value )
	{
		_firstPersonCamera->SetFocusDistance( value );
	}

}

//--------------------------------------------------------------------------------------
// File: Terrain.cpp
//
// Exposes the Terrain class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"
#include "Graphics/Renderer.h"

#pragma managed
#include "Terrain.h"
#include "Entity.h"

namespace MaoliSharp
{
	// Ctor
	Terrain::Terrain( Maoli::Terrain* terrain )
	{
		_terrain = terrain;
		_materials = gcnew array<Material^>( 4 );
		for(uint32 i=0; i<_terrain->GetNumLayers(); ++i)
		{
			if(_terrain->GetLayerMaterial(i))
				_materials[i] = gcnew Material(_terrain->GetLayerMaterial(i));
		}
	}

	// Import a heightmap
	bool Terrain::ImportHeightmap( System::String^ fileName )
	{
		return _terrain->CreateFromFile( Interop::StringToChar( fileName ) );
	}

	// Save the current terrain to file
	void Terrain::ExportHeightmap( System::String^ fileName )
	{
		_terrain->ExportHeightmap( Interop::StringToChar( fileName ) );
	}

	// Save the current blend map to file
	void Terrain::ExportBlendmap( System::String^ file )
	{
		_terrain->ExportBlendmap( Interop::StringToChar( file ) );
	}

	// Create a new terrain
	bool Terrain::CreateHeightmap( uint32 size )
	{
		return _terrain->Create( size );
	}

	// Get the scale for the current terrain
	float Terrain::SizeScale::get()
	{
		return _terrain->GetScale();
	}

	// Set the scale for the current terrain
	void Terrain::SizeScale::set( float f )
	{
		_terrain->SetScale( f );
	}

	// Get the height scale for the current terrain
	float Terrain::HeightScale::get()
	{
		return _terrain->GetHeightScale();
	}

	// Set the height scale for the current terrain
	void Terrain::HeightScale::set( float f )
	{
		_terrain->SetHeightScale( f );
	}

	// Get the sculpting radius
	float Terrain::Radius::get()
	{
		return _terrain->GetEngine()->GetGraphics()->GetDebugVisualizer()->GetTerrainWidgetRadius();
	}

	// Set the sculpting radius
	void Terrain::Radius::set( float value )
	{
		return _terrain->GetEngine()->GetGraphics()->GetDebugVisualizer()->SetTerrainWidgetRadius(value);
	}

	// Get the sculpting hardness
	float Terrain::Hardness::get()
	{
		return _terrain->GetEngine()->GetGraphics()->GetDebugVisualizer()->GetTerrainWidgetHardness();
	}

	// Set the sculpting hardness
	void Terrain::Hardness::set( float value )
	{
		return _terrain->GetEngine()->GetGraphics()->GetDebugVisualizer()->SetTerrainWidgetHardness( value );
	}

	// Get the sculpting strength
	float Terrain::Strength::get()
	{
		return _terrain->GetEngine()->GetGraphics()->GetDebugVisualizer()->GetTerrainWidgetStrength();
	}

	// Set the sculpting strength
	void Terrain::Strength::set( float value )
	{
		return _terrain->GetEngine()->GetGraphics()->GetDebugVisualizer()->SetTerrainWidgetStrength( value );
	}

	// Get heightmap size
	uint32 Terrain::GetDimensions()
	{
		return _terrain->GetSize();
	}

	// Set the default material layer for the terrain
	void Terrain::SetMaterial( uint32 layer, Material^ mat )
	{
		_materials[layer] = mat;
		_terrain->AttachMaterial( mat->GetUnmanaged(), layer );
	}

	// Get the default material layer for the terrain
	Material^ Terrain::GetMaterial( uint32 layer )
	{
		return _materials[layer];
	}

	// Check for a ray intersection with the terrain
	bool Terrain::RayIntersect( Vector3 pos, Vector3 dir, float% dist )
	{
		float t;
		bool result = _terrain->RayIntersection(pos.AsMaoliVector(), dir.AsMaoliVector(), t);
		dist = t;
		return result;
	}

	// Import a blendmap
	bool Terrain::ImportBlendmap( System::String^ file )
	{
		return _terrain->LoadBlendmap( Interop::StringToChar(file) );
	}


}

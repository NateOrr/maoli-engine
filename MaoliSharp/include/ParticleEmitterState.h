//--------------------------------------------------------------------------------------
// File: ParticleEmitter
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/ParticleEmitterState.h"

#pragma managed
using namespace System;
using namespace Honua;
using namespace System::Drawing;
using namespace System::Collections::Generic;
#include "Component.h"
#include "Dependency.h"
#include "Interop.h"

// Property helper
#define PROPERTY(type, name) property type name{\
	inline type  get() { return _state->Get##name(); }\
	inline void set( type value ) { _state->Set##name( value ); }\
}

#define TEXT_PROPERTY(name) property String^ name{\
	inline String^ get() { return gcnew String(_state->Get##name()); }\
	inline void set( String^ value ) { _state->Set##name( Interop::StringToChar(value) ); }\
}

namespace MaoliSharp
{
	// Forward decl
	ref class Texture;
	ref class ParticleSet;

	public ref class ParticleEmitterState
	{
	public:

		// Ctor
		ParticleEmitterState( Maoli::ParticleEmitterState* state );

		// Get the unmanaged Light object
		inline Maoli::ParticleEmitterState* GetUnmanaged() { return _state; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( ParticleEmitterState^ state ) { return (_state == state->_state); }

		// ToString for GUI boxes
		String^ ToString() override { return gcnew String( _state->GetName() ); }

		[HonuaFloat( ValueControls::Box, 0, 1000.0f )]
		PROPERTY( float, Duration );

		[HonuaText]
		TEXT_PROPERTY( Name );

		// Add an existing set
		void AddSet( ParticleSet^ set );
		void AddSet( ParticleSet^ set, int32 index );

		// Remove a state
		void RemoveSet( ParticleSet^ set );

		// Get a state
		inline ParticleSet^ GetSet( int32 index ) { return _sets[index]; }

		// Get the number of states
		inline int32 GetNumSets() { return _sets->Count; }

		// The next state to transition to
		property ParticleEmitterState^ NextState
		{
			inline ParticleEmitterState^ get(){ return _nextState; }
			void set(ParticleEmitterState^ value);
		}

	private:
		Maoli::ParticleEmitterState*		_state;
		ParticleEmitterState^				_nextState;
		List<ParticleSet^>^					_sets;
	};


}

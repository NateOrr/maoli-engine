//--------------------------------------------------------------------------------------
// File: World.h
//
// Game world, contains a set of entities
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged

// Forward decl
namespace Maoli { class World; }

#pragma managed
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace MaoliSharp
{
	// Forward decl
	ref class Engine;
	ref class Entity;

	public ref class World
	{
	public:

		// Ctor
		World( Engine^ engine );

		// Dtor
		~World();

		// Access the unmanaged object
		inline Maoli::World* GetUnmanaged() { return _world; }

		// Add an entity
		void AddEntity( Entity^ e );

		// Remove an entity
		void RemoveEntity( Entity^ e );

		// Remove all entities
		void Clear();

		// Save the world
		void Save( String^ file );

		// Open the world
		bool Load( String^ file );

		// Delete all entities
		void DeleteEntities();

		// Get the entities
		property List<Entity^>^ Entities
		{
			inline List<Entity^>^ get() { return _entities; }
		}

		// Clone the world
		World^ Clone();

	private:

		Engine^			_engine;
		List<Entity^>^	_entities;
		Maoli::World*	_world;
	};
}

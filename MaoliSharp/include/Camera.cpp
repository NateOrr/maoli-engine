//--------------------------------------------------------------------------------------
// File: Camera.h
//
// Exposes the camera class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Camera.h"

namespace MaoliSharp
{
	// Ctor
	Camera::Camera( UnmanagedComponent^ component ) : Transform( component )
	{
		_camera = (Maoli::Camera*)_component;
	}

}

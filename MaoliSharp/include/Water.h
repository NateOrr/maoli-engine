//--------------------------------------------------------------------------------------
// File: Water.cpp
//
// Ocean water
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "Graphics/Water.h"

#pragma managed
using namespace System::Drawing;
using namespace Honua;

// Property helper
#define PROPERTY(type, name) property type name{\
	inline type  get() { return _ocean->Get##name(); }\
	inline void set( type value ) { _ocean->Set##name( value ); }\
}


#define COLOR_PROPERTY(name) property Color name\
{\
	Color get()\
{\
	return Color::FromArgb( 255, \
	(int32)(_ocean->Get##name().r*255.0f), \
	(int32)(_ocean->Get##name().g*255.0f), \
	(int32)(_ocean->Get##name().b*255.0f) ); \
}\
	void set( Color c ) { _ocean->Set##name( Maoli::Color( (float)c.R / 255.0f, (float)c.G / 255.0f, (float)c.B / 255.0f ) ); }\
}

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Ocean
	{
	public:

		// Ctor
		Ocean( Maoli::Ocean* ocean );

		[HonuaFloat( ValueControls::Slider, -1000.0f, 1000.0f )]
		PROPERTY( float, WaterLevel );
		
		[HonuaVector]
		property Vector3 WindDirection
		{
			Vector3 get(){ return Vector3(_ocean->GetWindDirection().x, 0, _ocean->GetWindDirection().y); }
			void set(Vector3 value){ _ocean->SetWindDirection(value.x, value.y); }
		}

		[HonuaFloat( ValueControls::Slider, 0.0f, 50.0f )]
		PROPERTY( float, WindVelocity );

		[HonuaFloat( ValueControls::Slider, 0.0f, 10.0f )]
		PROPERTY( float, WaveConstant );

		[HonuaFloat( ValueControls::Slider, 0.0f, 20.0f )]
		PROPERTY( float, WaveLength );
		
		[HonuaFloat( ValueControls::Slider, -20.0f, 20.0f )]
		PROPERTY( float, WaveGravity );
		
		[HonuaFloat( ValueControls::Slider, 0.0f, 5.0f )]
		PROPERTY( float, WaveHeightScale );
		
		[HonuaFloat( ValueControls::Slider, 0.0f, 2.0f )]
		PROPERTY( float, WaveDisplacementScale );

		[HonuaColor]
		COLOR_PROPERTY( ExtinctionColor );

		[HonuaColor]
		COLOR_PROPERTY( BackscatterColor );

	private:

		Maoli::Ocean*	_ocean;
	};
}

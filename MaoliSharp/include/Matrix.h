//--------------------------------------------------------------------------------------
// File: Matrix.h
//
// 4x4 Matrix
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once
#include <d3dx10.h>

namespace MaoliSharp
{
//	[System::Security::SuppressUnmanagedCodeSecurity]
// 	public value class Matrix
// 	{
// 	public:
// 
// 		// Convert to a Matrix
// 		Matrix AsD3DXMATRIX();
// 
// 		// Make an identity matrix
// 		Matrix% Identity();
// 
// 		// Make a rotation matrix about the x-axis
// 		Matrix% RotationX(float theta);
// 
// 		// Make a rotation matrix about the y-axis
// 		Matrix% RotationY(float theta);
// 
// 		// Make a rotation matrix about the z-axis
// 		Matrix% RotationZ(float theta);
// 
// 		// Transpose the matrix
// 		Matrix% Transpose();
// 		
// 
// 	private:
// 		float _11, _12, _13, _14;
// 		float _21, _22, _23, _24;
// 		float _31, _32, _33, _34;
// 		float _41, _42, _43, _44;
// 	};
}

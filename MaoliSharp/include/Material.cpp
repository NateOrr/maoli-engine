//--------------------------------------------------------------------------------------
// File: Material.cpp
//
// Exposes the material class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Material.h"
#include "Texture.h"

namespace MaoliSharp
{

	// Ctor
	Material::Material( Material^ mat )
	{
		_ptr = new Maoli::MaterialBinding::pointer;
		*_ptr = *mat->_ptr;
		_materialBinding = **_ptr;
	}

	// Ctor
	Material::Material( Maoli::MaterialBinding::pointer pMat )
	{
		_ptr = new Maoli::MaterialBinding::pointer;
		*_ptr = pMat;
		_materialBinding = **_ptr;
	}

	// Dtor
	Material::~Material()
	{
		*_ptr = nullptr;
		delete _ptr;
	}


	// Load a texture set from the game pack
	bool Material::LoadTextureSet( System::String^ file )
	{
		return false;
	}


	// Clear the texture
	void Material::ClearTexture( TextureType type )
	{
		_materialBinding->ClearTexture( (Maoli::TEX_TYPE)type );
	}


	// Get a texture's name
	System::String^ Material::GetTextureName( TextureType index )
	{
		if ( _materialBinding->GetTexture( (Maoli::TEX_TYPE)index ) == nullptr )
			return nullptr;
		return gcnew System::String( _materialBinding->GetTexture( (Maoli::TEX_TYPE)index )->GetName().c_str() );
	}

	// Texture loading
	bool Material::LoadTexture( System::String^ file, TextureType type )
	{
		return (_materialBinding->LoadTexture( Interop::StringToChar( file ), (Maoli::TEX_TYPE)type ) != nullptr);
	}

	// Set a texture
	void Material::SetTexture( Texture^ tex, TextureType type )
	{
		_materialBinding->SetTexture( tex->GetUnmanaged(), (Maoli::TEX_TYPE)type );
	}


	// Export this data into an open file
	void Material::Export( String^ file )
	{
		_materialBinding->GetMaterial()->Save( Interop::StringToChar( file ) );
	}

	// Imports from an open file
	void Material::Import( String^ file )
	{
		*_ptr = _materialBinding->GetParent()->CreateMaterialBindingFromFile( Interop::StringToChar( file ) );
		_materialBinding = **_ptr;
	}

	// Get a texture
	Texture^ Material::GetTexture( TextureType type )
	{
		auto tex = _materialBinding->GetTexture( (Maoli::TEX_TYPE)type );
		if ( tex )
			return gcnew Texture( tex );
		return nullptr;
	}

	// Get the material file name
	String^ Material::GetPreviewFile()
	{
		return gcnew String( _materialBinding->GetPreviewFile() );
	}


}

//--------------------------------------------------------------------------------------
// File: Camera.h
//
// Exposes the camera class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged

// Forward decl
namespace Maoli
{
	class Camera;
}

#pragma managed
#include "Transform.h"
using namespace System;
using namespace System::Runtime::InteropServices;

namespace MaoliSharp
{
	// Forward decl
	ref class Engine;

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Camera : public Transform
	{
	public:

		// Ctor
		Camera( UnmanagedComponent^ component );


		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "A 3D transform that acts as a camera";
		}

	protected:
		Maoli::Camera*		_camera;
	};
}

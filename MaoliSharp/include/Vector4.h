//--------------------------------------------------------------------------------------
// File: Vector4.h
//
// 4D Vector4
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace MaoliSharp
{

	// 4D Vector
	public value class Vector4
	{
	public:
		// Constructors
		Vector4(const Maoli::Vector4& v) : x(v.x), y(v.y), z(v.z), w(v.w) {}
		Vector4(const Maoli::Vector3& v) : x(v.x), y(v.y), z(v.z), w(0) {}
		Vector4(float vx, float vy, float vz) : x(vx), y(vy), z(vz), w(0) {}
		Vector4(float vx, float vy, float vz, float vw) : x(vx), y(vy), z(vz), w(vw) {}

		// Convert to d3d vectors
		Maoli::Vector3 AsD3DXVECTOR3(){ return Maoli::Vector3(x, y, z); }

		// Assign to d3d vectors
		void Set(const Maoli::Vector3& v){ x=v.x; y=v.y; z=v.z; }
		void Set(const Maoli::Vector4& v){ x=v.x; y=v.y; z=v.z; w=v.w; }

		// Convert to a color
		System::Drawing::Color ToColor(); 

		// Subtraction
		static Vector4 operator-(Vector4 a, Vector4 b);
		static Vector4 operator-=(Vector4 v, Vector4 b);

		// Addition
		static Vector4 operator+(Vector4 a, Vector4 b);
		static Vector4 operator+=(Vector4 v, Vector4 b);

		// Scalar mult
		static Vector4 operator*(Vector4 v, float f);
		static Vector4 operator*=(Vector4 v, float f);

		// Scalar div
		static Vector4 operator/(Vector4 v, float d);
		static Vector4 operator/=(Vector4 v, float f);

		// Dot product
		float Dot(Vector4 v);
		static float operator*(Vector4 a, Vector4 b);

		// Cross product of the 3d components
		Vector4 Cross(Vector4 v);

		// Length
		float Length();

		// Normalize the vector
		Vector4 Normalize();

		float x, y, z, w;
	};

}

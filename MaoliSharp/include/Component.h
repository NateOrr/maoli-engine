//--------------------------------------------------------------------------------------
// File: Game/Component.h
//
// Game object component class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

using namespace System;

namespace MaoliSharp
{

	// Holds an un managed component, just for allowing reflection
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class UnmanagedComponent
	{
	public:
		inline UnmanagedComponent( Maoli::Component* c, Maoli::Engine* e ) : component( c ), engine( e ) {}

		Maoli::Component* component;
		Maoli::Engine* engine;
	};

	// Forward decl so we can use entity now
	ref class Entity;

	// Basic building block for an entity
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Component
	{
	public:

		// Ctor
		Component( UnmanagedComponent^ component );

		// Dtor
		virtual ~Component();

		// Access the unmanaged part
		inline Maoli::Component* GetUnmanaged() { return _component; }

		// Gets the component typename
		String^ GetTypeName();

		// Lets the editors know if this component is file based
		virtual bool FileBased() { return false; }

		// Gets a file dialog filter for loading
		virtual String^ GetFileFilter()
		{
			return "All Files | *.*;";
		}

		// Load from file
		virtual bool Load( System::String^ fileName )
		{
			return true;
		}


		// The parent entity
		property Entity^ Owner
		{
			inline Entity^ get() { return _entity; }
			inline void set( Entity^ entity ) { _entity = entity; }
		}

		// Convert to string
		virtual String^ ToString() override;

		// Register a dependency
		void AddDependency( Component^ target );

		// Remove a dependency
		void RemoveDependency( Component^ target );

		// Custom rendering
		virtual void Render();

	protected:

		Entity^				_entity;
		Maoli::Component*	_component;
		Maoli::Engine*		_engine;
	};

}

//--------------------------------------------------------------------------------------
// File: RigidBody.h
//
// Rigid body component
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"
#include "Physics/RigidBody.h"

#pragma managed
#include "RigidBody.h"
#include "Interop.h"
#include "Engine.h"
#include "Model.h"
#include "Entity.h"

namespace MaoliSharp
{

	// Ctor
	RigidBody::RigidBody( UnmanagedComponent^ component ) : Component( component )
	{
		_body = (Maoli::RigidBody*)component->component;
		_desc = new Maoli::RigidBodyDesc;
		memcpy( _desc, &_body->GetDesc(), sizeof(Maoli::RigidBodyDesc) );
		_fitToModel = false;
	}

	// Dtor
	RigidBody::~RigidBody()
	{
		delete _desc;
	}

	// Update body based on the desc
	bool RigidBody::Create()
	{
		// Since a whole new body has to be created, remove the old one first
		if ( _body->IsActive() && Owner )
			_body->GetEngine()->GetPhysics()->UnregisterComponent( _body );

		// Update the desc as needed
		if ( _entity )
		{
			auto model = _entity->GetComponent<MaoliSharp::Model^>();
			_desc->geometry = (model && model->GetUnmanaged()) ? model->GetUnmanaged()->GetGeometry() : nullptr;
			if ( _fitToModel && model )
			{
				_desc->size = model->GetUnmanaged()->GetBounds().GetSize();
				_desc->radius = model->GetUnmanaged()->GetBounds().GetRadius();
			}
		}

		// Create the new one
		bool result = _body->Create( *_desc );

		// Add it back in
		if ( Owner )
			_body->GetEngine()->GetPhysics()->RegisterComponent( _body );

		return result;
	}


	// Fit to the model, if there is one
	bool RigidBody::FitToModel::get()
	{
		return _fitToModel;
	}

	// Fit to the model, if there is one
	void RigidBody::FitToModel::set( bool value )
	{
		_fitToModel = value;
		Create();
	}


	// Mass
	float RigidBody::Mass::get()
	{
		return _desc->mass;
	}

	// Mass
	void RigidBody::Mass::set( float f )
	{
		_desc->mass = f;
		Create();
	}


	// Restitition
	float RigidBody::Restitution::get()
	{
		return _desc->restitution;
	}

	// Restitition
	void RigidBody::Restitution::set( float f )
	{
		_desc->restitution = f;
		Create();
	}


	// Radius
	float RigidBody::Radius::get()
	{
		return _desc->radius;
	}

	// Radius
	void RigidBody::Radius::set( float f )
	{
		_desc->radius = f;
		Create();
	}

	// Radius
	float RigidBody::Height::get()
	{
		return _desc->size.y;
	}

	// Radius
	void RigidBody::Height::set( float f )
	{
		_desc->size.y = f;
		Create();
	}


	// AABB
	MaoliSharp::Vector3 RigidBody::Size::get()
	{
		return Vector3( _desc->size );
	}

	// AABB
	void RigidBody::Size::set( Vector3 f )
	{
		_desc->size = Maoli::Vector3( f.x, f.y, f.z );
		Create();
	}


	// Body type
	MaoliSharp::RigidBodyShape RigidBody::Shape::get()
	{
		return (RigidBodyShape)_desc->shape;
	}

	// Body type
	void RigidBody::Shape::set( RigidBodyShape value )
	{
		if ( !_entity->GetComponent<MaoliSharp::Model^>() && (value == RigidBodyShape::Mesh || value == RigidBodyShape::ConvexHull) )
		{
			_body->GetEngine()->GetPlatform()->MessageBox( "Cannot create a mesh or convex hull shape without a model component" );
			return;
		}
		_desc->shape = (Maoli::PhysicsShape)value;
		Create();
	}


	// Motion type
	MaoliSharp::MotionType RigidBody::Motion::get()
	{
		return (MotionType)_desc->motionType;
	}

	// Motion type
	void RigidBody::Motion::set( MotionType f )
	{
		_desc->motionType = (Maoli::MotionType)f;
		Create();
	}


	// Collision group
	CollisionLayer RigidBody::CollisionGroup::get()
	{
		return (CollisionLayer)_body->GetCollisionLayer();
	}

	// Collision group
	void RigidBody::CollisionGroup::set( CollisionLayer value )
	{
		_body->SetCollisionLayer( (int32)value );
	}

	// Check for model component
	bool RigidBody::HasModel::get()
	{
		return (_entity->GetComponent<MaoliSharp::Model^>() != nullptr);
	}


	// Local position
	Vector3 RigidBody::LocalPosition::get()
	{
		return Vector3( _body->GetLocalPosition() );
	}

	// Local position
	void RigidBody::LocalPosition::set( Vector3 value )
	{
		_body->SetLocalPosition( value.AsMaoliVector() );
	}

}

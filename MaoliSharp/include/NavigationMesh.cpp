//--------------------------------------------------------------------------------------
// File: NavigationMesh.cpp
//
// Exposes the NavMesh class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "NavigationMesh.h"
#include "Interop.h"

namespace MaoliSharp
{
	// Ctor
	NavMesh::NavMesh( UnmanagedComponent^ component ) : Component(component)
	{
		_navMesh = (Maoli::NavMesh*)component->component;
	}

	// Load from file
	bool NavMesh::Load( System::String^ fileName )
	{
		return _navMesh->Load( Interop::StringToChar( fileName ) );
	}

}
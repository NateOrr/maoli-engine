//--------------------------------------------------------------------------------------
// File: Animation.h
//
// Exposes the AnimationInstance class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
namespace Maoli { class AnimationController; }

#pragma managed
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace Honua;

namespace MaoliSharp
{
	// Forward decl
	ref class Engine;

	// Animation track names
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class AnimationTrack
	{
	public:

		// Ctor
		AnimationTrack( Maoli::AnimationController* animation, uint32 index );

		// Play the track
		void Play();

		// Add an event
		void AddEvent( int32 id, float time );

		// Remove all instances of this event
		void RemoveEvent( int32 id );

		// Clear all events
		void RemoveAllEvents();

		// Track name
		[HonuaText]
		property String^ Name
		{
			String^ get();
			void set( String^ value );
		}

		// Get the looping state for the track
		[HonuaBool]
		property bool Loop
		{
			bool get();
			void set( bool value );
		}

		// Duration
		property float Duration
		{
			float get();
		}

		// Animation time
		property float Time
		{
			float get();
			void set( float value );
		}

		// Track file name
		property String^ FileName
		{
			String^ get();
		}

		// Playback speed
		[HonuaFloat(ValueControls::Slider, -20.0f, 20.0f)]
		property float Speed
		{
			float get();
			void set( float value );
		}

		// Index in the animation set
		property int32 Index
		{
			int32 get();
			void set(int32 value);
		}

		// Convert to string
		virtual String^ ToString() override;

	private:

		Maoli::AnimationController*	_animation;
		int32 _index;
	};

}
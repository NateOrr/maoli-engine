//--------------------------------------------------------------------------------------
// File: ParticleEmitter
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/ParticleSet.h"

#pragma managed
using namespace System;
using namespace Honua;
using namespace System::Drawing;
#include "Component.h"
#include "Dependency.h"
#include "Interop.h"

// Property helper
#define PROPERTY(type, name) property type name{\
	inline type  get() { return _set->Get##name(); }\
	inline void set( type value ) { _set->Set##name( value ); }\
}

#define VECTOR_PROPERTY(name) property Vector3 name{\
	inline Vector3 get() { return Vector3( _set->Get##name() ); }\
	inline void set( Vector3 value ) { _set->Set##name( value.AsMaoliVector() ); }\
}

#define COLOR_PROPERTY(name) property Color name\
{\
	Color get()\
{\
	return Color::FromArgb( 255, \
	(int32)(_set->Get##name().r*255.0f), \
	(int32)(_set->Get##name().g*255.0f), \
	(int32)(_set->Get##name().b*255.0f) ); \
}\
	void set( Color c ) { _set->Set##name( Maoli::Color( (float)c.R / 255.0f, (float)c.G / 255.0f, (float)c.B / 255.0f ) ); }\
}

#define TEXT_PROPERTY(name) property String^ name{\
	inline String^ get() { return gcnew String(_set->Get##name()); }\
	inline void set( String^ value ) { _set->Set##name( Interop::StringToChar(value) ); }\
}

namespace MaoliSharp
{
	// Forward decl
	ref class Texture;

	public ref class ParticleSet
	{
	public:

		// Ctor
		ParticleSet( Maoli::ParticleSet* set);

		// Get the unmanaged Light object
		inline Maoli::ParticleSet* GetUnmanaged() { return _set; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( ParticleSet^ emitter ) { return (_set == emitter->_set); }

		// ToString for GUI boxes
		String^ ToString() override { return gcnew String( _set->GetName() ); }

		// Position
		[AxisWidget( AxisMode::Translate )]
		VECTOR_PROPERTY( Position );

		[HonuaText]
		TEXT_PROPERTY( Name );

		[HonuaBool]
		property bool Animated
		{
			inline bool get() { return _set->IsAnimated(); }
			inline void set( bool value ) { _set->EnableAnimation( value ); }
		}

		[HonuaTexture]
		property MaoliSharp::Texture^ Texture
		{
			inline MaoliSharp::Texture^ get() { return _texture; }
			void set( MaoliSharp::Texture^ value );
		}

		[HonuaDependency( "Animated", true )]
		[HonuaInt( ValueControls::Box, 32, 1024 )]
		PROPERTY( int32, AnimationFrameWidth );

		[HonuaDependency( "Animated", true )]
		[HonuaInt( ValueControls::Box, 32, 1024 )]
		PROPERTY( int32, AnimationFrameHeight );

		[HonuaDependency( "Animated", true )]
		[HonuaInt( ValueControls::Box, 1, 64 )]
		PROPERTY( int32, SpriteSheetWidth );

		[HonuaDependency( "Animated", true )]
		[HonuaInt( ValueControls::Box, 1, 64 )]
		PROPERTY( int32, SpriteSheetHeight );

		[HonuaDependency( "Animated", true )]
		[HonuaFloat( ValueControls::Box, 0.0f, 100.0f )]
		PROPERTY( float, AnimationLength );

		[HonuaInt( ValueControls::Box, 0, 1000000 )]
		PROPERTY( int32, MaxParticles );

		[HonuaInt( ValueControls::Slider, 0, 100000 )]
		PROPERTY( int32, EmissionRate );

		[HonuaFloat( ValueControls::Box, 0.0f, 100.0f )]
		PROPERTY( float, Lifetime );

		[HonuaFloat( ValueControls::Box, 0.0f, 100.0f )]
		PROPERTY( float, FadeInTime );

		[HonuaFloat( ValueControls::Box, 0.0f, 100.0f )]
		PROPERTY( float, FadeOutTime );

		[HonuaFloat( ValueControls::Slider, 0, 100.0f )]
		PROPERTY( float, SpawnRadius );

		[HonuaVector]
		VECTOR_PROPERTY( Gravity );

		[HonuaVector]
		VECTOR_PROPERTY( MinVelocity );

		[HonuaVector]
		VECTOR_PROPERTY( MaxVelocity );

		[HonuaVector]
		VECTOR_PROPERTY( MinSize );

		[HonuaVector]
		VECTOR_PROPERTY( MaxSize );

		[HonuaVector]
		VECTOR_PROPERTY( EndSize );

		[HonuaColor]
		COLOR_PROPERTY( MinColor );

		[HonuaColor]
		COLOR_PROPERTY( MaxColor );

		[HonuaColor]
		COLOR_PROPERTY( EndColor );

	private:
		Maoli::ParticleSet*			_set;
		MaoliSharp::Texture^		_texture;
	};


}

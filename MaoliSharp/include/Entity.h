//--------------------------------------------------------------------------------------
// File: Entity.h
//
// Game object class
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

namespace MaoliSharp
{

	// Forward decl
	ref class Component;
	ref class Engine;

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Entity
	{
	public:

		// Ctor
		Entity( String^ name, Engine^ engine );
		Entity( String^ name, Engine^ engine, bool isTemplate );

		// Ctor
		Entity( Engine^ engine, Maoli::Entity* entity );

		// Get the unmanaged part
		inline Maoli::Entity* GetUnmanaged() { return _entity; }

		// Get the entity name
		inline String^ GetName() { return gcnew String( _entity->GetName() ); }

		// Get the entity name
		virtual String^ ToString() override { return GetName(); }

		// Add a component
		bool AddComponent( Component^ component );

		// Remove a component
		void RemoveComponent( Component^ component );

		// Get a component by type
		generic <typename T> where T : Component
			T GetComponent();

		// Get the components
		property List<Component^>^ Components
		{
			inline List<Component^>^ get() { return _components; }
		}

		// Get the bounding box of an entity
		void GetBounds( [Out] Vector3% center, [Out] Vector3% size );

		// Get the center of an entity
		Vector3 GetPosition();

		// Clone the entity
		Entity^ Clone();

		// Get the engine
		property MaoliSharp::Engine^ Engine
		{
			inline MaoliSharp::Engine^ get() { return _engine; }
		}

		// Dtor
		~Entity();

		// Finalizer
		!Entity();

	private:

		// Build the component list
		void BuildComponentList();

		Maoli::Entity*		_entity;		// Unmanaged entity
		List<Component^>^	_components;	// Component data
		MaoliSharp::Engine^	_engine;		// Game engine
		bool				_disposed;
	};

}

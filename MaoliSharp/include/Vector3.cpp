//--------------------------------------------------------------------------------------
// File: Vector3.h
//
// 3D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Vector3.h"

namespace MaoliSharp
{
	// Subtraction
	Vector3 Vector3::operator-(Vector3 a, Vector3 b)
	{
		return Vector3(a.x-b.x, a.y-b.y, a.z-b.z);
	}

	// Subtraction
	Vector3 Vector3::operator-=(Vector3 a, Vector3 v)
	{
		a.x-=v.x;
		a.y-=v.y;
		a.z-=v.z;
		return a;
	}

	// Addition
	Vector3 Vector3::operator+(Vector3 a, Vector3 b)
	{
		return Vector3(a.x+b.x, a.y+b.y, a.z+b.z);
	}

	// Addition
	Vector3 Vector3::operator+=(Vector3 a, Vector3 v)
	{
		a.x+=v.x;
		a.y+=v.y;
		a.z+=v.z;
		return a;
	}


	// Scalar mult
	Vector3 Vector3::operator*(Vector3 v, float f)
	{
		return Vector3(v.x*f, v.y*f, v.z*f);
	}


	// Scalar mult
	Vector3 Vector3::operator*=(Vector3 v, float f)
	{
		v.x*=f;
		v.y*=f;
		v.z*=f;
		return v;
	}


	// Scalar div
	Vector3 Vector3::operator/(Vector3 v, float d)
	{
		float f = 1.0f/d;
		return Vector3(v.x*f, v.y*f, v.z*f);
	}


	// Scalar div
	Vector3 Vector3::operator/=(Vector3 v, float d)
	{
		float f = 1.0f/d;
		v.x*=f;
		v.y*=f;
		v.z*=f;
		return v;
	}



	// Dot product
	float Vector3::Dot(Vector3 v)
	{
		return x*v.x + y*v.y + z*v.z;
	}


	// Dot product
	float Vector3::operator*(Vector3 a, Vector3 b)
	{
		return a.Dot(b);
	}


	// Cross product of the 3d components
	Vector3 Vector3::Cross(Vector3 v)
	{
		return Vector3(
			y*v.z - v.y*z,
			v.x*z - x*v.z,
			x*v.y - v.x*y );
	}


	// Length
	float Vector3::Length()
	{
		return sqrtf(x*x + y*y + z*z);
	}


	// Normalize the vector
	Vector3 Vector3::Normalize()
	{
		float l = 1.0f / Length();
		x*=l;
		y*=l;
		z*=l;
		return *this;
	}



	// Convert to a color
	System::Drawing::Color Vector3::ToColor()
	{
		return System::Drawing::Color::FromArgb(255, int32(x*255), int32(y*255), int32(z*255));
	}

	// Negation
	Vector3 Vector3::operator-()
	{
		return Vector3( -x, -y, -z );
	}

	// Component wise max
	Vector3 Vector3::Max( Vector3 a, Vector3 b )
	{
		return Vector3( std::max( a.x, b.x ), std::max( a.y, b.y ), std::max( a.z, b.z ) );
	}

	// Component wise min
	Vector3 Vector3::Min( Vector3 a, Vector3 b )
	{
		return Vector3( std::min( a.x, b.x ), std::min( a.y, b.y ), std::min( a.z, b.z ) );
	}

	// Component wise abs
	Vector3 Vector3::Absolute()
	{
		return Vector3(fabs(x), fabs(y), fabs(z));
	}

}

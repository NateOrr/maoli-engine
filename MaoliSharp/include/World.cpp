//--------------------------------------------------------------------------------------
// File: World.cpp
//
// Game world, contains a set of entities
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"
#include "Game/World.h"

#pragma managed
#include "World.h"
#include "Engine.h"
#include "Interop.h"
#include "Entity.h"

namespace MaoliSharp
{
	// Ctor
	World::World( Engine^ engine )
	{
		_world = new Maoli::World( engine->GetUnmanaged() );
		_entities = gcnew List<Entity^>();
		_engine = engine;
	}

	// Dtor
	World::~World()
	{
		delete _world;
	}

	// Save the world
	void World::Save( String^ file )
	{
		_world->Save( Interop::StringToChar( file ) );
	}

	// Open the world
	bool World::Load( String^ file )
	{
		if ( _world->Load( Interop::StringToChar( file ) ) )
		{
			for ( uint32 i = 0; i < _world->GetEntities().Size(); ++i )
				_entities->Add( gcnew Entity( _engine, _world->GetEntities()[i] ) );
			return true;
		}
		return false;
	}

	// Remove all entities
	void World::Clear()
	{
		_entities->Clear();
		_world->GetEntities().Clear();
	}

	// Remove an entity
	void World::RemoveEntity( Entity^ e )
	{
		_entities->Remove( e );
		_world->GetEntities().Remove( e->GetUnmanaged() );
	}

	// Add an entity
	void World::AddEntity( Entity^ e )
	{
		_entities->Add( e );
		_world->GetEntities().Add( e->GetUnmanaged() );
	}

	// Delete all entities
	void World::DeleteEntities()
	{
		for ( int32 i = 0; i < _entities->Count; ++i )
			delete _entities[i];
		_entities->Clear();
	}

	// Clone the world
	World^ World::Clone()
	{
		World^ clone = gcnew World( _engine );
		for ( int32 i = 0; i < _entities->Count; ++i )
			clone->AddEntity( _entities[i]->Clone() );
		return clone;
	}

}




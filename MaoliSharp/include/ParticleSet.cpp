//--------------------------------------------------------------------------------------
// File: ParticleEmitter
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
using namespace System;
#include "ParticleSet.h"
#include "Texture.h"

namespace MaoliSharp
{
	// Ctor
	ParticleSet::ParticleSet( Maoli::ParticleSet* set )
	{
		_set = set;
		_texture = gcnew MaoliSharp::Texture( _set->GetTexture() );
	}

	// Set the texture
	void ParticleSet::Texture::set( MaoliSharp::Texture^ value )
	{
		if(_texture != value)
		{
			_texture = value;
			_set->SetTexture( _texture->GetUnmanaged() );
		}
	}

}
//--------------------------------------------------------------------------------------
// File: Engine.h
//
// Direct3D 11 Game Engine Wrapper
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"
#include "Game.h"

#pragma managed
#include "Engine.h"
#include "Renderer.h"
#include "Platform.h"
#include "Entity.h"
#include "FirstPersonCamera.h"
#include "World.h"

using namespace System::Reflection;

namespace MaoliSharp
{
	// Ctor
	Engine::Engine()
	{
		_engine = nullptr;
		_graphics = nullptr;
		_platform = nullptr;
		_game = nullptr;
		_entities = gcnew List<Entity^>();
		_renderableComponents = gcnew List<Component^>();
	}


	// Setup the engine
	bool Engine::Init( IntPtr mainWindow, IntPtr renderWindow )
	{
		_game = new Maoli::Game;
		if ( !_game->Create( (HWND)mainWindow.ToPointer(), (HWND)renderWindow.ToPointer() ) )
		{
			Maoli::ErrorMessage( "Failed to create the game engine" );
			delete _game;
			return false;
		}
		_engine = _game->GetEngine();
		_engine->GetGraphics()->CreateDebugVisualizer();
		_platform = gcnew MaoliSharp::Platform( _engine->GetPlatform() );
		_graphics = gcnew Renderer( this );
		return true;
	}


	// Frame update
	void Engine::Update()
	{
		_game->Step();
	}

	// Render using the primary renderer
	void Engine::Render()
	{
		// Render components (for editor)
		for ( int32 i = 0; i < _renderableComponents->Count; ++i )
			_renderableComponents[i]->Render();

		_game->Render();
	}

	// Cleanup
	void Engine::Release()
	{
		delete _game;
		_game = nullptr;
		_engine = nullptr;
		_platform = nullptr;
		_graphics = nullptr;
	}

	// Register a component with the engine
	void Engine::RegisterComponent( Component^ component )
	{
		// Only render components who have overidden the Render method
		if(component->GetType()->GetMethod( "Render" )->DeclaringType == component->GetType())
			_renderableComponents->Add( component );
	}

	// Unregister a component with the engine
	void Engine::UnregisterComponent( Component^ component )
	{
		// Only render components who have overidden the Render method
		if ( component->GetType()->GetMethod( "Render" )->DeclaringType == component->GetType() )
			_renderableComponents->Remove( component );
	}

	// Add an entity to the world
	void Engine::AddEntity( Entity^ entity )
	{
		_entities->Add( entity );
		_engine->AddEntity( entity->GetUnmanaged() );

		// Check for renderable components
		for ( int32 i = 0; i < entity->Components->Count; ++i )
			RegisterComponent( entity->Components[i] );
	}

	// Remove an entity from the world
	void Engine::RemoveEntity( Entity^ entity )
	{
		_entities->Remove( entity );
		_engine->RemoveEntity( entity->GetUnmanaged() );

		// Remove renderable components
		for ( int32 i = 0; i < entity->Components->Count; ++i )
			UnregisterComponent( entity->Components[i] );
	}

	// Create a new entity
	Entity^ Engine::CreateEntity( String^ name )
	{
		return gcnew Entity( name, this );
	}

	// Create a new entity
	Entity^ Engine::CreateEntityTemplate( String^ name )
	{
		return gcnew Entity( name, this, true );
	}


	// Create a component based on type
	generic <typename T> where T : Component T Engine::CreateComponent()
	{
		// Get the class type name
		Type^ type = T::typeid;
		Maoli::String className = Interop::StringToChar( type->FullName );
		className.Replace( "MaoliSharp.", "class Maoli::" );

		// Create the component
		auto component = Maoli::Component::Create( _engine, className );

		// Get the managed constructor
		array<Type^>^ types = gcnew array<Type^>( 1 );
		types[0] = UnmanagedComponent::typeid;
		auto ctor = type->GetConstructor( types );
		array<Object^>^ params = gcnew array<Object^>( 1 );
		params[0] = gcnew UnmanagedComponent( component, _engine );
		return (T)ctor->Invoke( params );
	}

	// Create a component from type
	Component^ Engine::CreateComponent( Type^ type )
	{
		// Get the class type name
		Maoli::String className = Interop::StringToChar( type->FullName );
		className.Replace( "MaoliSharp.", "class Maoli::" );

		// Create the component
		auto component = Maoli::Component::Create( _engine, className );

		// Get the managed constructor
		array<Type^>^ types = gcnew array<Type^>( 1 );
		types[0] = UnmanagedComponent::typeid;
		auto ctor = type->GetConstructor( types );
		array<Object^>^ params = gcnew array<Object^>( 1 );
		params[0] = gcnew UnmanagedComponent( component, _engine );
		return (Component^)ctor->Invoke( params );
	}

	// Create a component from a Maoli::Component
	Component^ Engine::CreateComponent( Maoli::Component* component )
	{
		// Get the class type name
		Maoli::String className = component->GetTypeName();
		className.Replace( "class Maoli::", "MaoliSharp." );
		String^ netClassName = gcnew String( className );

		// Get the managed constructor
		Type^ type = Type::GetType( netClassName );
		array<Type^>^ types = gcnew array<Type^>( 1 );
		types[0] = UnmanagedComponent::typeid;
		auto ctor = type->GetConstructor( types );
		array<Object^>^ params = gcnew array<Object^>( 1 );
		params[0] = gcnew UnmanagedComponent( component, _engine );
		return (Component^)ctor->Invoke( params );
	}

	// Delete an entity
	void Engine::DeleteEntity( Entity^ entity )
	{
		_entities->Remove( entity );
		_engine->DeleteEntity( entity->GetUnmanaged() );
	}

	// Rename an entity
	bool Engine::RenameEntity( Entity^ entity, String^ newName )
	{
		return _engine->RenameEntity( entity->GetUnmanaged(), Interop::StringToChar( newName ) );
	}

	// Save an entity to file
	void Engine::SaveEntity( Entity^ entity, String^ file )
	{
		_engine->SaveEntity( entity->GetUnmanaged(), Interop::StringToChar( file ) );
	}

	// Load an entity from file
	Entity^ Engine::LoadEntity( String^ file )
	{
		auto entity = _engine->LoadEntity( Interop::StringToChar( file ) );
		return (entity != nullptr) ? gcnew Entity( this, entity ) : nullptr;
	}

	// Load an entity from file
	Entity^ Engine::LoadEntityTemplate( String^ file )
	{
		auto entity = _engine->LoadEntityTemplate( Interop::StringToChar( file ) );
		return (entity != nullptr) ? gcnew Entity( this, entity ) : nullptr;
	}

	// Set the game world
	void Engine::SetWorld( World^ world )
	{
		ClearWorld();
		for ( int32 i = 0; i < world->Entities->Count; ++i )
			AddEntity( world->Entities[i] );
		_graphics->UpdateTerrain();
	}

	// Clear the game world
	void Engine::ClearWorld()
	{
		_entities->Clear();
		_renderableComponents->Clear();
		_engine->ClearWorld();
		_engine->RemovePendingEntities();
	}

	// Delete the game world
	void Engine::DeleteWorld()
	{
		_entities->Clear();
		_renderableComponents->Clear();
		_engine->DeleteWorld();
		_engine->RemovePendingEntities();
		_engine->DeletePendingEntities();
	}

	// Get a new message ID
	int32 Engine::CreateMessage( String^ name )
	{
		return Maoli::ComponentEvent::CreateEventID( Interop::StringToChar( name ) ).GetID();
	}

	// Get the name of a message
	String^ Engine::GetMessageName( int32 id )
	{
		return gcnew String( Maoli::ComponentEvent::GetEventName( id ) );
	}

	// Get an entity by name
	Entity^ Engine::GetEntity( String^ name )
	{
		for ( int32 i = 0; i < _entities->Count; ++i )
		{
			if ( _entities[i]->GetName() == name )
				return _entities[i];
		}
		return nullptr;
	}

	// Render lights for the editor
	void Engine::DrawLights()
	{
		_engine->GetGraphics()->GetDebugVisualizer()->DrawLights();
	}

	// Enable/disable physics
	void Engine::SetState( EngineState value )
	{
		_engine->SetState( (Maoli::EngineState)value );
	}

	// Enable or disable debug visualization for physics
	void Engine::EnablePhysicsVisualization( bool value )
	{
		_engine->GetPhysics()->EnableDebugRendering( value );
	}



}

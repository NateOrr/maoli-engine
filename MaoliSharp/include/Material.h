//--------------------------------------------------------------------------------------
// File: Material.h
//
// Exposes the material class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Material.h"

#pragma managed
#include "Interop.h"
using namespace System;
using namespace System::Runtime::InteropServices;

namespace MaoliSharp
{
	// Forward decl
	ref class Texture;

	// Texture types
	public enum class TextureType
	{
		BaseColor,
		NormalMap,
		Roughness,
		Metallic,
		Alpha,
		Emissive,

		None,
	};

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Material
	{
	public:
		// Ctor
		Material( Material^ mat );

		// Ctor
		Material( Maoli::MaterialBinding::pointer pMat );

		// Dtor
		~Material();

		// Gets the material it wraps
		inline Maoli::MaterialBinding::pointer GetUnmanaged( ) { return *_ptr; }

		// Tests if two materials are the same
		inline bool Equals( Material^ mat ) { return (_materialBinding == mat->_materialBinding); }

		// Export this data into a file
		void Export( String^ file );

		// Import from file
		void Import( String^ file );

		// Texture loading
		bool LoadTexture( System::String^ file, TextureType type );

		// Set a texture
		void SetTexture( Texture^ tex, TextureType type );

		// Get a texture
		Texture^ GetTexture( TextureType type );

		// Get a texture's name
		System::String^ GetTextureName( TextureType index );

		// Clear the texture
		void ClearTexture( TextureType type );

		// Load a texture set from the game pack
		bool LoadTextureSet( System::String^ file );

		// Convert to string
		virtual String^ ToString() override { return Name; }

		// Get the material file name
		String^ GetPreviewFile();


		// Roughness
		property float Roughness
		{
			inline float get() { return _materialBinding->GetMaterial()->GetRoughness(); }
			inline void set( float f ) { _materialBinding->GetMaterial()->SetRoughness( f ); }
		}

		// Metallic
		property float Metallic
		{
			inline float get() { return _materialBinding->GetMaterial()->GetMetallic(); }
			inline void set( float f ) { _materialBinding->GetMaterial()->SetMetallic( f ); }
		}

		// True if transparent
		property bool AlphaTesting
		{
			inline bool get() { return _materialBinding->GetMaterial()->IsAlphaTested(); }
			inline void set(bool value) { return _materialBinding->GetMaterial()->EnableAlphaTesting(value); }
		}

		// True if transparent
		property bool IsTransparent
		{
			inline bool get() { return _materialBinding->GetMaterial()->IsTransparent(); }
		}


		// Transparency
		property float Transparency
		{
			inline float get() { return _materialBinding->GetMaterial()->GetTransparency(); }
			inline void set( float f ) { _materialBinding->GetMaterial()->SetTransparency( f ); }
		}

		// Index into the material list
		property int32 Index
		{
			inline int32 get() { return _materialBinding->GetMaterial()->GetID(); }
		}

		// Base color
		property System::Drawing::Color BaseColor
		{
			System::Drawing::Color get()
			{
				return System::Drawing::Color::FromArgb( 255,
					(int32)(_materialBinding->GetMaterial()->GetBaseColor().x*255.0f),
					(int32)(_materialBinding->GetMaterial()->GetBaseColor().y*255.0f),
					(int32)(_materialBinding->GetMaterial()->GetBaseColor().z*255.0f) );
			}
			void set( System::Drawing::Color c ) { _materialBinding->GetMaterial()->SetBaseColor( (float)c.R / 255.0f, (float)c.G / 255.0f, (float)c.B / 255.0f ); }
		}

		// Emissive Color
		property System::Drawing::Color Emissive
		{
			System::Drawing::Color get()
			{
				return System::Drawing::Color::FromArgb( 255,
					(int32)(_materialBinding->GetMaterial()->GetEmissive().x*255.0f),
					(int32)(_materialBinding->GetMaterial()->GetEmissive().y*255.0f),
					(int32)(_materialBinding->GetMaterial()->GetEmissive().z*255.0f) );
			}
			void set( System::Drawing::Color c ) { _materialBinding->GetMaterial()->SetEmissive( (float)c.R / 255.0f, (float)c.G / 255.0f, (float)c.B / 255.0f, (float)c.A / 255.0f ); }
		}

		// Gets the name
		property System::String^ Name
		{
			inline System::String^ get() { return gcnew System::String( _materialBinding->GetMaterial()->GetName().c_str() ); }
			inline void set( String^ name ) { _materialBinding->GetMaterial()->Rename( Interop::StringToChar( name ) ); }
		}


	private:
		Maoli::MaterialBinding::pointer*	_ptr;
		Maoli::MaterialBinding*				_materialBinding;
	};
}

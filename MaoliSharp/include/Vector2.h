//--------------------------------------------------------------------------------------
// File: Vector2.h
//
// 2d Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
namespace MaoliSharp
{

	// 2D Vector
	public value class Vector2
	{
	public:
		// Constructors
		Vector2(const Maoli::Vector2& v) : x(v.x), y(v.y) {}
		Vector2(const Maoli::Vector3& v) : x(v.x), y(v.y) {}
		Vector2(float vx, float vy) : x(vx), y(vy) {}
		Vector2(float vx, float vy, float vz) : x(vx), y(vy) {}

		// Convert to d3d vectors
		Maoli::Vector2 AsMaoliVector() { return Maoli::Vector2( x, y ); }

		// Assign to d3d vectors
		void Set(const Maoli::Vector3& v){ x=v.x; y=v.y; }
		void Set(const Maoli::Vector2& v){ x=v.x; y=v.y; }

		// Subtraction
		static Vector2 operator-(Vector2 a, Vector2 b);
		static Vector2 operator-=(Vector2 v, Vector2 b);

		// Addition
		static Vector2 operator+(Vector2 a, Vector2 b);
		static Vector2 operator+=(Vector2 v, Vector2 b);

		// Scalar mult
		static Vector2 operator*(Vector2 v, float f);
		static Vector2 operator*=(Vector2 v, float f);

		// Scalar div
		static Vector2 operator/(Vector2 v, float d);
		static Vector2 operator/=(Vector2 v, float f);

		// Dot product
		float Dot(Vector2 v);
		static float operator*(Vector2 a, Vector2 b);

		// Length
		float Length();

		// Normalize the vector
		Vector2 Normalize();

		float x, y;
	};

}

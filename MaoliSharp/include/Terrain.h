//--------------------------------------------------------------------------------------
// File: Terrain.h
//
// Exposes the Terrain class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/Terrain.h"

#pragma managed
#include "Material.h"
#include "Component.h"
#include "Dependency.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;
using namespace System::ComponentModel;
using namespace Honua;

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Terrain
	{
	public:

		// Ctor
		Terrain( Maoli::Terrain* terrain );

		// Gets a file dialog filter for loading
		static inline String^ GetFileFilter()
		{
			return "Terrain Files|*.dds;*.jpg;*.png;*.tga;*.terrain";
		}

		// Get the unmanaged object
		inline  Maoli::Terrain * GetUnmanaged() { return _terrain; }

		// Returns true if the meshes are identical
		inline bool Equals( Terrain^ m ) { return _terrain == m->GetUnmanaged(); }

		// Check for a ray intersection with the terrain
		bool RayIntersect(Vector3 pos, Vector3 dir, float% dist);

		// Import a heightmap
		bool ImportHeightmap( System::String^ file );

		// Import a blendmap
		bool ImportBlendmap( System::String^ file );

		// Save the current terrain to file
		void ExportHeightmap( System::String^ file );

		// Save the current blend map to file
		void ExportBlendmap( System::String^ file );

		// Create a new terrain
		bool CreateHeightmap( uint32 size );

		// Get heightmap size
		uint32 GetDimensions();

		// The scale for the current terrain
		property float SizeScale
		{
			inline float get();
			inline void set( float value );
		}

		// The scale for the current terrain
		property float HeightScale
		{
			inline float get();
			inline void set( float value );
		}

		// Sculpting radius
		property float Radius
		{
			inline float get();
			inline void set( float value );
		}

		// Sculpting hardness
		property float Hardness
		{
			inline float get();
			inline void set( float value );
		}

		// Sculpting radius
		property float Strength
		{
			inline float get();
			inline void set( float value );
		}

		// Set the default material layer for the terrain
		void SetMaterial( uint32 layer, Material^ mat );

		// Get the default material layer for the terrain
		Material^ GetMaterial( uint32 layer );

	private:
		Maoli::Terrain*				_terrain;			// Unmanaged object to wrap
		array<Material^>^			_materials;			// Layer materials
	};
}

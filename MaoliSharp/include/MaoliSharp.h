//--------------------------------------------------------------------------------------
// File: ManagedStdafx.h
//
// Globals for now, this entire file should not exist
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Stranded.h"

#pragma managed
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Matrix.h"
#pragma unmanaged

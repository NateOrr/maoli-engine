//--------------------------------------------------------------------------------------
// File: Math.cpp
//
// Math functions
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Math.h"

namespace MaoliSharp
{

	// Ray-Box intersection
	bool Math::RayBoxIntersect( Vector3 pos, Vector3 dir, Vector3 boxPosition, Vector3 boxSize, float% dist )
	{
		float d;
		bool result = Maoli::Math::RayBoxIntersect( Maoli::Vector3( pos.x, pos.y, pos.z ),
			Maoli::Vector3( dir.x, dir.y, dir.z ),
			Maoli::Vector3( boxPosition.x, boxPosition.y, boxPosition.z ),
			Maoli::Vector3( boxSize.x, boxSize.y, boxSize.z ), d );
		if ( result )
			dist = d;
		return result;
	}


	// Performs a ray-sphere intersect test
	bool Math::RaySphereIntersect( Vector3 pos, Vector3 dir, Vector3 center, float radius )
	{
		// 		Vector3 vPos(pos.x, pos.y, pos.z);
		// 		Vector3 vDir(dir.x, dir.y, dir.z);
		// 		Vector3 vCenter(center.x, center.y, center.z);
		// 		return D3DXSphereBoundProbe(&vCenter, radius, &vPos, &vDir)==TRUE;
		M_ASSERT( false, "NOT_IMPLEMENTED" );
		return false;
	}


	// Performs a ray-entity intersect test
	bool Math::RayEntityIntersect( Vector3 pos, Vector3 dir, Entity^ e )
	{
		//return RayBoxIntersect(pos, dir, e->Position-e->Bounds, e->Position+e->Bounds);
		M_ASSERT( false, "NOT_IMPLEMENTED" );
		return false;
	}

	// Box-Sphere interselction
	bool Math::BoxSphereIntersect( Vector3 boxCenter, Vector3 boxSize, Vector3 sphereCenter, float radius )
	{
		return Maoli::Math::BoxSphereIntersect(
			boxCenter.AsMaoliVector(), boxSize.AsMaoliVector(), sphereCenter.AsMaoliVector(), radius );
	}

}

//--------------------------------------------------------------------------------------
// File: Platform.cpp
//
// OS wrapper
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Platform.h"

namespace MaoliSharp
{
	// Ctor
	Platform::Platform( Maoli::Platform* input )
	{
		_platform = input;
	}

}

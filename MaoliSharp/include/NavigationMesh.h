//--------------------------------------------------------------------------------------
// File: NavigationMesh.h
//
// Exposes the NavMesh class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
namespace Maoli { class NavMesh; }

#pragma managed
#include "Component.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class NavMesh : public Component
	{
	public:

		// Ctor
		NavMesh( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "Navigation mesh for pathfinding";
		}

		// Get the unmanaged Light object
		inline Maoli::NavMesh* GetUnmanaged( ) { return _navMesh; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( NavMesh^ mesh ) { return (_navMesh == mesh->_navMesh); }

		// Lets the editors know if this component is file based
		virtual bool FileBased() override { return true; }

		// Gets a file dialog filter for loading
		virtual String^ GetFileFilter() override
		{
			return "All Files | *.*;";
		}

		// Load from file
		virtual bool Load( System::String^ fileName ) override;

	private:

		Maoli::NavMesh*		_navMesh;
	};
}
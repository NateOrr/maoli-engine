//--------------------------------------------------------------------------------------
// File: Transform.cpp
//
// Exposes the Transform class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Transform.h"
#include "Engine.h"
#include "Interop.h"

namespace MaoliSharp
{
	// Ctor
	Transform::Transform( UnmanagedComponent^ component ) : Component( component )
	{
		_transform = (Maoli::Transform*)_component;
	}

}

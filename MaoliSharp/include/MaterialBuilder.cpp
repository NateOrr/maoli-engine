//--------------------------------------------------------------------------------------
// File: MaterialBuilder.cpp
//
// Used for building material database using minimal memory
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"
#include "Maoli.h"

#pragma managed
#include "MaterialBuilder.h"
#include "Material.h"
#include "Interop.h"
#include "Renderer.h"

namespace MaoliSharp
{
	// Ctor
	MaterialBuilder::MaterialBuilder( Renderer^ graphics )
	{
		_graphics = graphics->GetUnmanaged();
		_material = new Maoli::Material( _graphics->GetEngine() );
		_disposed = false;
	}

	// Dtor
	MaterialBuilder::~MaterialBuilder()
	{
		if ( !_disposed )
		{
			_disposed = true;
			this->!MaterialBuilder();
		}
	}

	// Finalizer
	MaterialBuilder::!MaterialBuilder()
	{

	}

	// Add a material texture file
	void MaterialBuilder::AddTexture( String^ texture, TextureType type )
	{
		if ( texture != nullptr )
			_material->SetTextureName( Interop::StringToChar( _directory + "\\" + texture ), Maoli::TEX_TYPE( type ) );
	}

	// Get a texture file name
	String^ MaterialBuilder::GetTexture( TextureType type )
	{
		if ( _material->GetTextureFileNames()[(int32)type] != Maoli::String::none )
			return gcnew String( _material->GetTextureFileNames()[(int32)type] );
		return nullptr;
	}

	// Save to file
	void MaterialBuilder::Export( String^ file )
	{
		_material->Save( Interop::StringToChar( file ) );
	}

	// Reset this material
	void MaterialBuilder::Create( String^ name )
	{
		_material = Maoli::Material::Create( Interop::StringToChar( name ), _graphics->GetEngine() );
	}

	// Create a material binding for this material
	Material^ MaterialBuilder::CreateBinding()
	{
		return gcnew Material( _graphics->CreateMaterialBinding( _material ) );
	}



}
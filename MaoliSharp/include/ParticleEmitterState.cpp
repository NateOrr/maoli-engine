//--------------------------------------------------------------------------------------
// File: ParticleEmitter
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
using namespace System;
#include "ParticleEmitterState.h"
#include "ParticleSet.h"
#include "Texture.h"

namespace MaoliSharp
{
	// Ctor
	ParticleEmitterState::ParticleEmitterState( Maoli::ParticleEmitterState* state )
	{
		_state = state;
		_sets = gcnew List<ParticleSet^>();
		_nextState = nullptr;
		for ( uint32 i = 0; i < _state->GetNumParticleSets(); ++i )
			_sets->Add( gcnew ParticleSet( _state->GetParticleSet( i ) ) );
	}

	// Add a new state
	void ParticleEmitterState::AddSet( ParticleSet^ set )
	{
		_sets->Add( set );
		_state->AddParticleSet( set->GetUnmanaged() );
	}

	// Add an existing state
	void ParticleEmitterState::AddSet( ParticleSet^ set, int32 index )
	{
		_sets->Insert( index, set );
		_state->AddParticleSet( set->GetUnmanaged() );
	}

	// Remove a state
	void ParticleEmitterState::RemoveSet( ParticleSet^ set )
	{
		_sets->Remove( set );
		_state->RemoveParticleSet( set->GetUnmanaged() );
	}


	// The next state to transition to
	void ParticleEmitterState::NextState::set( ParticleEmitterState^ state )
	{
		_nextState = state;
		_state->SetNextState( (state != nullptr) ? state->GetUnmanaged() : nullptr );
	}

}
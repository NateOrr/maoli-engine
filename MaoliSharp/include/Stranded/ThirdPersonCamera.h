//--------------------------------------------------------------------------------------
// File: ThirdPersonCamera.h
//
// Exposes the camera class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
#include "..\Camera.h"
using namespace System;
using namespace System::Runtime::InteropServices;

// Forward decl
namespace Maoli
{
	class ThirdPersonCamera;
}

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class ThirdPersonCamera : public Camera
	{
	public:

		// Ctor
		ThirdPersonCamera( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "A user-controlled third person view camera that locks to a target entity";
		}

		// Target
		property Entity^ Target
		{
			Entity^ get();
			void set( Entity^ value );
		}

	private:

		Maoli::ThirdPersonCamera*	_thirdPersonCamera;
		Entity^ _target;
	};
}

//--------------------------------------------------------------------------------------
// File: ThirdPersonCamera.h
//
// Exposes the camera class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"
#include "Components\ThirdPersonCamera.h"

#pragma managed
#include "ThirdPersonCamera.h"
#include "..\Engine.h"
#include "..\Interop.h"
#include "..\Entity.h"

namespace MaoliSharp
{
	// Ctor
	ThirdPersonCamera::ThirdPersonCamera( UnmanagedComponent^ component ) : Camera( component )
	{
		_thirdPersonCamera = (Maoli::ThirdPersonCamera*)_component;
	}


	// Target
	Entity^ ThirdPersonCamera::Target::get()
	{
		return _target;
	}

	// Target
	void ThirdPersonCamera::Target::set( Entity^ value )
	{
		_target = value;
		_thirdPersonCamera->SetCamTarget( _target->GetUnmanaged() );
	}


}

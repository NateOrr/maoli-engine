//--------------------------------------------------------------------------------------
// File: Entity.cpp
//
// Game object
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Entity.h"
#include "Engine.h"
#include "Component.h"
#include "Interop.h"
#include "Model.h"
#include "Light.h"
#include "Transform.h"
using namespace System::Reflection;

namespace MaoliSharp
{
	// Ctor
	Entity::Entity( String^ name, MaoliSharp::Engine^ engine )
	{
		_entity = engine->GetUnmanaged()->CreateEntity( Interop::StringToChar( name ) );
		_engine = engine;
		_disposed = false;
		BuildComponentList();
	}

	// Ctor
	Entity::Entity( MaoliSharp::Engine^ engine, Maoli::Entity* entity )
	{
		_entity = entity;
		_engine = engine;
		_disposed = false;
		BuildComponentList();
	}

	// Ctor
	Entity::Entity( String^ name, MaoliSharp::Engine^ engine, bool isTemplate )
	{
		if ( isTemplate )
			_entity = engine->GetUnmanaged()->CreateEntityTemplate( Interop::StringToChar( name ) );
		else
			_entity = engine->GetUnmanaged()->CreateEntity( Interop::StringToChar( name ) );
		_engine = engine;
		_disposed = false;
		BuildComponentList();
	}

	// Dtor
	Entity::~Entity()
	{
		if ( !_disposed )
		{
			_disposed = true;
			this->!Entity();
		}
	}

	// Finalizer
	Entity::!Entity()
	{
		_entity->GetEngine()->DeleteEntity( _entity );
	}

	// Build the component list
	void Entity::BuildComponentList()
	{
		_components = gcnew List<Component^>();
		const Maoli::Entity::ComponentMap& componentMap = _entity->GetComponentMap();
		for ( auto iter = componentMap.begin(); iter != componentMap.end(); ++iter )
		{
			auto component = _engine->CreateComponent( iter->second );
			component->Owner = this;
			_components->Add( component );
		}

	}

	// Add a component
	bool Entity::AddComponent( Component^ component )
	{
		_components->Add( component );
		component->Owner = this;
		_engine->RegisterComponent( component );
		return _entity->AddComponent( component->GetUnmanaged() );
	}

	// Remove a component
	void Entity::RemoveComponent( Component^ component )
	{
		_components->Remove( component );
		_entity->RemoveComponent( component->GetUnmanaged() );
		_engine->UnregisterComponent( component );
		component->Owner = nullptr;
	}

	// Get a component by type
	generic <typename T> where T : Component T Entity::GetComponent()
	{
		Type^ type = T::typeid;
		Maoli::String typeName = Interop::StringToChar( type->FullName );
		typeName.Replace( "MaoliSharp.", "class Maoli::" );
		for ( int32 i = 0; i < _components->Count; ++i )
		{
			if ( _components[i]->GetUnmanaged()->GetTypeName() == typeName )
				return (T)_components[i];
		}
		return T();
	}

	// Clone the entity
	Entity^ Entity::Clone()
	{
		return gcnew Entity( _engine, _entity->Clone() );
	}

	// Get the bounding box of an entity
	void Entity::GetBounds( [Out] Vector3% center, [Out] Vector3% size )
	{
		// Get model min/max, otherwise its just zero
		auto model = GetComponent<Model^>();
		auto light = GetComponent<Light^>();
		auto transform = GetComponent<Transform^>();
		if ( !transform )
			return;
		Vector3 max = transform->Position + Vector3( 0.5f, 0.5f, 0.5f );
		Vector3 min = transform->Position - Vector3( 0.5f, 0.5f, 0.5f );
		if ( model )
		{
			max = Vector3::Max( max, model->BoundCenter + model->BoundSize );
			min = Vector3::Min( min, model->BoundCenter - model->BoundSize );
		}
		if ( light )
		{
			max = Vector3::Max( max, light->Position + transform->Position + Vector3( 0.5f, 0.5f, 0.5f ) );
			min = Vector3::Min( min, light->Position + transform->Position - Vector3( 0.5f, 0.5f, 0.5f ) );
		}
		center = (max + min) * 0.5f;
		size = max - center;
	}

	// Get the center of an entity
	Vector3 Entity::GetPosition()
	{
		Transform^ transform = GetComponent<Transform^>();
		if ( transform )
			return transform->Position;
		return Vector3( 0, 0, 0 );
	}

}

//--------------------------------------------------------------------------------------
// File: Vector3.h
//
// 3D Vector3
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

namespace MaoliSharp
{

	// 3D Vector
	public value class Vector3
	{
	public:
		// Constructors
		Vector3(const Maoli::Vector4& v) : x(v.x), y(v.y), z(v.z) {}
		Vector3(const Maoli::Vector3& v) : x(v.x), y(v.y), z(v.z) {}
		Vector3(float vx, float vy, float vz) : x(vx), y(vy), z(vz) {}
		Vector3(float vx, float vy, float vz, float vw) : x(vx), y(vy), z(vz) {}

		// Convert to d3d vectors
		Maoli::Vector3 AsMaoliVector(){ return Maoli::Vector3(x, y, z); }

		// Assign to d3d vectors
		void Set(const Maoli::Vector3& v){ x=v.x; y=v.y; z=v.z; }
		void Set(const Maoli::Vector4& v){ x=v.x; y=v.y; z=v.z; }

		// Convert to a color
		System::Drawing::Color ToColor(); 

		// Negation
		Vector3 operator-();

		// Component wise abs
		Vector3 Absolute();

		// Subtraction
		static Vector3 operator-(Vector3 a, Vector3 b);
		static Vector3 operator-=(Vector3 v, Vector3 b);

		// Addition
		static Vector3 operator+(Vector3 a, Vector3 b);
		static Vector3 operator+=(Vector3 v, Vector3 b);

		// Scalar mult
		static Vector3 operator*(Vector3 v, float f);
		static Vector3 operator*=(Vector3 v, float f);

		// Scalar div
		static Vector3 operator/(Vector3 v, float d);
		static Vector3 operator/=(Vector3 v, float f);

		// Dot product
		float Dot(Vector3 v);
		static float operator*(Vector3 a, Vector3 b);

		// Cross product of the 3d components
		Vector3 Cross(Vector3 v);

		// Component wise max
		static Vector3 Max( Vector3 a, Vector3 b );

		// Component wise min
		static Vector3 Min( Vector3 a, Vector3 b );

		// Length
		float Length();

		// Normalize the vector
		Vector3 Normalize();

		float x, y, z;
	};

}

//--------------------------------------------------------------------------------------
// File: Component.cpp
//
// Game object component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Component.h"

namespace MaoliSharp
{

	// Ctor
	Component::Component( UnmanagedComponent^ component )
	{
		_entity = nullptr;
		_component = component->component;
		_engine = component->engine;
	}

	// Dtor
	Component::~Component()
	{

	}

	// Convert to string
	String^ Component::ToString()
	{
		return GetTypeName()->Replace( "MaoliSharp.", "" );
	}

	// Register a dependency
	void Component::AddDependency( Component^ target )
	{
		_component->AddDependency( target->GetUnmanaged() );
	}

	// Remove a dependency
	void Component::RemoveDependency( Component^ target )
	{
		_component->RemoveDependency( target->GetUnmanaged() );
	}

	// Gets the component typename
	String^ Component::GetTypeName()
	{
		return gcnew String( _component->GetTypeName().Replace( "class Maoli::", "" ) );
	}

	// Custom rendering
	void Component::Render()
	{

	}


}

//--------------------------------------------------------------------------------------
// File: LightProbe.cpp
//
// Image based light probe
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "LightProbe.h"
#include "Interop.h"

namespace MaoliSharp
{

	// Ctor
	LightProbe::LightProbe( Maoli::EnvironmentProbe* probe )
	{
		_probe = probe;
	}


	// Load a cubemap
	bool LightProbe::LoadCubemap( String^ file )
	{
		return SUCCEEDED(_probe->LoadCubemap(Interop::StringToChar(file)));
	}

}

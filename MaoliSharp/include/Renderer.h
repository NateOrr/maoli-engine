//--------------------------------------------------------------------------------------
// File: Renderer.h
//
// Direct3D 11 Graphics Engine Wrapper
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/Renderer.h"
#include "Graphics/Terrain.h"
#include "Graphics/DebugVisualizer.h"
#include "Engine\Engine.h"

#pragma managed
#include "Game/Component.h"
#include "Light.h"
#include "Model.h"
#include "Camera.h"
#include "LightProbe.h"
#include "Texture.h"
#include "Water.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace MaoliSharp
{

	// Forward decl
	ref class Engine;
	ref class Terrain;

	// Terrain rendering modes
	public enum class TerrainMode
	{
		Blendmap,		// Single blend texture map
		VT,				// Full virtual texturing
		BlendVT,		// Blend map stored as a virtual texture
	};

	// Virtual texture filter modes
	public enum class VTFilter
	{
		None,
		Linear,
		Anisotropic,
	};

	// Sculpting modes for terrain
	public enum class SculptMode
	{
		Raise = Maoli::SCULPT_RAISE,
		Lower = Maoli::SCULPT_LOWER,
		Grab = Maoli::SCULPT_GRAB,
		Smooth = Maoli::SCULPT_SMOOTH,
		Ramp = Maoli::SCULPT_RAMP,
		Noise = Maoli::SCULPT_NOISE,
		Erosion = Maoli::SCULPT_EROSION,
		Paint = Maoli::SCULPT_PAINT,
	};


	// Managed wrapper for the unmanaged renderer
	// This is a class, do not try to make an instance
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Renderer
	{
	public:

		// Ctor
		Renderer( Engine^ engine );

		// Get a pointer to the unmanaged graphics object
		inline Maoli::Renderer* GetUnmanaged() { return _graphics; }

		// Cleanup
		void Release();

		// Resize the backbuffer
		void Resize();

		// Update the scene
		void Update();

		// Render the scene
		void Render();

		// Reload all shaders
		void ReloadEffect() { _graphics->ReloadEffect(); }

		// Destroy the scene
		void ClearScene();

		// Optimize the scene layout
		void OptimizeScene();


		// Saves a screenshot
		inline void TakeScreenShot() { _graphics->TakeScreenShot(); }

		// Logs a message to file
		inline void Log( System::String^ msg ) { /*Maoli::Log::_Print(Interop::StringToChar(msg));*/ }




		///////////////////////////////////////
		// Properties and accessors

		// Get the screen dimensions
		inline int32 GetWidth() { return _graphics->GetWidth(); }
		inline int32 GetHeight() { return _graphics->GetHeight(); }

		// Mouse coords
		inline Vector2 GetMousePosition() { return Vector2( (float)_engine->GetPlatform()->GetMouseLocation().x, (float)_engine->GetPlatform()->GetMouseLocation().y ); }

		// Returns true if the mouse is within the screen range
		inline bool MouseOnScreen() { return _graphics->MouseOnScreen(); }

		// Get the total polygons in this scene
		inline int32 GetScenePolyCount() { return _graphics->GetPolyCount(); }

		// Set the time of day
		inline void SetTimeOfDay( int32 hours, int32 minutes, int32 seconds ) { _graphics->SetTimeOfDay( hours, minutes, seconds ); }
		property float TimeOfDay
		{
			inline void set( float f ) { _graphics->SetTimeOfDay( (int32)f, 0, 0 ); }
			inline float get() { return (float)_graphics->GetTimeOfDayHours(); }
		}

		// Set the ambient color
		inline void SetAmbient( float r, float g, float b ) { _graphics->SetAmbientLighting( r, g, b ); }

		// Sun shadow softness
		property float SunShadowSoftness
		{
			inline void set( float f ) { _graphics->SetSunShadowSoftness( (int32)f ); }
			inline float get() { return (float)_graphics->GetSunShadowSoftness(); }
		}

		// Set the date
		inline void SetDate( int32 month, int32 day, int32 year ) { _graphics->SetDate( month, day, year ); }

		// Enable/disable the sky rendering
		inline void EnableSky( bool b ) { _graphics->EnableSky( b ); }

		// Enable / disable sun shadows
		inline void EnableSunShadows( bool value ) { _graphics->EnableSunShadows( value ); }

		// Enable/disable the ocean
		inline void EnableOcean( bool b ) { _graphics->EnableOcean( b ); }

		inline Camera^ GetCamera() { return gcnew Camera( gcnew UnmanagedComponent( _graphics->GetCamera(), _engine ) ); }

		// Load a skybox
		inline bool LoadSkybox( String^ file ) { return _graphics->LoadSkyboxCubemap( Interop::StringToChar( file ) ); }

		// VSync
		inline void EnableVSync( bool value ) { _graphics->EnableVSync( value ); }

		// Check if the mouse is over the terrain
		inline bool MouseOnTerrain(){ return _graphics->MouseOnTerrain(); }

		// Get the mouse pick location on the terrain
		Vector3 GetTerrainPickPoint();

		// Get the mouse pick point with the scene
		Vector3 GetPickPoint();
		Vector3 GetPickPoint(Model^ modelToIgnore);


		///////////////////////////////////////
		// Light probes

		// Create a new probe
		LightProbe^ AddLightProbe( String^ file );

		// Remove a probe
		void RemoveProbe( LightProbe^ probe );


		///////////////////////////////////////
		// Textures

		// Load a texture from file
		Texture^ LoadTexture( String^ file );


		///////////////////////////////////////
		// Meshes

		// Load a mesh from file
		Model^ LoadMesh( System::String^ file );

		// Delete a mesh from the scsne
		void DeleteMesh( Model^ mesh );



		///////////////////////////////////////
		// Materials

		// Get the number of materials
		inline int32 GetNumMaterials() { return Maoli::Material::GetCount(); }

		// Get a material by index
		inline Material^ GetMaterial( int32 i ) { return gcnew Material( _graphics->CreateMaterialBinding( Maoli::Material::Get( i ) ) ); }

		// Create a new material
		Material^ CreateMaterial( System::String^ name );

		// Load a material from file
		Material^ LoadMaterial( String^ file );

		// Permanently delete a material
		void RemoveMaterial( Material^ mat );

		// Blend two materials together
		void BlendMaterials( Material^ blendBase, Material^ blendLayer, Texture^ blendMask, Material^ result );

		// Compute a normal map from a bump map
		Texture^ ComputeNormalMap( Texture^ bumpMap );

		// Store height in alpha of a normal map
		Texture^ StoreHeightInNormalMap( Texture^ normalMap, Texture^ heightMap );




		///////////////////////////////////////
		// Terrains

		// Returns true if the scene has an active terrain
		inline bool HasTerrain() { return _graphics->GetTerrain() != nullptr; }

		// Load in a heightmap
		Terrain^ LoadHeightmap( String^ file );

		// Save the heightmap to file
		void SaveHeightmap( String^ file );

		// Create a blank terrain
		Terrain^ CreateTerrain( uint32 size );

		// Get the terrain
		inline Terrain^ GetTerrain() { return _terrain; }

		// Update the terrain after map loading
		void UpdateTerrain();

		// Delete the terrain
		void ClearTerrain();

		// Show / hide terrain
		inline void ShowTerrain( bool show ) { _graphics->ShowTerrain( show ); }


		///////////////////////////////////////
		// Water
		inline Ocean^ GetOcean() { return gcnew Ocean( _graphics->GetOcean() ); }



		///////////////////////////////////////
		// Preview image rendering

		// Save a material preview texture
		void MakeMaterialPreview( Material^ mat )
		{
			_visualizer->DrawMaterialPreview( mat->GetUnmanaged() );
		}

		// Save a mesh preview texture
		System::String^ MakeMeshPreview( Model^ mesh )
		{
			return gcnew System::String( _visualizer->RenderMeshPreview( mesh->GetUnmanaged() ).c_str() );
		}


		///////////////////////////////////////
		// Editor stuff

		// Clear debug render list
		void ClearDebugRenderList();

		// Render an object in debug mode
		void DebugRenderMesh( Model^ mesh, int32 submesh, bool submeshOnly, bool wireframe, bool boundBox );
		void DebugRenderMesh( Model^ mesh, Vector3 color, bool ignoreDepth );
		void DebugRenderMesh( Model^ mesh, Vector3 color, Vector3 pos, Vector3 rot, Vector3 scale, bool ignoreDepth );
		void DebugRenderLight( Light^ light );

		// Render a line
		void DebugRenderLine( Vector3 pos, Vector3 rot, Vector3 scale, Vector3 color, bool ignoreDepth );

		// Render a box
		void DebugRenderBox( Vector3 pos, Vector3 size, Vector3 color );


		// Render the screen selection box
		void RenderSelectionBox( float x1, float y1, float x2, float y2 );

		// Grid lines
		property bool DrawGrid
		{
			inline bool get() { return _visualizer->IsGridEnabled(); }
			inline void set( bool b ) { _visualizer->EnableGridLines( b ); }
		}



		///////////////////////////////////////
		// Virtual texture painting
		inline void AttachLayerMaterial( Material^ mat, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->AttachMaterial( mat->GetUnmanaged(), i ); }
		System::String^ GetLayerMask( int32 i );
		inline void ClearLayerMask( int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->ClearLayerMask( i ); }
		inline void LoadLayerMask( System::String^ file, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->LoadLayerMask( Interop::StringToChar( file ), i ); }
		inline void SetLayerPaintMode( int32 mode, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->SetPaintMode( (Maoli::PAINT_TYPE)mode, i ); }
		inline void SetLayerMaskMode( int32 mode, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->SetMaskMode( (Maoli::PAINT_TYPE)mode, i ); }
		inline void SetLayerMaskInvert( bool b, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->SetMaskInvert( b, i ); }
		inline void SetLayerMaskInvertColor( bool b, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->SetMaskInvertColor( b, i ); }
		inline void SetLayerMaskAbsolute( bool b, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->SetMaskAbsolute( b, i ); }
		inline void SetLayerMaskColor( bool b, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->SetMaskColor( b, i ); }
		inline void SetLayerMaskModulate( bool b, int32 i ) { if ( HasTerrain() ) _graphics->GetTerrain()->SetMaskModulate( b, i ); }
		inline bool GetLayerMaskInvert( int32 i ) { if ( HasTerrain() ) return _graphics->GetTerrain()->GetMaskInvert( i ); return false; }
		inline bool GetLayerMaskInvertColor( int32 i ) { if ( HasTerrain() ) return _graphics->GetTerrain()->GetMaskInvertColor( i ); return false; }
		inline bool GetLayerMaskAbsolute( int32 i ) { if ( HasTerrain() ) return _graphics->GetTerrain()->GetMaskAbsolute( i ); return false; }
		inline bool GetLayerMaskColor( int32 i ) { if ( HasTerrain() ) return _graphics->GetTerrain()->GetMaskColor( i ); return false; }
		inline bool GetLayerMaskModulate( int32 i ) { if ( HasTerrain() ) return _graphics->GetTerrain()->GetMaskModulate( i ); return false; }
		inline int32 GetLayerPaintMode( int32 i ) { if ( HasTerrain() ) return (int32)_graphics->GetTerrain()->GetPaintMode( i ); return 0; }
		inline int32 GetLayerMaskMode( int32 i ) { if ( HasTerrain() ) return (int32)_graphics->GetTerrain()->GetMaskMode( i ); return 0; }
		inline void SetActiveLayer( int32 i ) { if ( HasTerrain() ) { _visualizer->SetTerrainWidgetDetail( i ); } }





		///////////////////////////////////////
		// Terrain widgets and sculpting
		inline void SculptTerrain() { _visualizer->QueueTerrainSculpt(); }
		inline void SetTerrainWidgetDelta( float f ) { _visualizer->SetTerrainWidgetDelta( f ); }
		inline void SetTerrainWidgetRadius( float f ) { _visualizer->SetTerrainWidgetRadius( f ); }
		inline void SetTerrainWidgetHardness( float f ) { _visualizer->SetTerrainWidgetHardness( f ); }
		inline void SetTerrainWidgetStrength( float f ) { _visualizer->SetTerrainWidgetStrength( f ); }
		inline void SetTerrainSculptMode( SculptMode mode ) { _visualizer->SetTerrainSculptMode( (Maoli::SCULPT_TYPE)mode ); }
		inline void UndoSculpt() { if ( _graphics->GetTerrain() ) _graphics->GetTerrain()->UndoSculpt(); }
		inline void RedoSculpt() { if ( _graphics->GetTerrain() ) _graphics->GetTerrain()->RedoSculpt(); }

		// Draw the terrain widget
		void DrawTerrainWidget();

		// Gets the ray that corresponds to the 2d screen position
		void GetPickRay( int32 x, int32 y, [Out]Vector3% oPos, [Out]Vector3% oDir );

		// Gets the ray that corresponds to the 2d screen position
		void GetPickRay( [Out]Vector3% oPos, [Out]Vector3% oDir );



	private:


		// Main renderer object
		Maoli::Engine* _engine;
		Maoli::Renderer* _graphics;
		Maoli::DebugVisualizer* _visualizer;
		Terrain^ _terrain;
		bool _needsDelete;
	};

}

//--------------------------------------------------------------------------------------
// File: MaterialBuilder.h
//
// Used for building material database using minimal memory
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged

// Forward decl
namespace Maoli
{
	class Material;
	class Renderer;
}

#pragma managed
#include "Material.h"

namespace MaoliSharp
{
	// Forward decl
	ref class Material;
	ref class Renderer;

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class MaterialBuilder
	{
	public:

		// Ctor
		MaterialBuilder( Renderer^ graphics );

		// Dtor
		~MaterialBuilder();

		// Finalizer
		!MaterialBuilder();

		// Add a material texture file
		void AddTexture( String^ texture, TextureType type );

		// Get a texture file name
		String^ GetTexture( TextureType type );

		// Save to file
		void Export( String^ file );

		// Reset this material
		void Create(String^ name);

		// Set the current directory
		inline void SetDirectory( String^ dir ) { _directory = dir; }

		// Create a material binding for this material
		Material^ CreateBinding();

	private:

		Maoli::Renderer*				_graphics;
		Maoli::Material*				_material;
		bool							_disposed;
		String^							_directory;
	};
}
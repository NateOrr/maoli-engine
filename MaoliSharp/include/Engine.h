//--------------------------------------------------------------------------------------
// File: Engine.h
//
// Direct3D 11 Game Engine Wrapper
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
// Forward decl
namespace Maoli { class Engine; class Game; }

#pragma managed
using namespace System;
using namespace System::Collections::Generic;
#include "Component.h"

namespace MaoliSharp
{
	// Forward decl
	ref class Renderer;
	ref class Platform;
	ref class Entity;
	ref class Camera;
	ref class Transform;
	ref class FirstPersonCamera;
	ref class World;

	// Engine states
	public enum class EngineState
	{
		Default,
		Editor,
		Paused,
	};

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Engine
	{
	public:
		// Ctor
		Engine();

		// Get access to the unmanaged pointer
		inline Maoli::Engine* GetUnmanaged() { return _engine; }

		// Setup the engine
		bool Init( IntPtr mainWindow, IntPtr renderWindow );

		// Cleanup
		void Release();

		// Get access to the graphics engine
		property Renderer^ Graphics
		{
			inline Renderer^ get() { return _graphics; }
		}

		// Get access to the input
		property MaoliSharp::Platform^ Platform
		{
			inline MaoliSharp::Platform^ get() { return _platform; }
		}

		// Frame update
		void Update();

		// Render using the primary renderer
		void Render();


		// Set the game world
		void SetWorld( World^ world );

		// Clear the game world
		void ClearWorld();

		// Delete the game world
		void DeleteWorld();

		// Time of last frame
		inline float GetElapsedTime() { return _engine->GetFrameTime(); }

		// Get the frames per second
		inline int32 GetFPS() { return _engine->GetFPS(); }

		// Sets the working directory
		inline String^ GetWorkingDirectory() { return _workingDirectory; }

		// Create a new entity
		Entity^ CreateEntity( String^ name );
		Entity^ CreateEntityTemplate( String^ name );

		// Delete an entity
		void DeleteEntity( Entity^ entity );

		// Rename an entity
		bool RenameEntity( Entity^ entity, String^ newName );

		// Add an entity to the world
		void AddEntity( Entity^ entity );

		// Remove an entity from the world
		void RemoveEntity( Entity^ entity );

		// Save an entity to file
		void SaveEntity( Entity^ entity, String^ file );

		// Load an entity from file
		Entity^ LoadEntity( String^ file );
		Entity^ LoadEntityTemplate( String^ file );

		// Get an entity by name
		Entity^ GetEntity( String^ name );

		// Get the entities
		property List<Entity^>^ Entities
		{
			inline List<Entity^>^ get() { return _entities; }
		}


		// Create a component based on type
		generic <typename T> where T : Component
			T CreateComponent();

		// Create a component from type
		Component^ CreateComponent( Type^ type );

		// Create a component from a Maoli::Component
		Component^ CreateComponent( Maoli::Component* component );

		// Register a component with the engine
		void RegisterComponent( Component^ component );

		// Unregister a component with the engine
		void UnregisterComponent( Component^ component );

		// Enable/disable physics
		void SetState( EngineState value );

		// Get a new message ID
		int32 CreateMessage( String^ name );

		// Get the name of a message
		String^ GetMessageName( int32 id );

		// Enable or disable debug visualization for physics
		void EnablePhysicsVisualization( bool value );

		// Render lights for the editor
		void DrawLights();
	private:

		Maoli::Game*			_game;					// Unmanaged game system
		Maoli::Engine*			_engine;				// Unmanaged engine
		Renderer^				_graphics;				// Main renderer
		MaoliSharp::Platform^	_platform;				// OS wrapper
		String^					_workingDirectory;		// Working directory
		List<Entity^>^			_entities;				// Entities in the game world
		List<Component^>^		_renderableComponents;	// Components in the world that have a render function
	};
}


//--------------------------------------------------------------------------------------
// File: Interop.h
//
// c++/c# interop
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::IO;
using namespace System::Text;

namespace MaoliSharp
{
	// Interop Helpers
	class Interop
	{
	public:

		// Convert managed string to unmanaged char
		static inline char* Interop::StringToChar(String^ str)
		{
			return (char*)(void*)Marshal::StringToHGlobalAnsi(str);
		}

		// Convert d3d vector to manged color
		static inline System::Drawing::Color VectorToColor(const Vector4& c)
		{
			return System::Drawing::Color::FromArgb(int32(c.w*255), int32(c.x*255), int32(c.y*255), int32(c.z*255));
		}

		// Convert managed color to d3d vector
		static inline Vector4 ColorToVector(System::Drawing::Color c)
		{
			return Vector4((float)c.R/255.0f, (float)c.G/255.0f, (float)c.B/255.0f, 0);
		}

	};

}

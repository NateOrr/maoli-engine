//--------------------------------------------------------------------------------------
// File: Vector2.h
//
// 2D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Vector2.h"

namespace MaoliSharp
{
	// Subtraction
	Vector2 Vector2::operator-(Vector2 a, Vector2 b)
	{
		return Vector2(a.x-b.x, a.y-b.y);
	}

	// Subtraction
	Vector2 Vector2::operator-=(Vector2 a, Vector2 v)
	{
		a.x-=v.x;
		a.y-=v.y;
		return a;
	}

	// Addition
	Vector2 Vector2::operator+(Vector2 a, Vector2 b)
	{
		return Vector2(a.x+b.x, a.y+b.y);
	}

	// Addition
	Vector2 Vector2::operator+=(Vector2 a, Vector2 v)
	{
		a.x+=v.x;
		a.y+=v.y;
		return a;
	}


	// Scalar mult
	Vector2 Vector2::operator*(Vector2 v, float f)
	{
		return Vector2(v.x*f, v.y*f);
	}


	// Scalar mult
	Vector2 Vector2::operator*=(Vector2 v, float f)
	{
		v.x*=f;
		v.y*=f;
		return v;
	}


	// Scalar div
	Vector2 Vector2::operator/(Vector2 v, float d)
	{
		float f = 1.0f/d;
		return Vector2(v.x*f, v.y*f);
	}


	// Scalar div
	Vector2 Vector2::operator/=(Vector2 v, float d)
	{
		float f = 1.0f/d;
		v.x*=f;
		v.y*=f;
		return v;
	}



	// Dot product
	float Vector2::Dot(Vector2 v)
	{
		return x*v.x + y*v.y;
	}


	// Dot product
	float Vector2::operator*(Vector2 a, Vector2 b)
	{
		return a.Dot(b);
	}

	// Length
	float Vector2::Length()
	{
		return sqrtf(x*x + y*y);
	}


	// Normalize the vector
	Vector2 Vector2::Normalize()
	{
		float l = 1.0f / Length();
		x*=l;
		y*=l;
		return *this;
	}
}

//--------------------------------------------------------------------------------------
// File: Phantom.h
//
// Exposes the Phantom class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
#include "Component.h"
#include "Dependency.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace Honua;

// Forward decl
namespace Maoli
{
	class Phantom;
}

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Phantom : public Component
	{
	public:

		// Ctor
		Phantom( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "Phantom for physics simulation, receives collision events but does not affect the world";
		}

		// Get the unmanaged animation object
		inline Maoli::Phantom* GetUnmanaged() { return _phantom; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( Phantom^ body ) { return (_phantom == body->_phantom); }

		// Position
		[AxisWidget( AxisMode::Translate )]
		property Vector3 Position
		{
			inline Vector3 get();
			inline void set( Vector3 pos );
		}

		// Size
		[HonuaVector]
		property Vector3 Size
		{
			inline Vector3 get();
			inline void set( Vector3 pos );
		}

	private:

		Maoli::Phantom*		_phantom;
		bool				_fitToModel;
	};
}
//--------------------------------------------------------------------------------------
// File: Renderer.cpp
//
// Direct3D 11 Graphics Engine Wrapper
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"
#include "Graphics/Renderer.h"

#pragma managed
#include "Renderer.h"
#include "Engine.h"
#include "Terrain.h"

namespace MaoliSharp
{

	// Ctor
	Renderer::Renderer( Engine^ engine )
	{
		_engine = engine->GetUnmanaged();
		_graphics = (Maoli::Renderer*)_engine->GetGraphics();
		_visualizer = _graphics->GetDebugVisualizer();
		_needsDelete = false;
	}


	// Cleanup
	void Renderer::Release()
	{
		if ( _needsDelete )
			delete _graphics;
		_needsDelete = false;
	}


	// Resize the backbuffer
	void Renderer::Resize()
	{
		Maoli::ErrorMessage( "Need to implement m_Renderer->Resize()" );
		//m_Renderer->Resize();
	}

	// Update the scene
	void Renderer::Update()
	{
		_graphics->Update( _engine->GetFrameTime() );
	}

	// Render the scene
	void Renderer::Render()
	{
		_graphics->Render();
	}

	// Destroy the scene
	void Renderer::ClearScene()
	{
		_graphics->ClearScene();
	}


	// Optimize the scene layout
	void Renderer::OptimizeScene()
	{
		Maoli::ErrorMessage( "Need to implement m_Renderer->Optimize()" ); /*m_Renderer->Optimize();*/
	}


#pragma region Meshes

	// Load a mesh from file
	Model^ Renderer::LoadMesh( System::String^ file )
	{
		Maoli::Model* pMesh = Maoli::Model::CreateFromFile( Interop::StringToChar( file ), _engine );
		if ( pMesh )
			return gcnew Model( gcnew UnmanagedComponent( pMesh, _engine ) );
		else
			return nullptr;
	}


	// Delete a mesh from the scsne
	void Renderer::DeleteMesh( Model^ mesh )
	{
		delete mesh->GetUnmanaged();
	}


#pragma endregion


#pragma region Materials

	// Create a new material
	Material^ Renderer::CreateMaterial( System::String^ name )
	{
		return gcnew Material( _graphics->CreateMaterialBinding( Interop::StringToChar( name ) ) );
	}

	// Load a material from file
	Material^ Renderer::LoadMaterial( String^ file )
	{
		return gcnew Material( _graphics->CreateMaterialBindingFromFile( Interop::StringToChar( file ) ) );
	}

	// Permanently delete a material
	void Renderer::RemoveMaterial( Material^ mat )
	{
		// Remove all references
		for ( uint32 i = 0; i < _graphics->GetNumModels(); i++ )
		{
			for ( uint32 j = 0; j < _graphics->GetModel( i )->GetNumSubMesh(); j++ )
			{
				if ( mat->GetUnmanaged() == _graphics->GetModel( i )->GetSubMesh( j )->GetMaterialBinding() )
					_graphics->GetModel( i )->GetSubMesh( j )->SetMaterialBinding( _graphics->CreateMaterialBinding( "Default" ) );
			}
		}

		// Now delete the material
		delete mat;
	}

#pragma endregion



#pragma region Editor


	// Draw the terrain widget
	void Renderer::DrawTerrainWidget()
	{
		_visualizer->DrawTerrainWidget();
	}


	// Gets the ray that corresponds to the 2d screen position
	void Renderer::GetPickRay( int32 x, int32 y, [Out]Vector3%oPos, [Out]Vector3%oDir )
	{
		Maoli::Vector3 pos, dir;
		Maoli::Math::GetPickRay( x, y, *_graphics->GetCamera(), pos, dir );
		oPos = Vector3( pos );
		oDir = Vector3( dir );
	}

	// Gets the ray that corresponds to the 2d screen position
	void Renderer::GetPickRay( Vector3% oPos, Vector3% oDir )
	{
		oPos = Vector3( _graphics->GetPickRayPosition() );
		oDir = Vector3( _graphics->GetPickRayDirection() );
	}



#pragma endregion


#pragma region Probes

	// Create a new probe
	LightProbe^ Renderer::AddLightProbe( String^ file )
	{
		return gcnew LightProbe( _graphics->AddProbe( Interop::StringToChar( file ) ) );
	}

	// Remove a probe
	void Renderer::RemoveProbe( LightProbe^ probe )
	{
		_graphics->RemoveProbe( probe->GetUnmanaged() );
	}

#pragma endregion



#pragma region Textures

	// Load a texture from file
	Texture^ Renderer::LoadTexture( String^ file )
	{
		Maoli::Texture::pointer tex = _graphics->CreateTextureFromFile( Interop::StringToChar( file ) );
		if ( tex )
			return gcnew Texture( tex );
		return nullptr;
	}

	// Blend two materials together
	void Renderer::BlendMaterials( Material^ blendBase, Material^ blendLayer, Texture^ blendMask, Material^ result )
	{
		_graphics->BlendMaterials( blendBase->GetUnmanaged(), blendLayer->GetUnmanaged(),
			blendMask->GetUnmanaged(), result->GetUnmanaged() );
	}


	// Compute a normal map from a bump map
	Texture^ Renderer::ComputeNormalMap( Texture^ bumpMap )
	{
		return gcnew Texture( _graphics->ComputeNormalMap( bumpMap->GetUnmanaged() ) );
	}


	// Store height in alpha of a normal map
	Texture^ Renderer::StoreHeightInNormalMap( Texture^ normalMap, Texture^ heightMap )
	{
		return gcnew Texture( _graphics->StoreHeightInNormalMap( normalMap->GetUnmanaged(), heightMap->GetUnmanaged() ) );
	}

	// Clear debug render list
	void Renderer::ClearDebugRenderList()
	{
		_visualizer->Clear();
	}

	// Render a line
	void Renderer::DebugRenderLine( Vector3 pos, Vector3 rot, Vector3 scale, Vector3 color, bool ignoreDepth )
	{
		_visualizer->DrawLine( Maoli::Vector3( pos.x, pos.y, pos.z ),
			Maoli::Vector3( rot.x, rot.y, rot.z ),
			Maoli::Vector3( scale.x, scale.y, scale.z ),
			Maoli::Color( color.x, color.y, color.z ), true );
	}

	// Render a box
	void Renderer::DebugRenderBox( Vector3 pos, Vector3 size, Vector3 color )
	{
		_visualizer->DrawBox( Maoli::Vector3( pos.x, pos.y, pos.z ),
			Maoli::Vector3( size.x, size.y, size.z ),
			Maoli::Color( color.x, color.y, color.z ) );
	}

	// Render an object in debug mode
	void Renderer::DebugRenderMesh( Model^ mesh, Vector3 color, Vector3 pos, Vector3 rot, Vector3 scale, bool ignoreDepth )
	{
		_visualizer->DrawMesh( mesh->GetUnmanaged(), Maoli::Color( color.x, color.y, color.z ),
			Maoli::Vector3( pos.x, pos.y, pos.z ), Maoli::Vector3( rot.x, rot.y, rot.z ), Maoli::Vector3( scale.x, scale.y, scale.z ), ignoreDepth );
	}

	// Render an object in debug mode
	void Renderer::DebugRenderMesh( Model^ mesh, Vector3 color, bool ignoreDepth )
	{
		_visualizer->DrawMesh( mesh->GetUnmanaged(), Maoli::Color( color.x, color.y, color.z ), ignoreDepth );
	}

	// Render an object in debug mode
	void Renderer::DebugRenderMesh( Model^ mesh, int32 submesh, bool submeshOnly, bool wireframe, bool boundBox )
	{
		_visualizer->DrawMesh( mesh->GetUnmanaged(), submesh, submeshOnly, wireframe, boundBox );
	}

	// Render an object in debug mode
	void Renderer::DebugRenderLight( Light^ light )
	{
		_visualizer->VisualizeLight( light->GetUnmanaged() );
	}

	// Render the screen selection box
	void Renderer::RenderSelectionBox( float x1, float y1, float x2, float y2 )
	{
		_visualizer->DrawSelectionBox( x1, y1, x2, y2 );
	}

	// Load in a heightmap
	Terrain^ Renderer::LoadHeightmap( String^ file )
	{
		auto terrain = _graphics->ImportHeightmap( Interop::StringToChar( file ) );
		if ( terrain )
		{
			_terrain = gcnew Terrain( terrain );
			return _terrain;
		}
		return nullptr;
	}

	// Save the heightmap to file
	void Renderer::SaveHeightmap( String^ file )
	{
		if ( _terrain )
			_terrain->ExportHeightmap( file );
	}

	// Create a blank terrain
	Terrain^ Renderer::CreateTerrain( uint32 size )
	{
		auto terrain = _graphics->CreateHeightmap( size );
		if ( terrain )
		{
			_terrain = gcnew Terrain( terrain );
			return _terrain;
		}
		return nullptr;
	}

	// Get the layer mask file path
	System::String^ Renderer::GetLayerMask( int32 i )
	{
		if ( HasTerrain() )
		{
			const Maoli::String& str = _graphics->GetTerrain()->GetLayerMaskName( i );
			if ( str == Maoli::String::none )
				return nullptr;
			return gcnew System::String( str.c_str() );
		}
		return nullptr;
	}

	// Update the terrain after map loading
	void Renderer::UpdateTerrain()
	{
		if ( _graphics->GetTerrain() )
			_terrain = gcnew Terrain( _graphics->GetTerrain() );
		else
			_terrain = nullptr;
	}

	// Delete the terrain
	void Renderer::ClearTerrain()
	{
		_graphics->ClearTerrain();
		_terrain = nullptr;
	}

	// Get the mouse pick location on the terrain
	Vector3 Renderer::GetTerrainPickPoint()
	{
		return Vector3( _graphics->GetTerrainPickPoint() );
	}

	// Get the mouse pick point with the scene
	Vector3 Renderer::GetPickPoint()
	{
		return Vector3( _graphics->GetPickPoint() );
	}

	// Get the mouse pick point with the scene
	Vector3 Renderer::GetPickPoint( Model^ modelToIgnore )
	{
		return Vector3( _graphics->GetPickPoint( modelToIgnore->GetUnmanaged() ) );
	}



#pragma endregion

}

//--------------------------------------------------------------------------------------
// File: Texture.h
//
// Wraps textures
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/Texture.h"

#pragma managed
using namespace System;

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Texture
	{
	public:

		// Ctor
		Texture(Maoli::Texture::pointer tex);

		// Dtor
		~Texture();
		!Texture();

		// Get access to the unmanaged pointer
		inline Maoli::Texture::pointer GetUnmanaged(){ return *_texture; }

		// Save to file
		void Save(String^ file);

		// Get the file name
		property String^ FileName
		{
			inline String^ get(){ return gcnew String((*_texture)->GetName()); }
		}

	private:
		Maoli::Texture::pointer* _texture;
	};
}


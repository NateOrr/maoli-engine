//--------------------------------------------------------------------------------------
// File: Phantom.cpp
//
// Exposes the Phantom class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"
#include "Physics/Phantom.h"

#pragma managed
#include "Phantom.h"
#include "Interop.h"

namespace MaoliSharp
{
	// Ctor
	Phantom::Phantom( UnmanagedComponent^ component ) : Component( component )
	{
		_phantom = (Maoli::Phantom*)component->component;
	}

	// Position
	Vector3 Phantom::Position::get()
	{
		return Vector3( _phantom->GetPosition() );
	}

	// Position
	void Phantom::Position::set( Vector3 pos )
	{
		_phantom->SetPosition( Maoli::Vector3( pos.x, pos.y, pos.z ) );
	}

	// Size
	Vector3 Phantom::Size::get()
	{
		return Vector3( _phantom->GetSize() );
	}

	// Size
	void Phantom::Size::set( Vector3 pos )
	{
		_phantom->SetSize( Maoli::Vector3( pos.x, pos.y, pos.z ) );
	}

}
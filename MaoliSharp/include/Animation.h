//--------------------------------------------------------------------------------------
// File: Animation.h
//
// Exposes the AnimationInstance class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
#include "Component.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace MaoliSharp
{
	// Forward decl
	ref class Engine;
	ref class AnimationTrack;

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Animation : public Component
	{
	public:

		// Ctor
		Animation( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "Animation set";
		}

		// Get the unmanaged animation object
		inline Maoli::AnimationController* GetUnmanaged() { return _animation; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( Animation^ animation ) { return (_animation == animation->_animation); }


		// Play an animation
		void PlayTrack( int32 trackIndex );

		// Pause an animation
		void Pause();

		// Get the number of animation tracks
		int32 GetNumTracks();

		// Load an animation track
		AnimationTrack^ AddTrack( String^ fileName );
		AnimationTrack^ AddTrack( String^ fileName, int32 index );

		// Delete an animation track
		void DeleteTrack( AnimationTrack^ track );
		void DeleteTrack( int32 trackIndex );
		void DeleteTrack( String^ trackName );

		// Check if a track is loaded
		bool IsTrackLoaded( String^ file );

		// Get the tracks
		property List<AnimationTrack^>^ Tracks
		{
			List<AnimationTrack^>^ get();
		}

	private:

		// Update track indices
		void UpdateIndices();

		Maoli::AnimationController*		_animation;
		List<AnimationTrack^>^	_tracks;

	};
}

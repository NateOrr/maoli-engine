//--------------------------------------------------------------------------------------
// File: ViewFrustum.h
//
// View frustum
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged

// Forward decl
namespace Maoli
{
	class Frustum;
}

#pragma managed

namespace MaoliSharp
{
	public ref class Frustum
	{
	public:

		// Ctor
		Frustum();

		// Dtor
		~Frustum();

		// Create from 4 corners and a camera
		void Create( Vector3 origin, array<Vector3>^ corners, float minZ, float maxZ );

		// Box intersect test
		bool BoxIntersect( Vector3 pos, Vector3 size );

		// Sphere intersect test
		bool SphereIntersect( Vector3 pos, float radius );

	private:

		Maoli::Frustum*		_frustum;
	};
}
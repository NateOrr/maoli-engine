//--------------------------------------------------------------------------------------
// File: Math.h
//
// Main include for the managed math lib
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
#include "Entity.h"
#include "Model.h"

namespace MaoliSharp 
{

	// Various math functions
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Math
	{
	public:

		// PI
		static property float PI
		{ 
			inline float get() { return Maoli::Math::pi; }
		}

		// Ray-Box intersection
		static bool RayBoxIntersect( Vector3 pos, Vector3 dir, Vector3 boxPosition, Vector3 boxSize, float% dist );

		// Box-Sphere interselction
		static bool BoxSphereIntersect( Vector3 boxCenter, Vector3 boxSize, Vector3 sphereCenter, float radius );

		// Performs a ray-sphere intersect test
		static bool RaySphereIntersect(Vector3 pos, Vector3 dir, Vector3 center, float radius);

		// Performs a ray-entity intersect test
		static bool RayEntityIntersect(Vector3 pos, Vector3 dir, Entity^ e);
		
	};

}

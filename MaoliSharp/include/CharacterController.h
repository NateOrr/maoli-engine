//--------------------------------------------------------------------------------------
// File: CharacterController.h
//
// CharacterController component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
#include "Component.h"
#include "Dependency.h"
#include "RigidBody.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace Honua;

// Forward decl
namespace Maoli
{
	class CharacterController;
}

namespace MaoliSharp
{
	// Forward decl
	ref class Model;
	ref class RigidBody;

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class CharacterController : public Component
	{
	public:

		// Ctor
		CharacterController( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "Rigid body for physics simulation";
		}

		// Get the unmanaged animation object
		inline Maoli::CharacterController* GetUnmanaged() { return _controller; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( CharacterController^ body ) { return (_controller == body->_controller); }

		// Position
		[AxisWidget( AxisMode::Translate )]
		property Vector3 Position
		{
			Vector3 get();
			void set( Vector3 pos );
		}

		// Rotation
		[AxisWidget( AxisMode::Rotate )]
		property Vector3 Rotation
		{
			Vector3 get();
			void set( Vector3 pos );
		}

		// Mass
		[HonuaFloat(ValueControls::Box, 0.001f, 10000.0f)]
		property float Mass
		{
			float get();
			void set( float value );
		}

		// Radius
		[HonuaFloat( ValueControls::Box, 0.001f, 100.0f )]
		property float Radius
		{
			float get();
			void set( float value );
		}

		// Height
		[HonuaFloat( ValueControls::Box, 0.001f, 100.0f )]
		property float Height
		{
			float get();
			void set( float value );
		}

		// Collision group
		[HonuaSelectionBox( SelectionBoxTarget::Enum, CollisionLayer::typeid )]
		property CollisionLayer CollisionGroup
		{
			CollisionLayer get();
			void set( CollisionLayer value );
		}

		// Create a controller
		void Create( float height, float radius, float mass );

	private:

		Maoli::CharacterController*		_controller;
	};
}

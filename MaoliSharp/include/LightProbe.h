//--------------------------------------------------------------------------------------
// File: LightProbe.h
//
// Image based light probe
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/Probe.h"

#pragma managed
using namespace System;

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class LightProbe
	{
	public:
		
		// Ctor
		LightProbe(Maoli::EnvironmentProbe* probe);

		// Load a cubemap
		bool LoadCubemap(String^ file);

		// Access the unmanaged pointer
		inline Maoli::EnvironmentProbe* GetUnmanaged(){ return _probe; }

	private:

		// Unmanged
		Maoli::EnvironmentProbe* _probe;
	};

}

//--------------------------------------------------------------------------------------
// File: Light.h
//
// Exposes the light class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/Light.h"

#pragma managed
#include "Material.h"
#include "Model.h"
#include "Component.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace Honua;

namespace MaoliSharp
{
	// Light types
	public enum class LightType : int32
	{
		Point = 1,
		Spot,
	};

	// Forward decl
	ref class Texture;

	// Wraps the Light class
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Light : public Component
	{
	public:

		// Ctor
		Light( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "Light";
		}

		// Get the unmanaged Light object
		inline Maoli::Light* GetUnmanaged() { return _light; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( Light^ light ) { return (_light == light->_light); }

		// Position
		[AxisWidget( AxisMode::Translate )]
		property Vector3 Position
		{
			inline Vector3 get() { return Vector3( _light->GetLocalPosition() ); }
			inline void set( Vector3 pos ) { _light->SetLocalPosition( pos.x, pos.y, pos.z ); }
		}

		// Direction
		[AxisWidget( AxisMode::Rotate )]
		property Vector3 Direction
		{
			inline Vector3 get() { return Vector3( _light->GetLocalDirection() ); }
			inline void set( Vector3 pos ) { _light->SetLocalDirection( pos.x, pos.y, pos.z ); }
		}

		// Range
		[AxisWidget( AxisMode::UniformScale )]
		[HonuaFloat( ValueControls::Slider, 0.01f, 1000.0f )]
		property float Range
		{
			inline float get() { return _light->GetRange(); }
			inline void set( float r ) { _light->SetRange( r ); }
		}

		// Color
		[HonuaColor]
		property System::Drawing::Color Color
		{
			inline System::Drawing::Color get() { return System::Drawing::Color::FromArgb( 255, (int32)(_light->GetColor().r*255.0f), (int32)(_light->GetColor().g*255.0f), (int32)(_light->GetColor().b*255.0f) ); }
			inline void set( System::Drawing::Color c ) { _light->SetColor( (float)c.R / 255.0f, (float)c.G / 255.0f, (float)c.B / 255.0f ); }
		}

		// Inner radius
		[HonuaDependency( "Type", LightType::Spot)]
		[HonuaFloat( ValueControls::Slider, 0.01f, 1.0f )]
		property float SpotInnerRadius
		{
			inline float get() { return _light->GetInnerRadius(); }
			inline void set( float r ) { _light->SetInnerRadius( r ); }
		}

		// Outer radius
		[HonuaDependency( "Type", LightType::Spot )]
		[HonuaFloat( ValueControls::Slider, 0.01f, 1.0f )]
		property float SpotOuterRadius
		{
			float get() { return _light->GetOuterRadius(); }
			void set( float r ) { _light->SetOuterRadius( r ); }
		}

		// Shadow Quality
		[HonuaDependency( "Shadows", true )]
		[HonuaInt( ValueControls::Slider, 0, 10 )]
		property int32 ShadowSoftness
		{
			inline int32 get() { return _light->ShadowSoftness; }
			inline void set( int32 r ) { _light->ShadowSoftness = r; }
		}

		// Type
		[HonuaSelectionBox( SelectionBoxTarget::Enum )]
		property LightType Type
		{
			inline LightType get() { return (LightType)_light->GetType(); }
			inline void set( LightType i ) { _light->SetType( (Maoli::LightType)i ); }
		}

		// Shadows
		[HonuaBool]
		property bool Shadows
		{
			inline bool get() { return _light->CastsShadows() == TRUE; }
			inline void set( bool b ) { _light->EnableShadows( b ); }
		}

		// Projective texture
		[HonuaDependency( "Type", LightType::Spot )]
		[HonuaTexture]
		property Texture^ ProjectedTexture
		{
			Texture^ get();
			void set( Texture^ value );
		}


	private:
		Maoli::Light*	_light;			// Unmanaged object to wrap
		Texture^		_texture;		// Projected texture
	};
}

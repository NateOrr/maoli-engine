//--------------------------------------------------------------------------------------
// File: RigidBody.h
//
// Rigid body component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
#include "Component.h"
#include "Dependency.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace Honua;

// Forward decl
namespace Maoli
{
	class RigidBody;
	struct RigidBodyDesc;
}

namespace MaoliSharp
{
	// Rigid body types
	public enum class RigidBodyShape
	{
		Box = (int32)Maoli::PhysicsShape::Box,
		Sphere = (int32)Maoli::PhysicsShape::Sphere,
		Capsule = (int32)Maoli::PhysicsShape::Capsule,
		Cylinder = (int32)Maoli::PhysicsShape::Cylinder,
		ConvexHull = (int32)Maoli::PhysicsShape::ConvexHull,
		Mesh = (int32)Maoli::PhysicsShape::Mesh,
	};

	// Collision groups
	public enum class CollisionLayer
	{
		Default,
	};

	// Maps to hkpMotion::MotionType
	public enum class MotionType
	{
		Invalid = (int32)Maoli::MotionType::Invalid,
		Dynamic = (int32)Maoli::MotionType::Dynamic,
		SphereInertia = (int32)Maoli::MotionType::SphereInertia,
		BoxInertia = (int32)Maoli::MotionType::BoxInertia,
		Keyframed = (int32)Maoli::MotionType::Keyframed,
		Static = (int32)Maoli::MotionType::Static,
		ThinBoxInertia = (int32)Maoli::MotionType::ThinBoxInertia,
		Character = (int32)Maoli::MotionType::Character,
	};

	// Forward decl
	ref class Model;

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class RigidBody : public Component
	{
	public:

		// Ctor
		RigidBody( UnmanagedComponent^ component );

		// Dtor
		~RigidBody();

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "Rigid body for physics simulation";
		}

		// Get the unmanaged animation object
		inline Maoli::RigidBody* GetUnmanaged() { return _body; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( RigidBody^ body ) { return (_body == body->_body); }

		// Local position
		[AxisWidget( AxisMode::Translate )]
		property Vector3 LocalPosition
		{
			Vector3 get();
			void set( Vector3 value );
		}

		// Body type
		[HonuaSelectionBox( SelectionBoxTarget::Enum, RigidBodyShape::typeid )]
		property RigidBodyShape Shape
		{
			RigidBodyShape get();
			void set( RigidBodyShape f );
		}

		// Motion type
		[HonuaSelectionBox( SelectionBoxTarget::Enum, MotionType::typeid )]
		property MotionType Motion
		{
			MotionType get();
			void set( MotionType f );
		}

		// Fit to the model, if there is one
		[HonuaDependency( "HasModel", true )]
		[HonuaBool]
		property bool FitToModel
		{
			bool get();
			void set( bool value );
		}

		// Capsule / Cylinder height
		[HonuaDependency( "Shape", RigidBodyShape::Capsule, RigidBodyShape::Cylinder )]
		[HonuaFloat( ValueControls::Slider, 0.01f, 100.0f )]
		property float Height
		{
			float get();
			void set( float f );
		}

		// Radius
		[HonuaDependency( "Shape", RigidBodyShape::Sphere, RigidBodyShape::Capsule, RigidBodyShape::Cylinder )]
		[HonuaFloat( ValueControls::Slider, 0.01f, 100.0f )]
		property float Radius
		{
			float get();
			void set( float f );
		}

		// AABB
		[HonuaDependency( "Shape", RigidBodyShape::Box )]
		[HonuaVector]
		property Vector3 Size
		{
			Vector3 get();
			void set( Vector3 f );
		}

		// Mass
		[HonuaFloat( ValueControls::Box, 0.01f, 10000.0f )]
		property float Mass
		{
			float get();
			void set( float f );
		}

		// Restitition
		[HonuaFloat( ValueControls::Box, 0.0f, 1.0f )]
		property float Restitution
		{
			float get();
			void set( float f );
		}

		// Collision group
		[HonuaSelectionBox( SelectionBoxTarget::Enum, CollisionLayer::typeid )]
		property CollisionLayer CollisionGroup
		{
			CollisionLayer get();
			void set( CollisionLayer value );
		}

		// Check for model component
		property bool HasModel
		{
			bool get();
		}

	private:

		// Update body based on the desc
		bool Create();

		Maoli::RigidBody*		_body;
		Maoli::RigidBodyDesc*	_desc;
		bool					_fitToModel;
	};
}

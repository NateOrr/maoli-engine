//--------------------------------------------------------------------------------------
// File: CharacterController.cpp
//
// Exposes the CharacterController class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"
#include "Physics\CharacterController.h"

#pragma managed
#include "CharacterController.h"
#include "Interop.h"
#include "RigidBody.h"
#include "Engine.h"
#include "Entity.h"

namespace MaoliSharp
{
	// Ctor
	CharacterController::CharacterController( UnmanagedComponent^ component ) : Component( component )
	{
		_controller = (Maoli::CharacterController*)component->component;
	}

	// Create a controller
	void CharacterController::Create( float height, float radius, float mass )
	{
		// Since a whole new body has to be created, remove the old one first
		if ( _controller->IsActive() && Owner )
			_controller->GetEngine()->GetPhysics()->UnregisterComponent( _controller );

		// Create the new one
		_controller->Create( height, radius, mass );

		// Add it back in
		if ( Owner )
			_controller->GetEngine()->GetPhysics()->RegisterComponent( _controller );
	}

	// Mass
	float CharacterController::Mass::get()
	{
		return _controller->GetMass();
	}

	// Mass
	void CharacterController::Mass::set( float value )
	{
		Create( Height, Radius, value );
	}

	// Radius
	float CharacterController::Radius::get()
	{
		return _controller->GetRadius();
	}

	// Radius
	void CharacterController::Radius::set( float value )
	{
		Create( Height, value, Mass );
	}

	// Height
	float CharacterController::Height::get()
	{
		return _controller->GetHeight();
	}

	// Height
	void CharacterController::Height::set( float value )
	{
		Create( value, Radius, Mass );
	}

	// Collision group
	CollisionLayer CharacterController::CollisionGroup::get()
	{
		return (CollisionLayer)_controller->GetCollisionLayer();
	}

	// Collision group
	void CharacterController::CollisionGroup::set( CollisionLayer value )
	{
		_controller->SetCollisionLayer( (int32)value );
	}

	// Rotation
	Vector3 CharacterController::Rotation::get()
	{
		return Vector3( _controller->GetLocalRotation() );
	}

	// Rotation
	void CharacterController::Rotation::set( Vector3 value )
	{
		_controller->SetLocalRotation( value.AsMaoliVector() );
	}

	// Position
	Vector3 CharacterController::Position::get()
	{
		return Vector3( 0, 0, 0 );
	}

	// Position
	void CharacterController::Position::set( Vector3 pos )
	{

	}


}
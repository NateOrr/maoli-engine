//--------------------------------------------------------------------------------------
// File: Submesh.h
//
// Exposes SubMesh to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/RenderNode.h"

#pragma managed
#include "Material.h"

namespace MaoliSharp
{
	// A mesh section
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class SubMesh
	{
	public:

		// Ctor
		SubMesh(Maoli::RenderNode* parent);

		// Convert to string
		virtual String^ ToString() override{ return Name; }

		// Name
		property System::String^ Name
		{
			inline System::String^ get(){ return gcnew System::String(_submesh->name.c_str()); }
			inline void set(System::String^ str){ _submesh->name = (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(str); }
		}

		// Material
		property MaoliSharp::Material^ Material
		{
			inline MaoliSharp::Material^ get( ) { return gcnew MaoliSharp::Material( _submesh->GetMaterialBinding() ); }
			inline void set(MaoliSharp::Material^ mat){ _submesh->SetMaterialBinding( mat->GetUnmanaged() ); }
		}

		// Bounding radius
		property float Radius
		{ 
			inline float get(){ return _submesh->bounds.GetRadius(); } 
		}


	private:
		Maoli::RenderNode*		_submesh;		// Unmanaged parent
	};
}

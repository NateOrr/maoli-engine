//--------------------------------------------------------------------------------------
// File: Camera.h
//
// Exposes the Transform class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/Renderer.h"

#pragma managed
#include "Component.h"
#include "Dependency.h"
using namespace System;
using namespace System::Runtime::InteropServices;

namespace MaoliSharp
{
	// Forward decl
	ref class Engine;

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Transform : public Component
	{
	public:

		// Ctor
		Transform( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "A 3D transform, contains an orientation matrix as well as velocity and angular velocity control";
		}

		// Point the camera
		inline void LookAt( Vector3 pos ) { _transform->LookAt( pos.x, pos.y, pos.z ); }

		// Move the camera
		inline void AddToVelocity( float x, float y, float z ) { _transform->AddToVelocity( x, y, z ); }

		// Change the view angle smoothly
		inline void AddToAngularVelocity( float x, float y, float z ) { _transform->AddToAngularVelocity( x, y, z ); }


		// Position
		[AxisWidget( AxisMode::Translate )]
		property Vector3 Position
		{
			inline Vector3 get() { return Vector3( _transform->GetPosition() ); }
			inline void set( Vector3 vec ) { _transform->SetPosition( vec.x, vec.y, vec.z ); }
		}


		// Velocity
		property Vector3 Velocity
		{
			inline Vector3 get() { return Vector3( _transform->GetVelocity() ); }
			inline void set( Vector3 vec ) { _transform->SetVelocity( vec.x, vec.y, vec.z ); }
		}


		// Direction
		property Vector3 Direction
		{
			inline Vector3 get() { return Vector3( _transform->GetViewDirection() ); }
		}


		// View angles
		property Vector2 ViewAngles
		{
			inline Vector2 get() { return Vector2( _transform->GetXRotation(), _transform->GetYRotation() ); }
		}

		// General rotation angles
		[AxisWidget( AxisMode::Rotate )]
		property Vector3 Rotation
		{
			inline Vector3 get() { return Vector3( _transform->GetRotation() ); }
			inline void set( Vector3 r ) { _transform->SetRotation( r.x, r.y, r.z ); }
		}


	protected:
		Maoli::Transform*	_transform;
	};
}

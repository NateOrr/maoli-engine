//--------------------------------------------------------------------------------------
// File: Model.cpp
//
// Exposes the Model class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"
#include "Graphics/Renderer.h"

#pragma managed
#include "Model.h"
#include "Entity.h"
#include "Animation.h"

namespace MaoliSharp
{
	// Ctor
	Model::Model( UnmanagedComponent^ component ) : Component( component )
	{
		_model = (Maoli::Model*)_component;
		SubMeshes = gcnew System::Collections::Generic::List<SubMesh^>();
		if ( _model && _model->GetNumSubMesh() > 0 )
		{
			for ( uint32 i = 0; i < _model->GetNumSubMesh(); i++ )
				SubMeshes->Add( gcnew SubMesh( _model->GetSubMesh( i ) ) );
		}
	}

	// Save to file
	void Model::Save( System::String^ fileName )
	{
		_model->GetGeometry()->Save( Interop::StringToChar( fileName ) );
	}

	// Save to file
	void Model::Save()
	{
		if ( _model->GetGeometry()->FileBased() )
			_model->GetGeometry()->Save( _model->GetGeometry()->GetFilePath() );
	}

	// Load a mesh from a file
	bool Model::Load( System::String^ fileName )
	{
		Maoli::String name = Interop::StringToChar( fileName );
		Maoli::Geometry* geometry = Maoli::Geometry::CreateFromFile( name, _model->GetEngine() );

		if ( _model->Create( geometry ) )
		{
			SubMeshes->Clear();
			for ( uint32 i = 0; i < _model->GetNumSubMesh(); i++ )
				SubMeshes->Add( gcnew SubMesh( _model->GetSubMesh( i ) ) );
			return true;
		}
		return false;
	}

	// Performs a ray intersection test with this mesh
	bool Model::RayIntersect( Vector3 rayPos, Vector3 rayDir, bool initialBoxTest )
	{
		return _model->RayIntersect( Maoli::Vector3( rayPos.x, rayPos.y, rayPos.z ), Maoli::Vector3( rayDir.x, rayDir.y, rayDir.z ), initialBoxTest );
	}

	// Performs a ray intersection test with this mesh
	bool Model::RayIntersect( Vector3 rayPos, Vector3 rayDir, bool initialBoxTest, [Out] float dist )
	{
		float outdist;
		if ( _model->RayIntersect( Maoli::Vector3( rayPos.x, rayPos.y, rayPos.z ), Maoli::Vector3( rayDir.x, rayDir.y, rayDir.z ), initialBoxTest, outdist ) )
		{
			dist = outdist;
			return true;
		}
		return false;
	}


	// Dimensions
	float Model::XDimension::get()
	{
		return _model->GetDimensions().x;
	}

	// Dimensions
	void Model::XDimension::set( float value )
	{
		Maoli::Vector3 d = _model->GetDimensions();
		_model->SetDimensions( value, d.y, d.z );
	}

	// Dimensions
	float Model::YDimension::get()
	{
		return _model->GetDimensions().y;
	}

	// Dimensions
	void Model::YDimension::set( float value )
	{
		Maoli::Vector3 d = _model->GetDimensions();
		_model->SetDimensions( d.x, value, d.z );
	}

	// Dimensions
	float Model::ZDimension::get()
	{
		return _model->GetDimensions().z;
	}

	// Dimensions
	void Model::ZDimension::set( float value )
	{
		Maoli::Vector3 d = _model->GetDimensions();
		_model->SetDimensions( d.x, d.y, value );
	}


}

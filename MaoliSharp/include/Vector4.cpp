//--------------------------------------------------------------------------------------
// File: Vector4.h
//
// 4D Vector
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Vector4.h"

namespace MaoliSharp
{
	// Subtraction
	Vector4 Vector4::operator-(Vector4 a, Vector4 b)
	{
		return Vector4(a.x-b.x, a.y-b.y, a.z-b.z, a.w-b.w);
	}

	// Subtraction
	Vector4 Vector4::operator-=(Vector4 a, Vector4 v)
	{
		a.x-=v.x;
		a.y-=v.y;
		a.z-=v.z;
		a.w-=v.w;
		return a;
	}

	// Addition
	Vector4 Vector4::operator+(Vector4 a, Vector4 b)
	{
		return Vector4(a.x+b.x, a.y+b.y, a.z+b.z, a.w+b.w);
	}

	// Addition
	Vector4 Vector4::operator+=(Vector4 a, Vector4 v)
	{
		a.x+=v.x;
		a.y+=v.y;
		a.z+=v.z;
		a.w+=v.w;
		return a;
	}


	// Scalar mult
	Vector4 Vector4::operator*(Vector4 v, float f)
	{
		return Vector4(v.x*f, v.y*f, v.z*f, v.w*f);
	}


	// Scalar mult
	Vector4 Vector4::operator*=(Vector4 v, float f)
	{
		v.x*=f;
		v.y*=f;
		v.z*=f;
		v.w*=f;
		return v;
	}


	// Scalar div
	Vector4 Vector4::operator/(Vector4 v, float d)
	{
		float f = 1.0f/d;
		return Vector4(v.x*f, v.y*f, v.z*f, v.w*f);
	}


	// Scalar div
	Vector4 Vector4::operator/=(Vector4 v, float d)
	{
		float f = 1.0f/d;
		v.x*=f;
		v.y*=f;
		v.z*=f;
		v.w*=f;
		return v;
	}



	// Dot product
	float Vector4::Dot(Vector4 v)
	{
		return x*v.x + y*v.y + z*v.z + w*v.w;
	}


	// Dot product
	float Vector4::operator*(Vector4 a, Vector4 b)
	{
		return a.Dot(b);
	}


	// Cross product of the 3d components
	Vector4 Vector4::Cross(Vector4 v)
	{
		Vector4 c;
		c.x = y*v.z-v.y*z;
		c.y = v.x*z-x*v.z;
		c.z = x*v.y-v.x*y;
		return c;
	}


	// Length
	float Vector4::Length()
	{
		return sqrtf(x*x + y*y + z*z + w*w);
	}


	// Normalize the vector
	Vector4 Vector4::Normalize()
	{
		float l = 1.0f / Length();
		x*=l;
		y*=l;
		z*=l;
		w*=l;
		return *this;
	}

	// Convert to a color
	System::Drawing::Color Vector4::ToColor()
	{
		return System::Drawing::Color::FromArgb(int32(w*255), int32(x*255), int32(y*255), int32(z*255));
	}
}

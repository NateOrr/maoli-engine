//--------------------------------------------------------------------------------------
// File: Model.h
//
// Exposes the Model class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/Model.h"

#pragma managed
#include "Material.h"
#include "SubMesh.h"
#include "Component.h"
#include "Dependency.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;
using namespace System::ComponentModel;
using namespace Honua;

namespace MaoliSharp
{
	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class Model : public Component
	{
	public:

		// Ctor
		Model( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "3D Model";
		}

		// Lets the editors know if this component is file based
		bool FileBased() override { return true; }

		// Gets a file dialog filter for loading
		String^ GetFileFilter() override
		{
			return "3D Model Files|*.pbm;*.x;*.3ds;*.obj;*.lwo;*.lxo;*.dxf;*.ac;*.ms3d;*.ase;*.ply;*.smd;*.vta;*.dae;*.cob;*.scn;*.mdl;*.md2;*.md3;*.mdc;*.md5;*.bhv;*.csm;*.b3d;*.q3d;*.q3s;*.mesh;*.irrmesh;*.irr;*.nff;*.off;*.raw;*.ter;*.hmp;*.bsp";
		}

		// Load a mesh from a file
		bool Load( System::String^ fileName ) override;

		// Save to file
		void Save();
		void Save( System::String^ fileName );

		// Get the unmanaged object
		inline  Maoli::Model * GetUnmanaged() { return _model; }

		// Returns true if the meshes are identical
		inline bool Equals( Model^ m ) { return _model == m->GetUnmanaged(); }

		// Checks if the mesh intersects with a box
		inline bool BoxMeshIntersect( Vector3% min, Vector3% max ) { return _model->BoxMeshIntersect( Maoli::Vector3( min.x, min.y, min.z ), Maoli::Vector3( max.x, max.y, max.z ) ); }

		// Reverse face winding order
		inline void ReverseFaceWinding() { _model->GetGeometry()->ReverseFaceWinding(); _model->GetMeshBinding()->Update(); }

		// Compute mesh normals
		inline void RecomputeNormals() { _model->GetGeometry()->RecomputeNormals(); _model->GetMeshBinding()->Update(); }

		// Invert y uv coord
		inline void InvertUV() { _model->GetGeometry()->InvertUV(); _model->GetMeshBinding()->Update(); }

		// Flip x and y uvs
		inline void FlipUV() { _model->GetGeometry()->FlipUV(); _model->GetMeshBinding()->Update(); }

		// Flip x and y uvs
		inline void FlipCoordinateSystem() { _model->GetGeometry()->FlipCoordinateSystem(); _model->GetMeshBinding()->Update(); }

		// Delete a submesh
		inline void DeleteSubmesh( int32 i ) { _model->DeleteSubMesh( i ); }

		// Force an update
		inline void Update() { _model->UpdateWorldMatrix(); }

		// Performs a ray intersection test with this mesh
		bool RayIntersect( Vector3 rayPos, Vector3 rayDir, bool initialBoxTest );
		bool RayIntersect( Vector3 rayPos, Vector3 rayDir, bool initialBoxTest, [Out] float dist );

		// Bounding radius
		property float Radius
		{
			inline float get() { return _model->GetBounds().GetRadius(); }
		}

		// Position
		[AxisWidget( AxisMode::Translate )]
		property Vector3 Position
		{
			inline Vector3 get() { return Vector3( _model->GetPos() ); }
			inline void set( Vector3 pos ) { _model->SetPosition( pos.x, pos.y, pos.z ); }
		}

		// Rotation
		[AxisWidget( AxisMode::Rotate )]
		property Vector3 Rotation
		{
			inline Vector3 get() { return Vector3( _model->GetRot() ); }
			inline void set( Vector3 pos ) { _model->SetRotation( pos.x, pos.y, pos.z ); }
		}

		// Scale
		[AxisWidget( AxisMode::Scale )]
		property Vector3 Scale
		{
			inline Vector3 get() { return Vector3( _model->GetScale() ); }
			inline void set( Vector3 pos ) { _model->SetScale( pos.x, pos.y, pos.z ); }
		}

		// Uniform scale
		[AxisWidget( AxisMode::UniformScale )]
		property float UniformScale
		{
			inline float get( ) { return _model->GetScale().Average(); }
			inline void set( float s ) { _model->SetScale( s, s, s ); }
		}

		// Dimensions
		property float XDimension
		{
			float get();
			void set( float value );
		}

		// Dimensions
		property float YDimension
		{
			float get();
			void set( float value );
		}

		// Dimensions
		property float ZDimension
		{
			float get();
			void set( float value );
		}

		// Bounding box
		property Vector3 BoundSize
		{
			inline Vector3 get() { return Vector3( _model->GetBounds().GetSize() ); }
		}

		// Bounding box location
		property Vector3 BoundCenter
		{
			inline Vector3 get() { return Vector3( _model->GetBounds().GetWorldPosition() ); }
		}

		// Submeshes
		System::Collections::Generic::List<SubMesh^>^ SubMeshes;

	private:
		Maoli::Model*				_model;					// Unmanaged object to wrap
	};
}

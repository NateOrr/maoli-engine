//--------------------------------------------------------------------------------------
// File: ParticleEmitter
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
using namespace System;
#include "ParticleEmitter.h"
#include "ParticleEmitterState.h"
#include "ParticleSet.h"
#include "Texture.h"

namespace MaoliSharp
{
	// Ctor
	ParticleEmitter::ParticleEmitter( UnmanagedComponent^ component ) : Component( component )
	{
		_emitter = (Maoli::ParticleEmitter*)component->component;
		_states = gcnew List<ParticleEmitterState^>();

		// Add the states
		for ( uint32 i = 0; i < _emitter->GetNumStates(); ++i )
			_states->Add( gcnew ParticleEmitterState( _emitter->GetState( i ) ) );

		// Link the transition states
		for ( uint32 i = 0; i < _emitter->GetNumStates(); ++i )
		{
			for ( uint32 j = 0; j < _emitter->GetNumStates(); ++j )
			{
				if ( _emitter->GetState( i )->GetNextState() == _emitter->GetState( j ) )
				{
					_states[i]->NextState = _states[j];
					break;
				}
			}
		}
	}

	// Add a new state
	ParticleEmitterState^ ParticleEmitter::CreateState( String^ name )
	{
		auto state = gcnew ParticleEmitterState( _emitter->CreateState( Interop::StringToChar( name ) ) );
		return state;
	}

	// Add an existing state
	void ParticleEmitter::AddState( ParticleEmitterState^ state, int32 index )
	{
		_states->Insert( index, state );
		_emitter->AddState( state->GetUnmanaged() );
	}

	// Remove a state
	void ParticleEmitter::RemoveState( ParticleEmitterState^ state )
	{
		_states->Remove( state );
		_emitter->RemoveState( state->GetUnmanaged() );
	}

	// Set the active state
	void ParticleEmitter::SetActiveState( ParticleEmitterState^ state )
	{
		_emitter->SetState( state->GetUnmanaged() );
	}

	// Create a particle set
	ParticleSet^ ParticleEmitter::CreateParticleSet()
	{
		return gcnew ParticleSet( _emitter->CreateParticleSet() );
	}

}
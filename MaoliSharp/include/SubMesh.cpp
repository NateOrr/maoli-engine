//--------------------------------------------------------------------------------------
// File: Submesh.h
//
// Exposes SubMesh to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "SubMesh.h"

namespace MaoliSharp
{
	// Ctor
	SubMesh::SubMesh( Maoli::RenderNode* parent )
	{
		_submesh = parent;
	}
}

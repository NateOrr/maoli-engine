//--------------------------------------------------------------------------------------
// File: Frustum.cpp
//
// View frustum
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "ViewFrustum.h"

namespace MaoliSharp
{

	// Ctor
	Frustum::Frustum()
	{
		_frustum = new Maoli::Frustum;
	}

	// Dtor
	Frustum::~Frustum()
	{
		delete _frustum;
	}

	// Create from 4 corners and a camera
	void Frustum::Create( Vector3 origin, array<Vector3>^ corners, float minZ, float maxZ )
	{
		Maoli::Vector3 maoliCorners[] = {
			corners[0].AsMaoliVector(),
			corners[1].AsMaoliVector(),
			corners[2].AsMaoliVector(),
			corners[3].AsMaoliVector(),
		};
		_frustum->Build( origin.AsMaoliVector(), maoliCorners, minZ, maxZ );
	}

	// Box intersect test
	bool Frustum::BoxIntersect( Vector3 pos, Vector3 size )
	{
		return _frustum->CheckAABB( pos.AsMaoliVector(), size.AsMaoliVector() );
	}

	// Sphere intersect test
	bool Frustum::SphereIntersect( Vector3 pos, float radius )
	{
		return _frustum->CheckSphere( pos.AsMaoliVector(), radius );
	}

}
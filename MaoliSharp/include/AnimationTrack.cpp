//--------------------------------------------------------------------------------------
// File: AnimationTrack.h
//
// Exposes the AnimationInstance class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "AnimationTrack.h"
#include "Interop.h"

namespace MaoliSharp
{
	// Ctor
	AnimationTrack::AnimationTrack( Maoli::AnimationController* animation, uint32 index )
	{
		_animation = animation;
		_index = index;
	}

	// Get the looping state for the track
	bool AnimationTrack::Loop::get()
	{
		return _animation->GetTrack( _index )->IsLooping();
	}

	// Set the looping state for the track
	void AnimationTrack::Loop::set( bool value )
	{
		_animation->GetTrack( _index )->EnableLoop( value );
	}


	// Play the track
	void AnimationTrack::Play()
	{
		_animation->GetTrack( _index )->Play();
	}

	// Convert to string
	String^ AnimationTrack::ToString()
	{
		return Name;
	}

	// Add an event
	void AnimationTrack::AddEvent( int32 id, float time )
	{
		_animation->GetTrack( _index )->AddEvent( id, time );
	}

	// Remove all instances of this event
	void AnimationTrack::RemoveEvent( int32 id )
	{
		_animation->GetTrack( _index )->RemoveEvent( id );
	}

	// Clear all events
	void AnimationTrack::RemoveAllEvents()
	{
		_animation->GetTrack( _index )->RemoveAllEvents();
	}


	// Duration
	float AnimationTrack::Duration::get()
	{
		return _animation->GetTrack( _index )->GetDuration();
	}


	// Animation time
	float AnimationTrack::Time::get()
	{
		return _animation->GetTrack( _index )->GetTime();
	}

	// Animation time
	void AnimationTrack::Time::set( float value )
	{
		_animation->GetTrack( _index )->SetTime( value );
	}


	// Track name
	String^ AnimationTrack::Name::get()
	{
		return gcnew String( _animation->GetTrack( _index )->GetName() );
	}

	// Track name
	void AnimationTrack::Name::set( String^ value )
	{
		_animation->GetTrack( _index )->SetName( Interop::StringToChar( value ) );
	}


	// Track file name
	String^ AnimationTrack::FileName::get()
	{
		return gcnew String( _animation->GetTrack( _index )->GetFileName() );
	}


	// Playback speed
	float AnimationTrack::Speed::get()
	{
		return _animation->GetTrack( _index )->GetSpeed();
	}

	// Playback speed
	void AnimationTrack::Speed::set( float value )
	{
		_animation->GetTrack( _index )->SetSpeed( value );
	}


	// Index in the animation set
	int32 AnimationTrack::Index::get()
	{
		return _index;
	}

	// Index in the animation set
	void AnimationTrack::Index::set( int32 value )
	{
		_index = value;
	}


}
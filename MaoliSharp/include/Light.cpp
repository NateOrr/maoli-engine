//--------------------------------------------------------------------------------------
// File: LightWrapper.cpp
//
// Exposes the light class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma unmanaged
#include "MaoliSharp.h"
#include "Graphics/Renderer.h"

#pragma managed
#include "Light.h"
#include "Texture.h"

namespace MaoliSharp
{

	// Ctor
	Light::Light( UnmanagedComponent^ component ) : Component( component )
	{
		_light = (Maoli::Light*)_component;
		if ( _light->GetProjectiveTexture() )
			_texture = gcnew Texture( _light->GetProjectiveTexture() );
		else
			_texture = nullptr;
	}

	// Projective texture
	Texture^ Light::ProjectedTexture::get()
	{
		return _texture;
	}

	// Projective texture
	void Light::ProjectedTexture::set( Texture^ value )
	{
		_texture = value;
		if(_texture != nullptr)
			_light->SetProjectiveTexture( _texture->GetUnmanaged() );
	}

}

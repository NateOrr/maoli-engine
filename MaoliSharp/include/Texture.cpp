//--------------------------------------------------------------------------------------
// File: Texture.cpp
//
// Wraps textures
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"
#include "Graphics/Renderer.h"

#pragma managed
#include "Texture.h"
#include "Interop.h"

namespace MaoliSharp
{


	// Ctor
	Texture::Texture( Maoli::Texture::pointer tex )
	{
		_texture = new Maoli::Texture::pointer(tex);
	}

	// Dtor
	Texture::~Texture()
	{
		this->!Texture();
	}


	// Dtor
	Texture::!Texture()
	{
		if ( _texture )
		{
			*_texture = nullptr;
			delete _texture;
			_texture = nullptr;
		}
	}

	// Save to file
	void Texture::Save( String^ file )
	{
		String^ ext = System::IO::Path::GetExtension(file);
		if( ext == ".dds")
			(*_texture)->GetGraphics()->SaveTexture( Interop::StringToChar(file), *_texture, D3DX11_IFF_DDS);
		else if(ext == ".png")
			(*_texture)->GetGraphics()->SaveTexture( Interop::StringToChar(file), *_texture, D3DX11_IFF_PNG);
		else
			Maoli::ErrorMessage("Invalid texture format");
	}

}

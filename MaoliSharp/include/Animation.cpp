//--------------------------------------------------------------------------------------
// File: Animation.h
//
// Exposes the AnimationInstance class to .net as a component
//
// Nate Orr
//--------------------------------------------------------------------------------------

#pragma unmanaged
#include "MaoliSharp.h"

#pragma managed
#include "Animation.h"
#include "AnimationTrack.h"
#include "Interop.h"
#include "Engine.h"

namespace MaoliSharp
{
	// Ctor
	Animation::Animation( UnmanagedComponent^ component ) : Component( component )
	{
		_animation = (Maoli::AnimationController*)_component;
		_tracks = gcnew List<AnimationTrack^>( _animation->GetNumTracks() );
		for ( uint32 i = 0; i < _animation->GetNumTracks(); ++i )
			_tracks->Add( gcnew AnimationTrack( _animation, i ) );
	}

	// Play an animation
	void Animation::PlayTrack( int32 trackIndex )
	{
		if ( _animation )
		{
			_animation->Pause( false );
			_animation->GetTrack( trackIndex )->Play();
		}
	}

	// Pause an animation
	void Animation::Pause()
	{
		if ( _animation )
		{
			_animation->Pause( true );
		}
	}


	// Load an animation track
	AnimationTrack^ Animation::AddTrack( String^ fileName )
	{
		return AddTrack( fileName, _animation->GetNumTracks() );
	}

	// Load an animation track
	AnimationTrack^ Animation::AddTrack( String^ fileName, int32 index )
	{
		Maoli::String name = Interop::StringToChar( fileName );
		if ( _animation->IsTrackLoaded( name ) )
		{
			for(int32 i=0; i<_tracks->Count; ++i)
				if(_tracks[i]->FileName == fileName)
					return _tracks[i];
		}

		if ( _animation->LoadTrack( name, index ) )
		{
			auto track = gcnew AnimationTrack( _animation, index );
			_animation->GetTrack( index )->Play();
			_tracks->Insert( index, track );
			UpdateIndices();
			return track;
		}

		M_ERROR( "Failed to load animation" );
		return nullptr;
	}

	// Delete an animation track
	void Animation::DeleteTrack( int32 trackIndex )
	{
		if ( _animation )
		{
			if ( _animation->GetNumTracks() > 1 )
			{
				_animation->DeleteTrack( trackIndex );
				_tracks->RemoveAt( trackIndex );
			}
			else
			{
				M_ERROR( "You cannot delete the only track in an animation" );
			}

			UpdateIndices();
		}
	}

	// Delete an animation track
	void Animation::DeleteTrack( String^ trackName )
	{
		Maoli::String name = Interop::StringToChar( trackName );
		for ( uint32 i = 0; i < _animation->GetNumTracks(); ++i )
		{
			if ( name == _animation->GetTrack( i )->GetName() )
			{
				_animation->DeleteTrack( i );
				_tracks->RemoveAt( i );
				return;
			}
		}
	}

	// Delete an animation track
	void Animation::DeleteTrack( AnimationTrack^ track )
	{
		DeleteTrack( track->Index );
	}

	// Get the number of animation tracks
	int32 Animation::GetNumTracks()
	{
		if ( _animation )
		{
			return _animation->GetNumTracks();
		}
		return 0;
	}

	// Check if a track is loaded
	bool Animation::IsTrackLoaded( String^ file )
	{
		return _animation->IsTrackLoaded( Interop::StringToChar( file ) );
	}

	// Update track indices
	void Animation::UpdateIndices()
	{
		for ( int32 i = 0; i < _tracks->Count; ++i )
			_tracks[i]->Index = i;
	}


	// Get the tracks
	List<AnimationTrack^>^ Animation::Tracks::get()
	{
		return _tracks;
	}

}

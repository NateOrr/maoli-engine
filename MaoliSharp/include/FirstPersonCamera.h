//--------------------------------------------------------------------------------------
// File: Camera.h
//
// Exposes the camera class to .net
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged

// Forward decl
namespace Maoli
{
	class FirstPersonCamera;
}

#pragma managed
#include "Camera.h"
using namespace System;
using namespace System::Runtime::InteropServices;

namespace MaoliSharp
{
	// Camera modes
	public enum class CameraMode
	{
		FirstPerson,
		ArcBall,
	};

	[System::Security::SuppressUnmanagedCodeSecurity]
	public ref class FirstPersonCamera : public Camera
	{
	public:

		// Ctor
		FirstPersonCamera( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "A user-controlled first person view camera";
		}

		// Mode
		property CameraMode Mode
		{
			CameraMode get();
			void set( CameraMode value );
		}

		// Focal distance
		property float FocusDistance
		{
			float get();
			void set( float value );
		}

	private:

		Maoli::FirstPersonCamera* _firstPersonCamera;
	};
}

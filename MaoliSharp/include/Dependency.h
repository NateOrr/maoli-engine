//--------------------------------------------------------------------------------------
// File: Dependency.h
//
// Component dependency attribute
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma managed
using namespace System;

namespace MaoliSharp
{
	// Attach this attribute to properties that define dependencies to
	// other components. The editor will automatically fill these out
	// in the panel for that component type
	[AttributeUsage( AttributeTargets::Property )]
	ref struct ComponentDependency : public Attribute
	{
		ComponentDependency() {}
	};

	// Transfomation axes
	public enum class AxisMode
	{
		Translate,
		Rotate,
		Scale,
		UniformScale,
	};

	// Attach this attribute to components that can use the axis widget
	[AttributeUsage( AttributeTargets::Property )]
	public ref struct AxisWidget : public Attribute
	{
		AxisWidget( AxisMode axis) : Axis(axis) {}
		AxisMode Axis;
	};
}

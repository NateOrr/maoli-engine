//--------------------------------------------------------------------------------------
// File: ParticleEmitter
//
// 
//
// Nate Orr
//--------------------------------------------------------------------------------------
#pragma once

#pragma unmanaged
#include "Graphics/ParticleEmitter.h"

#pragma managed
using namespace System;
using namespace Honua;
using namespace System::Drawing;
using namespace System::Collections::Generic;
#include "Component.h"
#include "Dependency.h"

// Property helper
#define PROPERTY(type, name) property type name{\
	inline type  get() { return _emitter->Get##name(); }\
	inline void set( type value ) { _emitter->Set##name( value ); }\
}

#define VECTOR_PROPERTY(name) property Vector3 name{\
	inline Vector3 get() { return Vector3( _emitter->Get##name() ); }\
	inline void set( Vector3 value ) { _emitter->Set##name( value.AsMaoliVector() ); }\
}

namespace MaoliSharp
{
	// Forward decl
	ref class Texture;
	ref class ParticleEmitterState;
	ref class ParticleSet;

	public ref class ParticleEmitter : public Component
	{
	public:

		// Ctor
		ParticleEmitter( UnmanagedComponent^ component );

		// Get a description of the component
		static inline String^ GetDescription()
		{
			return "Particle Emitter";
		}

		// Get the unmanaged Light object
		inline Maoli::ParticleEmitter* GetUnmanaged() { return _emitter; }

		// When comparing, just compare the unmanaged pointers
		inline bool Equals( ParticleEmitter^ emitter ) { return (_emitter == emitter->_emitter); }

		// Position
		[AxisWidget( AxisMode::Translate )]
		VECTOR_PROPERTY( Position );

		// Create a particle set
		ParticleSet^ CreateParticleSet();

		// Create a new state
		ParticleEmitterState^ CreateState( String^ name );

		// Add an existing state
		void AddState( ParticleEmitterState^ state, int32 index );

		// Remove a state
		void RemoveState( ParticleEmitterState^ state );

		// Get a state
		inline ParticleEmitterState^ GetState( int32 index ) { return _states[index]; }

		// Get the number of states
		inline int32 GetNumStates() { return _states->Count; }

		// Set the active state
		void SetActiveState( ParticleEmitterState^ state );

	private:
		Maoli::ParticleEmitter*			_emitter;
		List<ParticleEmitterState^>^	_states;
	};


}

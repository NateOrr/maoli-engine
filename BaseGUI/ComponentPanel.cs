﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua
{
    public partial class ComponentPanel : Form
    {
        // Access the group box
        public GroupBox EditorBox
        {
            get { return _groupBox; }
        }

        public ComponentPanel()
        {
            InitializeComponent();
        }

        // Custom rendering code goes here
        public virtual void OnRender()
        {

        }

        // Set the component to edit
        public virtual void SetComponent(object component)
        {

        }

        // Called when a component is added to an entity
        public virtual void OnAddComponent(object component)
        {

        }


        // Called when a component is removed from an entity
        public virtual void OnRemoveComponent(object component)
        {

        }

    }
}

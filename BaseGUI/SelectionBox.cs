﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class SelectionBox : HonuaControl
    {
        int _prevIndex = -1;

        // Access the combo box
        public ComboBox ComboBox
        {
            get { return _box; }
        }

        // Ctor
        public SelectionBox()
        {
            InitializeComponent();
            _box.SelectedIndexChanged += new System.EventHandler(this.OnIndexChanged);
            NullIndex = -1;
            IgnoreEvents = true;
            TextAlignment = 100;
        }

        // Index to ignore
        public int NullIndex { get; set; }

        // Ignores updates if true
        public bool IgnoreEvents { get; set; }

        // Set the property to auto update
        public override PropertyInfo Property
        {
            set
            {
                if (value == null)
                    return;
                if (_object != null)
                {
                    _property = value;
                    IgnoreEvents = true;
                    var obj = _property.GetValue(_object);
                    if (obj != null)
                    {
                        _box.SelectedItem = obj;
                    }
                    else
                    {
                        _box.SelectedIndex = NullIndex;
                    }
                    _prevIndex = _box.SelectedIndex;
                    IgnoreEvents = false;
                    Text = _property.Name;
                }
                else
                    throw new Exception("SelectedObject must be set before Property");
            }
        }

        // Text
        public override String Text
        {
            get { return _label.Text; }
            set
            {
                _label.Text = value;

                // Proper offset
                using (var g = this.CreateGraphics())
                {
                    var size = g.MeasureString(_label.Text, _label.Font);
                    _label.Location = new Point(TextAlignment - (int)size.Width - 5, _label.Location.Y);
                    _box.Location = new Point(TextAlignment, _box.Location.Y);
                }
            }
        }

        // Fill using an enum
        public void SetEnum(Type enumeration)
        {
            _box.Items.Clear();
            var items = Enum.GetValues(enumeration);
            foreach (var item in items)
                _box.Items.Add(item);
            IgnoreEvents = true;
            _box.SelectedIndex = 0;
            IgnoreEvents = false;
        }

        // Undo/redo
        void Undo(object sender, object param)
        {
            IgnoreEvents = true;
            _box.SelectedIndex = (int)sender;
            _property.SetValue(_object, (_box.SelectedIndex == NullIndex) ? null : _box.SelectedItem);
            OnValueChanged();
            IgnoreEvents = false;
        }
        void Redo(object sender, object param)
        {
            IgnoreEvents = true;
            _box.SelectedIndex = (int)param;
            _property.SetValue(_object, (_box.SelectedIndex == NullIndex) ? null : _box.SelectedItem);
            OnValueChanged();
            IgnoreEvents = false;
        }

        private void OnIndexChanged(object sender, EventArgs e)
        {
            if (_property != null && _box.SelectedIndex != -1 && _box.SelectedIndex != _prevIndex && !IgnoreEvents)
            {
                Command.PerformAction(_prevIndex, _box.SelectedIndex, new Action(this.Redo),
                    new Action(this.Undo));
            }
            _prevIndex = _box.SelectedIndex;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class IntBar : HonuaControl
    {
        int _value = 1;
        int _prevValue;
        int _minimum = 0;
        int _maximum = 100;
        bool _mouseDown = false;

        // Ctor
        public IntBar()
        {
            InitializeComponent();
            NumTicks = 1000;
            this.DoubleBuffered = true;
            _bar.Minimum = 0;
            _bar.Maximum = 1000;
            TextAlignment = 100;
        }

        // Set the property to auto update
        public override PropertyInfo Property
        {
            set
            {
                if (value.PropertyType == typeof(int))
                {
                    _property = value;
                    if (_object != null)
                        Value = (int)_property.GetValue(_object);
                    else
                        throw new Exception("SelectedObject must be set before Property");
                }
                else
                    throw new Exception("PropertyType must be a int");
            }
        }

        // Get the value
        public int Value
        {
            get { return _value; }
            set
            {
                int f = value;
                if (f > Maximum) f = Maximum;
                if (f < Minimum) f = Minimum;
                _box.Text = f.ToString();
                int barFactor = (int)NumTicks / (Maximum - Minimum);
                _bar.Value = (int)(f * barFactor);
                _value = f;
                if (_property != null)
                {
                    _property.SetValue(_object, _value);
                    OnValueChanged();
                }
            }
        }

        // Get/set the minimum allowed value
        public int Minimum
        {
            get { return _minimum; }
            set
            {
                if (_value < value) Value = value;
                _minimum = value;
            }
        }

        // Get/set the maximum allowed value
        public int Maximum
        {
            get { return _maximum; }
            set
            {
                if (_value > value) Value = value;
                _maximum = value;
            }
        }

        // Set the label text
        public override String Text 
        {
            get { return _label.Text; }
            set
            {
                _label.Text = value;
                using (Graphics g = this.CreateGraphics())
                {
                    var size = g.MeasureString(_label.Text, _label.Font);
                    _label.Location = new Point(TextAlignment - (int)size.Width - 5, _label.Location.Y);
                    _box.Location = new Point(TextAlignment, _box.Location.Y);
                    _bar.Location = new Point(_box.Location.X + _box.Width + 8, _bar.Location.Y);
                    this.Size = new Size(_bar.Location.X + _bar.Width, this.Height);
                }
            }
        }

        // Set the number of trackbar ticks
        public int NumTicks
        {
            get
            {
                return _bar.Maximum;
            }
            set
            {
                _bar.Maximum = value;
            }
        }


        // Update playback speed when the control loses focus
        private void OnLoseFocus(object sender, EventArgs e)
        {
            try
            {
                int newValue = int.Parse(_box.Text);
                if (_value != newValue)
                {
                    Honua.Command.PerformAction(_value, newValue, new Honua.Action(this.Redo),
                                       new Honua.Action(this.Undo));
                }
            }
            catch
            {
                MessageBox.Show("Please enter a valid floating point value");
            }
        }

        // Update the playback speed on enter
        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                _label.Focus();
                e.Handled = true;
            }
        }

        // Update the range from the scroll bar
        private void OnScroll(object sender, EventArgs e)
        {
            if (!_mouseDown)
                _prevValue = Value;

            int barFactor = (int)NumTicks / (Maximum - Minimum);
            int newValue = (int)_bar.Value / barFactor;

            if (!_mouseDown && _prevValue != newValue)
            {
                Honua.Command.PerformAction(_prevValue, newValue, new Honua.Action(this.Redo),
                   new Honua.Action(this.Undo));
            }
        }

        // Function for undo/redo
        void Undo(object sender, object param)
        {
            Value = (int)sender;
        }
        void Redo(object sender, object param)
        {
            Value = (int)param;
        }

        // Mouse up for the slider to complete an action
        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            _mouseDown = false;
            OnScroll(null, null);
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            _prevValue = Value;
            _mouseDown = true;
        }
    }
}

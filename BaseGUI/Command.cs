﻿using System;
using System.Collections.Generic;
using System.Text;
using Honua;

namespace Honua
{
    // Delegate format for an action
    public delegate void Action(object sender, object param);

    // A command for the undo/redo system
    public class Command
    {
        // A command contains an action as well as an inverse of that action
        Action _action;
        Action _reverse;
        object _sender;
        object _param;

        // Manages actions for undo/redo
        static Stack<Command> _undo = new Stack<Command>();
        static Stack<Command> _redo = new Stack<Command>();

        // GUI
        static UndoRedoForm _window;

        // Set the window to work with
        public static void SetWindow(UndoRedoForm window)
        {
            _window = window;
        }

        // Perform an action
        public static void PerformAction(object sender, object param, Action action, Action reverse)
        {
            var command = new Command(sender, param, action, reverse);
            command.Excecute();
            _undo.Push(command);
            _window.UndoAvailable = true;

            // Cleanup the redo stack
            while (_redo.Count > 0)
                ReleaseCommand(_redo.Pop());
            _window.RedoAvailable = false;
        }

        // Clear all pending actions
        public static void Clear()
        {
            while (_undo.Count > 0)
                ReleaseCommand(_undo.Pop());
            while (_redo.Count > 0)
                ReleaseCommand(_redo.Pop());
        }

        // Cleanup after a command
        private static void ReleaseCommand(Command command)
        {
            if (command._sender != null)
            {
                var dispose = command._sender.GetType().GetMethod("Dispose");
                if (dispose != null)
                    dispose.Invoke(command._sender, null);
                command._sender = null;
            }
            if(command._param != null)
            {
                var dispose = command._param.GetType().GetMethod("Dispose");
                if (dispose != null)
                    dispose.Invoke(command._param, null);
                command._param = null;
            }
        }

        // Undo an action
        public static void Undo()
        {
            if(_undo.Count > 0)
            {
                var command = _undo.Pop();
                command.Reverse();
                _redo.Push(command);
                _window.RedoAvailable = true;
                _window.UndoAvailable = (_undo.Count > 0);
            }
        }

        // Redo an action
        public static void Redo()
        {
            if(_redo.Count > 0)
            {
                var command = _redo.Pop();
                command.Excecute();
                _undo.Push(command);
                _window.UndoAvailable = true;
                _window.RedoAvailable = (_redo.Count > 0);
            }
        }

        // Constructor
        private Command(object sender, object param, Action action, Action reverse)
        {
            _sender = sender;
            _param = param;
            _action = action;
            _reverse = reverse;
        }

        // Performs the action
        void Excecute()
        {
            _action(_sender, _param);
        }

        // Perform the reverse action
        void Reverse()
        {
            _reverse(_sender, _param);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class PropertyPage : UserControl
    {
        object _selectedObject;     // Object to build property page out of
        Type _type;                 // Object type

        // Game engine
        public static object Engine { get; set; }

        // Function used to build the page
        public delegate HonuaControl MakeControlFn(PropertyInfo prop, object obj);
        public static MakeControlFn MakeControl { get; set; }

        // Fires when the value has changed
        public EventHandler PropertyChanged { get; set; }

        public PropertyPage()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            TextAlignment = 150;
        }

        // X location to right align text to
        public int TextAlignment { get; set; }

        // Object to build property page out of
        public object SelectedObject
        {
            get { return _selectedObject; }
            set
            {
                _selectedObject = value;
                if (value == null)
                    return;

                // Get the properties that are flagged for the editor
                var properties = _selectedObject.GetType().GetProperties().Where(
                    prop => Attribute.IsDefined(prop, typeof(HonuaProperty))).ToList();
                properties.Reverse();

                // Don't re-create all the controls if the object is the same type as the last one
                if (_type == _selectedObject.GetType())
                {
                    int index = 0;
                    foreach (var prop in properties)
                    {
                        var control = (HonuaControl)Controls[index];
                        control.SelectedObject = _selectedObject;
                        control.Property = prop;
                        ++index;
                    }
                    return;
                }

                // Setup the controls
                _type = _selectedObject.GetType();
                this.Controls.Clear();
                int height = 0;
                foreach (var prop in properties)
                {
                    var control = MakeControl.Invoke(prop, _selectedObject);
                    if (control != null)
                    {
                        // Vertical spacing
                        control.Location = new Point(control.Location.X, height);
                        height += control.Height;

                        // Horizontal spacing
                        control.TextAlignment = this.TextAlignment;
                        control.Size = new Size(this.Width - control.Location.X - control.Width, control.Height);

                        // Fix naming
                        var chars = prop.Name.ToCharArray();
                        chars[0] = Char.ToUpper(chars[0]);
                        control.Text = new String(chars);

                        // Add it
                        control.SelectedObject = _selectedObject;
                        control.Property = prop;
                        control.PropertyPage = this;
                        Controls.Add(control);
                        this.Width = Math.Max(this.Width, control.Location.X + control.Width);
                        this.Height = Math.Max(this.Height, control.Location.Y + control.Height);
                    }
                }

                // Check for dependencies
                for (int i = 0; i < properties.Count; ++i)
                {
                    var depInfo = properties[i].GetCustomAttribute(typeof(HonuaDependency)) as HonuaDependency;
                    if (depInfo != null)
                    {
                        // Find the property
                        for (int j = 0; j < properties.Count; ++j)
                        {
                            if (properties[j].Name == depInfo.PropertyName)
                            {
                                // Setup the dependency data
                                var dep = new DependentControl();
                                dep.Property = properties[j];
                                dep.Control = Controls[i];
                                dep.Values = depInfo.KeyValues;
                                var currentValue = dep.Property.GetValue(_selectedObject);
                                dep.Control.Enabled = false;
                                foreach (var curValue in dep.Values)
                                {
                                    if (currentValue.Equals(curValue))
                                    {
                                        dep.Control.Enabled = true;
                                        break;
                                    }
                                }

                                // Add to this controls dependency list
                                if (Controls[j].Tag == null)
                                    Controls[j].Tag = new List<DependentControl>();
                                var depList = Controls[j].Tag as List<DependentControl>;
                                var control = (HonuaControl)Controls[j];
                                control.ValueChanged += UpdateControlDependency;
                                depList.Add(dep);
                                break;
                            }
                        }
                    }
                }
            }
        }

        // Dependency data for a control
        class DependentControl
        {
            // Control that is a dependent
            public Control Control { get; set; }

            // Property that defines the dependency
            public PropertyInfo Property { get; set; }

            // Values that will allow the control to become active
            public List<object> Values { get; set; }
        }

        // Validate a control dependency
        void UpdateControlDependency(object sender, EventArgs e)
        {
            // Update all dependencies of this control
            var control = sender as HonuaControl;
            var deps = control.Tag as List<DependentControl>;
            foreach (var depControl in deps)
            {
                // Check the current property value against the desired value
                var currentValue = depControl.Property.GetValue(_selectedObject);
                foreach (var curValue in depControl.Values)
                {
                    if (currentValue.Equals(curValue))
                    {
                        depControl.Control.Enabled = true;
                        break;
                    }
                }
            }
        }

    }
}

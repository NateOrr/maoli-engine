﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Honua
{
    // A windows form that supports undo/redo gui
    public partial class UndoRedoForm : Form
    {
        public UndoRedoForm()
        {
            InitializeComponent();
        }

        // Set the status of undo controls
        public virtual bool UndoAvailable { set {  } }

        // Set the status of redo controls
        public virtual bool RedoAvailable { set {  } }
    }
}

﻿namespace Honua
{
    partial class FloatBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._bar = new System.Windows.Forms.TrackBar();
            this._label = new System.Windows.Forms.Label();
            this._box = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this._bar)).BeginInit();
            this.SuspendLayout();
            // 
            // _bar
            // 
            this._bar.BackColor = System.Drawing.SystemColors.Control;
            this._bar.Location = new System.Drawing.Point(133, 6);
            this._bar.Maximum = 1000;
            this._bar.Minimum = 1;
            this._bar.Name = "_bar";
            this._bar.Size = new System.Drawing.Size(100, 56);
            this._bar.TabIndex = 132;
            this._bar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._bar.Value = 1;
            this._bar.Scroll += new System.EventHandler(this.OnScroll);
            this._bar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this._bar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnMouseUp);
            // 
            // _label
            // 
            this._label.AutoSize = true;
            this._label.Location = new System.Drawing.Point(5, 11);
            this._label.Name = "_label";
            this._label.Size = new System.Drawing.Size(44, 18);
            this._label.TabIndex = 131;
            this._label.Text = "Value";
            // 
            // _box
            // 
            this._box.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._box.Location = new System.Drawing.Point(80, 3);
            this._box.Name = "_box";
            this._box.Size = new System.Drawing.Size(52, 32);
            this._box.TabIndex = 130;
            this._box.Text = "1.0";
            this._box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            this._box.Leave += new System.EventHandler(this.OnLoseFocus);
            // 
            // FloatBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._bar);
            this.Controls.Add(this._label);
            this.Controls.Add(this._box);
            this.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FloatBar";
            this.Size = new System.Drawing.Size(237, 38);
            ((System.ComponentModel.ISupportInitialize)(this._bar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar _bar;
        private System.Windows.Forms.Label _label;
        private System.Windows.Forms.TextBox _box;
    }
}

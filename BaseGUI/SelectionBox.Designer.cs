﻿namespace Honua
{
    partial class SelectionBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._box = new System.Windows.Forms.ComboBox();
            this._label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _box
            // 
            this._box.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._box.FormattingEnabled = true;
            this._box.Location = new System.Drawing.Point(51, 3);
            this._box.Name = "_box";
            this._box.Size = new System.Drawing.Size(103, 26);
            this._box.TabIndex = 2;
            // 
            // _label
            // 
            this._label.AutoSize = true;
            this._label.Location = new System.Drawing.Point(1, 6);
            this._label.Name = "_label";
            this._label.Size = new System.Drawing.Size(44, 18);
            this._label.TabIndex = 3;
            this._label.Text = "label1";
            // 
            // SelectionBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._label);
            this.Controls.Add(this._box);
            this.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "SelectionBox";
            this.Size = new System.Drawing.Size(262, 37);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox _box;
        private System.Windows.Forms.Label _label;

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class StringBox : HonuaControl
    {
        String _value = "";

        // Ctor
        public StringBox()
        {
            InitializeComponent();
            TextAlignment = 15;
        }

        // Set the property to auto update
        public override PropertyInfo Property
        {
            set
            {
                if (value == null)
                    return;
                if (value.PropertyType == typeof(String))
                {
                    _property = value;
                    if (_object != null)
                        Value = (String)_property.GetValue(_object);
                    else
                        throw new Exception("SelectedObject must be set before Property");
                    Text = _property.Name;
                }
                else
                    throw new Exception("PropertyType must be a String");
            }
        }

        // Get the value
        public String Value 
        {
            get { return _box.Text; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    _box.Text = value;
                    if (_property != null)
                    {
                        _property.SetValue(_object, _value);
                        OnValueChanged();
                    }
                }
            }
        }

        // Set the label text
        public override String Text 
        { 
            get { return _label.Text; } 
            set 
            { 
                _label.Text = value;

                // Proper offset
                using (var g = this.CreateGraphics())
                {
                    var size = g.MeasureString(_label.Text, _label.Font);
                    _label.Location = new Point(TextAlignment - (int)size.Width - 5, _label.Location.Y);
                    _box.Location = new Point(TextAlignment, _box.Location.Y);
                    this.Size = new Size(_box.Location.X + _box.Width, this.Height);
                }
            } 
        }

        // Update playback speed when the control loses focus
        private void OnLoseFocus(object sender, EventArgs e)
        {
            try
            {
                String newValue = _box.Text;
                if (newValue != _value)
                {
                    Honua.Command.PerformAction(_value, newValue, new Honua.Action(this.Redo),
                                       new Honua.Action(this.Undo));
                }
            }
            catch
            {
                MessageBox.Show("Please enter a valid floating point value");
            }
        }

        // Update the playback speed on enter
        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                _label.Focus();
                e.Handled = true;
            }
        }

        // Function for undo/redo
        void Undo(object sender, object param)
        {
            Value = (String)sender;
        }
        void Redo(object sender, object param)
        {
            Value = (String)param;
        }
    }
}

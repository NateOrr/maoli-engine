﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honua
{

    // Property dependency
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaDependency : Attribute
    {
        public String PropertyName { get; set; }
        public List<object> KeyValues { get; set; }

        public HonuaDependency(String property, object value)
        {
            PropertyName = property;
            KeyValues = new List<object>(1);
            KeyValues.Add(value);
        }

        public HonuaDependency(String property, params object[] args)
        {
            PropertyName = property;
            KeyValues = new List<object>(args.Length);
            for (int i = 0; i < args.Length; ++i)
                KeyValues.Add(args[i]);
        }
    }

    // Property attribute base class
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaProperty : Attribute
    {

    }

    // Checkbox
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaBool : HonuaProperty
    {

    }

    // Textbox
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaText : HonuaProperty
    {

    }

    // Vector
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaVector : HonuaProperty
    {
        public float Minimum { get; set; }
        public float Maximum { get; set; }

        public HonuaVector()
        {
            Minimum = -1000.0f;
            Maximum = 1000.0f;
        }

        public HonuaVector(float min, float max)
        {
            Minimum = min;
            Maximum = max;
        }
    }

    // ColorBox
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaColor : HonuaProperty
    {

    }


    // TextureBox
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaTexture : HonuaProperty
    {

    }

    // Floating point control types
    public enum ValueControls
    {
        Box,
        Slider,
    }

    // Floating point control
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaFloat : HonuaProperty
    {
        public ValueControls Type { get; set; }
        public float Minimum { get; set; }
        public float Maximum { get; set; }

        public HonuaFloat(ValueControls type, float min, float max)
        {
            Type = type;
            Minimum = min;
            Maximum = max;
        }
    }

    // Integer control
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaInt : HonuaProperty
    {
        public ValueControls Type { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public HonuaInt(ValueControls type, int min, int max)
        {
            Type = type;
            Minimum = min;
            Maximum = max;
        }
    }

    // Selection box targets
    public enum SelectionBoxTarget
    {
        Enum,
        Component,
        Entity,
    }

    // Selection box
    [AttributeUsage(AttributeTargets.Property)]
    public class HonuaSelectionBox : HonuaProperty
    {
        public SelectionBoxTarget Target { get; set; }
        public int NullIndex { get; set; }
        public Type ItemType { get; set; }

        public HonuaSelectionBox(SelectionBoxTarget target, Type type, int nullIndex)
        {
            Target = target;
            NullIndex = nullIndex;
            ItemType = type;
        }

        public HonuaSelectionBox(SelectionBoxTarget target, int nullIndex)
        {
            Target = target;
            NullIndex = nullIndex;
        }

        public HonuaSelectionBox(SelectionBoxTarget target, Type type)
        {
            Target = target;
            NullIndex = -1;
            ItemType = type;
        }

        public HonuaSelectionBox(SelectionBoxTarget target)
        {
            Target = target;
            NullIndex = -1;
        }
    }
}

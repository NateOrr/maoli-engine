﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class BoolBox : HonuaControl
    {
        bool _value = false;
        Bitmap _checkedImage;
        Bitmap _uncheckedImage;

        // Ctor
        public BoolBox()
        {
            InitializeComponent();
            TextAlignment = 100;
            _checkedImage = LoadBitmap("Resources\\Textures\\Editor\\checkBox.png");
            _uncheckedImage = LoadBitmap("Resources\\Textures\\Editor\\uncheckBox.png");
            _box.BackgroundImage = _uncheckedImage;
        }

        Bitmap LoadBitmap(String file)
        {
            using (var bmpTemp = new Bitmap(file))
            {
                return new Bitmap(bmpTemp);
            }
        }

        // Set the property to auto update
        public override PropertyInfo Property
        {
            set
            {
                if (value.PropertyType == typeof(bool))
                {
                    _property = value;
                    if (_object != null)
                        Value = (bool)_property.GetValue(_object);
                    else
                        throw new Exception("SelectedObject must be set before Property");
                }
                else
                    throw new Exception("PropertyType must be a bool");
            }
        }

        // Get the value
        public bool Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _box.BackgroundImage = _value ? _checkedImage : _uncheckedImage;
                if (_property != null)
                {
                    _property.SetValue(_object, _value);
                    OnValueChanged();
                }
            }
        }

        // Set the label text
        public override String Text
        {
            get { return _label.Text; }
            set
            {
                _label.Text = value;

                // Proper offset
                using (var g = this.CreateGraphics())
                {
                    var size = g.MeasureString(_label.Text, _label.Font);
                    _label.Location = new Point(TextAlignment - (int)size.Width - 5, _label.Location.Y);
                    _box.Location = new Point(TextAlignment, _box.Location.Y);
                    this.Size = new Size(_box.Location.X + _box.Width, this.Height);
                }
            }
        }

        // Function for undo/redo
        void Undo(object sender, object param)
        {
            Value = (bool)sender;
        }
        void Redo(object sender, object param)
        {
            Value = (bool)param;
        }

        private void OnClick(object sender, EventArgs e)
        {
            Command.PerformAction(_value, !_value, new Action(Redo), new Action(Undo));
        }
    }
}

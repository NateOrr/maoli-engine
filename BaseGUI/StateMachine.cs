﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Honua
{
    // State interface
    public interface IState
    {
        // Enter the state
        void Enter();

        // Update/Render
        void Update();

        // Exit/cleanup
        void Exit();
    }

    // Finite state machine
    public class StateMachine
    {
        IState _currentState;

        // Set the state
        public void SetState(IState state)
        {
            if (_currentState != null)
                _currentState.Exit();
            _currentState = state;
            _currentState.Enter();
        }

        // Update
        public void Update()
        {
            if (_currentState != null)
                _currentState.Update();
        }
    }
}

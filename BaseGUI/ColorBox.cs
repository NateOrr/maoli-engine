﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class ColorBox : HonuaControl
    {
        Color _value = Color.White;
        Color _prevValue = Color.White;

        // Ctor
        public ColorBox()
        {
            InitializeComponent();
            TextAlignment = 100;
        }

        // Set the property to auto update
        public override PropertyInfo Property
        {
            set
            {
                if (value.PropertyType == typeof(Color))
                {
                    _property = value;
                    if (_object != null)
                        Value = (Color)_property.GetValue(_object);
                    else
                        throw new Exception("SelectedObject must be set before Property");
                }
                else
                    throw new Exception("PropertyType must be a Color");
            }
        }

        // Get the value
        public Color Value 
        { 
            get { return _value; }
            set
            {
                _box.BackColor = value;
                _prevValue = _value;
                _value = value;
                if (_property != null)
                {
                    _property.SetValue(_object, _value);
                    OnValueChanged();
                }
            }
        }

        // Set the label text
        public override String Text
        { 
            get { return _label.Text; } 
            set 
            { 
                _label.Text = value;

                // Proper offset
                using (var g = this.CreateGraphics())
                {
                    var size = g.MeasureString(_label.Text, _label.Font);
                    _label.Location = new Point(TextAlignment - (int)size.Width - 5, _label.Location.Y);
                    _box.Location = new Point(TextAlignment, _box.Location.Y);
                    this.Size = new Size(_box.Location.X + _box.Width, this.Height);
                }
            } 
        }

        // Function for undo/redo
        void Undo(object sender, object param)
        {
            Value = (Color)sender;
        }
        void Redo(object sender, object param)
        {
            Value = (Color)param;
        }

        private void OnClick(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.AnyColor = true;
            dlg.AllowFullOpen = true;
            dlg.FullOpen = false;
            dlg.SolidColorOnly = false;
            if (dlg.ShowDialog() == DialogResult.OK && _prevValue != dlg.Color)
            {
                Command.PerformAction(_prevValue, dlg.Color, new Action(Redo), new Action(Undo));
            }
        }
    }
}

﻿namespace Honua
{
    partial class IntBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._label = new System.Windows.Forms.Label();
            this._box = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _label
            // 
            this._label.AutoSize = true;
            this._label.Location = new System.Drawing.Point(5, 11);
            this._label.Name = "_label";
            this._label.Size = new System.Drawing.Size(44, 18);
            this._label.TabIndex = 131;
            this._label.Text = "Value";
            // 
            // _box
            // 
            this._box.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._box.Location = new System.Drawing.Point(80, 3);
            this._box.Name = "_box";
            this._box.Size = new System.Drawing.Size(52, 32);
            this._box.TabIndex = 130;
            this._box.Text = "1.0";
            this._box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnKeyPress);
            this._box.Leave += new System.EventHandler(this.OnLoseFocus);
            // 
            // IntBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._label);
            this.Controls.Add(this._box);
            this.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "IntBox";
            this.Size = new System.Drawing.Size(197, 38);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _label;
        private System.Windows.Forms.TextBox _box;
    }
}

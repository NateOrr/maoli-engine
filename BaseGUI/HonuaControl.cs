﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Honua
{
    public partial class HonuaControl : UserControl
    {
        public HonuaControl()
        {
            InitializeComponent();
        }

        public PropertyPage PropertyPage { get; set; }
        protected PropertyInfo _property;
        protected object _object;
        protected bool _ignoreEvents = false;

        // X location to right align text to
        public int TextAlignment { get; set; }

        // Fires when the value has changed
        public EventHandler ValueChanged { get; set; }

        // Set the property to auto update
        public virtual PropertyInfo Property
        {
            get { return _property; }
            set
            {

            }
        }

        // Set the selected object to update
        public object SelectedObject { set { _object = value; } }

        protected void OnValueChanged()
        {
            if (ValueChanged != null)
                ValueChanged.Invoke(this, null);
            if (PropertyPage != null && PropertyPage.PropertyChanged != null)
            {
                PropertyPage.PropertyChanged.Invoke(this, null);
            }
        }
    }
}
